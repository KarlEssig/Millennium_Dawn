music_station = "MD_Soundtrack"
music = {
	song = "maintheme"
	chance = {
		base = 5
		modifier = {
			threat > 0.4
			add = -5
		}
		modifier = {
			has_war = yes
			add = -5
		}
	}
}
music = {
	song = "Modern Day 4 Main Theme"
	chance = {
		base = 15
		modifier = {
			threat > 0.4
			add = -10
		}
		modifier = {
			has_war = yes
			add = -15
		}
	}
}
music = {
	song = "Looking into the distance"
	chance = {
		base = 25
		modifier = {
			threat > 0.4
			add = -15
		}
		modifier = {
			has_war = yes
			add = -25
		}
	}
}
music = {
	song = "Rise of Europe"
	chance = {
		base = 25
		modifier = {
			threat > 0.4
			add = -15
		}
		modifier = {
			has_war = yes
			add = -25
		}
	}
}
music = {
	song = "Global Equilibrium"
	chance = {
		base = 25
		modifier = {
			threat > 0.4
			add = -15
		}
		modifier = {
			has_war = yes
			add = -25
		}
	}
}
music = {
	song = "The world is yours"
	chance = {
		base = 25
		modifier = {
			threat > 0.4
			add = -15
		}
		modifier = {
			has_war = yes
			add = -25
		}
	}
}
music = {
	song = "Shadows of Influence"
	chance = {
		base = 25
		modifier = {
			threat > 0.4
			add = -15
		}
		modifier = {
			has_war = yes
			add = -25
		}
	}
}
music = {
	song = "Building the Future"
	chance = {
		base = 25
		modifier = {
			threat > 0.4
			add = -15
		}
		modifier = {
			has_war = yes
			add = -25
		}
	}
}
music = {
	song = "Mopikel - Back from Hell, alive"
	chance = {
		base = 15
		modifier = {
			threat > 0.4
			add = -10
		}
		modifier = {
			has_war = yes
			add = -15
		}
	}
}
music = {
	song = "Mopikel - Industrial destruction"
	chance = {
		base = 15
		modifier = {
			threat > 0.4
			add = -10
		}
		modifier = {
			has_war = yes
			add = -15
		}
	}
}
music = {
	song = "Mopikel - Technologies for Peace"
	chance = {
		base = 15
		modifier = {
			threat > 0.4
			add = -15
		}
		modifier = {
			has_war = yes
			add = -25
		}
	}
}
music = {
	song = "Distant Horizons"
	chance = {
		base = 25
		modifier = {
			threat > 0.4
			add = -15
		}
		modifier = {
			has_war = yes
			add = -25
		}
	}
}
###TENSION
music = {
	song = "Rising Tempest"
	chance = {
		base = 25
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -15
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 30
		}
	}
}
music = {
	song = "Fault Lines"
	chance = {
		base = 25
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -15
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 30
		}
	}
}
music = {
	song = "Diplomatic Breakdown"
	chance = {
		base = 25
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -15
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 30
		}
	}
}
music = {
	song = "Edge of the Abyss"
	chance = {
		base = 25
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -15
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 30
		}
	}
}
music = {
	song = "Countdown to Chaos"
	chance = {
		base = 25
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -15
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 30
		}
	}
}
music = {
	song = "Brink of War"
	chance = {
		base = 25
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -15
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 30
		}
	}
}
music = {
	song = "Mopikel - Ambush"
	chance = {
		base = 25
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -15
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 30
		}
	}
}
music = {
	song = "Pax Britannica: Uprising"
	chance = {
		base = 0
		modifier = {
			has_government = democratic
			add = 25
		}
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -15
		}
	}
}
###TENSION USA
music = {
	song = "Soldiers Without Guns"
	chance = {
		base = 0
		modifier = {
			original_tag = USA
			add = 25
		}
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -10
		}
	}
}
music = {
	song = "High Seas"
	chance = {
		base = 0
		modifier = {
			original_tag = USA
			add = 25
		}
		modifier = {
			threat < 0.4
			add = -25
		}
		modifier = {
			has_war = yes
			add = -10
		}
	}
}
###WAR
music = {
	song = "Global Conflagration"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "War Machine"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "Skies Aflame"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "Total Mobilization"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "Theatres of Destruction"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "Broken"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "Covert ops"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "Iron Resolve"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "Thunder of Victory"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
music = {
	song = "War in Kashmir"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
		}
		modifier = {
			any_other_country = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
			add = 40
		}
	}
}
###WAR GOING BAD
music = {
	song = "Collapse"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
			surrender_progress > 0.5
		}
	}
}
music = {
	song = "No Way Back"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
			surrender_progress > 0.5
		}
	}
}
music = {
	song = "We Remember"
	chance = {
		base = 0
		modifier = {
			has_war = yes
			add = 35
			surrender_progress > 0.5
		}
	}
}
### Fake Commercials
# Set to play seldom - are meant to occationally lift the mood

music = {
	song = "Commercial - Cashkick"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Incredible Opportunity"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Juicy Fresh"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Moon Realestate Corporation"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Nigeria Buisnessman"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - The Cheesy Finale"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Transnistrian Investment Authority"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - The Love Confirmator"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}