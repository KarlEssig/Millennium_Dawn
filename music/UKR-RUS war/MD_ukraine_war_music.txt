music_station = "MD_ukrwar_music"
music = {
	song = "--UKRAINE PLAYLIST--"
	chance = {
		base = 0
	}	
}
music = {
	song = "Heights - Tartak"
	chance = {
		base = 0
		modifier = {
			original_tag = UKR
			add = 1
		}
		modifier = {
			OR = {
				has_war_with = SOV
				has_war_with = DPR
				has_war_with = HPR
				has_war_with = OPR
				has_war_with = LPR
				has_war_with = CRM
				has_war_with = WAG
			}
			original_tag = UKR
			add = 45
		}
	}	
}
music = {
	song = "My Knight's Cross - Tartak"
	chance = {
		base = 0
		modifier = {
			original_tag = UKR
			add = 1
		}
		modifier = {
			OR = {
				has_war_with = SOV
				has_war_with = DPR
				has_war_with = HPR
				has_war_with = OPR
				has_war_with = LPR
				has_war_with = CRM
				has_war_with = WAG
			}
			original_tag = UKR
			add = 45
		}
	}	
}
music = {
	song = "Piryatin - Arta"
	chance = {
		base = 0
		modifier = {
			original_tag = UKR
			add = 1
		}
		modifier = {
			OR = {
				has_war_with = SOV
				has_war_with = DPR
				has_war_with = HPR
				has_war_with = OPR
				has_war_with = LPR
				has_war_with = CRM
				has_war_with = WAG
			}
			original_tag = UKR
			add = 45
		}
	}	
}
music = {
	song = "Irpin Ballad"
	chance = {
		base = 0
		modifier = {
			original_tag = UKR
			add = 1
		}
		modifier = {
			OR = {
				has_war_with = SOV
				has_war_with = DPR
				has_war_with = HPR
				has_war_with = OPR
				has_war_with = LPR
				has_war_with = CRM
				has_war_with = WAG
			}
			original_tag = UKR
			add = 45
		}
	}	
}
music = {
	song = "93rd Kholodnyi Yar"
	chance = {
		base = 0
		modifier = {
			original_tag = UKR
			add = 1
		}
		modifier = {
			OR = {
				has_war_with = SOV
				has_war_with = DPR
				has_war_with = HPR
				has_war_with = OPR
				has_war_with = LPR
				has_war_with = CRM
				has_war_with = WAG
			}
			original_tag = UKR
			add = 45
		}
	}	
}
music = {
	song = "ANTYTILA - Fortress Bakhmut"
	chance = {
		base = 0
		modifier = {
			original_tag = UKR
			add = 1
		}
		modifier = {
			OR = {
				has_war_with = SOV
				has_war_with = DPR
				has_war_with = HPR
				has_war_with = OPR
				has_war_with = LPR
				has_war_with = CRM
				has_war_with = WAG
			}
			original_tag = UKR
			add = 45
		}
	}	
}
music = {
	song = "--RUSSIAN PLAYLIST--"
	chance = {
		base = 0
	}	
}
music = {
	song = "Arise Donbas"
	chance = {
		base = 0
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			add = 1
		}
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			has_war_with = UKR
			add = 45
		}
	}	
}
music = {
	song = "Donbas behind us"
	chance = {
		base = 0
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			add = 1
		}
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			has_war_with = UKR
			add = 45
		}
	}	
}
music = {
	song = "Victory is Ours"
	chance = {
		base = 0
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			add = 1
		}
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			has_war_with = UKR
			add = 45
		}
	}	
}
music = {
	song = "Heroes"
	chance = {
		base = 0
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			add = 1
		}
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			has_war_with = UKR
			add = 45
		}
	}	
}
music = {
	song = "We will not leave"
	chance = {
		base = 0
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			add = 1
		}
		modifier = {
			OR = {
				original_tag = SOV
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = CRM
				original_tag = WAG
			}
			has_war_with = UKR
			add = 45
		}
	}	
}