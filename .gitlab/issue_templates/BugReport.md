# Issue Name

_Ensure the issue title has ``Bug:`` and the label Bug associated to it. The issue requires the following information in this issue:_

Bug Title: ``Replace with title``

Country: ``Replace with tag``

## Steps to Reproduce

_Please detail all of the steps to reproduce the bug._

### What is the current behavior?

### What is the expected behavior?

## Importance

_1 to 10 -- 10 being the most important, 1 being the least important. Scaling revolves around the following guidelines: 1-5; Minor Mechanics, 5-8; Country Specific Content, 8-10; Global Mechanic or Changes_


# Description of the Issue

_This section is where you put the "meat" of the issue. Any details, images or otherwise you would like to attach to the issue place below._

/milestone %"Version 1.9 Roadmap"

/assign me

/label ~Bug