# Summary

_DELETE ME: Replace me with details of the merge request. Specifically, state all of the changes that are being made within the requested merged branch._
_DELETE ME: When creating your merge request ensure you enable the buttons "squash commits" and "delete source branch". This is for cleanup purposes when a project is now merged into master. Branches should be short lived and merged quickly so they do not run into "age" issues. If your project is still ongoing we want to delete the branch and then recreate it from master to "clean" the git history. If you have questions please ping @AngriestBird._

_DELETE ME: Please make sure you have updated the changelog with any of your changes that are to be merged._

Example:

- Added new tags TAG TAG TAG TAG
- New focus tree for Slavistan
- Added new game rules for Monaco
- New python test

/assign me

/reviewer @AngriestBird @WarnerDev @Kalkalash @amtoj @crocomoth @alexmarkel0v @TraianoGitHub @KianGHK1530 @wellihavenoidea @mahhouse @XCezor @deflinok1