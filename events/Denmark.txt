﻿add_namespace = denmark
add_namespace = denmark_news
add_namespace = denmark_agrar_event

country_event = {
	id = denmark.2
	title = denmark.2.t
	desc = denmark.2.d
	picture = GFX_denmark_conservatives
	is_triggered_only = yes

	option = {
		name = denmark.2.a
		log = "[GetDateText]: [This.GetName]: denmark.2.a executed"

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark.3
	title = denmark.3.t
	desc = denmark.3.d
	picture = GFX_denmark_margrethe
	is_triggered_only = yes

	option = {
		name = denmark.3.a
		log = "[GetDateText]: [This.GetName]: denmark.3.a executed"

		ai_chance = {
			base = 1
		}
	}
}

#Negotiate foreign air license
country_event = {
	id = denmark.4
	title = denmark.4.t
	desc = denmark.4.d
	picture = GFX_fighter_jet
	fire_only_once = yes
	is_triggered_only = yes

	##Accept Send Request
	option = {
		name = denmark.4.o1
		trigger = { has_idea = NATO_member }
		log = "[GetDateText]: [This.GetName]: denmark.4.o1 executed"
		USA = { country_event = { id = denmark.4001 days = 1 } }

		ai_chance = { base = 1 }
	}
	option = {
		name = denmark.4.o2
		trigger = { is_in_faction_with = SOV }
		log = "[GetDateText]: [This.GetName]: denmark.4.o2 executed"
		SOV = { country_event = { id = denmark.4001 days = 1 } }

		ai_chance = { base = 1 }
	}

	#Deny the Request/We Do not have the funds
	option = {
		name = denmark.4.o3
		log = "[GetDateText]: [This.GetName]: denmark.4.o3 executed"
		add_political_power = 50
		add_war_support = -0.03

		ai_chance = {
			base = 0
			modifier = {
				add = 10
				OR = {
					check_variable = { interest_rate > 7.999 }
					has_idea = depression
				}
			}
		}
	}
}
country_event = {
	id = denmark.4001
	title = denmark.4001.t
	desc = {
		trigger = {
			is_in_faction = yes
			has_idea = NATO_member
		}
		text = denmark.4001.d1
	}
	desc = {
		trigger = {
			is_in_faction = no
			NOT = { has_idea = NATO_member }
		}
		text = denmark.4001.d1
	}
	picture = GFX_fighter_jet
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.4001.o1
		log = "[GetDateText]: [This.GetName]: denmark.4001.o1 executed"

		ai_chance = { base = 1 }
	}

	option = {
		name = denmark.4001.o2
		log = "[GetDateText]: [This.GetName]: denmark.4001.o2 executed"

		ai_chance = {
			base = 0
			modifier = {
				add = 2
				THIS = { check_variable = { opinion@DEN < 11 } }
			}
		}
	}
}
# SOV/USA ACCEPTS OUR REQUEST
country_event = {
	id = denmark.4002
	title = denmark.4002.t
	desc = {
		trigger = {
			has_idea = NATO_member
			DEN = { has_idea = NATO_member }
		}
		text = denmark.4002.d1
	}
	desc = {
		trigger = {
			is_in_faction_with = DEN
			DEN = { is_in_faction_with = SOV }
		}
		text = denmark.4002.d2
	}
	picture = GFX_fighter_jet
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.4002.o1
		log = "[GetDateText]: [This.GetName]: denmark.4002.o1 executed"
		IF = {
			limit = { 
				has_dlc = "By Blood Alone"
				has_idea = NATO_member 
			}
			add_equipment_to_stockpile = {
				type = small_plane_strike_airframe_2
				amount = 15
				variant_name = "F-16A Blk 20"
				producer = USA
			}
		}
		else_if = {
			limit = { 
				NOT = {
					has_dlc = "By Blood Alone"
				}
				has_idea = NATO_member 
			}
			add_equipment_to_stockpile = {
				type = MR_Fighter2
				amount = 15
				producer = USA
			}
		}
		IF = {
			limit = { 
				has_dlc = "By Blood Alone"
				is_in_faction_with = SOV 
			}
			add_equipment_to_stockpile = {
				type = MR_Fighter2
				amount = 15
				producer = SOV
			}
		}
		else_if = {
			limit = { 
				NOT = {
					has_dlc = "By Blood Alone"
				}
				is_in_faction_with = SOV
			}
			add_equipment_to_stockpile = {
				type = small_plane_airframe_2
				amount = 15
				variant_name = "MiG-29 Fulcrum"
				producer = SOV
			}
		}
	}
}

# SOV/USA DENIES OUR REQUEST
country_event = {
	id = denmark.4003
	title = denmark.4003.t
	desc = {
		trigger = {
			has_idea = NATO_member
			DEN = { has_idea = NATO_member }
		}
		text = denmark.4003.d1
	}
	desc = {
		trigger = {
			is_in_faction_with = DEN
			DEN = { is_in_faction_with = SOV }
		}
		text = denmark.4003.d2
	}
	picture = GFX_fighter_jet
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.4003.o1
		log = "[GetDateText]: [This.GetName]: denmark.4003.o1 executed"
	}
}

# Reactionary Focus
country_event = {
	id = denmark.6
	title = denmark.6.t
	desc = denmark.6.d
	picture = GFX_denmark_conservatives
	is_triggered_only = yes

	option = {
		name = denmark.6.o1
		log = "[GetDateText]: [This.GetName]: denmark.6.o1 executed"

		ai_chance = {
			base = 1
		}
	}
}

# Conservative Focus
country_event = {
	id = denmark.7
	title = denmark.7.t
	desc = denmark.7.d
	picture = GFX_denmark_conservatives
	is_triggered_only = yes

	option = {
		name = denmark.7.o1
		log = "[GetDateText]: [This.GetName]: denmark.7.o1 executed"

		ai_chance = {
			base = 1
		}
	}
}

# Monarchist Focus
country_event = {
	id = denmark.8
	title = denmark.8.t
	desc = denmark.8.d
	picture = GFX_denmark_margrethe
	is_triggered_only = yes

	option = {
		name = denmark.8.a
		log = "[GetDateText]: [This.GetName]: denmark.8.a executed"

		ai_chance = {
			base = 0.3
		}
	}
	option = {
		name = denmark.8.b
		log = "[GetDateText]: [This.GetName]: denmark.8.b executed"

		ai_chance = {
			base = 0.3
		}
	}
	option = {
		name = denmark.8.c
		log = "[GetDateText]: [This.GetName]: denmark.8.c executed"

		ai_chance = {
			base = 0.3
		}
	}
}

# Martial Law declared
country_event = {
	id = denmark.9
	title = denmark.9.t
	desc = denmark.9.d
	picture = GFX_message_signing
	is_triggered_only = yes

	option = {
		name = denmark.9.o1
		log = "[GetDateText]: [This.GetName]: denmark.9.o1 executed"
		add_stability = -0.05
		set_temp_variable = { party_index = 4 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		add_relative_party_popularity = yes

		ai_chance = {
			base = 1
		}
	}
}

# Rally to Dannebrog
country_event = {
	id = denmark.10
	title = denmark.10.t
	desc = denmark.10.d
	picture = GFX_political_activist
	is_triggered_only = yes

	option = {
		name = denmark.10.o1
		log = "[GetDateText]: [This.GetName]: denmark.10.o1 executed"

		ai_chance = {
			base = 1
		}
	}
}

# Nationalist
country_event = {
	id = denmark.11
	title = denmark.11.t
	desc = denmark.11.d
	picture = GFX_politics_far_right
	is_triggered_only = yes

	option = {
		name = denmark.11.o1
		log = "[GetDateText]: [This.GetName]: denmark.11.o1 executed"

		ai_chance = {
			base = 1
		}
	}
}

#Crown Prince Frederik and His little brother becomes Field Marshal and general
country_event = {
	id = denmark.18
	title = denmark.18.t
	desc = denmark.18.d
	picture = GFX_ace_promoted
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.18.a
		log = "[GetDateText]: [This.GetName]: denmark.18.a executed"
		create_field_marshal = {
			name = "Crown Prince Frederik"
			picture = "Prince_Frederik_Mil.dds"
			traits = { ranger commando brilliant_strategist infantry_officer }
			id = 16839
			skill = 3
			attack_skill = 3
			defense_skill = 3
			planning_skill = 3
			logistics_skill = 3
		}
		create_corps_commander = {
			name = "Prince Joachim"
			picture = "Prince_Joachim_Mil.dds"
			traits = { armoured_cavalry_officer  artillery_officer }
			id = 16840
			skill = 2
			attack_skill = 2
			defense_skill = 2
			planning_skill = 2
			logistics_skill = 2
		}

		ai_chance = {
			base = 1
		}
	}
}

#Crown Prince Frederik Becomes King
country_event = {
	id = denmark.20
	title = denmark.20.t
	desc = denmark.20.d
	picture = GFX_united_states_capitol
	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = denmark.20.a
		log = "[GetDateText]: [This.GetName]: denmark.20.a executed"
			create_country_leader = {
			name = "King Frederik The 10th"
			desc = ""
			picture = Prince_Frederik.dds expire = "2085.1.1"
			ideology = Monarchist
			traits = {
				humble
				likeable
				military_career
			}
		}

		ai_chance = {
			base = 1
		}
	}
}

#International Diplomacy
country_event = {
	id = denmark.19
	title = denmark.19.t
	desc = denmark.19.d
	picture = GFX_report_event_Scarico_Merci_1
	is_triggered_only = yes

	option = {
		name = denmark.19.a		#England accepts purchase
		log = "[GetDateText]: [This.GetName]: denmark.19.a executed"
		DEN = {
			transfer_state = 437
			transfer_state = 1197
			set_temp_variable = { treasury_change = -50 }
			modify_treasury_effect = yes
		}
		ENG = {
		set_temp_variable = { treasury_change = 50 }
		modify_treasury_effect = yes
		}

		ai_chance = {
			base = 10
		}
	}

	option = {
		name = denmark.19.b		#That's gonna be a no from me dog
		log = "[GetDateText]: [This.GetName]: denmark.19.b executed"

		ai_chance = {
			base = 90
		}
	}
}

# let's create a faction
country_event = {
	id = denmark.21
	title = denmark.21.t
	desc = denmark.21.d
	picture = GFX_church_of_sweden
	is_triggered_only = yes

	option = {
		name = denmark.21.a		#The Kalmar Union upgraded
		log = "[GetDateText]: [This.GetName]: denmark.21.a executed"
		trigger = { is_subject = no }
		DEN = { add_to_faction = PREV }

		ai_chance = {
			base = 90
			modifier = {
				factor = 0.05
				is_in_faction = yes
			}
			modifier = {
				factor = 0
				is_faction_leader = yes
			}
		}
	}

	option = {
		name = denmark.21.b		#That's gonna be a no from me dog
		log = "[GetDateText]: [This.GetName]: denmark.21.b executed"
		trigger = { is_subject = no }

		ai_chance = {
			base = 10
		}
	}

	option = {
		name = denmark.21.c # Not for us to decide
		log = "[GetDateText]: [This.GetName]: denmark.21.c executed"
		trigger = { is_subject = yes }

		ai_chance = {
			base = 10
		}
	}
}

# Fascist
country_event = {
	id = denmark.12
	title = denmark.12.t
	desc = denmark.12.d
	picture = GFX_politics_nazi
	is_triggered_only = yes

	option = {
		name = denmark.12.o1
		log = "[GetDateText]: [This.GetName]: denmark.12.o1 executed"

		ai_chance = {
			base = 1
		}
	}
}

# Fight the Nationalists
country_event = {
	id = denmark.13
	title = denmark.13.t
	desc = denmark.13.d
	picture = GFX_politics_censorship
	is_triggered_only = yes

	option = {
		name = denmark.13.o1
		log = "[GetDateText]: [This.GetName]: denmark.13.o1 executed"
		add_popularity = { ideology = nationalist popularity = -0.10 }
		add_popularity = { ideology = democratic popularity = -0.05 }
		add_popularity = { ideology = neutrality popularity = -0.05 }
		add_manpower = 10000
		recalculate_party = yes

		ai_chance = {
			base = 1
		}
	}
}

# Communism
country_event = {
	id = denmark.17
	title = denmark.17.t
	desc = denmark.17.d
	picture = GFX_politics_communism
	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = denmark.17.o1
		log = "[GetDateText]: [This.GetName]: denmark.17.o1 executed"

		ai_chance = {
			base = 1
		}
	}
}

#puppet Iceland
country_event = {
	id = denmark.39
	title = denmark.39.t
	desc = denmark.39.d
	picture = GFX_politics_communism
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = denmark.39.a
		log = "[GetDateText]: [This.GetName]: denmark.39.a executed"
		DEN = { puppet = ICE }

		ai_chance = {
			factor = 5
			modifier = {
				factor = 100
				has_government = communism
			}
		}
	}
	option = { #No
		name = denmark.39.b
		log = "[GetDateText]: [This.GetName]: denmark.39.b executed"

		ai_chance = {
			factor = 95
			modifier = {
				factor = 0
				has_government = communism
			}
		}
	}
}

#puppet Sweden
country_event = {
	id = denmark.40
	title = denmark.40.t
	desc = denmark.40.d
	picture = GFX_politics_communism
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = denmark.40.a
		log = "[GetDateText]: [This.GetName]: denmark.40.a executed"
		DEN = { puppet = SWE }

		ai_chance = {
			factor = 5
			modifier = {
				factor = 100
				has_government = communism
			}
		}
	}
	option = { #No
		name = denmark.40.b
		log = "[GetDateText]: [This.GetName]: denmark.40.b executed"
		ai_chance = {
			factor = 95
			modifier = {
				factor = 0
				has_government = communism
			}
		}
	}
}

#puppet Norway
country_event = {
	id = denmark.41
	title = denmark.41.t
	desc = denmark.41.d
	picture = GFX_politics_communism
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = denmark.41.a
		log = "[GetDateText]: [This.GetName]: denmark.41.a executed"
		DEN = { puppet = NOR }

		ai_chance = {
			factor = 5
			modifier = {
				factor = 100
				has_government = communism
			}
		}
	}
	option = { #No
		name = denmark.41.b
		log = "[GetDateText]: [This.GetName]: denmark.41.b executed"

		ai_chance = {
			factor = 95
			modifier = {
				factor = 0
				has_government = communism
			}
		}
	}
}

#puppet Finland
country_event = {
	id = denmark.42
	title = denmark.42.t
	desc = denmark.42.d
	picture = GFX_politics_communism
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = denmark.42.a
		log = "[GetDateText]: [This.GetName]: denmark.42.a executed"
		DEN = { puppet = FIN }

		ai_chance = {
			factor = 5
			modifier = {
				factor = 100
				has_government = communism
			}
		}
	}
	option = { #No
		name = denmark.42.b
		log = "[GetDateText]: [This.GetName]: denmark.42.b executed"

		ai_chance = {
			factor = 95
			modifier = {
				factor = 0
				has_government = communism
			}
		}
	}
}

##News Event
#Abdication of Queen Margrethe II
news_event = {
	id = denmark_news.1
	title = denmark_news.1.t
	desc = denmark_news.1.d
	picture = GFX_news_press_conference
	is_triggered_only = yes
	fire_only_once = yes
	major = yes
	trigger = {
		DEN = { has_completed_focus = DEN_abolish_monarchy }
	}

	option = {
		name = denmark_news.1.o1
		log = "[GetDateText]: [This.GetName]: denmark_news.1.o1 executed"
		trigger = {
			is_in_array = { ruling_party = 23 }
		}

		ai_chance = {
			base = 0.3
		}
	}
	option = {
		name = denmark_news.1.o2
		log = "[GetDateText]: [This.GetName]: denmark_news.1.o2 executed"
		trigger = {
			tag = DEN
		}

		ai_chance = {
			base = 0.3
		}
	}
	option = {
		name = denmark_news.1.o3
		log = "[GetDateText]: [This.GetName]: denmark_news.1.o3 executed"
		trigger = {
			NOT = { is_in_array = { ruling_party = 23 } }
			NOT = { tag = DEN }
		}

		ai_chance = {
			base = 0.3
		}
	}
}

#Eurovision 2000
country_event = {
	id = Denmark.1
	title = Denmark.1.t
	desc = Denmark.1.d
	picture = GFX_olsen
	fire_only_once = yes
	trigger = {
		original_tag = DEN
		date > 2000.5.13
		date < 2001.5.20
	}

	option = {
		name = denmark.1.a
		log = "[GetDateText]: [This.GetName]: denmark.1.a executed"
		add_political_power = 50

		ai_chance = {
			base = 1
		}
	}

}

#Øresundsbroen
country_event = {
	id = denmark.22
	title = denmark.22.t
	desc = denmark.22.d
	picture = GFX_Oresundsbron
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.22.a
		log = "[GetDateText]: [This.GetName]: denmark.22.a executed"
		add_political_power = 25
		add_opinion_modifier = { target = SWE modifier = small_increase }
		reverse_add_opinion_modifier = { target = SWE modifier = small_increase }

		ai_chance = {
			base = 1
		}
	}
}

#Thule Air Base
country_event = {
	id = denmark.23
	title = denmark.23.t
	desc = denmark.23.d
	picture = GFX_blizzard
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.23.a
		log = "[GetDateText]: [This.GetName]: denmark.23.a executed"
		add_opinion_modifier = { target = USA modifier = medium_increase }
		reverse_add_opinion_modifier = { target = USA modifier = medium_increase }
		1 = {
			add_building_construction = {
				type = air_base
				level = 1
				instant_build = yes
			}
		}

		ai_chance = {
			base = 1
		}
	}
}

#Island Of Hans
country_event = {
	id = denmark.24
	title = denmark.24.t
	desc = denmark.24.d
	picture = GFX_politics_demands
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.24.a
		log = "[GetDateText]: [This.GetName]: denmark.24.a executed"
		add_opinion_modifier = { target = CAN modifier = small_decrease }
		reverse_add_opinion_modifier = { target = CAN modifier = small_decrease }

		ai_chance = {
			base = 1
		}
	}
}

#Muhammed Cartoon
country_event = {
	id = denmark.25
	title = denmark.25.t
	desc = denmark.25.d
	picture = GFX_politics_speak
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.25.a
		log = "[GetDateText]: [This.GetName]: denmark.25.a executed"
		reverse_add_opinion_modifier = { target = SAU modifier = large_decrease }
		reverse_add_opinion_modifier = { target = KUW modifier = large_decrease }
		reverse_add_opinion_modifier = { target = UAE modifier = large_decrease }
		reverse_add_opinion_modifier = { target = OMA modifier = large_decrease }
		reverse_add_opinion_modifier = { target = YEM modifier = large_decrease }

		ai_chance = {
			base = 1
		}
	}
}

#Pulling out of Iraq
news_event = {
	id = denmark.26
	title = denmark.26.t
	desc = denmark.26.d
	picture = GFX_news_event_war
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.26.a
		log = "[GetDateText]: [This.GetName]: denmark.26.a executed"
		add_manpower = 470
		reverse_add_opinion_modifier = { target = USA modifier = small_decrease }
		reverse_add_opinion_modifier = { target = IRQ modifier = small_increase }

		ai_chance = {
			base = 1
		}
	}
}

#Plot to Kill Kurt Westergaard
country_event = {
	id = denmark.27
	title = denmark.27.t
	desc = denmark.27.d
	picture = GFX_politics_eye
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.27.a
		log = "[GetDateText]: [This.GetName]: denmark.27.a executed"
		add_stability = -0.01

		ai_chance = {
			base = 1
		}
	}
}

#Arctic Military Command
country_event = {
	id = denmark.28
	title = denmark.28.t
	desc = denmark.28.d
	picture = GFX_blizzard
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.28.a
		log = "[GetDateText]: [This.GetName]: denmark.28.a executed"
		army_experience = 10
		navy_experience = 15
		air_experience = 10
		1 = {
			add_building_construction = {
				type = air_base
				level = 1
				instant_build = yes
			}
		}
		1 = {
			add_building_construction = {
				type = naval_base
				level = 1
				province = 12512
				instant_build = yes
			}
		}

		ai_chance = {
			base = 1
		}
	}
}

#Copenhagen Summit
country_event = {
	id = denmark.29
	title = denmark.29.t
	desc = denmark.29.d
	picture = GFX_political_deal
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.29.a
		log = "[GetDateText]: [This.GetName]: denmark.29.a executed"
		add_stability = 0.02
		set_temp_variable = { treasury_change = 1 }
		modify_treasury_effect = yes

		ai_chance = {
			base = 1
		}
	}
}

#Axe Attack Kurt Westergaard
country_event = {
	id = denmark.30
	title = denmark.30.t
	desc = denmark.30.d
	picture = GFX_politics_surveillence
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.30.a
		log = "[GetDateText]: [This.GetName]: denmark.30.a executed"
		add_stability = -0.01

		ai_chance = {
			base = 1
		}
	}
}

#foiled attack on Jyllandsposten
country_event = {
	id = denmark.31
	title = denmark.31.t
	desc = denmark.31.d
	picture = GFX_politics_surveillence
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.31.a
		log = "[GetDateText]: [This.GetName]: denmark.31.a executed"
		add_stability = 0.01
		add_political_power = 25

		ai_chance = {
			base = 1
		}
	}
}

#Border Control
country_event = {
	id = denmark.32
	title = denmark.32.t
	desc = denmark.32.d
	picture = GFX_declined_offer_no
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.32.a
		log = "[GetDateText]: [This.GetName]: denmark.32.a executed"
		add_political_power = -25

		ai_chance = {
			base = 1
		}
	}
}

#Same-sex Marriage legalised
country_event = {
	id = denmark.33
	title = denmark.33.t
	desc = denmark.33.d
	picture = GFX_politics_talks
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.33.a
		log = "[GetDateText]: [This.GetName]: denmark.33.a executed"
		add_stability = 0.02
		add_political_power = 25

		ai_chance = {
			base = 1
		}
	}
}

#Union shutting schools down
country_event = {
	id = denmark.34
	title = denmark.34.t
	desc = denmark.34.d
	picture = GFX_politics_demands
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.34.a
		log = "[GetDateText]: [This.GetName]: denmark.34.a executed"
		add_stability = -0.01
		add_political_power = -10

		ai_chance = {
			base = 1
		}
	}
}

#Islamist Omar El-Hussein
country_event = {
	id = denmark.35
	title = denmark.35.t
	desc = denmark.35.d
	picture = GFX_politics_salafism
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.35.a
		log = "[GetDateText]: [This.GetName]: denmark.35.a executed"
		add_stability = -0.02

		ai_chance = {
			base = 1
		}
	}
}

#Asylum Seeker surrender cash or valuables
country_event = {
	id = denmark.36
	title = denmark.36.t
	desc = denmark.36.d
	picture = GFX_trade_agreement
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.36.a
		log = "[GetDateText]: [This.GetName]: denmark.36.a executed"
		add_stability = -0.03
		set_temp_variable = { treasury_change = 1 }
		modify_treasury_effect = yes

		ai_chance = {
			base = 1
		}
	}
}

#Ban Hate speech
country_event = {
	id = denmark.37
	title = denmark.37.t
	desc = denmark.37.d
	picture = GFX_declined_offer_no
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.37.a
		log = "[GetDateText]: [This.GetName]: denmark.37.a executed"
		add_stability = 0.02

		ai_chance = {
			base = 1
		}
	}
}

#Ancient laws
country_event = {
	id = denmark.38
	title = denmark.38.t
	desc = denmark.38.d
	picture = GFX_declined_offer_no
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.38.a
		log = "[GetDateText]: [This.GetName]: denmark.38.a executed"
		add_stability = 0.01

		ai_chance = {
			base = 1
		}
	}
}

#Storm Erwin (Gudrun)
country_event = {
	id = denmark.43
	title = denmark.43.t
	desc = denmark.43.d
	picture = GFX_typhoon
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.43.a
		log = "[GetDateText]: [This.GetName]: denmark.43.a executed"
		add_stability = -0.01

		ai_chance = {
			base = 1
		}
	}
}

#Greenland Referendum
country_event = {
	id = denmark.44
	title = denmark.44.t
	desc = denmark.44.d
	picture = GFX_court
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.44.a
		log = "[GetDateText]: [This.GetName]: denmark.44.a executed"
		release = GRL
		set_autonomy = {
			target = GRL
			autonomy_state = danish_crown_holding
			end_wars = no
			end_civil_wars = no
		}
		1 = {
			remove_dynamic_modifier = {
				modifier = DEN_greenlandic_separatism_modifier
			}
		}
		GRL = {
			set_variable = { DEN_colonial_holding_modifier_1 = 0.05 }
			set_variable = { DEN_colonial_holding_modifier_2 = 0.05 }
			set_variable = { DEN_colonial_holding_modifier_3 = -0.02 }
			set_variable = { DEN_colonial_holding_modifier_4 = 0.10 }
			set_variable = { DEN_colonial_holding_modifier_5 = -0.10 }
			set_variable = { DEN_colonial_holding_modifier_6 = -0.05 }
			set_variable = { DEN_colonial_weekly_cost = 0.200 }
			add_dynamic_modifier = {
				modifier = DEN_colonial_holding_modifier
			}

			## Setup Values

			hidden_effect = {
				set_variable = { civilian_factory_employment_var = 1 }
				set_variable = { naval_factory_employment_var = 1 }
				set_variable = { military_factory_employment_var = 1 }
				set_variable = { office_employment_var = 1 }
				set_variable = { gdpc_converging_var = PREV.gdpc_converging_var }
				inherit_technology = PREV
				if = {
					limit = {
						NOT = { has_variable = domestic_influence_amount }
					}
					set_variable = { domestic_influence_amount = 100 }
				}
				if = {
					limit = { NOT = { has_variable = corporate_tax_rate } }
					set_variable = { var = corporate_tax_rate value = 20 }
				}
				if = {
					limit = { NOT = { has_variable = population_tax_rate } }
					set_variable = { var = population_tax_rate value = 20 }
				}
				ingame_init_investment_system = yes
				init_auto_influence_array = yes

				calculate_influence_percentage = yes #set alignment drifts from influence
				startup_politics = yes
				ingame_update_setup = yes 	#money system
				setup_init_factions = yes 	#flags for int. factions
				set_law_vars = yes 	#vars for change laws secondary effects
				update_neighbors_effects = yes # Updates Neighbor Effects
				economic_cycle_drift_popularity = yes #drift from neighbor
				ai_update_build_units = yes
				calculate_expected_spending = yes
			}
		}
		DEN = {
			set_variable = { DEN_colonial_holding_modifier_1 = 0.10 }
			set_variable = { DEN_colonial_holding_modifier_2 = 0.05 }
			set_variable = { DEN_colonial_weekly_cost = -0.200 }
			add_dynamic_modifier = {
				modifier = DEN_colonial_holding_master_modifier
			}
		}

		ai_chance = {
			base = 1
		}
	}
}

# Farorese Referendum
country_event = {
	id = denmark.45
	title = denmark.45.t
	desc = denmark.45.d
	picture = GFX_court
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.45.a
		log = "[GetDateText]: [This.GetName]: denmark.45.a executed"
		release = FAO
		set_autonomy = {
			target = FAO
			autonomy_state = danish_crown_holding
			end_wars = no
			end_civil_wars = no
		}
		2 = {
			remove_dynamic_modifier = {
				modifier = DEN_greenlandic_separatism_modifier
			}
		}
		FAO = {
			set_variable = { DEN_colonial_holding_modifier_1 = 0.05 }
			set_variable = { DEN_colonial_holding_modifier_2 = 0.05 }
			set_variable = { DEN_colonial_holding_modifier_3 = -0.02 }
			set_variable = { DEN_colonial_holding_modifier_4 = 0.10 }
			set_variable = { DEN_colonial_holding_modifier_5 = 0.10 }
			set_variable = { DEN_colonial_holding_modifier_6 = 0.10 }
			add_dynamic_modifier = {
				modifier = DEN_colonial_holding_modifier
			}

			## Setup Values
			hidden_effect = {
				set_variable = { civilian_factory_employment_var = 1 }
				set_variable = { naval_factory_employment_var = 1 }
				set_variable = { military_factory_employment_var = 1 }
				set_variable = { office_employment_var = 1 }
				set_variable = { gdpc_converging_var = PREV.gdpc_converging_var }
				inherit_technology = PREV
				if = {
					limit = {
						NOT = { has_variable = domestic_influence_amount }
					}
					set_variable = { domestic_influence_amount = 100 }
				}
				if = {
					limit = { NOT = { has_variable = corporate_tax_rate } }
					set_variable = { var = corporate_tax_rate value = 20 }
				}
				if = {
					limit = { NOT = { has_variable = population_tax_rate } }
					set_variable = { var = population_tax_rate value = 20 }
				}
				ingame_init_investment_system = yes
				init_auto_influence_array = yes

				calculate_influence_percentage = yes #set alignment drifts from influence
				startup_politics = yes
				ingame_update_setup = yes 	#money system
				setup_init_factions = yes 	#flags for int. factions
				set_law_vars = yes 	#vars for change laws secondary effects
				update_neighbors_effects = yes # Updates Neighbor Effects
				economic_cycle_drift_popularity = yes #drift from neighbor
				ai_update_build_units = yes
				calculate_expected_spending = yes
			}
		}
		DEN = {
			set_variable = { DEN_colonial_holding_modifier_1 = 0.10 }
			set_variable = { DEN_colonial_holding_modifier_2 = 0.05 }
			set_variable = { DEN_colonial_weekly_cost = -0.200 }
		}

		ai_chance = {
			base = 1
		}
	}
}

#the Euroreferendum
country_event = {
	id = denmark.46
	title = denmark.46.t
	desc = denmark.46.d
	picture = GFX_typhoon
	fire_only_once = yes
	is_triggered_only = yes

	# Results
	option = {
		name = denmark.46.a
		log = "[GetDateText]: [This.GetName]: denmark.46.a executed"
		custom_effect_tooltip = DEN_Euro_referendum_TT
		hidden_effect = {
			if = {
				limit = {
					check_variable = {
							var = eurosceptic
							value = 0.5
							compare = greater_than_or_equals
					}
				}
				random_list = {
					40 = {
						country_event = { id = denmark.47 days = 1 }
					}
					60 = {
						country_event = { id = denmark.48 days = 1 }
					}
				}
			}
			else_if = {
				limit = {
					check_variable = {
							var = eurosceptic
							value = 0.4
							compare = greater_than_or_equals
					}
				}
				random_list = {
					50 = {
						country_event = { id = denmark.47 days = 1 }
					}
					50 = {
						country_event = { id = denmark.48 days = 1 }
					}
				}
			}
			else = {
				random_list = {
					60 = {
						country_event = { id = denmark.47 days = 1 }
					}
					40 = {
						country_event = { id = denmark.48 days = 1 }
					}
				}
			}
		}

		ai_chance = {
			base = 1
		}
	}
}

# Yes to Euro
country_event = {
	id = denmark.47
	title = denmark.47.t
	desc = denmark.47.d
	picture = GFX_typhoon
	fire_only_once = yes
	is_triggered_only = yes

	#accept resulst
	option = {
		name = denmark.47.a
		log = "[GetDateText]: [This.GetName]: denmark.47.a executed"
		set_country_flag = DEN_accept
		remove_ideas = DEN_euro_question_idea
		set_temp_variable = { modify_eurosceptic = -0.05 }
		set_temp_variable = { modify_eurosceptic_target = DEN }
		eurosceptic_change = yes

		ai_chance = {
			base = 0.5
		}
	}
	#don´t accept
	option = {
		name = denmark.47.b
		log = "[GetDateText]: [This.GetName]: denmark.47.b executed"
		add_political_power = -100
		add_stability = -0.02
		remove_ideas = DEN_euro_question_idea
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = -0.04 }
		add_relative_party_popularity = yes
		set_temp_variable = { modify_eurosceptic = 0.05 }
		set_temp_variable = { modify_eurosceptic_target = DEN }
		eurosceptic_change = yes

		ai_chance = {
			base = 0.5
		}
	}
}

# NO to Euro
country_event = {
	id = denmark.48
	title = denmark.48.t
	desc = denmark.48.d
	picture = GFX_typhoon
	fire_only_once = yes
	is_triggered_only = yes

	#accept resulst
	option = {
		name = denmark.48.a
		log = "[GetDateText]: [This.GetName]: denmark.48.a executed"
		add_political_power = -50
		add_stability = -0.01
		remove_ideas = DEN_euro_question_idea
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.02 }
		add_relative_party_popularity = yes

		ai_chance = {
			base = 0.5
		}
	}
	#don´t accept
	option = {
		name = denmark.48.b
		log = "[GetDateText]: [This.GetName]: denmark.48.b executed"
		add_political_power = -100
		add_stability = -0.02
		remove_ideas = DEN_euro_question_idea
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = -0.04 }
		add_relative_party_popularity = yes
		set_temp_variable = { modify_eurosceptic = -0.05 }
		set_temp_variable = { modify_eurosceptic_target = DEN }
		eurosceptic_change = yes
		set_country_flag = DEN_accept

		ai_chance = {
			base = 0.5
		}
	}
}

# Status of Ertholemene
country_event = {
	id = denmark.100
	title = denmark.100.t
	desc = denmark.100.d
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = denmark.100.a
		log = "[GetDateText]: [THIS.GetName]: denmark.100.a executed"
		add_stability = 0.04
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.02 }
		set_temp_variable = { temp_outlook_increase = 0.02 }
		add_relative_party_popularity = yes

		ai_chance = {
			base = 0.5
		}
	}

	option = {
		name = denmark.100.b
		log = "[GetDateText]: [THIS.GetName]: denmark.100.b executed"
		add_political_power = 100
		increase_centralization = yes

		ai_chance = {
			base = 0.5
		}
	}
}

#agrar
country_event = {
	id = denmark_agrar_event.10
	title = denmark_agrar_event.10.t
	desc = denmark_agrar_event.10.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.10.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.10.a executed"
		DEN_Agrar_resolve = yes
		add_popularity = {
			ideology = neutrality
			popularity = 0.01
		}
		recalculate_party = yes

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.11
	title = denmark_agrar_event.11.t
	desc = denmark_agrar_event.11.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.11.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.11.a executed"
		DEN_Agrar_resolve = yes
		add_popularity = {
			ideology = neutrality
			popularity = 0.02
		}
		recalculate_party = yes
		add_timed_idea = {
			idea = DEN_peaceful_protests
			days = 90
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.12
	title = denmark_agrar_event.12.t
	desc = denmark_agrar_event.12.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.12.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.12.a executed"
		DEN_Agrar_resolve = yes
		add_popularity = {
			ideology = neutrality
			popularity = 0.03
		}
		recalculate_party = yes
		add_timed_idea = {
			idea = DEN_protests
			days = 90
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.13
	title = denmark_agrar_event.13.t
	desc = denmark_agrar_event.13.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.13.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.13.a executed"
		DEN_Agrar_resolve = yes
		add_popularity = {
			ideology = nationalist
			popularity = 0.04
		}
		recalculate_party = yes
		add_timed_idea = {
			idea = DEN_harsh_protests
			days = 90
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.14
	title = denmark_agrar_event.14.t
	desc = denmark_agrar_event.14.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.14.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.14.a executed"
		DEN_Agrar_resolve = yes
		add_popularity = {
			ideology = neutrality
			popularity = 0.05
		}
		recalculate_party = yes
		3 = {
			damage_building = {
				type = infrastructure
				damage = 0.2
			}
			damage_building = {
				type = offices
				damage = 0.2
			}
			damage_building = {
				type = industrial_complex
				damage = 0.2
			}
		}
		add_timed_idea = {
			idea = DEN_harsh_protests
			days = 90
		}
		add_stability = -0.05
		add_manpower = -5

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.15
	title = denmark_agrar_event.15.t
	desc = denmark_agrar_event.15.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.15.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.15.a executed"
		DEN_Agrar_resolve_II = yes
		add_popularity = {
			ideology = nationalist
			popularity = 0.02
		}
		recalculate_party = yes

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.16
	title = denmark_agrar_event.16.t
	desc = denmark_agrar_event.16.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.16.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.16.a executed"
		DEN_Agrar_resolve_II = yes
		add_popularity = {
			ideology = nationalist
			popularity = 0.02
		}
		recalculate_party = yes
		add_timed_idea = {
			idea = DEN_peaceful_protests
			days = 90
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.17
	title = denmark_agrar_event.17.t
	desc = denmark_agrar_event.17.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.17.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.17.a executed"
		DEN_Agrar_resolve_II = yes
		add_popularity = {
			ideology = nationalist
			popularity = 0.03
		}
		recalculate_party = yes

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.18
	title = denmark_agrar_event.18.t
	desc = denmark_agrar_event.18.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.18.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.18.a executed"
		DEN_Agrar_resolve_II = yes
		add_popularity = {
			ideology = nationalist
			popularity = 0.04
		}
		recalculate_party = yes
		add_timed_idea = {
			idea = DEN_protests
			days = 90
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = denmark_agrar_event.19
	title = denmark_agrar_event.19.t
	desc = denmark_agrar_event.19.d
	picture = GFX_politics_protest
	is_triggered_only = yes

	option = {
		name = denmark_agrar_event.19.a
		log = "[GetDateText]: [This.GetName]: denmark_agrar_event.19.a executed"
		DEN_Agrar_resolve_II = yes
		add_popularity = {
			ideology = nationalist
			popularity = 0.05
		}
		recalculate_party = yes
		3 = {
			damage_building = {
				type = infrastructure
				damage = 0.2
			}
			damage_building = {
				type = offices
				damage = 0.2
			}
			damage_building = {
				type = industrial_complex
				damage = 0.2
			}
		}
		add_timed_idea = {
			idea = DEN_harsh_protests
			days = 90
		}
		add_manpower = -5
		add_stability = -0.05

		ai_chance = {
			base = 1
		}
	}
}
