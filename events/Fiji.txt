#Events Written by Luigi IV
add_namespace = fiji_news


###Events Related To The 2000 Coups And 2006 Coup###
country_event = {
	id = fiji_news.1
	title = fiji_news.1.t
	desc = fiji_news.1.d
	picture = GFX_politics_protest
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.1.o1 #19th of May
		log = "[GetDateText]: [This.GetName]: fiji_news.1.o1 executed"
		add_political_power = -10
		add_stability = -0.25
		add_war_support = -0.15
		set_politics = {
			ruling_party = democratic
			elections_allowed = no
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 3 } #Democratic Socialism
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		ai_chance = {
			base = 1
		}
		country_event = {
			id = fiji_news.2
			days = 5
		}
	}
}

country_event = {
	id = fiji_news.2
	title = fiji_news.2.t
	desc = fiji_news.2.d
	picture = GFX_politics_talks
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.2.o1 #21st of May
		log = "[GetDateText]: [This.GetName]: fiji_news.2.o1 executed"
		add_political_power = 25
		add_stability = -0.20
		add_war_support = -0.10
		country_event = {
			id = fiji_news.3
			days = 7
		}
		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.3
	title = fiji_news.3.t
	desc = fiji_news.3.d
	picture = GFX_politics_autocracy
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.3.o1 #28th of May
		log = "[GetDateText]: [This.GetName]: fiji_news.3.o1 executed"
		add_political_power = -10
		add_stability = -0.14
		add_war_support = -0.10
		set_politics = {
			ruling_party = nationalist
			elections_allowed = no
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 22 } #Military Junta
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		country_event = {
			id = fiji_news.4
			days = 10
		}

		ai_chance = {
			base = 100
		}
	}

	option = {
		name = fiji_news.3.o2
		log = "[GetDateText]: [This.GetName]: fiji_news.3.o2 executed"
		add_political_power = 50
		add_stability = -0.09
		add_war_support = -0.07
		set_politics = {
			ruling_party = nationalist
			elections_allowed = no
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 22 } #Military Junta
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		country_event = {
			id = fiji_news.400
			days = 10
		}

		ai_chance = {
			base = 1
			modifier = {
				has_global_flag = EARLY_COUP_ESCALATION
				factor = 1000
			}
		}
	}
}

country_event = {
	id = fiji_news.400
	title = fiji_news.400.t
	desc = fiji_news.400.d
	picture = GFX_internal_conflict
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.400.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.400.o1 executed"
		add_political_power = -100
		add_stability = -0.30
		add_war_support = -0.30
		country_event = {
			id = fiji_news.401
			days = 10
		}
		hidden_effect = {
			set_temp_variable = { party_index = 22 }
			set_temp_variable = { party_popularity_increase = 0.35 }
			set_temp_variable = { temp_outlook_increase = 0.25 }
			add_relative_party_popularity = yes
		}
		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.401
	title = fiji_news.401.t
	desc = fiji_news.401.d
	picture = GFX_pkk_ambush
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.401.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.401.o1 executed"
		add_stability = -0.30
		add_war_support = -0.30
		country_event = {
			id = fiji_news.402
			days = 10
		}
		hidden_effect = {
			set_temp_variable = { party_index = 22 }
			set_temp_variable = { party_popularity_increase = 0.35 }
			set_temp_variable = { temp_outlook_increase = 0.25 }
			add_relative_party_popularity = yes
		}
		ai_chance = {
			base = 1
			modifier = {
				has_global_flag = EARLY_COUP_ESCALATION
				factor = 1000
			}
		}
	}

	option = {
		name = fiji_news.401.o2
		log = "[GetDateText]: [This.GetName]: fiji_news.401.o2 executed"
		add_stability = -0.30
		add_war_support = -0.30
		country_event = {
			id = fiji_news.4
			days = 10
		}
		hidden_effect = {
			set_temp_variable = { party_index = 1 }
			set_temp_variable = { party_popularity_increase = 0.35 }
			set_temp_variable = { temp_outlook_increase = 0.25 }
			add_relative_party_popularity = yes
		}
		ai_chance = {
			base = 100
		}
	}

	option = {
		name = fiji_news.401.o3
		log = "[GetDateText]: [This.GetName]: fiji_news.401.o3 executed"
		add_stability = -0.30
		add_war_support = -0.30
		country_event = {
			id = fiji_news.500
			days = 10
		}
		hidden_effect = {
			set_temp_variable = { party_index = 23 }
			set_temp_variable = { party_popularity_increase = 0.35 }
			set_temp_variable = { temp_outlook_increase = 0.25 }
			add_relative_party_popularity = yes
		}
		ai_chance = {
			base = 1
			modifier = {
				has_global_flag = EARLY_COUP_ESCALATION
				factor = 1000
			}
		}
	}
}

country_event = {
	id = fiji_news.402
	title = fiji_news.402.t
	desc = fiji_news.402.d
	picture = GFX_terrorist_attack
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.402.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.402.o1 executed"
		add_stability = -0.50
		add_war_support = -0.50
		add_ideas = fiji_militarized_society
		set_country_flag = fiji_post_coup_political_shift
		hidden_effect = {
			country_event = {
				id = fiji_news.403
				days = 1825
			}
			set_temp_variable = { party_index = 22 }
			set_temp_variable = { party_popularity_increase = 0.20 }
			set_temp_variable = { temp_outlook_increase = 0.20 }
			add_relative_party_popularity = yes
		}
		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.403
	title = fiji_news.403.t
	desc = fiji_news.403.d
	picture = GFX_terrorist_attack
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.403.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.403.o1 executed"
		add_stability = 0.09
		add_war_support = 0.06
		set_politics = {
			ruling_party = nationalist
			elections_allowed = no
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 22 } #Military Junta
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_country_flag = fiji_fast_tracked_military_junta

		set_temp_variable = { party_index = 22 }
		set_temp_variable = { party_popularity_increase = 0.20 }
		set_temp_variable = { temp_outlook_increase = 0.20 }
		add_relative_party_popularity = yes

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.500
	title = fiji_news.500.t
	desc = fiji_news.500.d
	picture = GFX_jury
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.500.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.500.o1 executed"
		set_country_flag = fiji_post_coup_political_shift
		set_cosmetic_tag = FIJ_Monarchy
			set_politics = {
				ruling_party = nationalist
				elections_allowed = no
			}
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 23 } #Monarchist
			recalculate_party = yes
			set_ruling_leader = yes
			set_leader = yes
			set_variable = { current_term = 1 }
			set_temp_variable = { party_index = 23 }
			set_temp_variable = { party_popularity_increase = 0.50 }
			set_temp_variable = { temp_outlook_increase = 0.45 }
			add_relative_party_popularity = yes
			hidden_effect = {
				country_event = {
					id = fiji_news.501
					days = 5
				}
			}
		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.501
	title = fiji_news.501.t
	desc = fiji_news.501.d
	picture = GFX_gen_construction
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.501.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.501.o1 executed"
		add_stability = 0.07
		add_war_support = 0.07
		add_ideas = fiji_society_of_the_elected_king
		hidden_effect = {
			country_event = {
				id = fiji_news.502
				days = 1825
			}
		}
		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.502
	title = fiji_news.502.t
	desc = fiji_news.502.d
	picture = GFX_gen_construction
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.502.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.502.o1 executed"
		add_stability = 0.13
		add_war_support = 0.09
		set_country_flag = fiji_fast_tracked_monarchy
			set_politics = {
				ruling_party = nationalist
				elections_allowed = no
			}
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 23 } #Monarchist
			recalculate_party = yes
			set_ruling_leader = yes
			set_leader = yes
			set_variable = { current_term = 1 }
			set_temp_variable = { party_index = 23 }
			set_temp_variable = { party_popularity_increase = 0.50 }
			set_temp_variable = { temp_outlook_increase = 0.45 }
			add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.4
	title = fiji_news.4.t
	desc = fiji_news.4.d
	picture = GFX_grozny_battle
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.4.o1 #12th of June
		log = "[GetDateText]: [This.GetName]: fiji_news.4.o1 executed"
		add_political_power = -50
		add_stability = -0.13
		add_war_support = -0.10
		country_event = {
			id = fiji_news.5
			days = 15
		}
		hidden_effect = {
			set_temp_variable = { party_index = 1 }
			set_temp_variable = { party_popularity_increase = 0.35 }
			add_relative_party_popularity = yes
		}
		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.5
	title = fiji_news.5.t
	desc = fiji_news.5.d
	picture = GFX_politics_talks
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.5.o1 #3rd of July
		log = "[GetDateText]: [This.GetName]: fiji_news.5.o1 executed"
		add_political_power = 10
		add_stability = 0.02
		add_war_support = 0.02
		country_event = {
			id = fiji_news.6
			days = 20
		}
		hidden_effect = {
			set_politics = {
				ruling_party = democratic
				elections_allowed = no
			}
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 1 } #conservative
			recalculate_party = yes
			set_ruling_leader = yes
			set_leader = yes
			set_variable = { current_term = 1 }
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.6
	title = fiji_news.6.t
	desc = fiji_news.6.d
	picture = GFX_politics_talks
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.6.o1 #26th of July
		log = "[GetDateText]: [This.GetName]: fiji_news.6.o1 executed"
		add_political_power = 15
		add_stability = 0.03
		add_war_support = 0.02
		set_country_flag = fiji_2000_coups_over

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.7
	title = fiji_news.7.t
	desc = fiji_news.7.d
	picture = GFX_politics_protest
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.7.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.7.o1 executed"
		add_political_power = 10
		add_stability = 0.04
		add_war_support = 0.01

		set_temp_variable = { party_index = 1 }
		set_temp_variable = { party_popularity_increase = 0.35 }
		set_temp_variable = { temp_outlook_increase = 0.25 }
		add_relative_party_popularity = yes

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.8
	title = fiji_news.8.t
	desc = fiji_news.8.d
	picture = GFX_internal_faction_political_conflict
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
		NOT = { has_country_flag = fiji_post_coup_political_shift }
	}

	option = {
		name = fiji_news.8.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.8.o1 executed"
		add_political_power = -50
		add_stability = -0.04
		add_war_support = -0.04

		country_event = {
			id = fiji_news.9
			days = 7
			random_days = 7
		}

		ai_chance = {
			base = 100
		}
	}
	option = {
		name = fiji_news.8.o2
		log = "[GetDateText]: [This.GetName]: fiji_news.8.o2 executed"
		add_political_power = -50
		add_stability = -0.04
		add_war_support = -0.04

		hidden_effect = {
			set_country_flag = fiji_2006_coup_avoided
		}

		ai_chance = {
			base = 1
			modifier = {
				has_global_flag = BREAKING_HISTORY
				factor = 100
			}
		}
	}
}

country_event = {
	id = fiji_news.9
	title = fiji_news.9.t
	desc = fiji_news.9.d
	picture = GFX_internal_faction_political_conflict
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.9.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.9.o1 executed"
		add_political_power = -75
		add_stability = -0.07
		add_war_support = -0.07

		country_event = {
			id = fiji_news.10
			days = 7
			random_days = 7
		}

		ai_chance = {
			base = 1
			modifier = {
				has_global_flag = BREAKING_HISTORY
				factor = 1000
			}
		}
	}
	option = {
		name = fiji_news.9.o2
		log = "[GetDateText]: [This.GetName]: fiji_news.9.o2 executed"
		add_political_power = -50
		add_stability = 0.02
		add_war_support = -0.06

		hidden_effect = {
			set_country_flag = fiji_2006_coup_historical
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.10
	title = fiji_news.10.t
	desc = fiji_news.10.d
	picture = GFX_politics_autocracy
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.10.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.10.o1 executed"
		add_political_power = -75
		add_stability = -0.07
		add_war_support = -0.07

		hidden_effect = {
			set_country_flag = fiji_2006_coup_military_junta
		}

		ai_chance = {
			base = 1
		}
	}
	option = {
		name = fiji_news.10.o2
		log = "[GetDateText]: [This.GetName]: fiji_news.10.o2 executed"
		add_political_power = -50
		add_stability = -0.09
		add_war_support = -0.09
		country_event = {
			id = fiji_news.11
			days = 7
			random_days = 7
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.11
	title = fiji_news.11.t
	desc = fiji_news.11.d
	picture = GFX_political_deal
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.11.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.11.o1 executed"
		add_political_power = -75
		add_stability = -0.07
		add_war_support = -0.07
		country_event = {
			id = fiji_news.12
			days = 7
			random_days = 7
		}

		ai_chance = {
			base = 1
		}
	}
	option = {
		name = fiji_news.11.o2
		log = "[GetDateText]: [This.GetName]: fiji_news.11.o2 executed"
		add_stability = 0.04
		add_war_support = 0.02
		hidden_effect = {
			set_country_flag = fiji_2006_coup_monarchist_era
		}

		ai_chance = {
			base = 1
		}
	}
}

country_event = {
	id = fiji_news.12
	title = fiji_news.12.t
	desc = fiji_news.12.d
	picture = GFX_political_activist
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		tag = FIJ
	}

	option = {
		name = fiji_news.12.o1
		log = "[GetDateText]: [This.GetName]: fiji_news.12.o1 executed"
		add_political_power = 25
		add_stability = 0.04
		add_war_support = 0.04
		set_country_flag = fiji_2006_coup_technocracy

		ai_chance = {
			base = 1
		}
	}
	option = {
		name = fiji_news.12.o2
		log = "[GetDateText]: [This.GetName]: fiji_news.12.o2 executed"
		add_political_power = 300
		add_stability = -0.15
		add_war_support = -0.15
		hidden_effect = {
			set_country_flag = fiji_2006_coup_avoided
		}

		ai_chance = {
			base = 1
		}
	}
}
