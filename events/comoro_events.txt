add_namespace = comor

country_event = {
	id = comor.1
	title = comor.1.t
	desc = comor.1.d
	picture = GFX_arab_spring_defection

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.1.a
		trigger = {
				tag = COM
			}
		log = "[GetDateText]: [This.GetName]: comor.1.a executed"
		add_equipment_to_stockpile = {
			type = infantry_weapons1
			amount = 4000
			producer = SOV
		}
		add_ideas = generic_emerging_boost_idea2
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = SYR }
		set_temp_variable = { influence_target = COM }
		change_influence_percentage = yes
		add_ideas = shia_movement
		hidden_effect = {
			create_country_leader = {
				name = "Ahmed Abdallah Mohamed Sambi"
				picture = "sambi_abdallah.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
				}
			}
		}
	}

	option = {
		name = comor.1.b
		trigger = {
				tag = COM
			}
		log = "[GetDateText]: [This.GetName]: comor.1.b executed"
		add_equipment_to_stockpile = {
			type = infantry_weapons1
			amount = 4000
			producer = IRQ
		}
		add_ideas = generic_nationalist_boost_idea2
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = IRQ }
		set_temp_variable = { influence_target = COM }
		change_influence_percentage = yes
		add_ideas = baath_movement
		hidden_effect = {
			create_country_leader = {
				name = "Said Ahmed Said Ali"
				picture = "said_ahmed.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}
		}
	}

}

country_event = {
	id = comor.2
	title = comor.2.t
	desc = comor.2.d
	picture = GFX_african_union

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.2.a
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = COM value < 30 }
				add = -60
			}
		}
		COM = {
			puppet = TNZ
		}
		log = "[GetDateText]: [This.GetName]: comor.2.a executed"
	}

	option = {
		name = comor.2.b
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = COM value > 30 }
				add = 10
			}
		}
		COM = {
			country_event = comor.3
		}
		log = "[GetDateText]: [This.GetName]: comor.1.b executed"
	}

	option = {
		name = comor.2.с
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = COM value > 30 }
				add = 10
			}
		}
		COM = {
			create_faction = COM_TNZ_ALL
			COM = { add_to_faction = TNZ }
		}
		log = "[GetDateText]: [This.GetName]: comor.1.c executed"
	}

}

country_event = {
	id = comor.3
	title = comor.3.t
	desc = comor.3.d
	picture = GFX_african_union

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.3.a
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = TNZ value < 30 }
				add = -60
			}
		}
		TNZ = {
			puppet = COM
		}
		log = "[GetDateText]: [This.GetName]: comor.3.a executed"
	}

	option = {
		name = comor.3.b
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = TNZ value > 30 }
				add = 10
			}
		}
		TNZ = {
			country_event = comor.2
		}
		log = "[GetDateText]: [This.GetName]: comor.3.b executed"
	}
}

country_event = {
	id = comor.4
	title = comor.4.t
	desc = comor.4.d
	picture = GFX_ivory_coast_protest_supressed

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.4.a
		trigger = {
				tag = COM
			}
			set_country_flag = COM_pro_fed
			log = "[GetDateText]: [This.GetName]: comor.4.a executed"
	}

	option = {
		name = comor.4.b
		trigger = { tag = COM }
		set_country_flag = COM_anti_fed
		log = "[GetDateText]: [This.GetName]: comor.4.b executed"
	}

}

country_event = {
	id = comor.5
	title = comor.5.t
	desc = comor.5.d
	picture = GFX_ivory_coast_protest_supressed

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.5.a
		trigger = { tag = FRA }
		log = "[GetDateText]: [This.GetName]: comor.5.a executed"
	}

	option = {
		name = comor.5.b
		trigger = { tag = MAD }
		log = "[GetDateText]: [This.GetName]: comor.5.b executed"
	}
}

country_event = {
	id = comor.6
	title = comor.6.t
	desc = comor.6.d
	picture = GFX_ETH_ELF

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.6.a
		log = "[GetDateText]: [This.GetName]: comor.6.a executed"
		trigger = {
			tag = FRA
		}
		add_opinion_modifier = { target = COM modifier = drama }
		COM = {
			add_war_support = 0.10
		}
	}
}

country_event = {
	id = comor.7
	title = comor.7.t
	desc = comor.7.d
	picture = GFX_handshake_black

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.7.a
		log = "[GetDateText]: [This.GetName]: comor.7.a executed"
		trigger = {
			OR = {
				tag = FRA
				tag = CHI
			}
		}
	}
}

country_event = {
	id = comor.8
	title = comor.8.t
	desc = comor.8.d
	picture = GFX_weapons_arms1

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.8.a
		log = "[GetDateText]: [This.GetName]: comor.8.a executed"
		trigger = {
			tag = COM
		}
		SEN = {
			country_event = comor.9
		}
		SUD = {
			country_event = comor.9
		}
		TNZ = {
			country_event = comor.9
		}
		FRA = {
			country_event = comor.9
		}
		USA = {
			country_event = comor.9
		}
		LBA = {
			country_event = comor.9
		}
		country_event = { id = comor.10 days = 20 }
	}
}

country_event = {
	id = comor.9
	title = comor.9.t
	desc = comor.9.d
	picture = GFX_puntland_police_force_ev

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.9.a
		log = "[GetDateText]: [This.GetName]: comor.9.a executed"
		COM = {
			add_manpower = 100
			add_equipment_to_stockpile = {
				type = infantry_weapons1
				amount = 1000
				producer = USA
			}
			set_country_flag = COM_got_support
		}
	}

	option = {
		name = comor.9.b
		log = "[GetDateText]: [This.GetName]: comor.9.b executed"
		add_opinion_modifier = { modifier = drama target = COM }
		reverse_add_opinion_modifier = { modifier = drama target = COM }
	}
}

country_event = {
	id = comor.10
	title = comor.10.t
	desc = comor.10.d
	picture = GFX_puntland_police_force_ev

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.10.a
		log = "[GetDateText]: [This.GetName]: comor.10.a executed"
		set_country_flag = COM_succ_oper
		add_stability = 0.05
	}
}

country_event = {
	id = comor.11
	title = comor.11.t
	desc = comor.11.d
	picture = GFX_red_guards

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.11.a
		log = "[GetDateText]: [This.GetName]: comor.11.a executed"
		FRA = {
			remove_state_core = 940
		}
	}
}

country_event = {
	id = comor.12
	title = comor.12.t
	desc = comor.12.d
	picture = GFX_red_guards

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.12.a
		log = "[GetDateText]: [This.GetName]: comor.12.a executed"
		940 = { transfer_state_to = COM }
	}

	option = {
		name = comor.12.b
		log = "[GetDateText]: [This.GetName]: comor.12.b executed"
		COM = {
			create_wargoal = {
				type = puppet_wargoal_focus
				target = FRA
			}
		}
	}
}

country_event = {
	id = comor.13
	title = comor.13.t
	desc = comor.13.d
	picture = GFX_african_union

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.13.a
		ai_chance = {
			base = -50
			modifier = {
				has_opinion = { target = COM value < 30 }
				add = -60
			}
		}
		COM = {
			puppet = FRA
		}
		log = "[GetDateText]: [This.GetName]: comor.13.a executed"
	}

	option = {
		name = comor.13.b
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = COM value > 30 }
				add = 10
			}
		}
		COM = {
			country_event = comor.14
		}
		log = "[GetDateText]: [This.GetName]: comor.13.b executed"
	}

	option = {
		name = comor.13.c
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = COM value > 30 }
				add = 10
			}
		}
		FRA = { add_to_faction = COM }
		log = "[GetDateText]: [This.GetName]: comor.13.c executed"
	}

}

country_event = {
	id = comor.14
	title = comor.14.t
	desc = comor.14.d
	picture = GFX_african_union

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.14.a
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = FRA value < 30 }
				add = -60
			}
		}
		FRA = {
			puppet = COM
		}
		log = "[GetDateText]: [This.GetName]: comor.14.a executed"
	}

	option = {
		name = comor.14.b
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = FRA value > 30 }
				add = 10
			}
		}
		TNZ = {
			country_event = comor.13
		}
		log = "[GetDateText]: [This.GetName]: comor.14.b executed"
	}

}

country_event = {
	id = comor.15
	title = comor.15.t
	desc = comor.15.d
	picture = GFX_burkina_coast_protests

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.15.a
		log = "[GetDateText]: [This.GetName]: comor.15.a executed"
	}
}

country_event = {
	id = comor.16
	title = comor.16.t
	desc = comor.16.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_CAN_cptpp_trade_agreement
	option = {
		name = comor.16.a
		log = "[GetDateText]: [This.GetName]: comor.16.a executed"
		set_temp_variable = { int_investment_change = 12 }
		modify_international_investment_effect = yes
		set_temp_variable = { percent_change = 1 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = COM }
		change_influence_percentage = yes
		COM = {
			add_offsite_building = { type = industrial_complex level = 1 }
			country_event = { id = comor.17 days = 1 }
		}

		ai_chance = { factor = 1 }
	}
	option = {
		name = comor.16.b
		log = "[GetDateText]: [This.GetName]: comor.16.b executed"
		set_temp_variable = { percent_change = 3 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = COM }
		change_influence_percentage = yes
		set_temp_variable = { int_investment_change = 3 }
		modify_international_investment_effect = yes
		COM = {
			set_temp_variable = { debt_change = -10 }
			modify_debt_effect = yes
			country_event = { id = comor.18 days = 1 }
		}

		ai_chance = { factor = 1 }
	}
	option = {
		name = comor.16.c
		log = "[GetDateText]: [This.GetName]: comor.16.c executed"
		COM = {
			country_event = { id = comor.19 days = 1 }
		}
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = comor.17
	title = comor.17.t
	desc = comor.17.d
	picture = GFX_CAN_cptpp_trade_agreement

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.17.a
		ai_chance = {
			factor = 1
		}
		log = "[GetDateText]: [This.GetName]: comor.17.a executed"
	}
}

country_event = {
	id = comor.18
	title = comor.18.t
	desc = comor.18.d
	picture = GFX_CAN_cptpp_trade_agreement

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.18.a
		ai_chance = {
			factor = 1
		}
		log = "[GetDateText]: [This.GetName]: comor.18.a executed"
	}
}

country_event = {
	id = comor.19
	title = comor.19.t
	desc = comor.19.d
	picture = GFX_CAN_cptpp_trade_agreement

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = comor.19.a
		ai_chance = {
			factor = 1
		}
		log = "[GetDateText]: [This.GetName]: comor.19.a executed"
	}
}
