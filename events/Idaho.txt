#Events Written by Luigi IV
add_namespace = idaho_event



##### Idaho's TAG is IDH
country_event = {
	id = idaho_event.1
	title = idaho_event.1.t
	desc = idaho_event.1.d
	picture = GFX_politics_demands
	is_triggered_only = yes
	trigger = {
		original_tag = IDH
	}

	option = {
		name = idaho_event.1.o1
		log = "[GetDateText]: [This.GetName]: idaho_event.1.o1 executed"
		add_manpower = -30000
		add_ideas = IDH_ideas_mountain_men_spirit
		808 = { 
			add_extra_state_shared_building_slots = 3 
			add_building_construction = {
				type = arms_factory
				level = 3
			}
		}
		if = {
			limit = { has_template = "Boise Brawlers" }
			capital_scope = {
				create_unit = {
					division = "name = \"Boise Brawlers\" division_template = \"Boise Brawlers\" start_experience_factor = 0.3"
					owner = ROOT
					count = 10
				}
			}
		}
		else = {
			division_template = {
				name = "Boise Brawlers"
				support = {
					L_Engi_Comp = { x = 0 y = 0 }
					Mot_Recce_Comp = { x = 0 y = 1 }
					combat_service_support_company = { x = 0 y = 2 }
				}
				regiments = {
					L_Inf_Bat = { x = 0 y = 0 }
					L_Inf_Bat = { x = 1 y = 0 }
					L_Inf_Bat = { x = 2 y = 0 }
				}
				priority = 0
			}
			capital_scope = {
				create_unit = {
					division = "name = \"Boise Brawlers\" division_template = \"Boise Brawlers\" start_experience_factor = 0.3"
					owner = ROOT
					count = 10
				}
			}
		}
		ai_chance = { base = 30 }
	}
}