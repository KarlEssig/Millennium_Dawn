﻿add_namespace = UAR
add_namespace = UAR_news

#Country invited to join UAR
country_event = {
	id = UAR.1
	title = UAR.1.t
	desc = UAR.1.d
	picture = GFX_united_arab_republic

	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR.1.a executed"
		name = UAR.1.a
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.25
				has_government = democratic
			}
			modifier = {
				factor = 0.1
				has_government = fascism
			}
			modifier = {
				factor = 0.75
				has_government = neutrality
			}
			modifier = {
				factor = 0.5
				has_government = nationalist
				FROM = { has_government = communism }
			}
			modifier = {
				factor = 1.75
				has_government = nationalist
				FROM = { has_government = nationalist }
			}
			modifier = {
				factor = 0.5
				has_government = communism
				FROM = { has_government = nationalist }
			}
			modifier = {
				factor = 1.75
				has_government = communism
				FROM = { has_government = communism }
			}
			modifier = {
				factor = 1.3
				OR = {
					AND = {
						check_variable = { influence_array^0 = FROM }
						check_variable = { influence_array_val^0 > 29.999 }
					}
					AND = {
						check_variable = { influence_array^1 = FROM }
						check_variable = { influence_array_val^1 > 29.999 }
					}
					AND = {
						check_variable = { influence_array^2 = FROM }
						check_variable = { influence_array_val^2 > 29.999 }
					}
				}
			}
			modifier = {
				factor = 1.5
				OR = {
					AND = {
						check_variable = { influence_array^0 = FROM }
						check_variable = { influence_array_val^0 > 49.999 }
					}
					AND = {
						check_variable = { influence_array^1 = FROM }
						check_variable = { influence_array_val^1 > 49.999 }
					}
					AND = {
						check_variable = { influence_array^2 = FROM }
						check_variable = { influence_array_val^2 > 49.999 }
					}
				}
			}
			modifier = {
				factor = 1.7
				OR = {
					AND = {
						check_variable = { influence_array^0 = FROM }
						check_variable = { influence_array_val^0 > 69.999 }
					}
					AND = {
						check_variable = { influence_array^1 = FROM }
						check_variable = { influence_array_val^1 > 69.999 }
					}
					AND = {
						check_variable = { influence_array^2 = FROM }
						check_variable = { influence_array_val^2 > 69.999 }
					}
				}
			}
			modifier = {
				factor = 0.25
				has_opinion = {
					target = FROM
					value < -50
				}
			}
			modifier = {
				factor = 0.5
				has_opinion = {
					target = FROM
					value < 0
				}
			}
			modifier = {
				factor = 1.5
				has_opinion = {
					target = FROM
					value > 0
				}
			}
			modifier = {
				factor = 1.75
				has_opinion = {
					target = FROM
					value > 50
				}
			}
		}
		drop_cosmetic_tag = yes
		effect_tooltip = {
			if = {
				limit = {
					OR = {
						has_country_flag = united_neo_baathist_uar
						has_country_flag = united_baathist_uar
					}
				}
				FROM = {
					set_autonomy = {
						target = ROOT
						autonomy_state = autonomy_uar_regional_command
					}
				}
			}
			else = {
				FROM = {
					set_autonomy = {
						target = ROOT
						autonomy_state = autonomy_uar_state
					}
				}
			}
		}
		FROM = { country_event = { id = UAR.2 hours = 6 } }
		hidden_effect = { news_event = { id = UAR_news.2 hours = 12 } }
	}
	option = {
		log = "[GetDateText]: [This.GetName]: UAR.1.b executed"
		name = UAR.1.b
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.75
				has_government = democratic
			}
			modifier = {
				factor = 1.9
				has_government = fascism
			}
			modifier = {
				factor = 1.25
				has_government = neutrality
			}
			modifier = {
				factor = 1.5
				has_government = nationalist
				FROM = { has_government = communism }
			}
			modifier = {
				factor = 0.25
				has_government = nationalist
				FROM = { has_government = nationalist }
			}
			modifier = {
				factor = 1.5
				has_government = communism
				FROM = { has_government = nationalist }
			}
			modifier = {
				factor = 0.25
				has_government = communism
				FROM = { has_government = communism }
			}
			modifier = {
				factor = 1.75
				has_opinion = {
					target = FROM
					value < -50
				}
			}
			modifier = {
				factor = 1.5
				has_opinion = {
					target = FROM
					value < 0
				}
			}
			modifier = {
				factor = 0.5
				has_opinion = {
					target = FROM
					value > 0
				}
			}
			modifier = {
				factor = 0.25
				has_opinion = {
					target = FROM
					value > 50
				}
			}
		}
		custom_effect_tooltip = UAR_invitation_declined_tt
		FROM = { country_event = { id = UAR.3 hours = 6 } }
	}
}

#Country accepts invitation to join UAR
country_event = {
	id = UAR.2
	title = UAR.2.t
	desc = UAR.2.d
	picture = GFX_united_arab_republic

	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR.2.a executed"
		name = UAR.2.a
		ai_chance = { factor = 100 }
		if = {
			limit = {
				OR = {
					has_country_flag = united_neo_baathist_uar
					has_country_flag = united_baathist_uar
				}
			}
			set_autonomy = {
				target = FROM
				autonomy_state = autonomy_uar_regional_command
			}
		}
		else = {
			set_autonomy = {
				target = FROM
				autonomy_state = autonomy_uar_state
			}
		}
		#Calculate the total GDP of all Arabic countries
		set_variable = { total_arabic_gdp = 0 }
		for_each_scope_loop = {
			array = global.arabic_countries
			add_to_variable = { ROOT.total_arabic_gdp = THIS.gdp_total }
		}
		#Calculate the GDP of UAR and their puppets
		set_variable = { total_uar_gdp = gdp_total }
		for_each_scope_loop = {
			array = ROOT.subjects
			add_to_variable = { ROOT.total_uar_gdp = THIS.gdp_total }
		}
	}
}

#Country rejecets invitation to join UAR
country_event = {
	id = UAR.3
	title = UAR.3.t
	desc = UAR.3.d
	picture = GFX_united_arab_republic

	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR.3.a executed"
		name = UAR.3.a
		ai_chance = {
			factor = 75
			modifier = {
				factor = 0.25
				has_government = nationalist
			}
		}
	}
	option = {
		log = "[GetDateText]: [This.GetName]: UAR.3.b executed"
		name = UAR.3.b
		ai_chance = {
			factor = 25
			modifier = {
				factor = 1.75
				has_government = nationalist
			}
			modifier = {
				factor = 0
				has_war = yes
			}
			modifier = {
				factor = 0.25
				check_variable = { debt_ratio > 0.7 }
			}
		}
		create_wargoal = {
			target = FROM
			type = puppet_wargoal_focus
		}
		add_stability = -0.05
		add_war_support = -0.05
	}
}

#Arabs are united, try to resist
country_event = {
	id = UAR.4
	title = UAR.4.t
	desc = UAR.4.d
	picture = GFX_united_arab_republic

	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR.4.a executed"
		name = UAR.4.a
		ai_chance = {
			factor = 80
			modifier = {
				factor = 0
				any_country = {
					OR = {
						has_country_flag = formed_neo_baathist_uar
						has_country_flag = united_neo_baathist_uar
					}
				}
				emerging_autocracy_are_in_power = yes
			}
			modifier = {
				factor = 0
				any_country = {
					OR = {
						has_country_flag = formed_baathist_uar
						has_country_flag = united_baathist_uar
					}
				}
				nationalist_fascist_are_in_power = yes
			}
			modifier = {
				factor = 0.2
				has_war = yes
			}
			modifier = {
				factor = 0.2
				check_variable = { debt_ratio > 0.8 }
			}
		}
		if = {
			limit = {
				any_country = {
					has_country_flag = UAR_created_arab_liberation_front
				}
			}
			random_country = {
				limit = { has_country_flag = UAR_created_arab_liberation_front }
				add_to_faction = ROOT
				ROOT = {
					add_ai_strategy = {
						type = alliance
						id = PREV
						value = 100
					}
				}
			}
		}
		else = {
			create_faction = UAR_arab_liberation_front
			set_country_flag = UAR_created_arab_liberation_front
			every_other_country = {
				limit = {
					NOT = { has_country_flag = united_neo_baathist_uar }
					NOT = { has_country_flag = united_baathist_uar }
					is_arabic_nation = yes
					OR = {
						has_idea = minor_power
						has_idea = non_power
					}
					is_subject = no
					has_war = no
					is_in_faction = no
				}
				country_event = { id = UAR.5 hours = 6 }
			}
			country_event = { id = UAR.6 days = 7 }
			hidden_effect = { news_event = { id = UAR_news.6 hours = 6 } }
		}
	}
	option = {
		log = "[GetDateText]: [This.GetName]: UAR.4.b executed"
		name = UAR.4.b
		ai_chance = {
			factor = 20
			modifier = {
				factor = 0
				any_country = {
					OR = {
						has_country_flag = formed_neo_baathist_uar
						has_country_flag = united_neo_baathist_uar
					}
				}
				emerging_autocracy_are_in_power = yes
			}
			modifier = {
				factor = 0
				any_country = {
					OR = {
						has_country_flag = formed_baathist_uar
						has_country_flag = united_baathist_uar
					}
				}
				nationalist_fascist_are_in_power = yes
			}
			modifier = {
				factor = 1.8
				has_war = yes
			}
			modifier = {
				factor = 1.8
				check_variable = { debt_ratio > 0.8 }
			}
		}
	}
}

#Call to join the Arab Liberation Front
country_event = {
	id = UAR.5
	title = UAR.5.t
	desc = UAR.5.d
	picture = GFX_united_arab_republic

	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR.5.a executed"
		name = UAR.5.a
		ai_chance = {
			factor = 60
			modifier = {
				factor = 0
				any_country = {
					OR = {
						has_country_flag = formed_neo_baathist_uar
						has_country_flag = united_neo_baathist_uar
					}
				}
				emerging_autocracy_are_in_power = yes
			}
			modifier = {
				factor = 0
				any_country = {
					OR = {
						has_country_flag = formed_baathist_uar
						has_country_flag = united_baathist_uar
					}
				}
				nationalist_fascist_are_in_power = yes
			}
			modifier = {
				factor = 0.2
				has_war = yes
			}
			modifier = {
				factor = 0.2
				check_variable = { debt_ratio > 0.8 }
			}
			modifier = {
				factor = 1.5
				has_government = democratic
			}
			modifier = {
				factor = 1.25
				has_government = neutrality
			}
			modifier = {
				factor = 1.25
				has_government = fascism
			}
			modifier = {
				factor = 0.5
				any_country = {
					OR = {
						has_country_flag = formed_neo_baathist_uar
						has_country_flag = united_neo_baathist_uar
					}
				}
				has_government = communism
			}
			modifier = {
				factor = 1.5
				any_country = {
					OR = {
						has_country_flag = formed_baathist_uar
						has_country_flag = united_baathist_uar
					}
				}
				has_government = communism
			}
			modifier = {
				factor = 0.5
				any_country = {
					OR = {
						has_country_flag = formed_baathist_uar
						has_country_flag = united_baathist_uar
					}
				}
				has_government = nationalist
			}
			modifier = {
				factor = 1.5
				any_country = {
					OR = {
						has_country_flag = formed_neo_baathist_uar
						has_country_flag = united_neo_baathist_uar
					}
				}
				has_government = nationalist
			}
		}
		FROM = {
			add_to_faction = ROOT
		}
		set_country_flag = UAR_joined_arab_liberation_front
	}
	option = {
		log = "[GetDateText]: [This.GetName]: UAR.5.b executed"
		name = UAR.5.b
		ai_chance = {
			factor = 40
			modifier = {
				factor = 1.8
				has_war = yes
			}
			modifier = {
				factor = 1.8
				check_variable = { debt_ratio > 0.8 }
			}
			modifier = {
				factor = 0.5
				has_government = democratic
			}
			modifier = {
				factor = 0.75
				has_government = neutrality
			}
			modifier = {
				factor = 0.75
				has_government = fascism
			}
			modifier = {
				factor = 1.5
				any_country = {
					OR = {
						has_country_flag = formed_neo_baathist_uar
						has_country_flag = united_neo_baathist_uar
					}
				}
				has_government = communism
			}
			modifier = {
				factor = 0.5
				any_country = {
					OR = {
						has_country_flag = formed_baathist_uar
						has_country_flag = united_baathist_uar
					}
				}
				has_government = communism
			}
			modifier = {
				factor = 1.5
				any_country = {
					OR = {
						has_country_flag = formed_baathist_uar
						has_country_flag = united_baathist_uar
					}
				}
				has_government = nationalist
			}
			modifier = {
				factor = 0.5
				any_country = {
					OR = {
						has_country_flag = formed_neo_baathist_uar
						has_country_flag = united_neo_baathist_uar
					}
				}
				has_government = nationalist
			}
		}
	}
}

#Strike against the Arab Union
country_event = {
	id = UAR.6
	title = UAR.6.t
	desc = UAR.6.d
	picture = GFX_united_arab_republic

	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR.6.a executed"
		name = UAR.6.a
		ai_chance = { factor = 100 }
		random_country = {
			limit = {
				OR = {
					has_country_flag = united_neo_baathist_uar
					has_country_flag = united_baathist_uar
				}
			}
			ROOT = {
				create_wargoal = {
					target = PREV
					type = annex_everything
				}
			}
		}
	}
}

#The competing Arab Union challenges us
country_event = {
	id = UAR.7
	title = UAR.7.t
	desc = UAR.7.d
	picture = GFX_united_arab_republic

	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR.7.a executed"
		name = UAR.7.a
		ai_chance = { factor = 100 }
		create_wargoal = {
			target = FROM
			type = annex_everything
		}
	}
}

#Formation of an UAR
news_event = {
	id = UAR_news.1
	title = UAR_news.1.t
	desc = UAR_news.1.d
	picture = GFX_news_united_arab_republic

	is_triggered_only = yes

	major = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR_news.1.a executed"
		name = UAR_news.1.a
		ai_chance = { factor = 100 }
	}
}

#Country joins UAR
news_event = {
	id = UAR_news.2
	title = UAR_news.2.t
	desc = UAR_news.2.d
	picture = GFX_news_united_arab_republic

	is_triggered_only = yes

	major = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR_news.2.a executed"
		name = UAR_news.2.a
		ai_chance = { factor = 100 }
	}
}

#Country leaves UAR
news_event = {
	id = UAR_news.3
	title = UAR_news.3.t
	desc = UAR_news.3.d
	picture = GFX_news_united_arab_republic

	is_triggered_only = yes

	major = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR_news.3.a executed"
		name = UAR_news.3.a
		ai_chance = { factor = 100 }
	}
}

#UAR is dissolved
news_event = {
	id = UAR_news.4
	title = UAR_news.4.t
	desc = UAR_news.4.d
	picture = GFX_news_united_arab_republic

	is_triggered_only = yes

	major = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR_news.4.a executed"
		name = UAR_news.4.a
		ai_chance = { factor = 100 }
	}
}

#UAR is united
news_event = {
	id = UAR_news.5
	title = UAR_news.5.t
	desc = UAR_news.5.d
	picture = GFX_news_united_arab_republic

	is_triggered_only = yes

	major = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR_news.5.a executed"
		name = UAR_news.5.a
		ai_chance = { factor = 100 }
	}
}

#Arab Liberation Front created
news_event = {
	id = UAR_news.6
	title = UAR_news.6.t
	desc = UAR_news.6.d
	picture = GFX_news_arab_spring

	is_triggered_only = yes

	major = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR_news.6.a executed"
		name = UAR_news.6.a
		ai_chance = { factor = 100 }
	}
}

#Arab Unification Wars
news_event = {
	id = UAR_news.7
	title = UAR_news.7.t
	desc = UAR_news.7.d
	picture = GFX_news_mideast_citycapture

	is_triggered_only = yes

	major = yes

	option = {
		log = "[GetDateText]: [This.GetName]: UAR_news.7.a executed"
		name = UAR_news.7.a
		ai_chance = { factor = 100 }
	}
}