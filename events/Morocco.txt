add_namespace = morocco

# 400 - Random Yearly event

# 2003 - Casablanca Bombings
country_event = {
	id = morocco.400
	title = morocco.400.t
	desc = morocco.400.d
	# TODO: Add an event image here
	is_triggered_only = yes

	# Truly a Tragedy
	option = {
		name = morocco.400.a
		log = "[GetDateText]: [This.GetName]: event morocco.400.a executed"
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = -0.02 }
		set_temp_variable = { temp_outlook_increase = -0.02 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 11 }
		set_temp_variable = { party_popularity_increase = 0.03 }
		set_temp_variable = { temp_outlook_increase = 0.03 }
		add_relative_party_popularity = yes

		add_stability = -0.04

		set_temp_variable = { rad_change = 5 }
		modify_radicalization_effect = yes
		set_temp_variable = { threat_change = 3 }
		modify_terror_threat_effect = yes

		ai_chance = {
			base = 1
		}
	}

	# Pay for the Funerals
	option = {
		name = morocco.400.b
		log = "[GetDateText]: [This.GetName]: event morocco.400.b executed"
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.02 }
		set_temp_variable = { temp_outlook_increase = 0.02 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 11 }
		set_temp_variable = { party_popularity_increase = 0.01 }
		set_temp_variable = { temp_outlook_increase = 0.01 }
		add_relative_party_popularity = yes

		add_stability = -0.02

		set_temp_variable = { treasury_change = gdp_total }
		multiply_temp_variable = { treasury_change = -0.01 }
		modify_treasury_effect = yes

		set_temp_variable = { rad_change = -3 }
		modify_radicalization_effect = yes
		set_temp_variable = { threat_change = 1 }
		modify_terror_threat_effect = yes

		ai_chance = {
			base = 1
			modifier = {
				factor = 0
				has_active_mission = bankruptcy_incoming_collapse
			}
		}
	}
}