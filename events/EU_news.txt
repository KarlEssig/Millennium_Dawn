add_namespace = EU_news ### next free #57
add_namespace = EU_info

# EU Info
country_event = {
	id = EU_info.1
	title = EU_info.1.t
	desc = EU_info.1.d
	#picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_info.1.a
		log = "[GetDateText]: [THIS.GetName]: event EU_info.1.a"

	}
}

#State of the European Union
news_event = {
	id = EU_news.1
	title = EU_news.1.t
	desc = EU_news.1.d
#	picture = GFX_news_EU

	is_triggered_only = yes

	option = {
		name = EU_news.1.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.1.a"
	}
}

#####################################
### Offices of the European Union ###
#####################################


### President of the European Commission ###
news_event = {
	id = EU_news.2
	title = EU_news.2.t
	desc = EU_news.2.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.2.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.2.a"
	}
}
### President of the European Council ###
news_event = {
	id = EU_news.3
	title = EU_news.3.t
	desc = EU_news.3.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.3.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.3.a"
	}
}
### High Representative of the Union for Foreign Affairs and Security Policy ###
news_event = {
	id = EU_news.4
	title = EU_news.4.t
	desc = EU_news.4.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.4.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.4.a"
	}
}
### President of the European Parliament ###
news_event = {
	id = EU_news.5
	title = EU_news.5.t
	desc = EU_news.5.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.5.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.5.a"
	}
}
### President of the European Central Bank ###
news_event = {
	id = EU_news.6
	title = EU_news.6.t
	desc = EU_news.6.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.6.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.6.a"
	}
}
### European Minister of Economy and Finance ###
news_event = {
	id = EU_news.7
	title = EU_news.7.t
	desc = EU_news.7.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.7.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.7.a"
	}
}
### Frontex Executive Director ###
news_event = {
	id = EU_news.8
	title = EU_news.8.t
	desc = EU_news.8.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.8.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.8.a"
	}
}
### Secretary General of EuroArmy ###
news_event = {
	id = EU_news.9
	title = EU_news.9.t
	desc = EU_news.9.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.9.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.9.a"
	}
}
### Secretary General of EuroNavy ###
news_event = {
	id = EU_news.10
	title = EU_news.10.t
	desc = EU_news.10.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.10.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.10.a"
	}
}
### Ambassador of the Union to the United Nations ###
news_event = {
	id = EU_news.11
	title = EU_news.11.t
	desc = EU_news.11.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.11.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.11.a"
	}
}
### President of the European Federation ###
news_event = {
	id = EU_news.12
	title = EU_news.12.t
	desc = EU_news.12.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_news.12.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.12.a"
	}
}

### EU_news.13 - EU_news.19 reserved for additional offices

############################################
### End of Offices of the European Union ###
############################################

#  EU Exit by accident (troika reforms)
news_event = {
	id = EU_news.20
	title = EU_news.20.t
	desc = EU_news.20.d
	picture = GFX_eu
	major = yes
	is_triggered_only = yes ### EUevent.9
	option = {
		name = EU_news.20.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.20.a"
	}
}

# EU Exit by accident (no deal exit)
news_event = {
	id = EU_news.21
	title = EU_news.21.t
	desc = EU_news.21.d
	picture = GFX_eu
	major = yes
	is_triggered_only = yes ### EU_exit
	option = {
		name = EU_news.21.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.21.a"
	}
}

# Article 50 is Revoked
news_event = {
	id = EU_news.22
	title = EU_news.22.t
	desc = EU_news.22.d
	picture = GFX_eu
	major = yes
	is_triggered_only = yes ### EU_exit
	trigger = {
		has_idea = EU_member
	}
	option = {
		name = EU_news.22.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.22.a"
	}
}

### withdrawal_treaty ###
news_event = {
	id = EU_news.23
	title = EU_news.23.t
	desc = EU_news.23.d
	picture = GFX_eu
	major = yes
	is_triggered_only = yes ### EU_exit
	option = {
		name = EU_news.23.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.23.a"
	}
}

### Article 50 Extension ###
news_event = {
	id = EU_news.24
	title = EU_news.24.t
	desc = EU_news.24.d
	picture = GFX_eu
	major = yes
	is_triggered_only = yes ### EU_extend_Article_50
	trigger = {
		has_idea = EU_member
	}
	option = {
		name = EU_news.24.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.24.a"
	}
}

### Article 50 is triggered ###
news_event = {
	id = EU_news.34
	title = EU_news.34.t
	desc = EU_news.34.d
	picture = GFX_eu
	major = yes
	is_triggered_only = yes ### EU_Article_50
	trigger = {
		has_idea = EU_member
	}
	option = {
		name = EU_news.34.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.34.a"
	}
}

### EuroNavy Transfer ###
news_event = {
	id = EU_news.25
	title = EU_news.25.t
	desc = EU_news.25.d
	picture = GFX_eu

	is_triggered_only = yes ### EuroNavy Transfer Navy
	option = {
		name = EU_news.25.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.25.a"
	}
}

### EuroNavy Transfer denied ###
news_event = {
	id = EU_news.26
	title = EU_news.26.t
	desc = EU_news.26.d
	picture = GFX_eu

	is_triggered_only = yes ### EuroNavy Transfer Navy
	option = {
		name = EU_news.26.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.26.a"
	}
}

### Country joins EU ###
news_event = {
	id = EU_news.27
	title = EU_news.27.t
	desc = EU_news.27.d
	picture = GFX_eu

	is_triggered_only = yes ### EU_apply_for_membership
	option = {
		name = EU_news.27.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.27.a"
	}
}

### euroscepticism decline -3% ###
news_event = {
	id = EU_news.28
	title = EU_news.28.t
	desc = EU_news.28.d
	picture = GFX_eu

	is_triggered_only = yes ### pro_european_campaign
	option = {
		name = EU_news.28.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.28.a"
	}
}

### euroscepticism decline -5% ###
news_event = {
	id = EU_news.29
	title = EU_news.29.t
	desc = EU_news.29.d
	picture = GFX_eu

	is_triggered_only = yes ### pro_european_campaign
	option = {
		name = EU_news.29.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.29.a"
	}
}

### euroscepticism rise 3% ###
news_event = {
	id = EU_news.30
	title = EU_news.30.t
	desc = EU_news.30.d
	picture = GFX_eu

	is_triggered_only = yes ### euroscepticism_campaign
	option = {
		name = EU_news.30.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.30.a"
	}
}

### euroscepticism rise 5% ###
news_event = {
	id = EU_news.31
	title = EU_news.31.t
	desc = EU_news.31.d
	picture = GFX_eu

	is_triggered_only = yes ### euroscepticism_campaign
	option = {
		name = EU_news.31.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.31.a"
	}
}


### ECB_Outright_Monetary_Transactions ###

### OMT Start ###
news_event = {
	id = EU_news.32
	title = EU_news.32.t
	desc = EU_news.32.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.32.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.32.a"
	}
}

### OMT End ###
news_event = {
	id = EU_news.33
	title = EU_news.33.t
	desc = EU_news.33.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.33.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.33.a"
	}
}

### Reduce OMT ###
news_event = {
	id = EU_news.45
	title = EU_news.45.t
	desc = EU_news.45.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.45.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.45.a"
	}
}

### Expand OMT ###
news_event = {
	id = EU_news.46
	title = EU_news.46.t
	desc = EU_news.46.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.46.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.46.a"
	}
}


### End of ECB_Outright_Monetary_Transactions ###


### new euro campaign events ###

### pro_european_campaign success
news_event = {
	id = EU_news.35
	title = EU_news.35.t
	desc = EU_news.35.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.35.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.35.a"
	}
}

### pro_european_campaign greate success
news_event = {
	id = EU_news.36
	title = EU_news.36.t
	desc = EU_news.36.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.36.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.36.a"
	}
}

### pro_european_campaign failure
news_event = {
	id = EU_news.37
	title = EU_news.37.t
	desc = EU_news.37.d
	picture = GFX_eu
	is_triggered_only = yes ###
	option = {
		name = EU_news.37.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.37.a"
	}
}


### euroscepticism_campaign success
news_event = {
	id = EU_news.38
	title = EU_news.38.t
	desc = EU_news.38.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.38.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.38.a"
	}
}

### euroscepticism_campaign greate success
news_event = {
	id = EU_news.39
	title = EU_news.39.t
	desc = EU_news.39.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.39.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.39.a"
	}
}

### euroscepticism_campaign failure
news_event = {
	id = EU_news.40
	title = EU_news.40.t
	desc = EU_news.40.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.40.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.40.a"
	}
}

### root_pro_european_campaign success
news_event = {
	id = EU_news.41
	title = EU_news.41.t
	desc = EU_news.41.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.41.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.41.a"
	}
}

### root_pro_european_campaign failure
news_event = {
	id = EU_news.42
	title = EU_news.42.t
	desc = EU_news.42.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.42.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.42.a"
	}
}

### root_euroscepticism_campaign success
news_event = {
	id = EU_news.43
	title = EU_news.43.t
	desc = EU_news.43.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.43.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.43.a"
	}
}

### euroscepticism_campaign failure
news_event = {
	id = EU_news.44
	title = EU_news.44.t
	desc = EU_news.44.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.44.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.44.a"
	}
}

### EDU
news_event = {
	id = EU_news.47
	title = EU_news.47.t
	desc = EU_news.47.d
	picture = GFX_eu

	is_triggered_only = yes ###
	option = {
		name = EU_news.47.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.47.a"
	}
}

### non-EU NATO members notification of EDU
news_event = {
	id = EU_news.48
	title = EU_news.48.t
	desc = EU_news.48.d
	picture = GFX_news_nato
	major = yes
	is_triggered_only = yes ###
	option = {
		name = EU_news.48.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.48.a"
	}
}

### NATO members leaves and joins EU and EDU
news_event = {
	id = EU_news.49
	title = EU_news.49.t
	desc = EU_news.49.d
	picture = GFX_news_nato

	is_triggered_only = yes ###
	option = {
		name = EU_news.49.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.49.a"
	}
}
### Military Exercise at border
news_event = {
	id = EU_news.50
	title = EU_news.50.t
	desc = EU_news.50.d
	picture = GFX_generic_soldiers

	is_triggered_only = yes ###
	option = {
		name = EU_news.50.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.50.a"
	}
}

### Military Exercise countered
news_event = {
	id = EU_news.51
	title = EU_news.51.t
	desc = EU_news.51.d
	picture = GFX_news_event_war

	is_triggered_only = yes ###
	option = {
		name = EU_news.51.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.51.a"
	}
}
### Result of EU Parliament elections
news_event = {
	id = EU_news.52
	title = EU_news.52.t
	desc = EU_news.52.d
	picture = GFX_eu

	is_triggered_only = yes ### EUelection.1.a
	option = {
		name = EU_news.52.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.52.a"
	}
}

### Result of EU Parliament elections
news_event = {
	id = EU_news.53
	title = EU_news.53.t
	desc = EU_news.53.d
	picture = GFX_eu

	is_triggered_only = yes ### EUelection.1.a
	option = {
		name = EU_news.53.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.53.a"
	}
}

### Result of EU Parliament elections
news_event = {
	id = EU_news.54
	title = EU_news.54.t
	desc = EU_news.54.d
	picture = GFX_eu

	is_triggered_only = yes ### EUelection.1.a
	option = {
		name = EU_news.54.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.54.a"
	}
}


# Unused - Probably should delete
### EU Budget
news_event = {
	id = EU_news.55
	title = EU_news.55.t
	desc = EU_news.55.d
	picture = GFX_eu

	is_triggered_only = yes ### on_startup / on_yearly
	option = {
		name = EU_news.55.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.55.a"
		add_to_variable = { THIS.treasury = THIS.EU_budget_total_gain }
		subtract_from_variable = { THIS.treasury = THIS.EU_budget_contribution }
		ingame_update_setup = yes
	}
}

### EU Budget
news_event = {
	id = EU_news.56
	title = EU_news.56.t
	desc = EU_news.56.d
	picture = GFX_eu

	is_triggered_only = yes ### on_startup / on_yearly
	option = {
		name = EU_news.56.a
		log = "[GetDateText]: [THIS.GetName]: event EU_news.56.a"
		subtract_from_variable = { THIS.treasury = THIS.EU_budget_contribution }
		ingame_update_setup = yes
	}
}