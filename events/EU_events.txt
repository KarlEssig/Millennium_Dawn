﻿# Author(s): AngriestBird, Doolittle, Papinian7
add_namespace = EUevent
add_namespace = EUcheat
add_namespace = EUupdate
add_namespace = EUelection
add_namespace = EUvote
add_namespace = EU_POTEF
add_namespace = EU_POTEF_news

# EU Cheat EVents
# Mainly Utility
# EUcheat.1 == Voting Cheat Section
country_event = {
	id = EUcheat.1
	title = EUcheat.1.t
	desc = EUcheat.1.d
	is_triggered_only = yes

	option = {
		name = EUcheat.1.a
		log = "[GetDateText]: [Root.GetName]: event EUcheat.1.a"
		set_global_flag = EU_voting_cheat_yes
	}
	option = {
		name = EUcheat.1.b
		log = "[GetDateText]: [Root.GetName]: event EUcheat.1.b"
		set_global_flag = EU_voting_cheat_no
	}
	option = {
		name = EUcheat.1.c
		log = "[GetDateText]: [Root.GetName]: event EUcheat.1.c"
		clr_global_flag = EU_voting_cheat_yes
		clr_global_flag = EU_voting_cheat_no
	}
}

### EUupdate

# Triggers the Update to the EUU_monitor nation and preps the
country_event = {
	id = EUupdate.1
	is_triggered_only = yes
	hidden = yes
	immediate = {
		log = "[GetDateText]: [ROOT.GetName]: event EUupdate.1"
		random_country = {
			limit = {
				has_country_flag = EU_potential
				is_ai = yes
			}
			add_ideas = EUU_monitor
			activate_mission = EU_update_vars_mission
			activate_mission = EU_clear_voting_mission
			if = { limit = { has_start_date < 2000.01.02 }
				country_event = { id = EUelection.2 }
			}
			else = {
				country_event = { id = EUelection.3 }
			}
			activate_mission = EU_election_5_years
		}
	}
}

country_event = {
	id = EUupdate.2
	is_triggered_only = yes
	hidden = yes
	immediate = {
		log = "[GetDateText]: [ROOT.GetName]: event EUupdate.2.a"
		every_country = {
			limit = {
				has_country_flag = EU_potential
				is_ai = yes
			}
			remove_ideas = EUU_monitor
			remove_mission = EU_update_vars_mission
			remove_mission = EU_clear_voting_mission
			EU_clear_voting = yes
		}
		country_event = { id = EUupdate.1 }
	}
}


# EU–Turkey deal TUR
country_event = {
	id = EUevent.2
	title = EUevent.2.t
	desc = EUevent.2.d
	picture = GFX_euro_turkey_deal
	is_triggered_only = yes

	option = {
		name = EUevent.2.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.2.a"
		###
		TUR = {
			set_temp_variable = { treasury_change = 3 }
			modify_treasury_effect = yes
		}
		for_each_scope_loop = {
			array = global.EU_member
			country_event = { id = EUevent.17 }
		}
	}
}

# Parliament Vote Started
country_event = {
	id = EUvote.1
	title = EUvote.1.t
	desc = EUvote.1.d
	picture = GFX_european_parliament
	is_triggered_only = yes

	option = {
		name = EUvote.1.a
		log = "[GetDateText]: [Root.GetName]: event EUvote.1.a"
		custom_effect_tooltip = eu_voting_minor_popup_tt
	}
}

# Council Vote Started
country_event = {
	id = EUvote.2
	title = EUvote.2.t
	desc = EUvote.2.d
	picture = GFX_european_council
	is_triggered_only = yes

	option = {
		name = EUvote.2.a
		log = "[GetDateText]: [Root.GetName]: event EUvote.2.a"
		custom_effect_tooltip = eu_voting_minor_popup_tt
	}
}

# The Parliament is Ready for Session
country_event = {
	id = EUvote.3
	title = EUvote.3.t
	desc = EUvote.3.d
	picture = GFX_european_council
	is_triggered_only = yes

	option = {
		name = EUvote.3.a
		log = "[GetDateText]: [Root.GetName]: event EUvote.3.a"
		custom_effect_tooltip = eu_voting_minor_popup_tt
	}
}

# The Council is Ready for Session
country_event = {
	id = EUvote.4
	title = EUvote.4.t
	desc = EUvote.4.d
	picture = GFX_european_council
	is_triggered_only = yes

	option = {
		name = EUvote.4.a
		log = "[GetDateText]: [Root.GetName]: event EUvote.4.a"
		custom_effect_tooltip = eu_voting_minor_popup_tt
	}
}

# United States of Europe
country_event = {
	id = EUevent.3
	title = EUevent.3.t
	desc = EUevent.3.d
	picture = GFX_european_union_flag
	is_triggered_only = yes

	option = { #a
		name = EUevent.3.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.3.a"
		hidden_effect = {
			EUU = {
				for_each_loop = {
					array = researched_techs
					if = {
						limit = {
							NOT = { is_in_array = { eu_technologies = v } }
						}
						add_to_temp_array = { eu_technologies = v }
					}
				}
			}
			for_each_loop = {
				array = eu_technologies
				meta_effect = {
					text = {
						set_technology = {
							[TECH] = 1
							popup = no
						}
					}
					TECH = "[?v.GetTokenKey]"
				}
			}
			every_country = {
				limit = { has_idea = EU_member }
				EUU = { inherit_technology = PREV }
				set_country_flag = USoE_member
				every_unit_leader = {
					set_unit_leader_flag = USoE_unit_leader
					set_nationality = ROOT
					add_unit_leader_trait = military_reorganization
				}
				every_owned_state = {
					limit = {
						OR = {
							is_on_continent = europe
							is_core_of = GEO
							is_core_of = ARM
							is_core_of = AZE
							is_core_of = TUR
							is_core_of = POR
							is_core_of = SPR
						}
						NOT = { is_core_of = ROOT }
					}
					add_core_of = ROOT
				}
				country_event = EUevent.4
			}
			ROOT = {
				set_cosmetic_tag = USoE
				clr_country_flag = dynamic_flag
				set_country_flag = USoE
				add_ideas = multi_ethnic_state_idea
			}
		}
		custom_effect_tooltip = form_USoE_tt
		hidden_effect = {
			if = {
				limit = {
					original_tag = GER
				}
				clr_country_flag = GER_Final_settlement
				clr_country_flag = GER_Grundgesetz
				remove_ideas = german_legacy
				remove_ideas = GER_idea_Sickman
				clr_country_flag = GER_no_nuclear
			}
		}
	}
}

country_event = {
	id = EUevent.4
	title = EUevent.4.t
	desc = EUevent.4.d
	picture = GFX_european_union_flag
	is_triggered_only = yes

	option = { #a
		name = EUevent.4.a
		log = "[GetDateText]: [THIS.GetName]: event EUevent.4.a"
		if = { limit = { NOT = { original_tag = FROM } }
			FROM = {
				annex_country = {
					target = ROOT
					transfer_troops = yes
				}
				# Integrates Entire Economy of the nation
				add_to_variable = { treasury = ROOT.treasury }
				add_to_variable = { debt = ROOT.debt }
				add_to_variable = { int_investments = ROOT.int_investments }
				set_variable = { tag_to_grab = ROOT.id }
				update_missile_satellite_inventory = yes
			}
		}
	}
}

# European Debt Crisis
country_event = {
	id = EUevent.5
	title = EUevent.5.t
	desc = EUevent.5.d
	picture = GFX_eurojail
	is_triggered_only = yes
	fire_only_once = yes

	# The Euro is at Stake!
	option = {
		name = EUevent.5.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.5.a"
		every_country = {
			limit = { has_idea = EU_member }
			add_ideas = european_debt_crisis
			if = {
				limit = {
					check_variable = { interest_rate > 4.5 }
					gdp_debt_ratio_lower_75 = yes
				}
				add_ideas = EUU_national_dept_crisis_eu
			}
		}
		set_global_flag = EUU_european_debt_crisis
	}
}

# Eurozone Reforms
country_event = {
	id = EUevent.6
	title = EUevent.6.t
	desc = EUevent.6.d
	picture = GFX_eurozone_reforms
	is_triggered_only = yes

	# In varietate concordia
	# - European Union Debt Crisis Will Never Happen
	option = {
		name = EUevent.6.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.6.a"
		set_global_flag = eurozone_reforms
		set_temp_variable = { modify_eurosceptic = -0.10 }
		eurosceptic_change = yes
	}
}

# Eurozone Reforms
country_event = {
	id = EUevent.7
	title = EUevent.7.t
	desc = EUevent.7.d
	picture = GFX_eurozone_reforms
	is_triggered_only = yes
	trigger = {
		NOT = { has_global_flag = eurozone_reforms }
		has_idea = EU_member
	}

	# So they begin
	option = {
		name = EUevent.7.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.7.a"
		every_country = {
			limit = { has_idea = EU_member }
			activate_mission = eurozone_reforms
		}
	}
}

# Troika Reforms Accomplished
country_event = {
	id = EUevent.8
	title = EUevent.8.t
	desc = EUevent.8.d
	picture = GFX_european_union_flag
	is_triggered_only = yes

	# two years
	option = { #a
		name = EUevent.8.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.8.a"
		remove_ideas = EUU_national_dept_crisis_eu
		add_timed_idea = { idea = austerity days = 730 }
	}
}

# EU Exit by Accident
country_event = {
	id = EUevent.9
	title = EUevent.9.t
	desc = EUevent.9.d
	picture = GFX_article_50
	is_triggered_only = yes

	option = {
		name = EUevent.9.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.9.a"
		ROOT = {
			add_timed_idea = {
				idea = no_deal_exit
				days = 1825 # five years # unprepared no deal exit # prepared no deal exit with two years with focus EU703
			}
			leaving_EU = yes
		}
		news_event = { id = EU_news.20 }
	}
}

# EuroNavy Transfer Navy
country_event = {
	id = EUevent.10
	title = EUevent.10.t
	desc = EUevent.10.d
	picture = GFX_carrier
	is_triggered_only = yes

	option = {
		name = EUevent.10.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.10.a"
		ROOT = {
			transfer_navy = {
				target = FROM
			}
		}
		every_country = {
			limit = { has_idea = EU_member }
			news_event = { id = EU_news.25 }
		}
		ai_chance = {
			factor = 100
		}
	}

	option = { #b no
		name = EUevent.10.b
		log = "[GetDateText]: [Root.GetName]: event EUevent.10.b"
		every_country = {
			limit = { has_idea = EU_member }
			news_event = { id = EU_news.26 }
		}
		ai_chance = {
			factor = 0
		}
	}
}


# EU–Turkey Deal Member States
country_event = {
	id = EUevent.17
	title = EUevent.17.t
	desc = EUevent.17.d
	picture = GFX_euro_turkey_deal
	is_triggered_only = yes

	option = { #a
		name = EUevent.17.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.17.a"
		ROOT = {
			set_temp_variable = { temp1 = THIS.var_EUpopRatio }
			multiply_temp_variable = { temp1 = -3.0 }
			set_temp_variable = { treasury_change = temp1 }
			modify_treasury_effect = yes
			ingame_update_setup = yes
		}
	}
}

### FROM asks us to host a Military Exercise
country_event = {
	id = EUevent.18
	title = EUevent.18.t
	desc = EUevent.18.d
	picture = GFX_france_leclerc
	is_triggered_only = yes

	option = { #a yes
		name = EUevent.18.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.18.a"


		set_country_flag = EU_host_military_exercise@FROM
		random_owned_state = {
			limit = {
				any_neighbor_state = {
					is_owned_by = FROM.var_EU_military_exercise_target
				}
			}
			set_variable = {
				var = FROM.var_EU_military_exercise_host_state
				value = THIS
			}
		}
		FROM = {
			activate_mission = EU_military_exercise_mission
			set_variable = {
				var = FROM.var_EU_military_exercise_host_tag
				value = ROOT
			}
		}

		ai_chance = {
			factor = 1
		}
	}
	option = { #b no
		name = EUevent.18.b
		log = "[GetDateText]: [Root.GetName]: event EUevent.18.b"

		ai_chance = {
			factor = 0
		}
	}
}

country_event = {
	id = EUevent.19
	hidden = yes
	is_triggered_only = yes
	immediate = {
		log = "[GetDateText]: [Root.GetName]: event EUevent.19"
		set_THIS_desire = yes
		set_PG_desire = yes
	}
}

# pre_accession_assistance
country_event = {
	id = EUevent.20
	title = EUevent.20.t
	desc = EUevent.20.d
	picture = GFX_euro
	is_triggered_only = yes

	option = { #a yes
		name = EUevent.20.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.20.a"
		set_temp_variable = { gdp_percent = FROM.gdp_total }
		multiply_temp_variable = { gdp_percent = -0.05 }

		set_temp_variable = { treasury_change = gdp_percent }
		modify_treasury_effect = yes
		FROM = {
			decrease_corruption = yes
		}
	}

	option = { #b no
		name = EUevent.20.b
		log = "[GetDateText]: [Root.GetName]: event EUevent.20.b"
	}
}

# Office Expired
country_event = {
	id = EUevent.21
	title = EUevent.21.t
	desc = EUevent.21.d
	picture = GFX_european_union_flag
	is_triggered_only = yes

	# Thanks for Tip!
	option = {
		name = EUevent.21.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.21.a"
	}
}

# The Migrant Crisis of Europe
country_event = {
	id = EUevent.22
	title = EUevent.22.t
	desc = EUevent.22.d
	picture = GFX_european_migrant_crisis
	is_triggered_only = yes

	# Focus on Anti-Asylum Policies
	option = {
		name = EUevent.22.a
		log = "[GetDateText]: [Root.GetName]: event EUevent.22.a"
		every_country = {
			limit = {
				has_idea = EU_member
			}
			add_ideas = Migrant_Crisis_Europe
		}
	}
}


# European Union Election
country_event = {
	id = EUelection.1
	title = EUelection.1.t
	desc = EUelection.1.d
	picture = GFX_european_parliament_elections
	is_triggered_only = yes

	option = {
		name = EUelection.1.a
		log = "[GetDateText]: [Root.GetName]: event EUelection.1.a"
		clear_EU_Parliament = yes
		election_EU_Parliament = yes

		### Result of EU Parliament elections
		for_each_scope_loop = {
			array = global.EU_member
			news_event = { id = EU_news.52 }
		}
	}
}

country_event = {
	id = EUelection.2
	title = EUelection.2.t
	desc = EUelection.2.d
	picture = GFX_european_parliament_elections
	is_triggered_only = yes

	option = { #a
		name = EUelection.2.a
		log = "[GetDateText]: [Root.GetName]: event EUelection.2.a"
		clear_EU_Parliament = yes
		election_EU_Parliament = yes
		activate_mission = EU_election_2004
	}
}

country_event = {
	id = EUelection.3
	title = EUelection.3.t
	desc = EUelection.3.d
	picture = GFX_european_parliament_elections
	is_triggered_only = yes

	option = { #a
		name = EUelection.3.a
		log = "[GetDateText]: [Root.GetName]: event EUelection.3.a"
		clear_EU_Parliament = yes
		election_EU_Parliament = yes
		activate_mission = EU_election_2019
	}
}

# ----- POTEF
news_event = {
	id = EU_POTEF_news.1
	title = EU_POTEF_news.1.t
	desc = EU_POTEF_news.1.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.1.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news.1.a"

	}
}

news_event = {
	id = EU_POTEF_news.2
	title = EU_POTEF_news.2.t
	desc = EU_POTEF_news.2.d
	picture = GFX_eu
	major = yes
	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.2.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news.2.a"

	}
}

news_event = {
	id = EU_POTEF_news.5
	title = EU_POTEF_news.5.t
	desc = EU_POTEF_news.5.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.5.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news.5.a"
	}
}

news_event = {
	id = EU_POTEF_news.6
	title = EU_POTEF_news.6.t
	desc = EU_POTEF_news.6.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.6.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news.6.a"
	}
}
