###########################
# 	  Cuba Events	  #
###########################
add_namespace = cuba
add_namespace = cuba_news
add_namespace = CubaConf

country_event = {
	id = cuba.3
	title = cuba.3.t
	desc = cuba.3.d
	picture = GFX_Army_reforms_event
	is_triggered_only = yes
	option = {
		name = cuba.3.a
		log = "[GetDateText]: [This.GetName]: cuba.3.a"
		create_corps_commander = {
			name = "Alvaro Lopez Miera"
			portrait_path = "gfx/leaders/CUB/small"
			traits = { panzer_leader }
			skill = 2
			attack_skill = 2
			defense_skill = 2
			planning_skill = 1
			logistics_skill = 2
		}
	 }
}

#blockade
country_event = {
	id = cuba.4
	title = cuba.4.t
	desc = cuba.4.d
	picture = GFX_special_forces
	is_triggered_only = yes
	option = {
		name = cuba.4.a
		log = "[GetDateText]: [This.GetName]: cuba.4.a"
		set_country_flag = blockade_cuba_yes
	}
}

country_event = {
	id = cuba.5
	title = cuba.5.t
	desc = cuba.5.d
	picture = GFX_special_forces
	is_triggered_only = yes
	option = {
		name = cuba.5.a
		log = "[GetDateText]: [This.GetName]: cuba.5.a"
	 }
}

country_event = {
	id = cuba.6
	title = cuba.6.t
	desc = cuba.6.d
	picture = GFX_special_forces
	is_triggered_only = yes
	option = {
		name = cuba.6.a
		log = "[GetDateText]: [This.GetName]: cuba.6.a"
		CUB = {
			remove_ideas = CUB_idea_blockade1
		}
		USA = {
			clr_country_flag = blockade_cuba_yes
		}
	 }
	 option = {
		name = cuba.6.b
		log = "[GetDateText]: [This.GetName]: cuba.6.a"
	 }
}

#castro event
country_event = {
	id = cuba.9
	title = cuba.9.t
	desc = cuba.9.d
	picture = GFX_castro_event
	is_triggered_only = yes
	option = {
		name = cuba.9.a
		log = "[GetDateText]: [This.GetName]: cuba.9.a"
	 }
}

country_event = {
	id = cuba.10
	title = cuba.10.t
	desc = cuba.10.d
	picture = GFX_cuba_constitution
	is_triggered_only = yes
	option = {
		name = cuba.10.a
		log = "[GetDateText]: [This.GetName]: cuba.10.a"
	 }
}

country_event = {
	id = cuba.11
	title = cuba.11.t
	desc = cuba.11.d
	is_triggered_only = yes
	picture = GFX_raul_castro
	option = {
		name = cuba.11.a
		log = "[GetDateText]: [This.GetName]: cuba.11.a"
		hidden_effect = {
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 19 }
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes
			set_ruling_leader = yes
			set_temp_variable = { party_index = 0 }
			set_temp_variable = { add_col_one = 0 }
			add_coalition_members_effect = yes
			set_temp_variable = { party_index = 4 }
			set_temp_variable = { add_col_one = 4 }
			add_coalition_members_effect = yes
		}
		set_politics = {
			ruling_party = neutrality
			elections_allowed = no
		}
		create_country_leader = {
			name = "Raul Castro"
			picture = "CUB_Raul_Castro.dds"
			ideology = Neutral_Communism
			traits = {
				neutrality_neutral_Communism
				political_dancer
				permanent_revolutionary
				likeable
			}
		}
	}
}

country_event = {
	id = cuba.12
	title = cuba.12.t
	desc = cuba.12.d
	picture = GFX_castro_democratic
	is_triggered_only = yes
	option = {
		name = cuba.12.a
		log = "[GetDateText]: [This.GetName]: cuba.11.a"
		hidden_effect = {
			clear_array = ruling_party
			add_to_array = { ruling_party = 0 }
			set_temp_variable = { current_cons_popularity = party_pop_array^0 }
			multiply_temp_variable = { current_cons_popularity = 0.5 }
			recalculate_party = yes
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes
			set_ruling_leader = yes
		}
		set_politics = {
			ruling_party = democratic
			elections_allowed = no
		}

		create_country_leader = {
			name = "Fidel Castro"
			picture = "fidel_castro.dds"
			ideology = liberalism
			traits = {
				western_liberalism
				western_socialism
				political_dancer
			}
		}
	}
}
country_event = {
	id = cuba.14
	title = cuba.14.t
	desc = cuba.14.d
	picture = GFX_cuba_guantanamo_question_event
	is_triggered_only = yes
	option = {
		name = cuba.14.a
		log = "[GetDateText]: [This.GetName]: cuba.14.a"
		CUB = { country_event = { id = cuba.26 days = 1 } }
		ai_chance = {
			base = 100
		}
	 }
	 option = {
		name = cuba.14.b
		log = "[GetDateText]: [This.GetName]: cuba.14.b"
		CUB = { country_event = { id = cuba.27 days = 1 } }
		ai_chance = {
			base = 10
		}
	 }
}
country_event = {
	id = cuba.15
	title = cuba.15.t
	desc = cuba.15.d
	picture = GFX_rohel
	is_triggered_only = yes
	option = {
		name = cuba.15.a
		log = "[GetDateText]: [This.GetName]: cuba.14.a"
		decrease_corruption = yes
		set_temp_variable = { temp_opinion = -5 }
		change_communist_cadres_opinion = yes
	 }
	 option = {
		name = cuba.15.b
		log = "[GetDateText]: [This.GetName]: cuba.15.a"
		increase_corruption = yes
		set_temp_variable = { temp_opinion = 5 }
		change_communist_cadres_opinion = yes
	 }
}
country_event = {
	id = cuba.16
	title = cuba.16.t
	desc = cuba.16.d
	picture = GFX_Pope_roma_and_raul
	is_triggered_only = yes
	option = {
		name = cuba.16.a
		log = "[GetDateText]: [This.GetName]: cuba.16.a"
	 }
}
#alba events
country_event = {
	id = cuba.17
	title = cuba.17.t
	desc = cuba.17.d
	picture = GFX_alba_country_event
	is_triggered_only = yes
	option = {
		name = cuba.17.a
		log = "[GetDateText]: [This.GetName]: cuba.17.a"
		CUB = {
			add_ideas = CUB_alba_idea
		}
		VEN = {
			add_ideas = CUB_alba_idea
		}
		CUB = { country_event = { id = cuba.18 days = 1 } }
		ai_chance = {
			base = 100
		}
	 }
	option = {
		name = cuba.17.b
		log = "[GetDateText]: [This.GetName]: cuba.17.a"
		CUB = { country_event = { id = cuba.19 days = 1 } }
		ai_chance = {
			base = 20
		}
	 }
}
#They said YES
country_event = {
	id = cuba.18
	title = cuba.18.t
	desc = cuba.18.d
	picture = GFX_handshake_black
	is_triggered_only = yes
	option = {
		custom_effect_tooltip = cuba_alba_tt
		name = cuba.18.a
		log = "[GetDateText]: [This.GetName]: cuba.18.a"
		news_event = {
			hours = 6
			id = cuba_news.20
		}
	 }
}
#They said NO
country_event = {
	id = cuba.19
	title = cuba.19.t
	desc = cuba.19.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		name = cuba.19.a
		log = "[GetDateText]: [This.GetName]: cuba.19.a"
	 }
}
#ALBA formed
news_event = {
	id = cuba_news.20
	picture = GFX_alba_creation
	title = cuba_news.20.t
	desc = cuba_news.20.d
	fire_only_once = yes
	major = yes
	is_triggered_only = yes
	option = {
		trigger = {
		NOT = { original_tag = CUB }
		}
		name = cuba_news.20.a
		log = "[GetDateText]: [This.GetName]: cuba_news.20.a"
	 }
}
#Second expansion (need work)
country_event = {
	id = cuba.21
	title = cuba.21.t
	desc = cuba.21.d
	picture = GFX_alba_country_event
	is_triggered_only = yes
	option = {
		log = "[GetDateText]: [This.GetName]: cuba_news.20.a"
		name = cuba.21.a
		increase_economic_growth = yes
	 }
}
#YES
country_event = {
	id = cuba.22
	title = cuba.22.t
	desc = cuba.22.d
	picture = GFX_handshake_black
	is_triggered_only = yes
	option = {
		name = cuba.22.a
		log = "[GetDateText]: [This.GetName]: cuba.22.a"
		decrease_economic_growth = yes
	 }
}
#EACU entering
news_event = {
	id = cuba_news.24
	title = cuba_news.24.t
	desc = cuba_news.24.d
	picture = GFX_cuba_eacu
	is_triggered_only = yes
	option = {
		name = cuba_news.24.a
		log = "[GetDateText]: [This.GetName]: cuba_news.24.a"
	 }
}

country_event = {
	id = cuba.25
	title = cuba.25.t
	desc = cuba.25.d
	picture = GFX_event_for_china_cuba
	is_triggered_only = yes
	option = {
		name = cuba.25.a
		log = "[GetDateText]: [This.GetName]: cuba.25.a"
		CHI = {  diplomatic_relation = {
			country = CUB
			relation = guarantee
			active = yes
		} }
	 }
	 option = {
		name = cuba.25.b
		log = "[GetDateText]: [This.GetName]: cuba.25.b"
	 }
}
#US will give us Guantanamo
country_event = {
	id = cuba.26
	title = cuba.26.t
	desc = cuba.26.d
	picture = GFX_prison_cuba
	is_triggered_only = yes
	option = {
		name = cuba.26.a
		log = "[GetDateText]: [This.GetName]: cuba.26.a"
		news_event = {
			hours = 6
			id = cuba_news.30
		}
		add_state_core = 424
		transfer_state = 424
	 }
}
#US wouldnt give us Guantanamo
country_event = {
	id = cuba.27
	title = cuba.27.t
	desc = cuba.27.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		name = cuba.27.a
		log = "[GetDateText]: [This.GetName]: cuba.27.a"
		CUB = {
			add_opinion_modifier = {
				target = USA
				modifier = major_breach_of_trust
			}
		}
	 }
}
#world event
news_event = {
	id = cuba_news.29
	picture = GFX_NATO_picture
	title = cuba_news.29.t
	desc = cuba_news.29.d
	is_triggered_only = yes
	option = {
		name = cuba_news.29.a
		log = "[GetDateText]: [This.GetName]: cuba_news.29.a"
	 }
}
news_event = {
	id = cuba_news.30
	title = cuba_news.30.t
	desc = cuba_news.30.d
	picture = GFX_cuba_return_guantanamo
	is_triggered_only = yes
	option = {
		name = cuba_news.30.a
		log = "[GetDateText]: [This.GetName]: cuba_news.30.a"
	 }
}
country_event = {
	id = cuba.32
	title = cuba.32.t
	desc = cuba.32.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		name = cuba.32.a
		log = "[GetDateText]: [This.GetName]: cuba.32.a"
	 }
}
country_event = {
	id = cuba.33
	title = cuba.33.t
	desc = cuba.33.d
	picture = GFX_navalny_prison
	is_triggered_only = yes
	option = {
		name = cuba.33.a
		log = "[GetDateText]: [This.GetName]: cuba.33.a"
	 }
}

country_event = {
	id = cuba.34
	title = cuba.34.t
	desc = cuba.34.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		name = cuba.34.a
		log = "[GetDateText]: [This.GetName]: cuba.34.a"
	 }
}
#NATIONALISTS EVENTS
news_event = {
	id = cuba_news.35
	title = cuba_news.35.t
	desc = cuba_news.35.d
	picture = GFX_falanga_in_power
	is_triggered_only = yes
	option = {
		name = cuba_news.35.a
		log = "[GetDateText]: [This.GetName]: cuba_news.35.a"
	 }
}
country_event = {
	id = cuba.36
	title = cuba.36.t
	desc = cuba.36.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = cuba.36.a
		log = "[GetDateText]: [This.GetName]: cuba.36.a"
		CUB = { country_event = { id = cuba.37 days = 1 } }
	 }
	 option = {
		name = cuba.36.b
		log = "[GetDateText]: [This.GetName]: cuba.36.b"
		CUB = { country_event = { id = cuba.38 days = 1 } }
	 }
}
country_event = {
	id = cuba.37
	title = cuba.37.t
	desc = cuba.37.d
	picture = GFX_handshake_black
	is_triggered_only = yes
	option = {
		name = cuba.37.a
		log = "[GetDateText]: [This.GetName]: cuba.37.a"
		CUB = {
			add_state_core = 862
			transfer_state = 862
			add_state_core = 861
			transfer_state = 861
		}
	 }
}
country_event = {
	id = cuba.38
	title = cuba.38.t
	desc = cuba.38.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		create_wargoal = {
			target = DOM
			type = annex_everything
		}
		name = cuba.38.a
		log = "[GetDateText]: [This.GetName]: cuba.38.a"
	 }
}
country_event = {
	id = cuba.39
	title = cuba.39.t
	desc = cuba.39.d
	picture = GFX_hard_choice_cuba
	is_triggered_only = yes
	option = {
		name = cuba.39.a
		#2 party
		log = "[GetDateText]: [This.GetName]: cuba.39.a"
		if = {
			limit = { NOT = { is_in_array = { ruling_party = 2 } } }
			set_temp_variable = { rul_party_temp = 2 }
			set_temp_variable = { enable_elections = 0 }
			change_ruling_party_effect = yes
		}

		set_temp_variable = { party_index = 2 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes

		custom_effect_tooltip = cub_liberal_union_tt
	 }
	 option = {
		name = cuba.39.b
		#3 party
		log = "[GetDateText]: [This.GetName]: cuba.39.b"

		if = {
			limit = { NOT = { is_in_array = { ruling_party = 3 } } }
			set_temp_variable = { rul_party_temp = 3 }
			set_temp_variable = { enable_elections = 0 }
			change_ruling_party_effect = yes
		}

		set_temp_variable = { party_index = 3 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes

		custom_effect_tooltip = cub_democratic_union_tt
	 }
	 option = {
		name = cuba.39.c
		log = "[GetDateText]: [This.GetName]: cuba.39.c"
		if = {
			limit = { NOT = { is_in_array = { ruling_party = 1 } } }
			set_temp_variable = { rul_party_temp = 1 }
			set_temp_variable = { enable_elections = 0 }
			change_ruling_party_effect = yes
		}

		set_temp_variable = { party_index = 1 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes

		custom_effect_tooltip = cub_christian_union_tt
	 }
}
#PUERTO RICO
country_event = {
	id = cuba.40
	title = cuba.40.t
	desc = cuba.40.d
	picture = GFX_puerto_rico_offer
	is_triggered_only = yes
	option = {
		name = cuba.40.a
		log = "[GetDateText]: [This.GetName]: cuba.40.a"
		PTR = {
		add_state_core = 815
		transfer_state = 815
		}
		USA = {
		set_temp_variable = { treasury_change = 10 }
		modify_treasury_effect = yes
		}
		hidden_effect = {
			CUB = {
				set_temp_variable = { treasury_change = 10 }
				modify_treasury_effect = yes
			}
		}
		CUB = { country_event = { id = cuba.41 days = 1 } }
	 }
	 option = {
		name = cuba.40.b
		log = "[GetDateText]: [This.GetName]: cuba.40.b"
		CUB = {
			add_opinion_modifier = {
				target = USA
				modifier = major_breach_of_trust
			}
		}
		CUB = { country_event = { id = cuba.42 days = 1 } }
		set_country_flag = puerto_disagree
	 }
}
#YES
country_event = {
	id = cuba.41
	title = cuba.41.t
	desc = cuba.41.d
	picture = GFX_handshake_black
	is_triggered_only = yes
	option = {
		name = cuba.41.a
		log = "[GetDateText]: [This.GetName]: cuba.41.a"
	 }
}
#NO
country_event = {
	id = cuba.42
	title = cuba.42.t
	desc = cuba.42.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		name = cuba.42.a
		log = "[GetDateText]: [This.GetName]: cuba.42.a"
	 }
}
#Democratic Finish Blockade
country_event = {
	id = cuba.43
	title = cuba.43.t
	desc = cuba.43.d
	picture = GFX_handshake_black
	is_triggered_only = yes
	option = {
		name = cuba.43.a
		log = "[GetDateText]: [This.GetName]: cuba.43.a"
		CUB = {
			remove_ideas = CUB_idea_blockade1
		}
		CUB = {
			add_opinion_modifier = {
				target = USA
				modifier = supports_us
			}
		}
		CUB = { country_event = { id = cuba.44 days = 1 } }
	 }
	 option = {
		name = cuba.43.b
		log = "[GetDateText]: [This.GetName]: cuba.43.b"
		CUB = {
			add_opinion_modifier = {
				target = USA
				modifier = major_breach_of_trust
			}
		}
		CUB = { country_event = { id = cuba.45 days = 1 } }
	 }
}
#yes
country_event = {
	id = cuba.44
	title = cuba.44.t
	desc = cuba.44.d
	picture = GFX_handshake_black
	is_triggered_only = yes
	option = {
		name = cuba.44.a
		log = "[GetDateText]: [This.GetName]: cuba.44.a"
	 }
}
#no
country_event = {
	id = cuba.45
	title = cuba.45.t
	desc = cuba.45.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		name = cuba.45.a
		log = "[GetDateText]: [This.GetName]: cuba.45.a"
	 }
}
country_event = {
	id = cuba.46
	title = cuba.46.t
	desc = cuba.46.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = cuba.46.a
		log = "[GetDateText]: [This.GetName]: cuba.46.a"
		give_military_access = CUB
		USA = {  diplomatic_relation = {
			country = CUB
			relation = guarantee
			active = yes
		} }
		CUB = { country_event = { id = cuba.47 days = 1 } }
	 }
	 option = {
		name = cuba.46.b
		log = "[GetDateText]: [This.GetName]: cuba.46.b"
		CUB = { country_event = { id = cuba.48 days = 1 } }
	 }
}
#yes
country_event = {
	id = cuba.47
	title = cuba.47.t
	desc = cuba.47.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = cuba.47.a
		log = "[GetDateText]: [This.GetName]: cuba.47.a"
	 }
}
#no
country_event = {
	id = cuba.48
	title = cuba.48.t
	desc = cuba.48.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		name = cuba.48.a
		log = "[GetDateText]: [This.GetName]: cuba.48.a"
	 }
}
country_event = {
	id = cuba.49
	title = cuba.49.t
	desc = cuba.49.d
	picture = GFX_important_decision_cuba
	is_triggered_only = yes
	option = {
		name = cuba.49.a
		log = "[GetDateText]: [This.GetName]: cuba.49.a"
		custom_effect_tooltip = cub_dem_unite_tt
		if = {
			limit = { is_in_array = { ruling_party = 3 } }
			hidden_effect = {
				set_temp_variable = { add_col_one = 1 }
				add_coalition_members_effect = yes
				subtract_from_variable = {
					party_pop_array^1 = current_cons_popularity
				}
				set_temp_variable = { add_col_one = 2 }
				add_coalition_members_effect = yes
				subtract_from_variable = {
					party_pop_array^2 = current_cons_popularity
				}
				update_party_name = yes
				set_coalition_drift = yes
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
				set_politics = {
					ruling_party = democratic
					elections_allowed = yes
				}
			}
		}
		if = {
			limit = { is_in_array = { ruling_party = 1 } }
			hidden_effect = {
				set_temp_variable = { add_col_one = 3 }
				add_coalition_members_effect = yes
				subtract_from_variable = {
					party_pop_array^3 = current_cons_popularity
				}
				set_temp_variable = { add_col_one = 2 }
				add_coalition_members_effect = yes
				subtract_from_variable = {
					party_pop_array^2 = current_cons_popularity
				}
				update_party_name = yes
				set_coalition_drift = yes
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
				set_politics = {
					ruling_party = democratic
					elections_allowed = yes
				}
			}
		}
		if = {
			limit = { is_in_array = { ruling_party = 2 } }
			hidden_effect = {
				set_temp_variable = { add_col_one = 3 }
				add_coalition_members_effect = yes
				subtract_from_variable = {
					party_pop_array^3 = current_cons_popularity
				}
				set_temp_variable = { add_col_one = 1 }
				add_coalition_members_effect = yes
				subtract_from_variable = {
					party_pop_array^1 = current_cons_popularity
				}
				update_party_name = yes
				set_coalition_drift = yes
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
				set_politics = {
					ruling_party = democratic
					elections_allowed = yes
				}
			}
		}
	 }
}
country_event = {
	id = CubaConf.50
	title = CubaConf.50.t
	desc = CubaConf.50.d
	picture = GFX_antillean_conf_cuba
	is_triggered_only = yes
	option = {
		name = CubaConf.50.a
		log = "[GetDateText]: [This.GetName]: CubaConf.50.a executed"
		CUB = {
		set_autonomy = {
			target = ROOT
			autonomy_state = autonomy_cuban_confederation
		}
	}
	set_country_flag = antillies_conf_agree
		ai_chance = {
			base = 15
			modifier = {
				add = 20
				is_top5_influencer = yes
			}
			modifier = {
				add = 20
				is_top3_influencer = yes
			}
		}
		set_temp_variable = { percent_change = -2 }
		set_temp_variable = { tag_index = CUB }
		set_temp_variable = { influence_target = ROOT }
		change_influence_percentage = yes
	}
	option = {
		name = CubaConf.50.b
		log = "[GetDateText]: [This.GetName]: CubaConf.50.a executed"
		ai_chance = { base = 85 }
		set_temp_variable = { percent_change = -5 }
		set_temp_variable = { tag_index = CUB }
		set_temp_variable = { influence_target = ROOT }
		change_influence_percentage = yes
		CUB = {	country_event = { id = cubaconf.51 days = 1 } }
	}
}
country_event = {
	id = cubaconf.51
	title = cubaconf.51.t
	desc = cubaconf.51.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes
	option = {
		name = cubaconf.51.a
		log = "[GetDateText]: [This.GetName]: cubaconf.51.a"
	 }
}
#Change Path AI
country_event = {
	id = cuba.52
	title = cuba.52.t
	desc = cuba.52.d
	is_triggered_only = yes
	option = {
		if = {
			limit = {
				has_global_flag = CUB_DEMOCRATS_PATH
			}
			if = {
				limit = { NOT = { is_in_array = { ruling_party = 3 } } }
				set_temp_variable = { rul_party_temp = 3 }
				set_temp_variable = { enable_elections = 0 }
				change_ruling_party_effect = yes
			}
		}
		if = {
			limit = {
				has_global_flag = CUB_COMMUNISM_PATH
			}
			if = {
				limit = { NOT = { is_in_array = { ruling_party = 4 } } }
				set_temp_variable = { rul_party_temp = 4 }
				set_temp_variable = { enable_elections = 1 }
				change_ruling_party_effect = yes
			}
		}
		if = {
			limit = {
				has_global_flag = game_rule_CUB_Tesak
			}
			if = {
				limit = { NOT = { is_in_array = { ruling_party = 22 } } }
				set_temp_variable = { rul_party_temp = 22 }
				set_temp_variable = { enable_elections = 1 }
				change_ruling_party_effect = yes

				add_popularity = {
					ideology = nationalist
					popularity = 0.15
				}
			}
		if = {
			limit = {
				has_global_flag = CUB_NATIONALIST_PATH
			}
			if = {
				limit = { NOT = { is_in_array = { ruling_party = 21 } } }
				set_temp_variable = { rul_party_temp = 21 }
				set_temp_variable = { enable_elections = 1 }
				change_ruling_party_effect = yes

				}
			}
		}
	}
}
country_event = {
	id = cuba.53
	title = cuba.53.t
	desc = cuba.53.d
	picture = GFX_weapons_arms1
	is_triggered_only = yes
	option = {
		name = cuba.53.a
		log = "[GetDateText]: [This.GetName]: cuba.53.a"
		ROOT = {
			add_opinion_modifier = {
				target = CUB
				modifier = major_breach_of_trust
			}
		}
	}
}
country_event = {
	id = cuba.54
	title = cuba.54.t
	desc = cuba.54.d
	picture = GFX_weapons_arms1
	is_triggered_only = yes
	option = {
		name = cuba.54.a
		log = "[GetDateText]: [This.GetName]: cuba.54.a"
		CUB = { country_event = { id = cuba.55 days = 1 } }
	 }
	 option = {
		name = cuba.54.a
		log = "[GetDateText]: [This.GetName]: cuba.54.a"
		CUB = {
			add_state_core = 858
			transfer_state = 858
			}
	 }
}
country_event = {
	id = cuba.55
	title = cuba.55.t
	desc = cuba.55.d
	picture = GFX_weapons_arms1
	is_triggered_only = yes
	option = {
		name = cuba.55.a
		log = "[GetDateText]: [This.GetName]: cuba.55.a"
		CUB = {
			create_wargoal = {
				target = JAM
				type = annex_everything
			}
		}
	}
}
country_event = {
	id = cuba.56
	title = cuba.56.t
	desc = cuba.56.d
	picture = GFX_cargo_ship
	is_triggered_only = yes
	option = {
		name = cuba.56.a
		log = "[GetDateText]: [This.GetName]: cuba.56.a"
		ROOT = {
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
		}
		CUB = {
			one_random_infrastructure = yes
		}
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = CUB }
		change_influence_percentage = yes
	 }
	 option = {
		name = cuba.56.b
		log = "[GetDateText]: [This.GetName]: cuba.56.b"
	 }
}
country_event = {
	id = cuba.57
	title = cuba.57.t
	desc = cuba.57.d
	picture = GFX_cargo_ship
	is_triggered_only = yes
	option = {
		name = cuba.57.a
		log = "[GetDateText]: [This.GetName]: cuba.57.a"
		set_temp_variable = { receiver_nation = ROOT.id }
		set_temp_variable = { sender_nation = CUB.id }
		set_improved_trade_agreement = yes
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = CUB }
		change_influence_percentage = yes
	 }
	 option = {
		name = cuba.57.b
		log = "[GetDateText]: [This.GetName]: cuba.57.b"
	 }
}
news_event = {
	id = cuba_news.59
	title = cuba_news.59.t
	desc = cuba_news.59.d
	picture = GFX_Obama_news_cuba
	is_triggered_only = yes
	option = {
		name = cuba_news.59.a
		log = "[GetDateText]: [This.GetName]: cuba_news.59.a"
	 }
}
country_event = {
	id = cuba.58
	title = cuba.58.t
	desc = cuba.58.d
	picture = GFX_computer
	is_triggered_only = yes
	option = {
		name = cuba.58.a
		log = "[GetDateText]: [This.GetName]: cuba.58.a"
		add_ideas = CUB_prepare_for_worst_idea
	 }
}
news_event = {
	id = cuba_news.60
	title = cuba_news.60.t
	desc = cuba_news.60.d
	picture = GFX_putin_cuba_news
	is_triggered_only = yes
	option = {
		name = cuba_news.60.a
		log = "[GetDateText]: [This.GetName]:cuba_news.60.a"
	 }
}
news_event = {
	id = cuba_news.61
	title = cuba_news.61.t
	desc = cuba_news.61.d
	picture = GFX_miguel_diaz_canel
	is_triggered_only = yes
	option = {
		name = cuba_news.61.a
		log = "[GetDateText]: [This.GetName]:cuba_news.61.a"
	 }
}
news_event = {
	id = cuba_news.62
	title = cuba_news.62.t
	desc = cuba_news.62.d
	picture = GFX_cuban_prisons_event
	is_triggered_only = yes
	option = {
		name = cuba_news.62.a
		log = "[GetDateText]: [This.GetName]:cuba_news.62.a"
	 }
}
country_event = {
	id = cuba.63
	title = cuba.63.t
	desc = cuba.63.d
	picture = GFX_USA_generic
	is_triggered_only = yes
	option = {
		name = cuba.63.a
		log = "[GetDateText]: [This.GetName]: cuba.63.a"
		USA = {  diplomatic_relation = {
			country = CUB
			relation = guarantee
			active = yes
		} }
		give_military_access = USA
		CUB = { country_event = { id = cuba.64 days = 1 } }
	 }
	 option = {
		name = cuba.63.b
		log = "[GetDateText]: [This.GetName]: cuba.63.b"
		CUB = { country_event = { id = cuba.65 days = 1 } }
	 }
}
country_event = {
	id = cuba.64
	title = cuba.64.t
	desc = cuba.64.d
	picture = GFX_USA_generic
	is_triggered_only = yes
	option = {
		name = cuba.64.a
		log = "[GetDateText]: [This.GetName]: cuba.64.a"
	 }
}
country_event = {
	id = cuba.65
	title = cuba.65.t
	desc = cuba.65.d
	picture = GFX_USA_generic
	is_triggered_only = yes
	option = {
		name = cuba.65.a
		log = "[GetDateText]: [This.GetName]: cuba.65.a"
	 }
}
country_event = {
	id = cuba.66
	title = cuba.66.t
	desc = cuba.66.d
	picture = GFX_handshake_black
	is_triggered_only = yes

	option = {
		name = cuba.66.a
		log = "[GetDateText]: [This.GetName]: cuba.66.a executed"
		USA = {
			clr_country_flag = blockade_cuba_yes
		}
		CUB = {
			remove_ideas = CUB_idea_blockade1
		}
		ai_chance = {
			base = 100
		}
		CUB = { country_event = { id = cuba.67 days = 1 } }
	}
	option = {
		name = cuba.66.a
		log = "[GetDateText]: [This.GetName]: cuba.66.a executed"
		CUB = { country_event = { id = cuba.68 days = 1 } }

		ai_chance = {
			base = 1
		}
	}
}
country_event = {
	id = cuba.67
	title = cuba.67.t
	desc = cuba.67.d
	picture = GFX_handshake_black
	is_triggered_only = yes

	option = {
		name = cuba.67.a
		log = "[GetDateText]: [This.GetName]: cuba.66.a executed"

	}
}
country_event = {
	id = cuba.68
	title = cuba.68.t
	desc = cuba.68.d
	picture = GFX_declined_offer_no
	is_triggered_only = yes

	option = {
		name = cuba.68.a
		log = "[GetDateText]: [This.GetName]: cuba.66.a executed"
	}
}
country_event = {
	id = cuba.72
	title = cuba.72.t
	desc = cuba.72.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = cuba.72.a
		log = "[GetDateText]: [This.GetName]: cuba.72.a"
		give_military_access = SOV
		SOV = {  diplomatic_relation = {
			country = CUB
			relation = guarantee
			active = yes
		} }
		CUB = { country_event = { id = cuba.73 days = 1 } }
	 }
	 option = {
		name = cuba.72.b
		log = "[GetDateText]: [This.GetName]: cuba.72.b"
		CUB = { country_event = { id = cuba.74 days = 1 } }
	 }
}
country_event = {
	id = cuba.73
	title = cuba.73.t
	desc = cuba.73.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = cuba.73.a
		log = "[GetDateText]: [This.GetName]: cuba.73.a"
	 }
}
country_event = {
	id = cuba.74
	title = cuba.74.t
	desc = cuba.74.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = cuba.74.a
		log = "[GetDateText]: [This.GetName]: cuba.74.a"
	 }
}

