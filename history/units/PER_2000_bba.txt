﻿instant_effect = {

	#Aircraft

	add_equipment_to_stockpile = {
		variant_name = "F-5E Tiger II"
		type = small_plane_airframe_1
		amount = 60
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "F-5 Freedom Fighter"
		type = small_plane_airframe_1
		amount = 20
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "J-7II"
		type = small_plane_strike_airframe_1
		amount = 24
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-29 Fulcrum"
		type = small_plane_airframe_2
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "HESA F-14A Tomcat"
		type = cv_medium_plane_fighter_airframe_1
		amount = 43
		producer = PER
	}
	add_equipment_to_stockpile = {
		variant_name = "F-4E Phantom II"
		type = medium_plane_airframe_1
		amount = 81
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-24M"
		type = medium_plane_airframe_2
		amount = 36
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_cas_airframe_2
		variant_name = "Su-25"
		amount = 7
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_cas_airframe_1
		amount = 20
		producer = SOV
		variant_name = "Su-17M4"
	}
	add_equipment_to_stockpile = {
		variant_name = "P-3 Orion"
		type = large_plane_maritime_patrol_airframe_1
		amount = 5
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 30
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_1 #Mirage F1
		amount = 24
		variant_name = "Mirage F1"
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "Dornier 228"
		type = large_plane_air_transport_airframe_1
		amount = 5
		producer = RAJ
	}
	add_equipment_to_stockpile = {
		variant_name = "Pilatus PC-7"
		type = small_plane_strike_airframe_1
		amount = 50
		producer = SWI
	}
	add_equipment_to_stockpile = {
		variant_name = "Shaanxi Y-7"
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "Il-76"
		type = large_plane_air_transport_airframe_2
		amount = 9
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Il-76MD"
		type = large_plane_air_transport_airframe_2
		amount = 3
		producer = SOV
	}
	# Missile OOB
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 300
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 300
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_1
			amount = 22
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_2
			amount = 10
		}
		add_equipment_to_stockpile = {
			type = ballistic_missile_equipment_1
			amount = 10
		}
		add_equipment_to_stockpile = {
			type = ballistic_missile_equipment_2
			amount = 50
		}
	}
}