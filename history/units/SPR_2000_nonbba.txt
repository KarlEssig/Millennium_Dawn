﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Eurocopter AS532 Cougar
		amount = 23
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 93
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 2
		producer = USA
		#version_name = "Sea King HU5"
	}
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #BAE Sea Harrier
		amount = 9
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 10
		producer = USA
	}
	#11SH-3D (8 -H ASW,
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Sikorsky UH-60 Black Hawk
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter3 #F/A-18 Hornet
		amount = 90
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5 Freedom Fighter
		amount = 35
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #Mirage F1
		amount = 65
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = naval_plane1 #P-3 Orion
		amount = 65
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane2 # C-212
		amount = 78
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #CASA C-101
		amount = 78
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Eurocopter AS332 Super Puma
		amount = 78
		producer = FRA
	}

	# Missile OOB
	if = {
		limit = {
			has_dlc	= "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 512
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 400
		}
	}
}