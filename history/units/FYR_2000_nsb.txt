division_template = {
	name = "Mekhanizirana Peshadiska Brigada"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }

		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }

		Arty_Bat = { x = 2 y = 0 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Peshadiska Brigada"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		Arty_Battery = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Bataljon Za Specijalni Operacii"
	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
	priority = 2
}

units = {
	division = {
		name = "1. Peshadiska Brigada"
		location = 3882
		division_template = "Mekhanizirana Peshadiska Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons = { amount = 550 owner = FYR creator = SER } }
		force_equipment_variants = { command_control_equipment = { amount = 136 owner = FYR creator = SER } }
		force_equipment_variants = { Anti_tank_1 = { amount = 24 owner = FYR creator = SOV } }
		force_equipment_variants = { Anti_Air_1 = { amount = 16 owner = FYR creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 12 owner = FYR creator = SER } }
		force_equipment_variants = { util_vehicle_0 = { amount = 40 owner = FYR creator = USA } }
		force_equipment_variants = { light_tank_hull_0 = { amount = 10 owner = SER creator = SOV version_name = "BRDM-2" } }
		force_equipment_variants = { apc_hull_0 = { amount = 56 owner = SER creator = SOV version_name = "BTR-70" } }
		force_equipment_variants = { artillery_0 = { amount = 6 owner = FYR creator = USA version_name = "M101" } }
		force_equipment_variants = { mbt_hull_0 = { amount = 84 owner = FYR creator = SOV version_name = "T-55" } }
		force_equipment_variants = { spart_hull_0 = { amount = 11 owner = FYR creator = SER version_name = "M-63" } }
	}

	division = {
		name = "2. Peshadiska Brigada"
		location = 3882
		division_template = "Peshadiska Brigada"
		start_experience_factor = 0.3
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons = { amount = 842 owner = FYR creator = SER } }
		force_equipment_variants = { command_control_equipment = { amount = 110 owner = FYR creator = SER } }
		force_equipment_variants = { Anti_tank_0 = { amount = 40 owner = FYR creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 27 owner = FYR creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 2 owner = FYR creator = SER } }
		force_equipment_variants = { util_vehicle_0 = { amount = 41 owner = FYR creator = SER } }
		force_equipment_variants = { artillery_0 = { amount = 6 owner = FYR creator = SER } }
	}

	division = {
		name = "3. Peshadiska Brigada"
		location = 3882
		division_template = "Peshadiska Brigada"
		start_experience_factor = 0.3
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons = { amount = 842 owner = FYR creator = SER } }
		force_equipment_variants = { command_control_equipment = { amount = 110 owner = FYR creator = SER } }
		force_equipment_variants = { Anti_tank_0 = { amount = 40 owner = FYR creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 27 owner = FYR creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 2 owner = FYR creator = SER } }
		force_equipment_variants = { util_vehicle_1 = { amount = 30 owner = FYR creator = SER } }
		force_equipment_variants = { artillery_0 = { amount = 6 owner = FYR creator = SER } }
	}

	division = {
		name = "Bataljon Za Specijalni Operacii"
		location = 3882
		division_template = "Bataljon Za Specijalni Operacii"
		start_experience_factor = 0.2
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons3 = { amount = 250 owner = FYR creator = USA } }
		force_equipment_variants = { command_control_equipment2 = { amount = 30 owner = FYR creator = USA } }
		force_equipment_variants = { Anti_tank_1 = { amount = 40 owner = FYR creator = FRA } }
		force_equipment_variants = { Anti_Air_1 = { amount = 27 owner = FYR creator = SOV } }
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 12
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 10
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 12
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "Leonidas 2"
		amount = 10
		producer = GRE
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113"
		amount = 30
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 30
		producer = SER
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 6
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1		#Mi-17
		amount = 4
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1		#Igla
		amount = 30
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0		#Strela-2
		amount = 30
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 200
		producer = SOV
	}
}