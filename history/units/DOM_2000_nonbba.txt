instant_effect = {

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		amount = 8
		#variant_name = "A-37 Dragonfly"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1		#in place of T-34, T-41, T-35
		amount = 17
		#variant_name = "T-38C Talon"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = transport_plane1	#in place of C-47
		amount = 4
		#variant_name = "C-130 Hercules"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = transport_plane1
		amount = 2
		#variant_name = "C-212 Aviocar"
		producer = SPR
	}
}