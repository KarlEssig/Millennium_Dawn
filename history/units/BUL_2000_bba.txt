﻿instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_cas_airframe_2
		variant_name = "Su-25"
		amount = 39
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = medium_plane_airframe_1		 #Mig-23
		amount = 20
		producer = SOV
		variant_name = "MiG-23"
	}
	
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_1		 #Mig-23
		amount = 10
		producer = SOV
		variant_name = "MiG-23 ML/MLD"
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1		 #Mig-21
		amount = 81
		producer = SOV
		variant_name = "MiG-21 Bis"
	}

	add_equipment_to_stockpile = {
		type = small_plane_airframe_2 		#MiG-29
		amount = 21
		producer = SOV
		variant_name = "MiG-29 Fulcrum"
	}

	add_equipment_to_stockpile = {
		type = small_plane_cas_airframe_1	#Su-22
		amount = 21
		producer = SOV
		variant_name = "Su-17M4"
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1 	#An-26
		amount = 7
		producer = UKR
		variant_name = "An-26"
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1 	#Let L-410
		amount = 6
		producer = CZE
		variant_name = "Let L-410"
	}
}