division_template = {
	name = "Mekhanizirana Brigada"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 3 }

		SP_Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}

	support = {
		Mech_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		SP_AA_Battery = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Leka Pekhotna Brigada"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }

		Arty_Bat = { x = 1 y = 0 }
		Arty_Bat = { x = 1 y = 1 }
	}

	support = {
		Mech_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Tankova Brigada"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 0 y = 4 }

		SP_Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}

	support = {
		Mech_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		SP_AA_Battery = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Rezervna Brigada"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }

		Mech_Inf_Bat = { x = 1 y = 0 }

		Arty_Bat = { x = 2 y = 0 }
		Arty_Bat = { x = 2 y = 1 }
	}

	priority = 0
}
division_template = {
	name = "Spetsialna Brigada"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
	}

	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		helicopter_combat_service_support = { x = 0 y = 1 }
	}

	priority = 2
}
division_template = {
	name = "Parashuten Polk"

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }

		attack_helo_bat = { x = 1 y = 0 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		L_Recce_Comp = { x = 0 y = 1 }
		helicopter_combat_service_support = { x = 0 y = 2 }
	}

	priority = 2
}
division_template = {
	name = "Planinski Pekhoten Polk"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		L_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Broniran Razuznavatelen Polk"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		L_arm_Bat = { x = 0 y = 1 }
		attack_helo_bat = { x = 0 y = 2 }
	}

	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

units = {
	#1 Corps
	division = {
		name = "9-ta Tankova Brigada"
		location = 949 #Sofia
		division_template = "Tankova Brigada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 543 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 157 owner = BUL creator = SOV } }
		force_equipment_variants = { util_vehicle_1 = { amount = 40 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 14 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 28 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 19 owner = BUL creator = SOV } }
		force_equipment_variants = { mbt_hull_1 = { amount = 126 owner = BUL creator = SOV version_name = "T-72A" } }
		force_equipment_variants = { apc_hull_0 = { amount = 20 owner = BUL creator = SOV version_name = "MT-LB" } }
		force_equipment_variants = { ifv_hull_0 = { amount = 112 owner = BUL creator = BUL version_name = "BMP-23" } }
		force_equipment_variants = { spaa_hull_0 = { amount = 6 owner = BUL creator = SOV version_name = "9K33 Osa" } }
		force_equipment_variants = { spart_hull_0 = { amount = 36 owner = BUL creator = SOV version_name = "2S1 Gvozdika" } }
	}
	division = {
		name = "101-iy Planinski Pekhoten Polk"
		location = 11813 #Smolin
		division_template = "Planinski Pekhoten Polk"
		start_experience_factor = 0.4
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 623 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 90 owner = BUL creator = SOV } }
		force_equipment_variants = { util_vehicle_1 = { amount = 20 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 28 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 18 owner = BUL creator = SOV } }
	}
	
	division = {
		name = "3-ta Rezervna Brigada"
		location = 6857 #Blagoevgrad
		division_template = "Rezervna Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons = { amount = 510 owner = BUL creator = SOV } }
		force_equipment_variants = { command_control_equipment1 = { amount = 55 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 6 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_0 = { amount = 24 owner = BUL creator = BUL } }
		force_equipment_variants = { Anti_Air_0 = { amount = 16 owner = BUL creator = SOV } }
		force_equipment_variants = { apc_hull_0 = { amount = 60 owner = BUL creator = SOV version_name = "MT-LB" } }
		force_equipment_variants = { artillery_0 = { amount = 36 owner = BUL creator = SOV version_name = "D-20" } }
	}
	division = {
		name = "8-ma Rezervna Brigada"
		location = 793 #Vratsa
		division_template = "Rezervna Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons = { amount = 510 owner = BUL creator = SOV } }
		force_equipment_variants = { command_control_equipment1 = { amount = 55 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 6 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_0 = { amount = 24 owner = BUL creator = BUL } }
		force_equipment_variants = { Anti_Air_0 = { amount = 16 owner = BUL creator = SOV } }
		force_equipment_variants = { apc_hull_0 = { amount = 60 owner = BUL creator = SOV version_name = "MT-LB" } }
		force_equipment_variants = { artillery_0 = { amount = 36 owner = BUL creator = SOV version_name = "D-20" } }
	}
	#2 Coprs
	division = {
		name = "2-ra Leka Pekhotna Brigada"
		location = 6814	#Stara Zagora
		division_template = "Leka Pekhotna Brigada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 928 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 141 owner = BUL creator = SOV } }
		force_equipment_variants = { util_vehicle_1 = { amount = 200 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 40 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 27 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 2 owner = BUL creator = SOV } }
		force_equipment_variants = { apc_hull_0 = { amount = 20 owner = BUL creator = SOV version_name = "MT-LB" } }
		force_equipment_variants = { artillery_0 = { amount = 36 owner = BUL creator = SOV version_name = "D-20" } }
	}
	division = {
		name = "5-ta Mekhanizirana Brigada"
		location = 6842 #Kazanlak
		division_template = "Mekhanizirana Brigada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 897 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 152 owner = BUL creator = SOV } }
		force_equipment_variants = { util_vehicle_1 = { amount = 40 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 20 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 40 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 27 owner = BUL creator = SOV } }
		force_equipment_variants = { mbt_hull_1 = { amount = 42 owner = BUL creator = SOV version_name = "T-72A" } }
		force_equipment_variants = { apc_hull_0 = { amount = 200 owner = BUL creator = SOV version_name = "BTR-60" } }
		force_equipment_variants = { spaa_hull_0 = { amount = 6 owner = BUL creator = SOV version_name = "2K12 Kub" } }
		force_equipment_variants = { spart_hull_0 = { amount = 36 owner = BUL creator = SOV version_name = "2S1 Gvozdika" } }
	}
	division = {
		name = "61-ta Mekhanizirana Brigada"
		location = 6982	#Karlovo
		division_template = "Mekhanizirana Brigada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 897 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 152 owner = BUL creator = SOV } }
		force_equipment_variants = { util_vehicle_1 = { amount = 40 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 20 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 40 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 27 owner = BUL creator = SOV } }
		force_equipment_variants = { mbt_hull_1 = { amount = 42 owner = BUL creator = SOV version_name = "T-72A" } }
		force_equipment_variants = { apc_hull_0 = { amount = 200 owner = BUL creator = SOV version_name = "BTR-60" } }
		force_equipment_variants = { spaa_hull_0 = { amount = 6 owner = BUL creator = SOV version_name = "2K12 Kub" } }
		force_equipment_variants = { spart_hull_0 = { amount = 36 owner = BUL creator = SOV version_name = "2S1 Gvozdika" } }	
	}
	#3 Corps
	division = {
		name = "13-ta Tankova Brigada"
		location = 6967 #Sliven
		division_template = "Tankova Brigada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 543 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 157 owner = BUL creator = SOV } }
		force_equipment_variants = { util_vehicle_1 = { amount = 40 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 14 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 28 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 19 owner = BUL creator = SOV } }
		force_equipment_variants = { mbt_hull_1 = { amount = 126 owner = BUL creator = SOV version_name = "T-72A" } }
		force_equipment_variants = { apc_hull_0 = { amount = 20 owner = BUL creator = SOV version_name = "MT-LB" } }
		force_equipment_variants = { ifv_hull_0 = { amount = 120 owner = BUL creator = SOV version_name = "BMP-1" } }
		force_equipment_variants = { spaa_hull_0 = { amount = 6 owner = BUL creator = SOV version_name = "9K33 Osa" } }
		force_equipment_variants = { spart_hull_0 = { amount = 36 owner = BUL creator = SOV version_name = "2S1 Gvozdika" } }
	}
	division = {
		name = "34-iy Planinski Pekhoten Polk"
		location = 893 #Momchilgrad
		division_template = "Planinski Pekhoten Polk"
		start_experience_factor = 0.4
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 623 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 90 owner = BUL creator = SOV } }
		force_equipment_variants = { util_vehicle_1 = { amount = 20 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 28 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 18 owner = BUL creator = SOV } }
	}
	division = {
		name = "7-ma Rezervna Brigada"
		location = 9902 #Yambol
		division_template = "Rezervna Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons = { amount = 510 owner = BUL creator = SOV } }
		force_equipment_variants = { command_control_equipment1 = { amount = 55 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 6 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_0 = { amount = 24 owner = BUL creator = BUL } }
		force_equipment_variants = { Anti_Air_0 = { amount = 16 owner = BUL creator = SOV } }
		force_equipment_variants = { apc_hull_0 = { amount = 60 owner = BUL creator = SOV version_name = "MT-LB" } }
		force_equipment_variants = { artillery_0 = { amount = 36 owner = BUL creator = SOV version_name = "D-20" } }
	}
	division = {
		name = "17-ta Rezervna Brigada"
		location = 6814 #Haskovo
		division_template = "Rezervna Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons = { amount = 510 owner = BUL creator = SOV } }
		force_equipment_variants = { command_control_equipment1 = { amount = 55 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 6 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_0 = { amount = 24 owner = BUL creator = BUL } }
		force_equipment_variants = { Anti_Air_0 = { amount = 16 owner = BUL creator = SOV } }
		force_equipment_variants = { apc_hull_0 = { amount = 60 owner = BUL creator = SOV version_name = "MT-LB" } }
		force_equipment_variants = { artillery_0 = { amount = 36 owner = BUL creator = SOV version_name = "M-46" } }
	}
	division = {
		name = "18-ta Rezervna Brigada"
		location = 487 #Shumen
		division_template = "Rezervna Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons = { amount = 510 owner = BUL creator = SOV } }
		force_equipment_variants = { command_control_equipment1 = { amount = 55 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 6 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_0 = { amount = 24 owner = BUL creator = BUL } }
		force_equipment_variants = { Anti_Air_0 = { amount = 16 owner = BUL creator = SOV } }
		force_equipment_variants = { apc_hull_0 = { amount = 60 owner = BUL creator = SOV version_name = "MT-LB" } }
		force_equipment_variants = { artillery_0 = { amount = 36 owner = BUL creator = SOV version_name = "M-46" } }
	}

	#Other Units
	division = {
		name = "1-iy Broniran Razuznavatelen Polk"
		location = 6982 #Svoboda
		division_template = "Broniran Razuznavatelen Polk"
		start_experience_factor = 0.5
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 260 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 64 owner = BUL creator = SOV } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { amount = 6 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 12 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 8 owner = BUL creator = SOV } }
		force_equipment_variants = { apc_hull_0 = { amount = 60 owner = BUL creator = SOV version_name = "BTR-60" } }
		force_equipment_variants = { light_tank_hull_0 = { amount = 56 owner = BUL creator = SOV version_name = "BRDM-2" } }
		force_equipment_variants = { attack_helicopter_hull_0 = { amount = 18 owner = BUL creator = SOV version_name = "Mi-24V" } }
	}
	division = {
		name = "68-i Spetsialna Brigada"
		location = 6923	#Plovdiv
		division_template = "Spetsialna Brigada"
		start_experience_factor = 0.7
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 833 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 120 owner = BUL creator = SOV } }
		force_equipment_variants = { util_vehicle_1 = { amount = 20 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 40 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 26 owner = BUL creator = SOV } }
		force_equipment_variants = { transport_helicopter1 = { amount = 20 owner = BUL creator = SOV } }	
	}
	division = {
		name = "1-i Parashuten Razuznavatelen Polk"
		location = 6967 #Sliven
		division_template = "Parashuten Polk"
		start_experience_factor = 0.7
		start_equipment_factor = 0.1
		force_equipment_variants = { infantry_weapons2 = { amount = 623 owner = BUL creator = BUL } }
		force_equipment_variants = { command_control_equipment1 = { amount = 118 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_tank_1 = { amount = 28 owner = BUL creator = SOV } }
		force_equipment_variants = { Anti_Air_0 = { amount = 18 owner = BUL creator = SOV } }
		force_equipment_variants = { transport_helicopter1 = { amount = 20 owner = BUL creator = SOV } }
		force_equipment_variants = { attack_helicopter_hull_0 = { amount = 18 owner = BUL creator = SOV version_name = "Mi-24V" } }	
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = command_control_equipment1
		amount = 1420
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 4000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons2	 #AR-M1
		amount = 9000
		producer = BUL
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0		 #AT-5
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 400
		producer = BUL
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0		 #SA-7
		amount = 250
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		amount = 62
		variant_name = "D-20"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72A"
		amount = 97
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-55"
		amount = 1042
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 176
		producer = SOV
		variant_name = "BRDM-2"
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 377
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 653
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 549
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 222
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		variant_name = "2K11 Krug"
		amount = 27
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_1
		amount = 150
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 7
		producer = SOV
		variant_name = "Mil Mi-24V"
	}
}