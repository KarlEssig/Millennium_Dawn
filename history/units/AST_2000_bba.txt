﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_1
		amount = 49
		producer = USA
		variant_name = "F-111 Aardvark"
	}
	add_equipment_to_stockpile = {
		type = large_plane_maritime_patrol_airframe_1 #P-3C Orion
		amount = 22
		producer = USA
		variant_name = "P-3 Orion"
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_airframe_2
		amount = 71
		producer = USA
		variant_name = "F/A-18A/B Hornet"
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 17
		variant_name = "C-130 Hercules"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 12
		variant_name = "C-130J Super Hercules"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 14
		variant_name = "DHC-4 Caribou"
		producer = CAN
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 62
		producer = SWI
		variant_name = "Pilatus PC-9"
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 12
		producer = ENG
		variant_name = "BAE Systems Hawk"
	}
	# Missile OOB
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 150
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 100
		}
	}
}