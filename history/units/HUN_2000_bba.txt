﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Aero L-39"
		type = small_plane_strike_airframe_1
		amount = 20
		producer = CZE
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-21 Bis"
		type = small_plane_strike_airframe_1
		amount = 22
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-29 Fulcrum"
		type = small_plane_airframe_2
		amount = 27
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "An-26"
		type = large_plane_air_transport_airframe_1
		amount = 9
		producer = UKR
	}
}