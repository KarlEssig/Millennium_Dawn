units = {
	fleet = {
		name = "German Submarines"
		naval_base = 4076
		task_force = {
			name = "German Submarines"
			location = 4076
			ship = { name = "U-40" definition = attack_submarine start_experience_factor = 0.65 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER creator = GER } } }
			ship = { name = "U-41" definition = attack_submarine start_experience_factor = 0.65 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER creator = GER } } }
			ship = { name = "U-42" definition = attack_submarine start_experience_factor = 0.65 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER creator = GER } } }
			ship = { name = "U-43" definition = attack_submarine start_experience_factor = 0.65 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER creator = GER } } }
			ship = { name = "U-44" definition = attack_submarine start_experience_factor = 0.65 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER creator = GER } } }
		}
	}
}