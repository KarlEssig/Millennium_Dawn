﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "OV-10 Bronco"
		type = small_plane_strike_airframe_1
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "A-4 Skyhawk"
		type = cv_small_plane_strike_airframe_1
		amount = 21
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "BAE Hawk T1"
		type = small_plane_strike_airframe_1
		amount = 22
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Hawk 200"
		type = small_plane_strike_airframe_1
		amount = 32
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "F-5E Tiger II"
		type = small_plane_airframe_1
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "F-16C Blk 40/42"
		type = small_plane_strike_airframe_2
		amount = 10
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "CASA/IPTN CN-235"
		type = large_plane_air_transport_airframe_1
		amount = 26
		producer = IND
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 20
		variant_name = "C-130 Hercules"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 10
		variant_name = "C-212 Aviocar"
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		amount = 2
		variant_name = "DHC-4 Caribou"
		producer = CAN
	}
	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		variant_name = "GAF Nomad"
		amount = 20
		producer = AST
	}
}