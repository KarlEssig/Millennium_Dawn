division_template = {
	name = "Ukranian Volunteer Unit"
	is_locked = yes

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
	}
}

units = {
	division = {
		location = 525
		name = "1st UAD Brigade"
		division_template = "Ukranian Volunteer Unit"
		start_experience_factor = 0.3
		start_equipment_factor = 1
	}
	division = {
		location = 525
		name = "2nd UAD Brigade"
		division_template = "Ukranian Volunteer Unit"
		start_experience_factor = 0.3
		start_equipment_factor = 1
	}
}