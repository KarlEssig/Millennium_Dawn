﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 20
		variant_name = "F-16C Blk 40/42"
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Dassault Alpha Jet"
		type = small_plane_strike_airframe_1
		amount = 40
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-212 Aviocar"
		type = large_plane_air_transport_airframe_1
		amount = 24
		producer = SPR
	}
	add_equipment_to_stockpile = {
		variant_name = "Socata TB 30 Epsilon"
		type = small_plane_strike_airframe_1
		amount = 16
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "P-3 Orion"
		type = large_plane_maritime_patrol_airframe_1
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-316 Alouette III
		amount = 28
		producer = FRA
	}
}