instant_effect = {

	add_equipment_to_stockpile = {
		#variant_name = "MiG-21 Bis"
		type = L_Strike_fighter1
		amount = 9
		producer = SOV
	}
	add_equipment_to_stockpile = {
		#variant_name = "An-26"
		type = transport_plane1
		amount = 15
		producer = UKR
	}
	add_equipment_to_stockpile = {
		#variant_name = "An-32"
		type = transport_plane1
		amount = 10
		producer = UKR
	}
}