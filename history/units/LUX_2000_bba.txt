instant_effect = {

	add_equipment_to_stockpile = {
		variant_name = "E-3 Sentry"
		type = large_plane_awacs_airframe_2
		amount = 1
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = USA
	}
}