﻿division_template = {
	name = "Firqa Moudar'a"			#Armoured Division

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }

		armor_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 1 y = 1 }
		armor_Bat = { x = 1 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 3 }

		armor_Bat = { x = 2 y = 0 }
		Mech_Inf_Bat = { x = 2 y = 1 }
		Mech_Inf_Bat = { x = 2 y = 2 }
		L_arm_Bat = { x = 2 y = 3 }

		SP_Arty_Bat = { x = 3 y = 0 }
		SP_Arty_Bat = { x = 3 y = 1 }
		SP_Arty_Bat = { x = 3 y = 2 }
		Mech_Inf_Bat = { x = 3 y = 3 }
	}
	support = {
		SP_AA_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Firqa Mushat Mechaniqui"			#Mechanised Division

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 0 y = 3 }

		armor_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mech_Inf_Bat = { x = 1 y = 2 }

		armor_Bat = { x = 2 y = 0 }
		Mech_Inf_Bat = { x = 2 y = 1 }
		Mech_Inf_Bat = { x = 2 y = 2 }
		Mech_Inf_Bat = { x = 2 y = 3 }

		Arty_Bat = { x = 3 y = 0 }
		Arty_Bat = { x = 3 y = 1 }
		Arty_Bat = { x = 3 y = 2 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Liwa Mushat Mechaniqui"		#Mechanised Brigade

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }

		Arty_Bat = { x = 1 y = 0 }
	}
}

division_template = {
	name = "Liwa Moudar'a"		#Armoured Brigade

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }

		Arty_Bat = { x = 1 y = 0 }
	}
}

division_template = {
	name = "Liwa Mushat"		#Infantry Brigade

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }

		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 2 }

		Arty_Bat = { x = 2 y = 0 }
	}
}

division_template = {
	name = "Liwa Alhawa Almahmul"		#Airmobile Brigade

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
		L_Air_assault_Bat = { x = 0 y = 2 }
		Arm_Air_assault_Bat = { x = 0 y = 3 }
	}

	priority = 2
}

division_template = {
	name = "Liwa Midali"		#Airborne Brigade

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
		Mech_Air_Inf_Bat = { x = 0 y = 3 }
	}

	priority = 2
}

division_template = {
	name = "Fawj Alquwwat Alkhasa"		#Special Forces Regiment

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
	}

	priority = 2
}

units = {
	#First Field Army
	##1st Corps
	division = {
		name = "1. Firqa Moudar'a Alharas Aljumhuriu"
		location = 10073		#Heliopolis
		division_template = "Firqa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "24. Liwa Mechaniqui Mustaqilun"
		location = 11964		#Heliopolis
		division_template = "Liwa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "135. Fawj Alquwwat Alkhasa"
		location = 10073		#Heliopolis
		division_template = "Fawj Alquwwat Alkhasa"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	##2nd Corps
	division = {
		name = "2. Firqa Mechaniqui"
		location = 4076		#Alexandria
		division_template = "Firqa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "18. Liwa Moudar'a Mustaqilun"
		location = 4145		#Alexandria
		division_template = "Liwa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "218. Liwa Mushat Mustaqilun"
		location = 7164		#Alexandria
		division_template = "Liwa Mushat"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "129. Fawj Alquwwat Alkhasa"
		location = 4076		#Alexandria
		division_template = "Fawj Alquwwat Alkhasa"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	##3rd Corps
	division = {
		name = "3. Firqa Mechaniqui"
		location = 11974		#Assiut
		division_template = "Firqa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "36. Liwa Moudar'a Mustaqilun"
		location = 7144		#Assiut
		division_template = "Liwa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "222. Liwa Alhawa Almahmul"
		location = 11974		#Assiut
		division_template = "Liwa Alhawa Almahmul"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	#Second Field Army
	##1st Corps
	division = {
		name = "21. Firqa Moudar'a"
		location = 12049		#Port Said
		division_template = "Firqa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "7. Firqa Mechaniqui"
		location = 1155		#Port Said
		division_template = "Firqa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "414. Liwa Midali"
		location = 12049		#Port Said
		division_template = "Liwa Midali"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "117. Fawj Alquwwat Alkhasa"
		location = 12049		#Port Said
		division_template = "Fawj Alquwwat Alkhasa"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	##2nd Corps
	division = {
		name = "4. Firqa Moudar'a"
		location = 12049		#Ismaelia
		division_template = "Firqa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "7. Firqa Mechaniqui"
		location = 9947		#Ismaelia
		division_template = "Firqa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "123. Fawj Alquwwat Alkhasa"
		location = 12049		#Ismaelia
		division_template = "Fawj Alquwwat Alkhasa"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	##3rd Corps
	division = {
		name = "6. Firqa Moudar'a"
		location = 10005		#El Mansoura
		division_template = "Firqa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "19. Firqa Mechaniqui"
		location = 11964		#El Mansoura
		division_template = "Firqa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "219. Liwa Mushat Mustaqilun"
		location = 10073		#El Mansoura
		division_template = "Liwa Mushat"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "153. Fawj Alquwwat Alkhasa"
		location = 10005		#El Mansoura
		division_template = "Fawj Alquwwat Alkhasa"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	#Third Field Army
	##1st Corps
	division = {
		name = "9. Firqa Moudar'a"
		location = 12002		#Hurghada
		division_template = "Firqa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "23. Firqa Mechaniqui"
		location = 9957		#Hurghada
		division_template = "Firqa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "94. Liwa Mechaniqui Mustaqilun"
		location = 12889		#Hurghada
		division_template = "Liwa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "159. Fawj Alquwwat Alkhasa"
		location = 12002		#Hurghada
		division_template = "Fawj Alquwwat Alkhasa"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	##2nd Corps
	division = {
		name = "36. Firqa Mechaniqui"
		location = 4073		#Suez
		division_template = "Firqa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "44. Liwa Moudar'a Mustaqilun"
		location = 12004		#Suez
		division_template = "Liwa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "141. Fawj Alquwwat Alkhasa"
		location = 4073		#Suez
		division_template = "Fawj Alquwwat Alkhasa"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	##3rd Corps
	division = {
		name = "16. Firqa Mechaniqui"
		location = 7011		#Cairo
		division_template = "Firqa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "82. Liwa Moudar'a Mustaqilun"
		location = 3996		#Cairo
		division_template = "Liwa Moudar'a"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "110. Liwa Mechaniqui Mustaqilun"
		location = 4055		#Cairo
		division_template = "Liwa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "111. Liwa Mechaniqui Mustaqilun"
		location = 1068		#Cairo
		division_template = "Liwa Mushat Mechaniqui"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "147. Fawj Alquwwat Alkhasa"
		location = 7011		#Cairo
		division_template = "Fawj Alquwwat Alkhasa"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 5500
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons		 #Maadi
		amount = 27000
		producer = EGY
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2		 #Beretta AR70/90
		amount = 3000
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons1		 #M16
		amount = 3000
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons
		variant_name = "SKS"
		producer = SOV
		amount = 5000
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons
		variant_name = "Mosin"
		producer = SOV
		amount = 500
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1	 	#MILAN
		amount = 220
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_0	 	#AT-3
		amount = 1400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1	 	#Swingfire
		amount = 260
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0	 	#TOW-I
		amount = 268
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1	 	#TOW-II
		amount = 262
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0	 	#Ayn al Saqr
		amount = 2500
		producer = EGY
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1	 	#Stinger
		amount = 1800
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0	 	#Igla
		amount = 600
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 36
		producer = FRA
		variant_name = "Crotale"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 40
		producer = SOV
		variant_name = "ZSU-57-2"
	}

	#Vehicles
	#MBT
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "M1A1 Abrams"
		amount = 555
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "M60A1"
		amount = 400
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "M60A3"
		amount = 1300
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 895
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "Ramses II"
		amount = 260
		producer = EGY
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-62"
		amount = 550
		producer = SOV
	}

	#armored vehicles
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 220
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "YPR-765"
		amount = 375
		producer = HOL
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113A2"
		amount = 2320
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-50"
		amount = 1075
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "Walid"
		amount = 600
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "Fahd"
		amount = 165
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 300
		producer = SOV
		variant_name = "BRDM-2"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 112
		producer = USA
		variant_name = "V150 Cadillac Commando"
	}

	#Utility
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "UAZ-452"
		producer = SOV
		amount = 300
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1
		variant_name = "UAZ-469"
		producer = SOV
		amount = 300
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2	 	#Casspir
		amount = 150
		producer = SAF
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2	 	#Humvee
		amount = 375
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_3	 	#Humvee ECV
		amount = 400
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2	 	#Iveco VM 90
		amount = 200	#
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2	 	#G-Class
		amount = 1000	#3910
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1 	#M151
		amount = 1400	#4750
		producer = USA
	}

	#Artillery
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "2S3 Akatsiya"
		producer = SOV
		amount = 72
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M109A2"
		amount = 175
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_0	 		#D-30 and similar
		amount = 971
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 96
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "SAQR-18/36"
		amount = 60
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "RM-51"
		amount = 36
		producer = CZE
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_2
		amount = 36
		producer = USA
		variant_name = "AH-64A Apache"
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 78
		producer = FRA
		variant_name = "SA-342 Gazelle"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1
		amount = 33
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1
		amount = 66
		producer = SOV
	}
}