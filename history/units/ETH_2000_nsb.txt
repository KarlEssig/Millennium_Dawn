﻿division_template = {
	name = "Reinforced Mechanised Brigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		Arm_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
		SP_Arty_Battery = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Infantry Division"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }

		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
		Arty_Bat = { x = 1 y = 2 }

		Mot_Inf_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 1 }
		Arty_Bat = { x = 2 y = 2 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
	}
}

units = {
	#1st Corps
	division = {
		name = "11th Reinforced Mechanised Brigade"
		location = 518		#
		division_template = "Reinforced Mechanised Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "12th Army Division"
		location = 5010	   # Addis Ababa
		division_template = "Infantry Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "13th Army Division"
		location = 510		#
		division_template = "Infantry Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	#2nd Corps
	division = {
		name = "21st Reinforced Mechanised Brigade"
		location = 2072		#
		division_template = "Reinforced Mechanised Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "22nd Army Division"
		location = 533		#
		division_template = "Infantry Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "23rd Army Division"
		location = 520		#
		division_template = "Infantry Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	#3rd Corps
	division = {
		name = "31st Reinforced Mechanised Brigade"
		location = 208		#
		division_template = "Reinforced Mechanised Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "32nd Army Division"
		location = 338		#
		division_template = "Infantry Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "33rd Army Division"
		location = 8019		#
		division_template = "Infantry Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 1400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		#AKM
		amount = 12000
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons
		variant_name = "SKS"
		producer = SOV
		amount = 1000
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		#G3
		amount = 1000
		producer = GER
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		#Vz. 58
		amount = 1000
		producer = CZE
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-62"
		amount = 60
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BMP-1"
		amount = 360
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = artillery_0		#D-30
		amount = 170
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 25
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 25
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_2
		variant_name = "2S19 Msta"
		amount = 25
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 60
		producer = SOV
		variant_name = "ZSU-23-4 Shilka"
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0		#Strela
		amount = 500
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1		#Spiral
		#version_name = "AT-6 Spiral"
		amount = 300
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0		#Malyutka
		amount = 800
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0		#ZIL trucks
		amount = 2862
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1		#Mi-8
		amount = 22
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 16
		producer = SOV
		variant_name = "Mi-24D"
	}
}