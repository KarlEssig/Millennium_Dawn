﻿division_template = {
	name = "Infantry Brigade (V1)"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		Arty_Battery = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Infantry Brigade (V2)"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }

		Mech_Inf_Bat = { x = 1 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		Arty_Battery = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}

units = {

	#Irish Defence Forces
	#https://en.wikipedia.org/wiki/Irish_Army

	#1st Brigade
	division = {
		name = "1st Brigade"
		location = 7394
		division_template = "Infantry Brigade (V2)"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
		force_equipment_variants = { infantry_weapons1 = { amount = 824 owner = IRE creator = AUS } }
		force_equipment_variants = { command_control_equipment = { amount = 121 owner = IRE creator = ENG } }
		force_equipment_variants = { util_vehicle_0 = { amount = 160 owner = IRE creator = GER } }
		force_equipment_variants = { Anti_tank_1 = { amount = 36 owner = IRE creator = FRA } }
		force_equipment_variants = { Anti_Air_1 = { amount = 24 owner = IRE creator = SWE } }
		force_equipment_variants = { Heavy_Anti_tank_1 = { amount = 6 owner = IRE creator = FRA } }
		force_equipment_variants = { artillery_1 = { amount = 6 owner = IRE creator = ENG } }
		force_equipment_variants = { apc_hull_0 = { amount = 47 owner = IRE creator = FRA version_name = "M3 Panhard" } }
		force_equipment_variants = { light_tank_hull_1 = { amount = 14 owner = IRE creator = ENG version_name = "FV101 Scorpion" } }
	}
	#1st Brigade
	division = {
		name = "2nd Brigade"
		location = 13939
		division_template = "Infantry Brigade (V1)"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
		force_equipment_variants = { infantry_weapons1 = { amount = 831 owner = IRE creator = AUS } }
		force_equipment_variants = { command_control_equipment = { amount = 121 owner = IRE creator = ENG } }
		force_equipment_variants = { util_vehicle_0 = { amount = 220 owner = IRE creator = SWE } }
		force_equipment_variants = { Anti_tank_1 = { amount = 36 owner = IRE creator = FRA } }
		force_equipment_variants = { Anti_Air_1 = { amount = 24 owner = IRE creator = SWE } }
		force_equipment_variants = { artillery_1 = { amount = 6 owner = IRE creator = ENG } }
		force_equipment_variants = { light_tank_hull_0 = { amount = 14 owner = IRE creator = FRA version_name = "Panhard AML 90" } }
	}
	division = {
		name = "4th Brigade"
		location = 13938
		division_template = "Infantry Brigade (V1)"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
		force_equipment_variants = { infantry_weapons1 = { amount = 831 owner = IRE creator = AUS } }
		force_equipment_variants = { command_control_equipment = { amount = 121 owner = IRE creator = ENG } }
		force_equipment_variants = { util_vehicle_0 = { amount = 220 owner = IRE creator = SWE } }
		force_equipment_variants = { Anti_tank_1 = { amount = 36 owner = IRE creator = FRA } }
		force_equipment_variants = { Anti_Air_1 = { amount = 24 owner = IRE creator = SWE } }
		force_equipment_variants = { artillery_1 = { amount = 6 owner = IRE creator = ENG } }
		force_equipment_variants = { light_tank_hull_0 = { amount = 14 owner = IRE creator = FRA version_name = "Panhard AML 90" } }
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1 #Steyr AUG
		amount = 200 #2784 deployed 
		producer = AUS
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 20 #303 deployed
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "M3 Panhard"
		amount = 0 #47 deployed
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "Timoney Mk 6"
		amount = 5
	}
	add_equipment_to_stockpile = {
		type = apc_hull_2
		variant_name = "XA-180"
		amount = 2
		producer = FIN
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 0 #14 deployed
		producer = ENG
		variant_name = "FV101 Scorpion"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 19 #28 deployed
		producer = FRA
		variant_name = "Panhard AML 90"
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0		#Swedish trucks
		amount = 0 #440 deployed
		producer = SWE
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0		#German trucks
		amount = 0 #220 deployed
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = artillery_1		#L119
		amount = 6 #6 deployed
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 0 #6 deployed
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 0 #120 deployed
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 0 #81 deployed
		producer = SWE
	}
}