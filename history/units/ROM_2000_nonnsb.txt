﻿division_template = {
	name = "Brigada de Infanterie"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		Mech_Recce_Comp = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Vanatori de Munte"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Brigada Blindata"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		SP_AA_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }
		Arty_Bat = { x = 1 y = 2 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		Mech_Recce_Comp = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Forte pentru Operatii Speciale"
	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
	}

	priority = 2
}

units = {
	division = {
		name = "Brigada 41 Transport 'Bobalna'"
		location = 9617	#Bucharest
		division_template = "Brigada Blindata"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Brigada 117 Sprijin Logistic 'Alexandru Polyzu'"
		location = 9617	#Craiova
		division_template = "Brigada de Infanterie"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Brigada 385 Artilerie de Munte 'Iancu de Hunedoara'"
		location = 11652 #Brasov
		division_template = "Vanatori de Munte"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Brigada 469 Sprijin 'Putna'"
		location = 657	#Constanta
		division_template = "Brigada Blindata"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Brigada 168 Sprijin 'Pontus Euxinus'"
		location = 9716	#Iasi
		division_template = "Brigada Blindata"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Brigada 400 Sprijin 'Feleacu'"
		location = 9701	#Focșani
		division_template = "Brigada de Infanterie"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Brigada 184 Aparare Antiaeriana 'Timis'"
		location = 9606	#Timișoara
		division_template = "Brigada de Infanterie"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Brigada 32 Survolare 'Mircea'"
		location = 9668 #Miercurea Ciuc
		division_template = "Vanatori de Munte"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Brigada 300 Sprijin 'Sarmis'"
		location = 11676
		division_template = "Brigada de Infanterie"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons2			 #Pusca Automata Model 1986
		amount = 7000
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 900
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0			 #CA-94
		amount = 230
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0			 #Malyutka
		amount = 100
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1			 #Konkurs
		amount = 45
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1			 #Fagot
		amount = 320
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter2	 #Aero L-39
		amount = 32
		producer = CZE
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 500
	}

	add_equipment_to_stockpile = {
		type = MBT_1	 #T-55
		amount = 717
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = MBT_2	 #TR-85
		amount = 314
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = MBT_1	 #TR-580
		amount = 227
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = APC_1 #BRDM-2
		amount = 4
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = IFV_3	 #MLI-84
		amount = 177
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = APC_3	 #TAB-77
		amount = 166
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = APC_1	 #TAB-71
		amount = 1259
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = APC_4	 #TAB Zimbru
		amount = 70
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = artillery_0		 #M81
		amount = 493
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = artillery_1		 #M85
		amount = 114
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = SP_arty_1 #Model 89 SPH
		amount = 42
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = SP_arty_0 #2S1 Gvozdika
		amount = 6
		producer = SOV
		#version_name = "2S1 Gvozdika"
	}
	add_equipment_to_stockpile = {
		type = SP_R_arty_0 #APR-40
		amount = 171
		producer = ROM
	}
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_0			 #SA-6
		amount = 64
		producer = SOV
	}
}