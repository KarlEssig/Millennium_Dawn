﻿division_template = {
	name = "Batalyon Dobrovolchev"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
	}

	priority = 0
}

division_template = {
	name = "Brigada Dobrovolchev"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
	}

	priority = 0
}

division_template = {
	name = "Pekhotnaya Brigada"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Motorizovannaya Brigada"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Batalyon Spetsnaza"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Mekhanizirovannaya Brigada"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Kazachya Brigada"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 1 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Tankovyi Batalyon"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
	}
}

units = {

	#Donbass People's Militia
	division = {
		name = "Balayon 'Sever'"
		location = 3421
		division_template = "Batalyon Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "1-y Slavyanskaya Brigada"
		location = 6474
		division_template = "Motorizovannaya Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "Brigada 'Vostok'"
		location = 6505
		division_template = "Pekhotnaya Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "Russkaya Pravoslavnaya Armiya"
		location = 6474
		division_template = "Brigada Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Shakhtyorskaya Diviziya"
		location = 6474
		division_template = "Brigada Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Brigada 'Kalmius'"
		location = 6505		#Debaltseve
		division_template = "Batalyon Dobrovolchev"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "Batalyon 'Voskhod'"
		location = 3421
		division_template = "Batalyon Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Brigada 'Pyatnashka'"
		location = 6474		#Marinka
		division_template = "Brigada Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Mariupolsko-Khinganskyi Morskaya Pekhota"
		location = 3421
		division_template = "Pekhotnaya Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}




}

instant_effect = {

	add_equipment_to_stockpile = {
		type = infantry_weapons		 #AKM
		amount = 1800
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1		 #AK-74
		amount = 2500
		producer = SOV
	}

	#add_equipment_to_stockpile = {
		#type = infantry_weapons2		 #AS Val
		#amount = 1800
		#producer = SOV
	#}

	add_equipment_to_stockpile = {
		type = infantry_weapons2		 #AK-74M
		amount = 1800
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1		 #Fagot
		amount = 32
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_2		 #Metis
		amount = 27
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2		 #Metis
		amount = 23
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1		 #Spiral
		#version_name = "AT-6 Spiral"
		amount = 43
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = MBT_1		 #T-55
		amount = 1
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = MBT_1		 #T-64
		#version_name = "T-64"
		amount = 38
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = MBT_2		 #T-72B
		#version_name = "T-72B"
		amount = 35
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = IFV_1		 #BMP-1
		amount = 22
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = IFV_3		 #BMP-2
		amount = 89
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = APC_1		 #BTR-60
		amount = 3
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = APC_1		 #MT-LB
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = APC_3		 #BTR-70
		amount = 9
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = APC_4		 #BTR-80
		amount = 18
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = APC_5		 #BTR-4
		amount = 2
		producer = UKR
	}

	add_equipment_to_stockpile = {
		type = Rec_tank_0		 #BRDM-2
		amount = 8
		producer = UKR
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0		 #Ural 4320
		amount = 160
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_1		 #UAZ-469
		amount = 78
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_3		 #Vodnik
		amount = 68
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_4		 #BPM-97
		amount = 80
		producer = SOV
	}

	#add_equipment_to_stockpile = {
		#type = H_infantry_weapons		 #Various Soviet mortars
		#amount = 110
		#producer = SOV
	#}

	add_equipment_to_stockpile = {
		type = artillery_0		 #Various Soviet artys
		amount = 45
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_arty_0		 #2S1 Gvozdika
		#version_name = "2S1 Gvozdika"
		amount = 8
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_arty_0		 #2S5 Giatsint-S
		#version_name = "2S5 Giatsint-S"
		amount = 1
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_arty_0		 #2S7 Pion
		#version_name = "2S7 Pion"
		amount = 3
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_arty_0		 #2S3 Akatsiya
		amount = 8
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_arty_1		 #2S19 Msta
		amount = 3
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_R_arty_0		 #BM-21 Grad
		amount = 18
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_R_arty_1		 #BM-27 Uragan
		amount = 3
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_R_arty_1		 #TOS-1
		#version_name = "TOS-1"
		amount = 3
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = SP_R_arty_1		 #BM-30 Smerch
		#version_name = "BM-30 Smerch"
		amount = 3
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0		 #Strela
		amount = 33
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1		 #Igla
		amount = 36
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1		 #Grom
		amount = 35
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 800
		producer = SOV
	}
}
