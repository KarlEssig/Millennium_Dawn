instant_effect = {

	add_equipment_to_stockpile = {
		variant_name = "Piper PA-31T Cheyenne"
		type = small_plane_strike_airframe_1
		amount = 2
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-20 Gulfstream"
		type = large_plane_air_transport_airframe_1
		amount = 1
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Harbin Y-12"
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		variant_name = "BN-2 Defender"
		amount = 1
		producer = ENG
	}

}