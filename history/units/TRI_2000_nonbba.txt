instant_effect = {
	
	add_equipment_to_stockpile = {
		type = awacs_equipment_1
		variant_name = "C-26 Metroliner"
		amount = 2
		producer = USA
	}
}