units = {
	### Naval OOB ###
	fleet = {
		name = "Croatian Navy"
		naval_base = 3924
		task_force = {
			name = "Croatian Navy"
			location = 3924
			ship = { name = "Kralj Petar Krešimir IV" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = CRO creator = CRO version_name = "Kralj Class" } }  }
			ship = { name = "Kralj Dmitar Zvonimir" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = CRO creator = CRO version_name = "Kralj Class" } }  }
		}
	}
}
