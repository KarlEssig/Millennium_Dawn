﻿units = {

	### Naval OOB ###
	fleet = {
		name = "Republic of China Navy"
		naval_base = 14701
		task_force = {
			name = "Republic of China Navy"
			location = 14701
			#Destroyers
			ship = { name = "ROCS Lo Yang" definition = destroyer start_experience_factor = 0.40 equipment = { destroyer_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Allen M.Summner Class (Taiwanese Modernization)" } } }
			ship = { name = "ROCS Nan Yang" definition = destroyer start_experience_factor = 0.40 equipment = { destroyer_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Allen M.Summner Class (Taiwanese Modernization)" } } }
			ship = { name = "ROCS Chen Yang" definition = destroyer start_experience_factor = 0.40 equipment = { destroyer_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Gearing Class (Taiwanese Modernization)" } } }
			ship = { name = "ROCS Liao Yang" definition = destroyer start_experience_factor = 0.40 equipment = { destroyer_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Gearing Class (Taiwanese Modernization)" } } }
			ship = { name = "ROCS Te Yang" definition = destroyer start_experience_factor = 0.40 equipment = { destroyer_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Gearing Class (Taiwanese Modernization)" } } }
			ship = { name = "ROCS Shen Yang" definition = destroyer start_experience_factor = 0.40 equipment = { destroyer_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Gearing Class (Taiwanese Modernization)" } } }
			ship = { name = "ROCS Lai Yang" definition = destroyer start_experience_factor = 0.40 equipment = { destroyer_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Gearing Class (Taiwanese Modernization)" } } }
			#FRIGATES

			ship = { name = "ROCS Chih Yang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Knox Class" } } }
			ship = { name = "ROCS Lan Yang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Knox Class" } } }
			ship = { name = "ROCS Ning Yang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Knox Class" } } }
			ship = { name = "ROCS Hae Yang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Knox Class" } } }
			ship = { name = "ROCS Fong Yang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Knox Class" } } }
			ship = { name = "ROCS Fen Yang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Knox Class" } } }
			ship = { name = "ROCS Hwai Yang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Knox Class" } } }
			ship = { name = "ROCS Yi Yang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Knox Class" } } }
			#completed

			ship = { name = "ROCS Cheng Kung" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_2 = { amount = 1 owner = TAI version_name = "Cheng Kung Class" } } }
			ship = { name = "ROCS Cheng Ho" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_2 = { amount = 1 owner = TAI version_name = "Cheng Kung Class" } } }
			ship = { name = "ROCS Chi Kuang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_2 = { amount = 1 owner = TAI version_name = "Cheng Kung Class" } } }
			ship = { name = "ROCS Yueh Fei" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_2 = { amount = 1 owner = TAI version_name = "Cheng Kung Class" } } }
			ship = { name = "ROCS Tzu I" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_2 = { amount = 1 owner = TAI version_name = "Cheng Kung Class" } } }
			ship = { name = "ROCS Pan Chao" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_2 = { amount = 1 owner = TAI version_name = "Cheng Kung Class" } } }
			ship = { name = "ROCS Chang Chien" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_2 = { amount = 1 owner = TAI version_name = "Cheng Kung Class" } } }
			#completed


			ship = { name = "ROCS Kang Ding" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_3 = { amount = 1 owner = TAI version_name = "Kang Ding Class" } } }
			ship = { name = "ROCS Si Ning" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_3 = { amount = 1 owner = TAI version_name = "Kang Ding Class" } } }
			ship = { name = "ROCS Wu Chang" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_3 = { amount = 1 owner = TAI version_name = "Kang Ding Class" } } }
			ship = { name = "ROCS Di Hua" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_3 = { amount = 1 owner = TAI version_name = "Kang Ding Class" } } }
			ship = { name = "ROCS Kun Ming" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_3 = { amount = 1 owner = TAI version_name = "Kang Ding Class" } } }
			ship = { name = "ROCS Chen De" definition = frigate start_experience_factor = 0.40 equipment = { frigate_hull_3 = { amount = 1 owner = TAI version_name = "Kang Ding Class" } } }
			#completed

			ship = { name = "ROCS Hai Lung" definition = attack_submarine start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TAI version_name = "Hai Lung Class" } } }
			ship = { name = "ROCS Hai Hu" definition = attack_submarine start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TAI version_name = "Hai Lung Class" } } }
			ship = { name = "ROCS Hai Shih" definition = attack_submarine start_experience_factor = 0.40 equipment = { attack_submarine_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Hai Shih Class" } } }
			ship = { name = "ROCS Hai Pao" definition = attack_submarine start_experience_factor = 0.40 equipment = { attack_submarine_hull_1 = { amount = 1 owner = TAI creator = USA version_name = "Hai Shih Class" } } }
		}
	}
}