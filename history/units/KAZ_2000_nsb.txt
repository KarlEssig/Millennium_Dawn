﻿division_template = {
	name = "Respublikaliq Ulani"		#Republican Guard

	division_names_group = KAZ_MECHANIZED_DIVISIONS

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 0 y = 0 }
		Arty_Bat = { x = 0 y = 1 }
	}

	priority = 2
}

division_template = {
	name = "Motoatqishtar Brïgada"		#Motor rifle brigade

	division_names_group = KAZ_MECHANIZED_DIVISIONS

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}
	support = {
		SP_AA_Battery = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Tank Brïgada"		#Motor rifle brigade

	division_names_group = KAZ_ARMOURED_DIVISIONS

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}
	support = {
		SP_AA_Battery = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Taw Bataloni"		#mountain battalion

	division_names_group = KAZ_ARMY_DIVISIONS

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Äwe Shabwildawshi Brïgada"		#air assault brigade

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
		L_Air_assault_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 0 y = 3 }
	}
}

units = {
	division = {
		name = "3-shi Jeke Motoatqishtar Brïgadasi"
		location = 1864 		#Usharal
		division_template = "Motoatqishtar Brïgada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "1-shi Respublikaliq Ulani Brigada"
		location = 10308 	#Astana
		division_template = "Respublikaliq Ulani"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "2-shi Respublikaliq Ulani Brigada"
		location = 10308 		#Astana
		division_template = "Respublikaliq Ulani"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "4-shi Jeke Motoatqishtar Brïgadasi"
		location = 1864 		#Ust-Kamenogorsk
		division_template = "Motoatqishtar Brïgada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "5-shi Jeke Motoatqishtar Brïgadasi"
		location = 10603 		#Taraz
		division_template = "Motoatqishtar Brïgada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "6-shi Jeke Motoatqishtar Brïgadasi"
		location = 10148 		#Shymkent
		division_template = "Motoatqishtar Brïgada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "7-shi Jeke Motoatqishtar Brïgadasi"
		location = 4333 		#Karagandy
		division_template = "Motoatqishtar Brïgada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "35-shi Äwe Shabwildawshi Brïgada"
		location = 10547 		#Kapshagai
		division_template = "Äwe Shabwildawshi Brïgada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "36-shi Äwe Shabwildawshi Brïgada"
		location = 10308 		#Astana
		division_template = "Äwe Shabwildawshi Brïgada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "37-shi Äwe Shabwildawshi Brïgada"
		location = 10619 		#Taldykorgan
		division_template = "Äwe Shabwildawshi Brïgada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "38-shi Äwe Shabwildawshi Brïgada"
		location = 13210 		#Almaty
		division_template = "Äwe Shabwildawshi Brïgada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1		#AK-74
		amount = 7000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 200
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 400
		producer = SOV
	}

	## Vehicles
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72B"
		amount = 650
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-64BV"
		amount = 100
		producer = UKR
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-62"
		amount = 280
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "BMP-2"
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 200
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 84
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 1686
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 140
		producer = SOV
		variant_name = "BRDM-2"
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0		#truck
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-27 Uragan"
		amount = 90
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 57
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "2S9 Nona"
		amount = 26
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "2S3 Akatsiya"
		amount = 89
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 74
		producer = SOV
	}

	add_equipment_to_stockpile = {
			type = artillery_0
			variant_name = "D-30"
			producer = SOV
			amount = 161
		}
		
	add_equipment_to_stockpile = {
			type = artillery_0
			variant_name = "D-20"
			producer = SOV
			amount = 74
		}
		
	add_equipment_to_stockpile = {
			type = artillery_1
			variant_name = "2A65 Msta-B"
			producer = SOV
			amount = 90
		}
	
	add_equipment_to_stockpile = {
				type = artillery_1
				variant_name = "2A36"
				producer = SOV
				amount = 180
		}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 20
		producer = SOV
		variant_name = "2K12 Kub"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		variant_name = "2K11 Krug"
		amount = 27
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 20
		producer = SOV
		variant_name = "S-125 Neva"
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1		#mi-8/17
		amount = 380
		producer = SOV
	}
}