﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 		#Alpha Jet
		amount = 6
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 		#Mirage 2000
		amount = 12
		producer = FRA
	}
}