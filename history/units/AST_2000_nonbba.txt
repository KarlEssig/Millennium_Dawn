instant_effect = {
	add_equipment_to_stockpile = {
		type = Strike_fighter1 #F-111 Aardvark
		amount = 35
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = naval_plane2 #P-3C Orion
		amount = 26
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #Boeing F/A-18 Hornet
		amount = 71
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Aermacchi MB-326
		amount = 25
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane3 #C-130J Super Hercules
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #Pilatus PC-9
		amount = 59
		producer = SWI
	}
	# Missile OOB
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 150
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 100
		}
	}
}