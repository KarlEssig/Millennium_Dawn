﻿division_template = {
	name = "Piechota" #territorial brigade

	division_names_group = POL_ARM_02

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Brygada P.Gorskiej" #Mountaineer brigade

	division_names_group = POL_ARM_07

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		Arty_Battery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Dywizja Zmechanizowana" #Mech Division

	division_names_group = POL_ARM_01

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 0 y = 4 }

		Mech_Inf_Bat = { x = 1 y = 0 }
		L_arm_Bat = { x = 1 y = 1 }
		armor_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 3 }
		SP_Arty_Bat = { x = 1 y = 4 }

		Mech_Inf_Bat = { x = 2 y = 0 }
		L_arm_Bat = { x = 2 y = 1 }
		armor_Bat = { x = 2 y = 2 }
		SP_Arty_Bat = { x = 2 y = 3 }
		SP_Arty_Bat = { x = 2 y = 4 }

		SP_Arty_Bat = { x = 3 y = 0 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		SP_AA_Battery = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Dywizja Kawalerii Pancernej" #Armored Cavalry Division

	division_names_group = POL_ARM_03

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 0 y = 4 }

		armor_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 1 y = 1 }
		armor_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 3 }
		SP_Arty_Bat = { x = 1 y = 4 }

		Arm_Inf_Bat = { x = 2 y = 0 }
		Arm_Inf_Bat = { x = 2 y = 1 }
		Arm_Inf_Bat = { x = 2 y = 2 }
		armor_Bat = { x = 2 y = 3 }
		SP_Arty_Bat = { x = 2 y = 4 }

		SP_Arty_Bat = { x = 3 y = 0 }
		SP_Arty_Bat = { x = 3 y = 1 }
		attack_helo_bat = { x = 3 y = 2 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		SP_AA_Battery = { x = 0 y = 3 }
	}

	priority = 2
}
division_template = {
	name = "Strzelcy Podhalanscy" #Mechanized Brigade

	division_names_group = POL_ARM_05

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 0 y = 4 }

		SP_AA_Bat = { x = 1 y = 0 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		SP_AA_Battery = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Kawaleria Powietrzna"

	division_names_group = POL_ARM_04

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }

		attack_helo_bat = { x = 1 y = 0 }
		attack_helo_bat = { x = 1 y = 1 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		Arty_Battery = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Komandosi"

	division_names_group = POL_ARM_08

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
	}

	priority = 2
}
division_template = {
	name = "Coastal Brigade"

	division_names_group = POL_ARM_10

	regiments = {
		Arm_Marine_Bat = { x = 0 y = 0 }
		Arm_Marine_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }

		SP_AA_Bat = { x = 1 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Airborne Brigade"

	division_names_group = POL_ARM_06

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		Arty_Battery = { x = 0 y = 2 }
	}
}

#This is technically OOB from 1999 not 2000. in 1999 reform ne number of military disctricts decreased from 4 to 2. ~~ Salamin
units = {
	division = {
		#name = "2 Pomorska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}

		officer = {
			name = "Waldemar Skrzypczak"

			portraits = {
				army = {
					large = "GFX_waldemar_skrypczak_large"
					small = "GFX_waldemar_skrypczak_small"
				}
			}
		}

		location = 11260		#Szczecinek
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "12 Szczecinska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 14
		}

		officer = {
			name = "Mirosław Różański"

			portraits = {
				army = {
					large = "GFX_miroslaw_wozanski_large"
					small = "GFX_miroslaw_wozanski_small"
		 		}
			}
		}

		location = 6282		#Szczecin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "16 Pomorska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 17
		}

		officer = {
			name = "Wiesław Michnowicz"

			portraits = {
				army = {
					large = "GFX_wieslaw_michnowicz_large"
					small = "GFX_wieslaw_michnowicz_small"
				}
			}
		}

		location = 3380		#Elbl�g
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "8 Baltycka Dywizja Obrony Wybrzeza"
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}

		officer = {
			name = "Zbigniew Glowienka"

		portraits = {
		 	army = {
		 		large = "GFX_zbigniew_glowienka_large"
		 		small = "GFX_zbigniew_glowienka_small"
		 	}
		}
	}

		location = 11372		#S�upsk/Koszalin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "1 Gdanska Brygada Obrony Terytorialnej" #brygaga - 50%-33% str.

		officer = {
			name = "Wieslaw Kukula"

		portraits = {
		 	army = {
		 		large = "GFX_wieslaw_kukula_large"
		 		small = "GFX_wieslaw_kukula_small"
		 	}
		}
	}

		location = 362		#Danzig
		division_template = "Piechota"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "5 Kresowa Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}

		officer = {
			name = "Zbigniew Smok"

		portraits = {
			army = {
		 		large = "GFX_zbigniew_smok_large"
		 		small = "GFX_zbigniew_smok_small"
			}
		}
	}
		location = 3438		#Gubin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "11 Lubuska Dywizja Kawalerii Pancernej"
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}

		officer = {
			name = "Stanisław Nowakowicz"

		portraits = {
		 	army = {
		 		large = "GFX_stanislaw_nowakowicz_large"
		 		small = "GFX_stanislaw_nowakowicz_small"
		 	}
		}
	}
		location = 3438		#Zagan
		division_template = "Dywizja Kawalerii Pancernej"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "1 Warszawska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Marek Sokolowski"

		portraits = {
		 	army = {
		 		large = "GFX_marek_sokolowski_large"
		 		small = "GFX_marek_sokolowski_small"
		 	}
		}
	}

		location = 524		#Legionowo
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "25 Dywizja Kawalerii Powietrznej"
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}

		officer = {
			name = "Mieczyslaw Bieniek"

		portraits = {
		 	army = {
		 		large = "GFX_mieczyslaw_bieniek_large"
		 		small = "GFX_mieczyslaw_bieniek_small"
		 	}
		}
	}

		location = 9508		#Lodz
		division_template = "Kawaleria Powietrzna"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "21 Brygada Strzelcow Podhalanskich"
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}

		officer = {
			name = "Jerzy Gut"

		portraits = {
		 	army = {
		 		large = "GFX_jerzy_gut_large"
		 		small = "GFX_jerzy_gut_small"
		 	}
		}
	}

		location = 6499		#Rzeszow
		division_template = "Strzelcy Podhalanscy"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "6 Brygada Desantowo-Szturmowa"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Tadeusz Mikutel"

		portraits = {
		 	army = {
		 		large = "GFX_tadeusz_mikutel_large"
		 		small = "GFX_tadeusz_mikutel_small"
		 	}
		}
	}

		location = 9427		#Krakow
		division_template = "Airborne Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "22 Karpacka Brygada Piechoty Gorskiej"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Michal Strzelecki"

		portraits = {
		 	army = {
				large = "GFX_michal_strzelecki_large"
		 		small = "GFX_michal_strzelecki_small"
		 	}
		}
	}

		location = 6512		#Nysa
		division_template = "Brygada P.Gorskiej"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "1 Pulk Specjalny Komandosow"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Piotr Patalong"

		portraits = {
		 	army = {
		 		large = "GFX_piotr_patalong_large"
		 		small = "GFX_piotr_patalong_small"
		 	}
		}
	}

		location = 3544		#Warsaw
		division_template = "Komandosi"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "Formoza Gdansk"
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}

		officer = {
			name = "Adam Joks"

		portraits = {
		 	army = {
		 		large = "GFX_adam_joks"
		 		small = "GFX_adam_joks_small"
		 	}
		}
	}

		location = 362		#Danzig
		division_template = "Coastal Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons3 #Kbz Wz.96
		amount = 8000
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2 #Kbz Wz.88
		amount = 20000
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons1 #AKM
		amount = 10000
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0 #SPG-9
		amount = 420
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1 #RPG-76 Komar
		amount = 3000
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment1 #C3
		amount = 2600
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1 #Grom
		amount = 700
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "GAZ-66"
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "ZIL-131"
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1
		variant_name = "UAZ-469"
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 510
		producer = SOV
		variant_name = "BRDM-2"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 38
		producer = SOV
		variant_name = "BRM-1K"
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BWP-1"
		amount = 1405
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M-30
		amount = 277
		variant_name = "M-30"
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0
		amount = 135
		variant_name = "D-1"
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 400
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55AM Merida"
		amount = 230
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72M1"
		amount = 706
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "PT-91"
		amount = 186
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "OT-64"
		amount = 726
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Goździk"
		amount = 461
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 80
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S7 Pion"
		amount = 8
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "ShKH vz. 77 DANA"
		amount = 111
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 228
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "RM-70"
		amount = 30
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 70
		producer = SOV
		variant_name = "ZSU-23-4 Shilka"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 115
		producer = SOV
		variant_name = "2K12 Kub"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 64
		producer = SOV
		variant_name = "9K33 Osa"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 168
		producer = SOV
		variant_name = "9K31 Strela-1"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 4
		producer = SOV
		variant_name = "9K35 Strela-10"
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 46
		producer = SOV
		variant_name = "Mi-24V"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 45
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2
		amount = 10
		producer = POL
	}
}