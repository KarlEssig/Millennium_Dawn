﻿division_template = {
	name = "Rapid Response Brigade"

	division_names_group = CZE_ARM_01

	regiments = {
		Arm_Air_Inf_Bat = { x = 0 y = 0 }
		Arm_Air_Inf_Bat = { x = 0 y = 1 }
		Arm_Air_Inf_Bat = { x = 0 y = 2 }
		
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }

		armor_Bat = { x = 2 y = 0 }
		armor_Bat = { x = 2 y = 1 }
		SP_AA_Bat = { x = 2 y = 2 }
		SP_Arty_Bat = { x = 2 y = 3 }
	}
	support = {
		Arm_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		attack_helo_comp = { x = 0 y = 3 }
	}
	priority = 2
}
division_template = {
	name = "Mechanized Brigade"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }

		SP_AA_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 2 y = 1 }
	}
	support = {
		Arm_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		attack_helo_comp = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Reserve Brigade"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 3 }
		
		SP_Arty_Bat = { x = 1 y = 0 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		Arty_Battery = { x = 0 y = 3 }
	}
	priority = 0
}

division_template = {
	name = "Výsadkový Pluk"

	division_names_group = CZE_ARM_02

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
	}

	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Dělostřelecký Pluk"

	division_names_group = CZE_ARM_03

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		SP_Arty_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}

	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		producer = CZE
		amount = 123
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55AM2B"
		producer = CZE
		amount = 128
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72M1"
		producer = CZE
		amount = 541
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 182
		producer = SOV
		variant_name = "BRDM-2"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 15
		producer = SOV
		variant_name = "BRM-1K"
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BVP-1"
		amount = 612
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "BVP-2"
		amount = 174
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "OT-90"
		amount = 403
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "OT-64"
		amount = 577
	}

	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "ShKH vz. 77 DANA"
		amount = 273
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "D-30"
		producer = SOV
		amount = 148
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 91
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "RM-70"
		amount = 135
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1 		#Konkurs
		producer = SOV
		amount = 21
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0 		#Malyutka
		producer = SOV
		amount = 521
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0 			#Tatra T813
		amount = 882
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1 			#Tatra T815
		amount = 434
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_1
		producer = SOV
		amount = 140
		variant_name = "9K35 Strela-10"
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0 				#SA-7
		producer = SOV
		amount = 600
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_0 #RPG-7
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1
		variant_name = "9M113 Konkurs"
		producer = SOV
		amount = 25
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons1 		#vz. 58
		amount = 9000
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2 		#vz. 58V
		amount = 1000
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 800
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 34
		producer = SOV
		variant_name = "Mi-24V"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 	#Mil Mi-8
		producer = SOV
		amount = 14
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter2	#Mil Mi-17
		amount = 42
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1 	#W-3 Sokół
		producer = POL
		amount = 11
	}

	add_equipment_production = {
		equipment = {
			type = infantry_weapons3
			creator = "CZE"
		}
		requested_factories = 1
		progress = 0.49
		efficiency = 60
	}

	add_equipment_production = {
		equipment = {
			type = command_control_equipment
			creator = "CZE"
		}
		requested_factories = 1
		progress = 0.80
		efficiency = 50
	}
}

units = {
	division = {
		name = "4. Brigáda Rychlého Nasazení"
		location = 3418 		#zatec
		division_template = "Rapid Response Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "7. Mechanizovaná Brigáda"
		location = 6590 		#Hranice
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "2. Mechanizovaná Brigáda"
		location = 9541 	#Písek
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "6. Mobilizační Základna"
		location = 11556 		#Jihlava
		division_template = "Reserve Brigade"
		start_experience_factor = 0.15
		start_equipment_factor = 0.01
	}
}