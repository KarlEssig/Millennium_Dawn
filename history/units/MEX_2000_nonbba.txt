﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1			#F-5
		amount = 8
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1		#Pilatus PC-7
		amount = 70
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = transport_plane1		#C-130 Hercules
		amount = 7
		producer = USA
	}
}