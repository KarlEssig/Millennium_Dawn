instant_effect = {

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-212 Aviocar"
		amount = 2
		producer = SPR
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "CASA/IPTN CN-235"
		amount = 2
		producer = SPR
	}

}