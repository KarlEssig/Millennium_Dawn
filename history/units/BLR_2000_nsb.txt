﻿division_template = {
	name = "Motastralkovaya Brigada" #Armored Inf
	division_names_group = BLR_ARM_03

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 0 y = 4 }
		
		SP_Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		armor_Recce_Comp = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Brigada VDV on APC"
	division_names_group = BLR_PAR_01

	regiments = {
		Mech_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Air_Inf_Bat = { x = 0 y = 1 }
		Mech_Air_Inf_Bat = { x = 0 y = 2 }
		attack_helo_bat = { x = 0 y = 3 }

		Arty_Bat = { x = 1 y = 0 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
		helicopter_combat_service_support = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Brigada VDV on IFV"
	division_names_group = BLR_PAR_01

	regiments = {
		Arm_Air_Inf_Bat = { x = 0 y = 0 }
		Arm_Air_Inf_Bat = { x = 0 y = 1 }
		Arm_Air_Inf_Bat = { x = 0 y = 2 }
		attack_helo_bat = { x = 0 y = 3 }

		Arty_Bat = { x = 1 y = 0 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
		helicopter_combat_service_support = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Brigada Spetsnaza"					#Spetsnaz Brigade
	division_names_group = BLR_INF_03
	
	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
		Special_Forces = { x = 0 y = 3 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		Mech_Recce_Comp = { x = 0 y = 1 }
		helicopter_combat_service_support = { x = 0 y = 2 }
	}

	priority = 2
}

division_template = {
	name = "Motastralkovaya Diviziya" #Mechanized
	division_names_group = BLR_ARM_04

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 0 y = 4 }

		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		armor_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 3 }
		SP_Arty_Bat = { x = 1 y = 4 }

		Arm_Inf_Bat = { x = 2 y = 0 }
		Arm_Inf_Bat = { x = 2 y = 1 }
		armor_Bat = { x = 2 y = 2 }
		SP_Arty_Bat = { x = 2 y = 3 }
		SP_Arty_Bat = { x = 2 y = 4 }

		armor_Bat = { x = 3 y = 0 }
		armor_Bat = { x = 3 y = 1 }
		SP_Arty_Bat = { x = 3 y = 2 }
		SP_AA_Bat = { x = 3 y = 3 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		SP_AA_Battery = { x = 0 y = 3 }
		Arty_Battery = { x = 0 y = 4 }
	}
}

units = {
	division = {
		#name = "120-ya Gvardeysakaya Mekhanizavanaya Diviziya"
		division_name = {
			is_name_ordered = yes
			name_order = 120
		}
		location = 11370		#Minsk
		division_template = "Motastralkovaya Diviziya"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		#name = "19-ya Asobnaya Mekhanizavanaya Brigada"
		division_name = {
			is_name_ordered = yes
			name_order = 19
		}
		location = 699
		division_template = "Motastralkovaya Brigada"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		#name = "6-ya Asobnaya Mekhanizavanaya Brigada"
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 3393			#Grodno
		division_template = "Motastralkovaya Brigada"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		#name = "11-ya Asobnaya Mekhanizavanaya Brigada"
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 6359			#Slonim
		division_template = "Motastralkovaya Brigada"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		#name = "50-ya Asobnaya Mekhanizavanaya Brigada"
		division_name = {
			is_name_ordered = yes
			name_order = 50
		}
		location = 9304			#Slonim
		division_template = "Motastralkovaya Brigada"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		#name = "38-ya Asobnaya Mabilnaya Brigada"
		division_name = {
			is_name_ordered = yes
			name_order = 38
		}
		location = 3392		#Brest
		division_template = "Brigada VDV on APC"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		#name = "317-ya Asobnaya Mabilnaya Brigada"
		division_name = {
			is_name_ordered = yes
			name_order = 317
		}
		location = 11241		#Vitebsk
		division_template = "Brigada VDV on IFV"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
		force_equipment_variants = { ifv_hull_0 = { amount = 154 owner = BLR creator = SOV version_name = "BMD-1" } }
	}
	division = {
		#name = "370-ya Asobnaya Mabilnaya Brigada"
		division_name = {
			is_name_ordered = yes
			name_order = 370
		}
		location = 699		#Polotsk
		division_template = "Brigada VDV on APC"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		#name = "5-ya Brigada Spetsnaza"	#ok
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 11370		#Maryina Horka
		division_template = "Brigada Spetsnaza"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
}

instant_effect = {

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72B"
		amount = 469
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72A"
		amount = 800
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72 Ural"
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 55
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-80BV"
		amount = 95
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		variant_name = "BRDM-2"
		amount = 161
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "BMP-2"
		amount = 1164
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 81
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMD-1"
		amount = 0 #154 deployed
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 193
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-D"
		amount = 22
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-70"
		amount = 445
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 160
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 70
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_1
		producer = SOV
		variant_name = "BM-30 Smerch"
		amount = 40
	}
	
	add_equipment_to_stockpile = {
		type = spart_hull_0
		producer = SOV
		variant_name = "BM-27 Uragan"
		amount = 84
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 208
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_2
		variant_name = "2S19 Msta"
		amount = 13
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "2S3 Akatsiya"
		amount = 168
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 235
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S5 Giatsynt"
		amount = 120
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S9 Nona"
		amount = 54
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = artillery_0	
		variant_name = "D-30"
		producer = SOV
		amount = 178
	}
	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "D-1"
		producer = SOV
		amount = 6
	}
	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "D-20"
		producer = SOV
		amount = 58
	}
	add_equipment_to_stockpile = {
		type = artillery_1
		variant_name = "2A36"
		producer = SOV
		amount = 50
	}
	add_equipment_to_stockpile = {
		type = artillery_1
		variant_name = "2A65 Msta-B"
		producer = SOV
		amount = 136
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0		#SA-8 Osa
		amount = 150
		producer = SOV
		variant_name = "9K33 Osa"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 100
		producer = SOV
		variant_name = "9K35 Strela-10"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 50
		producer = SOV
		variant_name = "2K22 Tunguska"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 50
		producer = SOV
		variant_name = "9K37 Buk"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 8
		producer = SOV
		variant_name = "9K330 Tor"
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment1
		amount = 1000
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "UAZ-452"
		producer = SOV
		amount = 300
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "GAZ-66"
		producer = SOV
		amount = 300
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1
		variant_name = "UAZ-469"
		producer = SOV
		amount = 300
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "ZIL-131"
		amount = 900
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 800
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 200
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1 #AK74
		amount = 10000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons #AKM
		amount = 2000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons
		variant_name = "Mosin"
		producer = SOV
		amount = 300
	}
	
	add_equipment_to_stockpile = {
		type = infantry_weapons
		variant_name = "SKS"
		producer = SOV
		amount = 1000
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons
		variant_name = "PPSH"
		producer = SOV
		amount = 300
	}


	#HELOS
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 57
		producer = SOV
		variant_name = "Mi-24V"
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 12
		producer = SOV
		variant_name = "Mi-24P"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1		#Mi-8
		amount = 160
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2		#Mi-26
		amount = 14
		producer = SOV
	}
}