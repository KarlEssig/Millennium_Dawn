﻿instant_effect = {	
	add_equipment_to_stockpile = {
		type = cv_medium_plane_airframe_2
		amount = 57
		variant_name = "F/A-18C Hornet"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_cas_airframe_2
		amount = 7
		variant_name = "F/A-18D Hornet"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 42
		variant_name = "BAE Systems Hawk"
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 5
		variant_name = "Fokker F27 Friendship"
		producer = HOL
	}
}