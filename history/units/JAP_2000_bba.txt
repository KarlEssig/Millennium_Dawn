﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "P-3C Orion"
		type = large_plane_maritime_patrol_airframe_1
		amount = 90
		producer = JAP
	}
	add_equipment_to_stockpile = {
		variant_name = "F-4EJ Kai Phantom"
		type = medium_plane_fighter_airframe_1
		amount = 90
		producer = JAP
	}
	add_equipment_to_stockpile = {
		variant_name = "F-15J Eagle"
		type = medium_plane_fighter_airframe_2
		amount = 60
		producer = JAP
	}
	add_equipment_to_stockpile = {
		variant_name = "F-15J-MSIP Eagle"
		type = medium_plane_fighter_airframe_2
		amount = 103
		producer = JAP
	}
	add_equipment_to_stockpile = {
		variant_name = "F-1"
		type = small_plane_naval_bomber_airframe_1
		amount = 40
		producer = JAP
	}
	add_equipment_to_stockpile = {
		variant_name = "YS-11M"
		type = large_plane_air_transport_airframe_1
		amount = 35
		producer = JAP
	}
	add_equipment_to_stockpile = {
		variant_name = "Kawasaki C-1"
		type = large_plane_air_transport_airframe_1
		amount = 20
		producer = JAP
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 10
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Mitsubishi MU-2"
		type = large_plane_air_transport_airframe_1
		amount = 10
		producer = JAP
	}
	add_equipment_to_stockpile = {
		variant_name = "E-2 Hawkeye"
		type = cv_medium_plane_scout_airframe_2
		amount = 14
		producer = USA
	}
	# Missile OOB
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 450
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 175
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_3
			amount = 85
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_4
			amount = 70
		}
	}
}