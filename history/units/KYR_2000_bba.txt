﻿instant_effect = {
#	add_equipment_to_stockpile = {
#		variant_name = "Aero L-39"
#		type = small_plane_strike_airframe_1
#		amount = 24
#		producer = CZE
#	}
#	add_equipment_to_stockpile = {
#		variant_name = "MiG-21 Bis"
#		type = small_plane_strike_airframe_1
#		amount = 50
#		producer = SOV
#	}
	add_equipment_to_stockpile = {
		variant_name = "An-12"
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "An-26"
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = SOV
	}
#	add_equipment_to_stockpile = {
#		variant_name = "An-2"
#		type = large_plane_air_transport_airframe_1
#		amount = 19
#		producer = SOV
#	}
}
air_wings = {
	720 = { 
		small_plane_strike_airframe_1 = { owner = "KYR" creator = "SOV" amount = 50 version_name = "MiG-21 Bis" }
		name = "322nd Training Aviation Regiment"
		start_experience_factor = 0.35
		#
		small_plane_strike_airframe_1 = { owner = "KYR" creator = "CZE" amount = 24 version_name = "Aero L-39" }
		name = "716th Training Aviation Regiment"
		start_experience_factor = 0.35
		#
		large_plane_air_transport_airframe_1 = { owner = "KYR" creator = "SOV" amount = 19 version_name = "An-2" }
		name = "349th Training Aviation Regiment"
		start_experience_factor = 0.35
		#
	}
}