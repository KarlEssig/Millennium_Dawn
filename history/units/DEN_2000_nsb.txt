﻿##### Division Templates #####
division_template = {
	name = "Mekaniseret Brigade"

	division_names_group = DEN_MECH

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Arty_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 2 y = 1 }
		Mot_Inf_Bat = { x = 3 y = 0 }
	}
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}

division_template = {
	name = "International Brigade"

	division_names_group = DEN_MECH

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Arty_Bat = { x = 2 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		Mech_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Special Operations"
	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
		support = {
		L_Recce_Comp = { x = 0 y = 0 }
		combat_service_support_company = { x = 0 y = 1 }
	}
	priority = 2
}


#Ground Forces
units = {
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		officer = {
			name = "Jørgen Andreassen"

	}
		location = 399		#Fredericia
		division_template = "Mekaniseret Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 3
	}
		officer = {
			name = "Per Ludvigsen"

	}
		location = 394		#Haderslev
		division_template = "Mekaniseret Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		officer = {
			name = "Niels Kjeldsen"
	}
		location = 6287		#Ringsted
		division_template = "Mekaniseret Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		officer = {
			name = "Jørgen Hansen-Nord"

	}
		location = 3305		#Vordingborg
		division_template = "International Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "Jægerkorpset"
		location = 6287
		division_template = "Special Operations"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Frømandskorpset"
		location = 3305
		division_template = "Special Operations"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#1st Squadron - Fredrikshavn
	#2nd Squadron - Korsor

	#UndervandsBådsEskadren
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1 	#M/66
		amount = 3500
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2 	#M/75
		amount = 20000 #70'000 thousand in service in 1985, not sure how many scrapped by 2000
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons3 	#Colt C7
		amount = 3500
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1	#AT-4
		producer = SWE
		amount = 220
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0				#TOW
		producer = USA
		amount = 140
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1				#Stinger
		producer = USA
		amount = 250
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment2
		producer = DEN
		amount = 600
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113"
		producer = USA
		amount = 657
	}

	add_equipment_to_stockpile = {
		type = apc_hull_2
		variant_name = "MOWAG Piranha III APC"
		producer = SWI
		amount = 113
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_3			#Mowag Eagle
		producer = SWI
		amount = 36
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_2			#Humvee
		producer = USA
		amount = 22
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0			#Various MAN trucks
		producer = GER
		amount = 220
	}

	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M109A3"
		producer = USA
		amount = 76
	}

	add_equipment_to_stockpile = {
		type = artillery_0		#M101
		producer = USA
		amount = 157
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter2		#AW101 & Westland Lynx
		producer = ENG
		amount = 19
	}
}

#https://marinehist.dk/FOF/FaktaOmFSV-Jan2000.pdf useful