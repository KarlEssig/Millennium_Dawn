﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Tu-22M"
		type = large_plane_airframe_1
		amount = 6
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-23"
		type = medium_plane_airframe_1
		amount = 130
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Mirage 5"
		type = small_plane_strike_airframe_1
		amount = 44
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "Mirage F1"
		type = small_plane_airframe_1
		amount = 35
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-24M"
		type = medium_plane_airframe_2
		amount = 12
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-17M4"
		type = small_plane_cas_airframe_1
		amount = 45
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-21 Bis"
		type = small_plane_strike_airframe_1
		amount = 50
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-25 Foxbat"
		type = medium_plane_fighter_airframe_1
		amount = 63
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "An-26"
		type = large_plane_air_transport_airframe_1
		amount = 15
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 7
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Aeritalia G.222"
		type = large_plane_air_transport_airframe_1
		amount = 16
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "Il-76"
		type = large_plane_air_transport_airframe_2
		amount = 20
		producer = SOV
	}
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_1	#Frog-7
			amount = 40
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_1	#Scuds
			amount = 80
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1	#S-75
			amount = 90
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1	#S-125
			amount = 48
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2	#S-200
			amount = 48
		}
	}
}