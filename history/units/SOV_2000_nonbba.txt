instant_effect = {

	############################################
	################STOCKPILE###################
	############################################

	add_equipment_to_stockpile = {
		type = AS_Fighter2 #MIG-31
		amount = 256
	}

	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #MiG-29K
		amount = 4
	}

	add_equipment_to_stockpile = {
		type = CV_MR_Fighter3 #SU-33
		amount = 24
	}

	add_equipment_to_stockpile = {
		type = AS_Fighter2 #SU-27
		amount = 413
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter2 #MiG-29
		amount = 291
	}

	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Su-24
		amount = 566
	}

	add_equipment_to_stockpile = {
		type = cas1 #SU-25
		amount = 245
	}

	add_equipment_to_stockpile = {
		type = strategic_bomber1 #tu95
		amount = 60
	}

	add_equipment_to_stockpile = {
		type = strategic_bomber2 #tu22
		amount = 94
	}

	add_equipment_to_stockpile = {
		type = strategic_bomber3 #tu160
		amount = 6
	}

	add_equipment_to_stockpile = {
		type = naval_plane2 #tu142
		amount = 28
	}

	add_equipment_to_stockpile = {
		type = transport_plane2 #Il-78
		amount = 320
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter2	 #Aero L-39
		amount = 200
		producer = CZE
	}

	# Missile OOB
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 2000
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 500
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_3
			amount = 1154
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_4
			amount = 129
		}
		add_equipment_to_stockpile = {
			type = nuclear_missile_equipment_1
			amount = 1860
		}
		add_equipment_to_stockpile = {
			type = nuclear_missile_equipment_2
			amount = 460
		}
		add_equipment_to_stockpile = {
			type = nuclear_missile_equipment_3
			amount = 2880
		}
		add_equipment_to_stockpile = {
			type = nuclear_missile_equipment_4
			amount = 300
		}
		add_equipment_to_stockpile = {
			type = ballistic_missile_equipment_2
			amount = 170
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_1
			amount = 1240
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_2
			amount = 2227
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_3
			amount = 210
		}
	}


	#70 MIG-25
}