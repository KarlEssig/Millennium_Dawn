﻿division_template = {
	name = "Liwa al-Muharib al-Muqadas"		

	regiments = {
		Mot_Militia_Bat = { x = 0 y = 0 }
		Mot_Militia_Bat = { x = 0 y = 1 }
		Mot_Militia_Bat = { x = 0 y = 2 }
	}

	support = {
		L_Recce_Comp = { x = 0 y = 0 }
	}

}

units = {
	division = {
		name = "Muqatilu Tshad"
		location = 1057
		division_template = "Liwa al-Muharib al-Muqadas"
		start_experience_factor = 0.5
		start_equipment_factor = 1
	}
}
