﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		amount = 13
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1
		amount = 11
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1
		amount = 3
		producer = USA
	}
}