division_template = {
	name = "Brigada de Mecanizata v2"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Brigada de Mecanizata"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		
		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }

		Arty_Bat = { x = 2 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Batalion de Forte Speciale"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}

	priority = 2
}

units = {
	division = {
		name = "1a Brigada de Infanterie 'Moldova'"
		location = 11686
		division_template = "Brigada de Mecanizata v2"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	division = {
		name = "2a Brigada de Infanterie 'Stefan Cel Mare'"
		location = 6600
		division_template = "Brigada de Mecanizata"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	division = {
		name = "3a Brigada de Infanterie 'Dacia'"
		location = 3724
		division_template = "Brigada de Mecanizata"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	division = {
		name = "Batalion de Forte Speciale"
		location = 11686
		division_template = "Batalion de Forte Speciale"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons		 #AKM
		amount = 1500
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		 #Pistol Mitralieră model 1963
		amount = 1500
		producer = ROM
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment1
		amount = 250
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 150
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1		 #AK-74
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1		 #Fagot SPIGOT IS NATO TERM
		amount = 70
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0		 #Konkurs
		amount = 32
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1		 #Spiral
		#version_name = "AT-6 Spiral"
		amount = 27
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "UAZ-452"
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "GAZ-66"
		amount = 200
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		variant_name = "ZIL-131"
		amount = 200
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1
		variant_name = "UAZ-469"
		producer = SOV
		amount = 100
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMD-1"
		amount = 54
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-D"
		amount = 11
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "MT-LB"
		amount = 40
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 11
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-70"
		amount = 5
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 13
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "TAB-71"
		amount = 161
		producer = ROM
	}

	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		variant_name = "BRDM-2"
		amount = 8
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-27 Uragan"
		amount = 14
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "2S9 Nona"
		amount = 9
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "2A36"
		amount = 21
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "D-20"
		amount = 32
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "M-30"
		amount = 17
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 11
		producer = SOV
	}
}