﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 5
		variant_name = "Dassault Alpha Jet"
		producer = FRA
	}
}