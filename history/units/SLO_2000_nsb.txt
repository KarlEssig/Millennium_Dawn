﻿division_template = {
	name = "Mechanized Brigade"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }

		armor_Bat = { x = 1 y = 0 }

		SP_Arty_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 2 y = 1 }
		Mech_Inf_Bat = { x = 2 y = 2 }
		SP_AA_Bat = { x = 2 y = 3 }
	}
}
division_template = {
	name = "Tank Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }

		Arm_Inf_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }

		SP_Arty_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 2 y = 1 }
		Mech_Inf_Bat = { x = 2 y = 2 }
		SP_AA_Bat = { x = 2 y = 3 }
	}
}

division_template = {
	name = "Special Forces"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}
	priority = 2
}

units = {
	division = {
		name = "1st Tank Brigade"
		location = 9692
		division_template = "Tank Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "2nd Mechanized Brigade"
		location = 6573
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "5th Special Operations Regiment"
		location = 9692
		division_template = "Special Forces"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 4250
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 555
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 500
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 650
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 200
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 400
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72M1"
		amount = 272
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 3
		producer = CZE
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BVP-1"
		amount = 311
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BVP-2"
		amount = 93
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "OT-90"
		amount = 207
		producer = CZE
		#version_name = "OT-90"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "OT-64"
		amount = 11
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 49
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "ShKH vz. 77 DANA"
		amount = 134
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "Zuzana"
		amount = 7
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #D-30
		amount = 76
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "RM-70"
		amount = 87
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 48
		producer = SOV
		variant_name = "9K35 Strela-10"
	}
	#Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 24
		producer = SOV
		variant_name = "Mi-24V"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-17
		amount = 13
		producer = SOV
		#version_name = "Mil Mi-17"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 20
		producer = SOV
	}
}