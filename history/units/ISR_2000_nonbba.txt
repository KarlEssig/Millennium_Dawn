﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = AS_Fighter1 #F-4 Phantom II
		amount = 70
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #F-15A Eagle
		amount = 46
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter3 #F-15C Eagle
		amount = 27
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter3 #F-15E Strike Eagle
		amount = 25
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #F-16A Fighting Falcon
		amount = 109
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter3 #F-16C Fighting Falcon
		amount = 128
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 22
		producer = USA
	}
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 153
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 96
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_4
			amount = 38
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_1
			amount = 185
		}
		add_equipment_to_stockpile = {
			type = ballistic_missile_equipment_2
			amount = 60
		}
		add_equipment_to_stockpile = {
			type = nuclear_ballistic_missile_equipment_3
			amount = 30
		}
	}
}