﻿capital = 151
set_research_slots = 3
2000.1.1 = {
	add_ideas = {
		#Economic Cycle
		fast_growth
		#Corruption
		unrestrained_corruption
		#Faction 1
		landowners
		#Faction 2
		industrial_conglomerates
		#Faction 3
		international_bankers

		#Bureacracy
		bureau_02
		#Military Spending
		defence_02
		#Internal Security
		police_03
		#Education budget
		edu_03
		#Health budget
		health_03
		#Social spending


		#Trade Law
		semi_consumption_economy
		#Conscription Law
		draft_army
		#Women in the military
		volunteer_women
		#Foreign Intervention Law

		#Officer Training
		officer_military_school

		#other
		pluralist
	}

	set_country_flag = positive_landowners
	set_country_flag = positive_international_bankers
	set_country_flag = positive_industrial_conglomerates

	set_technology = {
		legacy_doctrines = 1
		#For templates
		infantry_weapons = 1

		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		util_vehicle_0 = 1

		body_armor_1980 = 1

		basic_computing = 1
		integrated_circuit = 1
		computing1 = 1
		decryption1 = 1
		encryption1 = 1

		radar = 1
		internet1 = 1 	#1G
		fuel_silos = 1
	}

	set_variable = { gdp_per_capita = 1.280 }

	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 80 }
	add_to_array = { influence_array = HUN.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 6 }
	add_to_array = { influence_array = ENG.id }
	add_to_array = { influence_array_val = 2 }
	add_to_array = { influence_array = GER.id }
	add_to_array = { influence_array_val = 2 }
	startup_influence = yes

	set_popularities = {
		democratic = 65
		communism = 0
		fascism = 0.0
		neutrality = 15
		nationalist = 20
	}
	set_politics = {
		ruling_party = democratic
		last_election = "1996.11.20"
		election_frequency = 48
		elections_allowed = yes
	}

	start_politics_input = yes

	### Party Popularities
	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0.30 } #conservatism
	set_variable = { party_pop_array^2 = 0.30 } #liberalism
	set_variable = { party_pop_array^3 = 0.05 } #socialism
	set_variable = { party_pop_array^4 = 0.10 } #Communist-State
	set_variable = { party_pop_array^5 = 0 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0 } #oligarchism
	set_variable = { party_pop_array^16 = 0.15 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0.10 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0.05 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0.05 } #Monarchist

	### Ruling Party
	add_to_array = { ruling_party = 0 }

	startup_politics = yes

	create_country_leader = {
		name = "Transylvanian National Assembly"
		picture = "transylvanian_assembly.dds"
		ideology = Western_Autocracy
		traits = {
			western_Western_Autocracy
		}
	}
}

2017.1.1 = {
	add_ideas = {
		fast_growth
		#Corruption
		unrestrained_corruption
		#Faction 1
		landowners
		#Faction 2
		industrial_conglomerates
		#Faction 3
		international_bankers

		#Bureacracy
		bureau_02
		#Military Spending
		defence_02
		#Internal Security
		police_03
		#Education budget
		edu_03
		#Health budget
		health_03
		#Social spending


		#Trade Law
		semi_consumption_economy
		#Conscription Law
		draft_army
		#Women in the military
		volunteer_women
		#Foreign Intervention Law

		#Officer Training
		officer_military_school

		#other
		pluralist
	}

	set_country_flag = positive_landowners
	set_country_flag = positive_international_bankers
	set_country_flag = positive_industrial_conglomerates

	set_technology = {
		legacy_doctrines = 1
		#For templates
		infantry_weapons = 1

		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		util_vehicle_0 = 1

		body_armor_1980 = 1

		basic_computing = 1
		integrated_circuit = 1
		computing1 = 1
		decryption1 = 1
		encryption1 = 1

		radar = 1
		internet1 = 1 	#1G
		fuel_silos = 1
	}

	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 80 }
	add_to_array = { influence_array = HUN.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 6 }
	add_to_array = { influence_array = ENG.id }
	add_to_array = { influence_array_val = 2 }
	add_to_array = { influence_array = GER.id }
	add_to_array = { influence_array_val = 2 }
	startup_influence = yes

	set_popularities = {
		democratic = 65
		communism = 0
		fascism = 0.0
		neutrality = 15
		nationalist = 20
	}
	set_politics = {
		ruling_party = democratic
		last_election = "2016.11.20"
		election_frequency = 48
		elections_allowed = yes
	}

	start_politics_input = yes

	### Party Popularities
	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0.30 } #conservatism
	set_variable = { party_pop_array^2 = 0.30 } #liberalism
	set_variable = { party_pop_array^3 = 0.05 } #socialism
	set_variable = { party_pop_array^4 = 0.10 } #Communist-State
	set_variable = { party_pop_array^5 = 0 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0 } #oligarchism
	set_variable = { party_pop_array^16 = 0.15 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0.10 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0.05 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0.05 } #Monarchist

	### Ruling Party
	add_to_array = { ruling_party = 0 }

	startup_politics = yes

	create_country_leader = {
		name = "Transylvanian National Assembly"
		picture = "transylvanian_assembly.dds"
		ideology = Western_Autocracy
		traits = {
			western_Western_Autocracy
		}
	}
}
