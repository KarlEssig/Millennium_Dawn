﻿capital = 811
2000.1.1 = {
	set_variable = { gdp_per_capita = 50.170 }
	#load_oob = "CAL_2000"
	##Taking American Tech/Ideas

	add_ideas = {
		#Economic Cycle
		recession
		#Corruption
		medium_corruption
		#Faction 1
		international_bankers
		#Faction 2
		maritime_industry
		#Faction 3
		labour_unions

		#Bureacracy
		bureau_01
		#Military Spending?
		defence_01
		#Internal Security
		police_01
		#Education budget
		edu_01
		#Health budget
		health_01
		#Social spending
		social_01

		#Trade Law
		consumption_economy
		#Conscription Law
		draft_army
		#Women in the military
		volunteer_women
		#Foreign Intervention Law
		intervention_local_security
		#Officer Training
		officer_basic_training

		#Other
		western_country
		christian
	}

	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 90 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 10 }

	startup_influence = yes

	# General Technology Setting
	set_technology = {

		nuclear_technology = 1
		reactor1 = 1
		reactor2 = 1

		integrated_transportation_system = 1
		post_conventional_rail = 1
		early_freight_locomotive = 1

		legacy_doctrines = 1
		combined_arms = 1

		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		infantry_weapons3 = 1
		support_weapons = 1
		squad_automatic_weapon = 1
		tandem_charge_warheads = 1
		support_weapons2 = 1
		support_weapons3 = 1
		special_forces = 1
		special_forces2 = 1
		special_forces3 = 1
		special_forces4 = 1

		combat_eng_equipment = 1

		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1 #1985

		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1
		command_control_equipment3 = 1

		land_Drone_equipment = 1
		land_Drone_equipment1 = 1

		body_armor_1980 = 1
		body_armor_2000 = 1

		camouflage = 1
		camouflage2 = 1

		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1

		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		AT_upgrade_1 = 1
		AT_upgrade_2 = 1
		Anti_tank_1 = 1
		Heavy_Anti_tank_1 = 1
		Anti_tank_2 = 1
		Heavy_Anti_tank_2 = 1


		transport_helicopter1 = 1
		transport_helicopter2 = 1

		landing_craft = 1

		basic_computing = 1
		integrated_circuit = 1
		computing1 = 1
		decryption1 = 1
		encryption1 = 1
		microprocessors = 1
		computing2 = 1
		decryption2 = 1
		encryption2 = 1
		computing3 = 1
		decryption3 = 1
		encryption3 = 1
		DNA_fingerprinting = 1

		radar = 1
		internet1 = 1
		internet2 = 1
		gprs = 1
		fuel_silos = 1

		industrial_electrospun_polymeric_nanofibers = 1
		multiwalled_carbon_nanotubes = 1
		modern_electrospinning = 1
		stereolitography = 1
		early_3D_printers = 1
		t3D_wax_printer = 1
		DNA_fingerprinting = 1
		modern_gmo = 1
		fuel_refining = 1
		fuel_refining2 = 1
		energy_efficiency = 1
		energy_efficiency2 = 1
		fuel_efficiency = 1
		early_renewables = 1
		renewables = 1
		electrification_1 = 1
		construction1 = 1
		stronghold_building_1 = 1
		land_fort_building_1 = 1
		construction2 = 1
		excavation1 = 1
		excavation2 = 1
	}


	if = {
		limit = { has_dlc = "No Step Back" }
		set_technology = {
			mbt_tech = 1
			mbt_tech_1 = 1
			mbt_tech_2 = 1
			tank_components_tech = 1
			tank_components_tech_2 = 1
			tank_components_tech_3 = 1
			small_medium_gun_tech = 1
			small_medium_gun_tech_2 = 1
			medium_gun_tech = 1
			medium_gun_tech_2 = 1
			armor_tech = 1
			armor_tech_2 = 1
			era_tech = 1
			era_tech_2 = 1
			diesel_engine_tech = 1
			diesel_engine_tech_2 = 1
			diesel_engine_tech_3 = 1
			diesel_engine_tech_4 = 1
			turbine_engine_tech = 1
			turbine_engine_tech_2 = 1
			electric_engine_tech_1 = 1
			electric_engine_tech_2 = 1
			afv_tech = 1
			afv_tech_1 = 1
			afv_tech_2 = 1
			small_autocannon_tech = 1
			small_autocannon_tech_2 = 1
			big_autocannon_tech = 1
			big_autocannon_tech_2 = 1
			assault_gun_tech = 1
			assault_gun_tech_2 = 1
			nsb_artillery_0 = 1
			nsb_Arty_upgrade_1 = 1
			nsb_Arty_upgrade_2 = 1
			nsb_Arty_upgrade_3 = 1
			nsb_SP_arty_0 = 1
			nsb_SP_arty_1 = 1
			nsb_artillery_1 = 1
			nsb_SP_arty_2 = 1
			light_tank_tech = 1
			light_tank_tech_1 = 1
			light_tank_tech_2 = 1
			nsb_Anti_Air_0 = 1
			nsb_SP_Anti_Air_0 = 1
			nsb_AA_upgrade_1 = 1
			nsb_Anti_Air_1 = 1
			nsb_SP_Anti_Air_1 = 1
			nsb_AA_upgrade_2 = 1
			nsb_AA_upgrade_3 = 1
			nsb_attack_helicopter_tech_1 = 1
			nsb_attack_helicopter_tech_2 = 1
			nsb_attack_helicopter_tech_3 = 1
		}
		#complete_special_project = sp:sp_21stcentury_thermal

		create_equipment_variant = {
			name = "AH-64D Apache Longbow"
			type = attack_helicopter_hull_2
			parent_version = 1
			modules = {
				helicopter_inner_hardpoint = helicopter_multiple_atgm_gen3
				nose_gun_type_slot = helicopter_heavy_nose_gun_gen2
				armor_type_slot = helicopter_armor_gen2
				engine_type_slot = helicopter_turbine_engine_gen3
				defence_type_slot = flare_launchers
				sensor_and_avionics_slot = sensors_and_avionics_gen_3
				helicopter_middle_hardpoint_1 = helicopter_rocket_pod_gen1
				special_defensive_type_slot_1 = em_lock_detection_system_gen2
			}
			upgrades = {
				helicopter_nsb_upgrade = 5
			}
			icon = "GFX_USA_attack_helicopter3_medium"
			model = "USA_attack_helo_bat_entity"
		}

		create_equipment_variant = {
			name = "M1A2 SEP"
			type = mbt_hull_2
			parent_version = 1
			modules = {
				main_armament_slot = tank_medium_cannon_2
				turret_type_slot = tank_base_tank_turret
				suspension_type_slot = tank_torsion_bar_suspension
				armor_type_slot = tank_composite_armor_gen2
				engine_type_slot = tank_gas_turbine_engine_gen4
				reload_type_slot = manual_loading
				special_type_slot_1 = tank_aux_engine_2
				special_type_slot_2 = smoke_launchers_2
				special_type_slot_3 = wet_ammo_storage
				special_type_slot_4 = tank_battlestation_3
			}
			upgrades = {
				tank_nsb_armor_upgrade = 7
			}
			icon = "gfx/interface/technologies/USA/LAND/MBT4.dds"
			model = "USA_MD4_medium_armor_entity_2"
		}

		create_equipment_variant = {
			name = "M1126 Stryker Combat Vehicle"
			type = apc_hull_2
			parent_version = 0
			modules = {
				main_armament_slot = afv_machine_gun
				turret_type_slot = afv_unmanned_turret
				suspension_type_slot = afv_tracked_suspension
				armor_type_slot = afv_alum_armor_gen2
				engine_type_slot = tank_diesel_engine_gen2
				special_type_slot_1 = smoke_launchers_2
				special_type_slot_2 = afv_reverse_doors
				special_type_slot_3 = tank_aux_engine_2
				special_type_slot_4 = afv_battlestation_3
			}
			upgrades = {
				afv_nsb_armor_upgrade = 2
			}
			icon = "GFX_USA_APC_5"
			model = "Strykerapc_vehicle_entity"
		}

		create_equipment_variant = {
			name = "M2A3 Bradley"
			type = ifv_hull_2
			parent_version = 1
			modules = {
				main_armament_slot = small_autocannon_2
				turret_type_slot = afv_base_turret
				suspension_type_slot = afv_torsion_bar_suspension
				armor_type_slot = afv_steel_armor_gen2
				engine_type_slot = tank_diesel_engine_gen2
				special_type_slot_1 = smoke_launchers_2
				special_type_slot_2 = afv_atgm_gen3
				special_type_slot_3 = afv_reverse_doors
				special_type_slot_4 = afv_battlestation_3
			}
			upgrades = {
				afv_nsb_armor_upgrade = 2
			}
			icon = "GFX_USA_IFV_4"
			model = "USA_M2A3bradley_entity"
		}

		create_equipment_variant = {
			name = "M109A6 Paladin"
			type = spart_hull_1
			parent_version = 6
			modules = {
				arty_main_armament_slot = art_med_gun_gen2
				chassis_type_slot = chassis_afv_gen1
				engine_type_slot = tank_diesel_engine_gen2
				conversion_type_slot = empty
				special_type_slot_1 = support_ammo_medium_gen2
				special_type_slot_2 = cluster_ammo_gun_medium_gen2
				special_type_slot_3 = laser_ammo_gun_medium_gen2
				special_type_slot_4 = art_battlestation_gen3
			}
			upgrades = {
				art_nsb_fire_upgrade = 3
			}
			icon = "gfx/interface/technologies/USA/LAND/SPG3.dds"
		}

		create_equipment_variant = {
			name = "M270"
			type = spart_hull_1
			parent_version = 0
			modules = {
				arty_main_armament_slot = art_med_rocket_gen2
				chassis_type_slot = chassis_afv_gen0
				engine_type_slot = tank_diesel_engine_gen2
				conversion_type_slot = empty
				special_type_slot_1 = cluster_ammo_rocket_medium_gen2
				special_type_slot_2 = mine_rocket_med_gen2
				special_type_slot_3 = laser_ammo_rocket_medium_gen2
				special_type_slot_4 = art_battlestation_gen2
			}
			upgrades = {
				art_nsb_fire_upgrade = 2
			}
			icon = "GFX_USA_spart_hull_r_0"
		}

		create_equipment_variant = {
			name = "M3A3 Bradley CFV"
			type = light_tank_hull_2
			parent_version = 0
			modules = {
				main_armament_slot = apc_small_autocannon_2
				turret_type_slot = afv_base_turret
				chassis_type_slot = chassis_tracked_afv_gen2
				engine_type_slot = tank_diesel_engine_gen3
				armor_type_slot = afv_alum_armor_gen2
				reload_type_slot = light_tank_afv_automatic_loading
				special_armament_type_slot = afv_reverse_doors
				special_type_slot_2 = smoke_launchers_2
				special_type_slot_3 = afv_atgm_gen3
				special_type_slot_4 = afv_battlestation_3
			}
			upgrades = {
				afv_nsb_armor_upgrade = 2
			}
			icon = "GFX_USA_IFV_9_medium"
			model = "USA_M2A3bradley_entity"
		}

		create_equipment_variant = {
			name = "M6 Linebacker"
			type = spaa_hull_1
			parent_version = 0
			modules = {
				main_armament_slot = spaa_light_autocannon2
				chassis_type_slot = spaa_chassis_afv_gen1
				engine_type_slot = tank_diesel_engine_gen4
				radar_type_slot = spaa_optical_guidance_gen2
				spaa_secondary_armament = spaa_support_missile_system_2
				special_type_slot_1 = spaced_armor_gen2
				special_type_slot_2 = smoke_launchers_2
				special_type_slot_3 = empty
				special_type_slot_4 = spaa_battlestation_gen3
			}
			upgrades = {
				spaa_nsb_fire_upgrade = 1
			}
			icon = "gfx/interface/technologies/USA/LAND/Linebacker.dds"
		}

		create_equipment_variant = {
			name = "Colt M16A4"
			type = infantry_weapons3
			upgrades = {
				Inf_eq_gadjets = 2
				Inf_eq_heavy_weapons = 2
			}
		}
		create_equipment_variant = {
			name = "FGM-148C Javelin"
			type = Anti_tank_2 #Upgraded Javelin
			upgrades = {
				L_AT_Fire_Control = 3
				L_AT_Warhead = 3
				L_AT_Reliability = 3
			}
		}
		create_equipment_variant = {
			name = "FIM-92E Stinger RMP Block I"
			type = Anti_Air_1 #Upgraded Stinger
			upgrades = {
			}
		}
		create_equipment_variant = {
			name = "Textron C4I"
			type = command_control_equipment3
			upgrades = {
			Cnc_eq_better_ergonomics = 1
			}
		}
			create_equipment_variant = {
			name = "UH-60A"
			type = transport_helicopter2
			upgrades = {
			}
		}

		complete_special_project = sp:sp_armoured_vehicle_project

		else = {
			set_technology = {
				MBT_1 = 1
				MBT_2 = 1
				MBT_3 = 1
				MBT_4 = 1
				Early_APC = 1
				APC_1 = 1
				APC_2 = 1
				APC_3 = 1
				APC_4 = 1
				IFV_1 = 1
				IFV_2 = 1
				IFV_3 = 1
				IFV_4 = 1
				artillery_0 = 1
				Arty_upgrade_1 = 1
				Arty_upgrade_2 = 1
				Arty_upgrade_3 = 1
				SP_arty_1 = 1
				SP_arty_0 = 1
				SP_R_arty_0 = 1
				artillery_1 = 1
				SP_arty_1 = 1
				SP_R_arty_1 = 1
				Rec_tank_0 = 1
				Rec_tank_1 = 1
				Anti_Air_0 = 1
				SP_Anti_Air_0 = 1
				AA_upgrade_1 = 1
				Anti_Air_1 = 1
				SP_Anti_Air_1 = 1
				AA_upgrade_2 = 1
				Anti_Air_2 = 1
				AA_upgrade_3 = 1
				attack_helicopter1 = 1
				attack_helicopter2 = 1
			}

			create_equipment_variant = {
				name = "Dragoon 300"
				type = APC_3 #Dragoon 300
				upgrades = {
				}
			}
			create_equipment_variant = {
				name = "M133A2"
				type = APC_2
				upgrades = {
					tank_reliability_upgrade = 1
					tank_engine_upgrade = 1
				}
			}
			create_equipment_variant = {
				name = "HIMARS"
				type = SP_R_arty_2
				upgrades = {
					SP_Arty_Fire_Control = 2
					SP_Arty_Warhead = 2
				}
			}
			create_equipment_variant = { ##Hey, NSB team, this followed me home to BBA. I think it's yours, though :p
				name = "M3 Bradley"
				type = Rec_tank_2 #M3 Bradley
				upgrades = {
					tank_reliability_upgrade = 0
					tank_engine_upgrade = 0
					tank_armor_upgrade = 0
					tank_gun_upgrade = 0
				}
			}
		}
	}

	# Naval Technology
	if = { limit = { has_dlc = "Man the Guns" }
		set_technology = {
			corvette_hull_1 = 1
			corvette_hull_2 = 1
			corvette_hull_3 = 1

			frigate_hull_1 = 1
			frigate_hull_2 = 1
			frigate_hull_3 = 1

			destroyer_hull_1 = 1
			destroyer_hull_2 = 1

			cruiser_hull_1 = 1
			cruiser_hull_2 = 1

			battleship_hull_1 = 1

			helicopter_operator_hull_1 = 1
			helicopter_operator_hull_2 = 1

			carrier_hull_1 = 1
			carrier_hull_2 = 1

			attack_submarine_hull_1 = 1
			attack_submarine_hull_2 = 1
			attack_submarine_hull_3 = 1

			missile_submarine_hull_1 = 1
			missile_submarine_hull_2 = 1
			missile_submarine_hull_3 = 1

			aircraft_carrier_engineering = 1
			aircraft_heli_engineering_1 = 1
			aircraft_heli_engineering_2 = 1

			tech_submarine_engineering = 1
			tech_submarine_engineering_1 = 1
			tech_submarine_engineering_2 = 1

			tech_naval_systems_engineering = 1
			tech_power_systems = 1
			tech_power_systems_1 = 1
			tech_nuclear_power_systems = 1
			tech_nuclear_power_systems_1 = 1

			tech_combat_support_systems = 1
			tech_fire_control = 1
			tech_fire_control_1 = 1
			tech_combat_radar = 1
			tech_combat_radar_1 = 1
			tech_combat_radar_2 = 1
			tech_combat_sonar = 1
			tech_combat_sonar_1 = 1
			tech_combat_sonar_2 = 1

			tech_early_naval_weapon_systems = 1
			tech_naval_weapon_systems = 1
			tech_light_guns = 1
			tech_light_guns_1 = 1

			tech_point_defense_system = 1
			tech_point_defense_system_1 = 1
			tech_point_defense_system_2 = 1
			tech_point_defense_system_3 = 1

			tech_turret_missiles_surface = 1
			tech_turret_missiles_surface_1 = 1
			tech_turret_missiles_surface_2 = 1

			tech_turret_missiles_aa = 1
			tech_turret_missiles_aa_1 = 1
			tech_turret_missiles_aa_2 = 1

			tech_vls_surface = 1
			tech_vls_surface_1 = 1
			tech_vls_surface_2 = 1

			tech_vls_aa = 1
			tech_vls_aa_1 = 1
			tech_vls_aa_2 = 1

			tech_torpedoes = 1
			tech_torpedoes_1 = 1
			tech_torpedoes_2 = 1

			tech_mtg_landing_craft = 1
			tech_mtg_amphibious_assault_ship = 1
			tech_mtg_air_cushioned_landing_craft = 1
			tech_mtg_modern_landing_craft = 1
		}

		# Iowa Class
		create_equipment_variant = {
			name = "Iowa Class"
			type = battleship_hull_1
			#name_group = USA_BB_HISTORICAL
			role_icon_index = 7
			parent_version = 0
			modules = {
				fixed_ship_engine_slot = module_surface_oil_power
				fixed_ship_fire_control_system_slot = module_analog_fire_control
				fixed_ship_radar_slot = module_radar_2
				fixed_ship_auxillary_slot_3 = empty
				fixed_ship_auxillary_slot_2 = module_127mm_guns_1
				fixed_ship_auxillary_slot_1 = module_127mm_guns_1
				fixed_ship_battery_slot = module_127mm_guns_1
				front_1_custom_slot = module_heavy_guns_1
				front_2_custom_slot = module_heavy_guns_1
				mid_1_custom_slot = empty
				mid_2_custom_slot = empty
				mid_3_custom_slot = empty
				rear_1_custom_slot = empty
				rear_2_custom_slot = empty
			}
		}
		#Nimitz Class
		create_equipment_variant = {
			name = "Nimitz Class RCOH"
			type = carrier_hull_2
			name_group = USA_CV_HISTORICAL
			role_icon_index = 13
			parent_version = 0
			modules = {
				fixed_ship_engine_slot = module_surface_adv_reactor_power
				fixed_ship_fire_control_system_slot = module_digital_integrated_fire_control
				fixed_ship_radar_slot = module_radar_4
				fixed_ship_auxillary_slot_2 = module_ciws_3
				fixed_ship_auxillary_slot_1 = module_aa_missiles_3
				fixed_ship_auxillary_slot = module_esm_1
				fixed_ship_flight_deck = module_flight_deck_6
				front_1_custom_slot = module_ram_3
				front_2_custom_slot = module_ciws_3
				mid_1_custom_slot = module_helipads_2
				mid_2_custom_slot = module_helipads_2
				mid_3_custom_slot = module_helipads_2
				rear_1_custom_slot = module_ciws_3
				rear_2_custom_slot = module_ram_3
			}
			model = "USA_carrier_2_entity"
		}
		#San Antonio Class
		create_equipment_variant = {
			name = "San Antonio Class"
			type = helicopter_operator_hull_2
			name_group = USA_LPD_HISTORICAL
			role_icon_index = 8
			parent_version = 1
			modules = {
				fixed_ship_engine_slot = module_surface_jet_turbine_power
				fixed_ship_fire_control_system_slot = module_digital_integrated_fire_control
				fixed_ship_radar_slot = module_radar_3
				fixed_ship_auxillary_slot_3 = module_vls_sam_3
				fixed_ship_auxillary_slot_2 = module_ram_3
				fixed_ship_auxillary_slot = module_ciws_3
				fixed_ship_battery_slot = module_light_flight_deck_1
				front_1_custom_slot = module_helipads_2
				mid_1_custom_slot = module_fuel_tank
				rear_1_custom_slot = module_helipads_2
			}
			icon = "gfx/interface/technologies/USA/NAV/LPD2.dds"
			model = "USA_sanantonio_entity"
		}
		#Ticonderoga Class
		create_equipment_variant = {
			name = "Ticonderoga Class - Mark 41 VLS Variant"
			type = cruiser_hull_2
			name_group = USA_MODERN_CRUISERS_NAMES
			role_icon_index = 5
			parent_version = 1
			modules = {
				fixed_ship_engine_slot = module_surface_oil_power
				fixed_ship_fire_control_system_slot = module_digital_integrated_fire_control
				fixed_ship_radar_slot = module_radar_4
				fixed_ship_battery_slot_4 = module_torpedoes_2
				fixed_ship_battery_slot_3 = module_vls_lam_2
				fixed_ship_battery_slot_2 = module_ciws_3
				fixed_ship_battery_slot = module_127mm_guns_2
				front_1_custom_slot = module_127mm_guns_2
				front_2_custom_slot = module_vls_sam_3
				mid_1_custom_slot = module_ciws_3
				mid_2_custom_slot = module_asm_3
				rear_1_custom_slot = module_vls_asm_3
				rear_2_custom_slot = module_light_helipad_1
			}
			icon = "gfx/interface/technologies/USA/NAV/CA4.dds"
			model = "USA_cruiser_entity"
		}
		#Arleigh Burke Class
		create_equipment_variant = {
			name = "Arleigh Burke Class Flight IIA"
			type = destroyer_hull_2
			name_group = USA_MODERN_DESTROYERS_NAMES
			role_icon_index = 4
			parent_version = 2
			modules = {
				fixed_ship_engine_slot = module_surface_jet_turbine_power
				fixed_ship_fire_control_system_slot = module_digital_integrated_fire_control
				fixed_ship_radar_slot = module_radar_4
				fixed_ship_auxillary_slot_3 = module_vls_lam_2
				fixed_ship_auxillary_slot_2 = module_torpedoes_2
				fixed_ship_auxillary_slot = module_esm_1
				fixed_ship_battery_slot = module_127mm_guns_2
				front_1_custom_slot = module_vls_sam_3
				mid_1_custom_slot = module_vls_sam_3
				mid_2_custom_slot = module_ciws_3
				rear_1_custom_slot = module_light_helipad_1
			}
		}
		#Oliver Hazard Perry Class
		create_equipment_variant = {
			name = "Oliver Hazard Perry Class"
			type = frigate_hull_2
			name_group = USA_MODERN_FRIGATE_NAMES
			role_icon_index = 2
			parent_version = 0
			modules = {
				fixed_ship_engine_slot = module_light_surface_diesel_power
				fixed_ship_fire_control_system_slot = module_analog_fire_control
				fixed_ship_radar_slot = module_sonar_2
				fixed_ship_auxillary_slot_2 = module_asm_1
				fixed_ship_auxillary_slot_1 = module_aa_missiles_2
				fixed_ship_auxillary_slot = module_torpedoes_1
				fixed_ship_battery_slot = module_76mm_gun_2
				front_1_custom_slot = module_ciws_2
				rear_1_custom_slot = module_light_helipad_1
			}
		}

		#Ratanakosin Class https://en.wikipedia.org/wiki/Badr-class_corvette
		create_equipment_variant = {
			name = "Ratanakosin Class"
			type = corvette_hull_3
			name_group = USA_MODERN_CORVETTE_NAMES
			parent_version = 0
			modules = {
				fixed_ship_engine_slot = module_light_surface_diesel_power
				fixed_ship_fire_control_system_slot = module_analog_fire_control
				fixed_ship_radar_slot = module_radar_2
				fixed_ship_auxillary_slot = module_torpedoes_2
				fixed_ship_battery_slot = module_76mm_gun_2
				front_1_custom_slot = module_chain_gun
				rear_1_custom_slot = module_chain_gun
			}
		}
		#Seawolf Class
		create_equipment_variant = {
			name = "Seawolf Class"
			type = attack_submarine_hull_3
			name_group = USA_MODERN_SUBMARINES_NAMES
			role_icon_index = 15
			parent_version = 0
			modules = {
				fixed_ship_engine_slot = module_sub_mod_reactor_power
				fixed_ship_radar_slot = module_sonar_4
				fixed_ship_auxillary_slot_3 = module_torpedoes_2
				fixed_ship_auxillary_slot_2 = module_sub_esm_1
				fixed_ship_auxillary_slot_1 = module_torpedoes_2
				fixed_ship_auxillary_slot = module_vls_sub_asm_3
				fixed_ship_battery_slot = module_anti_ship_torpedoes_2
			}
			model = "USA_attack_submarine_3_entity"
			icon = "gfx/interface/technologies/USA/NAV/SSN4.dds"
		}
		#Ohio Class
		create_equipment_variant = {
			name = "Ohio Class"
			type = missile_submarine_hull_2
			name_group = USA_MISSILE_SUBMARINES_NAMES
			role_icon_index = 16
			parent_version = 0
			modules = {
				fixed_ship_engine_slot = module_sub_adv_reactor_power
				fixed_ship_radar_slot = module_sonar_4
				fixed_ship_auxillary_slot_3 = module_torpedoes_2
				fixed_ship_auxillary_slot_2 = module_sub_esm_1
				fixed_ship_auxillary_slot_1 = module_vls_sub_lam_2
				fixed_ship_auxillary_slot = module_vls_sub_lam_2
				fixed_ship_battery_slot = module_anti_ship_torpedoes_2
			}
			icon = "gfx/interface/technologies/USA/NAV/SSBN3.dds"
			model = "USA_missile_submarine_entity"
		}

		complete_special_project = sp:sp_naval_vessel_project
		complete_special_project = sp:sp_medium_naval_nuclear_engines

		else = {
			set_technology = {
				corvette_1 = 1
				corvette_2 = 1
				missile_corvette_1 = 1
				missile_corvette_2 = 1

				frigate_1 = 1
				frigate_2 = 1
				missile_frigate_1 = 1
				missile_frigate_2 = 1

				destroyer_1 = 1
				destroyer_2 = 1
				missile_destroyer_1 = 1
				missile_destroyer_2 = 1 #Arleigh Burke-class destroyer flight II

				cruiser_1 = 1
				cruiser_2 = 1
				missile_cruiser_1 = 1
				N_B_Cruiser_2 = 1

				submarine_1 = 1
				missile_submarine_1 = 1
				missile_submarine_2 = 1
				missile_submarine_3 = 1

				attack_submarine_1 = 1
				attack_submarine_2 = 1
				attack_submarine_3 = 1

				diesel_attack_submarine_1 = 1

				modern_carrier_0 = 1
				Nuclear_carrier_0 = 1
				Nuclear_carrier_1 = 1

				LPD_0 = 1 #Austin-class
				LPD_1 = 1 #San Antonio-class

				LHA_0 = 1 #Tarawa-class
				LHA_1 = 1 #Wasp class
			}

			create_equipment_variant = {
				name = "Lütjens-class"
				type = destroyer_1 #SPz 11-2 Kurz
				upgrades = {
				}
			}
			create_equipment_variant = {
				name = "Gabya-class"
				type = frigate_2 #Gabya-class
				upgrades = {
				}
			}
			create_equipment_variant = {
				name = "Enterprise Class"
				type = Nuclear_carrier_1 #USS Enterprise
				upgrades = {
				}
			}
			create_equipment_variant = {
				name = "Los Angeles i-Class"
				type = attack_submarine_3
				upgrades = {
				}
			}
			create_equipment_variant = {
				name = "Seawolf-Class"
				type = attack_submarine_3
				upgrades = {
				}
			}
			create_equipment_variant = {
				name = "Badr-Class"
				type = corvette_2 #Badr-Class
				upgrades = {
				}
			}
		}
	}

	# Air Technology

	if = {
		limit = { has_dlc = "By Blood Alone" }

			set_technology = {
				early_airframe_designs = 1
				gen_3_light = 1
				gen_4_light = 1
				gen_3_medium = 1
				gen_4_medium = 1
				gen_5_medium = 1
				gen_3_large = 1
				gen_4_large = 1
				gen_5_large = 1

				flying_wing_design = 1
				variable_wing_design = 1
				delta_wing_design = 1
				diamond_wing_design = 1

				avionics_1 = 1
				avionics_2 = 1

				drone_1 = 1
				suicide_drone_1 = 1

				awacs_1 = 1
				awacs_2 = 1

				engines_2 = 1
				engines_3 = 1

				refueling_1 = 1
				refueling_2 = 1

				redundant_1 = 1

				early_weapons = 1
				air_weapons_1 = 1
				air_weapons_2 = 1
				countermeasures_1 = 1
				countermeasures_2 = 1

				ag_weapons_1 = 1
				ag_weapons_2 = 1

				gunship_1 = 1
				gunship_2 = 1

				naval_weapons_1 = 1
				naval_weapons_2 = 1
				special_mad_1 = 1
				special_mad_2 = 1

				tgp_recon_1 = 1
				tgp_recon_2 = 1

				stealth_tech_1 = 1
				internal_weapons_1 = 1
			}

			##BBA Designs ##

			####Foreign Variants####

			#Medium Fighters

			create_equipment_variant = { #Med Fighter BRUH BRUH BRUH
				name = "F-15E Strike Eagle"
				type = medium_plane_airframe_2
				parent_version = 1
				modules = {
					fixed_main_weapon_slot = weap_mp_med_hardpoint_2
					fixed_gun_slot = weap_multi_gun_2
					engine_type_slot = engine_jet_double_3
					avionics_type_slot = avionics_manned_3
					wingform_type_slot = wing_delta
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_tgp_2
					special_slot_type_3 = spec_refuel_3
					fixed_auxiliary_weapon_slot_1 = weap_ag_cas_hardpoint_3
					fixed_auxiliary_weapon_slot_2 = weap_a2a_hardpoint_2
				}
				upgrades = {
					plane_bba_radar_upgrade = 2
				}
				icon = "gfx/interface/technologies/USA/AIR/STR3.dds"
				model = "USA_medium_plane_airframe_2_entity"
			}

			create_equipment_variant = { #Med Bomber
				name = "A-10A Thunderbolt II"
				type = medium_plane_cas_airframe_2
				parent_version = 0
				modules = {
					fixed_main_weapon_slot = weap_ag_cas_hardpoint_1
					fixed_gun_slot = weap_multi_at_2
					engine_type_slot = engine_jet_double_2
					avionics_type_slot = avionics_manned_2
					wingform_type_slot = wing_straight
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_protection_1
					special_slot_type_3 = spec_tgp_1
					fixed_auxiliary_weapon_slot_1 = weap_ag_cas_hardpoint_1
					fixed_auxiliary_weapon_slot_2 = weap_mp_light_hardpoint_2
				}
				upgrades = {
					plane_bba_engine_upgrade = 1
				}
				icon = "gfx/interface/technologies/USA/AIR/CAS1.dds"
				model = "USA_medium_plane_cas_airframe_2_entity"
			}

			create_equipment_variant = { #Med Air Superiority
				name = "F-22 Raptor"
				type = medium_plane_fighter_airframe_3
				parent_version = 0
				modules = {
					fixed_main_weapon_slot = weap_int_a2a_hardpoint_1
					fixed_gun_slot = weap_multi_gun_2
					engine_type_slot = engine_jet_double_3
					avionics_type_slot = avionics_manned_3
					wingform_type_slot = wing_diamond
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_stealthmats_1
					special_slot_type_3 = spec_stealthshape_1
					special_slot_type_4 = spec_thrust_vector_2
					fixed_auxiliary_weapon_slot_1 = weap_int_mp_light_hardpoint_2
					fixed_auxiliary_weapon_slot_2 = weap_int_a2a_hardpoint_1
				}
				upgrades = {
					plane_bba_radar_upgrade = 3
					plane_bba_engine_upgrade = 2
				}
				icon = "gfx/interface/technologies/USA/AIR/AS4.dds"
				model = "USA_medium_plane_fighter_airframe_3_entity"
			}

			#Small/light Fighters

			create_equipment_variant = { #Light Fighter
				name = "F-16C Blk 50/52"
				type = small_plane_strike_airframe_2
				parent_version = 2
				modules = {
					fixed_main_weapon_slot = weap_mp_med_hardpoint_2
					fixed_gun_slot = weap_multi_gun_2
					fixed_auxiliary_weapon_slot_1 = weap_a2a_hardpoint_2
					engine_type_slot = engine_light_single_3
					avionics_type_slot = avionics_manned_3
					wingform_type_slot = wing_delta
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_tgp_2
					special_slot_type_3 = spec_refuel_3
				}
				upgrades = {
					plane_bba_radar_upgrade = 4
					plane_bba_engine_upgrade = 3
				}
				icon = "gfx/interface/technologies/USA/AIR/MR3.dds"
				model = "USA_small_plane_airframe_2_entity"
			}

			create_equipment_variant = { #Light CV Fighter
				name = "AV-8B Harrier II"
				type = cv_small_plane_strike_airframe_2
				parent_version = 0
				modules = {
					fixed_main_weapon_slot = weap_mp_light_hardpoint_2
					fixed_gun_slot = weap_multi_gun_2
					fixed_auxiliary_weapon_slot_1 = weap_a2a_hardpoint_2
					engine_type_slot = engine_light_single_2
					avionics_type_slot = avionics_manned_3
					wingform_type_slot = wing_delta
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_thrust_vector_1
					special_slot_type_3 = spec_tgp_2
				}
				upgrades = {
					plane_bba_engine_upgrade = 1
				}
				icon = "gfx/interface/technologies/USA/AIR/LSTRCV1.dds"
				model = USA_cv_small_plane_airframe_2_entity
			}
			create_equipment_variant = { #Light Atk
				name = "OV-10 Bronco"
				type = small_plane_strike_airframe_1
				parent_version = 0
				modules = {
					fixed_main_weapon_slot = weap_mp_med_hardpoint_2
					fixed_gun_slot = weap_multi_gun_2
					engine_type_slot = engine_prop_double_1
					avionics_type_slot = avionics_manned_2
					wingform_type_slot = wing_straight
					special_slot_type_1 = spec_countermeasures_1
				}
				icon = "gfx/interface/technologies/USA/AIR/T1.dds"
			}


			#MPA
			create_equipment_variant = { #ASW/ASuW Large
				name = "P-3 Orion"
				type = large_plane_maritime_patrol_airframe_1
				parent_version = 0
				modules = {
					fixed_main_weapon_slot = weap_as_hardpoint_2
					fixed_gun_slot = empty
					engine_type_slot = engine_prop_quad_2
					avionics_type_slot = avionics_manned_2
					wingform_type_slot = wing_straight
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_mad_2
					special_slot_type_3 = spec_refuel_2
					fixed_auxiliary_weapon_slot_1 = weap_as_hardpoint_2
					fixed_auxiliary_weapon_slot_2 = weap_as_hardpoint_2
				}
				icon = "gfx/interface/technologies/USA/AIR/NAV2.dds"
				model = "USA_large_plane_maritime_patrol_airframe_2_entity"
			}

			#Strat Bombers
			create_equipment_variant = { #Strat Bomber
				name = "B-52H Stratofortress"
				type = large_plane_airframe_1
				parent_version = 0
				modules = {
					fixed_main_weapon_slot = weap_ag_strat_2
					fixed_gun_slot = empty
					engine_type_slot = engine_jet_eight_1
					avionics_type_slot = avionics_manned_2
					wingform_type_slot = wing_swept
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_refuel_1
					special_slot_type_3 = spec_refuel_3
					fixed_auxiliary_weapon_slot_1 = weap_ag_strat_2
					fixed_auxiliary_weapon_slot_2 = weap_ag_cas_hardpoint_3
				}
				icon = "gfx/interface/technologies/USA/AIR/B1.dds"
				model = "USA_large_plane_airframe_1_entity"
			}

			#Gunship

			create_equipment_variant = { #Strat Bomber
				name = "AC-130U Spooky"
				type = large_plane_cas_airframe_1
				parent_version = 1
				modules = {
					fixed_main_weapon_slot = weap_ag_gunship_2
					fixed_gun_slot = weap_ag_gunship_1
					engine_type_slot = engine_prop_quad_2
					avionics_type_slot = avionics_manned_3
					wingform_type_slot = wing_straight
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_redundant_2
					special_slot_type_3 = spec_refuel_3
					fixed_auxiliary_weapon_slot_1 = weap_ag_gunship_1
					fixed_auxiliary_weapon_slot_2 = weap_ag_gunship_1
				}
				icon = "gfx/interface/technologies/USA/AIR/C3.dds"
			}

			#TRANSPORT
			create_equipment_variant = { #Large Transport
				name = "C-130J Super Hercules"
				type = large_plane_air_transport_airframe_1
				parent_version = 1
				modules = {
					fixed_main_weapon_slot = weap_buff_transport
					fixed_gun_slot = empty
					engine_type_slot = engine_prop_quad_2
					avionics_type_slot = avionics_manned_3
					wingform_type_slot = wing_straight
					special_slot_type_1 = spec_countermeasures_2
					special_slot_type_2 = spec_refuel_3
					special_slot_type_3 = spec_refuel_1
					fixed_auxiliary_weapon_slot_1 = empty
					fixed_auxiliary_weapon_slot_2 = empty
				}
				icon = "gfx/interface/technologies/USA/AIR/C1.dds"
				model = "USA_large_plane_air_transport_airframe_1_entity"
			}
			create_equipment_variant = { #XLarge Transport
				name = "C-141 Starlifter"
				type = large_plane_air_transport_airframe_1
				parent_version = 0
				modules = {
					fixed_main_weapon_slot = weap_buff_transport
					fixed_gun_slot = empty
					engine_type_slot = engine_jet_quad_2
					avionics_type_slot = avionics_manned_1
					wingform_type_slot = wing_swept
					special_slot_type_1 = spec_countermeasures_1
					special_slot_type_2 = spec_refuel_2
					special_slot_type_3 = spec_redundant_1
					fixed_auxiliary_weapon_slot_1 = empty
					fixed_auxiliary_weapon_slot_2 = empty
				}
				icon = "gfx/interface/technologies/USA/AIR/C1.dds"
			}

			complete_special_project = sp:sp_aircraft_project
			complete_special_project = sp:sp_stealth_technology



			## End BBA Designs ##

		else = {
			set_technology = {
				early_fighter = 1
				Strike_fighter1 = 1
				Strike_fighter2 = 1
				Strike_upgrade_1 = 1
				Strike_fighter3 = 1

				MR_Fighter1 = 1
				MR_Fighter2 = 1
				MR_upgrade_1 = 1
				MR_Fighter3 = 1

				AS_Fighter1 = 1
				AS_Fighter2 = 1
				AS_upgrade_1 = 1
				AS_Fighter3 = 1

				CV_MR_Fighter1 = 1
				CV_MR_Fighter2 = 1
				CV_MR_Fighter3 = 1

				Air_UAV1 = 1

				L_Strike_fighter1 = 1
				L_Strike_fighter2 = 1

				CV_L_Strike_fighter1 = 1
				CV_L_Strike_fighter2 = 1

				early_bomber = 1
				strategic_bomber1 = 1
				strategic_bomber2 = 1
				strategic_bomber3 = 1
				strategic_bomber4 = 1

				transport_plane1 = 1
				transport_plane2 = 1
				transport_plane3 = 1
				transport_plane4 = 1

				naval_plane1 = 1
				naval_plane2 = 1
				naval_plane3 = 1

				cas1 = 1
				cas2 = 1

				awacs_plane1 = 1
				cv_awacs_plane1 = 1
			}

			create_equipment_variant = {
				name = "S-3 Viking"
				type = naval_plane1 #S-3 Viking
				upgrades = {

				}
			}
			create_equipment_variant = {
				name = "OV-10A Bronco"
				type = L_Strike_fighter1 #OV-10A Bronco
				upgrades = {

				}
			}

		}
	}

	set_variable = { var = treasury value = 10 }
	set_variable = { var = corporate_tax_rate value = 30 }
	set_variable = { var = population_tax_rate value = 15 }

	set_popularities = {
		democratic = 70.0
		fascism = 0.0
		communism = 10.0
		neutrality = 10.0
		nationalist = 10.0
	}
	set_politics = {
		ruling_party = democratic
		last_election = "1996.11.5"
		election_frequency = 48
		elections_allowed = yes
	}

	start_politics_input = yes
	set_variable = { party_pop_array^0 = 0.05 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0.05 } #conservatism
	set_variable = { party_pop_array^2 = 0.25 } #liberalism
	set_variable = { party_pop_array^3 = 0.10 } #socialism
	set_variable = { party_pop_array^4 = 0.10 } #Communist-State
	set_variable = { party_pop_array^5 = 0.02 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0.03 } #Conservative
	set_variable = { party_pop_array^7 = 0.05 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0.05 } #oligarchism
	set_variable = { party_pop_array^16 = 0.10 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0.025 } #Neutral_green
	set_variable = { party_pop_array^18 = 0.025 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0.15 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0.00 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0.00 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0.00 } #Monarchist
	add_to_array = { ruling_party = 2 }
	startup_politics = yes

	recruit_character = CAL_air_chief
}

