generic_coalition_politics_decisions = {
	# Portugal Expand Mines
	# Altenjo
	POR_expand_the_neves_corvo_mine = {
		allowed = {
			OR = {
				original_tag = POR
				original_tag = SPR
			}
		}
		icon = GFX_decision_tungsten

		cost = 75

		days_remove = 120
		fire_only_once = yes

		visible = {
			has_full_control_of_state = 1056
		}
		available = {
			has_full_control_of_state = 1056
			has_tech = excavation3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision POR_expand_the_neves_corvo_mine"
			set_temp_variable = { treasury_change = -4.25 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove POR_expand_the_neves_corvo_mine"
			add_resource = {
				type = tungsten
				amount = 6
				state = 1056
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				add = 10
				check_variable = { resource_imported@tungsten > 7 }
			}
			modifier = {
				factor = 0
				has_active_mission = bankruptcy_incoming_collapse
			}
		}
	}

	# Central Portugal
	POR_expand_the_panasqueira_mines = {
		allowed = {
			OR = {
				original_tag = POR
				original_tag = SPR
			}
		}
		icon = GFX_decision_tungsten

		cost = 75

		days_remove = 120
		fire_only_once = yes

		visible = {
			has_full_control_of_state = 97
		}
		available = {
			has_full_control_of_state = 97
			has_tech = excavation3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision POR_expand_the_panasqueira_mines"
			set_temp_variable = { treasury_change = -8.00 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove POR_expand_the_panasqueira_mines"
			add_resource = {
				type = steel
				amount = 6
				state = 97
			}
			add_resource = {
				type = tungsten
				amount = 6
				state = 97
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				add = 10
				check_variable = { resource_imported@steel > 7 }
			}
			modifier = {
				add = 10
				check_variable = { resource_imported@tungsten > 7 }
			}
			modifier = {
				factor = 0
				has_active_mission = bankruptcy_incoming_collapse
			}
		}
	}

	# Northern Portugal
	POR_expand_the_mua_mine = {
		allowed = {
			OR = {
				original_tag = POR
				original_tag = SPR
			}
		}
		icon = GFX_decision_tungsten

		cost = 75

		days_remove = 120
		fire_only_once = yes

		visible = {
			has_full_control_of_state = 1055
		}
		available = {
			has_full_control_of_state = 1055
			has_tech = excavation3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision POR_expand_the_mua_mine"
			set_temp_variable = { treasury_change = -6.25 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove POR_expand_the_mua_mine"
			add_resource = {
				type = steel
				amount = 10
				state = 1055
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				add = 10
				check_variable = { resource_imported@steel > 7 }
			}
			modifier = {
				factor = 0
				has_active_mission = bankruptcy_incoming_collapse
			}
		}
	}
}