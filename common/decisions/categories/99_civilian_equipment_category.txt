civilian_equipment_purchase_decisions = {
	icon = GFX_decision_category_generic_arms_trade

	visible = {
		ROOT = {
			num_of_military_factories < 5
		}
	}

	priority = 0
}