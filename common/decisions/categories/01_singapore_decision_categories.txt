SIN_government_of_singapore = {
	allowed = { original_tag = SIN }
	icon = decision_category
	scripted_gui = singaporean_alignment
	visible_when_empty = yes
	priority = 100
}

SIN_expand_the_entreport_decision_categories = {
	allowed = { original_tag = SIN }
	icon = decision_category
	priority = 95
	visible = {
		has_completed_focus = SIN_expand_the_entrepot
	}

}