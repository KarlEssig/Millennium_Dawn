GEO_cis_eu_influence_decision = {
	icon = GFX_decision_category_georgian_republics

	picture = GFX_decision_eaeu_eu_rival
	priority = 200
	allowed = { original_tag = GEO }

	visible_when_empty = yes
}

ARM_GEO_rebellion_category = {
	icon = GFX_decision_category_usa_intervention_mandate

	picture = GFX_decision_djavakhq_prot_plakat
	priority = 150
	allowed = {
		OR = {
			original_tag = ARM
			original_tag = GEO
		}
	}
	visible = {
		has_capitulated = no
		is_subject = no
		NOT = {
			GEO = { has_country_flag = ARM_GEO_stop_thing }
			ARM = { has_country_flag = ARM_GEO_stop_thing }
		}
	}
}
