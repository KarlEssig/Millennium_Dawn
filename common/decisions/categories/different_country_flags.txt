different_country_flags_category = {

	icon = GFX_decision_category_generic_formable_nations

	allowed = { always = yes }
	visible = {
		NOT = {
			AND = {
				has_global_flag = european_federation
				has_idea = EU_member
			}
			has_country_flag = isratine_formed
			has_country_flag = collapsed_nation
			has_country_flag = is_GERMAN
			has_country_flag = is_CHINA
			has_country_flag = is_IRAN
			has_country_flag = is_TAJIKISTAN
			has_country_flag = is_IRAQ
			has_country_flag = is_UAR
			has_country_flag = is_BLT
			has_country_flag = is_FCA
			has_country_flag = is_GCL
			has_country_flag = is_SCA
			has_country_flag = is_IBR
			has_country_flag = is_KOS
			has_country_flag = TUR_pan_turkey
			has_country_flag = is_VANGUARD
			has_country_flag = TUR_osmani
			has_country_flag = is_IKR_PER
			has_country_flag = is_SUN_CARTEL
			has_country_flag = is_CUB_communists_nationalists_in_power
			has_country_flag = Saudi_balls
		}
	}
}