NKO_united_front_department = {
	icon = GFX_decisions_category_nko_unite_korea
	scripted_gui = korea_reunion_gui
	allowed = { original_tag = NKO }
	visible_when_empty = yes

	priority = 150
}

NKO_black_market = {
	icon = GFX_decisions_category_nko_black_market
	allowed = { original_tag = NKO }
	scripted_gui = third_floor_gui
	priority = 150
	visible = {
		always = yes

		}
	}

NKO_Arduous_March = {
	icon = GFX_nko_decisions_category_march

	allowed = { original_tag = NKO }
	picture = GFX_decision_korea_arduous_march

	priority = 150

	visible = {
		has_dynamic_modifier = { modifier = arduous_march_modifiers }
	}
}

NKO_Power_struggle = {
	icon = GFX_decisions_category_nko_power_stuggle
	visible = {
		if = {
			has_completed_focus = NKO_Struggle_for_Power
		}
		NOT = {
			OR = {
				has_completed_focus = NKO_Kim_Jong_Nam
				has_completed_focus = NKO_Kim_Jong_un
				has_completed_focus = NKO_Junta
			}
		}
	}
	allowed = { original_tag = NKO }

	priority = 150

}


NKO_rus_friend_category = {
	icon = GFX_nko_decisions_category_russians
	scripted_gui = russia_friendship_gui
	priority = 150
	allowed = { 
		OR = {
			original_tag = NKO 
			original_tag = SOV
		}
	}
}

