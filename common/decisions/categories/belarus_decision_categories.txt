BLR_whitelegion_battalion_category = {
	icon = GFX_decisions_category_whitelegion
	priority = 100
	allowed = {	original_tag = BLR }
	visible = {
		OR = {
			nationalist_right_wing_populists_are_in_power = yes
			nationalist_monarchists_are_in_power = yes
		}
		has_completed_focus = BLR_white_legion_battalion
	}
}
BLR_belarus_support_category = {
	icon = GFX_decisions_category_blr_help
	priority = 100
	allowed = {	original_tag = BLR }
	visible = {
		OR = {
		has_completed_focus = BLR_Brigade_Territorial_Defence
		has_completed_focus = BLR_Internal_troops
		}
	}
}

BLR_lith_revol_category = {
	icon = GFX_decision_category_litbel
	priority = 110
	picture = GFX_decision_belarus_litbel
	allowed = {	original_tag = BLR }
	visible = {
		has_completed_focus = BLR_the_litbel_start
	}
}

BLR_lat_revol_category = {
	icon = GFX_decision_category_litbel
	priority = 110
	picture = GFX_decision_belarus_litbel
	allowed = {	original_tag = BLR }
	visible = {
		has_completed_focus = BLR_litbel_attack_lat
	}
}
BLR_Duchy_claims_category = {
	icon = GFX_decision_category_pozniak
	priority = 110
	picture = GFX_decision_belarus_claims
	allowed = {	original_tag = BLR }
	visible = {
		has_completed_focus = BLR_Territorial_claims
	}
}
BLR_pol_revol_category = {
	icon = GFX_decision_generic_arrest
	priority = 110
	picture = GFX_decision_belarus_in_poland_revoult
	allowed = {	original_tag = BLR }
	visible = {
		has_completed_focus = BLR_pro_poland_propoganda
	}
}
BLR_Poland_divide_category = {
	icon = GFX_decision_generic_arrest
	priority = 110
	picture = GFX_decision_belarus_belorusisation
	allowed = {	original_tag = BLR }
	visible = {
		has_completed_focus = BLR_ynia_Poland
	}
}
BLR_Litva_belarusizations_category = {
	icon = GFX_decisions_category_belorussian
	priority = 110
	picture = GFX_decision_belorus_belorussia_litva
	allowed = { original_tag = BLR }
	visible = {
		has_completed_focus = BLR_soviet_system
	}
}
BLR_Litva_belarusization_category = {
	icon = GFX_decision_category_pozniak
	priority = 110
	picture = GFX_decision_belarus_belorusisation
	allowed = {	original_tag = BLR }
	visible = {
		has_completed_focus = BLR_GDL
	}
}
BLR_Lukashenko_category = {
	icon = GFX_decision_category_lykashenko_regim
	priority = 130
	picture = GFX_decision_belarus_lykashenko_regime
	allowed = {
		emerging_reactionaries_are_in_power = yes
		original_tag = BLR
	}
	visible = {
		has_completed_focus = BLR_Lykashenko
	}
}
BLR_Kommunarka_category = {
	icon = GFX_decision_category_kommunarka
	priority = 110
	picture = GFX_decision_belarus_kommunarka
	allowed = {
		emerging_reactionaries_are_in_power = yes
		original_tag = BLR
	}
	visible = {
		has_completed_focus = BLR_Factory_Kommunarka
	}
}
BLR_Azot_category = {
	icon = GFX_decision_category_azot
	picture = GFX_decision_belarus_grodno_azot
	priority = 30
	allowed = {	
		OR = {
			original_tag = BLR 
			original_tag = SOV
		}
	}
	visible = {
		OR = {
			AND = {
				original_tag = BLR 
				BLR = { owns_state = 704 }
				has_completed_focus = BLR_GrodnoAzot
			}
			AND = {
				original_tag = SOV
				has_completed_focus = SOV_economic_grodno_annex
				SOV = { owns_state = 704 }
			}
		}
	}
}
BLR_Tracktor_category = {
	priority = 90
	picture = GFX_decision_belarus_mtz_zavod
	allowed = {	original_tag = BLR }
	icon = GFX_decision_category_mtz
		visible = {
		has_completed_focus = BLR_mtz
	}
}
BLR_Minotor_category = {
	priority = 90
	picture = GFX_decision_belarus_minotor_zavod
	allowed = {	original_tag = BLR }
	icon = GFX_decision_category_minotor
		visible = {
		has_completed_focus = BLR_minoitor
	}
}
BLR_558_zavod_category = {
	priority = 90
	icon = GFX_decision_category_558zadov
	allowed = {	original_tag = BLR }
	picture = GFX_decision_belarus_558
	visible = {
		has_completed_focus = BLR_558_zavod
	}
}
BLR_peleng_category = {
	priority = 90
	allowed = {	original_tag = BLR }
	icon = GFX_decision_category_peleng
		picture = GFX_decision_belarus_peleng_zavod
	visible = {
		has_completed_focus = BLR_peleng
	}
}
BLR_Foreign_azot_trade_category = {
	icon = GFX_decision_category_azot
	picture = GFX_decision_belarus_grodno_azot
	visible = {
		has_idea = BLR_azot_fertilizers_idea
	}
}
BLR_mzkt_category = {
	priority = 90
	allowed = {	original_tag = BLR }
	icon = GFX_decision_category_mzkt
	picture = GFX_decision_belarus_mzkt_zavod
	visible = {
		has_completed_focus = BLR_MZKT
	}
}
BLR_belvesh_category = {
	priority = 90
	allowed = {	original_tag = BLR }
	icon = GFX_decision_category_belspec
	picture = GFX_decision_belarus_belspec
	visible = {
		has_completed_focus = BLR_belspecveshtech
	}
}
BLR_140zadov_category = {
	priority = 90
	allowed = {	original_tag = BLR }
	icon = GFX_decision_category_140
	picture = GFX_decision_belarus_140zavod
	visible = {
		has_completed_focus = BLR_140_zavod
	}
}

