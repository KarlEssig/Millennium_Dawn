SOV_mvd_category = {
	allowed = {
		original_tag = SOV
	}
	priority = 160
	icon = GFX_decisions_category_sov_mvd
	visible_when_empty = yes
	scripted_gui = rusmvd_gui
	visible = {
		original_tag = SOV
	}
}
SOV_economy_category = {
	scripted_gui = ruseconomic_gui
	priority = 200
	icon = GFX_decision_category_economic
	allowed = {
		original_tag = SOV
	}
	visible_when_empty = yes
	visible = { always = yes }
}

SOV_army_category = {
	scripted_gui = rusarmy_gui
	priority = 200
	icon = GFX_decisions_category_sov_army
	allowed = {
		original_tag = SOV
	}
	visible_when_empty = yes
	visible = { always = yes }
}

SOV_democracy_taiwam_category = {
	allowed = {
		original_tag = SOV
	}
	priority = 100
	icon = GFX_decision_category_taiwan
	visible = {
		has_completed_focus = SOV_expand_trade_with_taiwan
	}
}
SOV_500_days_category = {
	allowed = {
		original_tag = SOV
	}
	priority = 60
	icon = GFX_decision_500_days_cate
	picture = GFX_decision_555photo
	visible = {
		has_completed_focus = SOV_capitalism_in_500_days
		NOT = { has_country_flag = SOV_reforms_done
		has_country_flag = SOV_reforms_failed }
	}
}
SOV_restoration_of_influence_category = {
	icon = GFX_decisions_category_warsaw
	priority = 270

	allowed = {
		original_tag = SOV
	}
	visible = {
		AND = {
			emerging_communist_state_are_in_power = yes
			has_completed_focus = SOV_restoration_of_warsaw_pact
		}
	}
}
SOV_warsaw_pact_decision_category = {
	icon = GFX_decision_category_sov_great_patriotic_war
	picture = GFX_decision_kavkazrevol_econplakat
	priority = 250

	allowed = {
		original_tag = SOV
	}
	visible = {
		AND = {
			has_completed_focus = SOV_restoration_of_warsaw_pact
			OR = {
				POL = {
					has_autonomy_state = autonomy_warsaw
				}
				CZE = {
					has_autonomy_state = autonomy_warsaw
				}
				SLO = {
					has_autonomy_state = autonomy_warsaw
				}
				HUN = {
					has_autonomy_state = autonomy_warsaw
				}
				ROM = {
					has_autonomy_state = autonomy_warsaw
				}
				BUL = {
					has_autonomy_state = autonomy_warsaw
				}
				ALB = {
					has_autonomy_state = autonomy_warsaw
				}
			}
		}
	}
}
SOV_fbk_organization_category = {

	icon = GFX_decisions_category_fbk
	priority = 240

	allowed = {
		original_tag = SOV
	}
	visible = {
		has_completed_focus = SOV_Make_the_Anti-Corruption_Foundation_a_State_Company
	}
}

sov_reunification_category = {
	priority = 200
	allowed = {
		original_tag = SOV
	}

	visible = { has_country_flag = SOV_dissolution_flag }
}

SOV_forbidden_bazaar = {

	icon = GFX_decision_generic_form_nation

	allowed = {
		original_tag = SOV
	}

	visible = {
		TAJ = {
			has_country_flag = TAJ_drugs_into_russia
		}
	}

	priority = {
		base = 1000
	}
}
