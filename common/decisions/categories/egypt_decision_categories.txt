EGY_slums_decision = {
	icon = GFX_decision_slums_egyptos
	priority = 200
	allowed = {
		original_tag = EGY
	}
}

EGY_coptic_decision = {
	icon = GFX_decision_coptic_decis
	picture = GFX_decisions_copts
	priority = 190
	allowed = {
		original_tag = EGY
	}
	visible = {
		NOT = {
			has_country_flag = copts_revolted
		}
	}

	visible_when_empty = yes
}

EGY_military_decision = {
	icon = GFX_decision_egypt_militar
	priority = 190
	allowed = {
		original_tag = EGY
	}

	visible_when_empty = yes
}

EGY_evergreen_category = {
	icon = GFX_decision_category_investment
	picture = GFX_decision_evergreen_company_photo
	priority = 30
	allowed = {	original_tag = EGY }
	visible = {
		has_completed_focus = EGY_evergrow
	}
}
