mp_decision_category = {
	icon = generic_crisis
	allowed = {
		is_ai = no
		has_game_rule = {
			rule = allow_mp_optimizations
			option = yes
		}
	}

	priority = 250
}

mp_factories_decision_category = {
	icon = generic_crisis
	allowed = {
		is_ai = no
		has_game_rule = {
			rule = rule_allow_free_factories
			option = yes
		}
	}

	priority = 249
}