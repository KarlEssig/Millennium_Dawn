african_union_decisions_category = {

	African_Union_join_the_union = {

		cost = 100

		visible = {
			NOT = { has_idea = AU_member }
		}

		available = {
			has_war = no
			jihadist_government = no
			nationalist_military_junta_are_not_in_power = yes
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision African_Union_join_the_union"
			add_ideas = AU_member
		}

		ai_will_do = {
			factor = 100
			modifier = {
				factor = 0
				original_tag = MOR
			}
		}

	}

	African_Union_leave_the_union = {

		cost = 100

		visible = {
			has_idea = AU_member
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision African_Union_join_the_union"
			remove_ideas = AU_member
		}

		ai_will_do = {
			factor = 0
		}
	}

}