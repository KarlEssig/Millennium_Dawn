decisions_ENG = {
	falkland_policy = {
		visible = {
			country_exists = ARG
			NOT = { has_country_flag = falkland_policy }
			has_full_control_of_state = 26
		}

		available = {
			NOT = { has_war_with = ARG }
			is_subject = no
		}

		icon = GFX_decision_falkland_islands

		cost = 50
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision falkland_policy"
			set_country_flag = falkland_policy
			country_event = United_Kingdom.1
		}

		ai_will_do = {
			base = 0

			modifier = {
				add = 1
				is_historical_focus_on = no
			}
			modifier = {
				add = 1
				ARG = { is_subject_of = ENG }
			}
			modifier = {
				factor = 0
				OR = {
					is_historical_focus_on = yes
					date < 2003.6.1
				}
			}
		}
	}
}