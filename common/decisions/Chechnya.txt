CHE_nationalise_caucas_category = {
	CHE_nationalise_armenia = {
		icon = GFX_decision_connection_kom
		allowed = { original_tag = CHE }
		available = {
			NOT = {
				country_exists = ARM
			}
		}
		cost = 80
		fire_only_once = yes
		days_remove = 270
		visible = {
			owns_state = 709
			owns_state = 1034
			owns_state = 1035
			owns_state = 711
		}
		remove_effect = {
			ARM = { every_core_state = { add_core_of = CHE } }
		}

	}

	CHE_nationalise_azer = {
		icon = GFX_decision_connection_kom
		allowed = { original_tag = CHE }
		available = {
			NOT = {
				country_exists = AZE
			}
		}
		cost = 80
		fire_only_once = yes
		days_remove = 270
		visible = {
			owns_state = 713
			owns_state = 712
			owns_state = 1038
		}
		remove_effect = {
			AZE = { every_core_state = { add_core_of = CHE } }
		}

	}
	CHE_nationalise_georgia = {
		icon = GFX_decision_connection_kom
		allowed = { original_tag = CHE }
		available = {
			NOT = {
				country_exists = GEO
			}
		}
		cost = 80
		fire_only_once = yes
		days_remove = 270
		visible = {
			owns_state = 707
			owns_state = 708
			owns_state = 1033
			owns_state = 466
		}
		remove_effect = {
			GEO = { every_core_state = { add_core_of = CHE } }
		}

	}
	CHE_nationalise_abkhazia = {
		icon = GFX_decision_connection_kom
		allowed = { original_tag = CHE }
		available = {
			NOT = {
				country_exists = ABK
			}
		}
		cost = 80
		fire_only_once = yes
		days_remove = 270
		visible = {
			owns_state = 706
		}
		remove_effect = {
			ABK = { every_core_state = { add_core_of = CHE } }
		}

	}
	CHE_nationalise_karabah = {
		icon = GFX_decision_connection_kom
		allowed = { original_tag = CHE }
		available = {
			NOT = {
				country_exists = NKR
			}
		}
		cost = 40
		fire_only_once = yes
		days_remove = 180
		visible = {
			owns_state = 1037
			owns_state = 710
			owns_state = 1036
		}
		remove_effect = {
			NKR = { every_core_state = { add_core_of = CHE } }
		}

	}
	CHE_nationalise_ossetia = {
		icon = GFX_decision_connection_kom
		allowed = { original_tag = CHE }
		available = {
			NOT = {
				country_exists = SOO
			}
		}
		cost = 40
		fire_only_once = yes
		days_remove = 150
		visible = {
			owns_state = 705
		}
		remove_effect = {
			SOO = { every_core_state = { add_core_of = CHE } }
		}
	}
}
CHE_caucasian_revol_category = {
	#azerbaijan
	CHE_Establish_connections_aze = {
		icon = GFX_decision_connection_kom
		allowed = {
			original_tag = CHE
		}
		available = {
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 25
		fire_only_once = yes
		days_remove = 10
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			set_country_flag = Establish_connections_aze_kom
			AZE = {
				hidden_effect = {
					add_popularity = {
						ideology = neutrality
						popularity = 0.03
					}
					add_to_variable = { party_pop_array^19 = 0.05 }
					recalculate_party = yes
				}
			}
		}
		complete_effect = {
			clr_country_flag = zsr_aze_revol2
		}
	}
	CHE_Financing_of_protest_movements_aze = {
		icon = GFX_decision_zsr_unrest
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_aze_revol2 }
			}
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_aze_kom
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			AZE = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.10 }
				set_temp_variable = { temp_outlook_increase = 0.10 }
				add_relative_party_popularity = yes
				add_stability = -0.07
			}
			clr_country_flag = zsr_aze_revol2
		}
		complete_effect = {
			set_country_flag = zsr_aze_revol2
		}
	}
	CHE_Strengthen_com_partys_agenda = {
		icon = GFX_decision_zsr_propaganda
		allowed = {
			original_tag = CHE
		}
		available = {
		hidden_trigger = {
			NOT = { has_country_flag = zsr_aze_revol2 }
			}
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_aze_kom
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 2.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			AZE = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.10 }
				set_temp_variable = { temp_outlook_increase = 0.10 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_aze_revol2
		}
		complete_effect = {
			set_country_flag = zsr_aze_revol2
		}
	}
	CHE_parlament_aze_places = {
		icon = GFX_decision_zsr_parlament
		allowed = {
			original_tag = CHE
		}
		available = {
		hidden_trigger = {
			NOT = { has_country_flag = zsr_aze_revol2 }
			}
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 250
		fire_only_once = yes
		days_remove = 150
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_aze_kom
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 3.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			AZE = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.10 }
				set_temp_variable = { temp_outlook_increase = 0.10 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_aze_revol2
		}
		complete_effect = {
			set_country_flag = zsr_aze_revol2
		}
	}
	CHE_aze_revol = {
		icon = GFX_decision_zsr_revol
		allowed = {
			original_tag = CHE
		}
		available = {
			AZE = {
				has_government = neutrality
				neutrality > 0.3
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^19 > 0.30 }
					tooltip = aze_more_than_30_support_for_communists_zsr_TT
				}
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 100
		fire_only_once = yes
		days_remove = 70
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			AZE = {
				neutrality_neutral_communism_are_not_in_power = yes
				has_government = neutrality
				neutrality > 0.15
				custom_trigger_tooltip = {
				check_variable = { party_pop_array^19 > 0.15 }
				tooltip = aze_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 5.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			AZE = {
				set_temp_variable = { rul_party_temp = 19 }
				if = {
					limit = { has_elections = no }
					set_temp_variable = { enable_elections = 1 }
				}
				else = {
					set_temp_variable = { enable_elections = 0 }
				}
				change_ruling_party_effect = yes

				add_popularity = {
					ideology = neutrality
					popularity = 0.35
				}
				add_to_variable = { party_pop_array^19 = 0.5 }
				recalculate_party = yes
				start_civil_war = {
					ideology = democratic
					size = 0.3
				}
			}
		}
	}
	#georgia
	CHE_Establish_connections_geo = {
		icon = GFX_decision_connection_kom
		allowed = {
			original_tag = CHE
		}
		available = {
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 25
		fire_only_once = yes
		days_remove = 10
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			set_country_flag = Establish_connections_geo_kom
			GEO = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.05 }
				set_temp_variable = { temp_outlook_increase = 0.05 }
				add_relative_party_popularity = yes
			}
		}
		complete_effect = {
			clr_country_flag = zsr_geo_revol2
		}
	}
	CHE_Financing_of_protest_movements_geo = {
		icon = GFX_decision_zsr_unrest
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_geo_revol2 }
			}
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_geo_kom
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			GEO = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.10 }
				set_temp_variable = { temp_outlook_increase = 0.10 }
				add_relative_party_popularity = yes
				add_stability = -0.07
			}
			clr_country_flag = zsr_geo_revol2
		}
		complete_effect = {
			set_country_flag = zsr_geo_revol2
		}
	}
	CHE_Strengthen_com_partys_agenda_geo = {
		icon = GFX_decision_zsr_propaganda
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_geo_revol2 }
			}
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_geo_kom
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 2.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			GEO = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.10 }
				set_temp_variable = { temp_outlook_increase = 0.10 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_geo_revol2
		}
		complete_effect = {
			set_country_flag = zsr_geo_revol2
		}
	}
	CHE_parlament_geo_places = {
		icon = GFX_decision_zsr_parlament
		allowed = {
			original_tag = CHE
		}
		available = {
		hidden_trigger = {
			NOT = { has_country_flag = zsr_geo_revol2 }
			}
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 250
		fire_only_once = yes
		days_remove = 150
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_geo_kom
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 3.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			GEO = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_geo_revol2
		}
		complete_effect = {
			set_country_flag = zsr_geo_revol2
		}
	}
	CHE_geo_revol = {
		icon = GFX_decision_zsr_revol
		allowed = {
			original_tag = CHE
		}
		available = {
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
				has_government = neutrality
				neutrality > 0.3
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^4 > 0.30 }
					tooltip = aze_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		cost = 100
		fire_only_once = yes
		days_remove = 70
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			GEO = {
				neutrality_neutral_communism_are_not_in_power = yes
				has_government = neutrality
				neutrality > 0.15
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^19 > 0.15 }
					tooltip = geo_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 5.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			GEO = {
				set_temp_variable = { rul_party_temp = 19 }
				if = {
					limit = { has_elections = no }
					set_temp_variable = { enable_elections = 1 }
				}
				else = {
					set_temp_variable = { enable_elections = 0 }
				}
				change_ruling_party_effect = yes

				add_popularity = {
					ideology = neutrality
					popularity = 0.35
				}
				add_to_variable = { party_pop_array^4 = 0.5 }
				recalculate_party = yes
				start_civil_war = {
					ideology = democratic
					size = 0.3
				}
			}
		}
	}
	#Armenia
	CHE_Establish_connections_arm = {
		icon = GFX_decision_connection_kom
		allowed = {
			original_tag = CHE
		}
		available = {
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 25
		fire_only_once = yes
		days_remove = 10
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_country_flag = Establish_connections_arm_kom
			ARM = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.05 }
				set_temp_variable = { temp_outlook_increase = 0.05 }
				add_relative_party_popularity = yes
			}
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
		}
		complete_effect = {
			clr_country_flag = zsr_arm_revol2
		}
	}
	CHE_Financing_of_protest_movements_arm = {
		icon = GFX_decision_zsr_unrest
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_arm_revol2 }
			}
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_arm_kom
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			ARM = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.10 }
				set_temp_variable = { temp_outlook_increase = 0.10 }
				add_relative_party_popularity = yes
				add_stability = -0.07
			}
			clr_country_flag = zsr_arm_revol2
		}
		complete_effect = {
			set_country_flag = zsr_arm_revol2
		}
	}
	CHE_Strengthen_com_partys_agenda_arm = {
		icon = GFX_decision_zsr_propaganda
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_arm_revol2 }
			}
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_arm_kom
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 2.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			ARM = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.10 }
				set_temp_variable = { temp_outlook_increase = 0.10 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_arm_revol2
		}
		complete_effect = {
			set_country_flag = zsr_arm_revol2
		}
	}
	CHE_parlament_arm_places = {
		icon = GFX_decision_zsr_parlament
		allowed = {
			original_tag = CHE
		}
		available = {
		hidden_trigger = {
			NOT = { has_country_flag = zsr_arm_revol2 }
			}
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		cost = 250
		fire_only_once = yes
		days_remove = 150
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			has_country_flag = Establish_connections_arm_kom
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 4.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			ARM = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_arm_revol2
		}
		complete_effect = {
			set_country_flag = zsr_arm_revol2
		}
	}
	CHE_arm_revol = {
		icon = GFX_decision_zsr_revol
		allowed = {
			original_tag = CHE
		}
		available = {
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
				has_government = neutrality
				neutrality > 0.3
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^4 > 0.30 }
					tooltip = aze_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		cost = 100
		fire_only_once = yes
		days_remove = 70
		visible = {
			neutrality_neutral_communism_are_in_power = yes
			ARM = {
				neutrality_neutral_communism_are_not_in_power = yes
				has_government = neutrality
				neutrality > 0.15
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^19 > 0.15 }
					tooltip = geo_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 5.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			ARM = {
				set_temp_variable = { rul_party_temp = 19 }
				if = {
					limit = { has_elections = no }
					set_temp_variable = { enable_elections = 1 }
				}
				else = {
					set_temp_variable = { enable_elections = 0 }
				}
				change_ruling_party_effect = yes

				add_popularity = {
					ideology = neutrality
					popularity = 0.35
				}
				add_to_variable = { party_pop_array^4 = 0.5 }
				recalculate_party = yes
				start_civil_war = {
					ideology = democratic
					size = 0.3
				}
			}
		}
	}
}
CHE_caucasians_revol_category = {
	#azerbaijan
	CHE_Establish_connectionsz_aze = {
		icon = GFX_decision_connection_com
		allowed = {
			original_tag = CHE
		}
		available = {
			AZE = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 25
		fire_only_once = yes
		days_remove = 10
		visible = {
			emerging_communist_state_are_in_power = yes
			AZE = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			set_country_flag = Establish_connections_aze_com
			AZE = {
				hidden_effect = {
					add_popularity = {
						ideology = communism
						popularity = 0.03
					}
					add_to_variable = { party_pop_array^4 = 0.05 }
					recalculate_party = yes
				}
			}
		}
		complete_effect = {
			clr_country_flag = zsr_aze_revol2
		}
	}
	CHE_Financing_of_protest_movementsz_aze = {
		icon = GFX_decision_zsr_unrest
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_aze_revol2 }
			}
			AZE = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_aze_com
			AZE = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			AZE = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
				add_stability = -0.07
			}
			clr_country_flag = zsr_aze_revol2
		}
		complete_effect = {
			set_country_flag = zsr_aze_revol2
		}
	}
	CHE_Strengthen_com_partysz_agenda = {
		icon = GFX_decision_zsr_propaganda
		allowed = {
			original_tag = CHE
		}
		available = {
		hidden_trigger = {
			NOT = { has_country_flag = zsr_aze_revol2 }
			}
			AZE = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_aze_com
			AZE = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 2.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			AZE = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_aze_revol2
		}
		complete_effect = {
			set_country_flag = zsr_aze_revol2
		}
	}
	CHE_parlament_azez_places = {
		icon = GFX_decision_zsr_parlament
		allowed = {
			original_tag = CHE
		}
		available = {
		hidden_trigger = {
			NOT = { has_country_flag = zsr_aze_revol2 }
			}
			AZE = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 250
		fire_only_once = yes
		days_remove = 150
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_aze_com
			AZE = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 3.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			AZE = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_aze_revol2
		}
		complete_effect = {
			set_country_flag = zsr_aze_revol2
		}
	}
	CHE_azez_revol = {
		icon = GFX_decision_zsr_revol
		allowed = {
			original_tag = CHE
		}
		available = {
			AZE = {
				has_government = communism
				communism > 0.3
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^4 > 0.30 }
					tooltip = aze_more_than_30_support_for_communists_zsr_TT
				}
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 100
		fire_only_once = yes
		days_remove = 70
		visible = {
			emerging_communist_state_are_in_power = yes
			AZE = {
				emerging_communist_state_are_not_in_power = yes
				has_government = communism
				communism > 0.15
				custom_trigger_tooltip = {
				check_variable = { party_pop_array^4 > 0.15 }
				tooltip = aze_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 5.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			AZE = {
				set_temp_variable = { rul_party_temp = 4 }
				if = {
					limit = { has_elections = no }
					set_temp_variable = { enable_elections = 1 }
				}
				else = {
					set_temp_variable = { enable_elections = 0 }
				}
				change_ruling_party_effect = yes

				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.25 }
				set_temp_variable = { temp_outlook_increase = 0.25 }
				add_relative_party_popularity = yes

				start_civil_war = {
					ideology = democratic
					size = 0.3
				}
			}
		}
	}
	#georgia
	CHE_Establish_connectionsz_geo = {
		icon = GFX_decision_connection_com
		allowed = {
			original_tag = CHE
		}
		available = {
			GEO = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 25
		fire_only_once = yes
		days_remove = 10
		visible = {
			emerging_communist_state_are_in_power = yes
			GEO = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			set_country_flag = Establish_connections_geo_com
			GEO = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.05 }
				set_temp_variable = { temp_outlook_increase = 0.05 }
				add_relative_party_popularity = yes
			}
		}
		complete_effect = {
			clr_country_flag = zsr_geo_revol2
		}
	}
	CHE_Financing_of_protest_movementsz_geo = {
		icon = GFX_decision_zsr_unrest
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_geo_revol2 }
			}
			GEO = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_geo_com
			GEO = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			GEO = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
				add_stability = -0.07
			}
			clr_country_flag = zsr_geo_revol2
		}
		complete_effect = {
			set_country_flag = zsr_geo_revol2
		}
	}
	CHE_Strengthen_com_partys_agendaz_geo = {
		icon = GFX_decision_zsr_propaganda
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_geo_revol2 }
			}
			GEO = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_geo_com
			GEO = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 2.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			GEO = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_geo_revol2
		}
		complete_effect = {
			set_country_flag = zsr_geo_revol2
		}
	}
	CHE_parlament_geoz_places = {
		icon = GFX_decision_zsr_parlament
		allowed = {
			original_tag = CHE
		}
		available = {
		hidden_trigger = {
			NOT = { has_country_flag = zsr_geo_revol2 }
			}
			GEO = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 250
		fire_only_once = yes
		days_remove = 150
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_geo_com
			GEO = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 3.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			GEO = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_geo_revol2
		}
		complete_effect = {
			set_country_flag = zsr_geo_revol2
		}
	}
	CHE_geoz_revol = {
		icon = GFX_decision_zsr_revol
		allowed = {
			original_tag = CHE
		}
		available = {
			GEO = {
				emerging_communist_state_are_not_in_power = yes
				has_government = communism
				communism > 0.3
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^4 > 0.30 }
					tooltip = aze_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		cost = 100
		fire_only_once = yes
		days_remove = 70
		visible = {
			emerging_communist_state_are_in_power = yes
			GEO = {
				emerging_communist_state_are_not_in_power = yes
				has_government = communism
				communism > 0.15
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^4 > 0.15 }
					tooltip = geo_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 5.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			GEO = {
				set_temp_variable = { rul_party_temp = 4 }
				if = {
					limit = { has_elections = no }
					set_temp_variable = { enable_elections = 1 }
				}
				else = {
					set_temp_variable = { enable_elections = 0 }
				}
				change_ruling_party_effect = yes

				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.25 }
				set_temp_variable = { temp_outlook_increase = 0.25 }
				add_relative_party_popularity = yes

				start_civil_war = {
					ideology = democratic
					size = 0.3
				}
			}
		}
	}
	#Armenia
	CHE_Establish_connectionsz_arm = {
		icon = GFX_decision_connection_com
		allowed = {
			original_tag = CHE
		}
		available = {
			ARM = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 25
		fire_only_once = yes
		days_remove = 10
		visible = {
			emerging_communist_state_are_in_power = yes
			ARM = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_country_flag = Establish_connections_arm_com
			ARM = {
				set_temp_variable = { party_index = 19 }
				set_temp_variable = { party_popularity_increase = 0.05 }
				set_temp_variable = { temp_outlook_increase = 0.05 }
				add_relative_party_popularity = yes
			}
			set_temp_variable = { percent_change = 1.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
		}
		complete_effect = {
			clr_country_flag = zsr_arm_revol2
		}
	}
	CHE_Financing_of_protest_movementsz_arm = {
		icon = GFX_decision_zsr_unrest
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_arm_revol2 }
			}
			ARM = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_arm_com
			ARM = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			ARM = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
				add_stability = -0.07
			}
			clr_country_flag = zsr_arm_revol2
		}
		complete_effect = {
			set_country_flag = zsr_arm_revol2
		}
	}
	CHE_Strengthen_com_partys_agendaz_arm = {
		icon = GFX_decision_zsr_propaganda
		allowed = {
			original_tag = CHE
		}
		available = {
			hidden_trigger = {
				NOT = { has_country_flag = zsr_arm_revol2 }
			}
			ARM = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 50
		fire_only_once = no
		days_remove = 25
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_arm_com
			ARM = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 2.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -0.25 }
			modify_treasury_effect = yes
			ARM = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_arm_revol2
		}
		complete_effect = {
			set_country_flag = zsr_arm_revol2
		}
	}
	CHE_parlament_armz_places = {
		icon = GFX_decision_zsr_parlament
		allowed = {
			original_tag = CHE
		}
		available = {
		hidden_trigger = {
			NOT = { has_country_flag = zsr_arm_revol2 }
			}
			ARM = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		cost = 250
		fire_only_once = yes
		days_remove = 150
		visible = {
			emerging_communist_state_are_in_power = yes
			has_country_flag = Establish_connections_arm_com
			ARM = {
				emerging_communist_state_are_not_in_power = yes
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 4.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			ARM = {
				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.08 }
				set_temp_variable = { temp_outlook_increase = 0.08 }
				add_relative_party_popularity = yes
			}
			clr_country_flag = 	zsr_arm_revol2
		}
		complete_effect = {
			set_country_flag = zsr_arm_revol2
		}
	}
	CHE_armz_revol = {
		icon = GFX_decision_zsr_revol
		allowed = {
			original_tag = CHE
		}
		available = {
			ARM = {
				emerging_communist_state_are_not_in_power = yes
				has_government = communism
				communism > 0.3
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^4 > 0.30 }
					tooltip = aze_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		cost = 100
		fire_only_once = yes
		days_remove = 70
		visible = {
			emerging_communist_state_are_in_power = yes
			ARM = {
				emerging_communist_state_are_not_in_power = yes
				has_government = communism
				communism > 0.15
				custom_trigger_tooltip = {
					check_variable = { party_pop_array^4 > 0.15 }
					tooltip = geo_more_than_30_support_for_communists_zsr_TT
				}
			}
		}
		remove_effect = {
			set_temp_variable = { percent_change = 5.11 }
			set_temp_variable = { tag_index = CHE }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			ARM = {
				set_temp_variable = { rul_party_temp = 4 }
				if = {
					limit = { has_elections = no }
					set_temp_variable = { enable_elections = 1 }
				}
				else = {
					set_temp_variable = { enable_elections = 0 }
				}
				change_ruling_party_effect = yes

				set_temp_variable = { party_index = 4 }
				set_temp_variable = { party_popularity_increase = 0.15 }
				set_temp_variable = { temp_outlook_increase = 0.15 }
				add_relative_party_popularity = yes

				start_civil_war = {
					ideology = democratic
					size = 0.3
				}
			}
		}
	}
}