public_war_wariness_decision_category = {
	AB_offer_stalemate_Atk = {
		icon = GFX_decisions_cease_fire_button
		target_array = enemies
		target_trigger = {
			FROM = { has_defensive_war_with = ROOT } #Defender
			has_offensive_war_with = FROM #Attacker
		}

		days_re_enable = 60

		available = {
			has_offensive_war_with = FROM #attacker
			NOT = {
				has_idea = three_months_of_war
				has_idea = recent_ceasefire_attempt
			}
		}
		visible = {
			NOT = { tag = ISI }
			has_offensive_war_with = FROM #attacker
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision AB_offer_stalemate_Atk target: [From.GetName]"
			hidden_effect = { save_event_target_as = AB_event_target set_country_flag = FC_with_@FROM }
			FROM = {
				hidden_effect = { set_country_flag = FC_with_@ROOT }
				country_event = { id = anti_bully.5 days = 1 }
			}
			effect_tooltip = {
				if = {
					limit = { #If you're in a faction, not a major and not the leader
						is_in_faction = yes
						is_faction_leader = no
						any_allied_country = { #Faction leader must have capitulated for me to see this decision
							has_war_together_with = ROOT
							is_faction_leader = yes
							has_capitulated = no
						}
					}
					custom_effect_tooltip = anti_bully.tt
					if = {
						limit = { has_idea = NATO_member }
						remove_ideas = NATO_member
					}
				}
			}
		}
	}

	AB_offer_stalemate_Def = {
		icon = GFX_decisions_cease_fire_button
		target_array = enemies
		target_trigger = {
			from = { has_offensive_war_with = ROOT } #attacker
			has_defensive_war_with = FROM #defender
		}
		available = {
			has_defensive_war_with = FROM #attacker
			NOT = {
				has_idea = three_months_of_war
				has_idea = recent_ceasefire_attempt
			}
		}

		days_re_enable = 60

		visible = {
			has_defensive_war_with = FROM #attacker

			# Custom Nation Content
			NOT = { tag = ISI }
			if = { limit = { original_tag = AFG }
				NOT = {
					has_war_with = TAL
				}
			}
			if = { limit = { original_tag = TAL }
				NOT = { has_war_with = AFG }
			}
			if = { limit = { original_tag = SER }
				NOT = { has_war_with = KOS }
			}
			if = { limit = { original_tag = KOS }
				NOT = { has_war_with = SER }
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision AB_offer_stalemate_Def target: [From.GetName]"
			hidden_effect = { save_event_target_as = AB_event_target set_country_flag = FC_with_@FROM }
			from = {
				hidden_effect = { set_country_flag = FC_with_@ROOT }
				country_event = { id = anti_bully.5 days = 1 }
			}
			effect_tooltip = {
				if = {
					limit = { #If you're in a faction, not a major and not the leader
						is_in_faction = yes
						is_faction_leader = no
						any_allied_country = { #Faction leader must have capitulated for me to see this decision
							has_war_together_with = ROOT
							is_faction_leader = yes
							has_capitulated = no
						}
					}
					custom_effect_tooltip = anti_bully.tt
					if = {
						limit = { has_idea = NATO_member }
						remove_ideas = NATO_member
					}
				}
			}
		}
	}
	AB_war_support_campaigns = {
		cost = 100
		icon = GFX_decisions_war_campaign_button
		available = {
			has_war_support < 0.80
			has_war = yes
		}
		visible = {
			has_war = yes
		}
		days_remove = 90
		days_re_enable = 30
		fire_only_once = no
		modifier = {
			war_support_weekly = 0.001
		}

		ai_will_do = {
			base = 5
			modifier = {
				add = 25
				has_war_support < 0.20
			}
			modifier = {
				add = 25
				has_war_support < 0.35
			}
			modifier = {
				add = 25
				has_war_support < 0.50
			}
		}
	}
	AB_promises_of_pecae = {
		cost = 100
		icon = GFX_decisions_promice_peace_button
		available = {
			has_war = yes
			has_stability < 0.80
		}
		visible = {
			has_war = yes
		}
		days_remove = 90
		days_re_enable = 30
		fire_only_once = no
		modifier = {
			stability_weekly = 0.001
			war_support_weekly = -0.001
		}
		ai_will_do = {
			factor = 5
			modifier = {
				add = 50
				has_stability < 0.30
			}
		}
	}
}
