## MATERIAL DESIGNERS ##
GER_rheinmetall_materiel_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Rheinmetall
	name = GER_rheinmetall_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

GER_heckler_koch_materiel_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_HK
	name = GER_heckler_koch_materiel_manufacturer
	include = GER_generic_small_arms_manufacturer
}

GER_SIG_Sauer_materiel_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_SIG_Sauer
	name = GER_SIG_Sauer_materiel_manufacturer_name
	include = GER_generic_small_arms_manufacturer
}

## TANK DESIGNER SECTION ##
GER_rheinmetall_tank_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Rheinmetall
	name = GER_rheinmetall_tank_manufacturer
	include = generic_tank_equipment_organization

	equipment_type = {
		mbt_hull
		apc_hull
		ifv_hull
		light_tank_hull
		util_vehicle_equipment
		spart_hull
		spaa_hull
	}

	initial_trait = {
		name = generic_mio_initial_trait_md_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = 0.07
			armor_value = 0.05
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.06
			production_cost_factor = 0.2
		}
	}

	trait = {
		token = GER_mio_trait_enhanced_fire_package
		name = GER_mio_trait_enhanced_fire_package
		icon = GFX_generic_mio_trait_icon_ap_attack

		position = { x = 1 y = 0 }

		all_parents = { generic_mio_tank_trait_heavy_tank_stats_2 }
		relative_position_id = generic_mio_tank_trait_heavy_tank_stats_2

		equipment_bonus = {
			hard_attack = 0.1
			ap_attack = 0.1
			maximum_speed = 0.05

		}

		limit_to_equipment_type = { mbt_hull }

		on_complete = {
			FROM = {
				medium_expenditure = yes
			}
		}

		ai_will_do = {
			factor = 50
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}
}

GER_krauss_maffei_tank_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_KMW
	name = GER_krauss_maffei_tank_manufacturer
	include = generic_tank_equipment_organization

	equipment_type = {
		mbt_hull
		apc_hull
		ifv_hull
		light_tank_hull
		util_vehicle_equipment
		spart_hull
		spaa_hull
	}

	initial_trait = {
		name = generic_mio_initial_trait_md_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			armor_value = 0.05
			maximum_speed = 0.1
			defense = 0.05
			reliability = 0.05
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
			production_cost_factor = 0.2
		}
	}
}

GER_BMW = {
	allowed = { original_tag = GER }
	icon = GFX_idea_BMW
	name = GER_BMW_name
	include = GER_generic_utility_vehicle_manufacturer

	initial_trait = {
		name = GER_generic_utility_vehicle_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			maximum_speed = 0.04
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.06
		}
	}
}

GER_Mercedes = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Mercedes_Benz
	name = GER_Mercedes_Benz_name
	include = GER_generic_utility_vehicle_manufacturer

	initial_trait = {
		name = GER_generic_utility_vehicle_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = 0.07
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}
}

GER_artec = {
	allowed = { original_tag = GER }
	icon = GFX_idea_ARTEC
	name = GER_artec_name
	include = generic_tank_equipment_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			maximum_speed = 0.06
			breakthrough = 0.04
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}
}

GER_MAN = {
	allowed = { original_tag = GER }
	icon = GFX_idea_MAN
	name = GER_MAN_name
	visible = {
		GER = {
			has_completed_focus = GER_MAN_again
		}
	}
	available = {
		GER = {
			has_completed_focus = GER_MAN_again
		}
	}

	include = generic_tank_equipment_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.01
		}

		equipment_bonus = {
			maximum_speed = 0.06
			breakthrough = 0.04
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}
}
GER_Flensburger_Fahrzeugbau = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Flensburger_Fahrzeugbau
	name = GER_Flensburger_Fahrzeugbau_name
	visible = {
		GER = {
			has_completed_focus = GER_Flensburger_Fahrzeugbau_I
		}
	}
	available = {
		GER = {
			has_completed_focus = GER_Flensburger_Fahrzeugbau_I
		}
	}

	include = generic_tank_equipment_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			maximum_speed = 0.02
			breakthrough = 0.01
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
			production_cost_factor = -0.2
		}
	}
}
GER_Armored_Car_Systems = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Armored_Car_Systems
	name = GER_Armored_Car_Systems_name
	visible = {
		GER = {
			has_completed_focus = GER_Armored_Car_Systems_I
		}
	}
	available = {
		GER = {
			has_completed_focus = GER_Armored_Car_Systems_I
		}
	}

	include = GER_generic_utility_vehicle_manufacturer

	initial_trait = {
		name = GER_generic_utility_vehicle_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.02
		}

		equipment_bonus = {
			defense = 0.03
			breakthrough = 0.1
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
			production_cost_factor = 0.2
		}
	}

	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			check_variable = { FROM.interest_rate > 8 }
		}
		modifier = {
			factor = 0.1
			FROM = {
				num_of_military_factories < 10
			}
		}
	}
}

GER_dynamit_nobel = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Dynamit_Nobel
	name = GER_dynamit_nobel_name
	include = generic_aa_at_equipment_organization
}

GER_lfk_lenkflugkorper = {
	allowed = { original_tag = GER }
	icon = GFX_idea_GER_lfk
	name = GER_lfk_lenkflugkorper
	include = generic_aa_at_equipment_organization

	equipment_type = {
		L_AT_Equipment
		H_AT_Equipment
		AA_Equipment
		#sam_missile
		#missile
	}
}

## AIRCRAFT DESIGNERS ##
#GER_airbus_helicopters_tank_manufacturer = {
#	allowed = { original_tag = GER }
#	icon = GFX_idea_Airbus_helicopters
#	name = GER_airbus_helicopters_tank_manufacturer
#	include = generic_specialized_helicopter_organization
#}

GER_Eurocopter_helicopters_tank_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_eurocopter
	name = GER_Eurocopter_helicopters_tank_manufacturer_name
	include = generic_specialized_helicopter_organization

	initial_trait = {
		name = generic_specialized_helicopter_organization

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = -0.05
			defense = 0.05
			breakthrough = 0.05
		}

		production_bonus = {
			production_cost_factor = 0.05
		}
	}
}

GER_MBB_helicopters_tank_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_MBB
	name = GER_MBB_helicopters_tank_manufacturer_name
	include = generic_specialized_helicopter_organization

	equipment_type = {
		attack_helicopter_hull
		transport_helicopter_equipment
		spaa_hull
		L_AT_Equipment
		AA_Equipment
	}

	initial_trait = {
		name = generic_specialized_helicopter_organization

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = 0.07
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}
}

GER_airbus_defence_aircraft_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Airbus_Defence
	name = GER_airbus_defence_aircraft_manufacturer
	include = generic_air_equipment_organization
}

GER_panavia_aircraft_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Panavia
	name = GER_panavia_aircraft_manufacturer
	include = generic_fixed_wing_equipment_organization

	equipment_type = {
		mio_cat_eq_only_light_aircraft
		mio_cat_eq_only_medium_aircraft
	}

	initial_trait = {
		name = generic_fixed_wing_equipment_organization

		organization_modifier = {
			military_industrial_organization_design_team_assign_cost = 0.2
		}

		equipment_bonus = {
			reliability = 0.05
			air_range = 0.05
			air_ground_attack = 0.1
		}
	}
}

GER_Eurofighter_aircraft_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Eurofighter
	name = GER_Eurofighter_aircraft_manufacturer
	include = generic_fixed_wing_equipment_organization

	visible = {
		GER = {
			has_country_flag = GER_the_Eurofighter
		}
	}

	available = {
		GER = {
			has_country_flag = GER_the_Eurofighter
		}
	}

	initial_trait = {
		name = generic_mio_initial_trait_fixed_wing_manufacturer

		organization_modifier = {
			military_industrial_organization_task_capacity = 2
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			air_agility = 0.03
			air_defence = 0.05
			air_attack = 0.1
		}
	}
}

GER_Dornier_aircraft_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_dornier
	name = GER_Dornier_aircraft_manufacturer_name
	include = generic_air_equipment_organization

	equipment_type = {
		mio_cat_eq_only_light_aircraft
		mio_cat_eq_only_light_cv_aircraft
		mio_cat_eq_only_medium_aircraft
		mio_cat_eq_only_cv_medium_aircraft
		mio_cat_eq_only_large_aircraft
		mio_cat_eq_only_uav
	}
}

## NAVAL DESIGNERS ##
GER_thyssenkrupp_marine_naval_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Thyssen_Krupp
	name = GER_thyssenkrupp_marine_naval_manufacturer
	include = generic_naval_light_equipment_organization
}

GER_lurssen_werft_naval_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Lurssen
	name = GER_lurssen_werft_naval_manufacturer
	include = generic_naval_light_equipment_organization
}

#GER_thyssenkrupp_marine_naval_manufacturer2 = {
#	allowed = { original_tag = GER }
#	icon = GFX_idea_Thyssen_Krupp
#	name = GER_thyssenkrupp_marine_naval_manufacturer2
#	include = generic_sub_equipment_organization
#}

GER_HDW_naval_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_HDW
	name = GER_HDW_naval_manufacturer_name
	include = generic_sub_equipment_organization
}

GER_blohm_and_voss = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Blohm_and_Voss
	name = GER_blohm_and_voss_name
	include = generic_naval_equipment_organization
}

GER_siemens_defence_electronics = {
	allowed = { original_tag = GER }
	icon = GFX_idea_GER_siemens
	name = GER_siemens_defence_electronics_name
	
	equipment_type = {
		cnc_equipment
	}
	research_categories = { CAT_inf }

	research_bonus = 0.05


	initial_trait = {
		name = generic_mio_initial_trait_infantry_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}

	trait = {
		token = generic_mio_inf_trait_inf_rel
		name = generic_mio_inf_trait_inf_rel
		icon = GFX_generic_mio_trait_icon_reliability

		position = { x = 4 y = 0 }

		equipment_bonus = {
			reliability = 0.05
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_trait_support_art
		name = generic_mio_inf_trait_support_art
		icon = GFX_generic_mio_trait_icon_defense

		any_parent = { generic_mio_inf_trait_inf_rel  }

		position = { x = 0 y = 1 }
		relative_position_id = generic_mio_inf_trait_inf_rel

		limit_to_equipment_type = { artillery_equipment }
		equipment_bonus = {
			defense = 0.1
			breakthrough = 0.15
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_trait_art_stats
		name = generic_mio_inf_trait_art_stats
		icon = GFX_generic_mio_trait_icon_soft_attack

		any_parent = { generic_mio_inf_trait_inf_rel }

		position = { x = -2 y = 1 }
		relative_position_id = generic_mio_inf_trait_inf_rel

		limit_to_equipment_type = { artillery_equipment }
		equipment_bonus = {
			soft_attack = 0.05
			hard_attack = 0.15
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_at_aa_trait_at_stats
		name = generic_mio_at_aa_trait_at_stats
		icon = GFX_generic_mio_trait_icon_hard_attack

		any_parent = { generic_mio_inf_trait_art_stats }
		mutually_exclusive = { generic_mio_at_aa_trait_aa_stats }

		position = { x = -1 y = 1 }
		relative_position_id = generic_mio_inf_trait_art_stats

		limit_to_equipment_type = { L_AT_Equipment H_AT_Equipment }
		equipment_bonus = {
			hard_attack = 0.12
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_at_aa_trait_aa_stats
		name = generic_mio_at_aa_trait_aa_stats
		icon = GFX_generic_mio_trait_icon_soft_attack

		any_parent = { generic_mio_inf_trait_art_stats }
		mutually_exclusive = { generic_mio_at_aa_trait_at_stats }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_inf_trait_art_stats

		limit_to_equipment_type = { AA_Equipment spaa_hull }
		equipment_bonus = {
			air_attack = 0.12
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_trait_cnc_prod_cost
		name = generic_mio_inf_trait_cnc_prod_cost
		icon = GFX_generic_mio_trait_icon_production_capacity

		any_parent = { generic_mio_inf_trait_inf_rel }

		position = { x = 2 y = 1 }
		relative_position_id = generic_mio_inf_trait_inf_rel

		limit_to_equipment_type = { cnc_equipment }
		production_bonus = {
			production_cost_factor = -0.1
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_trait_at_stats
		name = generic_mio_inf_trait_at_stats
		icon = GFX_generic_mio_trait_icon_hard_attack

		any_parent = { generic_mio_inf_trait_cnc_prod_cost }
		mutually_exclusive = { generic_mio_inf_trait_aa_stats }

		position = { x = -1 y = 1 }
		relative_position_id = generic_mio_inf_trait_cnc_prod_cost

		limit_to_equipment_type = { L_AT_Equipment }
		equipment_bonus = {
			hard_attack = 0.2
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_trait_aa_stats
		name = generic_mio_inf_trait_aa_stats
		icon = GFX_generic_mio_trait_icon_soft_attack

		any_parent = { generic_mio_inf_trait_cnc_prod_cost }
		mutually_exclusive = { generic_mio_inf_trait_at_stats }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_inf_trait_cnc_prod_cost

		limit_to_equipment_type = { AA_Equipment }
		equipment_bonus = {
			air_attack = 0.2
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_prod_eff
		name = generic_mio_inf_prod_eff
		icon = GFX_generic_mio_trait_icon_efficiency_cap

		any_parent = { generic_mio_inf_trait_aa_stats generic_mio_inf_trait_at_stats }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_inf_trait_at_stats

		production_bonus = {
			production_efficiency_cap_factor = 0.05
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_at_ap
		name = generic_mio_inf_at_ap
		icon = GFX_generic_mio_trait_icon_ap_attack

		any_parent = { generic_mio_inf_prod_eff generic_mio_inf_at_support }

		position = { x = 0 y = 3 }
		relative_position_id = generic_mio_inf_trait_support_art

		limit_to_equipment_type = { L_AT_Equipment }
		equipment_bonus = {
			ap_attack = 0.2
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_at_support
		name = generic_mio_inf_at_support
		icon = GFX_generic_mio_trait_icon_breakthrough

		any_parent = { generic_mio_at_aa_trait_at_stats generic_mio_at_aa_trait_aa_stats }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_at_aa_trait_at_stats

		limit_to_equipment_type = { L_AT_Equipment }
		equipment_bonus = {
			defense = 0.5
			breakthrough = 0.5
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_cnc_org
		name = generic_mio_inf_cnc_org
		icon = GFX_generic_mio_trait_icon_unique

		any_parent = { generic_mio_inf_at_ap generic_mio_inf_at_ap }
		mutually_exclusive = { generic_mio_inf_cnc_rel }

		position = { x = -1 y = 1 }
		relative_position_id = generic_mio_inf_at_ap

		limit_to_equipment_type = { cnc_equipment }
		equipment_bonus = {
			max_organisation = 0.15
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_cnc_rel
		name = generic_mio_inf_cnc_rel
		icon = GFX_generic_mio_trait_icon_resources

		any_parent = { generic_mio_inf_at_ap }
		mutually_exclusive = { generic_mio_inf_cnc_org }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_inf_at_ap

		limit_to_equipment_type = { cnc_equipment }
		equipment_bonus = {
			reliability = 0.1
		}

		production_bonus = {
			production_resource_need_factor = -0.2
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_prod_cost_dec
		name = generic_mio_inf_prod_cost_dec
		icon = GFX_generic_mio_trait_icon_build_cost_ic

		any_parent = { generic_mio_inf_cnc_rel generic_mio_inf_cnc_org }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_inf_cnc_org

		limit_to_equipment_type = { L_AT_Equipment AA_Equipment }
		production_bonus = {
			production_cost_factor = -0.2
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_reass_buff
		name = generic_mio_inf_reass_buff
		icon = GFX_generic_mio_trait_icon_resources

		position = { x = 10 y = 0 }

		organization_modifier = {
			military_industrial_organization_design_team_assign_cost = -0.15
			military_industrial_organization_design_team_change_cost = -0.1
			military_industrial_organization_industrial_manufacturer_assign_cost = -0.1
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_research_buff
		name = generic_mio_inf_research_buff
		icon = GFX_generic_mio_trait_icon_production_capacity

		any_parent = { generic_mio_inf_reass_buff }
		mutually_exclusive = { generic_mio_inf_production }

		position = { x = -1 y = 1 }
		relative_position_id = generic_mio_inf_reass_buff

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.1
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_production
		name = generic_mio_inf_production
		icon = GFX_generic_mio_trait_icon_efficiency_gain

		any_parent = { generic_mio_inf_reass_buff }
		mutually_exclusive = { generic_mio_inf_research_buff }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_inf_reass_buff

		production_bonus = {
			production_efficiency_cap_factor = 0.05
			production_efficiency_gain_factor = 0.2
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_xp_gain
		name = generic_mio_inf_xp_gain
		icon = GFX_generic_mio_trait_icon_efficiency_gain

		any_parent = { generic_mio_inf_production generic_mio_inf_research_buff }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_inf_research_buff

		organization_modifier = {
			military_industrial_organization_funds_gain = 0.2
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_capacity
		name = generic_mio_inf_capacity
		icon = GFX_generic_mio_trait_icon_production_capacity

		any_parent = { generic_mio_inf_xp_gain }

		position = { x = 0 y = 1 }
		relative_position_id = generic_mio_inf_xp_gain

		organization_modifier = {
			military_industrial_organization_task_capacity = 3
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_research_prod
		name = generic_mio_inf_research_prod
		icon = GFX_generic_mio_trait_icon_efficiency_cap

		any_parent = { generic_mio_inf_capacity }
		mutually_exclusive = { generic_mio_inf_capacity_2 }

		position = { x = -1 y = 1 }
		relative_position_id = generic_mio_inf_capacity

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.05
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_capacity_2
		name = generic_mio_inf_capacity_2
		icon = GFX_generic_mio_trait_icon_production_capacity

		any_parent = { generic_mio_inf_capacity }
		mutually_exclusive = { generic_mio_inf_research_prod }

		position = { x = 1 y = 1 }
		relative_position_id = generic_mio_inf_capacity

		organization_modifier = {
			military_industrial_organization_task_capacity = 3
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}

	trait = {
		token = generic_mio_inf_bunch_of_buffs
		name = generic_mio_inf_bunch_of_buffs
		icon = GFX_generic_mio_trait_icon_unique

		any_parent = { generic_mio_inf_research_prod generic_mio_inf_capacity_2 }

		position = { x = -1 y = 1 }
		relative_position_id = generic_mio_inf_capacity_2

		organization_modifier = {
			military_industrial_organization_design_team_assign_cost = -0.15
			military_industrial_organization_design_team_change_cost = -0.1
			military_industrial_organization_industrial_manufacturer_assign_cost = -0.1
			military_industrial_organization_research_bonus = 0.05
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.05
		}

		on_complete = {
			if = {
				limit = {
					check_variable = { free_trait_picks > 0 }
				}
				add_to_variable = { free_trait_picks = -1 }
			}
			else = {
				FROM = {
					small_expenditure = yes
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
		}
	}
}