GEO_tbilisi_aircraft_manufacturing_tank_manufacturer = {
	allowed = { original_tag = GEO }
	icon = GFX_idea_Tbilisi_Aircraft_Manufacturing
	name = GEO_tbilisi_aircraft_manufacturing_tank_manufacturer
	include = generic_tank_equipment_organization
}

GEO_tbilisi_aircraft_manufacturing_materiel_manufacturer = {
	allowed = { original_tag = GEO }
	icon = GFX_idea_Tbilisi_Aircraft_Manufacturing
	name = GEO_tbilisi_aircraft_manufacturing_materiel_manufacturer
	include = generic_infantry_equipment_organization
}
