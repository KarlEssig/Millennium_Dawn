EGY_kader_factory_tank_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI_Kader
	name = EGY_kader_factory_tank_manufacturer
	include = generic_specialized_tank_organization

	equipment_type = {
		util_vehicle_equipment
		ifv_hull
		light_tank_hull
		spart_hull
	}
}

EGY_sakr_factory_material_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_sakr_factory_material_manufacturer
	include = generic_cnc_equipment_organization

	equipment_type = {
		cnc_equipment
		L_AT_Equipment
		H_AT_Equipment
		AA_Equipment
		artillery_equipment
	}
}

EGY_AAV_vehicle_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_Arab_American_Vehicle
	name = EGY_AAV_vehicle_manufacturer
	include = GER_generic_utility_vehicle_manufacturer
}

EGY_abu_zaabal_specialized_industries_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_EGY_abu_zaabal
	name = EGY_abu_zaabal_specialized_industries_manufacturer
	include = GER_generic_small_arms_manufacturer
}

EGY_helwan_machine_tools_tank_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_helwan_machine_tools_tank_manufacturer
	include = generic_tank_equipment_organization
}

EGY_abu_zaabal_tank_factory_tank_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_Abu_Zaabal_Factory
	name = EGY_abu_zaabal_tank_factory_tank_manufacturer
	include = generic_tank_equipment_organization
}

EGY_abhco_tank_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_abhco_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

EGY_kader_factory_materiel_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI_Kader
	name = EGY_kader_factory_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

EGY_maadi_arms_materiel_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_maadi_arms_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

EGY_aoi_aircraft_aircraft_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_AOI
	name = EGY_aoi_aircraft_aircraft_manufacturer
	include = generic_air_equipment_organization
}

EGY_alexandria_shipyard_naval_manufacturer = {
	allowed = { original_tag = EGY }
	icon = GFX_idea_Alexandria_Shipyard
	name = EGY_alexandria_shipyard_naval_manufacturer
	include = generic_naval_equipment_organization
}
