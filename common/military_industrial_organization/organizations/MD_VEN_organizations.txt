#manufacture 
VEN_cavim_manufacturer = {
	allowed = { original_tag = VEN }
	icon = GFX_idea_VEN_cavim
	name = VEN_cavim_manufacturer
	include = GER_generic_small_arms_manufacturer

	equipment_type = {
		Inf_equipment
		util_vehicle_equipment
	}

	initial_trait = {
		name = GER_generic_small_arms_manufacturer

		organization_modifier = {
			military_industrial_organization_task_capacity = -2
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
			production_efficiency_gain_factor = 0.05
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_rel
		name = GER_mio_spec_Utility_trait_light_rel
		icon = GFX_generic_mio_trait_icon_reliability

		position = { x = 16 y = 0 }

		equipment_bonus = {
			reliability = 0.05
			maximum_speed = 0.05
		}

		on_complete = {
			FROM = {
				small_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_soft1
		name = GER_mio_spec_Utility_trait_light_soft1
		icon = GFX_generic_mio_trait_icon_soft_attack

		position = { x = -2 y = 1 }

		mutually_exclusive = {
			GER_mio_spec_Utility_trait_light_brkthr1
			GER_mio_spec_Utility_trait_light_hard1
		}

		all_parents = { GER_mio_spec_Utility_trait_light_rel }
		relative_position_id = GER_mio_spec_Utility_trait_light_rel

		equipment_bonus = {
			reliability = 0.07
		}

		on_complete = {
			FROM = {
				small_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_hard1
		name = GER_mio_spec_Utility_trait_light_hard1
		icon = GFX_generic_mio_trait_icon_ap_attack

		position = { x = 2 y = 1 }

		all_parents = { GER_mio_spec_Utility_trait_light_rel }
		relative_position_id = GER_mio_spec_Utility_trait_light_rel

		mutually_exclusive = {
			GER_mio_spec_Utility_trait_light_soft1
			GER_mio_spec_Utility_trait_light_brkthr1
		}

		equipment_bonus = {
			hard_attack = 0.06
		}

		on_complete = {
			FROM = {
				small_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_brkthr1
		name = GER_mio_spec_Utility_trait_light_brkthr1
		icon = GFX_generic_mio_trait_icon_breakthrough

		position = { x = 0 y = 1 }

		all_parents = { GER_mio_spec_Utility_trait_light_rel }
		relative_position_id = GER_mio_spec_Utility_trait_light_rel

		mutually_exclusive = {
			GER_mio_spec_Utility_trait_light_soft1
			GER_mio_spec_Utility_trait_light_hard1 }

		equipment_bonus = {
			breakthrough = 0.08
		}

		on_complete = {
			FROM = {
				small_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_armor1
		name = GER_mio_spec_Utility_trait_light_armor1
		icon = GFX_generic_mio_trait_icon_armor_value

		position = { x = -1 y = 1 }

		any_parent = {
			GER_mio_spec_Utility_trait_light_soft1 GER_mio_spec_Utility_trait_light_hard1 GER_mio_spec_Utility_trait_light_brkthr1 }
		relative_position_id = GER_mio_spec_Utility_trait_light_brkthr1

		equipment_bonus = {
			hard_attack = 0.02
			reliability = 0.02
		}

		on_complete = {
			FROM = {
				small_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_armor2
		name = GER_mio_spec_Utility_trait_light_armor2
		icon = GFX_generic_mio_trait_icon_defense

		position = { x = 1 y = 1 }

		any_parent = { GER_mio_spec_Utility_trait_light_soft1 GER_mio_spec_Utility_trait_light_hard1 GER_mio_spec_Utility_trait_light_brkthr1 }
		relative_position_id = GER_mio_spec_Utility_trait_light_brkthr1

		equipment_bonus = {
			reliability = 0.04
		}

		on_complete = {
			FROM = {
				small_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_armor3
		name = GER_mio_spec_Utility_trait_light_armor3
		icon = GFX_generic_mio_trait_icon_armor_value

		position = { x = 0 y = 1 }

		all_parents = { GER_mio_spec_Utility_trait_light_armor1 GER_mio_spec_Utility_trait_light_armor2 }
		relative_position_id = GER_mio_spec_Utility_trait_light_armor1

		equipment_bonus = {
			hard_attack = 0.03
			reliability = 0.05
			hardness = 0.05
		}

		on_complete = {
			FROM = {
				medium_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_exp_weapon1
		name = GER_mio_spec_Utility_trait_light_exp_weapon1
		icon = GFX_generic_mio_trait_icon_ap_attack

		position = { x = 21 y = 0 }

		equipment_bonus = {
			reliability = 0.03
			defense = 0.05
		}

		on_complete = {
			FROM = {
				small_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_exp_ammo1
		name = GER_mio_spec_Utility_trait_light_exp_ammo1
		icon = GFX_generic_mio_trait_icon_soft_attack

		position = { x = -1 y = 1 }

		mutually_exclusive = { GER_mio_spec_Utility_trait_light_exp_ammo2 }
		any_parent = { GER_mio_spec_Utility_trait_light_exp_weapon1 }
		relative_position_id = GER_mio_spec_Utility_trait_light_exp_weapon1

		equipment_bonus = {
			breakthrough = 0.07
			hard_attack = 0.03
		}

		on_complete = {
			FROM = {
				medium_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_exp_ammo2
		name = GER_mio_spec_Utility_trait_light_exp_ammo2
		icon = GFX_generic_mio_trait_icon_ap_attack

		position = { x = 1 y = 1 }

		mutually_exclusive = { GER_mio_spec_Utility_trait_light_exp_ammo1 }
		any_parent = { GER_mio_spec_Utility_trait_light_exp_weapon1 }
		relative_position_id = GER_mio_spec_Utility_trait_light_exp_weapon1

		equipment_bonus = {
			ap_attack = 0.10
			hard_attack = 0.05
		}

		on_complete = {
			FROM = {
				medium_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_exp_def1
		name = GER_mio_spec_Utility_trait_light_exp_def1
		icon = GFX_generic_mio_trait_icon_defense

		position = { x = 0 y = 2 }

		any_parent = { GER_mio_spec_Utility_trait_light_exp_ammo1 GER_mio_spec_Utility_trait_light_exp_ammo2 }
		relative_position_id = GER_mio_spec_Utility_trait_light_exp_weapon1

		equipment_bonus = {
			defense = 0.1
		}

		on_complete = {
			FROM = {
				large_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_exp_cheap1
		name = GER_mio_spec_Utility_trait_light_exp_cheap1
		icon = GFX_generic_mio_trait_icon_build_cost_ic

		position = { x = -1 y = 1 }

		any_parent = { GER_mio_spec_Utility_trait_light_exp_def1 }
		relative_position_id = GER_mio_spec_Utility_trait_light_exp_def1

		equipment_bonus = {
			build_cost_ic = -0.08
		}

		production_bonus = {
			production_resource_need_factor = -0.08
		}

		on_complete = {
			FROM = {
				large_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}

	trait = {
		token = GER_mio_spec_Utility_trait_light_exp_cheap2
		name = GER_mio_spec_Utility_trait_light_exp_cheap2
		icon = GFX_generic_mio_department_icon_facilities

		position = { x = 1 y = 1 }

		any_parent = { GER_mio_spec_Utility_trait_light_exp_def1 }
		relative_position_id = GER_mio_spec_Utility_trait_light_exp_def1

		production_bonus = {
			production_efficiency_gain_factor = 0.10
			production_efficiency_cap_factor = 0.08
		}

		on_complete = {
			FROM = {
				large_expenditure = yes
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0
				check_variable = { FROM.interest_rate > 8 }
			}
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}
}

#navy

VEN_dianca_shipyard = {
	allowed = { original_tag = VEN }
	icon = GFX_idea_VEN_dianca
	name = VEN_dianca_shipyard
	include = generic_mixed_naval_equipment_organization

	equipment_type = {
		missile_submarine
		attack_submarine
		patrol_boat
	}

	initial_trait = {
		name = generic_mio_initial_trait_md_naval_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
			military_industrial_organization_task_capacity = -2
		}

		production_bonus = {
			production_capacity_factor = 0.05
		}
	}
}

VEN_ucocar_shipyard = {
	allowed = { original_tag = VEN }
	icon = GFX_idea_VEN_ucocar
	name = VEN_ucocar_shipyard
	include = RAJ_Garden_reach_naval_manufacturer

	initial_trait = {
		name = generic_mio_initial_trait_md_light_naval_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
			military_industrial_organization_task_capacity = -1
		}

		production_bonus = {
			production_capacity_factor = 0.03
		}
	}
}






