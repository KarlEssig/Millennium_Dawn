#manufactures

ARG_fabricaciones_militares_manufacturer = {
	allowed = { original_tag = ARG }
	icon = GFX_idea_ARG_fabricaciones_militares
	name = ARG_fabricaciones_militares_manufacturer
	include = generic_infantry_equipment_organization

	equipment_type = {
		artillery_equipment
		Inf_equipment
		L_AT_Equipment
		AA_Equipment
	}

	initial_trait = {
		name = generic_mio_initial_trait_infantry_manufacturer

		organization_modifier = {
			military_industrial_organization_task_capacity = -1
		}

		production_bonus = {
			production_capacity_factor = 0.05
		}
	}
}

ARG_bersa_manufacturer = {
	allowed = { original_tag = ARG }
	icon = GFX_idea_ARG_bersa
	name = ARG_bersa_manufacturer
	include = GER_generic_small_arms_manufacturer

	initial_trait = {
		name = GER_generic_small_arms_manufacturer

		organization_modifier = {
			military_industrial_organization_task_capacity = -2
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
			production_efficiency_gain_factor = 0.05
		}
	}
}

ARG_citefa_manufacturer = {
	allowed = { original_tag = ARG }
	icon = GFX_idea_ARG_citefa
	name = ARG_citefa_manufacturer
	include = generic_aa_at_equipment_organization

	equipment_type = {
		artillery_equipment
		L_AT_Equipment
		H_AT_Equipment
		AA_Equipment
		spaa_hull
		cnc_equipment
	}

	initial_trait = {
		name = generic_mio_initial_trait_at_aa_manufacturer

		organization_modifier = {
			military_industrial_organization_task_capacity = -1
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
			production_efficiency_gain_factor = 0.05
		}
	}
}

#tank

ARG_tamse_tank = {
	allowed = { original_tag = ARG }
	icon = GFX_idea_ARG_tam
	name = ARG_tamse_tank
	include = generic_specialized_tank_organization

	equipment_type = {
		mbt_hull
		apc_hull
		ifv_hull
		light_tank_hull
		spart_hull
	}

	initial_trait = {
		name = generic_mio_initial_trait_md_specialized_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_task_capacity = -1
		}

		production_bonus = {
			production_capacity_factor = 0.05
		}
	}
}

ARG_cicare_helicopter_tank = {
	allowed = { original_tag = ARG }
	icon = GFX_idea_ARG_cicare
	name = ARG_cicare_helicopter_tank
	include = generic_specialized_helicopter_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_specialized_helicopter_manufacturer

		organization_modifier = {
			military_industrial_organization_task_capacity = -2
		}

		production_bonus = {
			production_capacity_factor = 0.05
		}
	}
}

#navy 

ARG_rio_santiago_shipyard = {
	allowed = { original_tag = ARG }
	icon = GFX_idea_ARG_rio_santiago_dock
	name = ARG_rio_santiago_shipyard
	include = RAJ_Garden_reach_naval_manufacturer

	initial_trait = {
		name = generic_mio_initial_trait_md_light_naval_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
			military_industrial_organization_task_capacity = -1
		}

		production_bonus = {
			production_capacity_factor = 0.03
		}
	}
}

ARG_tandanor_shipyard = {
	allowed = { original_tag = ARG }
	icon = GFX_idea_ARG_tandanor
	name = ARG_tandanor_shipyard
	include = generic_mixed_naval_equipment_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_naval_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
			military_industrial_organization_task_capacity = -2
		}

		production_bonus = {
			production_capacity_factor = 0.05
		}
	}
}

#airforce

ARG_fma_air = {
	allowed = { original_tag = ARG }
	icon = GFX_idea_ARG_fadea
	name = ARG_fma_air
	include = generic_air_equipment_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_air_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
			military_industrial_organization_task_capacity = -1
		}

		production_bonus = {
			production_capacity_factor = 0.05
		}
	}
}
