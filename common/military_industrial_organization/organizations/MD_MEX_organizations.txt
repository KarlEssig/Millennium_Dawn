MEX_dgime_materiel_manufacturer = {
	allowed = { original_tag = MEX }
	icon = GFX_idea_SEDENA
	name = MEX_dgime_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

MEX_dina_tank_manufacturer = {
	allowed = { original_tag = MEX }
	icon = GFX_idea_MEX_dina
	name = MEX_dina_tank_manufacturer
	include = generic_specialized_tank_organization

	equipment_type = {
		util_vehicle_equipment
		ifv_hull
		light_tank_hull
		spart_hull
	}
}

MEX_stns_shipyard_naval_manufacturer = {
	allowed = { original_tag = MEX }
	icon = GFX_idea_MEX_Marina
	name = MEX_stns_shipyard_naval_manufacturer
	include = generic_naval_equipment_organization
}
