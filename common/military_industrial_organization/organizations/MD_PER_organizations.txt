PER_defense_industries_organization_materiel_manufacturer = {
	allowed = { original_tag = PER }
	icon = GFX_idea_Defense_Industries_Organization_PER
	name = PER_defense_industries_organization_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

PER_defense_industries_organization_tank_manufacturer = {
	allowed = { original_tag = PER }
	icon = GFX_idea_Defense_Industries_Organization_PER
	name = PER_defense_industries_organization_tank_manufacturer
	include = generic_tank_equipment_organization
}

PER_panha_tank_manufacturer = {
	allowed = { original_tag = PER }
	icon = GFX_idea_PANHA_PER
	name = PER_panha_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

PER_hesa_aircraft_manufacturer = {
	allowed = { original_tag = PER }
	icon = GFX_idea_SHAHA_PER
	name = PER_hesa_aircraft_manufacturer
	include = generic_fixed_wing_equipment_organization
}

PER_hesa_tank_manufacturer = {
	allowed = { original_tag = PER }
	icon = GFX_idea_SHAHA_PER
	name = PER_hesa_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

PER_marine_industries_organization_naval_manufacturer = {
	allowed = { original_tag = PER }
	icon = GFX_idea_Marine_Industries_Organization_PER
	name = PER_marine_industries_organization_naval_manufacturer
	include = generic_naval_equipment_organization
}

PER_marine_industries_organization_naval_manufacturer2 = {
	allowed = { original_tag = PER }
	icon = GFX_idea_Marine_Industries_Organization_PER
	name = PER_marine_industries_organization_naval_manufacturer2
	include = generic_sub_equipment_organization
}