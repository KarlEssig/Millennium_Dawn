SAF_denel_land_systems_materiel_manufacturer = {
	allowed = { original_tag = SAF }
	icon = GFX_idea_Denel_Dynamics
	name = SAF_denel_land_systems_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

SAF_denel_dynamics_materiel_manufacturer = {
	allowed = { original_tag = SAF }
	icon = GFX_idea_Denel_Dynamics
	name = SAF_denel_dynamics_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

SAF_denel_land_systems_tank_manufacturer = {
	allowed = { original_tag = SAF }
	icon = GFX_idea_Denel_Land_Systems
	name = SAF_denel_land_systems_tank_manufacturer
	# TODO: Replace with artillery MIO
	include = generic_tank_equipment_organization
}

SAF_land_systems_omc_tank_manufacturer = {
	allowed = { original_tag = SAF }
	icon = GFX_idea_Denel_Land_Systems
	name = SAF_land_systems_omc_tank_manufacturer
	include = generic_tank_equipment_organization
}