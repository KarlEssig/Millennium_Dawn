USA_alliant_techsystems_materiel_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Alliant_Techsystems
	name = USA_alliant_techsystems_materiel_manufacturer
	include = generic_aa_at_equipment_organization
}

USA_colt_defense_materiel_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Colt-Defense
	name = USA_colt_defense_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

USA_raytheon_materiel_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Raytheon
	name = USA_raytheon_materiel_manufacturer
	include = generic_aa_at_equipment_organization
}

USA_bell_helicopter_textron_tank_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Bell_Textron
	name = USA_bell_helicopter_textron_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

USA_sikorsky_aircraft_tank_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Sikorsky_Aircraft_Logo
	name = USA_sikorsky_aircraft_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

USA_boeing_aircraft_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Boeing
	name = USA_boeing_aircraft_manufacturer
	include = generic_fixed_wing_equipment_organization
}

USA_lockheed_martin_aircraft_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Lockheed_Martin
	name = USA_lockheed_martin_aircraft_manufacturer
	include = generic_air_equipment_organization
}

USA_northrop_grumman_aircraft_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Northrop_Grumman
	name = USA_northrop_grumman_aircraft_manufacturer
	# TODO: Replace with heavy plane MIO
	include = generic_air_equipment_organization
}

USA_textron_aviation_defense_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_textron
	name = USA_textron_aviation_defense_manufacturer
	include = generic_light_aircraft_equipment_organization
}

USA_general_dynamics_electric_boat_naval_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_General_Dynamics_Electric_Boat
	name = USA_general_dynamics_electric_boat_naval_manufacturer
	include = generic_sub_equipment_organization
}

USA_northrop_grumman_newport_news_naval_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_NNS
	name = USA_northrop_grumman_newport_news_naval_manufacturer
	include = generic_sub_equipment_organization
}

USA_bath_iron_works_naval_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_Bath_Iron_Works
	name = USA_bath_iron_works_naval_manufacturer
	include = generic_naval_equipment_organization
}

USA_national_steel_and_shipbuilding_company_naval_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_General_Dynamics
	name = USA_national_steel_and_shipbuilding_company_naval_manufacturer
	include = generic_naval_equipment_organization
}

USA_ingalls_shipbuilding_naval_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_ingalls-logo
	name = USA_ingalls_shipbuilding_naval_manufacturer
	include = generic_naval_light_equipment_organization
}

###
USA_northrop_grumman_newport_news_naval_manufacturer2 = {
	allowed = { original_tag = USA }
	icon = GFX_idea_NNS
	name = USA_northrop_grumman_newport_news_naval_manufacturer2
	include = generic_naval_light_equipment_organization
}
##
USA_textron_tank_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_textron
	name = USA_textron_tank_manufacturer
	include = generic_infantry_equipment_organization
}
USA_united_defense_tank_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_united_defense
	name = USA_united_defense_tank_manufacturer
	include = generic_tank_equipment_organization
}
USA_general_dynamics_APC_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_General_Dynamics
	name = USA_general_dynamics_APC_manufacturer
	include = generic_tank_equipment_organization
}
USA_general_dynamics_materiel_manufacturer = {
	allowed = { original_tag = USA }
	icon = GFX_idea_General_Dynamics
	name = USA_general_dynamics_materiel_manufacturer
	include = generic_specialized_tank_organization
}