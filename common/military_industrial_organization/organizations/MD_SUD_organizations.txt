SUD_military_industry_corporation_materiel_manufacturer = {
	allowed = { original_tag = SUD }
	icon = GFX_idea_Military_Industry_Corporation
	name = SUD_military_industry_corporation_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

SUD_military_industry_corporation_tank_manufacturer = {
	allowed = { original_tag = SUD }
	icon = GFX_idea_Military_Industry_Corporation
	name = SUD_military_industry_corporation_tank_manufacturer
	include = generic_tank_equipment_organization
}