#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

UKR_home_to_chernobyl_zone = {
	enable = { always = yes }
	icon = GFX_idea_chernobyl_zone
	custom_modifier_tooltip = UKR_chernobyl_tooltip
	custom_modifier_tooltip = UKR_chernobyl_effects_tooltip
	local_intel_to_enemies = 0.25
	local_building_slots_factor = -0.05
	army_attack_factor = -0.1
	local_resources = -0.05
	local_supplies = -0.05
}
UKR_odessa_ssaka = {
	enable = { always = yes }
	icon = GFX_idea_chernobyl_zone
	state_resources_factor = -0.25
	local_building_slots_factor = -0.15
	recruitable_population_factor = -0.05
}
UKR_odessa_ssak = {
	enable = { always = yes }
	icon = GFX_idea_chernobyl_zone
	state_resources_factor = 0.25
	local_building_slots_factor = 0.15
	recruitable_population_factor = 0.05
}
UKR_liberals_modifier = {
	enable = { always = yes }
	icon = GFX_idea_ukr_our_main
	remove_trigger = {
		NOT = {
			neutrality_neutral_libertarians_are_in_power = yes
		}
	}
	political_power_factor = UKR_liberals_political_power_factor
	drift_defence_factor = UKR_liberals_drift_defence_factor
	neutrality_drift = UKR_liberals_democratic_support
	bureaucracy_cost_multiplier_modifier = UKR_liberals_bureaucracy_cost_multiplier_modifier
	production_speed_infrastructure_factor = UKR_liberals_production_speed_infrastructure_factor
	emerging_outlook_campaign_cost_modifier = UKR_liberals_emerging_outlook_campaign_cost_modifier
	stability_factor = UKR_liberals_stability_factor
	social_cost_multiplier_modifier = UKR_liberals_social_cost_multiplier_modifier
	health_cost_multiplier_modifier = UKR_liberals_health_cost_multiplier_modifier
	communism_drift = UKR_liberals_communism_drift
	education_cost_multiplier_modifier = UKR_liberals_education_budget_cost_factor
}