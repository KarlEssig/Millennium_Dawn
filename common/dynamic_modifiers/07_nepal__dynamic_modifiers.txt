#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

NEP_guerrilla_conflict = {
	enable = { always = yes }
	icon = GFX_idea_mountain_defence_red
	custom_modifier_tooltip = nep_civil_war_info_tt
	local_org_regain = -0.15
	army_defence_factor = 0.15
	temporary_state_resource_oil = sabotaged_oil
	temporary_state_resource_aluminium = sabotaged_aluminium
	temporary_state_resource_rubber = sabotaged_rubber
	temporary_state_resource_tungsten = sabotaged_tungsten
	temporary_state_resource_steel = sabotaged_steel
	temporary_state_resource_chromium = sabotaged_chromium
}

