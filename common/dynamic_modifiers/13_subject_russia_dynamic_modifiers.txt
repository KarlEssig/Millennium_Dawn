#TATARSTAN MODIFIERS
TAT_liberals_modifier = {
	enable = { always = yes }
	icon = GFX_idea_democracy
	remove_trigger = {
		NOT = {
			has_government = democratic
		}
	}
	drift_defence_factor = TAT_liberals_drift_defence_factor
	democratic_drift = TAT_liberals_democratic_support
	bureaucracy_cost_multiplier_modifier = TAT_liberals_bureaucracy_cost_multiplier_modifier
	stability_factor = TAT_liberals_stability_factor
	social_cost_multiplier_modifier = TAT_liberals_social_cost_multiplier_modifier
	health_cost_multiplier_modifier = TAT_liberals_health_cost_multiplier_modifier
	nationalist_drift = TAT_liberals_nationalist_drift
	communism_drift = TAT_liberals_communism_drift
	monthly_population = TAT_liberals_monthly_population
	research_speed_factor = TAT_liberals_research_speed_factor
	education_cost_multiplier_modifier = TAT_liberals_education_budget_cost_factor
}
TAT_tatar_kommi_modifier = {
	icon = "GFX_idea_tat_commiesrule"
	enable = {
		original_tag = TAT
		TAT = { has_completed_focus = TAT_Communism_start }
	}
	remove_trigger = {
		NOT = {
			emerging_communist_state_are_in_power = yes
		}
	}
	local_resources_factor = TAT_tatar_kommi_local_resources_factor
	drift_defence_factor = TAT_tatar_kommi_drift_defence_factor
	research_speed_factor = TAT_tatar_kommi_research_speed_factor
	stability_factor = TAT_tatar_stability_factor
	political_power_gain = TAT_tatar_kommi_political_power_gain
	production_factory_efficiency_gain_factor = TAT_tatar_kommi_production_factory_efficiency_gain_factor
	production_speed_buildings_factor = TAT_tatar_kommi_production_speed_buildings_factor
	communism_drift = TAT_tatar_kommi_communism_drift
	education_cost_multiplier_modifier = TAT_tatar_kommi_education_cost_multiplier_modifier
	consumer_goods_factor = TAT_tatar_kommi_consumer_goods_factor
	social_cost_multiplier_modifier = TAT_tatar_kommi_social_cost_multiplier_modifier
	health_cost_multiplier_modifier = TAT_tatar_kommi_health_cost_multiplier_modifier
	recruitable_population_factor = TAT_tatar_kommi_recruitable_population_factor
	nationalist_drift = TAT_tatar_kommi_nationalist_drift
}
TAT_tatarin_nati_modifier = {
	icon = "GFX_idea_tat_nationalisms"
	enable = {
		original_tag = TAT
		TAT = { has_completed_focus = TAT_constitution_return }
	}
	remove_trigger = {
		NOT = {
			has_government = nationalist
		}
	}
	stability_factor = TAT_tatarin_nati_stability_factor
	nationalist_drift = TAT_tatarin_nati_nationalist_drift
	political_power_gain = TAT_tatarin_nati_political_power_gain
	drift_defence_factor = TAT_tatarin_nati_drift_defence_factor
	monthly_population = TAT_tatarin_nati_monthly_population
	democratic_drift = TAT_tatarin_nati_democratic_drift
	foreign_influence_defense_modifier = TAT_tatarin_nati_foreign_influence_defense_modifier
	research_speed_factor = TAT_tatarin_nati_research_speed_factor
}
SUB_tat_metro = {
	enable = { always = yes }
	icon = GFX_idea_cub_roads
	army_speed_factor_for_controller = 0.29
	state_productivity_growth_modifier = 0.15
}
#BASHKORTOSTAN MODIFIERS
BSH_tatarin_nati_modifier = {
	icon = "GFX_idea_bsh_indep"
	enable = {
		original_tag = BSH
		BSH = { has_completed_focus = BSH_constitution_return }
	}
	remove_trigger = {
		NOT = {
			has_government = nationalist
		}
	}
	stability_factor = BSH_tatarin_nati_stability_factor
	nationalist_drift = BSH_tatarin_nati_nationalist_drift
	political_power_gain = BSH_tatarin_nati_political_power_gain
	drift_defence_factor = BSH_tatarin_nati_drift_defence_factor
	monthly_population = BSH_tatarin_nati_monthly_population
	democratic_drift = BSH_tatarin_nati_democratic_drift
	foreign_influence_defense_modifier = BSH_tatarin_nati_foreign_influence_defense_modifier
	research_speed_factor = BSH_tatarin_nati_research_speed_factor
}
BSH_tatar_kommi_modifier = {
	icon = "GFX_idea_bsh_commiesrule"
	enable = {
		original_tag = BSH
		BSH = { has_completed_focus = BSH_Communism_start }
	}
	remove_trigger = {
		NOT = {
			emerging_communist_state_are_in_power = yes
		}
	}
	local_resources_factor = BSH_tatar_kommi_local_resources_factor
	drift_defence_factor = BSH_tatar_kommi_drift_defence_factor
	research_speed_factor = BSH_tatar_kommi_research_speed_factor
	stability_factor = BSH_tatar_stability_factor
	political_power_gain = BSH_tatar_kommi_political_power_gain
	production_factory_efficiency_gain_factor = BSH_tatar_kommi_production_factory_efficiency_gain_factor
	production_speed_buildings_factor = BSH_tatar_kommi_production_speed_buildings_factor
	communism_drift = BSH_tatar_kommi_communism_drift
	education_cost_multiplier_modifier = BSH_tatar_kommi_education_cost_multiplier_modifier
	consumer_goods_factor = BSH_tatar_kommi_consumer_goods_factor
	social_cost_multiplier_modifier = BSH_tatar_kommi_social_cost_multiplier_modifier
	health_cost_multiplier_modifier = BSH_tatar_kommi_health_cost_multiplier_modifier
	recruitable_population_factor = BSH_tatar_kommi_recruitable_population_factor
	nationalist_drift = BSH_tatar_kommi_nationalist_drift
}
SUB_bsh_hubei = {
	enable = { always = yes }
	icon = "GFX_idea_bsh_selhoz_idea"
	state_resources_factor = 0.05
	agriculture_tax_modifier = 0.15
	agricolture_productivity_modifier = 0.15
}
SUB_blr_bsh = {
	enable = { always = yes }
	icon = "GFX_idea_blr_agro"
	state_resources_factor = 0.05
	agriculture_tax_modifier = 0.15
	agricolture_productivity_modifier = 0.15
}
#MOSCOW STATE MODIFIERS
MSC_city_1_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.05
	state_productivity_growth_modifier = 0.05
	army_speed_factor_for_controller = 0.05
}
MSC_city_2_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.09
	state_productivity_growth_modifier = 0.09
	army_speed_factor_for_controller = 0.05
	local_manpower = 0.03
}
MSC_city_3_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.09
	state_productivity_growth_modifier = 0.09
	army_speed_factor_for_controller = 0.09
	local_manpower = 0.04
	local_building_slots = 1
}
MSC_city_4_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.11
	state_productivity_growth_modifier = 0.11
	army_speed_factor_for_controller = 0.11
	local_manpower = 0.05
	local_building_slots = 2
}
MSC_city_5_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.14
	state_productivity_growth_modifier = 0.14
	army_speed_factor_for_controller = 0.11
	local_manpower = 0.06
	local_building_slots = 2
}
MSC_city_6_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.17
	state_productivity_growth_modifier = 0.17
	army_speed_factor_for_controller = 0.13
	local_manpower = 0.08
	local_building_slots = 3
}
MSC_city_7_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.25
	state_productivity_growth_modifier = 0.25
	army_speed_factor_for_controller = 0.18
	local_manpower = 0.12
	local_building_slots = 4
}
MSC_city_8_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.30
	state_productivity_growth_modifier = 0.30
	army_speed_factor_for_controller = 0.20
	local_manpower = 0.14
	local_building_slots = 5
}
MSC_city_9_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.33
	state_productivity_growth_modifier = 0.33
	army_speed_factor_for_controller = 0.24
	local_manpower = 0.16
	local_building_slots = 6
}
MSC_city_16_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.15
	state_productivity_growth_modifier = 0.13
	army_speed_factor_for_controller = 0.13
	state_renewable_energy_generation_modifier = 0.10
	state_production_speed_infrastructure_factor = 0.10
	local_manpower = 0.07
	local_building_slots = 3
}
MSC_city_17_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.12
	state_productivity_growth_modifier = 0.11
	army_speed_factor_for_controller = 0.13
	state_renewable_energy_generation_modifier = 0.15
	state_production_speed_infrastructure_factor = 0.15
	local_manpower = 0.07
	local_building_slots = 3
}
MSC_city_18_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.11
	state_productivity_growth_modifier = 0.10
	army_speed_factor_for_controller = 0.12
	state_renewable_energy_generation_modifier = 0.20
	state_production_speed_infrastructure_factor = 0.20
	local_manpower = 0.06
	local_building_slots = 3
}
MSC_city_19_level = {
	icon = "GFX_idea_msc_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.10
	state_productivity_growth_modifier = 0.09
	army_speed_factor_for_controller = 0.11
	state_renewable_energy_generation_modifier = 0.25
	state_production_speed_infrastructure_factor = 0.25
	local_manpower = 0.06
	local_building_slots = 3
}
#EKATERINBURG STATE MODIFIERS
EKB_city_1_level = {
	icon = "GFX_idea_ekb_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.05
	state_productivity_growth_modifier = 0.05
	army_speed_factor_for_controller = 0.05
}
EKB_city_2_level = {
	icon = "GFX_idea_ekb_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.09
	state_productivity_growth_modifier = 0.09
	army_speed_factor_for_controller = 0.05
	local_manpower = 0.03
}
EKB_city_3_level = {
	icon = "GFX_idea_ekb_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.09
	state_productivity_growth_modifier = 0.09
	army_speed_factor_for_controller = 0.09
	local_manpower = 0.04
	local_building_slots = 1
}
EKB_city_4_level = {
	icon = "GFX_idea_ekb_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.11
	state_productivity_growth_modifier = 0.11
	army_speed_factor_for_controller = 0.11
	local_manpower = 0.05
	local_building_slots = 2
}
EKB_city_5_level = {
	icon = "GFX_idea_ekb_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.14
	state_productivity_growth_modifier = 0.14
	army_speed_factor_for_controller = 0.11
	local_manpower = 0.06
	local_building_slots = 2
}
EKB_city_6_level = {
	icon = "GFX_idea_ekb_idea"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.17
	state_productivity_growth_modifier = 0.17
	army_speed_factor_for_controller = 0.13
	local_manpower = 0.08
	local_building_slots = 3
}
#NON-UNIFICATE STATE MODIFIERS
#Kamchatka
SUB_cumchatka_bad = {
	icon = "GFX_idea_sov_kamchatka_ideas"
	enable = { always = yes }

	state_production_speed_buildings_factor = -0.15
	state_productivity_growth_modifier = -0.15
}
SUB_cumchatka_good = {
	icon = "GFX_idea_sov_kamchatka_ideas"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.10
	state_productivity_growth_modifier = 0.10
}
#Perm
SUB_perm_bad = {
	icon = "GFX_idea_sov_perm_ideas"
	enable = { always = yes }

	state_production_speed_buildings_factor = -0.15
	state_productivity_growth_modifier = -0.15
}
SUB_perm_good = {
	icon = "GFX_idea_sov_perm_ideas"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.10
	state_productivity_growth_modifier = 0.10
}
#Transbaikal
SUB_transbaikal_bad = {
	icon = "GFX_idea_sov_transkbaikal_ideas"
	enable = { always = yes }

	state_production_speed_buildings_factor = -0.15
	state_productivity_growth_modifier = -0.15
}
SUB_transbaikal_good = {
	icon = "GFX_idea_sov_transkbaikal_ideas"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.10
	state_productivity_growth_modifier = 0.10
}
#Krasnoyarks
SUB_krasnoyarsk_bad = {
	icon = "GFX_idea_sov_krasnoyarsk_ideas"
	enable = { always = yes }

	state_production_speed_buildings_factor = -0.15
	state_productivity_growth_modifier = -0.15
}
SUB_krasnoyarsk_good = {
	icon = "GFX_idea_sov_krasnoyarsk_ideas"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.10
	state_productivity_growth_modifier = 0.10
}
#URAL MODIFIERS
URA_metro_modifier = {
	enable = { always = yes }
	icon = GFX_idea_ura_ekb_metro_idea
	army_speed_factor_for_controller = 0.07
	state_productivity_growth_modifier = 0.15
}
URA_metro1_modifier = {
	enable = { always = yes }
	icon = GFX_idea_ura_ekb_metro_idea
	army_speed_factor_for_controller = 0.09
	state_productivity_growth_modifier = 0.20
}
#EKATERINBURG STATE MODIFIERS
EKB_city_1_level = {
	icon = "GFX_idea_ura_ekb_federal"
	enable = { always = yes }

	state_production_speed_buildings_factor = 0.05
	state_productivity_growth_modifier = 0.05
	army_speed_factor_for_controller = 0.05
}



#KUBAN MODIFIERS 
SUB_krasnodar_metro = {
	enable = { always = yes }
	icon = GFX_idea_cub_roads
	army_speed_factor_for_controller = 0.30
	state_productivity_growth_modifier = 0.15
	state_production_speed_buildings_factor = 0.05
}

