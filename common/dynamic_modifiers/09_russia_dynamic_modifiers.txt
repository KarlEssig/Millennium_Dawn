#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

#RUSSIAN DYNAMIC MODIFIERS
# Author(s): heastel#8503
SOV_revived_politburo_modifier = {
	icon = "GFX_idea_sov_politburo"
	enable = {
		original_tag = SOV
		has_completed_focus = SOV_zyuganov
	}
	remove_trigger = {
		NOT = {
			OR = {
				emerging_communist_state_are_in_power = yes
				neutrality_neutral_communism_are_in_power = yes
			}
		}
	}
	drift_defence_factor = SOV_revived_politburo_drift_defence_factor
	communism_drift = SOV_revived_politburo_communism_support
	nationalist_drift = SOV_revived_politburo_nationalist_support
	democratic_drift = SOV_revived_politburo_democratic_support
	consumer_goods_factor = SOV_revived_politburo_tax_cost
	mobilization_speed = SOV_revived_politburo_mobilization_speed
	operative_slot = SOV_revived_politburo_operative_slot
	conscription_factor = SOV_revived_politburo_recruitable_pop
	opinion_gain_monthly_same_ideology_factor = SOV_revived_politburo_opinion_gain
	production_speed_buildings_factor = SOV_revived_politburo_construction_speed
	production_speed_arms_factory_factor = SOV_revived_politburo_military_construction_speed
	research_speed_factor = SOV_revived_politburo_research_speed
	air_accidents = SOV_revived_politburo_air_accidents
	office_park_income_tax_modifier = SOV_revived_politburo_office_tax_gain
	military_industry_tax_modifier = SOV_revived_politburo_military_industry_tax_gain
	army_attack_factor = SOV_revived_politburo_army_attack_factor
	justify_war_goal_time = SOV_revived_politburo_justify_time
}

SOV_npp_modifier = {
	icon = "GFX_idea_sov_npp_ideas"
	enable = {
		original_tag = SOV
		has_completed_focus = SOV_npp
	}
	monthly_population = SOV_npp_population
	research_speed_factor = SOV_npp_research
	production_speed_infrastructure_factor = SOV_npp_infrastructure_factor
	production_speed_offices_factor = SOV_npp_offices
	production_speed_arms_factory_factor = SOV_npp_arms_factory
	bureaucracy_cost_multiplier_modifier = SOV_npp_bureaucracy_cost
	education_cost_multiplier_modifier = SOV_npp_education_cost
	health_cost_multiplier_modifier = SOV_npp_health_cost
}

SOV_500_days_modifier = {
	icon = "GFX_idea_SOV_5500_days"
	enable = {
		original_tag = SOV
		has_completed_focus = SOV_capitalism_in_500_days
		has_country_flag = days_500_in_progress
	}
	remove_trigger = {
		NOT = {
			OR = {
				western_liberals_are_in_power = yes
				western_conservatism_are_in_power = yes
				western_social_democrats_are_in_power = yes
			}
		}
	}
	production_speed_offices_factor = SOV_500_days_offices
	production_speed_buildings_factor = SOV_500_days_building
	education_cost_multiplier_modifier = SOV_500_days_education_cost
	health_cost_multiplier_modifier = SOV_500_days_health_cost
	stability_factor = SOV_500_days_stability
	consumer_goods_factor = SOV_500_days_consumer
	tax_gain_multiplier_modifier = SOV_500_days_tax_multiplier
	projects_cost_modifier = SOV_500_days_projects_cost
	econ_cycle_upg_cost_multiplier_modifier = SOV_500_days_econ_cycle_upg
	productivity_growth_modifier = SOV_500_days_productivity_growth
}
SOV_western_sanctions_modifier = {
	icon = "GFX_idea_sov_western_sanctions_idea"
	enable = {
		original_tag = SOV
		has_country_flag = SOV_western_sanctions
	}
	remove_trigger = {
		NOT = {
			has_country_flag = SOV_western_sanctions
		}
	}
	production_speed_buildings_factor = SOV_sanctions_construction_speed
	receiving_investment_cost_modifier = SOV_sanctions_invest_cost
	return_on_investment_modifier = SOV_sanctions_roi
	opinion_gain_monthly_same_ideology_factor = SOV_sanctions_opinion_gain
	research_speed_factor = SOV_sanctions_research_speed
	consumer_goods_factor = SOV_sanctions_tax_cost
	office_park_income_tax_modifier = SOV_sanctions_office_tax_gain
}

SOV_russian_oligarchs_modifier = {
	icon = "GFX_idea_SOV_the_true_tsars"
	enable = {
		original_tag = SOV
		has_completed_focus = SOV_revive_oligarch_influence
	}
	remove_trigger = {
		NOT = {
			neutrality_neutral_oligarch_are_in_power = yes
		}
	}
	corruption_cost_factor = SOV_oligarchs_corruption_cost_factor
	production_speed_buildings_factor = SOV_oligarchs_construction_speed_factor
	receiving_investment_duration_modifier = SOV_oligarchs_investment_duration_factor
	receiving_investment_cost_modifier = SOV_oligarchs_investment_cost_factor
	consumer_goods_factor = SOV_oligarchs_tax_cost_factor
	office_park_income_tax_modifier = SOV_oligarchs_office_income_factor
	political_power_factor = SOV_oligarchs_political_power_factor
	neutrality_drift = SOV_oligarchs_neutrality_drift_factor
	local_resources_factor = SOV_oligarchs_local_resources_factor
}

# Author(s): LordBogdanoff
SOV_navalny_inner_modifier = {
	icon = "GFX_idea_Provisional_Council_of_the_Russian_Republic"
	enable = {
		original_tag = SOV
		OR = {
		has_completed_focus = SOV_navalny
		has_completed_focus = SOV_nemtsov
		has_completed_focus = SOV_age_of_stability
		has_completed_focus = SOV_yavlinskiy
		}
	}
	remove_trigger = {
		NOT = {
			OR = {
				western_liberals_are_in_power = yes
				western_conservatism_are_in_power = yes
				western_social_democrats_are_in_power = yes
			}
		}
	}
	political_power_factor = SOV_navalny_inner_political_power_factor
	stability_factor = SOV_navalny_inner_stability_factor
	democratic_drift = SOV_navalny_inner_democratic_drift
	war_support_factor = SOV_navalny_inner_war_support_factor
	nationalist_drift = SOV_navalny_inner_nationalist_drift
	monthly_population = SOV_navalny_inner_monthly_population
	tax_gain_multiplier_modifier = SOV_navalny_inner_tax_gain_multiplier_modifier
	bureaucracy_cost_multiplier_modifier = SOV_navalny_inner_bureaucracy_cost_multiplier_modifier
	drift_defence_factor = SOV_navalny_inner_drift_defence_factor
	production_speed_arms_factory_factor = SOV_navalny_inner_production_speed_arms_factory_factor
	production_factory_efficiency_gain_factor = SOV_navalny_inner_production_factory_efficiency_gain_factor
	population_tax_income_multiplier_modifier = SOV_navalny_inner_population_tax_income_multiplier_modifier
	tax_rate_change_multiplier_modifier = SOV_navalny_inner_tax_rate_change_multiplier_modifier
	econ_cycle_upg_cost_multiplier_modifier = SOV_navalny_inner_econ_cycle_upg_cost_multiplier_modifier
	conscription_factor = SOV_navalny_inner_conscription_factor
	police_cost_multiplier_modifier = SOV_navalny_inner_police_cost_multiplier_modifier
}
SOV_koba_politic_modifier = {
	icon = "GFX_idea_sov_kob_politics"
	enable = {
		original_tag = SOV
		has_completed_focus = SOV_kob_party_start
	}
	custom_modifier_tooltip = SOV_koba_attack_tooltip
	remove_trigger = {
		NOT = {
		nationalist_military_junta_are_in_power = yes
			has_country_leader = {
				name = "Constantine Petrov"
				ruling_only = yes
			}
		}
	}
	political_power_factor = SOV_koba_politic_political_power_factor
	drift_defence_factor = SOV_koba_politic_drift_defence_factor
	nationalist_drift = SOV_koba_politic_nationalist_drift
	stability_factor = SOV_koba_politic_stability_factor
	conscription_factor = SOV_koba_politic_conscription_factor
	police_cost_multiplier_modifier = SOV_koba_politic_police_cost_multiplier_modifier
	tax_gain_multiplier_modifier = SOV_koba_politic_tax_gain_multiplier_modifier
	personnel_cost_multiplier_modifier = SOV_koba_politic_personnel_cost_multiplier_modifier
	bureaucracy_cost_multiplier_modifier = SOV_koba_politic_bureaucracy_cost_multiplier_modifier
	democratic_drift = SOV_koba_politic_democratic_drift
	research_speed_factor = SOV_koba_politic_research_speed_factor
	communism_drift = SOV_koba_politic_communism_drift
	air_volunteer_cap = SOV_koba_politic_air_volunteer_cap
	air_chief_cost_factor = SOV_koba_politic_air_chief_cost_factor
	airforce_personnel_cost_multiplier_modifier = SOV_koba_politic_airforce_personnel_cost_multiplier_modifier
}

BSH_ufa_modifier = {
	icon = "GFX_idea_construction"
	enable = {
		original_tag = BSH
		has_completed_focus = BSH_souverginity
	}
	tax_gain_multiplier_modifier = BSH_ufa_modifier_tax_gain_multiplier_modifier
	local_resources_factor = BSH_ufa_modifier_local_resources_factor
	country_resource_oil = BSH_ufa_modifier_country_resource_oil
	monthly_population = BSH_ufa_modifier_monthly_population
	corporate_tax_income_multiplier_modifier = BSH_ufa_modifier_corporate_tax_income_multiplier_modifier
}

MLR_ukr_modifier = {
	icon = "GFX_idea_mlr_reform"
	enable = {
		original_tag = MLR
		has_completed_focus = MLR_start
	}
	political_power_factor = MLR_ukr_modifier_political_power_factor
	stability_factor = MLR_ukr_modifier_stability_factor
	encryption_factor = MLR_ukr_encryption_factor
	army_core_defence_factor = MLR_ukr_army_core_defence_factor
	resistance_growth = MLR_ukr_police_resistance_growth
	police_cost_multiplier_modifier = MLR_ukr_police_cost_multiplier_modifier
	foreign_influence_defense_modifier = MLR_ukr_foreign_influence_defense_modifier
	personnel_cost_multiplier_modifier = MLR_ukr_personnel_cost_multiplier_modifier
	army_defence_factor = MLR_ukr_army_defence_factor
	army_org_factor = MLR_ukr_personnel_army_org_factor
	army_attack_factor = MLR_ukr_army_attack_factor
	training_time_army_factor = MLR_ukr_training_time_army_factor
}
SOV_putin_politic_modifier = {
	icon = "GFX_idea_sov_putinism_idea"
	enable = {
		original_tag = SOV
		has_completed_focus = SOV_putin
	}
	remove_trigger = {
		NOT = {
			OR = {
				emerging_reactionaries_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
	}
	political_power_factor = SOV_putin_politic_political_power_factor
	stability_factor = SOV_putin_politic_stability_factor
	communism_drift = SOV_putin_politic_communism_drift

}

SOV_just_inner_modifier = {
	icon = "GFX_idea_sov_sr_logo"
	enable = {
		original_tag = SOV
		has_completed_focus = SOV_a_just_russia
	}
	remove_trigger = {
		NOT = {
			OR = {
				emerging_anarchist_communism_are_in_power = yes
				western_social_democrats_are_in_power = yes
				emerging_autocracy_are_in_power = yes
			}
		}
	}
	political_power_factor = SOV_just_inner_political_power_factor
	stability_factor = SOV_just_inner_stability_factor
	democratic_drift = SOV_just_inner_democratic_drift
	communism_drift = SOV_just_inner_communism_drift
	neutrality_drift = SOV_just_inner_neutrality_drift
	war_support_factor = SOV_just_inner_war_support_factor
	monthly_population = SOV_just_inner_monthly_population
	tax_gain_multiplier_modifier = SOV_just_inner_tax_gain_multiplier_modifier
	bureaucracy_cost_multiplier_modifier = SOV_just_inner_bureaucracy_cost_multiplier_modifier
	production_speed_arms_factory_factor = SOV_just_inner_production_speed_arms_factory_factor
	population_tax_income_multiplier_modifier = SOV_just_inner_population_tax_income_multiplier_modifier
	police_cost_multiplier_modifier = SOV_just_inner_police_cost_multiplier_modifier
	social_cost_multiplier_modifier = SOV_just_inner_social_cost_multiplier_modifier
	health_cost_multiplier_modifier = SOV_just_inner_health_cost_multiplier_modifier
	corporate_tax_income_multiplier_modifier = SOV_just_inner_corporate_tax_income_multiplier_modifier
	production_speed_buildings_factor = SOV_just_inner_production_speed_buildings_factor_modifier
	agriculture_tax_modifier = SOV_just_inner_agriculture_tax_modifier
	agricolture_productivity_modifier = SOV_just_inner_agricolture_productivity_modifier
	consumer_goods_factor = SOV_just_inner_consumer
}

#CONSTANTINOPLE STATE MODIFIERS
SOV_const_rebels = {
	enable = { always = yes }
	remove_trigger = {
		NOT = {
			SOV = {
				controls_state = 934
			}
		}
	}
	icon = GFX_idea_tur_militar_idea
	state_resources_factor = -0.40
	resistance_growth = 0.15
	local_building_slots_factor = -0.10
}