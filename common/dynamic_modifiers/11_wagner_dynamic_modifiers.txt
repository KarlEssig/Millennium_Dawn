#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

#Wagner DYNAMIC MODIFIERS
# Author(s): Lord Bogdanoff
WAG_Bunt_modifer = {
	enable = { 
		SOV = {
			has_war_with = WAG
		}
	 }
	icon = wagner_armies
	army_speed_factor = 0.2
	army_defence_factor = 0.2
	army_attack_speed_factor = 0.2
	army_org = 10
}
RUS_WAG_Bunt_modifer = {
	enable = { 
		SOV = {
			has_war_with = WAG
		}
	 }
	icon = wagner_armies
	army_speed_factor = -0.2
	army_defence_factor = -0.2
	army_attack_speed_factor = -0.2
	army_org = -1000
}