defined_text = {
	name = egy_slums_desc
	text = {
		trigger = { check_variable = { EGY_slums_left > 11 } }
		localization_key = egy_slums_alot_left
	}
	text = {
		trigger = {
			check_variable = { EGY_slums_left > 6 }
			check_variable = { EGY_slums_left < 12 }
		}
		localization_key = egy_slums_medium_left
	}
	text = {
		trigger = {
			check_variable = { EGY_slums_left > 0 }
			check_variable = { EGY_slums_left < 7 }
		}
		localization_key = egy_slums_small_left
	}
	text = {
		trigger = { check_variable = { EGY_slums_left < 1 } }
		localization_key = egy_slums_no_left
	}
}

defined_text = {
	name = EGY_copt_new_opinion
	text = {
		trigger = { has_idea = EGY_Copts_is_very_satisfied_idea }
		localization_key = EGY_Copts_is_very_satisfied_idea
	}
	text = {
		trigger = { has_idea = EGY_Copts_is_satisfied_idea }
		localization_key = EGY_Copts_is_satisfied_idea
	}
	text = {
		trigger = { has_idea = EGY_Copts_are_neutral_idea }
		localization_key = EGY_Copts_are_neutral_idea
	}
	text = {
		trigger = { has_idea = EGY_Copts_are_not_satisfied_idea }
		localization_key = EGY_Copts_are_not_satisfied_idea
	}
	text = {
		trigger = { has_idea = EGY_Copts_are_enraged_idea }
		localization_key = EGY_Copts_are_enraged_idea
	}
}