defined_text = {
	name = selected_culture_index

	text = {
		trigger = { check_variable = { culture_index = 0 } }
		localization_key = catalonians_loc
	}
	text = {
		trigger = { check_variable = { culture_index = 1 } }
		localization_key = basques_loc
	}
	text = {
		trigger = { check_variable = { culture_index = 2 } }
		localization_key = galicians_loc
	}
	text = {
		trigger = { check_variable = { culture_index = 3 } }
		localization_key = andalucians_loc
	}
	text = {
		trigger = { check_variable = { culture_index = 4 } }
		localization_key = canarians_loc
	}
}
defined_text = {
	name = selected_culture_index_cur_opin

	text = {
		trigger = { check_variable = { culture_index = 0 } }
		localization_key = SPR_culture_index_1
	}
	text = {
		trigger = { check_variable = { culture_index = 1 } }
		localization_key = SPR_culture_index_2
	}
	text = {
		trigger = { check_variable = { culture_index = 2 } }
		localization_key = SPR_culture_index_3
	}
	text = {
		trigger = { check_variable = { culture_index = 3 } }
		localization_key = SPR_culture_index_4
	}
	text = {
		trigger = { check_variable = { culture_index = 4 } }
		localization_key = SPR_culture_index_5
	}
}