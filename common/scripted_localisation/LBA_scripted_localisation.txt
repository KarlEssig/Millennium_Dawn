#Tribalism stuff
defined_text = {
	name = tripolitania_loyalty_value

	text = {
		trigger = {
			check_variable = { tripolitania_loyalty > 0.49 }
		}
		localization_key = "tripolitania_loyalty_good"
	}
	text = {
		trigger = {
			check_variable = { tripolitania_loyalty < 0.49 }
			check_variable = { tripolitania_loyalty > 0.20 }
		}
		localization_key = "tripolitania_loyalty_neutral"
	}
	text = {
		trigger = {
			check_variable = { tripolitania_loyalty < 0.20 }
		}
		localization_key = "tripolitania_loyalty_bad"
	}

}

defined_text = {
	name = tripolitania_text_box_effect_tt

	text = {
		trigger = {
			check_variable = { tripolitania_loyalty > 0.49 }
		}
		localization_key = "loyalty_good_effect"
	}
	text = {
		trigger = {
			check_variable = { tripolitania_loyalty < 0.49 }
			check_variable = { tripolitania_loyalty > 0.20 }
		}
		localization_key = "loyalty_neutral_effect"
	}
	text = {
		trigger = {
			check_variable = { tripolitania_loyalty < 0.20 }
		}
		localization_key = "loyalty_bad_effect"
	}

}

defined_text = {
	name = tripolitania_map

	text = {
		trigger = {
			check_variable = { tripolitania_loyalty > 0.49 }
		}
		localization_key = "GFX_LBA_libya_tripolitania_map_good"
	}
	text = {
		trigger = {
			check_variable = { tripolitania_loyalty < 0.49 }
			check_variable = { tripolitania_loyalty > 0.20 }
		}
		localization_key = "GFX_LBA_libya_tripolitania_map_neutral"
	}
	text = {
		trigger = {
			check_variable = { tripolitania_loyalty < 0.20 }
		}
		localization_key = "GFX_LBA_libya_tripolitania_map_bad"
	}

}

defined_text = {
	name = cyrenaica_loyalty_value

	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty > 0.49 }
		}
		localization_key = "cyrenaica_loyalty_good"
	}
	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty < 0.49 }
			check_variable = { cyrenaica_loyalty > 0.20 }
		}
		localization_key = "cyrenaica_loyalty_neutral"
	}
	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty < 0.20 }
		}
		localization_key = "cyrenaica_loyalty_bad"
	}

}

defined_text = {
	name = cyrenaica_text_box_effect_tt

	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty > 0.49 }
		}
		localization_key = "loyalty_good_effect"
	}
	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty < 0.49 }
			check_variable = { cyrenaica_loyalty > 0.20 }
		}
		localization_key = "loyalty_neutral_effect"
	}
	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty < 0.20 }
		}
		localization_key = "loyalty_bad_effect"
	}

}

defined_text = {
	name = cyrenaica_map

	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty > 0.49 }
		}
		localization_key = "GFX_LBA_libya_cyrenaica_map_good"
	}
	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty < 0.49 }
			check_variable = { cyrenaica_loyalty > 0.20 }
		}
		localization_key = "GFX_LBA_libya_cyrenaica_map_neutral"
	}
	text = {
		trigger = {
			check_variable = { cyrenaica_loyalty < 0.20 }
		}
		localization_key = "GFX_LBA_libya_cyrenaica_map_bad"
	}

}

defined_text = {
	name = fezzan_loyalty_value

	text = {
		trigger = {
			check_variable = { fezzan_loyalty > 0.49 }
		}
		localization_key = "fezzan_loyalty_good"
	}
	text = {
		trigger = {
			check_variable = { fezzan_loyalty < 0.49 }
			check_variable = { fezzan_loyalty > 0.20 }
		}
		localization_key = "fezzan_loyalty_neutral"
	}
	text = {
		trigger = {
			check_variable = { fezzan_loyalty < 0.20 }
		}
		localization_key = "fezzan_loyalty_bad"
	}

}

defined_text = {
	name = fezzan_text_box_effect_tt

	text = {
		trigger = {
			check_variable = { fezzan_loyalty > 0.49 }
		}
		localization_key = "loyalty_good_effect"
	}
	text = {
		trigger = {
			check_variable = { fezzan_loyalty < 0.49 }
			check_variable = { fezzan_loyalty > 0.20 }
		}
		localization_key = "loyalty_neutral_effect"
	}
	text = {
		trigger = {
			check_variable = { fezzan_loyalty < 0.20 }
		}
		localization_key = "loyalty_bad_effect"
	}

}

defined_text = {
	name = fezzan_map

	text = {
		trigger = {
			check_variable = { fezzan_loyalty > 0.49 }
		}
		localization_key = "GFX_LBA_libya_fezzan_map_good"
	}
	text = {
		trigger = {
			check_variable = { fezzan_loyalty < 0.49 }
			check_variable = { fezzan_loyalty > 0.20 }
		}
		localization_key = "GFX_LBA_libya_fezzan_map_neutral"
	}
	text = {
		trigger = {
			check_variable = { fezzan_loyalty < 0.20 }
		}
		localization_key = "GFX_LBA_libya_fezzan_map_bad"
	}

}

defined_text = {
	name = tribal_offender

	text = {
		trigger = {
			has_country_flag = LBA_tribalism_tripolitanian_offender
		}
		localization_key = "LBA_tripolitanian"
	}
	text = {
		trigger = {
			has_country_flag = LBA_tribalism_cyrenaican_offender
		}
		localization_key = "LBA_cyrenaican"
	}
	text = {
		trigger = {
			has_country_flag = LBA_tribalism_fezzani_offender
		}
		localization_key = "LBA_fezzani"
	}
}

defined_text = {
	name = tribal_defender

	text = {
		trigger = {
			has_country_flag = LBA_tribalism_tripolitanian_defender
		}
		localization_key = "LBA_tripolitanian"
	}
	text = {
		trigger = {
			has_country_flag = LBA_tribalism_cyrenaican_defender
		}
		localization_key = "LBA_cyrenaican"
	}
	text = {
		trigger = {
			has_country_flag = LBA_tribalism_fezzani_defender
		}
		localization_key = "LBA_fezzani"
	}
}


#Gaddafi family
defined_text = {
	name = muammar_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = muammar_gaddafi_dead }
			NOT = { has_country_flag = muammar_gaddafi_exile }
		}
		localization_key = "GFX_muammar_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muammar_gaddafi_dead }
			has_country_flag = muammar_gaddafi_exile
		}
		localization_key = "GFX_muammar_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = muammar_gaddafi_dead
		}
		localization_key = "GFX_muammar_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = muhammad_muammar_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = muhammad_muammar_gaddafi_dead }
			NOT = { has_country_flag = muhammad_muammar_gaddafi_exile }
		}
		localization_key = "GFX_muhammad_muammar_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muhammad_muammar_gaddafi_dead }
			has_country_flag = muhammad_muammar_gaddafi_exile
		}
		localization_key = "GFX_muhammad_muammar_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = muhammad_muammar_gaddafi_dead
		}
		localization_key = "GFX_muhammad_muammar_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = saif_al_islam_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_islam_gaddafi_dead }
			NOT = { has_country_flag = saif_al_islam_gaddafi_exile }
		}
		localization_key = "GFX_saif_al_islam_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_islam_gaddafi_dead }
			has_country_flag = saif_al_islam_gaddafi_exile
		}
		localization_key = "GFX_saif_al_islam_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = saif_al_islam_gaddafi_dead
		}
		localization_key = "GFX_saif_al_islam_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = al_saadi_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = al_saadi_gaddafi_dead }
			NOT = { has_country_flag = al_saadi_gaddafi_exile }
		}
		localization_key = "GFX_al_saadi_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = al_saadi_gaddafi_dead }
			has_country_flag = al_saadi_gaddafi_exile
		}
		localization_key = "GFX_al_saadi_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = al_saadi_gaddafi_dead
		}
		localization_key = "GFX_al_saadi_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = mutassim_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = mutassim_gaddafi_dead }
			NOT = { has_country_flag = mutassim_gaddafi_exile }
		}
		localization_key = "GFX_mutassim_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = mutassim_gaddafi_dead }
			has_country_flag = mutassim_gaddafi_exile
		}
		localization_key = "GFX_mutassim_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = mutassim_gaddafi_dead
		}
		localization_key = "GFX_mutassim_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = hannibal_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_dead }
			NOT = { has_country_flag = hannibal_gaddafi_exile }
		}
		localization_key = "GFX_hannibal_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_dead }
			has_country_flag = hannibal_gaddafi_exile
		}
		localization_key = "GFX_hannibal_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = hannibal_gaddafi_dead
		}
		localization_key = "GFX_hannibal_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = ayesha_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_dead }
			NOT = { has_country_flag = ayesha_gaddafi_exile }
		}
		localization_key = "GFX_ayesha_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_dead }
			has_country_flag = ayesha_gaddafi_exile
		}
		localization_key = "GFX_ayesha_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = ayesha_gaddafi_dead
		}
		localization_key = "GFX_ayesha_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = saif_al_arab_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_arab_gaddafi_dead }
			NOT = { has_country_flag = saif_al_arab_gaddafi_exile }
		}
		localization_key = "GFX_saif_al_arab_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_arab_gaddafi_dead }
			has_country_flag = saif_al_arab_gaddafi_exile
		}
		localization_key = "GFX_saif_al_arab_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = saif_al_arab_gaddafi_dead
		}
		localization_key = "GFX_saif_al_arab_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = khamis_gaddafi_portrait

	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_dead }
			NOT = { has_country_flag = khamis_gaddafi_exile }
		}
		localization_key = "GFX_khamis_gaddafi_portrait"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_dead }
			has_country_flag = khamis_gaddafi_exile
		}
		localization_key = "GFX_khamis_gaddafi_portrait_exile"
	}
	text = {
		trigger = {
			has_country_flag = khamis_gaddafi_dead
		}
		localization_key = "GFX_khamis_gaddafi_portrait_dead"
	}

}

defined_text = {
	name = muammar_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Muammar Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Muammar Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muammar_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = muammar_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muammar_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muammar_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = muammar_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muammar_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muhammad_muammar_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Muhammad Muammar Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Muhammad Muammar Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muhammad_muammar_gaddafi_heir

	text = {
		trigger = {
			has_country_flag = muhammad_muammar_gaddafi_heir
		}
		localization_key = "LBA_Current_Heir"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muhammad_muammar_gaddafi_heir }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muhammad_muammar_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = muhammad_muammar_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muhammad_muammar_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muhammad_muammar_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = muhammad_muammar_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muhammad_muammar_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muhammad_muammar_gaddafi_head_of_GPTC

	text = {
		trigger = {
			has_country_flag = muhammad_muammar_gaddafi_head_of_GPTC
		}
		localization_key = "LBA_Head_of_GPTC"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muhammad_muammar_gaddafi_head_of_GPTC }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muhammad_muammar_gaddafi_uninterested_in_politics

	text = {
		trigger = {
			has_country_flag = muhammad_muammar_gaddafi_uninterested_in_politics
		}
		localization_key = "LBA_Uninterested_in_Politics"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muhammad_muammar_gaddafi_uninterested_in_politics }
		}
		localization_key = ""
	}

}

defined_text = {
	name = muhammad_muammar_gaddafi_king_of_coke

	text = {
		trigger = {
			has_country_flag = muhammad_muammar_gaddafi_king_of_coke
		}
		localization_key = "LBA_King_of_Coke"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = muhammad_muammar_gaddafi_king_of_coke }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_islam_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Saif al-Islam Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Saif al-Islam Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_islam_gaddafi_heir

	text = {
		trigger = {
			has_country_flag = saif_al_islam_gaddafi_heir
		}
		localization_key = "LBA_Current_Heir"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_islam_gaddafi_heir }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_islam_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = saif_al_islam_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_islam_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_islam_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = saif_al_islam_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_islam_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_islam_gaddafi_master_maneuverer

	text = {
		trigger = {
			has_country_flag = saif_al_islam_gaddafi_master_maneuverer
		}
		localization_key = "LBA_Master_Maneuverer"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_islam_gaddafi_master_maneuverer }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_islam_gaddafi_geopolitical_thinker

	text = {
		trigger = {
			has_country_flag = saif_al_islam_gaddafi_geopolitical_thinker
		}
		localization_key = "LBA_Geopolitical_Thinker"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_islam_gaddafi_geopolitical_thinker }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_islam_gaddafi_democratic_reformer

	text = {
		trigger = {
			has_country_flag = saif_al_islam_gaddafi_democratic_reformer
		}
		localization_key = "LBA_democratic_reformer"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_islam_gaddafi_democratic_reformer }
		}
		localization_key = ""
	}

}

defined_text = {
	name = al_saadi_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Al-Saadi Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Al-Saadi Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = al_saadi_gaddafi_heir

	text = {
		trigger = {
			has_country_flag = al_saadi_gaddafi_heir
		}
		localization_key = "LBA_Current_Heir"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = al_saadi_gaddafi_heir }
		}
		localization_key = ""
	}

}

defined_text = {
	name = al_saadi_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = al_saadi_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = al_saadi_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = al_saadi_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = al_saadi_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = al_saadi_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = al_saadi_gaddafi_vain_football_star

	text = {
		trigger = {
			has_country_flag = al_saadi_gaddafi_vain_football_star
		}
		localization_key = "LBA_Vain_Football_Star"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = al_saadi_gaddafi_vain_football_star }
		}
		localization_key = ""
	}

}

defined_text = {
	name = al_saadi_gaddafi_playboy_lifestyle

	text = {
		trigger = {
			has_country_flag = al_saadi_gaddafi_playboy_lifestyle
		}
		localization_key = "LBA_Playboy_Lifestyle"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = al_saadi_gaddafi_playboy_lifestyle }
		}
		localization_key = ""
	}

}

defined_text = {
	name = al_saadi_gaddafi_substance_abuser

	text = {
		trigger = {
			has_country_flag = al_saadi_gaddafi_substance_abuser
		}
		localization_key = "LBA_Substance_Abuser"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = al_saadi_gaddafi_substance_abuser }
		}
		localization_key = ""
	}

}

defined_text = {
	name = mutassim_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Al-Saadi Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Al-Saadi Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = mutassim_gaddafi_heir

	text = {
		trigger = {
			has_country_flag = mutassim_gaddafi_heir
		}
		localization_key = "LBA_Current_Heir"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = mutassim_gaddafi_heir }
		}
		localization_key = ""
	}

}

defined_text = {
	name = mutassim_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = mutassim_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = mutassim_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = mutassim_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = mutassim_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = mutassim_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = mutassim_gaddafi_77th_tank_battalion

	text = {
		trigger = {
			has_country_flag = mutassim_gaddafi_77th_tank_battalion
		}
		localization_key = "LBA_77th_Tank_Battalion"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = mutassim_gaddafi_77th_tank_battalion }
		}
		localization_key = ""
	}

}

defined_text = {
	name = mutassim_gaddafi_suspected_revolutionary

	text = {
		trigger = {
			has_country_flag = mutassim_gaddafi_suspected_revolutionary
		}
		localization_key = "LBA_suspected_revolutionary"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = mutassim_gaddafi_suspected_revolutionary }
		}
		localization_key = ""
	}

}

defined_text = {
	name = mutassim_gaddafi_king_of_coke

	text = {
		trigger = {
			has_country_flag = mutassim_gaddafi_king_of_coke
		}
		localization_key = "LBA_King_of_Coke"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = mutassim_gaddafi_king_of_coke }
		}
		localization_key = ""
	}

}

defined_text = {
	name = mutassim_gaddafi_national_security_advisor

	text = {
		trigger = {
			has_country_flag = mutassim_gaddafi_national_security_advisor
		}
		localization_key = "LBA_National_Security_Advisor"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = mutassimutassim_gaddafi_national_security_advisorm_gaddafi_king_of_coke }
		}
		localization_key = ""
	}

}

defined_text = {
	name = hannibal_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Hannibal Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Hannibal Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = hannibal_gaddafi_heir

	text = {
		trigger = {
			has_country_flag = hannibal_gaddafi_heir
		}
		localization_key = "LBA_Current_Heir"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_heir }
		}
		localization_key = ""
	}

}

defined_text = {
	name = hannibal_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = hannibal_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = hannibal_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = hannibal_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = hannibal_gaddafi_bachelor_of_marine_navigation

	text = {
		trigger = {
			has_country_flag = hannibal_gaddafi_bachelor_of_marine_navigation
		}
		localization_key = "LBA_Bachelor_of_Marine_Navigation"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_bachelor_of_marine_navigation }
		}
		localization_key = ""
	}

}

defined_text = {
	name = hannibal_gaddafi_master_of_shipping_economics_and_logistics

	text = {
		trigger = {
			has_country_flag = hannibal_gaddafi_master_of_shipping_economics_and_logistics
		}
		localization_key = "LBA_Master_of_Shipping_Economics_and_Logistics"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_master_of_shipping_economics_and_logistics }
		}
		localization_key = ""
	}

}

defined_text = {
	name = hannibal_gaddafi_graduate_of_kuznetsov

	text = {
		trigger = {
			has_country_flag = hannibal_gaddafi_graduate_of_kuznetsov
		}
		localization_key = "LBA_Graduate_of_Kuznetsov"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_graduate_of_kuznetsov }
		}
		localization_key = ""
	}

}

defined_text = {
	name = hannibal_gaddafi_scholarly_challenged

	text = {
		trigger = {
			has_country_flag = hannibal_gaddafi_scholarly_challenged
		}
		localization_key = "LBA_Scholarly_Challenged"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = hannibal_gaddafi_scholarly_challenged }
		}
		localization_key = ""
	}

}

defined_text = {
	name = ayesha_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Ayesha Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Ayesha Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = ayesha_gaddafi_heir

	text = {
		trigger = {
			has_country_flag = ayesha_gaddafi_heir
		}
		localization_key = "LBA_Current_Heir"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_heir }
		}
		localization_key = ""
	}

}

defined_text = {
	name = ayesha_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = ayesha_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = ayesha_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = ayesha_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = ayesha_gaddafi_claudia_schiffer

	text = {
		trigger = {
			has_country_flag = ayesha_gaddafi_claudia_schiffer
		}
		localization_key = "LBA_Claudia_Schiffer_of_North_Africa"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_claudia_schiffer }
		}
		localization_key = ""
	}

}

defined_text = {
	name = ayesha_gaddafi_un_goodwill_ambassador

	text = {
		trigger = {
			has_country_flag = ayesha_gaddafi_un_goodwill_ambassador
		}
		localization_key = "LBA_UN_Goodwill_Ambassador"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_un_goodwill_ambassador }
		}
		localization_key = ""
	}

}

defined_text = {
	name = ayesha_gaddafi_amazon_of_libya

	text = {
		trigger = {
			has_country_flag = ayesha_gaddafi_amazon_of_libya
		}
		localization_key = "LBA_Amazon_of_Libya"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_amazon_of_libya }
		}
		localization_key = ""
	}

}

defined_text = {
	name = ayesha_gaddafi_inexperienced_general

	text = {
		trigger = {
			has_country_flag = ayesha_gaddafi_inexperienced_general
		}
		localization_key = "LBA_Inexperienced_General"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = ayesha_gaddafi_inexperienced_general }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_arab_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Saif al-Arab Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Saif al-Arab Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_arab_gaddafi_heir

	text = {
		trigger = {
			has_country_flag = saif_al_arab_gaddafi_heir
		}
		localization_key = "LBA_Current_Heir"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_arab_gaddafi_heir }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_arab_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = saif_al_arab_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_arab_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_arab_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = saif_al_arab_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_arab_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = saif_al_arab_gaddafi_convicted_criminal

	text = {
		trigger = {
			has_country_flag = saif_al_arab_gaddafi_convicted_criminal
		}
		localization_key = "LBA_Convicted_Criminal"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = saif_al_arab_gaddafi_convicted_criminal }
		}
		localization_key = ""
	}

}

defined_text = {
	name = khamis_gaddafi_ruler

	text = {
		trigger = {
			has_country_leader = { name = "Khamis Gaddafi" ruling_only = yes }
		}
		localization_key = "LBA_Current_Ruler"
	}
	text = {
		trigger = {
			NOT = { has_country_leader = { name = "Khamis Gaddafi" ruling_only = yes } }
		}
		localization_key = ""
	}

}

defined_text = {
	name = khamis_gaddafi_heir

	text = {
		trigger = {
			has_country_flag = khamis_gaddafi_heir
		}
		localization_key = "LBA_Current_Heir"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_heir }
		}
		localization_key = ""
	}

}

defined_text = {
	name = khamis_gaddafi_exile

	text = {
		trigger = {
			has_country_flag = khamis_gaddafi_exile
		}
		localization_key = "LBA_Person_Exiled"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_exile }
		}
		localization_key = ""
	}

}

defined_text = {
	name = khamis_gaddafi_dead

	text = {
		trigger = {
			has_country_flag = khamis_gaddafi_dead
		}
		localization_key = "LBA_Person_Dead"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_dead }
		}
		localization_key = ""
	}

}

defined_text = {
	name = khamis_gaddafi_underage

	text = {
		trigger = {
			has_country_flag = khamis_gaddafi_underage
		}
		localization_key = "LBA_Underage"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_underage }
		}
		localization_key = ""
	}

}

defined_text = {
	name = khamis_gaddafi_khamis_brigade

	text = {
		trigger = {
			has_country_flag = khamis_gaddafi_khamis_brigade
		}
		localization_key = "LBA_Khamis_Brigade"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_khamis_brigade }
		}
		localization_key = ""
	}

}

defined_text = {
	name = khamis_gaddafi_war_industrialist

	text = {
		trigger = {
			has_country_flag = khamis_gaddafi_war_industrialist
		}
		localization_key = "LBA_War_Industrialist"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_war_industrialist }
		}
		localization_key = ""
	}

}

defined_text = {
	name = khamis_gaddafi_captain_of_industry

	text = {
		trigger = {
			has_country_flag = khamis_gaddafi_captain_of_industry
		}
		localization_key = "LBA_Captain_Of_Industry"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = khamis_gaddafi_captain_of_industry }
		}
		localization_key = ""
	}

}