
#UNION STATE STUFF
defined_text = {
	name = belarus_ready_union_state
	text = {
		trigger = {
			country_exists = BLR
		}
		localization_key = "BLR_belarus_ready_union_state"
	}
	text = {
		trigger = {
			NOT = { country_exists = BLR }
		}
		localization_key = ""
	}
}
defined_text = {
	name = belarus_readys_union_state
	text = {
		trigger = {
			country_exists = BLR
		}
		localization_key = "BLR_belarus_readys_union_state"
	}
	text = {
		trigger = {
			NOT = { country_exists = BLR }
		}
		localization_key = "BLR_belarus_notreadys_union_state"
	}
}
defined_text = {
	name = serbia_ready_union_state
	text = {
		trigger = {
			country_exists = SER
			SER = { has_completed_focus = SER_union_state } 
		}
		localization_key = "SER_serbia_ready_union_state"
	}
	text = {
		trigger = {
			OR = {
				NOT = { country_exists = SER }
				NOT = { SER = { has_completed_focus = SER_union_state } }
			}
		}
		localization_key = ""
	}
}
defined_text = {
	name = serbia_readys_union_state
	text = {
		trigger = {
			country_exists = SER
			SER = { has_completed_focus = SER_union_state } 
		}
		localization_key = "SER_serbia_readys_union_state"
	}
	text = {
		trigger = {
			OR = {
				NOT = { country_exists = SER }
				NOT = { SER = { has_completed_focus = SER_union_state } }
			}
		}
		localization_key = "SER_serbia_notreadys_union_state"
	}
}
defined_text = {
	name = israel_ready_union_state
	text = {
		trigger = {
			country_exists = ISR
			ISR = { has_country_flag = ISR_union_state } 
		}
		localization_key = "ISR_israel_ready_union_state"
	}
	text = {
		trigger = {
			OR = {
				NOT = { country_exists = ISR }
				NOT = { ISR = { has_country_flag = ISR_union_state } }
			}
		}
		localization_key = ""
	}
}
defined_text = {
	name = israel_readys_union_state
	text = {
		trigger = {
			country_exists = ISR
			ISR = { has_country_flag = ISR_union_state  } 
		}
		localization_key = "ISR_israel_readys_union_state"
	}
	text = {
		trigger = {
			OR = {
				NOT = { country_exists = ISR }
				NOT = { ISR = { has_country_flag = ISR_union_state } }
			}
		}
		localization_key = "ISR_israel_notreadys_union_state"
	}
}
defined_text = {
	name = armenia_ready_union_state
	text = {
		trigger = {
			country_exists = ARM
			ARM = { has_country_flag = ARM_union_state } 
		}
		localization_key = "ARM_armenia_ready_union_state"
	}
	text = {
		trigger = {
			OR = {
				NOT = { country_exists = ARM }
				NOT = { ARM = { has_country_flag = ARM_union_state } }
			}
		}
		localization_key = ""
	}
}
defined_text = {
	name = armenia_readys_union_state
	text = {
		trigger = {
			country_exists = ARM
			ARM = { has_country_flag = ARM_union_state  } 
		}
		localization_key = "ARM_armenia_readys_union_state"
	}
	text = {
		trigger = {
			OR = {
				NOT = { country_exists = ARM }
				NOT = { ARM = { has_country_flag = ARM_union_state } }
			}
		}
		localization_key = "ARM_armenia_notreadys_union_state"
	}
}
defined_text = {
	name = ukraine_ready_union_state
	text = {
		trigger = {
			country_exists = UKR
			UKR = { has_country_flag = UKR_union_state } 
		}
		localization_key = "UKR_ukraine_ready_union_state"
	}
	text = {
		trigger = {
			OR = {
				NOT = { country_exists = UKR }
				NOT = { UKR = { has_country_flag = UKR_union_state } }
			}
		}
		localization_key = ""
	}
}
defined_text = {
	name = ukraine_readys_union_state
	text = {
		trigger = {
			country_exists = UKR
			UKR = { has_country_flag = UKR_union_state  } 
		}
		localization_key = "UKR_ukraine_readys_union_state"
	}
	text = {
		trigger = {
			OR = {
				NOT = { country_exists = UKR }
				NOT = { UKR = { has_country_flag = UKR_union_state } }
			}
		}
		localization_key = "UKR_ukraine_notreadys_union_state"
	}
}
defined_text = {
	name = union_state_flag_sov_or_blr
	text = {
		trigger = {
			tag = BLR
		}
		localization_key = "SOV_union_state_flag"
	}
	text = {
		trigger = {
			tag = SOV
		}
		localization_key = "BLR_union_state_flag"
	}
}
defined_text = {
	name = union_state_flag_sov_or_ser
	text = {
		trigger = {
			tag = SER
		}
		localization_key = "SOV_union_state_flag"
	}
	text = {
		trigger = {
			tag = SOV
		}
		localization_key = "SER_union_state_flag"
	}
}
defined_text = {
	name = union_state_flag_sov_or_ukr
	text = {
		trigger = {
			tag = UKR
		}
		localization_key = SOV_union_state_flag
	}
	text = {
		trigger = {
			tag = SOV
		}
		localization_key = UKR_union_state_flag
	}
}


######UNION STATE MINISTRY#####
######UNION PARLIAMENT#####
defined_text = {
	name = USR_Union_Parliament_loc

	text = { 
		trigger = {
			has_idea = USR_parlament_idea
		}
		localization_key = USR_Union_Parliament_loc1
	}
}
######SUPREME STATE COUNCIL#####
defined_text = {
	name = USR_Supreme_State_Council_loc

	text = { 
		trigger = {
			has_idea = USR_gos_sovet_idea
		}
		localization_key = USR_Supreme_State_Council_loc1
	}
}
######COUNCIL OF MINISTERS#####
defined_text = {
	name = USR_Council_Ministers_loc

	text = { 
		trigger = {
			has_idea = USR_council_ministers_idea
		}
		localization_key = USR_Council_Ministers_loc1
	}
}
######ACCOUNTING CHAMBER#####
defined_text = {
	name = USR_Accounting_Chamber_loc

	text = { 
		trigger = {
			has_idea = USR_money_palata_idea
		}
		localization_key = USR_Accounting_Chamber_loc1
	}
}
######COURT OF THE UNION#####
defined_text = {
	name = USR_Court_Union_loc

	text = { 
		trigger = {
			has_idea = USR_eternal_commitie_idea
		}
		localization_key = USR_Court_Union_loc1
	}
}
######SINGLE CURRENCY#####
defined_text = {
	name = USR_Single_Currency_loc

	text = { 
		trigger = {
			has_idea = USR_valute_idea
		}
		localization_key = USR_Single_Currency_loc1
	}
}
######UNIFIED CITIZENSHIP#####
defined_text = {
	name = USR_Unified_Citizenship_loc

	text = { 
		trigger = {
			has_idea = USR_civils_idea
		}
		localization_key = USR_Unified_Citizenship_loc1
	}
}
######COMMON ECONOMIC SPACE#####
defined_text = {
	name = USR_Common_Economic_Space_loc

	text = { 
		trigger = {
			has_idea = USR_economic_space_idea
		}
		localization_key = USR_Common_Economic_Space_loc1
	}
}


