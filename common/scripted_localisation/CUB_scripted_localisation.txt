defined_text = {
	text = {
		trigger = {
			NOT = {
				HAI = {
				is_subject_of = CUB
				exists = yes
				}
			}
		}
		localization_key = Confederaion_HAI_no_tt
	}
	text = {
		trigger = {
			HAI = {
				is_subject_of = CUB
				exists = yes
			}
		}
		localization_key = Confederaion_HAI_yes_tt
	}
	name = Confederaion_HAI_loc
}
defined_text = {
	text = {
		trigger = {
			NOT = {
				DOM = {
				is_subject_of = CUB
				exists = yes
				}
			}
		}
		localization_key = Confederaion_DOM_loc_no_tt
	}
	text = {
		trigger = {
			DOM = {
				is_subject_of = CUB
				exists = yes
			}
		}
		localization_key = Confederaion_DOM_loc_yes_tt
	}
	name = Confederaion_DOM_loc
}
defined_text = {
	text = {
		trigger = {
			NOT = {
				COL = {
				is_subject_of = CUB
				exists = yes
				}
			}
		}
		localization_key = Confederaion_COL_loc_no_tt
	}
	text = {
		trigger = {
			COL = {
				is_subject_of = CUB
				exists = yes
			}
		}
		localization_key = Confederaion_COL_loc_yes_tt
	}
	name = Confederaion_COL_loc
}
defined_text = {
	text = {
		trigger = {
			NOT = {
				JAM = {
				is_subject_of = CUB
				exists = yes
				}
			}
		}
		localization_key = Confederaion_JAM_loc_no_tt
	}
	text = {
		trigger = {
			JAM = {
				is_subject_of = CUB
				exists = yes
			}
		}
		localization_key = Confederaion_JAM_loc_yes_tt
	}
	name = Confederaion_JAM_loc
}
defined_text = {
	text = {
		trigger = {
			NOT = {
				PTR = {
				is_subject_of = CUB
				exists = yes
				}
			}
		}
		localization_key = Confederaion_PTR_loc_no_tt
	}
	text = {
		trigger = {
			PTR = {
				is_subject_of = CUB
				exists = yes
			}
		}
		localization_key = Confederaion_PTR_loc_yes_tt
	}
	name = Confederaion_PTR_loc
}

defined_text = {
	text = {
		trigger = {
				USA = {
				has_country_flag = blockade_cuba_yes
				}

		}
		localization_key = blockade_USA_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				USA = {
				has_country_flag = blockade_cuba_yes
				}
			}

		}
		localization_key = blockade_USA_loc_no_tt
	}
	name = blockade_USA_loc
}
defined_text = {
	text = {
		trigger = {
				CAN = {
				has_country_flag = blockade_cuba_yes
				}

		}
		localization_key = blockade_CAN_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				CAN = {
				has_country_flag = blockade_cuba_yes
				}
			}

		}
		localization_key = blockade_CAN_loc_no_tt
	}
	name = blockade_CAN_loc
}
defined_text = {
	text = {
		trigger = {
			DOM = {
				has_country_flag = blockade_cuba_yes
				}

		}
		localization_key = blockade_DOM_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				DOM = {
				has_country_flag = blockade_cuba_yes
				}
			}

		}
		localization_key = blockade_DOM_loc_no_tt
	}
	name = blockade_DOM_loc
}
defined_text = {
	name = blockade_HAI_loc
	text = {
		trigger = {
					HAI = {
				has_country_flag = blockade_cuba_yes
			}

		}
		localization_key = blockade_HAI_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				HAI = {
				has_country_flag = blockade_cuba_yes
				}
			}

		}
		localization_key = blockade_HAI_loc_no_tt
	}
}
defined_text = {
	name = blockade_MEX_loc
	text = {
		trigger = {
				MEX = {
				has_country_flag = blockade_cuba_yes
				}
		}
		localization_key = blockade_MEX_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				MEX = {
				has_country_flag = blockade_cuba_yes
				}
			}
		}
		localization_key = blockade_MEX_loc_no_tt
	}
}
defined_text = {
	name = military_coop_russia_unionstate
	text = {
		trigger = {
			SOV = { has_cosmetic_tag = BLR_UNS_Communism }
		}
		localization_key = cub_military_coop_with_unionstate_tt
	}
	text = {
		trigger = {
			SOV = { NOT = { has_cosmetic_tag = BLR_UNS_Communism } }
		}
		localization_key = cub_military_coop_with_sov_tt
	}
}
defined_text = {
	name = military_coop_russia_unionstate_name
	text = {
		trigger = {
			SOV = { has_cosmetic_tag = BLR_UNS_Communism }
		}
		localization_key = cub_military_coop_with_unionstate_name_tt
	}
	text = {
		trigger = {
			SOV = { NOT = { has_cosmetic_tag = BLR_UNS_Communism } }
		}
		localization_key = cub_military_coop_with_sov_name_tt
	}
}