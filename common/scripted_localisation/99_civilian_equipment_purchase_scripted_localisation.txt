defined_text = {
	name = does_not_have_equipment_purchased_scripted_loc

	text = {
		trigger = { NOT = { has_country_flag = { flag = equipment_purchased_flag value = 1 } } }
		localization_key = does_not_have_equipment_purchased_scripted_loc_one
	}
	text = {
		trigger = { has_country_flag = { flag = equipment_purchased_flag value = 1 } }
		localization_key = does_not_have_equipment_purchased_scripted_loc_two
	}
}