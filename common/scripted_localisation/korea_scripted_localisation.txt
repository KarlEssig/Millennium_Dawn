defined_text = {
	name = 38th_parallel

	text = {
		trigger = {
			original_tag = KOR
		}
		localization_key = 38th_parallel_KOR
	}

	text = {
		trigger = {
			original_tag = NKO
		}
		localization_key = 38th_parallel_NKO
	}
}

defined_text = {
	name = recognises_UNSC_seat_USA

	text = {
		trigger = {
			USA = {
				has_country_flag = USA_recognition_campaign_UNSC
			}
		}
		localization_key = USA_recognition_yes_UNSC
	}
	text = {
		trigger = {
			USA = {
				NOT = { has_country_flag = USA_recognition_campaign_UNSC }
			}
		}
		localization_key = USA_recognition_no_UNSC
	}
}
defined_text = {
	name = recognises_UNSC_seat_SOV

	text = {
		trigger = {
			SOV = {
				has_country_flag = SOV_recognition_campaign_UNSC
			}
		}
		localization_key = SOV_recognition_yes_UNSC
	}
	text = {
		trigger = {
			SOV = {
				NOT = { has_country_flag = SOV_recognition_campaign_UNSC }
			}
		}
		localization_key = SOV_recognition_no_UNSC
	}
}
defined_text = {
	name = recognises_UNSC_seat_ENG

	text = {
		trigger = {
			ENG = {
				has_country_flag = ENG_recognition_campaign_UNSC
			}
		}
		localization_key = ENG_recognition_yes_UNSC
	}
	text = {
		trigger = {
			ENG = {
				NOT = { has_country_flag = ENG_recognition_campaign_UNSC }
			}
		}
		localization_key = ENG_recognition_no_UNSC
	}
}
defined_text = {
	name = recognises_UNSC_seat_FRA

	text = {
		trigger = {
			FRA = {
				has_country_flag = FRA_recognition_campaign_UNSC
			}
		}
		localization_key = FRA_recognition_yes_UNSC
	}
	text = {
		trigger = {
			ENG = {
				NOT = { has_country_flag = FRA_recognition_campaign_UNSC }
			}
		}
		localization_key = FRA_recognition_no_UNSC
	}
}
defined_text = {
	name = recognises_UNSC_seat_CHI

	text = {
		trigger = {
			CHI = {
				has_country_flag = CHI_recognition_campaign_UNSC
			}
		}
		localization_key = CHI_recognition_yes_UNSC
	}
	text = {
		trigger = {
			CHI = {
				NOT = { has_country_flag = CHI_recognition_campaign_UNSC }
			}
		}
		localization_key = CHI_recognition_no_UNSC
	}
}