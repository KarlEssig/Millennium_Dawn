POL_korwin_walesa_bop = {
	initial_value = 0.25
	left_side = POL_korwin_walesa_bop_left_side
	right_side = POL_korwin_walesa_bop_right_side
	decision_category = POL_korwin_walesa_bop_category
	range = {
		id = POL_korwin_walesa_bop_mid_range
		min = -0.1
		max = 0.1
		modifier = {
			stability_factor = 0.1
		}
	}

	side = {
		id = POL_korwin_walesa_bop_left_side
		icon = GFX_bop_POL_walesa
		range = {
			id = POL_korwin_walesa_bop_left_win_range
			min = -1
			max = -0.9
			modifier = {
				drift_defence_factor = -0.2
				stability_factor = 0.1
			}
			on_activate = {
				if = {
					limit = { has_idea = POL_walesa_opposition_idea }
					remove_ideas = POL_walesa_opposition_idea
				}
				else_if = {
					limit = { has_idea = POL_walesa_opposition2_idea }
					remove_ideas = POL_walesa_opposition2_idea
				}
				else = {
					remove_ideas = POL_walesa_opposition3_idea
				}

				remove_power_balance = {
					id = POL_korwin_walesa_bop
				}
				if = {
					limit = {
						nationalist_monarchists_are_in_power = yes
					}
					set_temp_variable = { rul_party_temp = 14 }
					change_ruling_party_effect = yes
					hidden_effect = {
						set_politics = {
							ruling_party = neutrality
							elections_allowed = yes
						}
					}
				}
				uncomplete_national_focus = {
					focus = POL_korwin_is_in_power
					uncomplete_children = yes
					refund_political_power = no
				}
				add_timed_idea = { idea = POL_unstable_government_idea days = 365 }
				set_temp_variable = { party_index = 14 }
				set_temp_variable = { party_popularity_increase = 0.6 }
				set_temp_variable = { temp_outlook_increase = 0.65 }
				add_relative_party_popularity = yes
				hidden_effect = {
					set_country_flag = POL_walesa_wins
					news_event = { id = poland_news.12 }
				}
			}
		}

		range = {
			id = POL_korwin_walesa_bop_left_advanced_range
			min = -0.9
			max = -0.6
			modifier = {
				drift_defence_factor = -0.15
				stability_factor = 0.05
			}
			on_activate = {
				country_event = poland.24
			}
		}

		range = {
			id = POL_korwin_walesa_bop_left_medium_range
			min = -0.6
			max = -0.4
			modifier = {
				drift_defence_factor = -0.1
			}
		}

		range = {
			id = POL_korwin_walesa_bop_left_low_range
			min = -0.4
			max = -0.1
			modifier = {
				drift_defence_factor = -0.05
			}
		}
	}

	side = {
		id = POL_korwin_walesa_bop_right_side
		icon = GFX_bop_POL_korwin
		range = {
			id = POL_korwin_walesa_bop_right_win_range
			min = 0.9
			max = 1
			modifier = {
				drift_defence_factor = 0.15
				stability_factor = 0.15
				political_power_factor = 0.05
			}
			on_activate = {
				remove_power_balance = {
					id = POL_korwin_walesa_bop
				}
				set_power_balance = {
					id = POL_korwin_win_bop
					left_side = POL_korwin_win_bop_left_side
					right_side = POL_korwin_win_bop_right_side
					set_value = 0.5
				}
				if = {
					limit = { has_idea = POL_walesa_opposition2_idea }
					swap_ideas = {
						remove_idea = POL_walesa_opposition2_idea
						add_idea = POL_walesa_opposition_idea
					}
				}
				else_if = {
					limit = { has_idea = POL_walesa_opposition3_idea }
					swap_ideas = {
						remove_idea = POL_walesa_opposition3_idea
						add_idea = POL_walesa_opposition_idea
					}
				}
				if = {
					limit = {
						neutrality_neutral_conservatism_are_in_power = yes
					}
					set_temp_variable = { rul_party_temp = 23 }
					change_ruling_party_effect = yes
					hidden_effect = {
						set_politics = {
							ruling_party = nationalist
							elections_allowed = yes
						}
					}
				}
				hidden_effect = {
					set_country_flag = POL_korwin_wins
					news_event = { id = poland_news.13 }
				}
			}
		}

		range = {
			id = POL_korwin_walesa_bop_right_advanced_range
			min = 0.6
			max = 0.9
			modifier = {
				drift_defence_factor = 0.1
				stability_factor = 0.1
				political_power_factor = 0.05
			}
		}

		range = {
			id = POL_korwin_walesa_bop_right_medium_range
			min = 0.4
			max = 0.6
			modifier = {
				drift_defence_factor = 0.05
				stability_factor = 0.05
				political_power_factor = 0.05
			}
		}

		range = {
			id = POL_korwin_walesa_bop_right_low_range
			min = 0.1
			max = 0.4
			modifier = {
				political_power_factor = 0.05
			}
		}
	}
}

POL_walesa_korwin_bop = {
	initial_value = 0.25
	left_side = POL_walesa_korwin_bop_left_side
	right_side = POL_walesa_korwin_bop_right_side
	decision_category = POL_walesa_korwin_bop_category
	range = {
		id = POL_walesa_korwin_bop_mid_range
		min = -0.1
		max = 0.1
		modifier = {
			stability_factor = 0.1
		}
	}

	side = {
		id = POL_walesa_korwin_bop_left_side
		icon = GFX_bop_POL_korwin
		range = {
			id = POL_walesa_korwin_bop_left_win_range
			min = -1
			max = -0.9
			modifier = {
				drift_defence_factor = -0.2
				stability_factor = 0.1
			}
			on_activate = {
				if = {
					limit = { has_idea = POL_korwin_opposition_idea }
					remove_ideas = POL_korwin_opposition_idea
				}
				else_if = {
					limit = { has_idea = POL_korwin_opposition2_idea }
					remove_ideas = POL_korwin_opposition2_idea
				}
				else = {
					remove_ideas = POL_korwin_opposition3_idea
				}

				remove_power_balance = {
					id = POL_walesa_korwin_bop
				}
				if = {
					limit = {
						neutrality_neutral_conservatism_are_in_power = yes
					}
					set_temp_variable = { rul_party_temp = 23 }
					change_ruling_party_effect = yes
					hidden_effect = {
						set_politics = {
							ruling_party = nationalist
							elections_allowed = yes
						}
					}
				}
				set_temp_variable = { party_index = 23 }
				set_temp_variable = { party_popularity_increase = 0.6 }
				set_temp_variable = { temp_outlook_increase = 0.65 }
				add_relative_party_popularity = yes
				set_power_balance = {
					id = POL_korwin_win_bop
					left_side = POL_korwin_win_bop_left_side
					right_side = POL_korwin_win_bop_right_side
					set_value = 0.5
				}
				add_ideas = POL_walesa_opposition_idea
				uncomplete_national_focus = {
					focus = POL_walesa_is_in_power
					uncomplete_children = yes
					refund_political_power = no
				}
				add_timed_idea = { idea = POL_unstable_government_idea days = 365 }
				hidden_effect = {
					set_country_flag = POL_korwin_wins
					news_event = { id = poland_news.30 }
				}
			}
		}

		range = {
			id = POL_korwin_walesa_bop_left_advanced_range
			min = -0.9
			max = -0.6
			modifier = {
				drift_defence_factor = -0.15
				stability_factor = 0.05
			}
			on_activate = {
				country_event = poland.104
			}
		}

		range = {
			id = POL_walesa_korwin_bop_left_medium_range
			min = -0.6
			max = -0.4
			modifier = {
				drift_defence_factor = -0.1
			}
		}

		range = {
			id = POL_walesa_korwin_bop_left_low_range
			min = -0.4
			max = -0.1
			modifier = {
				drift_defence_factor = -0.05
			}
		}
	}

	side = {
		id = POL_walesa_korwin_bop_right_side
		icon = GFX_bop_POL_walesa
		range = {
			id = POL_walesa_korwin_bop_right_win_range
			min = 0.9
			max = 1
			modifier = {
				drift_defence_factor = 0.15
				stability_factor = 0.15
				political_power_factor = 0.05
			}
			on_activate = {
				remove_power_balance = {
					id = POL_walesa_korwin_bop
				}
				if = {
					limit = { has_idea = POL_korwin_opposition2_idea }
					remove_ideas = POL_korwin_opposition2_idea
				}
				else_if = {
					limit = { has_idea = POL_korwin_opposition3_idea }
					remove_ideas = POL_korwin_opposition3_idea
				}
				else_if = {
					limit = { has_idea = POL_korwin_opposition_idea }
					remove_ideas = POL_korwin_opposition_idea
				}
				if = {
					limit = {
						nationalist_monarchists_are_in_power = yes
					}
					set_temp_variable = { rul_party_temp = 14 }
					change_ruling_party_effect = yes
					hidden_effect = {
						set_politics = {
							ruling_party = neutrality
							elections_allowed = yes
						}
					}
				}
				hidden_effect = {
					set_country_flag = POL_walesa_wins
					news_event = { id = poland_news.31 }
				}
			}
		}

		range = {
			id = POL_walesa_korwin_bop_right_advanced_range
			min = 0.6
			max = 0.9
			modifier = {
				drift_defence_factor = 0.1
				stability_factor = 0.1
				political_power_factor = 0.05
			}
		}

		range = {
			id = POL_walesa_korwin_bop_right_medium_range
			min = 0.4
			max = 0.6
			modifier = {
				drift_defence_factor = 0.05
				stability_factor = 0.05
				political_power_factor = 0.05
			}
		}

		range = {
			id = POL_walesa_korwin_bop_right_low_range
			min = 0.1
			max = 0.4
			modifier = {
				political_power_factor = 0.05
			}
		}
	}
}
POL_korwin_win_bop = {
	initial_value = 0.25
	left_side = POL_korwin_win_bop_left_side
	right_side = POL_korwin_win_bop_right_side
	decision_category = POL_korwin_win_bop_category
	range = {
		id = POL_korwin_win_bop_mid_range
		min = -0.1
		max = 0.1
		modifier = {
			stability_factor = 0.1
		}
	}

	side = {
		id = POL_korwin_win_bop_left_side
		icon = GFX_bop_POL_walesa
		range = {
			id = POL_korwin_win_bop_left_win_range
			min = -1
			max = -0.9
			modifier = {
				drift_defence_factor = -0.2
				stability_factor = 0.1
			}
			on_activate = {
				if = {
					limit = { has_idea = POL_walesa_opposition_idea }
					remove_ideas = POL_walesa_opposition_idea
				}
				else_if = {
					limit = { has_idea = POL_walesa_opposition2_idea }
					remove_ideas = POL_walesa_opposition2_idea
				}
				else = {
					remove_ideas = POL_walesa_opposition3_idea
				}

				remove_power_balance = {
					id = POL_korwin_win_bop
				}
				if = {
					limit = {
						nationalist_monarchists_are_in_power = yes
					}
					set_temp_variable = { rul_party_temp = 14 }
					change_ruling_party_effect = yes
					hidden_effect = {
						set_politics = {
							ruling_party = neutrality
							elections_allowed = yes
						}
					}
				}
				add_timed_idea = { idea = POL_unstable_government_idea days = 1825 }
				#set_power_balance = {
				#	id = POL_walesa_win_bop
				#	left_side = POL_korwin_walesa_bop_left_side
				#	right_side = POL_korwin_walesa_bop_right_side
				#	set_value = -0.25
				#}
				set_temp_variable = { party_index = 14 }
				set_temp_variable = { party_popularity_increase = 0.6 }
				set_temp_variable = { temp_outlook_increase = 0.65 }
				add_relative_party_popularity = yes
				uncomplete_national_focus = {
					focus = POL_korwin_is_in_power
					uncomplete_children = yes
					refund_political_power = no
				}
				hidden_effect = {
					set_country_flag = POL_walesa_wins
					news_event = { id = poland_news.12 }
					clr_country_flag = POL_korwin_wins
				}
			}
		}

		range = {
			id = POL_korwin_win_bop_left_advanced_range
			min = -0.9
			max = -0.6
			modifier = {
				drift_defence_factor = -0.15
				stability_factor = 0.05
			}
			on_activate = {
				country_event = poland.24
			}
		}

		range = {
			id = POL_korwin_win_bop_left_medium_range
			min = -0.6
			max = -0.4
			modifier = {
				drift_defence_factor = -0.1
			}
		}

		range = {
			id = POL_korwin_win_bop_left_low_range
			min = -0.4
			max = -0.1
			modifier = {
				drift_defence_factor = -0.05
			}
		}
	}

	side = {
		id = POL_korwin_win_bop_right_side
		icon = GFX_bop_POL_korwin_won
		range = {
			id = POL_korwin_win_bop_right_win_range
			min = 0.9
			max = 1
			modifier = {
				drift_defence_factor = 0.2
				stability_factor = 0.15
				political_power_factor = 0.07
			}
		}

		range = {
			id = POL_korwin_win_bop_right_advanced_range
			min = 0.6
			max = 0.9
			modifier = {
				drift_defence_factor = 0.15
				stability_factor = 0.12
				political_power_factor = 0.06
			}
		}

		range = {
			id = POL_korwin_win_bop_right_medium_range
			min = 0.4
			max = 0.6
			modifier = {
				drift_defence_factor = 0.07
				stability_factor = 0.07
				political_power_factor = 0.06
			}
		}

		range = {
			id = POL_korwin_win_bop_right_low_range
			min = 0.1
			max = 0.4
			modifier = {
				political_power_factor = 0.05
			}
		}
	}
}

#POL_walesa_win_bop = {
#	initial_value = 0.25
#	left_side = POL_korwin_walesa_bop_left_side
#	right_side = POL_korwin_walesa_bop_right_side
#	decision_category = POL_korwin_walesa_bop_category
#	range = {
#		id = POL_korwin_walesa_bop_mid_range
#		min = -0.1
#		max = 0.1
#		modifier = {
#			stability_factor = 0.1
#		}
#		on_activate = {
#
#		}
#		on_deactivate = {
#
#		}
#	}
#
#	side = {
#		id = POL_korwin_walesa_bop_left_side
#		icon = GFX_bop_ITA_grand_council_side
#		range = {
#			id = POL_korwin_walesa_bop_left_win_range
#			min = -1
#			max = -0.9
#			modifier = {
#				drift_defence_factor = 0.10
#				stability_factor = 0.2
#				political_power_factor = 0.07
#			}
#			on_activate = {
#
#			}
#			on_deactivate = {
#
#			}
#		}
#
#		range = {
#			id = POL_korwin_walesa_bop_left_advanced_range
#			min = -0.9
#			max = -0.6
#			modifier = {
#				drift_defence_factor = 0.05
#				stability_factor = 0.1
#				political_power_factor = 0.03
#			}
#			on_activate = {
#
#			}
#			on_deactivate = {
#
#			}
#		}
#
#		range = {
#			id = POL_korwin_walesa_bop_left_medium_range
#			min = -0.6
#			max = -0.4
#			modifier = {
#				stability_factor = 0.5
#				political_power_factor = 0.03
#			}
#			on_activate = {
#
#			}
#			on_deactivate = {
#
#			}
#		}
#
#		range = {
#			id = POL_korwin_walesa_bop_left_low_range
#			min = -0.4
#			max = -0.1
#			modifier = {
#				drift_defence_factor = -0.05
#				stability_factor = 0.3
#				political_power_factor = 0.02
#			}
#			on_activate = {
#
#			}
#			on_deactivate = {
#
#			}
#		}
#	}
#
#	side = {
#		id = POL_korwin_walesa_bop_right_side
#		icon = GFX_bop_ITA_balbo_improved_side
#		range = {
#			id = POL_korwin_walesa_bop_right_win_range
#			min = 0.9
#			max = 1
#			modifier = {
#				drift_defence_factor = -0.4
#				stability_factor = -0.15
#				political_power_factor = -0.15
#			}
#			on_activate = {
#				remove_power_balance = {
#					id = POL_walesa_win_bop
#				}
#				set_power_balance = {
#					id = POL_korwin_win_bop
#					left_side = POL_korwin_walesa_bop_left_side
#					right_side = POL_korwin_walesa_bop_right_side
#					set_value = 0.1
#				}
#				add_ideas = POL_walesa_opposition_idea
#				hidden_effect = {
#					clr_country_flag = POL_walesa_wins
#					set_country_flag = POL_korwin_wins
#					news_event = { id = poland_news.13 }
#				}
#			}
#			on_deactivate = {
#
#			}
#		}
#
#		range = {
#			id = POL_korwin_walesa_bop_right_advanced_range
#			min = 0.6
#			max = 0.9
#			modifier = {
#				drift_defence_factor = -0.3
#				stability_factor = -0.12
#				political_power_factor = -0.12
#			}
#			on_activate = {
#
#			}
#			on_deactivate = {
#
#			}
#		}
#
#		range = {
#			id = POL_korwin_walesa_bop_right_medium_range
#			min = 0.4
#			max = 0.6
#			modifier = {
#				drift_defence_factor = -0.2
#				stability_factor = -0.1
#				political_power_factor = -0.1
#			}
#			on_activate = {
#
#			}
#			on_deactivate = {
#
#			}
#		}
#
#		range = {
#			id = POL_korwin_walesa_bop_right_low_range
#			min = 0.1
#			max = 0.4
#			modifier = {
#				drift_defence_factor = -0.1
#				stability_factor = -0.05
#				political_power_factor = -0.05
#			}
#			on_activate = {
#
#			}
#			on_deactivate = {
#
#			}
#		}
#	}
#}