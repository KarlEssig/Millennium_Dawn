#Made by Lord Bogdanoff
focus_tree = {
	id = vitebsk_focus
	country = {
		factor = 0
		modifier = {
			add = 20
			original_tag = VTB
		}
	}
	continuous_focus_position = { x = 3200 y = 2100 }

	#Start
	focus = {
		id = VBT_Start
		icon = vtb_start

		x = 25
		y = 0

		ai_will_do = { factor = 20 }

		cost = 1.86

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_Start"
			hidden_effect = {
				add_manpower = 2000
			}
			add_political_power = 150
			division_template = {
				name = "Militia Brigade"
				is_locked = yes
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }

				}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 1094 }
				create_unit = {
					division = "name = \"Militia Brigade\" division_template = \"Militia Brigade\" start_experience_factor = 0"
					owner = VTB
				}
			}
			division_template = {
				name = "Militia Brigade "
				is_locked = yes
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }

				}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 1094 }
				create_unit = {
					division = "name = \"Militia Brigade\" division_template = \"Militia Brigade\" start_experience_factor = 0"
					owner = VTB
				}
			}
			division_template = {
				name = "Militia Brigade "
				is_locked = yes
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }

				}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 1094 }
				create_unit = {
					division = "name = \"Militia Brigade\" division_template = \"Militia Brigade\" start_experience_factor = 0"
					owner = VTB
				}
			}
		}
	}
	#Economic help
	focus = {
		id = VBT_Economic_help
		icon = russian_investments

		x = 24
		y = 1

		ai_will_do = { factor = 20 }

		prerequisite = { focus = VBT_Start }

		cost = 2

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_Economic_help"
			SOV = {
				country_event = { id = vitebsk_rus.3 days = 1 }
			}
		}
	}
	#Ruble from Russia
	focus = {
		id = VBT_Ruble
		icon = russian_ruble

		x = 23
		y = 3

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_Economic_help }

		cost = 4

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_Ruble"
			add_stability = 0.8
			remove_ideas = BLR_unstable_national_currency
			set_temp_variable = { percent_change = 7.12 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = VTB }
			change_influence_percentage = yes
		}
	}

	#Weapon help
	focus = {
		id = VBT_weapon_help
		icon = ak12

		x = 26
		y = 1

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_Start }

		cost = 2

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_weapon_help"
			SOV = {
				country_event = { id = vitebsk_rus.2 days = 1 }
			}
		}
	}
	#Create a government of the Republic
	focus = {
		id = VBT_government
		icon = government_beu

		x = 27
		y = 2

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_weapon_help }

		cost = 4

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_government"
			add_stability = 0.2
			add_political_power = 100
		}
	}
	#Passports
	focus = {
		id = VBT_passports
		icon = ITA_Focus_Passport

		x = 27
		y = 3

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_government }

		cost = 4

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_passports"
			add_stability = 0.1
			VTB = { add_state_core = 1094 }
		}
	}
	#Let in volunteers from Russia
	focus = {
		id = VBT_volunteers_help
		icon = antirussiaicon

		x = 25
		y = 3

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_Economic_help }
		prerequisite = { focus = VBT_weapon_help }

		cost = 4

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_volunteers_help"
			division_template = {
				name = "Battalion of volunteers"
				is_locked = yes
				regiments = {
					Special_Forces = { x = 0 y = 0 }

				}
				priority = 2
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 1094 }
				create_unit = {
					division = "name = \"Battalion of volunteers\" division_template = \"Battalion of volunteers\" start_experience_factor = 1.0"
					owner = VTB
				}
			}
		}
	}
	#Mobilisation
	focus = {
		id = VBT_mobilisation
		icon = russian_defence_strategy

		x = 26
		y = 4

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_passports }
		cost = 4

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_mobilisation"
			add_war_support = -0.05
			add_stability = -0.05
			add_manpower = 2500
		}
	}
	#Weapon help again
	focus = {
		id = VBT_weapon_help2
		icon = ak103

		x = 24
		y = 4

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_volunteers_help }
		prerequisite = { focus = VBT_Ruble }
		prerequisite = { focus = VBT_passports }
		cost = 4

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_weapon_help2"
			SOV = {
				country_event = { id = vitebsk_rus.4 days = 1 }
			}
		}
	}
	#Hold a referendum on joining Russia
	focus = {
		id = VBT_referendum
		icon = vitebsk_project

		x = 25
		y = 5

		ai_will_do = { factor = 20	}

		cost = 5

		prerequisite = {  focus = VBT_mobilisation }
		prerequisite = {  focus = VBT_weapon_help2 }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_referendum"
			SOV = {
				country_event = { id = vitebsk_rus.6 days = 1 }
			}
		}
	}
	#Nationalization of the electrotechnical enterprise Svet
	focus = {
		id = VBT_svet
		icon = consumer_goods

		x = 24
		y = 6


		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_referendum }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_svet"
			one_random_industrial_complex = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.01 }
			add_relative_party_popularity = yes
		}
	}
	#Restore roads
	focus = {
		id = VBT_roads
		icon = infrastructure1

		x = 26
		y = 6


		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_referendum }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_roads"
			one_random_infrastructure = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.04 }
			add_relative_party_popularity = yes
		}
	}
	#Strengthen government
	focus = {
		id = VBT_strengthen_government
		icon = propaganda

		x = 25
		y = 7

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_svet }
		prerequisite = { focus = VBT_roads }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_centralization"
			add_ideas = political_power_bonus
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.02 }
			add_relative_party_popularity = yes
		}
	}
	#Creation of the army of the Vitebsk Republic
	focus = {
		id = VBT_army_republic
		icon = infantry_allies

		x = 23
		y = 8

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_strengthen_government }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_army_republic"
			army_experience = 25
			add_command_power = 10
			increase_military_spending = yes
			add_ideas = VTB_profarmy
		}
	}
	#Special Forces of the Republic
	focus = {
		id = VBT_specnaz_republic
		icon = soldier2

		x = 22
		y = 9

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_army_republic }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_specnaz_republic"
			add_tech_bonus = {
				name = CAT_inf_wep
				bonus = 0.30
				category = CAT_inf_wep
			}
		}
	}
	#National Defense Strategy
	focus = {
		id = VBT_defence_republic
		icon = city_fort

		x = 24
		y = 9

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_army_republic }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_defence_republic"
			add_ideas = VTB_National_Defense_Strategy
		}
	}
	#Education
	focus = {
		id = VBT_education
		icon = research

		x = 27
		y = 8

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_strengthen_government }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_education"
			generic_research_buff = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.03 }
			add_relative_party_popularity = yes
		}
	}
	#Medicine
	focus = {
		id = VBT_medicine
		icon = medicine

		x = 26
		y = 9

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_education }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_medicine"
			increase_healthcare_budget = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.05 }
			add_relative_party_popularity = yes
		}
	}
	#Social
	focus = {
		id = VBT_social
		icon = free_higher_education

		x = 28
		y = 9

		ai_will_do = { factor = 20	}

		prerequisite = { focus = VBT_education }

		cost = 7

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VBT_social"
			increase_social_spending = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.05 }
			add_relative_party_popularity = yes
		}
	}

}