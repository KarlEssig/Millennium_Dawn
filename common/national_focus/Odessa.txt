focus_tree = {
	id = odessa_focus
	country = {
		factor = 0
		modifier = {
			add = 20
			original_tag = OPR
		}
	}
	continuous_focus_position = { x = 1700 y = 1400 }
	
	focus = {
		id = OPR_Start_junta
		icon = revive_the_black_sea_fleet

		x = 10
		y = 0
		cost = 0.2

		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OPR = { is_subject_of = PMR }
			nationalist_military_junta_are_in_power = yes
		}
	  
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_Start_junta"
			hidden_effect = {
				division_template = {
					name = "Bryhada Morskoyi Pikhoty"
					division_names_group = UKR_MAR_01
				
					regiments = {
						Arm_Marine_Bat = { x = 0 y = 2 }
						Mech_Marine_Bat = { x = 0 y = 0 }
						Mech_Marine_Bat = { x = 0 y = 1 }
						Mot_Marine_Bat = { x = 0 y = 3 }
						Mot_Marine_Bat = { x = 0 y = 4 }
					}
					support = {
						H_Engi_Comp = { x = 0 y = 0 }
					}
				}
		   }
		   695 = {
			   create_unit = {
				   division = "name = \"1st Odessa Brigade\" division_template = \"Bryhada Morskoyi Pikhoty\" start_experience_factor = 0.1"
				   prioritize_location = 11670
				   owner = OPR
			   }
		   }
	   }
	}

	focus = {
		id = OPR_odessa_defences
		icon = army_defence1

		x = -2
		y = 1
		cost = 1

		relative_position_id = OPR_Start_junta

		prerequisite = { focus = OPR_Start_junta }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OPR = { is_subject_of = PMR }
			nationalist_military_junta_are_in_power = yes
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_odessa_defence"
			add_stability = 0.1
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
			695 = {
				add_building_construction = { type = bunker province = 11670 level = 2 }
			}
		}
		ai_will_do = { factor = 175 }
	}

	focus = {
		id = OPR_trans_suplly
		icon = pmr_nation

		x = 2
		y = 1
		cost = 1

		relative_position_id = OPR_Start_junta

		prerequisite = { focus = OPR_Start_junta }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OPR = { is_subject_of = PMR }
			nationalist_military_junta_are_in_power = yes
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_trans_suplly"
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = PMR }
			set_temp_variable = { influence_target = OPR }
			change_influence_percentage = yes
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = 2000
				producer = PMR
			}
		}
		ai_will_do = { factor = 75 }
	}

	focus = {
		id = OPR_more_marines
		icon = ukrainian_armed_forces_reform

		x = 0
		y = 1
		cost = 1

		relative_position_id = OPR_Start_junta

		prerequisite = { focus = OPR_Start_junta }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OPR = { is_subject_of = PMR }
			nationalist_military_junta_are_in_power = yes
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_trans_suplly"
			695 = {
				create_unit = {
					division = "name = \"2nd Odessa Brigade\" division_template = \"Bryhada Morskoyi Pikhoty\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 75 }
	}

	focus = {
		id = OPR_teroboronas
		icon = ukr_tro

		x = -1
		y = 2
		cost = 2

		relative_position_id = OPR_Start_junta

		prerequisite = { focus = OPR_trans_suplly }
		prerequisite = { focus = OPR_more_marines }
		prerequisite = { focus = OPR_odessa_defences }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		
		available = {
			OPR = { is_subject_of = PMR }
			nationalist_military_junta_are_in_power = yes
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_teroborona"
			hidden_effect = {
				division_template = {
					name = "Territorial Defense Brigade"
					regiments = {
						L_Inf_Bat = { x = 0 y = 0 }
						L_Inf_Bat = { x = 0 y = 1 }
						Mot_Inf_Bat = { x = 1 y = 0 }
					}
				}
			}
			695 = {
				create_unit = {
					division = "name = \"Territorial Defense Brigade\" division_template = \"Territorial Defense Brigade\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 155 }
	}

	focus = {
		id = OPR_weapon_supllys
		icon = army_more_ak47

		x = 1
		y = 2
		relative_position_id = OPR_Start_junta

		cost = 1

		prerequisite = { focus = OPR_trans_suplly }
		prerequisite = { focus = OPR_more_marines }
		prerequisite = { focus = OPR_odessa_defences }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OPR = { is_subject_of = PMR }
			nationalist_military_junta_are_in_power = yes
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_weapon_suplly"
			add_equipment_to_stockpile = {
				type = infantry_weapons2
				amount = 1500
				producer = UKR
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment2
				amount = 250
				producer = UKR
			}
			add_equipment_to_stockpile = {
				type = AA_Equipment
				amount = 100
				producer = UKR
			}
			add_equipment_to_stockpile = {
				type = Anti_Air_0
				amount = 15
				producer = UKR
			}
		}
		ai_will_do = { factor = 75 }
	}

	focus = {
		id = OPR_national_guard
		icon = ukr_national_guard

		x = 0
		y = 3
		relative_position_id = OPR_Start_junta

		cost = 1

		prerequisite = { focus = OPR_weapon_supllys }
		prerequisite = { focus = OPR_teroboronas }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OPR = { is_subject_of = PMR }
			nationalist_military_junta_are_in_power = yes
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_national_guard"
			hidden_effect = {
				division_template = {
					name = "Natsionalna Hvardiya"
					regiments = {
						Militia_Bat = { x = 0 y = 0 }
						Militia_Bat = { x = 0 y = 1 }
						Militia_Bat = { x = 0 y = 2 }
						Militia_Bat = { x = 0 y = 3 }
					}
					support = {
						L_Engi_Comp = { x = 0 y = 0 }
						L_Recce_Comp = { x = 0 y = 0 }
					}
				}
			}
			695 = {
				create_unit = {
					division = "name = \"1sd Separate Brigade of National Guard\" division_template = \"Bryhada Morskoyi Pikhoty\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 75 }
	}

	focus = {
		id = OPR_army_norm
		icon = UKR_Ukrainian_Officers

		x = 0
		y = 4
		relative_position_id = OPR_Start_junta

		cost = 1

		prerequisite = { focus = OPR_weapon_supllys }
		prerequisite = { focus = OPR_teroboronas }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OPR = { is_subject_of = PMR }
			nationalist_military_junta_are_in_power = yes
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_army_norm"
			remove_ideas = OPR_unproffesional_army
		}
		ai_will_do = { factor = 75 }
	}

 	focus = {
	 	id = OPR_Start_rebellion
	 	icon = opr_start

	 	x = 20
	 	y = 0
	 	
	 	cost = 0.2

		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
	 	
 		completion_reward = {
		 	log = "[GetDateText]: [Root.GetName]: Focus OPR_Start_rebellion"
			 hidden_effect = {
				if = {
					limit = { ROOT = { is_ai = yes } }
					country_event = { id = dpr.6 days = 1 }
				}
				division_template = {
					name = "Pekhotnaya Brigada"
					regiments = {
						Mot_Inf_Bat = { x = 0 y = 0 }
						Mot_Inf_Bat = { x = 0 y = 1 }
						Mot_Inf_Bat = { x = 0 y = 2 }
					}
					support = {
						Arty_Battery = { x = 0 y = 0 }
						L_Engi_Comp = { x = 0 y = 1 }
					}
				}
			}
			695 = {
				create_unit = {
					division = "name = \"People's Militia Battalion\" division_template = \"Pekhotnaya Brigada\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
				create_unit = {
					division = "name = \"People's Militia Battalion\" division_template = \"Pekhotnaya Brigada\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 155 }
 	}
	focus = {
		id = OPR_people_militia
		icon = opr_people_militia

		x = 0
		y = 1
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_Start_rebellion }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_people_militia"
			swap_ideas = {
				remove_idea = OPR_unproffesional_army
				add_idea = OPR_proffesional_army
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_weapon_suplly
		icon = army_more_ak47

		x = -2
		y = 1
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_Start_rebellion }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_weapon_suplly"
			add_equipment_to_stockpile = {
				type = infantry_weapons2
				amount = 1500
				producer = UKR
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment2
				amount = 250
				producer = UKR
			}
			add_equipment_to_stockpile = {
				type = AA_Equipment
				amount = 100
				producer = UKR
			}
			add_equipment_to_stockpile = {
				type = Anti_Air_0
				amount = 15
				producer = UKR
			}
		}
		ai_will_do = { factor = 75 }
	}
	focus = {
		id = OPR_odessa_defence
		icon = army_defence1

		x = -3
		y = 2
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_weapon_suplly }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_odessa_defence"
			add_stability = 0.1
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
			695 = {
				add_building_construction = { type = bunker province = 11670 level = 2 }
			}
		}
		ai_will_do = { factor = 175 }
	}
	focus = {
		id = OPR_mobilisation
		icon = army_mobilisation

		x = 3
		y = 2
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_russian_suplly }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_mobilisation"
			if = {
				limit = { has_idea = partial_draft_army }
				swap_ideas = {
					remove_idea = partial_draft_army
					add_idea = draft_army
				}
			}
			else_if = {
				limit = { has_idea = volunteer_army }
				swap_ideas = {
					remove_idea = volunteer_army
					add_idea = draft_army
				}
			}
			else_if = {
				limit = { has_idea = no_military }
				swap_ideas = {
					remove_idea = no_military
					add_idea = draft_army
				}
			}
			add_manpower = 2000
		}
		ai_will_do = { factor = 175 }
	}
	focus = {
		id = OPR_russian_suplly
		icon = blr_russian_weapon_deal

		x = 2
		y = 1
		relative_position_id = OPR_Start_rebellion

		cost = 1

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		prerequisite = { focus = OPR_Start_rebellion }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_russian_suplly"
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = OPR }
			change_influence_percentage = yes
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = 2000
				producer = SOV
			}
			add_equipment_to_stockpile = {
				type = util_vehicle_equipment
				amount = 300
				producer = SOV
			}
			add_equipment_to_stockpile = {
				type = SP_Anti_Air_1
				amount = 20
				producer = SOV
			}
			add_equipment_to_stockpile = {
				type = transport_helicopter1
				amount = 7
				producer = SOV
			}
			if = {
				limit = { has_dlc = "No Step Back" }
				add_equipment_to_stockpile = {
					type = mbt_hull_1
					amount = 20
					producer = SOV
				}
				add_equipment_to_stockpile = {
					type = ifv_hull_1
					amount = 50
					producer = SOV
				}
				add_equipment_to_stockpile = {
					type = apc_hull_1
					amount = 40
					producer = SOV
				}
			}
			else = {
				add_equipment_to_stockpile = {
					type = MBT_3
					amount = 20
					producer = SOV
				}
				add_equipment_to_stockpile = {
					type = IFV_3
					amount = 50
					producer = SOV
				}
				add_equipment_to_stockpile = {
					type = APC_3
					amount = 40
					producer = SOV
				}
			}
		}
		ai_will_do = { factor = 75 }
	}
	focus = {
		id = OPR_government
		icon = dominant_party

		x = 0
		y = 2
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_russian_suplly }
		prerequisite = { focus = OPR_weapon_suplly }
		prerequisite = { focus = OPR_people_militia }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_government"
			remove_ideas = OPR_no_government_idea
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_bank
		icon = economic_banks

		x = -5
		y = 3
		relative_position_id = OPR_Start_rebellion

		cost = 7

		prerequisite = { focus = OPR_government }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_bank"
			one_office_construction = yes
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = OPR_bivalute
		icon = hpr_grivna_ban

		x = -3
		y = 3
		relative_position_id = OPR_Start_rebellion

		cost = 5

		prerequisite = { focus = OPR_government }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_bivalute"
			remove_ideas = OPR_bivaluta
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = OPR_passport
		icon = ITA_Focus_Passport

		x = -4
		y = 4
		relative_position_id = OPR_Start_rebellion

		cost = 5

		prerequisite = { focus = OPR_bivalute }
		prerequisite = { focus = OPR_bank }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_passport"
			add_stability = 0.07
			add_political_power = 150
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_operator
		icon = economic_internet

		x = -4
		y = 5
		relative_position_id = OPR_Start_rebellion

		cost = 5

		prerequisite = { focus = OPR_passport }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
			
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_operator"
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			random_owned_controlled_state = {
				add_building_construction = {
					type = internet_station
					level = 1
					instant_build = yes
				}
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_mgb
		icon = blr_police_training

		x = 3
		y = 3
		relative_position_id = OPR_Start_rebellion

		cost = 5

		prerequisite = { focus = OPR_government }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
			
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_mgb"
			add_ideas = OPR_mgb
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = OPR_interbrigabe
		icon = sov_nazbol_military

		x = 5
		y = 3
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_government }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_interbrigabe"
			hidden_effect = {
				division_template = {
					name = "Batalyon Dobrovolchev"
					regiments = {
						L_Inf_Bat = { x = 0 y = 0 }
						L_Inf_Bat = { x = 0 y = 1 }
						L_Inf_Bat = { x = 0 y = 2 }
						L_Inf_Bat = { x = 0 y = 3 }
					}
					priority = 0
				}
			}
			695 = {
				create_unit = {
					division = "name = \"Interbrigade Odessa\" division_template = \"Batalyon Dobrovolchev\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_desorg
		icon = opr_people_militia_up

		x = 4
		y = 4
		relative_position_id = OPR_Start_rebellion

		cost = 5

		prerequisite = { focus = OPR_interbrigabe }
		prerequisite = { focus = OPR_mgb }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		
		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_desorg"
			swap_ideas = {
				remove_idea = OPR_proffesional_army
				add_idea = OPR_proffesional_army2
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_defence
		icon = border_conflict_bunker

		x = 4
		y = 5
		relative_position_id = OPR_Start_rebellion

		cost = 3

		prerequisite = { focus = OPR_desorg }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_defence"
			add_ideas = OPR_forts_buildings
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_teroborona
		icon = opr_teroborona

		x = 0
		y = 3
		relative_position_id = OPR_Start_rebellion

		cost = 2

		prerequisite = { focus = OPR_government }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_teroborona"
			add_ideas = OPR_ter_orobona
			hidden_effect = {
				division_template = {
					name = "Territorial Defense Brigade"
					is_locked = yes
					regiments = {
						L_Inf_Bat = { x = 0 y = 0 }
						L_Inf_Bat = { x = 0 y = 1 }
						Mot_Inf_Bat = { x = 1 y = 0 }
					}
				}
			}
			695 = {
				create_unit = {
					division = "name = \"Territorial Defense Brigade\" division_template = \"Territorial Defense Brigade\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_afini
		icon = opr_odessa_drujina
		
		x = -1
		y = 4
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_teroborona }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		
		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_afini"
			hidden_effect = {
				division_template = {
					name = "Batalyon Spetsnaza"
					regiments = {
						Mech_Inf_Bat = { x = 0 y = 0 }
						Mech_Inf_Bat = { x = 0 y = 1 }
					}
					priority = 2
				}
			}
			695 = {
				create_unit = {
					division = "name = \"Odessa Squad\" division_template = \"Batalyon Spetsnaza\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_mali
		icon = opr_odessa_brigade

		x = 1
		y = 4
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_teroborona }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_mali"
			hidden_effect = {
				division_template = {
					name = "Brigada Dobrovolchev"
					regiments = {
						L_Inf_Bat = { x = 0 y = 0 }
						L_Inf_Bat = { x = 0 y = 1 }
						L_Inf_Bat = { x = 0 y = 2 }
					}
					support = {
						Arty_Battery = { x = 0 y = 0 }
					}
					priority = 0
				}
			}
			695 = {
				create_unit = {
					division = "name = \"Odessa Brigade\" division_template = \"Brigada Dobrovolchev\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_sloboda
		icon = opr_red_battalion
		x = 0
		y = 5
		relative_position_id = OPR_Start_rebellion

		cost = 1

		prerequisite = { focus = OPR_mali }
		prerequisite = { focus = OPR_afini }
		search_filters = { FOCUS_FILTER_INDUSTRY }
	
		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_sloboda"
			695 = {
				create_unit = {
					division = "name = \"Red Guard Brigade\" division_template = \"Brigada Dobrovolchev\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_guard_republic
		icon = opr_gvardia_battalion

		x = 2
		y = 6
		relative_position_id = OPR_Start_rebellion

		cost = 5

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		prerequisite = { focus = OPR_defence }
		prerequisite = { focus = OPR_sloboda }
		search_filters = { FOCUS_FILTER_INDUSTRY }
				
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_guard_republic"
			hidden_effect = {
				division_template = {
					name = "Mekhanizirovannaya Brigada"
					regiments = {
						Arm_Inf_Bat = { x = 0 y = 0 }
						Arm_Inf_Bat = { x = 0 y = 1 }
						Arm_Inf_Bat = { x = 0 y = 2 }
					}
					support = {
						SP_Arty_Battery = { x = 0 y = 0 }
						L_Engi_Comp = { x = 0 y = 1 }
					}
				}
			}
			695 = {
				create_unit = {
					division = "name = \"Respublikanskaya Gvardiya ONR\" division_template = \"Mekhanizirovannaya Brigada\" start_experience_factor = 0.1"
					prioritize_location = 11670
					owner = OPR
				}
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_energy
		icon = Hydro_sheep

		x = -2
		y = 6
		relative_position_id = OPR_Start_rebellion

		cost = 5

		prerequisite = { focus = OPR_operator }
		prerequisite = { focus = OPR_sloboda }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		
		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_energy"
			set_temp_variable = { temp_opinion = 3 }
			change_fossil_fuel_industry_opinion = yes
			696 = {
				add_building_construction = {
					type = fuel_silo
					level = 1
					instant_build = yes
				}
			}
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_infra
		icon = economic_roads_new

		x = 0
		y = 6
		relative_position_id = OPR_Start_rebellion

		cost = 5

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		
		prerequisite = { focus = OPR_sloboda }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_infra"
			one_random_infrastructure = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = OPR_referendum
		icon = SOV_russian_elections

		x = 0
		y = 7
		relative_position_id = OPR_Start_rebellion

		cost = 5

		prerequisite = { focus = OPR_infra }
		prerequisite = { focus = OPR_energy }
		prerequisite = { focus = OPR_guard_republic }
		search_filters = { FOCUS_FILTER_INDUSTRY }

		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
			has_war = no
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_referendum"
			SOV = { country_event = opr.1 }
			set_country_flag = SOV_subject_agree
			set_country_flag = SOV_subject_republic
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = OPR_economic
		icon = opr_economic
		x = 14
		y = 6
		search_filters = { FOCUS_FILTER_INDUSTRY }
		cost = 2
		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		ai_will_do = { factor = 1 }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_economic"
			increase_economic_growth = yes
		}
	}
	focus = {
		id = OPR_avto
		icon = continuous_repairments
		x = -1
		y = 1
		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		relative_position_id = OPR_economic
		prerequisite = { focus = OPR_economic }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		cost = 5
		ai_will_do = { factor = 155 }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_avto"
			add_ideas = OPR_avto
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = OPR_avia_zavod
		icon = economic_avia_industry
		x = 1
		y = 1
		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		relative_position_id = OPR_economic
		prerequisite = { focus = OPR_economic }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		cost = 5
		ai_will_do = { factor = 155 }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_avia_zavod"
			add_ideas = OPR_avia_zavod
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = OPR_tractor
		icon = construction_buldozer
		x = 0
		y = 2
		available = {
			OR = {
				OPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		relative_position_id = OPR_economic
		prerequisite = { focus = OPR_avia_zavod }
		prerequisite = { focus = OPR_avto }
		search_filters = { FOCUS_FILTER_INDUSTRY }
		cost = 5
		ai_will_do = { factor = 155 }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus OPR_tractor"
			hidden_effect = {
				remove_ideas = OPR_avto
				remove_ideas = OPR_avia_zavod
			}
			add_ideas = OPR_tractor
			set_temp_variable = { treasury_change = -0.7 }
			modify_treasury_effect = yes
		}
	}
	shared_focus = SUB_russia_politic
}