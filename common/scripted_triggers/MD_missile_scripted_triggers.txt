unique_missile_model_names = {
	OR = {
		original_tag = USA
		original_tag = SOV
		original_tag = CHI
		original_tag = ENG
		original_tag = FRA
		original_tag = ISR
		original_tag = RAJ
		original_tag = PAK
		original_tag = NKO
		original_tag = PER
		original_tag = GER
	}
}

unique_missile_model_texture = {
	always = no
}

unique_missile_model_names_selected_TAG = {
	OR = {
		check_variable = { orbit_selected_TAG = USA.id }
		check_variable = { orbit_selected_TAG = SOV.id }
		check_variable = { orbit_selected_TAG = CHI.id }
		check_variable = { orbit_selected_TAG = ENG.id }
		check_variable = { orbit_selected_TAG = FRA.id }
		check_variable = { orbit_selected_TAG = ISR.id }
		check_variable = { orbit_selected_TAG = RAJ.id }
		check_variable = { orbit_selected_TAG = PAK.id }
		check_variable = { orbit_selected_TAG = NKO.id }
		check_variable = { orbit_selected_TAG = PER.id }
		check_variable = { orbit_selected_TAG = GER.id }
	}
}

unique_missile_model_names_target_TAG = {
	OR = {
		check_variable = { ROOT.sat_target_TAG_array^i = USA.id }
		check_variable = { ROOT.sat_target_TAG_array^i = SOV.id }
		check_variable = { ROOT.sat_target_TAG_array^i = CHI.id }
		check_variable = { ROOT.sat_target_TAG_array^i = ENG.id }
		check_variable = { ROOT.sat_target_TAG_array^i = FRA.id }
		check_variable = { ROOT.sat_target_TAG_array^i = ISR.id }
		check_variable = { ROOT.sat_target_TAG_array^i = RAJ.id }
		check_variable = { ROOT.sat_target_TAG_array^i = PAK.id }
		check_variable = { ROOT.sat_target_TAG_array^i = NKO.id }
		check_variable = { ROOT.sat_target_TAG_array^i = PER.id }
		check_variable = { ROOT.sat_target_TAG_array^i = GER.id }
	}
}

#################################
### Satellite System Triggers ###
#################################

has_GNSS_mil_system_for_CM = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_for_CM_TT
		set_temp_variable = { temp_smaller_CM_idx = cruise_missile_display_array^i }
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx > 40 }
				check_variable = { temp_smaller_CM_idx < 49 }
			}
			subtract_from_temp_variable = { temp_smaller_CM_idx = 41 }
		}
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx > 50 }
				check_variable = { temp_smaller_CM_idx < 59 }
			}
			subtract_from_temp_variable = { temp_smaller_CM_idx = 51 }
		}
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx > 60 }
				check_variable = { temp_smaller_CM_idx < 69 }
			}
			subtract_from_temp_variable = { temp_smaller_CM_idx = 61 }
		}
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx = 71 }
			}
			set_temp_variable = { temp_smaller_CM_idx = 6 }
		}
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx = 72 }
			}
			set_temp_variable = { temp_smaller_CM_idx = 7 }
		}
		set_temp_variable = { temp_GNSS_idx = temp_smaller_CM_idx }
		subtract_from_temp_variable = { temp_smaller_CM_idx = 1 }
		OR = {
			check_variable = { var_GNSS_mil_system_idx > temp_smaller_CM_idx }
			AND = {
				if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 7 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 7 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 6 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 6 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 5 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 5 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 4 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 4 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 3 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 3 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 2 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 2 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 1 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 1 }
				}
				check_variable = { temp_GNSS_mil_access_system_idx > temp_smaller_CM_idx }
			}

			# any_of = {
			# 	array = GNSS_mil_access_system_idx_array
			# 	check_variable = { v > temp_smaller_CM_idx }
			# }
		}
	}
}

has_GNSS_mil_system_for_CM_AI_on_alert = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_for_CM_TT
		set_temp_variable = { temp_smaller_CM_idx = i2 }
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx > 40 }
				check_variable = { temp_smaller_CM_idx < 49 }
			}
			subtract_from_temp_variable = { temp_smaller_CM_idx = 41 }
		}
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx > 50 }
				check_variable = { temp_smaller_CM_idx < 59 }
			}
			subtract_from_temp_variable = { temp_smaller_CM_idx = 51 }
		}
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx > 60 }
				check_variable = { temp_smaller_CM_idx < 69 }
			}
			subtract_from_temp_variable = { temp_smaller_CM_idx = 61 }
		}
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx = 71 }
			}
			set_temp_variable = { temp_smaller_CM_idx = 6 }
		}
		if = {
			limit = {
				check_variable = { temp_smaller_CM_idx = 72 }
			}
			set_temp_variable = { temp_smaller_CM_idx = 7 }
		}
		set_temp_variable = { temp_GNSS_idx = temp_smaller_CM_idx }
		subtract_from_temp_variable = { temp_smaller_CM_idx = 1 }
		OR = {
			check_variable = { var_GNSS_mil_system_idx > temp_smaller_CM_idx }
			AND = {
				if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 7 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 7 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 6 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 6 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 5 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 5 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 4 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 4 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 3 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 3 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 2 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 2 }
				}
				else_if = {
					limit = {
						is_in_array = { GNSS_mil_access_system_idx_array = 1 }
					}
					set_temp_variable = { temp_GNSS_mil_access_system_idx = 1 }
				}
				check_variable = { temp_GNSS_mil_access_system_idx > temp_smaller_CM_idx }
			}

			# any_of = {
			# 	array = GNSS_mil_access_system_idx_array
			# 	check_variable = { v > temp_smaller_CM_idx }
			# }
		}
	}
}

has_GNSS_mil_system_0 = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_0_TT
		OR = {
			any_of = {
				array = GNSS_mil_systems_array
				check_variable = { GNSS_mil_systems_array^i > -1 }
			}
			any_of = {
				array = GNSS_mil_access_system_idx_array
				check_variable = { GNSS_mil_access_system_idx_array^i > -1 }
			}
		}
	}
}

has_GNSS_mil_system_1 = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_1_TT
		OR = {
			any_of = {
				array = GNSS_mil_systems_array
				check_variable = { GNSS_mil_systems_array^i > 0 }
			}
			any_of = {
				array = GNSS_mil_access_system_idx_array
				check_variable = { GNSS_mil_access_system_idx_array^i > 0 }
			}
		}
	}
}

has_GNSS_mil_system_2 = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_2_TT
		OR = {
			any_of = {
				array = GNSS_mil_systems_array
				check_variable = { GNSS_mil_systems_array^i > 1 }
			}
			any_of = {
				array = GNSS_mil_access_system_idx_array
				check_variable = { GNSS_mil_access_system_idx_array^i > 1 }
			}
		}
	}
}

has_GNSS_mil_system_3 = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_3_TT
		OR = {
			any_of = {
				array = GNSS_mil_systems_array
				check_variable = { GNSS_mil_systems_array^i > 2 }
			}
			any_of = {
				array = GNSS_mil_access_system_idx_array
				check_variable = { GNSS_mil_access_system_idx_array^i > 2 }
			}
		}
	}
}

has_GNSS_mil_system_4 = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_4_TT
		OR = {
			any_of = {
				array = GNSS_mil_systems_array
				check_variable = { GNSS_mil_systems_array^i > 3 }
			}
			any_of = {
				array = GNSS_mil_access_system_idx_array
				check_variable = { GNSS_mil_access_system_idx_array^i > 3 }
			}
		}
	}
}


has_GNSS_mil_system_5 = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_5_TT
		OR = {
			any_of = {
				array = GNSS_mil_systems_array
				check_variable = { GNSS_mil_systems_array^i > 4 }
			}
			any_of = {
				array = GNSS_mil_access_system_idx_array
				check_variable = { GNSS_mil_access_system_idx_array^i > 4 }
			}
		}
	}
}


has_GNSS_mil_system_6 = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_6_TT
		OR = {
			any_of = {
				array = GNSS_mil_systems_array
				check_variable = { GNSS_mil_systems_array^i > 5 }
			}
			any_of = {
				array = GNSS_mil_access_system_idx_array
				check_variable = { GNSS_mil_access_system_idx_array^i > 5 }
			}
		}
	}
}


has_GNSS_mil_system_7 = {
	custom_trigger_tooltip = {
		tooltip = has_GNSS_mil_system_7_TT
		OR = {
			any_of = {
				array = GNSS_mil_systems_array
				check_variable = { GNSS_mil_systems_array^i > 6 }
			}
			any_of = {
				array = GNSS_mil_access_system_idx_array
				check_variable = { GNSS_mil_access_system_idx_array^i > 6 }
			}
		}
	}
}


############
### OLV ###
############

has_OLV1_missile_inventory = {
	OR = {
		check_variable = { missile_inventory_array^101 > 0 }
		check_variable = { missile_inventory_array^102 > 0 }
		check_variable = { missile_inventory_array^103 > 0 }
		check_variable = { missile_inventory_array^104 > 0 }
		check_variable = { missile_inventory_array^105 > 0 }
		check_variable = { missile_inventory_array^106 > 0 }
		check_variable = { missile_inventory_array^107 > 0 }
		check_variable = { missile_inventory_array^108 > 0 }
	}
}

has_OLV2_missile_inventory = {
	OR = {
		check_variable = { missile_inventory_array^102 > 0 }
		check_variable = { missile_inventory_array^103 > 0 }
		check_variable = { missile_inventory_array^104 > 0 }
		check_variable = { missile_inventory_array^105 > 0 }
		check_variable = { missile_inventory_array^106 > 0 }
		check_variable = { missile_inventory_array^107 > 0 }
		check_variable = { missile_inventory_array^108 > 0 }
	}
}

has_OLV3_missile_inventory = {
	OR = {
		check_variable = { missile_inventory_array^103 > 0 }
		check_variable = { missile_inventory_array^104 > 0 }
		check_variable = { missile_inventory_array^105 > 0 }
		check_variable = { missile_inventory_array^106 > 0 }
		check_variable = { missile_inventory_array^107 > 0 }
		check_variable = { missile_inventory_array^108 > 0 }
	}
}

has_OLV4_missile_inventory = {
	OR = {
		check_variable = { missile_inventory_array^104 > 0 }
		check_variable = { missile_inventory_array^105 > 0 }
		check_variable = { missile_inventory_array^106 > 0 }
		check_variable = { missile_inventory_array^107 > 0 }
		check_variable = { missile_inventory_array^108 > 0 }
	}
}

has_OLV5_missile_inventory = {
	OR = {
		check_variable = { missile_inventory_array^105 > 0 }
		check_variable = { missile_inventory_array^106 > 0 }
		check_variable = { missile_inventory_array^107 > 0 }
		check_variable = { missile_inventory_array^108 > 0 }
	}
}

has_OLV6_missile_inventory = {
	OR = {
		check_variable = { missile_inventory_array^106 > 0 }
		check_variable = { missile_inventory_array^107 > 0 }
		check_variable = { missile_inventory_array^108 > 0 }
	}
}

has_OLV7_missile_inventory = {
	OR = {
		check_variable = { missile_inventory_array^107 > 0 }
		check_variable = { missile_inventory_array^108 > 0 }
	}
}

has_OLV8_missile_inventory = {
	check_variable = { missile_inventory_array^108 > 0 }
}

#################################
### check satellites in orbit ###
#################################

############
### GNSS ###
############

has_GNSS1_in_orbit = {
	OR = {
		check_variable = { GNSS_satellite_array^0 > 0 }
		check_variable = { GNSS_satellite_array^1 > 0 }
		check_variable = { GNSS_satellite_array^2 > 0 }
		check_variable = { GNSS_satellite_array^3 > 0 }
		check_variable = { GNSS_satellite_array^4 > 0 }
		check_variable = { GNSS_satellite_array^5 > 0 }
		check_variable = { GNSS_satellite_array^6 > 0 }
		check_variable = { GNSS_satellite_array^7 > 0 }
	}
}

has_GNSS2_in_orbit = {
	OR = {
		#check_variable = { GNSS_satellite_array^0 > 0 }
		check_variable = { GNSS_satellite_array^1 > 0 }
		check_variable = { GNSS_satellite_array^2 > 0 }
		check_variable = { GNSS_satellite_array^3 > 0 }
		check_variable = { GNSS_satellite_array^4 > 0 }
		check_variable = { GNSS_satellite_array^5 > 0 }
		check_variable = { GNSS_satellite_array^6 > 0 }
		check_variable = { GNSS_satellite_array^7 > 0 }
	}
}

has_GNSS3_in_orbit = {
	OR = {
		check_variable = { GNSS_satellite_array^2 > 0 }
		check_variable = { GNSS_satellite_array^3 > 0 }
		check_variable = { GNSS_satellite_array^4 > 0 }
		check_variable = { GNSS_satellite_array^5 > 0 }
		check_variable = { GNSS_satellite_array^6 > 0 }
		check_variable = { GNSS_satellite_array^7 > 0 }
	}
}

has_GNSS4_in_orbit = {
	OR = {
		check_variable = { GNSS_satellite_array^3 > 0 }
		check_variable = { GNSS_satellite_array^4 > 0 }
		check_variable = { GNSS_satellite_array^5 > 0 }
		check_variable = { GNSS_satellite_array^6 > 0 }
		check_variable = { GNSS_satellite_array^7 > 0 }
	}
}

has_GNSS5_in_orbit = {
	OR = {
		check_variable = { GNSS_satellite_array^4 > 0 }
		check_variable = { GNSS_satellite_array^5 > 0 }
		check_variable = { GNSS_satellite_array^6 > 0 }
		check_variable = { GNSS_satellite_array^7 > 0 }
	}
}

has_GNSS6_in_orbit = {
	OR = {
		check_variable = { GNSS_satellite_array^5 > 0 }
		check_variable = { GNSS_satellite_array^6 > 0 }
		check_variable = { GNSS_satellite_array^7 > 0 }
	}
}

has_GNSS7_in_orbit = {
	OR = {
		check_variable = { GNSS_satellite_array^6 > 0 }
		check_variable = { GNSS_satellite_array^7 > 0 }
	}
}

has_GNSS8_in_orbit = {
	check_variable = { GNSS_satellite_array^7 > 0 }
}

##############
### COMSAT ###
##############

has_COMSAT1_in_orbit = {
	OR = {
		check_variable = { COM_satellite_array^0 > 0 }
		check_variable = { COM_satellite_array^1 > 0 }
		check_variable = { COM_satellite_array^2 > 0 }
		check_variable = { COM_satellite_array^3 > 0 }
		check_variable = { COM_satellite_array^4 > 0 }
		check_variable = { COM_satellite_array^5 > 0 }
		check_variable = { COM_satellite_array^6 > 0 }
		check_variable = { COM_satellite_array^7 > 0 }
	}
}

has_COMSAT2_in_orbit = {
	OR = {
		check_variable = { COM_satellite_array^1 > 0 }
		check_variable = { COM_satellite_array^2 > 0 }
		check_variable = { COM_satellite_array^3 > 0 }
		check_variable = { COM_satellite_array^4 > 0 }
		check_variable = { COM_satellite_array^5 > 0 }
		check_variable = { COM_satellite_array^6 > 0 }
		check_variable = { COM_satellite_array^7 > 0 }
	}
}

has_COMSAT3_in_orbit = {
	OR = {
		check_variable = { COM_satellite_array^2 > 0 }
		check_variable = { COM_satellite_array^3 > 0 }
		check_variable = { COM_satellite_array^4 > 0 }
		check_variable = { COM_satellite_array^5 > 0 }
		check_variable = { COM_satellite_array^6 > 0 }
		check_variable = { COM_satellite_array^7 > 0 }
	}
}

has_COMSAT4_in_orbit = {
	OR = {
		check_variable = { COM_satellite_array^3 > 0 }
		check_variable = { COM_satellite_array^4 > 0 }
		check_variable = { COM_satellite_array^5 > 0 }
		check_variable = { COM_satellite_array^6 > 0 }
		check_variable = { COM_satellite_array^7 > 0 }
	}
}

has_COMSAT5_in_orbit = {
	OR = {
		check_variable = { COM_satellite_array^4 > 0 }
		check_variable = { COM_satellite_array^5 > 0 }
		check_variable = { COM_satellite_array^6 > 0 }
		check_variable = { COM_satellite_array^7 > 0 }
	}
}

has_COMSAT6_in_orbit = {
	OR = {
		check_variable = { COM_satellite_array^5 > 0 }
		check_variable = { COM_satellite_array^6 > 0 }
		check_variable = { COM_satellite_array^7 > 0 }
	}
}

has_COMSAT7_in_orbit = {
	OR = {
		check_variable = { COM_satellite_array^6 > 0 }
		check_variable = { COM_satellite_array^7 > 0 }
	}
}

has_COMSAT8_in_orbit = {
	check_variable = { COM_satellite_array^7 > 0 }
}

##############
### SPYSAT ###
##############

has_SPYSAT1_in_orbit = {
	OR = {
		check_variable = { SPY_satellite_array^0 > 0 }
		check_variable = { SPY_satellite_array^1 > 0 }
		check_variable = { SPY_satellite_array^2 > 0 }
		check_variable = { SPY_satellite_array^3 > 0 }
		check_variable = { SPY_satellite_array^4 > 0 }
		check_variable = { SPY_satellite_array^5 > 0 }
		check_variable = { SPY_satellite_array^6 > 0 }
		check_variable = { SPY_satellite_array^7 > 0 }
	}
}

has_SPYSAT2_in_orbit = {
	OR = {
		check_variable = { SPY_satellite_array^1 > 0 }
		check_variable = { SPY_satellite_array^2 > 0 }
		check_variable = { SPY_satellite_array^3 > 0 }
		check_variable = { SPY_satellite_array^4 > 0 }
		check_variable = { SPY_satellite_array^5 > 0 }
		check_variable = { SPY_satellite_array^6 > 0 }
		check_variable = { SPY_satellite_array^7 > 0 }
	}
}

has_SPYSAT3_in_orbit = {
	OR = {
		check_variable = { SPY_satellite_array^2 > 0 }
		check_variable = { SPY_satellite_array^3 > 0 }
		check_variable = { SPY_satellite_array^4 > 0 }
		check_variable = { SPY_satellite_array^5 > 0 }
		check_variable = { SPY_satellite_array^6 > 0 }
		check_variable = { SPY_satellite_array^7 > 0 }
	}
}

has_SPYSAT4_in_orbit = {
	OR = {
		check_variable = { SPY_satellite_array^3 > 0 }
		check_variable = { SPY_satellite_array^4 > 0 }
		check_variable = { SPY_satellite_array^5 > 0 }
		check_variable = { SPY_satellite_array^6 > 0 }
		check_variable = { SPY_satellite_array^7 > 0 }
	}
}

has_SPYSAT5_in_orbit = {
	OR = {
		check_variable = { SPY_satellite_array^4 > 0 }
		check_variable = { SPY_satellite_array^5 > 0 }
		check_variable = { SPY_satellite_array^6 > 0 }
		check_variable = { SPY_satellite_array^7 > 0 }
	}
}

has_SPYSAT6_in_orbit = {
	OR = {
		check_variable = { SPY_satellite_array^5 > 0 }
		check_variable = { SPY_satellite_array^6 > 0 }
		check_variable = { SPY_satellite_array^7 > 0 }
	}
}

has_SPYSAT7_in_orbit = {
	OR = {
		check_variable = { SPY_satellite_array^6 > 0 }
		check_variable = { SPY_satellite_array^7 > 0 }
	}
}

has_SPYSAT8_in_orbit = {
	check_variable = { SPY_satellite_array^7 > 0 }
}

###############
### KILLSAT ###
###############

has_KILLSAT1_in_orbit = {
	OR = {
		check_variable = { KILL_satellite_array^0 > 0 }
		check_variable = { KILL_satellite_array^1 > 0 }
		check_variable = { KILL_satellite_array^2 > 0 }
		check_variable = { KILL_satellite_array^3 > 0 }
		check_variable = { KILL_satellite_array^4 > 0 }
		check_variable = { KILL_satellite_array^5 > 0 }
	}
}

has_KILLSAT2_in_orbit = {
	OR = {
		check_variable = { KILL_satellite_array^1 > 0 }
		check_variable = { KILL_satellite_array^2 > 0 }
		check_variable = { KILL_satellite_array^3 > 0 }
		check_variable = { KILL_satellite_array^4 > 0 }
		check_variable = { KILL_satellite_array^5 > 0 }
	}
}

has_KILLSAT3_in_orbit = {
	OR = {
		check_variable = { KILL_satellite_array^2 > 0 }
		check_variable = { KILL_satellite_array^3 > 0 }
		check_variable = { KILL_satellite_array^4 > 0 }
		check_variable = { KILL_satellite_array^5 > 0 }
	}
}

has_KILLSAT4_in_orbit = {
	OR = {
		check_variable = { KILL_satellite_array^3 > 0 }
		check_variable = { KILL_satellite_array^4 > 0 }
		check_variable = { KILL_satellite_array^5 > 0 }
	}
}

has_KILLSAT5_in_orbit = {
	OR = {
		check_variable = { KILL_satellite_array^4 > 0 }
		check_variable = { KILL_satellite_array^5 > 0 }
	}
}

has_KILLSAT6_in_orbit = {
	check_variable = { KILL_satellite_array^5 > 0 }
}

ai_acceptance_request_satellites_with_friends = {
	OR = {
		THIS = { has_opinion = { target = ROOT value > 149 } }
		AND = {
			THIS = { has_opinion = { target = ROOT value > 49 } }
			THIS = { has_military_access_to = ROOT }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 99 } }
			THIS = { gives_military_access_to = ROOT }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 49 } }
			ROOT = { has_volunteers_amount_from = { tag = THIS count > 0 } }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 49 } }
			THIS = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
		}
		AND = {
			THIS = { has_idea = NATO_member }
			ROOT = { has_idea = NATO_member }
		}
		AND = {
			THIS = { has_idea = EU_member }
			ROOT = { has_idea = EU_member }
		}
		THIS = { is_in_faction_with = ROOT }
		ROOT = { has_war_together_with = THIS }
		### TAG specific
		#USA
		AND = {
			THIS = { original_tag = USA }
			ROOT = { has_idea = Major_Non_NATO_Ally }
		}
		AND = {
			THIS = { original_tag = USA }
			ROOT = { has_idea = NATO_member }
		}
		#ENG
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = ENG }
			ROOT = { has_idea = commonwealth_of_nations_member }
		}
		#SOV
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = SOV }
			ROOT = { original_tag = SYR }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = SOV }
			ROOT = { original_tag = NOV }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = SOV }
			ROOT = { original_tag = ABK }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = SOV }
			ROOT = { original_tag = SOO }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = SOV }
			ROOT = { original_tag = SOO }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = SOV }
			ROOT = { original_tag = BLR }
		}
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = SOV }
			ROOT = { original_tag = PMR }
		}
		#CHI
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = CHI }
			ROOT = { has_idea = sco_member }
		}
		AND = {
			THIS = { original_tag = CHI }
			ROOT = { has_idea = sco_member_mil }
		}
		#PER
		AND = {
			THIS = { has_opinion = { target = ROOT value > 19 } }
			THIS = { original_tag = PER }
			ROOT = { has_idea = shia }
		}
		AND = {
			THIS = { original_tag = PER }
			ROOT = { original_tag = SYR }
		}
		AND = {
			THIS = { original_tag = PER }
			ROOT = { original_tag = LEB }
		}
		AND = {
			THIS = { original_tag = PER }
			ROOT = { original_tag = HAM }
		}
		AND = {
			THIS = { original_tag = PER }
			ROOT = { original_tag = HOU }
		}
		AND = {
			THIS = { original_tag = PER }
			ROOT = { original_tag = HEZ }
		}
		#PAK
		AND = {
			THIS = { original_tag = PAK }
			ROOT = { original_tag = SAU }
		}
	}
}

NOT_ai_acceptance_request_satellites_with_foes = {
	OR = {
		THIS = { has_opinion = { target = ROOT value < 150 } }
		ROOT = { has_war_with = THIS }
		ROOT = { is_justifying_wargoal_against = THIS }
		ROOT = { has_wargoal_against = THIS }
		### TAG specific
		#SOV
		AND = {
			THIS = { original_tag = SOV }
			ROOT = { has_idea = EU_member }
		}
		AND = {
			THIS = { original_tag = SOV }
			ROOT = { has_idea = NATO_member }
		}
		AND = {
			THIS = { original_tag = SOV }
			ROOT = { has_idea = Major_Non_NATO_Ally }
		}
		#CHI
		AND = {
			THIS = { original_tag = CHI }
			ROOT = { has_idea = EU_member }
		}
		AND = {
			THIS = { original_tag = CHI }
			ROOT = { has_idea = NATO_member }
		}
		AND = {
			THIS = { original_tag = CHI }
			ROOT = { has_idea = Major_Non_NATO_Ally }
		}
		AND = {
			THIS = { original_tag = CHI }
			ROOT = { original_tag = TAI }
		}
		#USA
		AND = {
			THIS = { original_tag = USA }
			ROOT = { has_idea = sco_member_mil }
		}
		AND = {
			THIS = { original_tag = USA }
			ROOT = { has_idea = sco_member }
		}
		AND = {
			THIS = { original_tag = USA }
			ROOT = { original_tag = NKO }
		}
		#FRA
		AND = {
			THIS = { original_tag = FRA }
			ROOT = { has_idea = sco_member_mil }
		}
		AND = {
			THIS = { original_tag = FRA }
			ROOT = { has_idea = sco_member }
		}
		#ENG
		AND = {
			THIS = { original_tag = ENG }
			ROOT = { has_idea = sco_member_mil }
		}
		AND = {
			THIS = { original_tag = ENG }
			ROOT = { has_idea = sco_member }
		}
		#NOK
		AND = {
			THIS = { original_tag = NKO }
			ROOT = { original_tag = USA }
		}
		AND = {
			THIS = { original_tag = NKO }
			ROOT = { has_idea = Major_Non_NATO_Ally }
		}
		AND = {
			THIS = { original_tag = NKO }
			ROOT = { has_idea = NATO_member }
		}
		#ISR
		AND = {
			THIS = { original_tag = ISR }
			ROOT = { original_tag = PER }
		}
		AND = {
			THIS = { original_tag = ISR }
			ROOT = { original_tag = SYR }
		}
		AND = {
			THIS = { original_tag = ISR }
			ROOT = { original_tag = LEB }
		}
		AND = {
			THIS = { original_tag = ISR }
			ROOT = { original_tag = HAM }
		}
		AND = {
			THIS = { original_tag = ISR }
			ROOT = { original_tag = HOU }
		}
		AND = {
			THIS = { original_tag = ISR }
			ROOT = { original_tag = HEZ }
		}
		#PER
		AND = {
			THIS = { original_tag = PER }
			ROOT = { original_tag = USA }
		}
		AND = {
			THIS = { original_tag = PER }
			ROOT = { original_tag = ISR }
		}
		AND = {
			THIS = { original_tag = PER }
			ROOT = { has_idea = Major_Non_NATO_Ally }
		}
		AND = {
			THIS = { original_tag = PER }
			ROOT = { has_idea = NATO_member }
		}
		#PAK
		AND = {
			THIS = { original_tag = PAK }
			ROOT = { original_tag = RAJ }
		}
		#RAJ
		AND = {
			THIS = { original_tag = RAJ }
			ROOT = { original_tag = PAK }
		}
	}
}

# offer
# PREV = target / bonus TAG
# ROOT = sending TAG / access TAG

share_satellites_with_friends = {
	OR = {
		ROOT = { has_opinion = { target = PREV value > 149 } }
		AND = {
			ROOT = { has_opinion = { target = PREV value > 49 } }
			ROOT = { has_military_access_to = PREV }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 99 } }
			ROOT = { gives_military_access_to = PREV }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 49 } }
			THIS = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 49 } }
			ROOT = { has_volunteers_amount_from = { tag = PREV count > 0 } }
		}
		AND = {
			ROOT = { has_idea = NATO_member }
			THIS = { has_idea = NATO_member }
		}
		AND = {
			ROOT = { has_idea = EU_member }
			THIS = { has_idea = EU_member }
		}
		ROOT = { is_in_faction_with = PREV }
		THIS = { has_war_together_with = ROOT }
		### TAG specific
		#USA
		AND = {
			ROOT = { original_tag = USA }
			THIS = { has_idea = Major_Non_NATO_Ally }
		}
		AND = {
			ROOT = { original_tag = USA }
			THIS = { has_idea = NATO_member }
		}
		#ENG
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = ENG }
			THIS = { has_idea = commonwealth_of_nations_member }
		}
		#SOV
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = SOV }
			THIS = { original_tag = SYR }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = SOV }
			THIS = { original_tag = NOV }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = SOV }
			THIS = { original_tag = ABK }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = SOV }
			THIS = { original_tag = SOO }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = SOV }
			THIS = { original_tag = SOO }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = SOV }
			THIS = { original_tag = BLR }
		}
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = SOV }
			THIS = { original_tag = PMR }
		}
		#CHI
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = CHI }
			THIS = { has_idea = sco_member }
		}
		AND = {
			ROOT = { original_tag = CHI }
			THIS = { has_idea = sco_member_mil }
		}
		#PER
		AND = {
			ROOT = { has_opinion = { target = PREV value > 19 } }
			ROOT = { original_tag = PER }
			THIS = { has_idea = shia }
		}
		AND = {
			ROOT = { original_tag = PER }
			THIS = { original_tag = SYR }
		}
		AND = {
			ROOT = { original_tag = PER }
			THIS = { original_tag = LEB }
		}
		AND = {
			ROOT = { original_tag = PER }
			THIS = { original_tag = HAM }
		}
		AND = {
			ROOT = { original_tag = PER }
			THIS = { original_tag = HOU }
		}
		AND = {
			ROOT = { original_tag = PER }
			THIS = { original_tag = HEZ }
		}
		#PAK
		AND = {
			ROOT = { original_tag = PAK }
			THIS = { original_tag = SAU }
		}
	}
}

NOT_share_satellites_with_foes = {
	OR = {
		ROOT = { has_opinion = { target = PREV value < 150 } }
		THIS = { has_war_with = ROOT }
		THIS = { is_justifying_wargoal_against = ROOT }
		THIS = { has_wargoal_against = ROOT }
		### TAG specific
		#SOV
		AND = {
			ROOT = { original_tag = SOV }
			THIS = { has_idea = EU_member }
		}
		AND = {
			ROOT = { original_tag = SOV }
			THIS = { has_idea = NATO_member }
		}
		AND = {
			ROOT = { original_tag = SOV }
			THIS = { has_idea = Major_Non_NATO_Ally }
		}
		#CHI
		AND = {
			ROOT = { original_tag = CHI }
			THIS = { has_idea = EU_member }
		}
		AND = {
			ROOT = { original_tag = CHI }
			THIS = { has_idea = NATO_member }
		}
		AND = {
			ROOT = { original_tag = CHI }
			THIS = { has_idea = Major_Non_NATO_Ally }
		}
		AND = {
			ROOT = { original_tag = CHI }
			THIS = { original_tag = TAI }
		}
		#USA
		AND = {
			ROOT = { original_tag = USA }
			THIS = { has_idea = sco_member_mil }
		}
		AND = {
			ROOT = { original_tag = USA }
			THIS = { has_idea = sco_member }
		}
		AND = {
			ROOT = { original_tag = USA }
			THIS = { original_tag = NKO }
		}
		#FRA
		AND = {
			ROOT = { original_tag = FRA }
			THIS = { has_idea = sco_member_mil }
		}
		AND = {
			ROOT = { original_tag = FRA }
			THIS = { has_idea = sco_member }
		}
		#ENG
		AND = {
			ROOT = { original_tag = ENG }
			THIS = { has_idea = sco_member_mil }
		}
		AND = {
			ROOT = { original_tag = ENG }
			THIS = { has_idea = sco_member }
		}
		#NOK
		AND = {
			ROOT = { original_tag = NKO }
			THIS = { original_tag = USA }
		}
		AND = {
			ROOT = { original_tag = NKO }
			THIS = { has_idea = Major_Non_NATO_Ally }
		}
		AND = {
			ROOT = { original_tag = NKO }
			THIS = { has_idea = NATO_member }
		}
		#ISR
		AND = {
			ROOT = { original_tag = ISR }
			THIS = { original_tag = PER }
		}
		AND = {
			ROOT = { original_tag = ISR }
			THIS = { original_tag = SYR }
		}
		AND = {
			ROOT = { original_tag = ISR }
			THIS = { original_tag = LEB }
		}
		AND = {
			ROOT = { original_tag = ISR }
			THIS = { original_tag = HAM }
		}
		AND = {
			ROOT = { original_tag = ISR }
			THIS = { original_tag = HOU }
		}
		AND = {
			ROOT = { original_tag = ISR }
			THIS = { original_tag = HEZ }
		}
		#PER
		AND = {
			ROOT = { original_tag = PER }
			THIS = { original_tag = USA }
		}
		AND = {
			ROOT = { original_tag = PER }
			THIS = { original_tag = ISR }
		}
		AND = {
			ROOT = { original_tag = PER }
			THIS = { has_idea = Major_Non_NATO_Ally }
		}
		AND = {
			ROOT = { original_tag = PER }
			THIS = { has_idea = NATO_member }
		}
		#PAK
		AND = {
			ROOT = { original_tag = PAK }
			THIS = { original_tag = RAJ }
		}
		#RAJ
		AND = {
			ROOT = { original_tag = RAJ }
			THIS = { original_tag = PAK }
		}
	}
}

NOT_share_COM_mil_satellites_above_network_traffic_limit = {
	OR = {
		check_variable = {
			ROOT.var_sat_network_traffic_mil > 1.249
		}
		AND = {
			check_variable = {
				ROOT.var_sat_network_traffic_mil < 1
			}
			set_temp_variable = { temp1 = ROOT.var_COM_mil_receiver_num }
			add_to_temp_variable = { temp1 = THIS.num_battalions }
			add_to_temp_variable = { temp1 = THIS.num_ships }
			add_to_temp_variable = { temp1 = THIS.num_deployed_planes }
			divide_temp_variable = { temp1 = ROOT.var_COM_mil_receiver_cap }
			check_variable = {
				temp1 > 1.249
			}
		}
	}
}

NOT_share_COM_civ_satellites_above_network_traffic_limit = {
	OR = {
		check_variable = {
			ROOT.var_sat_network_traffic_civ > 1.249
		}
		AND = {
			check_variable = {
				ROOT.var_sat_network_traffic_civ < 1
			}
			set_temp_variable = { temp1 = ROOT.var_COM_civ_receiver_num }
			set_temp_variable = { temp2 = THIS.num_controlled_states }
			multiply_temp_variable = { temp2 = 100 }
			add_to_temp_variable = { temp1 = temp2 }
			divide_temp_variable = { temp1 = ROOT.var_COM_civ_receiver_cap }
			check_variable = {
				temp1 > 1.249
			}
		}
	}
}
