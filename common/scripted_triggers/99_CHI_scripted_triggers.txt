### Author: Manchu
### Chinese Trigger Conditions

is_sco = {
	OR = {
		has_idea = sco_member
		has_idea = sco_member_econ
		has_idea = sco_member_pol
		has_idea = sco_member_mil
	}
}


all_sco_members_are_china_ruling_party = {
	if = {
		limit = { has_government = communism }
		custom_trigger_tooltip = {
			tooltip = SCO_all_emerging_tt
			NOT = {
				any_of_scopes = {
					array = global.sco_members
					is_sco = yes
					NOT = { has_government = communism }
				}
			}
		}
	}
	if = {
		limit = { has_government = democratic }
		custom_trigger_tooltip = {
			tooltip = SCO_all_democratic_tt
			NOT = {
				any_of_scopes = {
					array = global.sco_members
					is_sco = yes
					NOT = { has_government = democratic }
				}
			}
		}
	}
}