CZE_skoda_set_up_new_engine_production_idea_check = {
	OR = {
		has_idea = CZE_skoda_set_up_new_engine_production_idea
		hidden_trigger = {
			OR = {
				has_idea = CZE_skoda_set_up_new_engine_production2_idea
				has_idea = CZE_skoda_set_up_new_engine_production3_idea
			}
		}
	}
}

SPOLU_member_is_in_power = {
	custom_trigger_tooltip = {
		tooltip = SPOLU_member_is_in_power_desc
		OR = {
			western_conservatism_are_in_power = yes
			western_liberals_are_in_power = yes
			neutrality_neutral_conservatism_are_in_power = yes
		}
	}
}

CZE_check_petr_pavel_training_points = {
	custom_trigger_tooltip = {
		tooltip = CZE_check_petr_pavel
		check_variable = { petr_pavel_training_points > 99.99 }
	}
}

CZE_exists_not_at_war_not_embargoing = {
	custom_trigger_tooltip = {
		tooltip = CZE_exists_not_at_war_not_embargoing_TT
		NOT = {
			has_war_with = CZE
			is_embargoed_by = CZE
			is_embargoing = CZE
		}
		exists = yes
		has_opinion = {
			target = CZE
			value > 44
		}
	}
}

CZE_SLO_generic_focus_trigger = {
	CZE = {
		exists = yes
		NOT = {
			has_war_with = SLO
			is_subject_of = SLO
		}
		has_opinion = {
			target = SLO
			value > 9
		}
	}
	SLO = {
		exists = yes
		NOT = {
			has_war_with = CZE
			is_subject_of = CZE
		}
		has_opinion = {
			target = CZE
			value > 9
		}
	}
}

CZE_SLO_in_nato = {
	CZE = {
		is_in_faction_with = USA
	}
	SLO = {
		is_in_faction_with = USA
	}
}

CZE_SLO_both_or_none_in_nato = {
	OR = {
		AND = {
			CZE = {
				is_in_faction_with = USA
			}
			SLO = {
				is_in_faction_with = USA
			}
		}
		AND = {
			NOT = {
				CZE = {
					is_in_faction_with = USA
				}
				SLO = {
					is_in_faction_with = USA
				}
			}
		}
		has_global_flag = GAME_RULE_nato_disabled
	}
}

CZE_SLO_together_again_trigger = {
	custom_trigger_tooltip = {
		tooltip = czechoslovakia_restored_TT
		OR = {
			CZE = {
				has_cosmetic_tag = CZE_SLO_czechoslovakia
			}
			SLO = {
				has_cosmetic_tag = CZE_SLO_czechoslovakia
			}
		}
	}
}

is_czechoslovakian_country = {
	OR = {
		original_tag = CZE
		original_tag = SLO
	}
}