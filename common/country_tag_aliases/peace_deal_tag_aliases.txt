# AMERICAN OCCUPIED RUSSIA
AOR = {
	original_tag = SOV
	has_country_flag = SOV_american_occupation_zone
}

# EUROPEAN OCCUPIED RUSSIA
EOR = {
	original_tag = SOV
	has_country_flag = SOV_european_occupation_zone
}