unique_id = MD_custom_ribbons_2777392649

# Ribbons
# Ribbons have four frames as of current. Create a dummy ribbon thing to test out your ribbon colors before creating it.
# I haven't found another way of testing this yet.
# DO NOT USE TAG SCRIPTED LOC TOOLTIPS. You need hard coded tooltips for this else it errors.
started_millennium_dawn = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
	}

	happened = {
		date > 1999.01.02
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

conquer_taiwan_as_china = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = CHI
	}

	happened = {
		date > 1999.01.02
		is_taiwan_area_state_owned = yes
		NOT = { country_exists = TAI }
	}

	ribbon = { # TODO: Redo the Ribbon
		frames = { 1 1 1 1 }
		colors = {
			{ 0.0 0.0 149.0 1.0 }
			{ 254.0 0.0 0.0 1.0 }
			{ 255.0 255.0 255.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

annex_mongolia_as_china = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = CHI
	}

	happened = {
		date > 1999.01.02
		is_outer_mongolia_state_owned = yes
		NOT = { country_exists = MON }
	}

	ribbon = { # TODO: Redo the Ribbon
		frames = { 1 1 1 1 }
		colors = {
			{ 218.0 32.0 49.0 1.0 }
			{ 0.0 102.0 178.0 1.0 }
			{ 255.0 211.0 0.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

control_south_sea_as_china = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = CHI
	}

	happened = {
		date > 1999.01.02
		is_south_china_sea_minor_state_owned = yes
	}

	ribbon = { # TODO: Redo the Ribbon
		frames = { 1 1 1 1 }
		colors = {
			{ 238.0 28.0 37.0 1.0 }
			{ 255.0 255.0 0.0 1.0 }
			{ 255.0 255.0 255.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}


reclaim_ambazonia = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = NIG
	}

	happened = {
		date > 1999.01.02
		owns_state = 322
	}

	ribbon = { # TODO: Redo the Ribbon
		frames = { 1 1 1 1 }
		colors = {
			{ 45.0 90.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

take_back_eritrea = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = ETH
	}

	happened = {
		date > 1999.01.02
		owns_state = 228
		owns_state = 229
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 228.0 0.0 43.0 1.0 }
			{ 255.0 199.0 44.0 1.0 }
			{ 67.0 176.0 42.0 1.0 }
			{ 65.0 143.0 222.0 1.0 }
		}
	}
}

take_back_sahrawi = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = MOR
	}

	happened = {
		date > 1999.01.02
		owns_state = 372
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

take_back_western_sahara = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = SHA
	}

	happened = {
		date > 1999.01.02
		owns_state = 373
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

take_back_the_breakaways = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = GEO
	}

	happened = {
		date > 1999.01.02
		owns_state = 706
		owns_state = 705
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

end_the_angolan_civil_war = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		OR = {
			original_tag = UNI
			original_tag = AGL
		}
	}

	happened = {
		date > 1999.01.02
		hidden_trigger = {
			owns_state = 297
			owns_state = 298
			owns_state = 299
			owns_state = 301
			owns_state = 300
			owns_state = 296
		}
		has_global_flag = Angola_Civil_War_Over
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

end_the_somali_civil_war = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		OR = {
			original_tag = SNA
			original_tag = SOM
		}
	}

	happened = {
		date > 1999.01.02
		hidden_trigger = {
			owns_state = 581
			owns_state = 584
			owns_state = 593
			owns_state = 240
			owns_state = 975
			owns_state = 239
			owns_state = 928
			owns_state = 238
			owns_state = 938
			owns_state = 237
			owns_state = 937
		}
		has_global_flag = Somalia_Civil_War_Over
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

armenian_western_arm = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = ARM
	}

	happened = {
		date > 1999.01.02
		owns_state = 935
		owns_state = 163
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

armenian_polish_hungarian_commonwealth = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = ARM
	}

	happened = {
		date > 1999.01.02
		has_completed_focus = ARM_bagratuni
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

armenian_shadow_of_khoj_unthinkable = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = ARM
	}

	happened = {
		date > 1999.01.02
		has_completed_focus = ARM_Sarkisyan
		AND = {
			owns_state = 712
			owns_state = 713
			owns_state = 1039
			NOT = { country_exists = AZE }
		}
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

armenian_everybody_dies_i_will_live = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = ARM
	}

	happened = {
		date > 1999.01.02
		has_completed_focus = ARM_trade_republic
		AND = {
			owns_state = 712
			owns_state = 713
			owns_state = 1039
			owns_state = 163
			owns_state = 935
			NOT = { country_exists = AZE }
			owns_state = 710
			owns_state = 1036
			owns_state = 1037
			NOT = { country_exists = NKR }
		}
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = { # TODO: Redo the Ribbon
			{ 45.0 64.0 102.0 1.0 }
			{ 154.0 73.0 107.0 1.0 }
			{ 238.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

usa_manifest_destiny = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = USA
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		all_other_country = {
			OR = {
				has_idea = superpower
				has_idea = great_power
			}

			has_western_aligned_government = yes
		}
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 45.0 64.0 102.0 }
			{ 154.0 73.0 107.0 }
			{ 238.0 189.0 96.0 }
			{ 211.0 181.0 128.0 }
		}
	}
}

usa_the_purple_plutocrat = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = USA
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		has_completed_focus = USA_focus_2000_republicans
		has_completed_focus = USA_focus_2004_republicans
		has_completed_focus = USA_focus_2008_democrats
		has_completed_focus = USA_focus_2012_democrats
		has_completed_focus = USA_focus_2016_republican_victory
	}


	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 135.0 135.0 135.0 }
			{ 156.0 56.0 199.0 }
			{ 156.0 56.0 199.0 }
			{ 204.0 183.0 2.0 }
		}
	}
}

usa_republican_service_cross = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = USA
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		has_completed_focus = USA_focus_2000_republicans
		has_completed_focus = USA_focus_2004_republicans
		has_completed_focus = USA_focus_2008_republicans
		has_completed_focus = USA_focus_2012_republicans
		has_completed_focus = USA_focus_2016_republican_victory
	}


	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 135.0 135.0 135.0 }
			{ 179.0 0.0 0.0 }
			{ 179.0 0.0 0.0 }
			{ 166.0 104.0 18.0 }
		}
	}
}

usa_democratic_service_award = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = USA
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		has_completed_focus = USA_focus_2000_democrats
		has_completed_focus = USA_focus_2004_democrats
		has_completed_focus = USA_focus_2008_democrats
		has_completed_focus = USA_focus_2012_democrats
		has_completed_focus = USA_focus_2016_democrat_victory
	}


	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 135.0 135.0 135.0 }
			{ 25.0 33.0 255.0 }
			{ 25.0 33.0 255.0 }
			{ 166.0 104.0 18.0 }
		}
	}
}

usa_liberty_warrior = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = USA
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		has_completed_focus = USA_pulling_back_the_veil
		custom_trigger_tooltip = {
			tooltip = has_neutral_libertarian_government_TT
			is_in_array = { ruling_party = 16 }
		}
	}


	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 227.0 227.0 227.0 }
			{ 206.0 209.0 0.0 }
			{ 206.0 209.0 0.0 }
			{ 135.0 135.0 135.0 }
		}
	}
}

usa_perot_meritorious_service_award = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = USA
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		has_completed_focus = USA_reformed_people_reformed_nation_reforming_the_era
		custom_trigger_tooltip = {
			tooltip = has_neutral_conservatism_government_TT
			is_in_array = { ruling_party = 13 }
		}
	}


	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 227.0 227.0 227.0 }
			{ 77.0 90.0 189.0 }
			{ 77.0 90.0 189.0 }
			{ 135.0 135.0 135.0 }
		}
	}
}

usa_green_grenadier = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = USA
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		has_completed_focus = USA_a_green_future_for_all
		custom_trigger_tooltip = {
			tooltip = has_neutral_green_government_TT
			is_in_array = { ruling_party = 17 }
		}
		}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 227.0 227.0 227.0 }
			{ 7.0 133.0 0.0 1.0 }
			{ 7.0 133.0 0.0 1.0 }
			{ 138.0 138.0 138.0 }
		}
	}
}

usa_roosevelt_runner = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = USA
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		has_completed_focus = USA_the_drug_policies_reforms
		custom_trigger_tooltip = {
			tooltip = has_socialism_government_TT
			is_in_array = { ruling_party = 3 }
		}
		}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 227.0 227.0 227.0 }
			{ 255.0 82.0 102.0 1.0 }
			{ 255.0 82.0 102.0 1.0 }
			{ 138.0 138.0 138.0 }
		}
	}
}

fij_innovation_station = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = FIJ
	}

	happened = {
		date > 1999.01.02
		has_idea = superpower
		has_completed_focus = fiji_focus_technate_council_emerges
		custom_trigger_tooltip = {
			tooltip = has_oligarchism_government_TT
			is_in_array = { ruling_party = 15 }
		}
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 0 195 255 }
			{ 145 142 142 }
			{ 219 24 24 }
			{ 186 186 186 }
		}
	}
}

ger_capture_poland_again = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = GER
	}

	happened = {
		date > 1999.01.02
		AND = {
			owns_state = 474
			owns_state = 625
			owns_state = 626
			owns_state = 111
			owns_state = 112
			owns_state = 113
			owns_state = 114
			owns_state = 115
			owns_state = 116
			owns_state = 433
			owns_state = 628
			owns_state = 1014
			NOT = { country_exists = POL }
		}
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 45.0 64.0 102.0 1.0 }
			{ 180.0 73.0 107.0 1.0 }
			{ 155.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

take_the_riau_islands = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = SIN
	}

	happened = {
		date > 2000.1.01
		controls_state = 627
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 68.0 64.0 102.0 1.0 }
			{ 190.0 73.0 107.0 1.0 }
			{ 155.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

the_federation_of_malaya = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = SIN
	}

	happened = {
		date > 2000.1.1
		MAY = { is_subject_of = SIN }
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 92.0 64.0 102.0 1.0 }
			{ 190.0 73.0 107.0 1.0 }
			{ 140.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

the_financial_center_of_asia = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = SIN
	}

	happened = {
		date > 2000.1.1
		custom_trigger_tooltip = {
			tooltip = the_financial_center_of_asia_tt
			check_variable = { office_park_total > 19 }
		}
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 68.0 64.0 102.0 1.0 }
			{ 190.0 73.0 107.0 1.0 }
			{ 155.0 189.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

end_the_first_sudanese_war = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		OR = {
			original_tag = SUD
			original_tag = SSU
		}
	}

	happened = {
		date > 2000.1.1
		has_global_flag = Sudan_Civil_War_Over
	}

	ribbon = {
		frames = { 1 1 1 }
		colors = {
			{ 98.0 64.0 94.0 1.0 }
			{ 120.0 73.0 78.0 1.0 }
			{ 211.0 181.0 56.0 1.0 }
		}
	}
}

end_the_nepalese_civil_war = {
	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		OR = {
			original_tag = NPM
			original_tag = NEP
		}
	}

	happened = {
		date > 2000.1.1
		has_global_flag = nep_truth_civil_war_Ending
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 98.0 64.0 102.0 1.0 }
			{ 120.0 73.0 107.0 1.0 }
			{ 45.0 209.0 96.0 1.0 }
			{ 211.0 181.0 128.0 1.0 }
		}
	}
}

#Libya
libya_order_of_republic = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = LBA
	}

	happened = {
		date > 2020.1.1
	}

	ribbon = {
		frames = { 1 4 4 1 }
		colors = {
			{ 192 0 0 1.0 }
			{ 0 0 0 1.0 }
			{ 255 255 255 1.0 }
			{ 0 0 0 0 }
		}
	}
}

libya_order_of_jihad = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = LBA
	}

	happened = {
		check_variable = { gdp_per_capita > 50 }
	}

	ribbon = {
		frames = { 1 1 1 4 }
		colors = {
			{ 0 128 0 1 }
			{ 0 0 0 0 }
			{ 0 0 0 0 }
			{ 192 0 0 1 }
		}
	}
}

libya_order_of_grand_conqueror = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = LBA
	}

	happened = {
		any_other_country = {
			is_in_faction_with = ROOT
			OR = {
				has_idea = superpower
				has_idea = great_power
			}
		}
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 0 128 0 1 }
			{ 0 0 0 0 }
			{ 0 0 0 0 }
			{ 0 0 0 0 }
		}
	}
}

libya_medal_of_merit = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = LBA
	}

	happened = {
		has_country_flag = ribbon_libya_victory_over_egypt
		has_country_flag = ribbon_libya_victory_over_chad
	}

	ribbon = {
		frames = { 1 4 1 4 }
		colors = {
			{ 0 128 0 1 }
			{ 192 0 0 1 }
			{ 0 0 0 1 }
			{ 255 255 255 1 }
		}
	}
}

#Botswana
botswana_naledi_ya_botswana = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = BOT
	}

	happened = {
		has_idea = low_level_of_aids
	}

	ribbon = {
		frames = { 1 3 1 1 }
		colors = {
			{ 255 255 255 1 }
			{ 0 153 255 1 }
			{ 0 0 0 1 }
			{ 0 0 0 0 }
		}
	}
}

botswana_conduct_valour_cross = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = BOT
	}

	happened = {
		any_enemy_country = {
			strength_ratio = {
				tag = BOT
				ratio > 5
			}
		}
	}

	ribbon = {
		frames = { 1 4 1 4 }
		colors = {
			{ 255 0 0 1 }
			{ 0 128 0 1 }
			{ 0 0 0 0 }
			{ 255 255 255 1 }
		}
	}
}

botswana_medal_for_meritorious_service = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = BOT
	}

	happened = {
		NOT = {
			has_idea = BOT_zimbabwean_refugees
			has_idea = BOT_zimbabwean_refugees_encouraged
		}
	}

	ribbon = {
		frames = { 1 1 1 4 }
		colors = {
			{ 204 51 102 1 }
			{ 255 204 0 1 }
			{ 0 0 0 0 }
			{ 255 204 0 1 }
		}
	}
}

botswana_teachers_silver_jubilee_medal = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = BOT
	}

	happened = {
		check_variable = { literacy_rate > 0.9 }
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 0 0 0 1 }
			{ 0 0 0 0 }
			{ 255 255 255 1 }
			{ 0 0 0 0 }
		}
	}
}

czech_conquered_kaliningrad_medal = {

	possible = {
		difficulty > 1
		has_any_custom_difficulty_setting = no
		game_rules_allow_achievements = yes
		original_tag = CZE
	}

	happened = {
		642 = {
			is_fully_controlled_by = CZE
		}
	}

	ribbon = {
		frames = { 1 1 1 1 }
		colors = {
			{ 236 26 35 1 }
			{ 255 210 17 1 }
			{ 40 53 149 1 }
			{ 255 255 255 1 }
		}
	}
}