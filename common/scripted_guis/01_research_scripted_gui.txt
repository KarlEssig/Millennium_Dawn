scripted_gui = {
	open_research_window = {
		window_name = "open_research_slot_gui"
		context_type = player_context
		parent_window_token = technology_tab

		effects = {
			open_research_slot_button_click = {
				if = {
					limit = { NOT = { has_country_flag = open_research_screen } }
					set_country_flag = open_research_screen
				}
				else = {
					clr_country_flag = open_research_screen
				}
			}
		}
	}

	MD_research_slot_gui = {
		window_name = "research_slot_gui_screen"
		context_type = player_context
		parent_window_token = technology_tab

		visible = {
			ROOT = {
				OR = {
					is_ai = yes
					has_country_flag = open_research_screen
				}
			}
		}

		triggers = {
			gdp_slot_gain_industry_size_click_enabled = {
				check_variable = { next_slot_from_gdp < 10 }
				NOT = { has_decision = gdp_gain_slot }
				custom_trigger_tooltip = {
					tooltip = gdp_gain_industry_size_flag_tt
					NOT = { has_country_flag = gdp_gain_industry_slot_flag }
				}
				custom_trigger_tooltip = {
					tooltip = gdp_gain_slot.tt
					check_variable = { gdp_total > GLOBAL.gdp_slot^next_slot_from_gdp }
				}
			}
			gdp_slot_gain_developmental_size_click_enabled = {
				check_variable = { next_slot_from_gdp_c < 7 }
				NOT = { has_decision = gdp_c_gain_slot }
				custom_trigger_tooltip = {
					tooltip = gdp_gain_dev_size_flag_tt
					NOT = { has_country_flag = gdp_gain_developmental_size_flag }
				}
				custom_trigger_tooltip = {
					tooltip = gdp_c_gain_slot.tt
					check_variable = { gdp_per_capita > GLOBAL.gdp_c_slot^next_slot_from_gdp_c }
				}
			}
		}
		effects = {
			gdp_slot_gain_industry_size_click = {
				custom_effect_tooltip = once_the_decision_completes_rs_tt
				effect_tooltip = {
					add_research_slot = 1
					add_to_variable = { research_slots_from_gdp = 1 }
					add_to_variable = { next_slot_from_gdp = 1 }
					medium_expenditure = yes
					calculate_new_education_spending_tooltip = yes
				}
				line = yes
				set_country_flag = { flag = gdp_gain_industry_slot_flag value = 1 days = 240 }
				activate_decision = gdp_gain_slot
			}
			gdp_slot_gain_developmental_size_click = {
				custom_effect_tooltip = once_the_decision_completes_rs_tt
				effect_tooltip = {
					add_research_slot = 1
					add_to_variable = { research_slots_from_gdp_c = 1 }
					add_to_variable = { next_slot_from_gdp_c = 1 }
					medium_expenditure = yes
					calculate_new_education_spending_tooltip = yes
				}
				line = yes
				set_country_flag = { flag = gdp_gain_developmental_size_flag value = 1 days = 240 }
				activate_decision = gdp_c_gain_slot
			}
		}

	}
}
