scripted_gui = {
	union_state_gui = {
		context_type = decision_category
		window_name = "union_state_ui_window"
		visible = { always = yes }
	}
	wagner_ui = {
		context_type = decision_category
		window_name = "wagner_ui_window"
		visible = { always = yes }
	}
	rusmvd_gui = {
		context_type = decision_category
		window_name = "rusmvd_ui_window"
		visible = { always = yes }
	}
	rusarmy_gui = {
		context_type = decision_category
		window_name = "rusarmy_ui_window"
		visible = { always = yes }
	}
	russovfed_gui = {
		context_type = decision_category
		window_name = "russovfed_ui_window"
		visible = { always = yes }
	}
	ruseconomic_gui = {
		context_type = decision_category
		window_name = "ruseconomic_ui_window"
		visible = { always = yes }
	}
	pmrcouncil_gui = {
		context_type = decision_category
		window_name = "pmrcouncil_ui_window"
		visible = { always = yes }
	}
}
