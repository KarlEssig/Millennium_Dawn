scripted_gui = {

	IRQ_pmf_recovery = {
		window_name = "IRQ_pmf_recovery_container"
		context_type = decision_category

		properties = {
			pmf_recovery_progress = { frame = IRQ.pmf_recovery_status }
		}
	}

	Iraq_civil_war_gui = {
		context_type = decision_category
		window_name = "Iraq_civil_war_decision_ui_window"
		dirty = global.num_days
		ai_enabled = { always = yes }
		ai_check = { always = yes }

		# triggers = {
		# 	IRQ_basrah_button_click_enabled = {
		# 		has_country_flag = IRQ_war_with_shias
		# 	}
		# }

		effects = {
			IRQ_anbar_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						168 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_anbar_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_najaf_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						169 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_najaf_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_karbala_button_click = {
				if = {
					limit = {
						has_country_flag = IRQ_war_with_shias
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						1143 = {
							OR = {
								has_state_flag = IRQ_cw_shia_control
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_karbala_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_muthanna_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						1122 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_muthanna_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_sinjar_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						641 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_sinjar_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_nineveh_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						166 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_nineveh_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_wasit_button_click = {
				if = {
					limit = {
						has_country_flag = IRQ_war_with_shias
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						1141 = {
							OR = {
								has_state_flag = IRQ_cw_shia_control
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_wasit_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_diyala_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						170 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_diyala_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_baghdad_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						557 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_babylon_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_basrah_button_click = {
				if = {
					limit = {
						has_country_flag = IRQ_war_with_shias
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						171 = {
							OR = {
								has_state_flag = IRQ_cw_shia_control
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_basrah_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_salahuddin_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						167 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_salahuddin_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_dhi_qar_button_click = {
				if = {
					limit = {
						has_country_flag = IRQ_war_with_shias
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						1123 = {
							OR = {
								has_state_flag = IRQ_cw_shia_control
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_dhi_qar_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_qadisiyah_button_click = {
				if = {
					limit = {
						has_country_flag = IRQ_war_with_shias
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						1142 = {
							OR = {
								has_state_flag = IRQ_cw_shia_control
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_qadisiyah_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_maysan_button_click = {
				if = {
					limit = {
						has_country_flag = IRQ_war_with_shias
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						1124 = {
							OR = {
								has_state_flag = IRQ_cw_shia_control
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_maysan_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
			IRQ_kirkuk_button_click = {
				if = {
					limit = {
						NOT = {
							IRQ_is_selected_state_iraq = yes
						}
						165 = {
							OR = {
								has_state_flag = IRQ_high_insurgent_presence
								has_state_flag = IRQ_medium_insurgent_presence
							}
						}
					}
					set_country_flag = IRQ_kirkuk_selected
					IRQ_conduct_attack = yes
					set_variable = { IRQ_count_down_var = 10 }
				}
			}
		}

		properties = {
			IRQ_anbar_button = {
				image = "[IRQ_anbar_status]"
			}
			IRQ_karbala_button = {
				image = "[IRQ_karbala_status]"
			}
			IRQ_najaf_button = {
				image = "[IRQ_najaf_status]"
			}
			IRQ_muthanna_button = {
				image = "[IRQ_muthanna_status]"
			}
			IRQ_sinjar_button = {
				image = "[IRQ_sinjar_status]"
			}
			IRQ_nineveh_button = {
				image = "[IRQ_nineveh_status]"
			}
			IRQ_diyala_button = {
				image = "[IRQ_diyala_status]"
			}
			IRQ_wasit_button = {
				image = "[IRQ_wasit_status]"
			}
			IRQ_qadisiyah_button = {
				image = "[IRQ_qadisiyah_status]"
			}
			IRQ_baghdad_button = {
				image = "[IRQ_baghdad_status]"
			}
			IRQ_basrah_button = {
				image = "[IRQ_basrah_status]"
			}
			IRQ_salahuddin_button = {
				image = "[IRQ_salahuddin_status]"
			}
			IRQ_dhi_qar_button = {
				image = "[IRQ_dhi_qar_status]"
			}
			IRQ_maysan_button = {
				image = "[IRQ_maysan_status]"
			}
			IRQ_kirkuk_button = {
				image = "[IRQ_kirkuk_status]"
			}
		}

		ai_weights = {
			# Make the AI clicky buttony
			IRQ_anbar_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							168.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							168.IRQ_jihadist_troops > 700
						}
					}
				}
			}
			# Najaf
			IRQ_najaf_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							169.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							169.IRQ_jihadist_troops > 700
						}
					}
				}
			}
			# Karbala
			IRQ_karbala_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							1143.IRQ_shiite_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							1143.IRQ_shiite_troops > 700
						}
					}
				}
			}
			# Muthanna
			IRQ_muthanna_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							1122.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							1122.IRQ_jihadist_troops > 700
						}
					}
				}
			}
			# sinjar
			IRQ_sinjar_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							641.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							641.IRQ_jihadist_troops > 700
						}
					}
				}
			}
			# Nineveh
			IRQ_nineveh_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							166.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							166.IRQ_jihadist_troops > 700
						}
					}
				}
			}
			# Wasit
			IRQ_wasit_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							1141.IRQ_shiite_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							1141.IRQ_shiite_troops > 700
						}
					}
				}
			}
			# Diyala
			IRQ_diyala_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							170.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							170.IRQ_jihadist_troops > 700
						}
					}
				}
			}
			# Baghdad
			IRQ_baghdad_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							557.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							557.IRQ_jihadist_troops > 700
						}
					}
				}
			}
			# Basrah
			IRQ_basrah_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							171.IRQ_shiite_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							171.IRQ_shiite_troops > 700
						}
					}
				}
			}
			# Salahuddin
			IRQ_salahuddin_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							167.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							167.IRQ_jihadist_troops > 700
						}
					}
				}
			}
			# Dhi-Qar
			IRQ_dhi_qar_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							1123.IRQ_shiite_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							1123.IRQ_shiite_troops > 700
						}
					}
				}
			}
			# Qadisiyah
			IRQ_qadisiyah_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							1142.IRQ_shiite_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							1142.IRQ_shiite_troops > 700
						}
					}
				}
			}
			# Maysan
			IRQ_maysan_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							1124.IRQ_shiite_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							1124.IRQ_shiite_troops > 700
						}
					}
				}
			}
			# Kirkuk
			IRQ_kirkuk_button_click = {
				ai_will_do = {
					factor = 40
					modifier = {
						add = -50
						check_variable = {
							165.IRQ_jihadist_troops < 700
						}
					}
					modifier = {
						add = 100
						check_variable = {
							165.IRQ_jihadist_troops > 700
						}
					}
				}
			}
		}
	}
}

