scripted_gui = {
	Iran_civil_war_gui = {
		context_type = decision_category
		window_name = "Iran_civil_war_decision_ui_window"
		dirty = global.num_days

		triggers = {
			GFX_iranian_azerbaijan_red_visible = { NOT = { 407 = { has_state_flag = PER_locals_swayed } } }
			GFX_iranian_azerbaijan_green_visible = { 407 = { has_state_flag = PER_locals_swayed } }
			GFX_kordestan_red_visible = { NOT = { 408 = { has_state_flag = PER_locals_swayed } } }
			GFX_kordestan_green_visible = { 408 = { has_state_flag = PER_locals_swayed } }
			GFX_greater_luristan_red_visible = { NOT = { 406 = { has_state_flag = PER_locals_swayed } } }
			GFX_greater_luristan_green_visible = { 406 = { has_state_flag = PER_locals_swayed } }
			GFX_sistan_and_baluchestan_red_visible = { NOT = { 401 = { has_state_flag = PER_locals_swayed } } }
			GFX_sistan_and_baluchestan_green_visible = { 401 = { has_state_flag = PER_locals_swayed } }
			GFX_hormuzgan_red_visible = { NOT = { 978 = { has_state_flag = PER_locals_swayed } } }
			GFX_hormuzgan_green_visible = { 978 = { has_state_flag = PER_locals_swayed } }
			GFX_khuzestan_red_visible = { NOT = { 399 = { has_state_flag = PER_locals_swayed } } }
			GFX_khuzestan_green_visible = { 399 = { has_state_flag = PER_locals_swayed } }
			GFX_fars_red_visible = { NOT = { 400 = { has_state_flag = PER_locals_swayed } } }
			GFX_fars_green_visible = { 400 = { has_state_flag = PER_locals_swayed } }
			GFX_khoy_red_visible = { NOT = { 1039 = { has_state_flag = PER_locals_swayed } } }
			GFX_khoy_green_visible = { 1039 = { has_state_flag = PER_locals_swayed } }
			GFX_isfahan_red_visible = { NOT = { 976 = { has_state_flag = PER_locals_swayed } } }
			GFX_isfahan_green_visible = { 976 = { has_state_flag = PER_locals_swayed } }
			GFX_west_azerbaijan_red_visible = { NOT = { 979 = { has_state_flag = PER_locals_swayed } } }
			GFX_west_azerbaijan_green_visible = { 979 = { has_state_flag = PER_locals_swayed } }
			GFX_mazandaran_red_visible = { NOT = { 403 = { has_state_flag = PER_locals_swayed } } }
			GFX_mazandaran_green_visible = { 403 = { has_state_flag = PER_locals_swayed } }
			GFX_gilan_red_visible = { NOT = { 1073 = { has_state_flag = PER_locals_swayed } } }
			GFX_gilan_green_visible = { 1073 = { has_state_flag = PER_locals_swayed } }
			GFX_qom_green_visible = { 1144 = { has_state_flag = PER_locals_swayed } }
			GFX_qom_red_visible = { 1144 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_markazi_green_visible = { 404 = { has_state_flag = PER_locals_swayed } }
			GFX_markazi_red_visible = { 404 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_hamadan_green_visible = { 1148 = { has_state_flag = PER_locals_swayed } }
			GFX_hamadan_red_visible = { 1148 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_kermanshah_green_visible = { 1139 = { has_state_flag = PER_locals_swayed } }
			GFX_kermanshah_red_visible = { 1139 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_ilam_green_visible = { 1138 = { has_state_flag = PER_locals_swayed } }
			GFX_ilam_red_visible = { 1138 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_bushehr_green_visible = { 1146 = { has_state_flag = PER_locals_swayed } }
			GFX_bushehr_red_visible = { 1146 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_kerman_green_visible = { 977 = { has_state_flag = PER_locals_swayed } }
			GFX_kerman_red_visible = { 977 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_yazd_green_visible = { 1147 = { has_state_flag = PER_locals_swayed } }
			GFX_yazd_red_visible = { 1147 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_south_khorasan_green_visible = { 1151 = { has_state_flag = PER_locals_swayed } }
			GFX_south_khorasan_red_visible = { 1151 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_razavi_khorasan_green_visible = { 402 = { has_state_flag = PER_locals_swayed } }
			GFX_razavi_khorasan_red_visible = { 402 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_north_khorasan_green_visible = { 1150 = { has_state_flag = PER_locals_swayed } }
			GFX_north_khorasan_red_visible = { 1150 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_golestan_green_visible = { 1145 = { has_state_flag = PER_locals_swayed } }
			GFX_golestan_red_visible = { 1145 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_semnan_green_visible = { 1149 = { has_state_flag = PER_locals_swayed } }
			GFX_semnan_red_visible = { 1149 = { NOT = { has_state_flag = PER_locals_swayed } } }
			GFX_tehran_red_visible = { NOT = { 405 = { has_state_flag = PER_locals_swayed } } }
			GFX_tehran_green_visible = { 405 = { has_state_flag = PER_locals_swayed } }
		}
		effects = {
			GFX_iranian_azerbaijan_red_button_click = {
				set_country_flag = PER_initiating_attack
			}
		}
	}
	Iran_cold_war_gui = {
		context_type = decision_category
		window_name = "Iran_cold_war_decision_ui_window"
		dirty = global.num_days

		triggers = {
			GFX_saudi_arabia_no_influence_visible = { check_variable = { from.saudi_anger > 0 } }
			GFX_saudi_arabia_low_influence_visible = { check_variable = { from.saudi_anger > 0.20 } }
			GFX_saudi_arabia_medium_influence_visible = { check_variable = { from.saudi_anger > 0.40 } }
			GFX_saudi_arabia_high_influence_visible = { check_variable = { from.saudi_anger > 0.60 } }
			GFX_uae_no_influence_visible = { check_variable = { from.saudi_anger > 0 } }
			GFX_uae_low_influence_visible = { check_variable = { from.saudi_anger > 0.20 } }
			GFX_uae_medium_influence_visible = { check_variable = { from.saudi_anger > 0.40 } }
			GFX_uae_high_influence_visible = { check_variable = { from.saudi_anger > 0.60 } }
			GFX_kuwait_no_influence_visible = { check_variable = { from.saudi_anger > 0 } }
			GFX_kuwait_low_influence_visible = { check_variable = { from.saudi_anger > 0.20 } }
			GFX_kuwait_medium_influence_visible = { check_variable = { from.saudi_anger > 0.40 } }
			GFX_kuwait_high_influence_visible = { check_variable = { from.saudi_anger > 0.60 } }
			GFX_oman_no_influence_visible = { check_variable = { from.saudi_anger > 0 } }
			GFX_oman_low_influence_visible = { check_variable = { from.saudi_anger > 0.20 } }
			GFX_oman_medium_influence_visible = { check_variable = { from.saudi_anger > 0.40 } }
			GFX_oman_high_influence_visible = { check_variable = { from.saudi_anger > 0.60 } }
			GFX_qatar_no_influence_visible = { check_variable = { from.saudi_anger > 0 } }
			GFX_qatar_low_influence_visible = { check_variable = { from.saudi_anger > 0.20 } }
			GFX_qatar_medium_influence_visible = { check_variable = { from.saudi_anger > 0.40 } }
			GFX_qatar_high_influence_visible = { check_variable = { from.saudi_anger > 0.60 } }
		}
	}
	Iran_prime_minister_gui = {
		context_type = decision_category
		window_name = "Iran_prime_minister_decision_ui_window"
		dirty = iranian_president_change

		triggers = {
			GFX_seyed_hassan_amin_visible = { has_country_flag = PER_seyed_hassan_minister } # 1
			GFX_c_hashem_sabbaghian_visible = { has_country_flag = PER_hashem_sabbaghian_minister } # 2
			GFX_c_hedayatollah_matin_daftari_visible = { has_country_flag = PER_hedayatollah_matin_minister } # 3
			GFX_c_mohammad_tavasoli_visible = { has_country_flag = PER_mohammad_tavasoli_minister } # 4
			GFX_c_mostafa_moein_visible = { has_country_flag = PER_mostafa_moein_minister } # 5
			GFX_c_mostafa_tajzade_visible = { has_country_flag = PER_mostafa_tajzade_minister } # 6
			GFX_l_ali_khavari_visible = { has_country_flag = PER_ali_khavari_minister } # 8
			GFX_l_hashem_aghajari_visible = { has_country_flag = PER_hashem_aghajari_minister } # 9
			GFX_l_mohsen_makhmalbaf_visible = { has_country_flag = PER_mohsen_makhmalbaf_minister } # 11
			GFX_lib_abdolali_bazargan_visible = { has_country_flag = PER_abdolali_bazargan_minister } # 13
			GFX_lib_amir_etemadi_visible = { has_country_flag = PER_amir_etemadi_minister } # 14
			GFX_lib_hassan_shariatmadari_visible = { has_country_flag = PER_hassan_shariatmadari_minister } # 16
			GFX_lib_roozbeh_farahinpour_visible = { has_country_flag = PER_roozbeh_farahinpour_minister } # 18
			GFX_r_abulqasem_porhashmi_visible = { has_country_flag = PER_abulqasem_porhashmi_minister } # Pan Iranist # 19
			GFX_r_alireza_kiani_visible = { has_country_flag = PER_alireza_kiani_minister } # Monarchist # 20
			GFX_r_foad_pashei_visible = { has_country_flag = PER_foad_pashei_minister } # Monarchist - Not allowed to be picked
			GFX_r_ezatollah_sahabi_visible = { has_country_flag = PER_ezatollah_sahabi_minister } # Monarchist # 21
			GFX_r_reza_pirzadeh_visible = { has_country_flag = PER_reza_pirzadeh_minister } # Monarchist # 22
			PER_no_pm_visible = { NOT = { has_country_flag = PER_appointed_pm } }
		}
	}
	iranic_unity_gui = {
		context_type = decision_category
		window_name = "Iran_iranic_unity_ui_window"
		dirty = global.num_days

		triggers = {
			GFX_tajikistan_red_visible = { check_variable = { from.iranic_tajik > 0 } }
			GFX_tajikistan_yellow_visible = { check_variable = { from.iranic_tajik > 0.49 } }
			GFX_tajikistan_green_visible = { AND = { 723 = { is_core_of = PER } 724 = { is_core_of = PER } 722 = { is_core_of = PER } } }
			GFX_afghanistan_red_visible = { check_variable = { from.iranic_afghan > 0 } }
			GFX_afghanistan_yellow_visible = { check_variable = { from.iranic_afghan > 0.49 } }
			GFX_afghanistan_green_visible = { AND = { 411 = { is_core_of = PER } 410 = { is_core_of = PER } 412 = { is_core_of = PER } 414 = { is_core_of = PER } 415 = { is_core_of = PER } 409 = { is_core_of = PER } } }
		}
	}
}

