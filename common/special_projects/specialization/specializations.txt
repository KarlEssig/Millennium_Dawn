# Author(s): Doolittle, AngriestBIrd

# specialization_nuclear is going to be Civilian R&D making it a catch all for everything
specialization_nuclear = {
	blueprint_image = GFX_generic_plane_blueprint_background
	color = { 255 255 0 } #YELLOW
	program_background = "GFX_sp_facility_card_bg"
}

# Naval Engineering Facility
specialization_naval = {
	color = { 0 0 255 }
	program_background = "GFX_sp_facility_card_bg"
}

# Avionics & Aeronautics Facility
specialization_air = {
	color = { 255 0 0 }
	program_background = "GFX_sp_facility_card_bg"
}

# Land Warfare Facility
specialization_land = {
	color = { 0 255 0 }
	program_background = "GFX_sp_facility_card_bg"
}
