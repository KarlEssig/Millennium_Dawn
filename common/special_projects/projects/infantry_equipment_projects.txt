# Author(s): Kalkalash
#Special projects related to infantry equipment tech tab
sp_caseless_ammunition = {
	specialization = specialization_land

	project_tags = sp_tag_infantry_equipment

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			has_tech = infantry_weapons1
		}
	}

	available = {
		FROM = {
			has_tech = infantry_weapons4
		}
	}

	breakthrough_cost = {
		specialization_land = 2
	}

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	project_output = {
		equipment_bonus = {
			Inf_equipment = {
				build_cost_ic = -0.10
				reliability = -0.04
				instant = yes
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_infantry_tech_1
		sp_land_generic_reward_infantry_tech_2
		sp_land_generic_reward_infantry_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_caseless_ammunition_reward_new_propellant = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_caseless_ammunition_reward_new_propellant_option

				iteration_output = {
					equipment_bonus = {
						Inf_equipment = {
							reliability = 0.025
						}
					}

					country_effects = {
						FROM = {
							add_project_progress_ratio = constant:sp_progress.gain.low
						}
					}
				}
			}
		}
	}

}

sp_fire_support_land_drone = {
	specialization = specialization_land

	project_tags = sp_tag_infantry_equipment

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			OR = {
				has_tech = land_Drone_equipment2
				has_tech = support_weapons4
			}
		}
	}

	available = {
		FROM = {
			has_tech = land_Drone_equipment3
		}
	}

	breakthrough_cost = {
		specialization_land = 2
	}

	prototype_time = sp_time.prototype.medium

	complexity = sp_complexity.medium

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_TECH
				TECH = rc_fire_support_drone
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_infantry_tech_1
		sp_land_generic_reward_infantry_tech_2
		sp_land_generic_reward_infantry_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_fire_support_land_drone_reward_improved_communication = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_fire_support_land_drone_reward_improved_communication_civilian_application

				iteration_output = {
					country_effects = {
						add_tech_bonus = {
							name = sp_fire_support_land_drone_reward_improved_communication_tech_bonus
							bonus = 0.15
							uses = 1
							category = CAT_cnc
							category = CAT_l_drone
						}
					}
				}
			}

			option = {
				token = sp_fire_support_land_drone_reward_improved_communication_military_application

				iteration_output = {
					country_effects = {
						add_tech_bonus = {
							name = sp_fire_support_land_drone_reward_improved_communication_tech_bonus
							bonus = 0.15
							uses = 1
							category = CAT_computing_tech
							category = CAT_internet_tech
						}
					}
				}
			}
		}
	}

}

sp_tactical_augmented_reality = {
	specialization = specialization_land

	project_tags = sp_tag_infantry_equipment

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			OR = {
				has_tech = command_control_equipment3
				has_tech = night_vision_4
			}
		}
	}

	available = {
		FROM = {
			has_tech = command_control_equipment4
		}
	}

	breakthrough_cost = {
		specialization_land = 2
	}

	prototype_time = sp_time.prototype.medium

	complexity = sp_complexity.medium

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_TECH
				TECH = augmented_vision_1
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_infantry_tech_1
		sp_land_generic_reward_infantry_tech_2
		sp_land_generic_reward_infantry_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_tactical_augmented_reality_reward_civilian_interest = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_tactical_augmented_reality_reward_civilian_interest_civ_factory

				iteration_output = {
					facility_state_effects = {
						add_building_construction = {
							type = industrial_complex
							level = 1
							instant_build = yes
						}
					}
				}
			}

			option = {
				token = sp_tactical_augmented_reality_reward_civilian_interest_mil_factory

				iteration_output = {
					facility_state_effects = {
						add_building_construction = {
							type = arms_factory
							level = 1
							instant_build = yes
						}
					}
				}
			}
		}
	}

}

sp_double_shot_rifle = {
	specialization = specialization_land

	project_tags = sp_tag_infantry_equipment

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			has_tech = special_forces2
		}
	}

	available = {
		FROM = {
			has_tech = special_forces4
		}
	}

	breakthrough_cost = {
		specialization_land = 2
	}

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	project_output = {
		country_effects = {
			custom_effect_tooltip = sp_double_shot_rifle_tt
			hidden_effect = {
				set_technology = {
					sp_double_shot_rifle_tech = 1
					popup = no
				}
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_infantry_tech_1
		sp_land_generic_reward_infantry_tech_2
		sp_land_generic_reward_infantry_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_double_shot_rifle_reward_double_shot_type = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_double_shot_rifle_reward_double_shot_type_double_barrel

				iteration_output = {
					sub_unit_bonus = {
						L_Air_Inf_Bat = {
							defense = 0.05
						}
						Mot_Air_Inf_Bat = {
							defense = 0.05
						}
						Mech_Air_Inf_Bat = {
							defense = 0.05
						}
						Arm_Air_Inf_Bat = {
							defense = 0.05
						}
						L_Marine_Bat = {
							defense = 0.05
						}
						L_Marine_Cdo_Bat = {
							defense = 0.05
						}
						Mot_Marine_Bat = {
							defense = 0.05
						}
						Mech_Marine_Bat = {
							defense = 0.05
						}
						Arm_Marine_Bat = {
							defense = 0.05
						}
						L_Air_assault_Bat = {
							defense = 0.05
						}
						Arm_Air_assault_Bat = {
							defense = 0.05
						}
						Special_Forces = {
							defense = 0.05
						}
					}
				}
			}

			option = {
				token = sp_double_shot_rifle_reward_double_shot_type_pulley

				iteration_output = {
					sub_unit_bonus = {
						L_Air_Inf_Bat = {
							breakthrough = 0.05
						}
						Mot_Air_Inf_Bat = {
							breakthrough = 0.05
						}
						Mech_Air_Inf_Bat = {
							breakthrough = 0.05
						}
						Arm_Air_Inf_Bat = {
							breakthrough = 0.05
						}
						L_Marine_Bat = {
							breakthrough = 0.05
						}
						L_Marine_Cdo_Bat = {
							breakthrough = 0.05
						}
						Mot_Marine_Bat = {
							breakthrough = 0.05
						}
						Mech_Marine_Bat = {
							breakthrough = 0.05
						}
						Arm_Marine_Bat = {
							breakthrough = 0.05
						}
						L_Air_assault_Bat = {
							breakthrough = 0.05
						}
						Arm_Air_assault_Bat = {
							breakthrough = 0.05
						}
						Special_Forces = {
							breakthrough = 0.05
						}
					}
				}
			}
		}
		sp_double_shot_rifle_reward_improved_firing_rate = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_double_shot_rifle_reward_improved_firing_rate_offence

				iteration_output = {
					equipment_bonus = {
						Inf_equipment = {
							soft_attack = 0.02
						}
					}
				}
			}

			option = {
				token = sp_double_shot_rifle_reward_improved_firing_rate_defence

				iteration_output = {
					equipment_bonus = {
						Inf_equipment = {
							defense = 0.02
						}
					}
				}
			}
		}
	}

}

sp_experimental_exoskeleton = {
	specialization = specialization_land

	project_tags = sp_tag_infantry_equipment

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			OR = {
				has_tech = body_armor_2010
				has_tech = advanced_industrial_robotics
			}
		}
	}

	available = {
		FROM = {
			has_tech = body_armor_2010
			has_tech = advanced_industrial_robotics
		}
	}

	breakthrough_cost = {
		specialization_land = 2
	}

	prototype_time = sp_time.prototype.medium

	complexity = sp_complexity.medium

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_TECH
				TECH = exoskeleton_1
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_infantry_tech_1
		sp_land_generic_reward_infantry_tech_2
		sp_land_generic_reward_infantry_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_experimental_exoskeleton_reward_engineers = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_experimental_exoskeleton_reward_engineers_defence

				iteration_output = {
					sub_unit_bonus = {
						L_Engi_Comp = {
							entrenchment = 0.50
						}
						H_Engi_Comp = {
							entrenchment = 0.50
						}
					}
				}
			}

			option = {
				token = sp_experimental_exoskeleton_reward_engineers_recovery

				iteration_output = {
					sub_unit_bonus = {
						L_Engi_Comp = {
							equipment_capture_factor = 0.05
						}
						H_Engi_Comp = {
							equipment_capture_factor = 0.05
						}
					}
				}
			}
		}
	}

}

sp_active_camouflage = {
	specialization = specialization_land

	project_tags = sp_tag_infantry_equipment

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			has_tech = camouflage3
		}
	}

	available = {
		FROM = {
			has_tech = camouflage4
		}
	}

	breakthrough_cost = {
		specialization_land = 2
	}

	prototype_time = sp_time.prototype.medium

	complexity = sp_complexity.medium

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_TECH
				TECH = camouflage5
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_infantry_tech_1
		sp_land_generic_reward_infantry_tech_2
		sp_land_generic_reward_infantry_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_active_camouflage_reward_vehicle_protection = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_active_camouflage_reward_vehicle_protection_ground

				iteration_output = {
					sub_unit_bonus = {
						Mech_Inf_Bat = {
							defense = 0.03
						}
						Arm_Inf_Bat = {
							defense = 0.03
						}
						Mech_Air_Inf_Bat = {
							defense = 0.03
						}
						Arm_Air_Inf_Bat = {
							defense = 0.03
						}
						Mech_Marine_Bat = {
							defense = 0.03
						}
						Arm_Marine_Bat = {
							defense = 0.03
						}
						Arm_Air_assault_Bat = {
							defense = 0.03
						}
						armor_Bat = {
							defense = 0.03
						}
						armor_Comp = {
							defense = 0.03
						}
						L_arm_Bat = {
							defense = 0.03
						}
						SP_Arty_Bat = {
							defense = 0.03
						}
						SP_Arty_Battery = {
							defense = 0.03
						}
						SP_AA_Bat = {
							defense = 0.03
						}
						SP_AA_Battery = {
							defense = 0.03
						}
						Mech_Recce_Comp = {
							defense = 0.03
						}
						Arm_Recce_Comp = {
							defense = 0.03
						}
						armor_Recce_Comp = {
							defense = 0.03
						}
					}
				}
			}

			option = {
				token = sp_active_camouflage_reward_vehicle_protection_ground_air

				iteration_output = {
					sub_unit_bonus = {
						Mech_Inf_Bat = {
							air_attack = 0.03
						}
						Arm_Inf_Bat = {
							air_attack = 0.03
						}
						Mech_Air_Inf_Bat = {
							air_attack = 0.03
						}
						Arm_Air_Inf_Bat = {
							air_attack = 0.03
						}
						Mech_Marine_Bat = {
							air_attack = 0.03
						}
						Arm_Marine_Bat = {
							air_attack = 0.03
						}
						Arm_Air_assault_Bat = {
							air_attack = 0.03
						}
						armor_Bat = {
							air_attack = 0.03
						}
						armor_Comp = {
							air_attack = 0.03
						}
						L_arm_Bat = {
							air_attack = 0.03
						}
						SP_Arty_Bat = {
							air_attack = 0.03
						}
						SP_Arty_Battery = {
							air_attack = 0.03
						}
						SP_AA_Bat = {
							air_attack = 0.03
						}
						SP_AA_Battery = {
							air_attack = 0.03
						}
						Mech_Recce_Comp = {
							air_attack = 0.03
						}
						Arm_Recce_Comp = {
							air_attack = 0.03
						}
						armor_Recce_Comp = {
							air_attack = 0.03
						}
					}
				}
			}
		}
	}

}

sp_truck_modular_seating = {
	specialization = specialization_land

	project_tags = sp_tag_armored_vehicles

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			has_tech = util_vehicle_1
		}
	}

	available = {
		FROM = {
			has_tech = util_vehicle_3
		}
	}

	breakthrough_cost = {
		specialization_land = 1
	}

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	project_output = {
		equipment_bonus = {
			util_vehicle_equipment = {
				build_cost_ic = 0.05
				breakthrough = 0.10
				instant = yes
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_motorised_tech_1
		sp_land_generic_reward_motorised_tech_2
		sp_land_generic_reward_motorised_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_truck_modular_seating_reward_quick_release_belt = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_truck_modular_seating_reward_quick_release_belt_option

				iteration_output = {
					sub_unit_bonus = {
						combat_service_support_company = {
							casualty_trickleback = 0.02
						}
						helicopter_combat_service_support = {
							casualty_trickleback = 0.02
						}
					}

					country_effects = {
						FROM = {
							add_project_progress_ratio = constant:sp_progress.gain.low
						}
					}
				}
			}
		}
	}

}

sp_utility_vehicle_electric_engines = {
	specialization = specialization_land

	project_tags = sp_tag_armored_vehicles

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			has_tech = util_vehicle_3
		}
	}

	available = {
		FROM = {
			has_tech = util_vehicle_5
		}
	}

	breakthrough_cost = {
		specialization_land = 1
	}

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	project_output = {
		equipment_bonus = {
			util_vehicle_equipment = {
				fuel_consumption = -0.50
				build_cost_ic = 0.10		#Use this since resource cost can't be changed
				instant = yes
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_motorised_tech_1
		sp_land_generic_reward_motorised_tech_2
		sp_land_generic_reward_motorised_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_utility_vehicle_electric_engines_reward_fuel_efficiency = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_utility_vehicle_electric_engines_reward_fuel_efficiency_option

				iteration_output = {
					country_effects = {
						add_tech_bonus = {
							name = sp_utility_vehicle_electric_engines_reward_fuel_efficiency_option_tech_bonus
							bonus = 0.25
							uses = 1
							category = CAT_fuel_oil
						}
					}

					country_effects = {
						FROM = {
							add_project_progress_ratio = constant:sp_progress.gain.low
						}
					}
				}
			}
		}
	}

}

sp_coilgun_memes = {
	specialization = specialization_land
	icon = GFX_sp_coilgun
	project_tags = sp_tag_infantry_equipment

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			is_special_project_completed = sp:sp_railgun_cannon
		}
	}

	available = {
		FROM = {
			is_special_project_completed = sp:sp_railgun_cannon_2
		}
	}

	breakthrough_cost = {
		specialization_land = 15
	}

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	project_output = {
		sub_unit_bonus = {
			L_Air_Inf_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			Mot_Air_Inf_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			Mech_Air_Inf_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			Arm_Air_Inf_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			L_Marine_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			L_Marine_Cdo_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			Mot_Marine_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			Mech_Marine_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			Arm_Marine_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			L_Air_assault_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			Arm_Air_assault_Bat = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
			Special_Forces = {
				soft_attack = 0.15
				hard_attack = 0.15
			}
		}
	}

	generic_prototype_rewards = {
		sp_land_generic_reward_scientist_xp_1
		sp_land_generic_reward_scientist_xp_2
		sp_land_generic_reward_scientist_xp_3
		sp_land_generic_reward_army_xp_1
		sp_land_generic_reward_army_xp_2
		sp_land_generic_reward_army_xp_3
		sp_land_generic_reward_major_progress_1
		sp_land_generic_reward_major_progress_2
		sp_land_generic_reward_major_progress_3
		sp_land_generic_reward_test_failure_1
		sp_land_generic_reward_test_failure_2
		sp_land_generic_reward_test_failure_3
		sp_land_generic_reward_infantry_tech_1
		sp_land_generic_reward_infantry_tech_2
		sp_land_generic_reward_infantry_tech_3
		sp_land_generic_reward_resource_scarcity
		sp_land_generic_reward_critical_failure
	}
}