# Author(s): AngriestBird, Doolittle, DROID, Cyrus
#This is required to research any aircraft hull
sp_aircraft_project = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air
	icon = GFX_sp_aircraft_experimentation
	project_tags = sp_tag_avionics_aeronautics

	ai_will_do = {
		base = 1
	}

	breakthrough_cost = {
		specialization_air = 1
	}

	prototype_time = sp_time.prototype.medium

	complexity = sp_complexity.medium

	resource_cost = {
		resources = {
			aluminium = 10
			tungsten = 10
		}
	}

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = sp_aircraft_project_tt
			}
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}
}

sp_helicopter_project = {
	specialization = specialization_air
	icon = GFX_sp_aircraft_experimentation
	project_tags = sp_tag_avionics_aeronautics

	ai_will_do = {
		base = 1
	}

	breakthrough_cost = {
		specialization_air = 2
	}

	prototype_time = sp_time.prototype.medium

	complexity = sp_complexity.medium

	resource_cost = {
		resources = {
			aluminium = 5
			tungsten = 5
		}
	}

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = sp_helicopter_project_tt
			}
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}
}

sp_experimental_engines = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air
	icon = GFX_sp_experimental_engine_types
	project_tags = sp_tag_aircraft_engines

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	visible = {
		FROM = {
			has_tech = early_airframe_designs
		}
	}
	available = {
		FROM = {
			has_tech = engines_2
		}
	}

	breakthrough_cost = {
		specialization_air = 2
	}

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_nuclear_engines
			}
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_solar_engines
			}
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_scramjet_engines
			}
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_ramjet_engines
			}
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}

	ai_will_do = {
		base = 5
		modifier = {
			add = 20
			FROM = {
				OR = {
					original_tag = USA
					original_tag = SOV
					original_tag = CHI
					original_tag = FRA
					original_tag = ENG
				}
			}
		}
	}
}

sp_nuclear_engines = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air
	icon = GFX_sp_airplane_nuclear_engines
	project_tags = sp_tag_aircraft_engines

	prototype_time = sp_time.prototype.very_long

	complexity = sp_complexity.large

	breakthrough_cost = {
		specialization_air = 2
	}

	special_project_parent = {
		sp_experimental_engines
	}

	visible = {
		FROM = {
			OR = {
				has_tech = nuclear_technology
				has_tech = tech_nuclear_power_systems
				has_tech = engines_2
			}
		}
	}

	available = {
		FROM = {
			has_tech = reactor1
			has_tech = engines_2
			has_tech = tech_nuclear_power_systems
		}
	}

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_TECH
				TECH = nuclear_engines
			}
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}

	ai_will_do = {
		base = 1
	}
}

sp_solar_engines = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air

	project_tags = sp_tag_aircraft_engines

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	breakthrough_cost = {
		specialization_air = 2
	}

	special_project_parent = {
		sp_experimental_engines
	}

	available = {
		FROM = {
			has_tech = early_renewables
		}
	}

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_TECH
				TECH = solar_engines_1
			}
		}
	}

	ai_will_do = {
		base = 1
	}
}

sp_scramjet_engines = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air

	project_tags = sp_tag_aircraft_engines

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	breakthrough_cost = {
		specialization_air = 3
	}

	special_project_parent = {
		sp_experimental_engines
	}

	visible = {
		FROM = {
			OR = {
				has_tech = engines_3
				is_special_project_completed = sp:sp_experimental_engines
			}
		}
	}
	available = {
		FROM = {
			has_tech = engines_4
			is_special_project_completed = sp:sp_experimental_engines
		}
	}

	project_output = {
		enable_equipment_modules = {
			engine_scramjet
		}
	}

	ai_will_do = {
		base = 1
	}
}

sp_ramjet_engines = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air

	project_tags = sp_tag_aircraft_engines

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	breakthrough_cost = {
		specialization_air = 4
	}

	special_project_parent = {
		sp_experimental_engines
	}

	visible = {
		FROM = {
			OR = {
				has_tech = engines_3
				is_special_project_completed = sp:sp_experimental_engines
			}
		}
	}
	available = {
		FROM = {
			has_tech = engines_4
			is_special_project_completed = sp:sp_scramjet_engines
		}
	}

	project_output = {
		enable_equipment_modules = {
			engine_ramjet
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}

	ai_will_do = {
		base = 1
	}
}
#sp_5thgen_airframe = {
#	allowed = { has_dlc = "By Blood Alone" }
#	specialization = specialization_air
#	icon = GFX_sp_aircraft_experimentation
#	project_tags = sp_tag_avionics_aeronautics
#
#	ai_will_do = {
#		base = 1
#	}
#
#	visible = {
#		FROM = {
#			has_tech = flying_wing_design
#		}
#	}
#
#	available = {
#		FROM = {
#			has_tech = stealth_tech_1
#		}
#	}
#
#	breakthrough_cost = {
#		specialization_air = 2
#	}
#
#	prototype_time = sp_time.prototype.very_long
#
#	complexity = sp_complexity.large
#
#	project_output = {
#		country_effects = {
#			custom_effect_tooltip = {
#				localization_key = sp_5thgen_airframe_tt
#			}
#		}
#	}
#
#	generic_prototype_rewards = {
#		sp_air_generic_reward_scientist_xp_1
#		sp_air_generic_reward_scientist_xp_2
#		sp_air_generic_reward_scientist_xp_3
#		sp_air_generic_reward_army_xp_1
#		sp_air_generic_reward_army_xp_2
#		sp_air_generic_reward_army_xp_3
#		sp_air_generic_reward_major_progress_1
#		sp_air_generic_reward_major_progress_2
#		sp_air_generic_reward_major_progress_3
#		sp_air_generic_reward_test_failure_1
#		sp_air_generic_reward_test_failure_2
#		sp_air_generic_reward_test_failure_3
#		sp_air_generic_reward_resource_scarcity
#		sp_air_generic_reward_critical_failure
#	}
#}
#sp_modern_air_electronics = {
#	allowed = { has_dlc = "By Blood Alone" }
#	specialization = specialization_air
#	icon = GFX_sp_aircraft_experimentation
#	project_tags = sp_tag_avionics_aeronautics
#
#	ai_will_do = {
#		base = 1
#	}
#
#	visible = {
#		FROM = {
#			has_tech = avionics_1
#		}
#	}
#
#	available = {
#		FROM = {
#			has_tech = avionics_2
#		}
#	}
#
#	breakthrough_cost = {
#		specialization_air = 2
#	}
#
#	prototype_time = sp_time.prototype.very_long
#
#	complexity = sp_complexity.large
#
#	project_output = {
#		country_effects = {
#			custom_effect_tooltip = {
#				localization_key = sp_modern_air_electronics_tt
#			}
#		}
#	}
#
#	generic_prototype_rewards = {
#		sp_air_generic_reward_scientist_xp_1
#		sp_air_generic_reward_scientist_xp_2
#		sp_air_generic_reward_scientist_xp_3
#		sp_air_generic_reward_army_xp_1
#		sp_air_generic_reward_army_xp_2
#		sp_air_generic_reward_army_xp_3
#		sp_air_generic_reward_major_progress_1
#		sp_air_generic_reward_major_progress_2
#		sp_air_generic_reward_major_progress_3
#		sp_air_generic_reward_test_failure_1
#		sp_air_generic_reward_test_failure_2
#		sp_air_generic_reward_test_failure_3
#		sp_air_generic_reward_resource_scarcity
#		sp_air_generic_reward_critical_failure
#	}
#}
#sp_future_air_electronics = {
#	allowed = { has_dlc = "By Blood Alone" }
#	specialization = specialization_air
#	icon = GFX_sp_aircraft_experimentation
#	project_tags = sp_tag_avionics_aeronautics
#
#	ai_will_do = {
#		base = 1
#	}
#
#	visible = {
#		FROM = {
#			has_tech = avionics_2
#		}
#	}
#
#	available = {
#		FROM = {
#			has_tech = avionics_3
#		}
#	}
#
#	breakthrough_cost = {
#		specialization_air = 3
#	}
#
#	prototype_time = sp_time.prototype.very_long
#
#	complexity = sp_complexity.large
#
#	project_output = {
#		country_effects = {
#			custom_effect_tooltip = {
#				localization_key = sp_future_air_electronics_tt
#			}
#		}
#	}
#
#	generic_prototype_rewards = {
#		sp_air_generic_reward_scientist_xp_1
#		sp_air_generic_reward_scientist_xp_2
#		sp_air_generic_reward_scientist_xp_3
#		sp_air_generic_reward_army_xp_1
#		sp_air_generic_reward_army_xp_2
#		sp_air_generic_reward_army_xp_3
#		sp_air_generic_reward_major_progress_1
#		sp_air_generic_reward_major_progress_2
#		sp_air_generic_reward_major_progress_3
#		sp_air_generic_reward_test_failure_1
#		sp_air_generic_reward_test_failure_2
#		sp_air_generic_reward_test_failure_3
#		sp_air_generic_reward_resource_scarcity
#		sp_air_generic_reward_critical_failure
#	}
#}
#sp_modern_air_weapons = {
#	allowed = { has_dlc = "By Blood Alone" }
#	specialization = specialization_air
#	icon = GFX_sp_aircraft_experimentation
#	project_tags = sp_tag_avionics_aeronautics
#
#	ai_will_do = {
#		base = 1
#	}
#
#	visible = {
#		FROM = {
#			has_tech = avionics_1
#		}
#	}
#
#	available = {
#		FROM = {
#			has_tech = avionics_2
#		}
#	}
#
#	breakthrough_cost = {
#		specialization_air = 2
#	}
#
#	prototype_time = sp_time.prototype.very_long
#
#	complexity = sp_complexity.large
#
#	project_output = {
#		country_effects = {
#			custom_effect_tooltip = {
#				localization_key = sp_modern_air_weapons_tt
#			}
#		}
#	}
#
#	generic_prototype_rewards = {
#		sp_air_generic_reward_scientist_xp_1
#		sp_air_generic_reward_scientist_xp_2
#		sp_air_generic_reward_scientist_xp_3
#		sp_air_generic_reward_army_xp_1
#		sp_air_generic_reward_army_xp_2
#		sp_air_generic_reward_army_xp_3
#		sp_air_generic_reward_major_progress_1
#		sp_air_generic_reward_major_progress_2
#		sp_air_generic_reward_major_progress_3
#		sp_air_generic_reward_test_failure_1
#		sp_air_generic_reward_test_failure_2
#		sp_air_generic_reward_test_failure_3
#		sp_air_generic_reward_resource_scarcity
#		sp_air_generic_reward_critical_failure
#	}
#}
#sp_future_air_weapons = {
#	allowed = { has_dlc = "By Blood Alone" }
#	specialization = specialization_air
#	icon = GFX_sp_aircraft_experimentation
#	project_tags = sp_tag_avionics_aeronautics
#
#	ai_will_do = {
#		base = 1
#	}
#
#	visible = {
#		FROM = {
#			has_tech = avionics_2
#		}
#	}
#
#	available = {
#		FROM = {
#			has_tech = avionics_3
#		}
#	}
#
#	breakthrough_cost = {
#		specialization_air = 3
#	}
#
#	prototype_time = sp_time.prototype.very_long
#
#	complexity = sp_complexity.large
#
#	project_output = {
#		country_effects = {
#			custom_effect_tooltip = {
#				localization_key = sp_future_air_weapons_tt
#			}
#		}
#	}
#
#	generic_prototype_rewards = {
#		sp_air_generic_reward_scientist_xp_1
#		sp_air_generic_reward_scientist_xp_2
#		sp_air_generic_reward_scientist_xp_3
#		sp_air_generic_reward_army_xp_1
#		sp_air_generic_reward_army_xp_2
#		sp_air_generic_reward_army_xp_3
#		sp_air_generic_reward_major_progress_1
#		sp_air_generic_reward_major_progress_2
#		sp_air_generic_reward_major_progress_3
#		sp_air_generic_reward_test_failure_1
#		sp_air_generic_reward_test_failure_2
#		sp_air_generic_reward_test_failure_3
#		sp_air_generic_reward_resource_scarcity
#		sp_air_generic_reward_critical_failure
#	}
#}
sp_fully_autonomous_computers_framework = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air
	icon = GFX_sp_fully_autonomous_computer_frameworks
	project_tags = sp_tag_pilotless_vehicles

	prototype_time = sp_time.prototype.medium

	complexity = sp_complexity.large

	breakthrough_cost = {
		specialization_air = 3
	}

	visible = {
		FROM = {
			OR = {
				has_tech = fully_automated_drones
				has_tech = human_imitation_ai
			}
		}
	}
	available = {
		FROM = {
			has_tech = fully_automated_drones
			has_tech = human_imitation_ai
		}
	}

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_fully_autonomous_fighters
			}
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_fully_autonomous_bombers
			}
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_air_autonomous_computers_reward_improved_detection = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_air_autonomous_computers_reward_improved_detection

				iteration_output = {
					equipment_bonus = {
						AS_Fighter_equipment = {
							air_ground_attack = 0.03
							air_agility = 0.05
							reliability = 0.05
						}
					}
				}
			}
		}
	}

	ai_will_do = {
		base = 1
	}
}

sp_fully_autonomous_fighters = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air
	icon = GFX_sp_autonomous_fighters
	project_tags = sp_tag_pilotless_vehicles

	prototype_time = sp_time.prototype.very_long

	complexity = sp_complexity.large

	breakthrough_cost = {
		specialization_air = 3
	}

	special_project_parent = {
		sp_fully_autonomous_computers_framework
	}

	visible = {
		FROM = {
			OR = {
				has_tech = fully_automated_drones
				has_tech = human_imitation_ai
			}
		}
	}
	available = {
		FROM = {
			has_tech = fully_automated_drones
			has_tech = human_imitation_ai
			is_special_project_completed = sp:sp_fully_autonomous_computers_framework
		}
	}

	project_output = {
		enable_equipment_modules = {
			fully_autonomous_computer_system_light
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}

	ai_will_do = {
		base = 1
	}
}

sp_fully_autonomous_bombers = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air
	icon = GFX_sp_autonomous_bombers
	project_tags = sp_tag_pilotless_vehicles

	prototype_time = sp_time.prototype.very_long

	complexity = sp_complexity.large

	breakthrough_cost = {
		specialization_air = 3
	}

	special_project_parent = {
		sp_fully_autonomous_computers_framework
	}

	visible = {
		FROM = {
			OR = {
				has_tech = fully_automated_drones
				has_tech = human_imitation_ai
				has_tech = tgp_recon_2
			}
		}
	}
	available = {
		FROM = {
			has_tech = fully_automated_drones
			has_tech = tgp_recon_2
			has_tech = human_imitation_ai
			is_special_project_completed = sp:sp_fully_autonomous_computers_framework
		}
	}

	project_output = {
		enable_equipment_modules = {
			fully_autonomous_computer_system_heavy
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}

	unique_prototype_rewards = {
		sp_air_autonomous_bombers_reward_improved_detection = {
			fire_only_once = yes

			threshold = {
				min = 0
				max = 100
			}

			weight = {
				base = 1
			}

			option = {
				token = sp_air_autonomous_bombers_reward_improved_detection

				iteration_output = {
					equipment_bonus = {
						large_plane_airframe = {
							reliability = 0.10
							air_agility = 0.05
						}
					}
				}
			}
		}
	}

	ai_will_do = {
		base = 1
	}
}

#TODO: NEEDS ICON
sp_stealth_technology = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air
	project_tags = sp_tag_avionics_aeronautics

	prototype_time = sp_time.prototype.long

	complexity = sp_complexity.large

	visible = {
		FROM = {
			has_tech = early_airframe_designs
		}
	}
	available = {
		FROM = {
			has_tech = flying_wing_design
		}
	}

	breakthrough_cost = {
		specialization_air = 3
	}

	project_output = {
		country_effects = {
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_TECH
				TECH = stealth_tech_1
			}
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}

	ai_will_do = {
		base = 5
		modifier = {
			add = 20
			FROM = {
				OR = {
					original_tag = USA
					original_tag = SOV
					original_tag = CHI
					original_tag = FRA
					original_tag = ENG
				}
			}
		}
	}
}

sp_arsenal_bird = {
	allowed = { has_dlc = "By Blood Alone" }
	specialization = specialization_air
	icon = GFX_sp_aircraft_experimentation
	project_tags = sp_tag_avionics_aeronautics

	ai_will_do = {
		base = 1
	}

	visible = {
		FROM = {
			has_tech = gen_6_large
			is_special_project_completed = sp:sp_fully_autonomous_computers_framework
		}
	}

	available = {
		FROM = {
			is_special_project_completed = sp:sp_nuclear_engines
			is_special_project_completed = sp:sp_fully_autonomous_fighters
			is_special_project_completed = sp:sp_fully_autonomous_bombers
		}
	}

	breakthrough_cost = {
		specialization_air = 4
	}

	prototype_time = sp_time.prototype.very_long

	complexity = sp_complexity.large

	resource_cost = {
		resources = {
			chromium = 10
			tungsten = 10
			aluminium = 10
		}
	}

	project_output = {
		enable_equipments = {
			mothership_equipment_1
		}
		enable_equipment_modules = {
			weap_droneswarm_fighter_1
			weap_droneswarm_bomber_1
			weap_missile_bay_1
			weap_droneswarm_anti_ship_1
			fully_autonomous_computer_system_arsenal
		}
	}

	generic_prototype_rewards = {
		sp_air_generic_reward_scientist_xp_1
		sp_air_generic_reward_scientist_xp_2
		sp_air_generic_reward_scientist_xp_3
		sp_air_generic_reward_army_xp_1
		sp_air_generic_reward_army_xp_2
		sp_air_generic_reward_army_xp_3
		sp_air_generic_reward_major_progress_1
		sp_air_generic_reward_major_progress_2
		sp_air_generic_reward_major_progress_3
		sp_air_generic_reward_test_failure_1
		sp_air_generic_reward_test_failure_2
		sp_air_generic_reward_test_failure_3
		sp_air_generic_reward_resource_scarcity
		sp_air_generic_reward_critical_failure
	}

	ai_will_do = {
		base = 5
		modifier = {
			add = 20
			FROM = {
				OR = {
					original_tag = USA
					original_tag = SOV
					original_tag = CHI
					original_tag = FRA
					original_tag = ENG
				}
			}
		}
	}
}