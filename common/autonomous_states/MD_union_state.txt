# Author(s): LordBogdanoff

# Union state
autonomy_state = {
	id = autonomy_union_state

	default = no
	is_puppet = no
	min_freedom_level = 0.85
	manpower_influence = 0.3
	use_overlord_color = yes

	rule = {
		can_not_declare_war = no
		can_decline_call_to_war = yes
		units_deployed_to_overlord = no
		can_send_volunteers = yes
		can_guarantee_other_ideologies = yes
	}

	modifier = {
		autonomy_manpower_share = 0.0
		can_master_build_for_us = 1
		extra_trade_to_overlord_factor = 0.0
		overlord_trade_cost_factor = -0.3
		cic_to_overlord_factor = 0.0
		mic_to_overlord_factor = 0.0
		license_subject_master_purchase_cost = -1
		research_sharing_per_country_bonus_factor = -0.6
		drift_defence_factor = 0.2
		master_ideology_drift = 0.5
	}


	ai_subject_wants_higher = { factor = 0.0 }
	ai_overlord_wants_lower = { factor = 0.3 }
	ai_overlord_wants_garrison = { always = yes }

	allowed = {
		OVERLORD = { 
			original_tag = SOV 
		}
		OR = {
			original_tag = BLR
			original_tag = SER
			original_tag = ARM
			original_tag = AZE
			original_tag = TAJ
			original_tag = UKR
			original_tag = KYR
			original_tag = GEO
			original_tag = KAZ
			original_tag = TRK
		}
		has_country_flag = BLR_unionstate_agree
	}

	can_take_level = { always = no }
	can_lose_level = { always = no }
}
