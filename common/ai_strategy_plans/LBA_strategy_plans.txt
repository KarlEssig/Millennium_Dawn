LBA_libyan_islamic_fighting_group = {
	allowed = { original_tag = LBA }
	name = "LIFG Plan"
	desc = "Behaviour for Libya when going with LIFG"

	enable = {
		original_tag = LBA
		has_global_flag = LBA_LIBYAN_ISLAMIC_FIGHTING_GROUP
	}

	abort = {
		is_subject = yes
	}

	ai_national_focuses = {
		#Always do this as soon as it is available
		LBA_activate_arsenal_of_islam

		#Get rid of Gaddafi ASAP
		LBA_ignore_lifg
		LBA_peace_with_lifg
		LBA_assassinate_gaddafi

		#Get a nuke
		LBA_negotiations_with_un
		LBA_meeting_with_taliban
		LBA_assassinate_dissidents
		LBA_meet_hezbollah
		LBA_house_terrorists
		LBA_celebrate_lockerbie_bombers
		LBA_wmds
		LBA_start_building_reactor
		LBA_arms_smuggling
		LBA_buy_nukes

		#Finish political path before war
		LBA_mujahideen_of_libya
		LBA_old_taliban_friends
		LBA_mujahideen_legion
		LBA_call_back_veterans
		LBA_unity_in_islam
		LBA_work_together_with_brotherhood
		LBA_hardline_sharia_law
		LBA_de_westernisation
		LBA_join_sahel_war
		LBA_islamic_state_of_sahel

		#Additional political stuff
		LBA_islam_over_nation
		LBA_family_is_a_central_unit
		LBA_follow_religious_law
		LBA_traditional_role_for_women
		LBA_religious_education
		LBA_retain_political_balance
		LBA_hunt_down_royals
		LBA_royal_wealth

		#Prepare military
		LBA_lessons_from_the_past
		LBA_toyota_war_guerrilla
		LBA_desert_warfare
		LBA_egypt_war_behind_the_lines
		LBA_foreign_procurement_lines
		LBA_weapons_from_balkans
		LBA_new_trucks
		LBA_new_tanks
		LBA_new_artillery
		LBA_local_supply_system

		#Go to town
		LBA_our_brothers_in_chad
		LBA_topple_chad
		LBA_our_brothers_in_niger
		LBA_topple_niger
		LBA_ally_with_boko_haram
		LBA_invade_nigeria
		LBA_our_brothers_in_algeria
		LBA_topple_algeria
		LBA_infiltrate_mali
		LBA_expand_to_mauritania
		LBA_destroy_last_crusaders
		LBA_malta_repopulation_campaign
		LBA_malta_province
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

LBA_libyan_grand_mufti = {
	allowed = { original_tag = LBA }
	name = "Grand Mufti Plan"
	desc = "Behaviour for Libya when going with the Grand Mufti"

	enable = {
		original_tag = LBA
		has_global_flag = LBA_LIBYAN_GRAND_MUFTI
	}

	abort = {
		is_subject = yes
	}

	ai_national_focuses = {
		#Always do this as soon as it is available
		LBA_activate_arsenal_of_islam

		#Get rid of Gaddafi ASAP
		LBA_ignore_lifg
		LBA_islam_over_nation
		LBA_family_is_a_central_unit
		LBA_follow_religious_law
		LBA_traditional_role_for_women
		LBA_religious_education
		LBA_appoint_grand_mufti
		LBA_grand_mufti_rule
		LBA_denounce_green_book
		LBA_tanasoh_tv
		LBA_supreme_revolutionaries_council
		LBA_anti_saudi_coalition

		#Go against sanctions but don't get a nuke
		LBA_negotiations_with_un
		LBA_meeting_with_taliban
		LBA_assassinate_dissidents
		LBA_meet_hezbollah
		LBA_house_terrorists
		LBA_celebrate_lockerbie_bombers
		LBA_wmds
		LBA_start_building_reactor
		LBA_arms_smuggling

		#Finish political path
		LBA_retain_political_balance
		LBA_hunt_down_royals
		LBA_royal_wealth

		#Do a little economy
		LBA_economy_of_libya
		LBA_gmmr_kufra
		LBA_gmmr_ghadames
		LBA_gmmr_jaghbub
		LBA_western_mountain_power_plant
		LBA_gmmr_connect_east_west
		LBA_zawia_power_plant
		LBA_convert_pipe_factories
		LBA_create_ministry_of_agriculture
		LBA_desalination_plants
		LBA_expand_agriculture
		LBA_foreign_food_imports

		#Do a little military
		LBA_lessons_from_the_past
		LBA_el_dorado_canyon
		LBA_better_aa
		LBA_gulf_of_sidra
		LBA_toyota_war_mobility
		LBA_desert_warfare
		LBA_egypt_war_tank_formations

		#Hang around with Hamas
		LBA_neighbourly_politics
		LBA_approach_hamas
		LBA_send_fighters_to_hamas
		LBA_new_intifada
		LBA_champion_of_al_aqsa

		#Do rest of the military stuff
		LBA_foreign_procurement_lines
		LBA_weapons_from_balkans
		LBA_new_trucks
		LBA_new_tanks
		LBA_new_artillery
		LBA_local_supply_system
		LBA_expand_airbases
		LBA_new_fighters
		LBA_new_helicopters
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

LBA_libyan_democratic_reform = {
	allowed = { original_tag = LBA }
	name = "Democratic Reform Plan"
	desc = "Behaviour for Libya when going becoming a democracy"

	enable = {
		original_tag = LBA
		has_global_flag = LBA_DEMOCRATIC_REFORM
	}

	abort = {
		is_subject = yes
	}

	ai_national_focuses = {
		#Always do this as soon as it is available
		LBA_activate_arsenal_of_islam

		#Crush LIFG ASAP
		LBA_crush_lifg

		#Get rid of sanctions
		LBA_negotiations_with_un
		LBA_compensation_to_lockerbie_victims
		LBA_disarmament_program
		LBA_approach_western_leaders
		LBA_taxation_deal_with_uk
		LBA_compensation_from_italy
		LBA_request_usaid
		LBA_remove_sanctions
		LBA_invite_foreign_investors
		LBA_arms_deals_with_italy

		#Do political path
		LBA_revise_green_book
		LBA_green_book_part_1_1
		LBA_green_book_part_1_3
		LBA_green_book_part_1_8
		LBA_green_book_part_1_10
		LBA_green_book_part_2_13
		LBA_green_book_part_2_14
		LBA_green_book_part_2_16
		LBA_green_book_part_3_21
		LBA_green_book_part_3_24
		LBA_green_book_part_3_25
		LBA_green_book_part_3_28
		LBA_release_new_green_book
		LBA_empower_peoples_congresses
		LBA_allow_political_parties
		LBA_grassroots_democracy
		LBA_presidential_republic
		LBA_unitary_state
		LBA_minority_protection
		LBA_protect_toubou_and_tuaregs
		LBA_call_back_exiles

		#Do a little economy
		LBA_economy_of_libya
		LBA_gmmr_kufra
		LBA_gmmr_ghadames
		LBA_gmmr_jaghbub
		LBA_western_mountain_power_plant
		LBA_gmmr_connect_east_west
		LBA_zawia_power_plant
		LBA_convert_pipe_factories
		LBA_create_ministry_of_agriculture
		LBA_desalination_plants
		LBA_expand_agriculture
		LBA_land_reform
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

LBA_libyan_monarchy = {
	allowed = { original_tag = LBA }
	name = "Monarchy Plan"
	desc = "Behaviour for Libya when going becoming a monarchy"

	enable = {
		original_tag = LBA
		has_global_flag = LBA_RISE_OF_MONARCHY
	}

	abort = {
		is_subject = yes
	}

	ai_national_focuses = {
		#Always do this as soon as it is available
		LBA_activate_arsenal_of_islam

		#Crush LIFG ASAP
		LBA_crush_lifg

		#Get rid of sanctions
		LBA_negotiations_with_un
		LBA_compensation_to_lockerbie_victims
		LBA_disarmament_program
		LBA_approach_western_leaders
		LBA_taxation_deal_with_uk
		LBA_compensation_from_italy
		LBA_request_usaid
		LBA_remove_sanctions
		LBA_invite_foreign_investors
		LBA_arms_deals_with_italy

		#Do political path
		LBA_revise_green_book
		LBA_green_book_part_1_1
		LBA_green_book_part_1_3
		LBA_green_book_part_1_8
		LBA_green_book_part_1_10
		LBA_green_book_part_2_13
		LBA_green_book_part_2_14
		LBA_green_book_part_2_16
		LBA_green_book_part_3_21
		LBA_green_book_part_3_24
		LBA_green_book_part_3_25
		LBA_green_book_part_3_28
		LBA_release_new_green_book
		LBA_empower_peoples_congresses
		LBA_allow_political_parties
		LBA_grassroots_democracy
		LBA_allow_royal_family_return
		LBA_compensation_to_royals
		LBA_the_real_monarch
		LBA_royal_unity
		LBA_revive_senussi_order
		LBA_approach_arab_league
		LBA_approach_nato
		LBA_call_back_exiles
		LBA_join_war_on_terror
		LBA_oef_trans_sahara

		#Do a little economy
		LBA_economy_of_libya
		LBA_gmmr_kufra
		LBA_gmmr_ghadames
		LBA_gmmr_jaghbub
		LBA_western_mountain_power_plant
		LBA_gmmr_connect_east_west
		LBA_zawia_power_plant
		LBA_convert_pipe_factories
		LBA_create_ministry_of_agriculture
		LBA_desalination_plants
		LBA_expand_agriculture
		LBA_land_reform
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

LBA_libya_gaddafi_anti_colonialist = {
	allowed = { original_tag = LBA }
	name = "Anti-Colonialist Gaddafi Plan"
	desc = "Behaviour for Libya when Gaddafi is anti-colonialist"

	enable = {
		original_tag = LBA
		has_global_flag = LBA_GADDAFI_ANTI_COLONIAL_UNION
	}

	abort = {
		is_subject = yes
	}

	ai_national_focuses = {
		#Always do this as soon as it is available
		LBA_activate_arsenal_of_islam

		#Crush LIFG ASAP
		LBA_crush_lifg

		#Get rid of sanctions
		LBA_negotiations_with_un
		LBA_compensation_to_lockerbie_victims
		LBA_disarmament_program
		LBA_approach_western_leaders
		LBA_taxation_deal_with_uk
		LBA_compensation_from_italy
		LBA_remove_sanctions
		LBA_invite_foreign_investors
		LBA_arms_deals_with_italy

		#Do political path
		LBA_revise_green_book
		LBA_green_book_part_1_1
		LBA_green_book_part_1_3
		LBA_green_book_part_1_8
		LBA_green_book_part_1_10
		LBA_green_book_part_2_13
		LBA_green_book_part_2_14
		LBA_green_book_part_2_16
		LBA_green_book_part_3_21
		LBA_green_book_part_3_24
		LBA_green_book_part_3_25
		LBA_green_book_part_3_28
		LBA_release_new_green_book
		LBA_empower_revolutionary_committees
		LBA_peoples_intelligence_agency
		LBA_trust_revolutionary_guard
		LBA_fatherly_leader

		#Do foreign focuses
		LBA_gaddafis_dream
		LBA_approach_algeria
		LBA_approach_egypt
		LBA_approach_eritrea
		LBA_weapons_to_taylor
		LBA_central_african_mercs
		LBA_red_sea_trade_deal
		LBA_meet_mengistu
		LBA_anti_colonialist
		LBA_islamic_legion
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

LBA_libya_gaddafi_united_arab_republic = {
	allowed = { original_tag = LBA }
	name = "United Arab Republic Gaddafi Plan"
	desc = "Behaviour for Libya when Gaddafi goes for the United Arab Republic"

	enable = {
		original_tag = LBA
		has_global_flag = LBA_GADDAFI_UAR
	}

	abort = {
		is_subject = yes
	}

	ai_national_focuses = {
		#Always do this as soon as it is available
		LBA_activate_arsenal_of_islam

		#Crush LIFG ASAP
		LBA_crush_lifg

		#Get rid of sanctions
		LBA_negotiations_with_un
		LBA_compensation_to_lockerbie_victims
		LBA_disarmament_program
		LBA_approach_western_leaders
		LBA_taxation_deal_with_uk
		LBA_compensation_from_italy
		LBA_remove_sanctions
		LBA_invite_foreign_investors
		LBA_arms_deals_with_italy

		#Do political path
		LBA_revise_green_book
		LBA_green_book_part_1_1
		LBA_green_book_part_1_3
		LBA_green_book_part_1_8
		LBA_green_book_part_1_10
		LBA_green_book_part_2_13
		LBA_green_book_part_2_14
		LBA_green_book_part_2_16
		LBA_green_book_part_3_21
		LBA_green_book_part_3_24
		LBA_green_book_part_3_25
		LBA_green_book_part_3_28
		LBA_release_new_green_book
		LBA_empower_revolutionary_committees
		LBA_peoples_intelligence_agency
		LBA_trust_revolutionary_guard
		LBA_fatherly_leader

		#Do foreign focuses
		LBA_gaddafis_dream
		LBA_approach_algeria
		LBA_approach_egypt
		LBA_approach_eritrea
		LBA_weapons_to_taylor
		LBA_central_african_mercs
		LBA_red_sea_trade_deal
		LBA_meet_mengistu
		LBA_anti_colonialist
		LBA_strive_for_uar

		#Get influence on Arabic countries
		LBA_neighbourly_politics
		LBA_close_ties_sahrawi
		LBA_support_algeria
		LBA_influence_mauritania
		LBA_all_are_idiots
		LBA_southern_focus
		LBA_support_sudan
		LBA_send_mercenaries
		LBA_reconcile_with_chad
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

LBA_libya_gaddafi_united_africa = {
	allowed = { original_tag = LBA }
	name = "United States of Africa Gaddafi Plan"
	desc = "Behaviour for Libya when Gaddafi goes for the United States Of Africa"

	enable = {
		original_tag = LBA
		has_global_flag = LBA_GADDAFI_AFRICA
	}

	abort = {
		is_subject = yes
	}

	ai_national_focuses = {
		#Always do African Union focus if available
		AFRICAN_UNION_shared_focus
		AFRICAN_UNION_shared_focus_create_investment_bank
		AFRICAN_UNION_shared_focus_create_monetary_fund
		AFRICAN_UNION_shared_focus_create_stock_exchange_our_country
		AFRICAN_UNION_shared_focus_create_oil_fund
		AFRICAN_UNION_shared_focus_create_central_bank
		AFRICAN_UNION_shared_focus_common_currency_south_african_rand
		AFRICAN_UNION_shared_focus_create_african_free_trade_area
		AFRICAN_UNION_shared_focus_coastal_protection
		AFRICAN_UNION_shared_focus_sign_treaty_of_pelindaba
		AFRICAN_UNION_shared_focus_form_defence_council
		AFRICAN_UNION_shared_focus_create_african_space_agency
		AFRICAN_UNION_shared_focus_include_african_diaspora
		AFRICAN_UNION_shared_focus_employ_local_defence
		AFRICAN_UNION_shared_focus_hq_in_ethiopia
		AFRICAN_UNION_shared_focus_african_energy_commission
		AFRICAN_UNION_shared_focus_common_disease_fight
		AFRICAN_UNION_shared_focus_speak_arabic
		AFRICAN_UNION_shared_focus_embrace_corruption
		AFRICAN_UNION_shared_focus_pan_african_university
		AFRICAN_UNION_shared_focus_empower_assembly
		AFRICAN_UNION_shared_focus_federation_of_africa
		AFRICAN_UNION_shared_focus_unite_africa

		#Always do this as soon as it is available
		LBA_activate_arsenal_of_islam

		#Crush LIFG ASAP
		LBA_crush_lifg

		#Get rid of sanctions
		LBA_negotiations_with_un
		LBA_compensation_to_lockerbie_victims
		LBA_disarmament_program
		LBA_approach_western_leaders
		LBA_taxation_deal_with_uk
		LBA_compensation_from_italy
		LBA_remove_sanctions
		LBA_invite_foreign_investors
		LBA_arms_deals_with_italy

		#Do political path
		LBA_revise_green_book
		LBA_green_book_part_1_1
		LBA_green_book_part_1_3
		LBA_green_book_part_1_8
		LBA_green_book_part_1_10
		LBA_green_book_part_2_13
		LBA_green_book_part_2_14
		LBA_green_book_part_2_16
		LBA_green_book_part_3_21
		LBA_green_book_part_3_24
		LBA_green_book_part_3_25
		LBA_green_book_part_3_28
		LBA_release_new_green_book
		LBA_empower_revolutionary_committees
		LBA_peoples_intelligence_agency
		LBA_trust_revolutionary_guard
		LBA_hunt_down_royals
		LBA_royal_wealth
		LBA_the_real_monarch

		#Do foreign focuses
		LBA_gaddafis_dream
		LBA_approach_algeria
		LBA_approach_egypt
		LBA_approach_eritrea
		LBA_weapons_to_taylor
		LBA_central_african_mercs
		LBA_red_sea_trade_deal
		LBA_meet_mengistu
		LBA_anti_colonialist
		LBA_strive_for_african_union

		#Get influence on Arabic countries
		LBA_neighbourly_politics
		LBA_close_ties_sahrawi
		LBA_support_algeria
		LBA_influence_mauritania
		LBA_southern_focus
		LBA_support_sudan
		LBA_send_mercenaries
		LBA_reconcile_with_chad
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}