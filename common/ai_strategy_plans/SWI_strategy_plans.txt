SWI_historical_plan = {
	allowed = { original_tag = SWI }
	name = "Switzerland Historical Plan"
	enable = {
		original_tag = SWI
		is_historical_focus_on = yes
	}

	ai_national_focuses = {
		SWI_strengthen_the_government
		SWI_join_the_united_nations
		SWI_economic_independence
		SWI_strengthen_the_fdfa
		SWI_international_neutrality
		SWI_maintain_border_controls
		SWI_condemn_factionalism
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

SWI_mountain_fortress_plan = {
	allowed = { original_tag = SWI }
	name = "Switzerland NATO Plan"

	enable = {
		original_tag = SWI
		has_global_flag = SWI_MOUNTAIN_FORTRESS_PATH
	}

	ai_national_focuses = {
		SWI_strengthen_the_government
		SWI_the_mountain_fortress
	}


	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

SWI_NATO_plan = {
	allowed = { original_tag = SWI }
	name = "Switzerland NATO Plan"

	enable = {
		original_tag = SWI
		has_global_flag = SWI_NATO_PATH
	}

	ai_national_focuses = {
		SWI_strengthen_the_government
		SWI_join_the_united_nations
		SWI_seek_eu_membership
		SWI_strengthen_the_fdfa
		SWI_declare_the_end_of_swiss_neutrality
		SWI_seek_nato_membership
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

SWI_swiss_protection_league_plan = {
	allowed = { original_tag = SWI }
	name = "Switzerland Swiss Protection League plan"

	enable = {
		original_tag = SWI
		has_global_flag = SWI_SWISS_PROTECTION_LEAGUE_PATH
	}

	ai_national_focuses = {
		SWI_strengthen_the_government
		SWI_join_the_united_nations
		SWI_economic_independence
		SWI_strengthen_the_fdfa
		SWI_international_neutrality
		SWI_maintain_border_controls
		SWI_the_swiss_protection_league
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}