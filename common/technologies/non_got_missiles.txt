# Author(s): Doolittle
technologies = {
	@1945 = 0
	@1950 = 1
	@1955 = 2
	@1960 = 3
	@1965 = 4
	@1970 = 5
	@1975 = 6
	@1980 = 7
	@1985 = 8
	@1990 = 9
	@1995 = 10
	@2000 = 11
	@2005 = 12
	@2010 = 13
	@2015 = 14
	@2020 = 15
	@2025 = 16
	@2030 = 17
	@2035 = 18
	@2040 = 19
	@2045 = 20
	@2050 = 21

	#Y Axis
	@row1 = -4
	@row2 = 0
	@row3 = 4
	@row4 = 8
	@row5 = 12
	@row6 = 16
	@row7 = 20
	@row8 = 24
	@row9 = 28
	@row10 = 32
	@row11 = 36
	@row12 = 40
	@row13 = 44
	@row14 = 48
	@row15 = 52
	@row16 = 56

	air_defense_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech air_defense"
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_missile_project_non_got
			}
		}
		research_cost = 1
		show_effect_as_desc = yes
		start_year = 1945
		enable_building = {
			building = anti_air_building
			level = 1
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 15 y = @1945 }
		}
		path = {
			leads_to_tech = SAM_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_Military
			CAT_missile
			CAT_sam
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			base = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	SAM_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech SAM"
			add_missile_building_level = yes
		}

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		show_effect_as_desc = yes
		start_year = 1955
		enable_building = {
			building = anti_air_building
			level = 2
		}

		folder = {
			name = missile_non_got_folder
			position = { x = 15 y = @1955 }
		}
		path = {
			leads_to_tech = SAM0_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_Military
			CAT_missile
			CAT_sam
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			base = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	SAM0_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech SAM0" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 1965
		show_equipment_icon = yes

		enable_building = {
			building = anti_air_building
			level = 3
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 15 y = @1965 }
		}
		path = {
			leads_to_tech = SAM1_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_Military
			CAT_missile
			CAT_sam
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			base = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}

	SAM1_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech SAM1" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 1975
		show_equipment_icon = yes

		folder = {
			name = missile_non_got_folder
			position = { x = 15 y = @1975 }
		}
		path = {
			leads_to_tech = SAM2_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_Military
			CAT_missile
			CAT_sam
		}
		special_project_specialization = { specialization_air }

		enable_building = {
			building = anti_air_building
			level = 4
		}

		ai_will_do = {
			base = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	SAM2_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech SAM2" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 1985
		show_equipment_icon = yes

		folder = {
			name = missile_non_got_folder
			position = { x = 15 y = @1985 }
		}

		enable_building = {
			building = anti_air_building
			level = 5
		}

		path = {
			leads_to_tech = SAM3_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_Military
			CAT_missile
			CAT_sam
		}
		special_project_specialization = { specialization_air }

		ai_will_do = {
			base = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}

	cruise_missile_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech cruise_missile"
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_missile_project_non_got
			}
		}
		research_cost = 1
		start_year = 1945
		show_effect_as_desc = yes
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @1945 }
		}
		path = {
			leads_to_tech = GLCM_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_alcm
			CAT_slcm
			CAT_hscm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	HSCM_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech HSCM" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 4
		start_year = 2015
		enable_equipments = {
			hypersonic_missile_equipment
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 8 y = @2015 }
		}
		path = {
			leads_to_tech = HSCM1_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_hscm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	HSCM1_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech HSCM1"
		}

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 3
		start_year = 2025
		show_effect_as_desc = yes
		show_equipment_icon = yes
		enable_equipments = {
			hypersonic_missile_equipment_1
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 8 y = @2025 }
		}
		path = {
			leads_to_tech = HSCM2_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_hscm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	HSCM2_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech HSCM2" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 3
		start_year = 2040
		show_effect_as_desc = yes
		show_equipment_icon = yes
		enable_equipments = {
			hypersonic_missile_equipment_2
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 8 y = @2035 }
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_hscm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech GLCM"
			add_missile_building_level = yes
			custom_effect_tooltip = {
				localization_key = SP_UNLOCK_PROJECT
				PROJECT = sp_space_program_non_gdm
			}
		}

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		enable_equipments = {
			guided_missile_equipment_0
		}

		research_cost = 1
		start_year = 1955
		show_effect_as_desc = yes
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @1955 }
		}
		path = {
			leads_to_tech = TEL_launched_missiles_non_got
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = GLCM1_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_slcm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	TEL_launched_missiles_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech TEL_launched_missiles"
			add_missile_building_level = yes
		}
		research_cost = 1
		start_year = 1955
		show_effect_as_desc = yes

		folder = {
			name = missile_non_got_folder
			position = { x = 10 y = @1955 }
		}
		path = {
			leads_to_tech = TEL_3_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_bm
			CAT_irbm
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	TEL_3_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech TEL_3"
			add_missile_building_level = yes
		}
		research_cost = 1
		start_year = 1965
		show_effect_as_desc = yes

		folder = {
			name = missile_non_got_folder
			position = { x = 10 y = @1965 }
		}
		path = {
			leads_to_tech = TEL_4_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_bm
			CAT_irbm
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	TEL_4_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech TEL_4"
			add_missile_building_level = yes
		}
		research_cost = 1
		start_year = 1975
		show_effect_as_desc = yes

		folder = {
			name = missile_non_got_folder
			position = { x = 10 y = @1975 }
		}
		path = {
			leads_to_tech = TEL_5_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_bm
			CAT_irbm
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	TEL_5_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech TEL_5"
			add_missile_building_level = yes
		}
		research_cost = 1
		start_year = 1985
		show_effect_as_desc = yes

		folder = {
			name = missile_non_got_folder
			position = { x = 10 y = @1985 }
		}
		path = {
			leads_to_tech = TEL_6_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_bm
			CAT_irbm
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	TEL_6_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech TEL_6"
			add_missile_building_level = yes
		}
		research_cost = 1
		start_year = 1995
		show_effect_as_desc = yes

		folder = {
			name = missile_non_got_folder
			position = { x = 10 y = @1995 }
		}
		path = {
			leads_to_tech = TEL_7_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_bm
			CAT_irbm
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	TEL_7_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech TEL_7"
			add_missile_building_level = yes
		}
		research_cost = 1
		start_year = 2005
		show_effect_as_desc = yes

		folder = {
			name = missile_non_got_folder
			position = { x = 10 y = @2005 }
		}

		categories = {
			CAT_missile
			CAT_bm
			CAT_irbm
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM1_non_got = {
		on_research_complete = {
			log = "[GetDateText]: [Root.GetName]: add tech GLCM1"
		}

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 1965
		show_equipment_icon = yes
		enable_equipments = {
			guided_missile_equipment_1
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @1965 }
		}
		path = {
			leads_to_tech = GLCM2_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM2_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech GLCM2" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 1975
		show_equipment_icon = yes
		enable_equipments = {
			guided_missile_equipment_2
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @1975 }
		}
		path = {
			leads_to_tech = GLCM3_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM3_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech GLCM3" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 1985
		show_equipment_icon = yes
		enable_equipments = {
			guided_missile_equipment_3
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @1985 }
		}
		path = {
			leads_to_tech = GLCM4_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM4_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech GLCM4" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 1995
		show_equipment_icon = yes
		enable_equipments = {
			guided_missile_equipment_4
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @1995 }
		}
		path = {
			leads_to_tech = GLCM5_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM5_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech GLCM5" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 2005
		show_equipment_icon = yes
		enable_equipments = {
			guided_missile_equipment_5
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @2005 }
		}
		path = {
			leads_to_tech = GLCM6_non_got
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = HSCM_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM6_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech GLCM6" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 2015
		show_equipment_icon = yes
		enable_equipments = {
			guided_missile_equipment_6
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @2015 }
		}

		path = {
			leads_to_tech = GLCM7_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM7_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech GLCM7" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 2025
		show_equipment_icon = yes
		enable_equipments = {
			guided_missile_equipment_7
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @2025 }
		}
		path = {
			leads_to_tech = GLCM8_non_got
			research_cost_coeff = 1
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
	GLCM8_non_got = {
		on_research_complete = { log = "[GetDateText]: [Root.GetName]: add tech GLCM8" }

		allow = {
			ROOT = {
				is_special_project_completed = sp:sp_missile_project_non_got
			}
		}

		research_cost = 1
		start_year = 2035
		show_equipment_icon = yes
		enable_equipments = {
			guided_missile_equipment_8
		}
		folder = {
			name = missile_non_got_folder
			position = { x = 12 y = @2035 }
		}
		categories = {
			CAT_missile
			CAT_cm
			CAT_glcm
			CAT_Military
		}
		special_project_specialization = { specialization_air }
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 2
				check_variable = { gdp_per_capita > 19.999 }
			}
			modifier = {
				factor = 0
				AND = {
					NOT = {
						unique_missile_model_names = yes
					}
					check_variable = { gdp_per_capita < 20 }
				}
			}
		}
	}
}