set_leader_CYR = {

	if = { limit = { has_country_flag = set_Conservative }

		if = { limit = { check_variable = { Conservative_leader = 0 } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Aguila Saleh Issa"
				picture = "Aguila_Saleh_Issa.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Caliphate }
		if = { limit = { check_variable = { Caliphate_leader = 0 } }
			add_to_variable = { Caliphate_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abu Habib al-Libi"
				picture = "Abu_Habib_al_Libi.dds"
				ideology = Caliphate
				traits = {
					salafist_Caliphate
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Caliphate_leader = 1 } }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed al-Menfi"	
				picture = "Mohamed_al_Menfi.dds"	
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Autocracy }
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 0 } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Fariha al-Berkawi"
				picture = "Fariha_al_Berkawi.dds"	
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_oligarchism }
		if = { limit = { check_variable = { oligarchism_leader = 0 } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Basit Igtet"
				picture = "Basit_Igtet.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Populism }
		if = { limit = { check_variable = { Nat_Populism_leader = 0 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ashour Bin Khayal"
				picture = "Ashour_Bin_Khayal.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Khalifa Haftar"
				picture = "Khalifa_Haftar.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
					political_dancer
					ruthless
					pro_american
					pro_russia
					opposes_muslim_brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	
	
	
	
}