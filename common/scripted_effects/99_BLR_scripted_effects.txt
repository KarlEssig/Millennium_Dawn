modify_batka_support = {
	add_to_variable = { batka = modify_batka }

	custom_effect_tooltip = BLR_batka_tt

	clamp_variable = {
		var = batka
		min = 0
		max = 100
	}

	if = {
		limit = {
			OR = {
				has_idea = BLR_batka_very_weak
				has_idea = BLR_batka_weak
				has_idea = BLR_batka_medium
				has_idea = BLR_batka_strong
				has_idea = BLR_batka_very_strong
			}
		}
		hidden_effect = {
			remove_ideas = {
				BLR_batka_very_weak
				BLR_batka_weak
				BLR_batka_medium
				BLR_batka_strong
				BLR_batka_very_strong
			}
		}
	}

	if = {
		limit = { check_variable = { batka > 79 } }
		add_ideas = BLR_batka_very_strong
	}
	else_if = {
		limit = {
			check_variable = { batka < 80 }
			check_variable = { batka > 59 }
		}
		add_ideas = BLR_batka_strong
	}
	else_if = {
		limit = {
			check_variable = { batka < 60 }
			check_variable = { batka > 39 }
		}
		add_ideas = BLR_batka_medium
	}
	else_if = {
		limit = {
			check_variable = { batka < 40 }
			check_variable = { batka > 19 }
		}
		add_ideas = BLR_batka_weak
	}
	else_if = {
		limit = { check_variable = { batka < 20 } }
		add_ideas = BLR_batka_very_weak
	}
}
modify_tracktors_support = {
	add_to_variable = { tracktors = modify_tracktors }

	custom_effect_tooltip = tracktors_TT

	clamp_variable = {
		var = tracktors
		min = 0
		max = 30000
	}
}

modify_white_legion_support = {
	add_to_variable = { white_legion = modify_white_legion }

	custom_effect_tooltip = BLR_white_legions_TT

	clamp_variable = {
		var = white_legion
		min = 0
		max = 4000
	}
}

batka_deleted = {
	if = {
		limit = {
		has_idea = BLR_batka_very_weak
		}
		remove_ideas = BLR_batka_very_weak

	}
	if = {
		limit = {
		has_idea = BLR_batka_weak
		}
		remove_ideas = BLR_batka_weak

	}
		if = {
		limit = {
		has_idea = BLR_batka_medium
		}
		remove_ideas = BLR_batka_medium

	}
	if = {
		limit = {
		has_idea = BLR_batka_very_strong
		}
		remove_ideas = BLR_batka_very_strong

	}
	if = {
		limit = {
		has_idea = BLR_batka_strong
		}
		remove_ideas = BLR_batka_strong
	}
}
KGB_idea_belarus = {
	if = {
		limit = { has_idea = BLR_kgb2 }
		swap_ideas = {
			remove_idea = BLR_kgb2
			add_idea = BLR_kgb3
		}
	}
	else_if = {
		limit = { has_idea = BLR_kgb1 }
		swap_ideas = {
			remove_idea = BLR_kgb1
			add_idea = BLR_kgb2
		}
	}
	else_if = {
		limit = { has_idea = BLR_kgb }
		swap_ideas = {
			remove_idea = BLR_kgb
			add_idea = BLR_kgb1
		}
	}
	else_if = {
		limit = { NOT = { has_idea = BLR_kgb } }
		add_ideas = BLR_kgb1
	}
}
AGRO_idea_belarus = {
	if = {
		limit = { has_idea = BLR_agriculture2 }
		swap_ideas = {
			remove_idea = BLR_agriculture2
			add_idea = BLR_agriculture3
		}
	}
	else_if = {
		limit = { has_idea = BLR_agriculture }
		swap_ideas = {
			remove_idea = BLR_agriculture
			add_idea = BLR_agriculture2
		}
	}
}
BLR_idea_army = {
	if = {
		limit = { has_idea = BLR_reforming_army2 }
		swap_ideas = {
			remove_idea = BLR_reforming_army2
			add_idea = BLR_reforming_army3
		}
	}
	else_if = {
		limit = { has_idea = BLR_reforming_army1 }
		swap_ideas = {
			remove_idea = BLR_reforming_army1
			add_idea = BLR_reforming_army2
		}
	}
	else_if = {
		limit = { has_idea = BLR_reforming_army }
		swap_ideas = {
			remove_idea = BLR_reforming_army
			add_idea = BLR_reforming_army1
		}
	}
	else_if = {
		limit = { has_idea = BLR_outdated_army }
		swap_ideas = {
			remove_idea = BLR_outdated_army
			add_idea = BLR_reforming_army
		}
	}
}
BLR_union_state_pro_rus_revoult = {
	if = {
		limit = {
			SOV = { NOT = { nationalist_fascist_are_in_power = yes } }
		}
		random_list = {
			30 = {
				BLR = {
					batka_deleted = yes
					add_popularity = {
						ideology = communism
						popularity = -0.10
					}
					recalculate_party = yes
					if = { limit = { NOT = { is_in_array = { ruling_party = 15 } } }
						set_temp_variable = { rul_party_temp = 15 }
						change_ruling_party_effect = yes
						set_politics = {
							ruling_party = neutrality
							elections_allowed = yes
						}
					}
					set_temp_variable = { party_index = 15 }
					set_temp_variable = { party_popularity_increase = 0.20 }
					set_temp_variable = { temp_outlook_increase = 0.26 }
					add_relative_party_popularity = yes
					set_temp_variable = { percent_change = 5.2 }
					set_temp_variable = { tag_index = SOV }
					set_temp_variable = { influence_target = BLR }
					change_influence_percentage = yes
				}
			}
			30 = {
				BLR = {
					set_cosmetic_tag = BLR_limonka
					clr_country_flag = dynamic_flag
					batka_deleted = yes
					add_popularity = {
						ideology = communism
						popularity = -0.10
					}
					recalculate_party = yes
					if = { limit = { NOT = { is_in_array = { ruling_party = 21 } } }
						set_temp_variable = { rul_party_temp = 21 }
						change_ruling_party_effect = yes
						set_politics = {
							ruling_party = nationalist
							elections_allowed = yes
						}
					}
					set_temp_variable = { party_index = 21 }
					set_temp_variable = { party_popularity_increase = 0.20 }
					set_temp_variable = { temp_outlook_increase = 0.26 }
					add_relative_party_popularity = yes
					set_temp_variable = { percent_change = 5.2 }
					set_temp_variable = { tag_index = SOV }
					set_temp_variable = { influence_target = BLR }
					change_influence_percentage = yes
				}
			}
			30 = {
				add_opinion_modifier = {
					target = BLR
					modifier = bad_revoult
				}
				reverse_add_opinion_modifier = {
					target = BLR
					modifier = bad_revoult
				}
				BLR = {
					set_temp_variable = { percent_change = -15.2 }
					set_temp_variable = { tag_index = SOV }
					set_temp_variable = { influence_target = BLR }
					change_influence_percentage = yes
				}
			}
		}
	}
	else_if = {
		limit = {
			SOV = { nationalist_fascist_are_in_power = yes }
		}
		random_list = {
			50 = {
				BLR = {
					batka_deleted = yes
					add_popularity = {
						ideology = communism
						popularity = -0.10
					}
					recalculate_party = yes
					if = { limit = { NOT = { is_in_array = { ruling_party = 15 } } }
						set_temp_variable = { rul_party_temp = 15 }
						change_ruling_party_effect = yes
						set_politics = {
							ruling_party = neutrality
							elections_allowed = yes
						}
					}
					set_temp_variable = { party_index = 15 }
					set_temp_variable = { party_popularity_increase = 0.20 }
					set_temp_variable = { temp_outlook_increase = 0.26 }
					add_relative_party_popularity = yes
					set_temp_variable = { percent_change = 5.2 }
					set_temp_variable = { tag_index = SOV }
					set_temp_variable = { influence_target = BLR }
					change_influence_percentage = yes
				}
			}
			50 = {
				BLR = {
					set_cosmetic_tag = BLR_limonka
					clr_country_flag = dynamic_flag
					batka_deleted = yes
					add_popularity = {
						ideology = communism
						popularity = -0.10
					}
					recalculate_party = yes
					if = { limit = { NOT = { is_in_array = { ruling_party = 21 } } }
						set_temp_variable = { rul_party_temp = 21 }
						change_ruling_party_effect = yes
						set_politics = {
							ruling_party = nationalist
							elections_allowed = yes
						}
					}
					set_temp_variable = { party_index = 21 }
					set_temp_variable = { party_popularity_increase = 0.20 }
					set_temp_variable = { temp_outlook_increase = 0.26 }
					add_relative_party_popularity = yes
					set_temp_variable = { percent_change = 5.2 }
					set_temp_variable = { tag_index = SOV }
					set_temp_variable = { influence_target = BLR }
					change_influence_percentage = yes
				}
			}
		}
	}
}

