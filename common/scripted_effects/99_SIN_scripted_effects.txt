#Author: AngriestBird
SIN_calculate_singaporean_trade_agreements = {
	set_variable = { SIN_foreign_influence_strength = signed_trade_agreements }
	set_variable = { SIN_bureaucracy_cost_multiplier_modifier_strength = signed_trade_agreements }
	# Tariff Income Generates in the Money System

	multiply_variable = { SIN_foreign_influence_strength = 10 }
	multiply_variable = { SIN_bureaucracy_cost_multiplier_modifier_strength = 10 }

	multiply_variable = { SIN_foreign_influence_strength = 0.002 }
	multiply_variable = { SIN_bureaucracy_cost_multiplier_modifier_strength = 0.002 }

	clamp_variable = { var = SIN_foreign_influence_strength min = 0 max = 0.30 }
	clamp_variable = { var = SIN_bureaucracy_cost_multiplier_modifier_strength min = 0 max = 0.30 }

	force_update_dynamic_modifier = yes
	hidden_effect = {
		ingame_update_setup = yes
	}
}

SIN_censorship_law_increase = {
	if = { limit = { has_idea = SIN_loose_censorship_idea }
		swap_ideas = {
			remove_idea = SIN_loose_censorship_idea
			add_idea = SIN_medium_censorship_idea
		}
	}
	else_if = { limit = { has_idea = SIN_medium_censorship_idea }
		swap_ideas = {
			remove_idea = SIN_medium_censorship_idea
			add_idea = SIN_harsh_censorship_idea
		}
	}
	else_if = { limit = { has_idea = SIN_harsh_censorship_idea }
		swap_ideas = {
			remove_idea = SIN_harsh_censorship_idea
			add_idea = SIN_extreme_censorship_idea
		}
	}
}

SIN_censorship_law_decrease = {
	if = { limit = { has_idea = SIN_extreme_censorship_idea }
		swap_ideas = {
			remove_idea = SIN_extreme_censorship_idea
			add_idea = SIN_harsh_censorship_idea
		}
	}
	else_if = { limit = { has_idea = SIN_harsh_censorship_idea }
		swap_ideas = {
			remove_idea = SIN_harsh_censorship_idea
			add_idea = SIN_medium_censorship_idea
		}
	}
	else_if = { limit = { has_idea = SIN_medium_censorship_idea }
		swap_ideas = {
			remove_idea = SIN_medium_censorship_idea
			add_idea = SIN_loose_censorship_idea
		}
	}
}