set_leader_CZE = {

	if = { limit = { has_country_flag = set_conservatism }

		if = { limit = { check_variable = { conservatism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Václav Havel"
				picture = "vaclav_havel.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Václav Klaus"
				picture = "Vaclav_Klaus.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					economist
					educator
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mirek Topolánek"
				picture = "topolanek.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					ex_engineer
					businessman
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Petr Fiala"
				picture = "petr_fiala.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					educator
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Stanislav Gross"
				picture = "Stanislav_Gross.dds"
				ideology = socialism
				traits = {
					western_socialism
					union_man
					silent_workhorse
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jiří Paroubek"
				picture = "paroubek.dds"
				ideology = socialism
				traits = {
					western_socialism
					businessman
					economist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Bohuslav Sobotka"
				picture = "Bohuslav_Sobotka.dds"
				ideology = socialism
				traits = {
					western_socialism
					former_finance_minister
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jan Hamáček"
				picture = "hamacek.dds"
				ideology = socialism
				traits = {
					western_socialism
					writer
					former_foreign_minister
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 4 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Michal Šmarda"
				picture = "Michal_Smarda.dds"
				ideology = socialism
				traits = {
					western_socialism
					smooth_talking_charmer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Miroslava Nemcova"
				picture = "miroslava_nemcova.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Miroslav Grebeníček"
				picture = "miroslav_grebenicek.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
					communist_sympathizer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Communist-State_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Vojtěch Filip"
				picture = "vojtech_filip.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
					communist_sympathizer
					lawyer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Communist-State_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Kateřina Konečná"
				picture = "katerina_konecna.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
					communist_sympathizer
					ex_engineer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_liberalism date < 2011.1.1 }
		if = { limit = { check_variable = { liberalism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Karel Kühnl"
				picture = "karel_kuhnl.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					lawyer
					diplomat
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Hana Marvanová"
				picture = "hana_morvonova.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					lawyer
					triumphant_revolutionary
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ivan Pilip"
				picture = "ivan_pillip.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					economist
					businessman
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Petr Mareš"
				picture = "petr_mares.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					diplomat
					educator
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_liberalism date > 2011.1.1 }
		if = { limit = { check_variable = { liberalism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Karel Schwarzenberg"
				picture = "Karel_Schwarzenberg.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					diplomat
					triumphant_revolutionary
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Miroslav Kalousek"
				picture = "Miroslav_Kalousek.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					political_dancer
					technocrat
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jiří Pospíšil"
				picture = "Jiri_Pospisil.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					lawyer
					judiciary
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Markéta Pekarová Adamová"
				picture = "Marketa_Pekarova_Adamova.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					businessman
					educator
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Autocracy }
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Andrej Babiš"
				picture = "andrej_babis.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
					former_finance_minister
					economist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jan Kasal"
				picture = "jan_kasal.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					ex_engineer
					career_politician
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Cyril Svoboda"
				picture = "Cyril_Svoboda.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					lawyer
					diplomat
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jiří Čunek"
				picture = "Jiri_Cunek.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					economist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Pavel Bělobrádek"
				picture = "pavel_belohradek.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					doctor
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ivan Bartoš"
				picture = "ivan_bartos.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
					economist
					agronomist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Lukáš Černohorský"
				picture = "lukas_cernohorsky.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
					economist
					agronomist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
}