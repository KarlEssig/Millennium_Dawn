set_leader_TRA = {

	if = { limit = { has_country_flag = set_liberalism }

		if = { limit = { check_variable = { liberalism_leader = 0 } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Dan Barna"
				picture = "dan_barna.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Nicusor Dan"
				picture = "nicusor_dan.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_conservatism }
		if = { limit = { check_variable = { conservatism_leader = 0 } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Emil Boc"
				picture = "emil_boc.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Klaus Iohanis"
				picture = "Klaus_Iohannis.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Tobias Rieper"
				picture = "tobias_rieper.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Populism }
		if = { limit = { check_variable = { Nat_Populism_leader = 0 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "George Simion"
				picture = "george_simion.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
}