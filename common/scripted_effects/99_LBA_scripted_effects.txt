libya_update_nationalisation = {
	set_variable = { oil_nationalisation_resource_effect = lba_oil_nationalisation }
	set_variable = { oil_nationalisation_oil_income_effect = lba_oil_nationalisation }
	set_temp_variable = { lba_oil_nationalisation_temp = lba_oil_nationalisation }
	multiply_temp_variable = { lba_oil_nationalisation_temp = -1 }
	set_variable = { oil_nationalisation_productivity_effect = lba_oil_nationalisation_temp }
	force_update_dynamic_modifier = yes
	clear_variable = lba_oil_nationalisation_temp

	set_variable = { lba_oil_nationalisation_temp = lba_oil_nationalisation }
	multiply_variable = { lba_oil_nationalisation_temp = -1 }
	set_variable = { oil_nationalisation_productivity_effect = lba_oil_nationalisation_temp }
	set_variable = { oil_nationalisation_export_effect = lba_oil_nationalisation_temp }
	set_variable = { oil_nationalisation_income = 0.014 }
	multiply_variable = { oil_nationalisation_income = lba_oil_nationalisation }
	force_update_dynamic_modifier = yes
	clear_variable = lba_oil_nationalisation_temp

	set_variable = { libya_oil_nationalisation_additional_income = resource_produced@oil }
	multiply_variable = { libya_oil_nationalisation_additional_income = oil_nationalisation_income }
}

libya_increase_nationalisation_5 = {
	custom_effect_tooltip = libya_increase_nationalisation_5_tt
	add_to_variable = { lba_oil_nationalisation = 0.05 }
	clamp_variable = {
		var = lba_oil_nationalisation
		max = 1
	}
	libya_update_nationalisation = yes
}

libya_decrease_nationalisation_5 = {
	custom_effect_tooltip = libya_decrease_nationalisation_5_tt
	add_to_variable = { lba_oil_nationalisation = -0.05 }
	clamp_variable = {
		var = lba_oil_nationalisation
		min = 0
	}
	libya_update_nationalisation = yes
}

libya_find_next_gaddafi_heir = {
	clr_country_flag = muhammad_muammar_gaddafi_heir
	clr_country_flag = saif_al_islam_gaddafi_heir
	clr_country_flag = al_saadi_gaddafi_heir
	clr_country_flag = mutassim_gaddafi_heir
	clr_country_flag = hannibal_gaddafi_heir
	clr_country_flag = ayesha_gaddafi_heir
	clr_country_flag = saif_al_arab_gaddafi_heir
	clr_country_flag = khamis_gaddafi_heir

	if = {
		limit = {
			NOT = {
				has_country_flag = muhammad_muammar_gaddafi_exile
				has_country_flag = muhammad_muammar_gaddafi_dead
			}
			check_variable = { Autocracy_leader < 1 }
		}
		set_country_flag = muhammad_muammar_gaddafi_heir
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = saif_al_islam_gaddafi_exile
				has_country_flag = saif_al_islam_gaddafi_dead
			}
			check_variable = { Autocracy_leader < 2 }
		}
		set_country_flag = saif_al_islam_gaddafi_heir
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = al_saadi_gaddafi_exile
				has_country_flag = al_saadi_gaddafi_dead
			}
			check_variable = { Autocracy_leader < 3 }
		}
		set_country_flag = al_saadi_gaddafi_heir
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = mutassim_gaddafi_exile
				has_country_flag = mutassim_gaddafi_dead
			}
			check_variable = { Autocracy_leader < 4 }
		}
		set_country_flag = mutassim_gaddafi_heir
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = hannibal_gaddafi_exile
				has_country_flag = hannibal_gaddafi_dead
			}
			check_variable = { Autocracy_leader < 5 }
		}
		set_country_flag = hannibal_gaddafi_heir
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = ayesha_gaddafi_exile
				has_country_flag = ayesha_gaddafi_dead
			}
			check_variable = { Autocracy_leader < 6 }
		}
		set_country_flag = ayesha_gaddafi_heir
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = saif_al_arab_gaddafi_exile
				has_country_flag = saif_al_arab_gaddafi_dead
			}
			check_variable = { Autocracy_leader < 7 }
		}
		set_country_flag = saif_al_arab_gaddafi_heir
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = khamis_gaddafi_exile
				has_country_flag = khamis_gaddafi_dead
			}
			check_variable = { Autocracy_leader < 7 }
		}
		set_country_flag = khamis_gaddafi_heir
	}

}

update_gaddafi_family_ideas = {
	hidden_effect = {
		remove_ideas = LBA_head_of_gptc_idea_version
		remove_ideas = LBA_uninterested_in_politics_idea_version
		remove_ideas = LBA_master_maneuverer_idea_version
		remove_ideas = LBA_vain_football_star_idea_version
		remove_ideas = LBA_substance_abuser_idea_version
		remove_ideas = LBA_77th_tank_battalion_idea_version
		remove_ideas = LBA_suspected_revolutionary_idea_version
		remove_ideas = LBA_playboy_lifestyle_idea_version
		remove_ideas = LBA_king_of_coke_idea_version
		remove_ideas = LBA_bachelor_of_marine_navigation_idea_version
		remove_ideas = LBA_master_of_shipping_economics_and_logistics_idea_version
		remove_ideas = LBA_graduate_of_kuznetsov_idea_version
		remove_ideas = LBA_national_security_advisor_idea_version
		remove_ideas = LBA_scholarly_challenged_idea_version
		remove_ideas = LBA_claudia_schiffer_of_north_africa_idea_version
		set_variable = { gaddafi_family_instability_decay = 0 }
		if = {
			limit = {
				NOT = { has_country_flag = muhammad_muammar_gaddafi_exile }
				NOT = { has_country_flag = muhammad_muammar_gaddafi_dead }
				NOT = { has_country_leader = { name = "Muhammad Muammar Gaddafi" ruling_only = yes } }
			}
			add_to_variable = { gaddafi_family_instability_decay = -0.25 }
			if = {
				limit = { has_country_flag = muhammad_muammar_gaddafi_head_of_GPTC }
				add_ideas = LBA_head_of_gptc_idea_version
			}
			if = {
				limit = { has_country_flag = muhammad_muammar_gaddafi_uninterested_in_politics }
				add_ideas = LBA_uninterested_in_politics_idea_version
			}
			if = {
				limit = { has_country_flag = muhammad_muammar_gaddafi_king_of_coke }
				add_ideas = LBA_king_of_coke_idea_version
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = saif_al_islam_gaddafi_exile }
				NOT = { has_country_flag = saif_al_islam_gaddafi_dead }
				NOT = { has_country_leader = { name = "Saif al-Islam Gaddafi" ruling_only = yes } }
			}
			add_to_variable = { gaddafi_family_instability_decay = -0.25 }
			if = {
				limit = { has_country_flag = saif_al_islam_gaddafi_master_maneuverer }
				add_ideas = LBA_master_maneuverer_idea_version
			}
			if = {
				limit = { has_country_flag = saif_al_islam_gaddafi_geopolitical_thinker }
				add_ideas = LBA_geopolitical_thinker_idea_version
			}
			if = {
				limit = { has_country_flag = saif_al_islam_gaddafi_democratic_reformer }
				add_ideas = LBA_democratic_reformer_idea_version
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = al_saadi_gaddafi_exile }
				NOT = { has_country_flag = al_saadi_gaddafi_dead }
				NOT = { has_country_leader = { name = "Al-Saadi Gaddafi" ruling_only = yes } }
			}
			add_to_variable = { gaddafi_family_instability_decay = -0.25 }
			if = {
				limit = { has_country_flag = al_saadi_gaddafi_vain_football_star }
				add_ideas = LBA_vain_football_star_idea_version
			}
			if = {
				limit = { has_country_flag = al_saadi_gaddafi_substance_abuser }
				add_ideas = LBA_substance_abuser_idea_version
			}
			if = {
				limit = { has_country_flag = al_saadi_gaddafi_playboy_lifestyle }
				add_ideas = LBA_playboy_lifestyle_idea_version
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = mutassim_gaddafi_exile }
				NOT = { has_country_flag = mutassim_gaddafi_dead }
				NOT = { has_country_leader = { name = "Al-Saadi Gaddafi" ruling_only = yes } }
			}
			add_to_variable = { gaddafi_family_instability_decay = -0.25 }
			if = {
				limit = { has_country_flag = mutassim_gaddafi_77th_tank_battalion }
				add_ideas = LBA_77th_tank_battalion_idea_version
			}
			if = {
				limit = { has_country_flag = mutassim_gaddafi_suspected_revolutionary }
				add_ideas = LBA_suspected_revolutionary_idea_version
			}
			if = {
				limit = { has_country_flag = mutassim_gaddafi_king_of_coke }
				add_ideas = LBA_king_of_coke_idea_version
			}
			if = {
				limit = { has_country_flag = mutassim_gaddafi_national_security_advisor }
				add_ideas = LBA_national_security_advisor_idea_version
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = hannibal_gaddafi_exile }
				NOT = { has_country_flag = hannibal_gaddafi_dead }
				NOT = { has_country_leader = { name = "Hannibal Gaddafi" ruling_only = yes } }
			}
			add_to_variable = { gaddafi_family_instability_decay = -0.25 }
			if = {
				limit = { has_country_flag = hannibal_gaddafi_bachelor_of_marine_navigation }
				add_ideas = LBA_bachelor_of_marine_navigation_idea_version
			}
			if = {
				limit = { has_country_flag = hannibal_gaddafi_master_of_shipping_economics_and_logistics }
				add_ideas = LBA_master_of_shipping_economics_and_logistics_idea_version
			}
			if = {
				limit = { has_country_flag = hannibal_gaddafi_graduate_of_kuznetsov }
				add_ideas = LBA_graduate_of_kuznetsov_idea_version
			}
			if = {
				limit = { has_country_flag = hannibal_gaddafi_scholarly_challenged }
				add_ideas = LBA_scholarly_challenged_idea_version
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = ayesha_gaddafi_exile }
				NOT = { has_country_flag = ayesha_gaddafi_dead }
				NOT = { has_country_leader = { name = "Ayesha Gaddafi" ruling_only = yes } }
			}
			add_to_variable = { gaddafi_family_instability_decay = -0.25 }
			if = {
				limit = { has_country_flag = ayesha_gaddafi_claudia_schiffer }
				add_ideas = LBA_claudia_schiffer_of_north_africa_idea_version
			}
			if = {
				limit = { has_country_flag = ayesha_gaddafi_un_goodwill_ambassador }
				add_ideas = LBA_un_goodwill_ambassador_idea_version
			}
			if = {
				limit = { has_country_flag = ayesha_gaddafi_amazon_of_libya }
				add_ideas = LBA_amazon_of_libya_idea_version
			}
			if = {
				limit = { has_country_flag = ayesha_gaddafi_inexperienced_general }
				add_ideas = LBA_inexperienced_general_idea_version
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = saif_al_arab_gaddafi_exile }
				NOT = { has_country_flag = saif_al_arab_gaddafi_dead }
				NOT = { has_country_leader = { name = "Saif al-Arab Gaddafi" ruling_only = yes } }
			}
			add_to_variable = { gaddafi_family_instability_decay = -0.25 }
			if = {
				limit = { has_country_flag = saif_al_arab_gaddafi_convicted_criminal }
				add_ideas = LBA_convicted_criminal_idea_version
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = khamis_gaddafi_exile }
				NOT = { has_country_flag = khamis_gaddafi_dead }
				NOT = { has_country_leader = { name = "Khamis Gaddafi" ruling_only = yes } }
			}
			if = {
				limit = { NOT = { has_country_flag = khamis_gaddafi_underage } }
				add_to_variable = { gaddafi_family_instability_decay = -0.25 }
			}
			if = {
				limit = { has_country_flag = khamis_gaddafi_khamis_brigade }
				add_ideas = LBA_khamis_brigade_idea_version
			}
			if = {
				limit = { has_country_flag = khamis_gaddafi_war_industrialist }
				add_ideas = LBA_war_industrialist_idea_version
			}
			if = {
				limit = { has_country_flag = khamis_gaddafi_captain_of_industry }
				add_ideas = LBA_captain_of_industry_idea_version
			}
		}
	}
}

libya_gaddafi_family_modify_instability = {
	custom_effect_tooltip = libya_gaddafi_family_modify_instability_tt
	add_to_variable = { gaddafi_family_instability = family_instability_change }
	clamp_variable = { var = gaddafi_family_instability min = 0 }
	set_variable = { gaddafi_family_stability_effect = gaddafi_family_instability }
	multiply_variable = { gaddafi_family_stability_effect = -0.005 }
	force_update_dynamic_modifier = yes
}

#set_temp_variable = { loyalty_change = 0.XX }
libya_change_tripolitania_loyalty = {
	if = {
		limit = {
			NOT = { has_country_flag = LBA_tribalism_disabled }
		}
		custom_effect_tooltip = libya_change_tripolitania_loyalty_tt
		add_to_variable = { tripolitania_loyalty = loyalty_change }
		clamp_variable = { var = tripolitania_loyalty min = 0 max = 1 }
	}
}
libya_change_cyrenaica_loyalty = {
	if = {
		limit = {
			NOT = { has_country_flag = LBA_tribalism_disabled }
		}
		custom_effect_tooltip = libya_change_cyrenaica_loyalty_tt
		add_to_variable = { cyrenaica_loyalty = loyalty_change }
		clamp_variable = { var = cyrenaica_loyalty min = 0 max = 1 }
	}
}
libya_change_fezzan_loyalty = {
	if = {
		limit = {
			NOT = { has_country_flag = LBA_tribalism_disabled }
		}
		custom_effect_tooltip = libya_change_fezzan_loyalty_tt
		add_to_variable = { fezzan_loyalty = loyalty_change }
		clamp_variable = { var = fezzan_loyalty min = 0 max = 1 }
	}
}

libya_tribal_loyalty_from_building = {
	if = {
		limit = {
			OR = {
				state = 393
				state = 391
				state = 919
			}
		}
		ROOT = {
			set_temp_variable = { loyalty_change = 0.10 }
			libya_change_tripolitania_loyalty = yes
			newline = yes
			set_temp_variable = { loyalty_change = -0.10 }
			libya_change_cyrenaica_loyalty = yes
			newline = yes
			set_temp_variable = { loyalty_change = -0.10 }
			libya_change_fezzan_loyalty = yes
			libya_update_tribalism_modifiers = yes
		}
	}
	else_if = {
		limit = {
			OR = {
				state = 397
				state = 395
				state = 396
				state = 579
				state = 1101
				state = 1100
			}
		}
		ROOT = {
			set_temp_variable = { loyalty_change = -0.10 }
			libya_change_tripolitania_loyalty = yes
			newline = yes
			set_temp_variable = { loyalty_change = 0.10 }
			libya_change_cyrenaica_loyalty = yes
			newline = yes
			set_temp_variable = { loyalty_change = -0.10 }
			libya_change_fezzan_loyalty = yes
			libya_update_tribalism_modifiers = yes
		}
	}
	else_if = {
		limit = {
			OR = {
				state = 392
				state = 918
				state = 920
			}
		}
		ROOT = {
			set_temp_variable = { loyalty_change = -0.10 }
			libya_change_tripolitania_loyalty = yes
			newline = yes
			set_temp_variable = { loyalty_change = -0.10 }
			libya_change_cyrenaica_loyalty = yes
			newline = yes
			set_temp_variable = { loyalty_change = 0.10 }
			libya_change_fezzan_loyalty = yes
			libya_update_tribalism_modifiers = yes
		}
	}
}

libya_increase_loyalty_single_offender = {
	if = {
		limit = { has_country_flag = LBA_tribalism_tripolitanian_offender }
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_tripolitania_loyalty = yes
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_cyrenaica_loyalty = yes
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_fezzan_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_cyrenaican_offender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_tripolitania_loyalty = yes
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_cyrenaica_loyalty = yes
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_fezzan_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_fezzani_offender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_tripolitania_loyalty = yes
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_cyrenaica_loyalty = yes
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_fezzan_loyalty = yes
	}
	libya_update_tribalism_modifiers = yes
	clr_country_flag = LBA_tribalism_tripolitanian_offender
	clr_country_flag = LBA_tribalism_cyrenaican_offender
	clr_country_flag = LBA_tribalism_fezzani_offender
}

libya_decrease_loyalty_single_offender = {
	if = {
		limit = { has_country_flag = LBA_tribalism_tripolitanian_offender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_tripolitania_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_cyrenaican_offender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_cyrenaica_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_fezzani_offender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_fezzan_loyalty = yes
	}
	libya_update_tribalism_modifiers = yes
	clr_country_flag = LBA_tribalism_tripolitanian_offender
	clr_country_flag = LBA_tribalism_cyrenaican_offender
	clr_country_flag = LBA_tribalism_fezzani_offender
}

libya_tribal_loyalty_offender_over_defender = {
	if = {
		limit = { has_country_flag = LBA_tribalism_tripolitanian_offender }
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_tripolitania_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_cyrenaican_offender }
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_cyrenaica_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_fezzani_offender }
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_fezzan_loyalty = yes
	}
	if = {
		limit = { has_country_flag = LBA_tribalism_tripolitanian_defender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_tripolitania_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_cyrenaican_defender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_cyrenaica_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_fezzani_defender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_fezzan_loyalty = yes
	}
	libya_update_tribalism_modifiers = yes
	clr_country_flag = LBA_tribalism_tripolitanian_offender
	clr_country_flag = LBA_tribalism_cyrenaican_offender
	clr_country_flag = LBA_tribalism_fezzani_offender
	clr_country_flag = LBA_tribalism_tripolitanian_defender
	clr_country_flag = LBA_tribalism_cyrenaican_defender
	clr_country_flag = LBA_tribalism_fezzani_defender
}

libya_tribal_loyalty_defender_over_offender = {
	if = {
		limit = { has_country_flag = LBA_tribalism_tripolitanian_defender }
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_tripolitania_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_cyrenaican_defender }
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_cyrenaica_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_fezzani_defender }
		set_temp_variable = { loyalty_change = 0.10 }
		libya_change_fezzan_loyalty = yes
	}
	if = {
		limit = { has_country_flag = LBA_tribalism_tripolitanian_offender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_tripolitania_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_cyrenaican_offender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_cyrenaica_loyalty = yes
	}
	else_if = {
		limit = { has_country_flag = LBA_tribalism_fezzani_offender }
		set_temp_variable = { loyalty_change = -0.10 }
		libya_change_fezzan_loyalty = yes
	}
	libya_update_tribalism_modifiers = yes
	clr_country_flag = LBA_tribalism_tripolitanian_offender
	clr_country_flag = LBA_tribalism_cyrenaican_offender
	clr_country_flag = LBA_tribalism_fezzani_offender
	clr_country_flag = LBA_tribalism_tripolitanian_defender
	clr_country_flag = LBA_tribalism_cyrenaican_defender
	clr_country_flag = LBA_tribalism_fezzani_defender
}

libya_update_tribalism_modifiers = {
	if = {
		limit = {
			NOT = { has_country_flag = LBA_tribalism_disabled }
		}
		hidden_effect = {
			every_owned_state = {
				if = {
					limit = {
						has_dynamic_modifier = {
							modifier = libya_tribalism_loyal_dynamic_modifier
						}
					}
					remove_dynamic_modifier = { modifier = libya_tribalism_loyal_dynamic_modifier }
				}
				if = {
					limit = {
						has_dynamic_modifier = {
							modifier = libya_tribalism_neutral_dynamic_modifier
						}
					}
					remove_dynamic_modifier = { modifier = libya_tribalism_neutral_dynamic_modifier }
				}
				if = {
					limit = {
						has_dynamic_modifier = {
							modifier = libya_tribalism_bad_dynamic_modifier
						}
					}
					remove_dynamic_modifier = { modifier = libya_tribalism_bad_dynamic_modifier }
				}
			}
			every_owned_state = {
				limit = {
					OR = {
						state = 393
						state = 391
						state = 919
					}
					is_owned_by = ROOT
				}
				if = {
					limit = { ROOT = { check_variable = { tripolitania_loyalty > 0.499 } } }
					add_dynamic_modifier = { modifier = libya_tribalism_loyal_dynamic_modifier }
				}
				else_if = {
					limit = { ROOT = { check_variable = { tripolitania_loyalty > 0.199 } } }
					add_dynamic_modifier = { modifier = libya_tribalism_neutral_dynamic_modifier }
				}
				else = {
					add_dynamic_modifier = { modifier = libya_tribalism_bad_dynamic_modifier }
				}
			}
			every_owned_state = {
				limit = {
					OR = {
						state = 397
						state = 395
						state = 396
						state = 579
						state = 1101
						state = 1100
					}
					is_owned_by = ROOT
				}
				if = {
					limit = { ROOT = { check_variable = { cyrenaica_loyalty > 0.499 } } }
					add_dynamic_modifier = { modifier = libya_tribalism_loyal_dynamic_modifier }
				}
				else_if = {
					limit = { ROOT = { check_variable = { cyrenaica_loyalty > 0.199 } } }
					add_dynamic_modifier = { modifier = libya_tribalism_neutral_dynamic_modifier }
				}
				else = {
					add_dynamic_modifier = { modifier = libya_tribalism_bad_dynamic_modifier }
				}
			}
			every_owned_state = {
				limit = {
					OR = {
						state = 392
						state = 918
						state = 920
					}
					is_owned_by = ROOT
				}
				if = {
					limit = { ROOT = { check_variable = { fezzan_loyalty > 0.499 } } }
					add_dynamic_modifier = { modifier = libya_tribalism_loyal_dynamic_modifier }
				}
				else_if = {
					limit = { ROOT = { check_variable = { fezzan_loyalty > 0.199 } } }
					add_dynamic_modifier = { modifier = libya_tribalism_neutral_dynamic_modifier }
				}
				else = {
					add_dynamic_modifier = { modifier = libya_tribalism_bad_dynamic_modifier }
				}
			}
		}
	}
}

lba_disable_tribalism = {
	every_owned_state = {
		remove_dynamic_modifier = { modifier = libya_tribalism_loyal_dynamic_modifier }
		remove_dynamic_modifier = { modifier = libya_tribalism_neutral_dynamic_modifier }
		remove_dynamic_modifier = { modifier = libya_tribalism_bad_dynamic_modifier }
	}
}

lba_increase_reform = {
	custom_effect_tooltip = lba_increase_reform_tt
	add_to_variable = { libya_green_book_reform = 1 }
}

lba_decrease_reform = {
	custom_effect_tooltip = lba_decrease_reform_tt
	add_to_variable = { libya_green_book_reform = -1 }
}

libya_update_casablanca_accords_acceptance = {
	#Morocco
	set_variable = { Morocco_acceptance = 0 }
	if = {
		limit = { MOR = { is_subject_of = ROOT } }
		set_variable = { Morocco_acceptance = 100 }
	}
	else = {
		if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_annexation }
			add_to_variable = { Morocco_acceptance = 95 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_autonomy }
			add_to_variable = { Morocco_acceptance = 40 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_independence }
			add_to_variable = { Morocco_acceptance = 5 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_mauritania }
			add_to_variable = { Morocco_acceptance = 5 }
		}
		if = {
			limit = {
				is_in_array = {
					array = MOR.influence_array
					value = ROOT.id
				}
			}
			for_each_loop = {
				array = MOR.influence_array
				index = i
				if = {
					limit = { check_variable = { MOR.influence_array^i = ROOT.id } }
					add_to_variable = { ROOT.Morocco_acceptance = MOR.influence_array_val^i }
				}
			}
		}
		if = {
			limit = {
				MOR = {
					has_opinion = {
						target = ROOT
						value > 150
					}
				}
			}
			multiply_variable = { Morocco_acceptance = 1.3 }
		}
		else_if = {
			limit = {
				MOR = {
					has_opinion = {
						target = ROOT
						value > 100
					}
				}
			}
			multiply_variable = { Morocco_acceptance = 1.2 }
		}
		else_if = {
			limit = {
				MOR = {
					has_opinion = {
						target = ROOT
						value > 50
					}
				}
			}
			multiply_variable = { Morocco_acceptance = 1.1 }
		}
		if = {
			limit = {
				OR = {
					AND = {
						ROOT = { has_government = democratic }
						MOR = { has_government = democratic }
					}
					AND = {
						ROOT = { has_government = communism }
						MOR = { has_government = communism }
					}
					AND = {
						ROOT = { has_government = fascism }
						MOR = { has_government = fascism }
					}
					AND = {
						ROOT = { has_government = neutrality }
						MOR = { has_government = neutrality }
					}
					AND = {
						ROOT = { has_government = nationalist }
						MOR = { has_government = nationalist }
					}
				}
			}
			multiply_variable = { Morocco_acceptance = 1.2 }
		}
		clamp_variable = {
			var = Morocco_acceptance
			max = 100
		}
		set_variable = { Morocco_acceptance_neg = 100 }
		subtract_from_variable = { Morocco_acceptance_neg = Morocco_acceptance }
	}
	#Sahrawi
	set_variable = { Sahrawi_acceptance = 0 }
	if = {
		limit = { SHA = { is_subject_of = ROOT } }
		set_variable = { Sahrawi_acceptance = 100 }
	}
	else = {
		if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_annexation }
			add_to_variable = { Sahrawi_acceptance = 5 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_autonomy }
			add_to_variable = { Sahrawi_acceptance = 20 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_independence }
			add_to_variable = { Sahrawi_acceptance = 95 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_mauritania }
			add_to_variable = { Sahrawi_acceptance = 5 }
		}
		if = {
			limit = {
				is_in_array = {
					array = SHA.influence_array
					value = ROOT.id
				}
			}
			for_each_loop = {
				array = SHA.influence_array
				index = i
				if = {
					limit = { check_variable = { MOR.influence_array^i = ROOT.id } }
					add_to_variable = { ROOT.Sahrawi_acceptance = SHA.influence_array_val^i }
				}
			}
		}
		if = {
			limit = {
				SHA = {
					has_opinion = {
						target = ROOT
						value > 150
					}
				}
			}
			multiply_variable = { Sahrawi_acceptance = 1.3 }
		}
		else_if = {
			limit = {
				SHA = {
					has_opinion = {
						target = ROOT
						value > 100
					}
				}
			}
			multiply_variable = { Sahrawi_acceptance = 1.2 }
		}
		else_if = {
			limit = {
				SHA = {
					has_opinion = {
						target = ROOT
						value > 50
					}
				}
			}
			multiply_variable = { Sahrawi_acceptance = 1.1 }
		}
		if = {
			limit = {
				OR = {
					AND = {
						ROOT = { has_government = democratic }
						SHA = { has_government = democratic }
					}
					AND = {
						ROOT = { has_government = communism }
						SHA = { has_government = communism }
					}
					AND = {
						ROOT = { has_government = fascism }
						SHA = { has_government = fascism }
					}
					AND = {
						ROOT = { has_government = neutrality }
						SHA = { has_government = neutrality }
					}
					AND = {
						ROOT = { has_government = nationalist }
						SHA = { has_government = nationalist }
					}
				}
			}
			multiply_variable = { Sahrawi_acceptance = 1.2 }
		}
		clamp_variable = {
			var = Sahrawi_acceptance
			max = 100
		}
		set_variable = { Sahrawi_acceptance_neg = 100 }
		subtract_from_variable = { Sahrawi_acceptance_neg = Sahrawi_acceptance }
	}
	#Algeria
	set_variable = { Algeria_acceptance = 0 }
	if = {
		limit = { ALG = { is_subject_of = ROOT } }
		set_variable = { Algeria_acceptance = 100 }
	}
	else = {
		if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_annexation }
			add_to_variable = { Algeria_acceptance = 10 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_autonomy }
			add_to_variable = { Algeria_acceptance = 30 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_independence }
			add_to_variable = { Algeria_acceptance = 80 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_mauritania }
			add_to_variable = { Algeria_acceptance = 15 }
		}
		if = {
			limit = {
				is_in_array = {
					array = ALG.influence_array
					value = ROOT.id
				}
			}
			for_each_loop = {
				array = ALG.influence_array
				index = i
				if = {
					limit = { check_variable = { ALG.influence_array^i = ROOT.id } }
					add_to_variable = { ROOT.Algeria_acceptance = ALG.influence_array_val^i }
				}
			}
		}
		if = {
			limit = {
				ALG = {
					has_opinion = {
						target = ROOT
						value > 150
					}
				}
			}
			multiply_variable = { Algeria_acceptance = 1.3 }
		}
		else_if = {
			limit = {
				ALG = {
					has_opinion = {
						target = ROOT
						value > 100
					}
				}
			}
			multiply_variable = { Algeria_acceptance = 1.2 }
		}
		else_if = {
			limit = {
				ALG = {
					has_opinion = {
						target = ROOT
						value > 50
					}
				}
			}
			multiply_variable = { Algeria_acceptance = 1.1 }
		}
		if = {
			limit = {
				OR = {
					AND = {
						ROOT = { has_government = democratic }
						ALG = { has_government = democratic }
					}
					AND = {
						ROOT = { has_government = communism }
						ALG = { has_government = communism }
					}
					AND = {
						ROOT = { has_government = fascism }
						ALG = { has_government = fascism }
					}
					AND = {
						ROOT = { has_government = neutrality }
						ALG = { has_government = neutrality }
					}
					AND = {
						ROOT = { has_government = nationalist }
						ALG = { has_government = nationalist }
					}
				}
			}
			multiply_variable = { Algeria_acceptance = 1.2 }
		}
		clamp_variable = {
			var = Algeria_acceptance
			max = 100
		}
		set_variable = { Algeria_acceptance_neg = 100 }
		subtract_from_variable = { Algeria_acceptance_neg = Algeria_acceptance }
	}
	#Mauritania
	set_variable = { Mauritania_acceptance = 0 }
	if = {
		limit = { MAU = { is_subject_of = ROOT } }
		set_variable = { Mauritania_acceptance = 100 }
	}
	else = {
		if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_annexation }
			add_to_variable = { Mauritania_acceptance = 50 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_autonomy }
			add_to_variable = { Mauritania_acceptance = 50 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_independence }
			add_to_variable = { Mauritania_acceptance = 50 }
		}
		else_if = {
			limit = { has_country_flag = libya_western_sahara_peace_plan_mauritania }
			if = {
				limit = { MAU = { has_government = nationalist } }
				add_to_variable = { Mauritania_acceptance = 100 }
			}
			else = {
				add_to_variable = { Mauritania_acceptance = 10 }
			}
		}
		if = {
			limit = {
				is_in_array = {
					array = MAU.influence_array
					value = ROOT.id
				}
			}
			for_each_loop = {
				array = MAU.influence_array
				index = i
				if = {
					limit = { check_variable = { MAU.influence_array^i = ROOT.id } }
					add_to_variable = { ROOT.Mauritania_acceptance = MAU.influence_array_val^i }
				}
			}
		}
		if = {
			limit = {
				MAU = {
					has_opinion = {
						target = ROOT
						value > 150
					}
				}
			}
			multiply_variable = { Mauritania_acceptance = 1.3 }
		}
		else_if = {
			limit = {
				MAU = {
					has_opinion = {
						target = ROOT
						value > 100
					}
				}
			}
			multiply_variable = { Mauritania_acceptance = 1.2 }
		}
		else_if = {
			limit = {
				MAU = {
					has_opinion = {
						target = ROOT
						value > 50
					}
				}
			}
			multiply_variable = { Mauritania_acceptance = 1.1 }
		}
		if = {
			limit = {
				OR = {
					AND = {
						ROOT = { has_government = democratic }
						MAU = { has_government = democratic }
					}
					AND = {
						ROOT = { has_government = communism }
						MAU = { has_government = communism }
					}
					AND = {
						ROOT = { has_government = fascism }
						MAU = { has_government = fascism }
					}
					AND = {
						ROOT = { has_government = neutrality }
						MAU = { has_government = neutrality }
					}
					AND = {
						ROOT = { has_government = nationalist }
						MAU = { has_government = nationalist }
					}
				}
			}
			multiply_variable = { Mauritania_acceptance = 1.2 }
		}
		clamp_variable = {
			var = Mauritania_acceptance
			max = 100
		}
		set_variable = { Mauritania_acceptance_neg = 100 }
		subtract_from_variable = { Mauritania_acceptance_neg = Mauritania_acceptance }
	}
}

libya_upgrade_railway_idea_level = {
	if = {
		limit = { has_idea = LBA_state_supported_railways }
		swap_ideas = {
			remove_idea = LBA_state_supported_railways
			add_idea = LBA_state_supported_railways_functional_neighbour_1
		}
	}
	else_if = {
		limit = { has_idea = LBA_state_supported_railways_functional_neighbour_1 }
		swap_ideas = {
			remove_idea = LBA_state_supported_railways_functional_neighbour_1
			add_idea = LBA_state_supported_railways_functional_neighbour_2
		}
	}
	else_if = {
		limit = { has_idea = LBA_state_supported_railways_functional_neighbour_2 }
		swap_ideas = {
			remove_idea = LBA_state_supported_railways_functional_neighbour_2
			add_idea = LBA_state_supported_railways_functional_neighbour_3
		}
	}
	else_if = {
		limit = { has_idea = LBA_state_supported_railways_functional_neighbour_3 }
		swap_ideas = {
			remove_idea = LBA_state_supported_railways_functional_neighbour_3
			add_idea = LBA_state_supported_railways_functional_neighbour_4
		}
	}
}

libya_upgrade_gmmr_idea_level = {
	if = {
		limit = { has_idea = LBA_great_man_made_river_phase2 }
		swap_ideas = {
			remove_idea = LBA_great_man_made_river_phase2
			add_idea = LBA_great_man_made_river_phase3
		}
	}
	else_if = {
		limit = { has_idea = LBA_great_man_made_river_phase3 }
		swap_ideas = {
			remove_idea = LBA_great_man_made_river_phase3
			add_idea = LBA_great_man_made_river_phase41
		}
	}
	else_if = {
		limit = { has_idea = LBA_great_man_made_river_phase41 }
		swap_ideas = {
			remove_idea = LBA_great_man_made_river_phase41
			add_idea = LBA_great_man_made_river_phase42
		}
	}
	else_if = {
		limit = { has_idea = LBA_great_man_made_river_phase42 }
		swap_ideas = {
			remove_idea = LBA_great_man_made_river_phase42
			add_idea = LBA_great_man_made_river_phase5
		}
	}
}