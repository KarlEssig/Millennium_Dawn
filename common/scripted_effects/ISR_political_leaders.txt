set_leader_ISR = {
	if = { limit = { has_country_flag = set_Western_Autocracy }
		if = { limit = { check_variable = { Western_Autocracy_leader = 0 } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Naftali Bennett"
				picture = "naftali_bennett.dds"
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
					businessman
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 } 
		}
		if = { limit = { check_variable = { Western_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ayelet Shaked"
				picture = "Ayelet_Shaked.dds"
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
					economic_managers
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 } 
		}
	}		
	else_if = { limit = { has_country_flag = set_conservatism NOT = { has_country_flag = ISR_bibi_power } }
		if = { limit = { check_variable = { conservatism_leader = 0 } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ariel Sharon"
				picture = "ariel_sharon.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					pro_russia
					military_career
					corruptible
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } NOT = { has_country_flag = ISR_bibi_power } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Benjamin Netanyahu"
				picture = "Benjamin_Netanyahu.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					corruptible
					economic_managers
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ehud Olmert"
				picture = "Ehud_Olmert.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					career_politician
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Tzipi Livni"
				picture = "tzipi_livni.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					spy
					diplomat
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 0 } OR = { NOT = { has_country_flag = ISR_yeshatid has_country_flag = ISR_resilance has_country_flag = ISR_newhope has_country_flag = ISR_nationalcamp has_country_flag = ISR_bluewhite } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yitzhak Mordechai"
				picture = "yitzak_mordechai.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2001.1.1 } set_temp_variable = { b = 1 } }
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } OR = { NOT = { has_country_flag = ISR_yeshatid has_country_flag = ISR_resilance has_country_flag = ISR_newhope has_country_flag = ISR_nationalcamp has_country_flag = ISR_bluewhite } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Tommy Lapid"
				picture = "tommy_lapid.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 0 } NOT = { check_variable = { b = 1 } } has_country_flag = ISR_yeshatid }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yair Lapid"
				picture = "yair_lapid.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					journalist
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 0 } NOT = { check_variable = { b = 1 } } OR = { has_country_flag = ISR_resilance has_country_flag = ISR_bluewhite has_country_flag = ISR_nationalcamp } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Benny Gantz"
				picture = "Benny_Gantz.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					military_genius
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 0 } NOT = { check_variable = { b = 1 } } OR = { has_country_flag = ISR_newhope has_country_flag = ISR_bluewhite has_country_flag = ISR_nationalcamp } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Gideon Sa'ar"
				picture = "Gideon_Saar.dds"
				ideology = liberalism
				traits = {
					judiciary
					career_politician
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 0 } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ehud Barak"
				desc = "Ehud Barak"
				picture = "barak_ehud.dds"
				expire = "2050.3.7"
				ideology = socialism
				traits = {
					western_socialism
					military_career
					military_genius
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Avi Gabbay"
				desc = "Avi Gabbay"
				picture = "avi_gabbay.dds"
				expire = "2050.1.1"
				ideology = socialism
				traits = {
					western_socialism
					economic_managers
					ecological_economist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Isaac Herzog"
				desc = "Isaac Herzog"
				picture = "Isaac_Herzog.dds"
				expire = "2050.1.1"
				ideology = socialism
				traits = {
					western_socialism
					lawyer
					spy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohammad Barakeh"
				picture = "mohammad_barakeh.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Communist-State_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ayman Odeh"
				picture = "ayman_odeh.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Conservative }
		if = { limit = { check_variable = { Conservative_leader = 0 } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Aryeh Deri"
				picture = "aryeh_deri.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Autocracy }
		if = { limit = { check_variable = { Autocracy_leader = 0 } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ahmad Tibi"
				picture = "ahmad_tibi.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_anarchist_communism }
		if = { limit = { check_variable = { anarchist_communism_leader = 0 } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Azmi Bishara"
				picture = "azmi_bishara.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { anarchist_communism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jamal Zahalka"
				picture = "generic.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } NOT = { has_country_flag = ISR_bibi_power } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yuli Edelstein"
				picture = "yuli_edelstein.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					career_politician
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } has_country_flag = ISR_bibi_power }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Benjamin Netanyahu"
				desc = "Benjamin Netanyahu"
				picture = "Benjamin_Netanyahu.dds"
				expire = "2050.1.1"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					economic_managers
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_oligarchism }
		if = { limit = { check_variable = { oligarchism_leader = 0 } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Avigdor Liberman"
				picture = "Avigdor_Liberman.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism
					career_politician
					pro_russia
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_neutral_Social }
		if = { limit = { check_variable = { neutral_Social_leader = 0 } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yossi Sarid"
				picture = "yossi_sarid.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { neutral_Social_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Zehava Gal-On"
				picture = "zehava_gal-on.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Moshe Feiglin"
				picture = "moshe_feiglin.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Autocracy }
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 0 } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yaakov Litzman"
				picture = "yaakov_litzman.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Communism }
		if = { limit = { check_variable = { Neutral_Communism_leader = 0 } }
			add_to_variable = { Neutral_Communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Nir Nadir"
				picture = "generic.dds"
				ideology = Neutral_Communism
				traits = {
					neutrality_Neutral_Communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Communism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Neutral_Communism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Asma Agbarieh"
				picture = "generic.dds"
				ideology = Neutral_Communism
				traits = {
					neutrality_Neutral_Communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdulmalik Dehamshe"
				picture = "abdulmalik_dehamshe.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Masud Ghnaim"
				picture = "generic.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_green }
		if = { limit = { check_variable = { Neutral_green_leader = 0 } }
			add_to_variable = { Neutral_green_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yael Cohen Paran"
				picture = "yael_cohen_paran.dds"
				ideology = Neutral_green
				traits = {
					neutrality_Neutral_green
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_green_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Lt. Gen. Shaul Mofaz"
				picture = "shaul_mofaz.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Shaul Mofaz"
				picture = "shaul_mofaz.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Lt. Gen. Gadi Eizenkot"
				picture = "gen_Gadi_Eizenkot.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Fascism }
		if = { limit = { check_variable = { Nat_Fascism_leader = 0 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ben Givir"
				picture = "ben_gvir.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
					lawyer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Nat_Fascism_leader = 1 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Michael Ben-Ari"
				picture = "michael_ben_ari.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Populism }
		if = { limit = { check_variable = { Nat_Populism_leader = 0 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Zvi Hendel"
				picture = "zvi_hendel.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Nat_Populism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Uri Ariel"
				picture = "uri_ariel.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
}