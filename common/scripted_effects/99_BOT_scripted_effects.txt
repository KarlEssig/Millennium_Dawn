BOT_increase_desertification = {
	if = {
		limit = { has_idea = BOT_kalahari_expansion_1 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_2
			remove_idea = BOT_kalahari_expansion_1
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_2 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_3
			remove_idea = BOT_kalahari_expansion_2
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_3 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_4
			remove_idea = BOT_kalahari_expansion_3
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_4 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_5
			remove_idea = BOT_kalahari_expansion_4
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_5 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_6
			remove_idea = BOT_kalahari_expansion_5
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_6 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_7
			remove_idea = BOT_kalahari_expansion_6
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_7 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_8
			remove_idea = BOT_kalahari_expansion_7
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_8 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_9
			remove_idea = BOT_kalahari_expansion_8
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_9 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_10
			remove_idea = BOT_kalahari_expansion_9
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_10 }
		add_political_power = -200
	}
}

BOT_decrease_desertification = {
	if = {
		limit = { has_idea = BOT_kalahari_expansion_1 }
		add_political_power = 200
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_2 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_1
			remove_idea = BOT_kalahari_expansion_2
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_3 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_2
			remove_idea = BOT_kalahari_expansion_3
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_4 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_3
			remove_idea = BOT_kalahari_expansion_4
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_5 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_4
			remove_idea = BOT_kalahari_expansion_5
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_6 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_5
			remove_idea = BOT_kalahari_expansion_6
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_7 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_6
			remove_idea = BOT_kalahari_expansion_7
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_8 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_7
			remove_idea = BOT_kalahari_expansion_8
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_9 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_8
			remove_idea = BOT_kalahari_expansion_9
		}
	}
	else_if = {
		limit = { has_idea = BOT_kalahari_expansion_10 }
		swap_ideas = {
			add_idea = BOT_kalahari_expansion_9
			remove_idea = BOT_kalahari_expansion_10
		}
	}
}

BOT_increase_desertification_rate = {
	if = {
		limit = {
			check_variable = { kalahari_retreats > 4 }
		}
		custom_effect_tooltip = BOT_increase_yearly_bad_desertification_5
		add_to_variable = { kalahari_expands = 5 }
		custom_effect_tooltip = BOT_reduce_yearly_good_desertification_5
		subtract_from_variable = { kalahari_retreats = 5 }
	}
	else_if = {
		limit = {
			check_variable = { kalahari_steady > 4 }
		}
		custom_effect_tooltip = BOT_increase_yearly_bad_desertification_5
		add_to_variable = { kalahari_expands = 5 }
		custom_effect_tooltip = BOT_reduce_yearly_neutral_desertification_5
		subtract_from_variable = { kalahari_steady = 5 }
	}
	else = {
		add_political_power = -200
	}
}

BOT_decrease_desertification_rate = {
	if = {
		limit = {
			check_variable = { kalahari_steady < 30 }
		}
		custom_effect_tooltip = BOT_decrease_yearly_bad_desertification_5
		subtract_from_variable = { kalahari_expands = 5 }
		custom_effect_tooltip = BOT_increase_yearly_neutral_desertification_5
		add_to_variable = { kalahari_steady = 5 }
	}
	else_if = {
		limit = {
			check_variable = { kalahari_retreats < 70 }
		}
		custom_effect_tooltip = BOT_decrease_yearly_bad_desertification_5
		subtract_from_variable = { kalahari_expands = 5 }
		custom_effect_tooltip = BOT_increase_yearly_good_desertification_5
		add_to_variable = { kalahari_retreats = 5 }
	}
	else = {
		add_political_power = 200
	}
}

BOT_boost_BNF = {
	if = {
		limit = {
			has_country_flag = UDC_formed
			has_country_flag = BNF_joined_UDC
		}
		custom_effect_tooltip = BOT_BOOST_UDC
		add_to_variable = { party_pop_array^18 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	else = {
		custom_effect_tooltip = BOT_BOOST_BNF
		add_to_variable = { party_pop_array^3 = 0.02 }
		add_popularity = { ideology = democratic popularity = 0.02 }
	}
	recalculate_party = yes
}

BOT_boost_MELS = {
	if = {
		limit = {
			has_country_flag = UDC_formed
			has_country_flag = MELS_joined_UDC
		}
		custom_effect_tooltip = BOT_BOOST_UDC
		add_to_variable = { party_pop_array^18 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	else = {
		custom_effect_tooltip = BOT_BOOST_MELS
		add_to_variable = { party_pop_array^4 = 0.02 }
		add_popularity = { ideology = communism popularity = 0.02 }
	}
	recalculate_party = yes
}

BOT_boost_BPP = {
	if = {
		limit = {
			has_country_flag = UDC_formed
			has_country_flag = BPP_joined_UDC
		}
		custom_effect_tooltip = BOT_BOOST_UDC
		add_to_variable = { party_pop_array^18 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	else = {
		custom_effect_tooltip = BOT_BOOST_BPP
		add_to_variable = { party_pop_array^5 = 0.02 }
		add_popularity = { ideology = communism popularity = 0.02 }
	}
	recalculate_party = yes
}

BOT_boost_BDP_A = {
	custom_effect_tooltip = BOT_BOOST_BDP_A
	add_to_variable = { party_pop_array^14 = 0.02 }
	add_popularity = { ideology = neutrality popularity = 0.02 }
	recalculate_party = yes
}

BOT_boost_BDP_B = {
	if = {
		limit = {
			has_country_flag = UDC_formed
			has_country_flag = BMD_joined_UDC
			has_country_flag = BDP_split
		}
		custom_effect_tooltip = BOT_BOOST_UDC
		add_to_variable = { party_pop_array^18 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	else_if = {
		limit = {
			has_country_flag = BDP_split
		}
		custom_effect_tooltip = BOT_BOOST_BDP_B
		add_to_variable = { party_pop_array^16 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	else = {
		custom_effect_tooltip = BOT_BOOST_BDP_B
		add_to_variable = { party_pop_array^16 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	recalculate_party = yes
}

BOT_boost_BCP = {
	if = {
		limit = {
			has_country_flag = UDC_formed
			has_country_flag = BCP_joined_UDC
		}
		custom_effect_tooltip = BOT_BOOST_UDC
		add_to_variable = { party_pop_array^18 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	else = {
		custom_effect_tooltip = BOT_BOOST_BCP
		add_to_variable = { party_pop_array^18 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	recalculate_party = yes
}

BOT_boost_ISBO = {
	if = {
		limit = {
			has_country_flag = UDC_formed
			has_country_flag = ISBO_joined_UDC
		}
		custom_effect_tooltip = BOT_BOOST_UDC
		add_to_variable = { party_pop_array^18 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	else = {
		custom_effect_tooltip = BOT_BOOST_ISBO
		add_to_variable = { party_pop_array^19 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	recalculate_party = yes
}

BOT_boost_BWF = {
	if = {
		limit = {
			has_country_flag = UDC_formed
			has_country_flag = BWF_joined_UDC
		}
		custom_effect_tooltip = BOT_BOOST_UDC
		add_to_variable = { party_pop_array^18 = 0.02 }
		add_popularity = { ideology = neutrality popularity = 0.02 }
	}
	else = {
		custom_effect_tooltip = BOT_BOOST_BWF
		add_to_variable = { party_pop_array^20 = 0.02 }
		add_popularity = { ideology = nationalist popularity = 0.02 }
	}
	recalculate_party = yes
}

BOT_boost_digkosi = {
	custom_effect_tooltip = BOT_BOOST_DIGKOSI
	add_to_variable = { party_pop_array^23 = 0.02 }
	add_popularity = { ideology = nationalist popularity = 0.02 }
	recalculate_party = yes
}

BOT_update_anti_poaching = {
	#From internal security level
	if = {
		limit = { has_idea = police_01 }
		set_variable = { chance_to_kill_poachers = 15 }
	}
	else_if = {
		limit = { has_idea = police_02 }
		set_variable = { chance_to_kill_poachers = 30 }
	}
	else_if = {
		limit = { has_idea = police_03 }
		set_variable = { chance_to_kill_poachers = 45 }
	}
	else_if = {
		limit = { has_idea = police_04 }
		set_variable = { chance_to_kill_poachers = 60 }
	}
	else_if = {
		limit = { has_idea = police_05 }
		set_variable = { chance_to_kill_poachers = 75 }
	}
	#From hiring extra security
	if = {
		limit = { has_decision = BOT_anti_poaching_extra_security }
		add_to_variable = { chance_to_kill_poachers = 10 }
	}

	clamp_variable = { var = chance_to_kill_poachers max = 100 }
	set_variable = { chance_to_not_kill_poachers = 100 }
	subtract_from_variable = { chance_to_not_kill_poachers = chance_to_kill_poachers }

	set_variable = { no_of_poachers_to_kill = 10 }
	if = {
		limit = { has_decision = BOT_anti_poaching_heavy_weapons }
		add_to_variable = { no_of_poachers_to_kill = 5 }
	}
	if = {
		limit = { has_idea = BOT_army_against_poaching }
		add_to_variable = { no_of_poachers_to_kill = 5 }
	}
}