set_leader_UKR = {
	if = { limit = { has_country_flag = set_Conservative }
		if = { limit = { check_variable = { Conservative_leader = 0 } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Viktor Yanukovych"
				picture = "gfx/leaders/UKR/Viktor_Yanukovych_new.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
					inexperienced
					stubborn
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Conservative_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Conservative_leader = 2 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Viktor Medvedchuk"
				picture = "gfx/leaders/UKR/Viktor_Medvedchuk_new.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Conservative_leader = 2 } NOT = { check_variable = { b = 2 } } }
			add_to_variable = { Conservative_leader = 3 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yuriy Boyko"
				picture = "gfx/leaders/UKR/Emerging_Yuriy_Boyko.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_conservatism }
		if = { limit = { check_variable = { conservatism_leader = 0 } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yulia Tymoshenko"
				picture = "Yulia_Tymoshenko.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					career_politician
					sly
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Eugenia Tymoshenko"
				picture = "Eugenia_Tymoshenko.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					career_politician
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Oleksandr Turchynov"
				picture = "ukr_civil_turchinov.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					career_politician
					sly
				}
			}
			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 0 } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Viktor Medvedchuk"
				picture = "gfx/leaders/UKR/Viktor_Medvedchuk_new.dds"
				ideology = socialism
				traits = {
					western_socialism
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 1 }  NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Volodymyr Groysman"
				picture = "gfx/leaders/UKR/ukr_groysman.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}

	else_if = { limit = { has_country_flag = set_Nat_Populism }
		if = { limit = { check_variable = { Nat_Populism_leader = 0 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Oleh Tyahnybok"
				picture = "gfx/leaders/UKR/Nat_Oleh_Tyahnybok.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Nat_Populism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ihor Miroshnychenko"
				picture = "gfx/leaders/UKR/ukr_miroshnychenko.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Fascism }
	if = { limit = { check_variable = { Nat_Fascism_leader = 0 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Stepan Bratsiun"
				picture = "gfx/leaders/UKR/ukr_bratsiun.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Nat_Fascism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Andriy Tarasenko"
				picture = "gfx/leaders/UKR/andriy_tarasenko.dds"
				ideology = Nat_Fascism
				traits = {
					military_career
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Andriy Kornat"
				picture = "andriy_kornat.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					career_politician
					sly
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Borys Tarasyuk"
				picture = "ukr_tarasyuk.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 0 } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Petro Poroshenko"
				picture = "Petro_Poroshenko.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					rational
					humble
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Iryna Herashchenko"
				picture = "ukr_herashenko.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_libertarian_leader = 0 } }
			add_to_variable = { Neutral_libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Viktor Yushchenko"
				picture = "gfx/leaders/UKR/Viktor_Yushchenko.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
					humble
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_libertarian_leader = 1 } has_country_flag = UKR_zelensky_formed NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Volodymyr Zelenskyy"
				picture = "gfx/leaders/UKR/Volodymyr_Zelensky.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
					humble
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_libertarian_leader = 1 } }
			set_temp_variable = { b = 1 } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Petro Symonenko"
				picture = "petro_symonenko.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
					rational
					ruthless
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Communist-State_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Igor Alekseyev"
				picture = "ukr_alekseev.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_neutral_Social }
		if = { limit = { check_variable = { neutral_Social_leader = 0 } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Oleksandr Moroz"
				picture = "oleksandr_moroz.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { neutral_Social_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Serhiy Kaplin"
				picture = "ukr_kaplin.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { neutral_Social_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Illia Kyva"
				picture = "ukr_kiva.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Volodymyr Shkidchenko"
				picture = "ukr_shidchenko.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Viktor Muzhenko"
				picture = "ukr_muzhenko.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ruslan Khomchak"
				picture = "ruslan_khomchak.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }
			create_country_leader = {
				name = "Valerii Zaluzhnyi"
				picture = "valerii_zaluzhnyi.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Autocracy }
		if = { limit = { check_variable = { Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Natalia Vitrenko"
				picture = "Natalia_Vitrenko.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Western_Autocracy }
		if = { limit = { check_variable = { Western_Autocracy_leader = 0 } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yuri Karmazin"
				picture = "yuri_karmazin.dds"
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Western_Autocracy_leader = 1 } has_country_flag = ukr_revoult_bilo NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Oleksandr Turchynov"
				picture = "ukr_turchinov.dds"
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
					ruthless
					}
				}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Western_Autocracy_leader = 2 } has_country_flag = ukr_revoult_bilo NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Arseniy Yatsenyuk"
				picture = "ukr_yatsenuk.dds"
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
					career_politician
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
		}
		}
		else_if = { limit = { has_country_flag = set_anarchist_communism }
			if = { limit = { check_variable = { anarchist_communism_leader = 0 } }
				add_to_variable = { anarchist_communism_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Vasyl Volha"
					picture = "ukr_volha.dds"
					ideology = anarchist_communism
					traits = {
						emerging_anarchist_communism
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
				set_temp_variable = { b = 1 }
			}
			if = { limit = { check_variable = { anarchist_communism_leader = 1 } NOT = { check_variable = { b = 1 } } }
				add_to_variable = { anarchist_communism_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Maksym Holdarb"
					picture = "ukr_holdarb.dds"
					ideology = anarchist_communism
					traits = {
						emerging_anarchist_communism
						}
					}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
				set_temp_variable = { b = 1 }
			}
		}
		else_if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
			if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }
				add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Vasvi Abduraimov"
					picture = "ukr_abduraivov.dds"
					ideology = anarchist_communism
					traits = {
						emerging_anarchist_communism
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
				set_temp_variable = { b = 1 }
			}
		}
		else_if = { limit = { has_country_flag = set_oligarchism }
			if = { limit = { check_variable = { oligarchism_leader = 0 } }
				add_to_variable = { oligarchism_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Vitali Klitschko"
					picture = "ukr_klichko.dds"
					ideology = oligarchism
					traits = {
						neutrality_oligarchism
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
				set_temp_variable = { b = 1 }
			}
			if = { limit = { check_variable = { oligarchism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Lev Partshaladze"
				picture = "ukr_partshaladze.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			}
		}
		else_if = { limit = { has_country_flag = set_Neutral_Autocracy }
			if = { limit = { check_variable = { neutral_autocracy_leader = 0 } }
				add_to_variable = { neutral_autocracy_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Darth Alekseevich Vader"
					picture = "ukr_vaider.dds"
					ideology = Neutral_Autocracy
					traits = {
						neutrality_Neutral_Autocracy
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_autocracy_leader = 1 } }
				set_temp_variable = { b = 1 }
			}
		}
		else_if = { limit = { has_country_flag = set_Neutral_Communism }
			if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } }
				add_to_variable = { Neutral_conservatism_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Tamila Yabrova"
					picture = "ukr_tamila.dds"
					ideology = Neutral_Communism
					traits = {
						neutrality_Neutral_Communism
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
				set_temp_variable = { b = 1 }
			}
		}
		else_if = { limit = { has_country_flag = set_Neutral_green }
			if = { limit = { check_variable = { Neutral_Green_leader = 0 } NOT = { has_country_flag = ukr_revoult_bilo } }
					add_to_variable = { Neutral_Green_leader = 1 }
					hidden_effect = { kill_country_leader = yes }

					create_country_leader = {
						name = "Oleksandr Omelchenko"
						picture = "ukr_omelchenko.dds"
						ideology = Neutral_green
						traits = {
							neutrality_Neutral_green
						}
					}

					if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Green_leader = 1 } }
					set_temp_variable = { b = 1 }
				}
			if = { limit = { check_variable = { Neutral_Green_leader = 1 } NOT = { has_country_flag = ukr_revoult_bilo } NOT = { check_variable = { b = 1 } } }
				add_to_variable = { Neutral_Green_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Vitaliy Kononov"
					picture = "vitaliy_kononov.dds"
					ideology = Neutral_green
					traits = {
						neutrality_Neutral_green
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Green_leader = 1 } }
			}
			if = { limit = { check_variable = { Neutral_Green_leader = 2 }  has_country_flag = ukr_revoult_bilo NOT = { check_variable = { b = 1 } } }
				add_to_variable = { Neutral_Green_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Hennadii Korban"
					picture = "ukr_korban.dds"
					ideology = Neutral_green
					traits = {
						neutrality_Neutral_green
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Green_leader = 1 } }
			}
		}

		else_if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
			if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }
				add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Vasvi Abduraimov"
					picture = "ukr_abduraivov.dds"
					ideology = Neutral_Muslim_Brotherhood
					traits = {
						neutrality_Neutral_Muslim_Brotherhood
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
				set_temp_variable = { b = 1 }
			}
		}

		else_if = { limit = { has_country_flag = set_Monarchist }
			if = { limit = { check_variable = { Monarchist_leader = 0 } }
				add_to_variable = { Monarchist_leader = 1 }
				hidden_effect = { kill_country_leader = yes }

				create_country_leader = {
					name = "Yelena I"
					picture = "UKR_skoropadskaya_Ott.dds"
					ideology = Monarchist
					traits = {
						nationalist_Monarchist
					}
				}

				if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
				set_temp_variable = { b = 1 }
			}
		}
	}
