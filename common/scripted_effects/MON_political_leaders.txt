set_leader_MON = {

	if = { limit = { has_country_flag = set_neutral_Social }

		if = { limit = { check_variable = { neutral_Social_leader = 0 } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Tsakhiagiin Elbegdorj"
				picture = "Tsakhiagiin_Elbegdorj.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					pro_american
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { neutral_Social_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Khaltmaagiin Battulga"
				picture = "Khaltmaagiin_Battulga.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					pro_american
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jügderdemidiin Gürragchaa"
				picture = "generic.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Lieutenant General Tserendejidiin Byambajav"
				picture = "tserendejidiin_byambajav.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Monarchist }
		if = { limit = { check_variable = { Monarchist_leader = 0 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jebtsundamba Khutuktu IX"
				picture = "jebtsundamba_khutuktu_ix.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Monarchist_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jebtsundamba Khutuktu X"
				picture = "generic.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_anarchist_communism }
		if = { limit = { check_variable = { anarchist_communism_leader = 0 } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Nambaryn Enkhbayar"
				picture = "Nambaryn_Enkhbayar.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { anarchist_communism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ukhnaagiin Khurelsukh"
				picture = "Ukhnaagiin_Khurelsukh.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_green }
		if = { limit = { check_variable = { Neutral_green_leader = 0 } }
			add_to_variable = { Neutral_green_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Sanjaasürengiin Oyuun"
				picture = "sanjaasurengiin_oyuun.dds"
				ideology = Neutral_green
				traits = {
					neutrality_Neutral_green
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_green_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
}