
GER_update_commanders = {
	### 2003
	if = {
		limit = { AND = { date > 2002.1.2 date < 2003.1.2 } }
		create_corps_commander = {
			name = "Hans-Werner Fritz"
			desc = "GER_HANS_WERNER_FRITZ_DESC"
			picture = "Portrait_Hans_Werner_Fritz.dds"
			id = 80014
			traits = { panzer_leader }
			skill = 3
			attack_skill = 4
			defense_skill = 2
			planning_skill = 3
			logistics_skill = 3
		}
	}
	### 2007
	if = {
		limit = { AND = { date > 2006.1.2 date < 2007.1.2 } }
		create_corps_commander = {
			name = "Jörg Vollmer"
			desc = "GER_JORG_VOLLMER_DESC"
			picture = "Portrait_Jörg_Vollmer.dds"
			id = 80007
			traits = { infantry_leader }
			skill = 3
			attack_skill = 3
			defense_skill = 2
			planning_skill = 3
			logistics_skill = 3
		}
	}
	### 2011
	if = {
		limit = { AND = { date > 2010.1.2 date < 2011.1.2 } }
		create_corps_commander = {
			name = "Alfons Mais"
			desc = "GER_ALFONS_MAIS_DESC"
			picture = "Portrait_Alfons_Mais.dds"
			id = 80008
			traits = { commando paratrooper }
			skill = 3
			attack_skill = 4
			defense_skill = 2
			planning_skill = 3
			logistics_skill = 3
		}
	}
}
#Operatives#
GER_create_operative_Philipp_Schaaf = { #KSK
	create_operative_leader = {
		name = GER_Philipp_Schaaf
		GFX = GFX_portrait_Philipp_Schaaf
		traits = { operative_demolition_expert operative_commando operative_tough }
		bypass_recruitment = no
		available_to_spy_master = yes
		nationalities = { GER }
		gender = male
	}
}
GER_create_operative_Curveball = { #KSK
	create_operative_leader = {
		name = GER_Rafid_Ahmed_Alwan_al_Janabi
		GFX = GFX_portrait_Curveball
		traits = { operative_double_agent operative_web_of_lies }
		bypass_recruitment = no
		available_to_spy_master = yes
		nationalities = { GER IRQ }
		gender = male
	}
}
GER_create_operative_al_masri = { #KSK
	create_operative_leader = {
		name = GER_Khalid_al_Masri
		GFX = GFX_portrait_al_Masri
		traits = { operative_infiltrator operative_web_of_lies }
		bypass_recruitment = no
		available_to_spy_master = yes
		nationalities = { GER LEB }
		gender = male
	}
}
GER_create_operative_Gerhard_Conrad = { #KSK
	create_operative_leader = {
		name = GER_Gerhard_Conrad
		GFX = GFX_portrait_Gerhard_Conrad
		traits = { operative_well_groomed operative_natural_orator }
		bypass_recruitment = no
		available_to_spy_master = yes
		nationalities = { GER ISR }
		gender = male
	}
}
GER_create_operative_Erich_schmidt_eenboom = { #KSK
	create_operative_leader = {
		name = GER_Erich_schmidt_eenbloom
		GFX = GFX_portrait_Erich_schmidt_eenbloom
		traits = { operative_master_interrogator }
		bypass_recruitment = no
		available_to_spy_master = yes
		nationalities = { GER }
		gender = male
	}
}

# HEALTHCARE COST
	update_GER_healthcare_cost = {
	custom_effect_tooltip = GER_healthcare_cost_tt
	add_to_variable = { GER_health_cost = GER_health_cost_change }
}
# WELFARE COST
	update_GER_welfare_cost = {
	custom_effect_tooltip = GER_welfare_cost_tt
	add_to_variable = { GER_welfare_cost = GER_welfare_cost_change }
}

# Centralization COST
	update_GER_centralization_cost = {
	custom_effect_tooltip = GER_centralization_cost_tt
	add_to_variable = { GER_centralization_cost = GER_centralization_cost_change }
}

# EDUCATION COST
	update_GER_education_cost = {
	custom_effect_tooltip = GER_education_cost_tt
	add_to_variable = { GER_education_cost = GER_education_cost_change }
}
# INTERNAL SECURITY COST
	update_GER_police_cost = {
	custom_effect_tooltip = GER_police_cost_tt
	add_to_variable = { GER_police_cost = GER_police_cost_change }
}
# INTEREST RATE COST
	update_GER_interest_cost = {
	custom_effect_tooltip = GER_interest_cost_tt
	add_to_variable = { GER_interest_cost = GER_interest_cost_change }
}
# MILITARY COST
	update_GER_military_cost = {
	custom_effect_tooltip = GER_military_cost_tt
	add_to_variable = { GER_military_cost = GER_military_cost_change }
}

# ENERGY EFFECTS
#modify_energy_gain_effect = {
#	custom_effect_tooltip = modify_energy_gain_TT
#	add_to_variable = { GER_energy_gain_var = GER_energy_gain_change }
#}
GER_die_linke_events = {
	random_list = {
		70 = {
			add_to_variable = { GER_event_counter_1_dielinke = 1 }
		}
		30 = {
		}
	}
	if = {
		limit = { check_variable = { GER_event_counter_1_dielinke > 5 } }
		set_variable = { GER_event_counter_1_dielinke = 0 }
		random_list = {
			10 = {
				country_event = germany_divide.1
			}
			10 = {
				country_event = germany_divide.2
			}
			10 = {
				country_event = germany_divide.3
			}
			10 = {
				country_event = germany_divide.4
			}
			10 = {
				country_event = germany_divide.5
			}
			10 = {
				country_event = germany_divide.6
			}
			10 = {
				country_event = germany_divide.7
			}
			10 = {
				country_event = germany_divide.8
			}
			10 = {
				country_event = germany_divide.9
			}
			10 = {
				country_event = germany_divide.10
			}
		}
	}
}
#
GER_east_german_events = {
	random_list = {
		70 = {
			add_to_variable = { GER_event_counter_1_egerman = 1 }
		}
		30 = {
		}
	}
	if = {
		limit = { check_variable = { GER_event_counter_1_egerman > 7 } }
		set_variable = { GER_event_counter_1_egerman = 0 }
		random_list = {
			10 = {
				country_event = germany_divide.11
			}
			10 = {
				country_event = germany_divide.12
			}
			10 = {
				country_event = germany_divide.13
			}
			10 = {
				country_event = germany_divide.14
			}
			10 = {
				country_event = germany_divide.15
			}
			10 = {
				country_event = germany_divide.16
			}
			10 = {
				country_event = germany_divide.17
			}
			10 = {
				country_event = germany_divide.18
			}
			10 = {
				country_event = germany_divide.19
			}
			10 = {
				country_event = germany_divide.20
			}
		}
	}
}
# BAVARIA MECHANIC #
GER_update_bavaria_standing = {
	if = {
		limit = {
			OR = {
				has_idea = stagnation
				has_idea = recession
				has_idea = depression
			}
		}
		random_list = {
			50 = {
			}
			40 = {
				add_to_variable = { GER_bavarian_nationalism_var = 1 }
			}
		}
	}
	if = {
		limit = {
			OR = {
				has_idea = fast_growth
				has_idea = economic_boom
			}
			check_variable = { GER_bavarian_nationalism_var > 0 }
		}
		random_list = {
			50 = {
			}
			40 = {
				add_to_variable = { GER_bavarian_nationalism_var = -1 }
			}
		}
	}
}
# WoT system
GER_wot_random_events = {
	random_list = {
		50 = {
		add_to_variable = { GER_event_counter_1_wot = 1 }
		}
		50 = {
		}
	}
	if = {
		limit = { check_variable = { GER_event_counter_1_wot > 6 } }
		random_list = {
			70 = {
				add_to_variable = { GER_event_counter_1_wot = 1 }
			}
			30 = {
			}
		}
		set_variable = { GER_event_counter_1_wot = 0 }
		random_list = {
			50 = {
				country_event = Germany.406
			}
			50 = {
				country_event = Germany.405
			}
		}
	}
}
#
GER_bfv_events = {
	random_list = {
		80 = {
			add_to_variable = { GER_event_counter_1_bfv = 1 }
		}
		20 = {
		}
	}
	if = {
		limit = { check_variable = { GER_event_counter_1_bfv > 2 } }
		set_variable = { GER_event_counter_1_bfv = 0 }
		random_list = {
			25 = {
				country_event = Germany.415
			}
			25 = {
				country_event = Germany.416
			}
			25 = {
				country_event = Germany.417
			}
			25 = {
				country_event = Germany.418
			}
		}
	}
}
#
GER_government_change = {
	if = {
		limit = {
			original_tag = GER
			has_cosmetic_tag = GER_franco_german_union
			emerging_anarchist_communism_are_not_in_power = yes
		}
		drop_cosmetic_tag = yes
	}
	if = {
		limit = {
			original_tag = GER
			has_cosmetic_tag = GER_fourth_reich
			nationalist_fascist_are_not_in_power = yes
		}
		drop_cosmetic_tag = yes
	}
	if = {
		limit = {
			original_tag = GER
			OR = {
				has_cosmetic_tag = GER_german_empire
				has_cosmetic_tag = GER_holy_german_empire
			}
			nationalist_monarchists_are_not_in_power = yes
		}
		drop_cosmetic_tag = yes
	}
}

# new ones
update_GER_military_cost2 = {
	custom_effect_tooltip = GER_military_cost2_tt
	add_to_variable = { GER_military_cost2 = GER_military_cost2_change }
}
update_GER_Army_attack = {
	custom_effect_tooltip = GER_Army_attack_tt
	add_to_variable = { GER_Army_attack = GER_Army_attack_change }
}
update_GER_recon_factor = {
	custom_effect_tooltip = GER_recon_factor_tt
	add_to_variable = { GER_recon_factor = GER_recon_factor_change }
}
update_GER_Army_defence = {
	custom_effect_tooltip = GER_Army_defence_tt
	add_to_variable = { GER_Army_defence = GER_Army_defence_change }
}
update_GER_breakthrough_factor = {
	custom_effect_tooltip = GER_breakthrough_factor_tt
	add_to_variable = { GER_breakthrough_factor = GER_breakthrough_factor_change }
}
update_Ger_army_artillery_attack_factor = {
	custom_effect_tooltip = Ger_army_artillery_attack_factor_tt
	add_to_variable = { Ger_army_artillery_attack_factor = Ger_army_artillery_attack_factor_change }
}
update_GER_leader_xp_gain = {
	custom_effect_tooltip = GER_leader_xp_gain_tt
	add_to_variable = { GER_leader_xp_gain = GER_leader_xp_gain_change }
}
update_GER_experience_gain_army = {
	custom_effect_tooltip = GER_experience_gain_army_tt
	add_to_variable = { GER_experience_gain_army = GER_experience_gain_army_change }
}
update_GER_army_org_regain = {
	custom_effect_tooltip = GER_army_org_regain_tt
	add_to_variable = { GER_army_org_regain = GER_army_org_regain_change }
}
update_GER_terrain_penalty_reduction = {
	custom_effect_tooltip = GER_terrain_penalty_reduction_tt
	add_to_variable = { GER_terrain_penalty_reduction = GER_terrain_penalty_reduction_change }
}
update_GER_air_attack = {
	custom_effect_tooltip = GER_air_attack_tt
	add_to_variable = { GER_air_attack = GER_air_attack_change }
}
update_GER_air_defence = {
	custom_effect_tooltip = GER_air_defence_tt
	add_to_variable = { GER_air_defence = GER_air_defence_change }
}
update_GER_air_mission_efficiency = {
	custom_effect_tooltip = GER_air_mission_efficiency_tt
	add_to_variable = { GER_air_mission_efficiency = GER_air_mission_efficiency_change }
}
update_GER_positioning = {
	custom_effect_tooltip = GER_positioning_tt
	add_to_variable = { GER_positioning = GER_positioning_change }
}
update_GER_navy_max_range_factor = {
	custom_effect_tooltip = GER_navy_max_range_factor_tt
	add_to_variable = { GER_navy_max_range_factor = GER_navy_max_range_factor_change }
}
update_GER_navy_fuel_consumption_factor = {
	custom_effect_tooltip = GER_navy_fuel_consumption_factor_tt
	add_to_variable = { GER_navy_fuel_consumption_factor = GER_navy_fuel_consumption_factor_change }
}
update_GER_dockyard_productivity = {
	custom_effect_tooltip = GER_dockyard_productivity_tt
	add_to_variable = { GER_dockyard_productivity = GER_dockyard_productivity_change }
}

#deutsche bahn
update_GER_offices_contruction_speed = {
	custom_effect_tooltip = GER_offices_contruction_speed_tt
	add_to_variable = { GER_offices_contruction_speed = GER_offices_contruction_speed_change }
}
update_GER_arms_factory_contruction_speed = {
	custom_effect_tooltip = GER_arms_factory_contruction_speed_tt
	add_to_variable = { GER_arms_factory_contruction_speed = GER_arms_factory_contruction_speed_change }
}
update_GER_civ_construction_speed = {
	custom_effect_tooltip = GER_civ_construction_speed_tt
	add_to_variable = { GER_civ_construction_speed = GER_civ_construction_speed_change }
}
update_GER_population_energy_consumption = {
	custom_effect_tooltip = GER_population_energy_consumption_tt
	add_to_variable = { GER_population_energy_consumption = GER_population_energy_consumption_change }
}
update_GER_mil_factory_workforce = {
	custom_effect_tooltip = GER_mil_factory_workforce_tt
	add_to_variable = { GER_mil_factory_workforce = GER_mil_factory_workforce_change }
}
update_GER_office_workforce = {
	custom_effect_tooltip = GER_office_workforce_tt
	add_to_variable = { GER_office_workforce = GER_office_workforce_change }
}
update_GER_supply_consumption_factor_I = {
	custom_effect_tooltip = GER_supply_consumption_factor_I_tt
	add_to_variable = { GER_supply_consumption_factor_I = GER_supply_consumption_factor_I_change }
}
update_GER_attrition_modifier = {
	custom_effect_tooltip = GER_attrition_modifier_TT
	add_to_variable = { GER_attrition_modifier = GER_attrition_modifier_change }
}
update_GER_max_planning_modifier = {
	add_to_variable = { GER_max_planning_modifier = GER_max_planning_modifier_change }
	custom_effect_tooltip = GER_max_planning_modifier_TT
}
update_GER_army_night_attack = {
	add_to_variable = { GER_army_night_attack = GER_army_night_attack_change }
	custom_effect_tooltip = GER_army_night_attack_tt
}
update_GER_army_core_defence_factor = {
	custom_effect_tooltip = GER_army_core_defence_factor_tt
	add_to_variable = { GER_army_core_defence_factor = GER_army_core_defence_factor_change }
}
update_GER_special_forces_out_of_supply_factor = {
	custom_effect_tooltip = GER_special_forces_out_of_supply_factor_tt
	add_to_variable = { GER_special_forces_out_of_supply_factor = GER_special_forces_out_of_supply_factor_change }
}
update_GER_army_attack_speed_factor = {
	custom_effect_tooltip = GER_army_attack_speed_factor_tt
	add_to_variable = { GER_aarmy_attack_speed_factor = GER_army_attack_speed_factor_change }
}
update_GER_special_forces_attack_factor = {
	custom_effect_tooltip = GER_special_forces_attack_factor_tt
	add_to_variable = { GER_special_forces_attack_factor = GER_special_forces_attack_factor_change }
}
update_GER_initiative_factor = {
	custom_effect_tooltip = GER_initiative_factor_tt
	add_to_variable = { GER_initiative_factor = GER_initiative_factor_change }
}
update_GER_army_armor_attack_factor = {
	custom_effect_tooltip = GER_army_armor_attack_factor_tt
	add_to_variable = { GER_army_armor_attack_factor = GER_army_armor_attack_factor_change }
}
update_GER_army_armor_defence_factor = {
	custom_effect_tooltip = GER_army_armor_defence_factor_tt
	add_to_variable = { GER_army_armor_defence_factor = GER_army_armor_defence_factor_change }
}
update_GER_army_armor_speed_factor = {
	custom_effect_tooltip = GER_army_armor_speed_factor_tt
	add_to_variable = { GER_army_armor_speed_factor = GER_army_armor_speed_factor_change }
}
update_GER_intel_from_combat_factor = {
	custom_effect_tooltip = GER_intel_from_combat_factor_tt
	add_to_variable = { GER_intel_from_combat_factor = GER_intel_from_combat_factor_change }
}
update_GER_army_infantry_attack_factor = {
	custom_effect_tooltip = GER_army_infantry_attack_factor_tt
	add_to_variable = { GER_army_infantry_attack_factor = GER_army_infantry_attack_factor_change }
}
update_GER_army_infantry_defence_factor = {
	custom_effect_tooltip = GER_army_infantry_defence_factor_tt
	add_to_variable = { GER_army_infantry_defence_factor = GER_army_infantry_defence_factor_change }
}
update_GER_mechanized_attack_factor = {
	custom_effect_tooltip = GER_mechanized_attack_factor_tt
	add_to_variable = { GER_mechanized_attack_factor = GER_mechanized_attack_factor_change }
}
update_GER_mechanized_defence_factor = {
	custom_effect_tooltip = GER_mechanized_defence_factor_tt
	add_to_variable = { GER_mechanized_defence_factor = GER_mechanized_defence_factor_change }
}
update_GER_intercept_efficiency = {
	custom_effect_tooltip = GER_intercept_efficiency_tt
	add_to_variable = { GER_intercept_efficiency = GER_intercept_efficiency_change }
}
update_GER_cas_efficiency = {
	custom_effect_tooltip = GER_cas_efficiency_tt
	add_to_variable = { GER_cas_efficiency = GER_cas_efficiency_change }
}
update_GER_naval_strike_targetting = {
	custom_effect_tooltip = GER_naval_strike_targetting_tt
	add_to_variable = { GER_naval_strike_targetting = GER_naval_strike_targetting_change }
}
update_GER_air_accidents = {
	custom_effect_tooltip = GER_air_accidents_tt
	add_to_variable = { GER_air_accidents = GER_air_accidents_change }
}
update_GER_ground_attack = {
	custom_effect_tooltip = GER_ground_attack_tt
	add_to_variable = { GER_ground_attack = GER_ground_attack_change }
}
update_GER_air_night_penalty = {
	custom_effect_tooltip = GER_air_night_penalty_tt
	add_to_variable = { GER_air_night_penalty = GER_air_night_penalty_change }
}
update_GER_air_weather_penalty = {
	custom_effect_tooltip = GER_air_weather_penalty_tt
	add_to_variable = { GER_air_weather_penalty = GER_air_weather_penalty_change }
}
update_GER_home_defence_factor = {
	custom_effect_tooltip = GER_home_defence_factor_tt
	add_to_variable = { GER_home_defence_factor = GER_home_defence_factor_change }
} #
update_GER_naval_speed_factor = {
	custom_effect_tooltip = GER_naval_speed_factor_tt
	add_to_variable = { GER_naval_speed_factor = GER_naval_speed_factor_change }
}
update_GER_naval_damage_factor = {
	custom_effect_tooltip = GER_naval_damage_factor_tt
	add_to_variable = { GER_naval_damage_factor = GER_naval_damage_factor_change }
}
update_GER_naval_defense_factor = {
	custom_effect_tooltip = GER_naval_defense_factor_tt
	add_to_variable = { GER_naval_defense_factor = GER_naval_defense_factor_change }
}
update_GER_navy_anti_air_attack_factor = {
	custom_effect_tooltip = GER_navy_anti_air_attack_factor_tt
	add_to_variable = { GER_navy_anti_air_attack_factor = GER_navy_anti_air_attack_factor_change }
}
update_GER_naval_coordination = {
	custom_effect_tooltip = GER_naval_coordination_tt
	add_to_variable = { GER_naval_coordination = GER_naval_coordination_change }
}
update_GER_naval_detection = {
	custom_effect_tooltip = GER_naval_detection_tt
	add_to_variable = { GER_naval_detection = GER_naval_detection_change }
}
update_GER_navy_org_factor = {
	custom_effect_tooltip = GER_navy_org_factor_tt
	add_to_variable = { GER_navy_org_factor = GER_navy_org_factor_change }
}
update_GER_acclimatization_hot_climate_gain_factor = {
	custom_effect_tooltip = GER_acclimatization_hot_climate_gain_factor_tt
	add_to_variable = { GER_acclimatization_hot_climate_gain_factor = GER_acclimatization_hot_climate_gain_factor_change }
}
update_GER_acclimatization_cold_climate_gain_factor = {
	custom_effect_tooltip = GER_acclimatization_cold_climate_gain_factor_tt
	add_to_variable = { GER_acclimatization_cold_climate_gain_factor = GER_acclimatization_cold_climate_gain_factor_change }
}
update_GER_air_training_xp_gain_factor = {
	custom_effect_tooltip = GER_air_training_xp_gain_factor_tt
	add_to_variable = { GER_air_training_xp_gain_factor = GER_air_training_xp_gain_factor_change }
}
update_GER_air_ace_generator_chance_factor = {
	custom_effect_tooltip = GER_air_ace_generator_chance_factor_tt
	add_to_variable = { GER_air_ace_generator_chance_factor = GER_air_ace_generator_chance_factor_change }
}
update_GER_army_leader_start_level = {
	custom_effect_tooltip = GER_army_leader_start_level_tt
	add_to_variable = { GER_army_leader_start_level = GER_army_leader_start_level_change }
}
update_GER_army_leader_start_attack_level = {
	custom_effect_tooltip = GER_army_leader_start_attack_level_tt
	add_to_variable = { GER_army_leader_start_attack_level = GER_army_leader_start_attack_level_change }
}
update_GER_army_leader_start_defence_level = {
	custom_effect_tooltip = GER_army_leader_start_defence_level_tt
	add_to_variable = { GER_army_leader_start_defence_level = GER_army_leader_start_defence_level_change }
}
update_GER_army_leader_start_logistics_level = {
	custom_effect_tooltip = GER_army_leader_start_logistics_level_tt
	add_to_variable = { GER_army_leader_start_logistics_level = GER_army_leader_start_logistics_level_change }
}
update_GER_army_leader_start_planning_level = {
	custom_effect_tooltip = GER_army_leader_start_planning_level_tt
	add_to_variable = { GER_army_leader_start_planning_level = GER_army_leader_start_planning_level_change }
}
update_GER_spotting_chance = {
	custom_effect_tooltip = GER_spotting_chance_tt
	add_to_variable = { GER_spotting_chance = GER_spotting_chance_change }
}
update_GER_naval_accidents_chance = {
	custom_effect_tooltip = GER_naval_accidents_chance_tt
	add_to_variable = { GER_naval_accidents_chance = GER_naval_accidents_chance_change }
}
update_GER_modifier_army_sub_unit_L_Air_assault_Bat_attack_factor = {
	custom_effect_tooltip = GER_modifier_army_sub_unit_L_Air_assault_Bat_attack_factor_tt
	add_to_variable = { GER_modifier_army_sub_unit_L_Air_assault_Bat_attack_factor = GER_modifier_army_sub_unit_L_Air_assault_Bat_attack_factor_change }
}
update_GER_modifier_army_sub_unit_L_Air_assault_Bat_defence_factor = {
	custom_effect_tooltip = GER_modifier_army_sub_unit_L_Air_assault_Bat_defence_factor_tt
	add_to_variable = { GER_modifier_army_sub_unit_L_Air_assault_Bat_defence_factor = GER_modifier_army_sub_unit_L_Air_assault_Bat_defence_factor_change }
}
update_GER_modifier_army_sub_unit_Arm_Air_assault_Bat_attack_factor = {
	custom_effect_tooltip = GER_modifier_army_sub_unit_Arm_Air_assault_Bat_attack_factor_tt
	add_to_variable = { GER_modifier_army_sub_unit_Arm_Air_assault_Bat_attack_factor = GER_modifier_army_sub_unit_Arm_Air_assault_Bat_attack_factor_change }
}
update_GER_modifier_army_sub_unit_Arm_Air_assault_Bat_defence_factor = {
	custom_effect_tooltip = GER_modifier_army_sub_unit_Arm_Air_assault_Bat_defence_factor_chance_tt
	add_to_variable = { GER_modifier_army_sub_unit_Arm_Air_assault_Bat_defence_factor = GER_modifier_army_sub_unit_Arm_Air_assault_Bat_defence_factor_change }
}
update_GER_modifier_army_sub_unit_attack_helo_bat_attack_factor = {
	custom_effect_tooltip = GER_modifier_army_sub_unit_attack_helo_bat_attack_factor_tt
	add_to_variable = { GER_modifier_army_sub_unit_attack_helo_bat_attack_factor = GER_modifier_army_sub_unit_attack_helo_bat_attack_factor_change }
}
update_GER_modifier_army_sub_unit_attack_helo_bat_defence_factor = {
	custom_effect_tooltip = GER_modifier_army_sub_unit_attack_helo_bat_defence_factor_tt
	add_to_variable = { GER_modifier_army_sub_unit_attack_helo_bat_defence_factor = GER_modifier_army_sub_unit_attack_helo_bat_defence_factor_change }
}
update_GER_army_morale_factor = {
	custom_effect_tooltip = GER_army_morale_factor_tt
	add_to_variable = { GER_army_morale_factor = GER_army_morale_factor_change }
}
update_GER_army_core_defence_factor = {
	custom_effect_tooltip = GER_army_core_defence_factor_tt
	add_to_variable = { GER_army_core_defence_factor = GER_army_core_defence_factor_change }
}
update_GER_army_core_attack_factor = {
	custom_effect_tooltip = GER_army_core_attack_factor_tt
	add_to_variable = { GER_army_core_attack_factor = GER_army_core_attack_factor_change }
}
update_GER_combat_width_factor = {
	custom_effect_tooltip = GER_combat_width_factor_tt
	add_to_variable = { GER_combat_width_factor = GER_combat_width_factor_change }
}
update_GER_recon_factor_while_entrenched = {
	custom_effect_tooltip = GER_recon_factor_while_entrenched_tt
	add_to_variable = { GER_recon_factor_while_entrenched = GER_recon_factor_while_entrenched_change }
}
update_GER_planning_speed = {
	custom_effect_tooltip = GER_planning_speed_tt
	add_to_variable = { GER_planning_speed = GER_planning_speed_change }
}
update_GER_supply_factor = {
	custom_effect_tooltip = GER_supply_factor_tt
	add_to_variable = { GER_supply_factor = GER_supply_factor_change }
}
update_GER_no_supply_grace = {
	custom_effect_tooltip = GER_no_supply_grace_tt
	add_to_variable = { GER_no_supply_grace = GER_no_supply_grace_change }
}
update_GER_land_reinforce_rate = {
	custom_effect_tooltip = GER_land_reinforce_rate_tt
	add_to_variable = { GER_land_reinforce_rate = GER_land_reinforce_rate_change }
}
update_GER_out_of_supply_factor = {
	custom_effect_tooltip = GER_out_of_supply_factor_tt
	add_to_variable = { GER_out_of_supply_factor = GER_out_of_supply_factor_change }
}
update_GER_send_volunteer_size = {
	custom_effect_tooltip = GER_send_volunteer_size_tt
	add_to_variable = { GER_send_volunteer_size = GER_send_volunteer_size_change }
}
update_GER_minimum_training_level = {
	custom_effect_tooltip = GER_minimum_training_level_tt
	add_to_variable = { GER_minimum_training_level = GER_minimum_training_level_change }
}
update_GER_army_artillery_defence_factor = {
	custom_effect_tooltip = GER_army_artillery_defence_factor_tt
	add_to_variable = { GER_army_artillery_defence_factor = GER_army_artillery_defence_factor_change }
}
update_GER_training_time_factor = {
	custom_effect_tooltip = GER_training_time_factor_tt
	add_to_variable = { GER_training_time_factor = GER_training_time_factor_change }
}
update_GER_supply_consumption_factor = {
	custom_effect_tooltip = GER_supply_consumption_factor_tt
	add_to_variable = { GER_supply_consumption_factor = GER_supply_consumption_factor_change }
}
update_GER_dig_in_speed_factor = {
	custom_effect_tooltip = GER_dig_in_speed_factor_tt
	add_to_variable = { GER_dig_in_speed_factor = GER_dig_in_speed_factor_change }
}
update_GER_mobilization_speed = {
	custom_effect_tooltip = GER_mobilization_speed_tt
	add_to_variable = { GER_mobilization_speed = GER_mobilization_speed_change }
}
update_GER_command_abilities_cost_factor = {
	custom_effect_tooltip = GER_command_abilities_cost_factor_tt
	add_to_variable = { GER_command_abilities_cost_factor = GER_command_abilities_cost_factor_change }
}
update_GER_army_speed_factor = {
	custom_effect_tooltip = GER_army_speed_factor_tt
	add_to_variable = { GER_army_speed_factor = GER_army_speed_factor_change }
}
update_GER_army_org_factor = {
	custom_effect_tooltip = GER_army_org_factor_tt
	add_to_variable = { GER_army_org_factor = GER_army_org_factor_change }
}
update_GER_special_forces_cap = {
	custom_effect_tooltip = GER_special_forces_cap_tt
	add_to_variable = { GER_special_forces_cap = GER_special_forces_cap_change }
}
update_GER_special_forces_defence_factor = {
	custom_effect_tooltip = GER_special_forces_defence_factor_tt
	add_to_variable = { GER_special_forces_defence_factor = GER_special_forces_defence_factor_change }
}
update_GER_modifier_army_sub_unit_Special_Forces_speed_factor = {
	custom_effect_tooltip = GER_modifier_army_sub_unit_Special_Forces_speed_factor_tt
	add_to_variable = { GER_modifier_army_sub_unit_Special_Forces_speed_factor = GER_modifier_army_sub_unit_Special_Forces_speed_factor_change }
}
update_GER_amphibious_invasion = {
	custom_effect_tooltip = GER_amphibious_invasion_tt
	add_to_variable = { GER_amphibious_invasion = GER_amphibious_invasion_change }
}
update_GER_air_range_factor = {
	custom_effect_tooltip = GER_air_range_factor_tt
	add_to_variable = { GER_air_range_factor = GER_air_range_factor_change }
}
update_GER_air_close_air_support_org_damage_factor = {
	custom_effect_tooltip = GER_air_close_air_support_org_damage_factor_tt
	add_to_variable = { GER_air_close_air_support_org_damage_factor = GER_air_close_air_support_org_damage_factor_change }
}
update_GER_air_superiority_bonus_in_combat = {
	custom_effect_tooltip = GER_air_superiority_bonus_in_combat_tt
	add_to_variable = { GER_air_superiority_bonus_in_combat = GER_air_superiority_bonus_in_combat_change }
}
update_GER_air_superiority_efficiency = {
	custom_effect_tooltip = GER_air_superiority_efficiency_tt
	add_to_variable = { GER_air_superiority_efficiency = GER_air_superiority_efficiency_change }
}
update_GER_air_mission_xp_gain_factor = {
	custom_effect_tooltip = GER_air_mission_xp_gain_factor_tt
	add_to_variable = { GER_air_mission_xp_gain_factor = GER_air_mission_xp_gain_factor_change }
}
update_GER_naval_mine_hit_chance = {
	custom_effect_tooltip = GER_naval_mine_hit_chance_tt
	add_to_variable = { GER_naval_mine_hit_chance = GER_naval_mine_hit_chance_change }
}
update_GER_naval_mines_damage_factor = {
	custom_effect_tooltip = GER_naval_mines_damage_factor_tt
	add_to_variable = { GER_naval_mines_damage_factor = GER_naval_mines_damage_factor_change }
}
update_GER_naval_mines_effect_reduction = {
	custom_effect_tooltip = GER_naval_mines_effect_reduction_tt
	add_to_variable = { GER_naval_mines_effect_reduction = GER_naval_mines_effect_reduction_change }
}
update_GER_naval_morale_factor = {
	custom_effect_tooltip = GER_naval_morale_factor_tt
	add_to_variable = { GER_naval_morale_factor = GER_naval_morale_factor_change }
}
update_GER_navy_capital_ship_attack_factor = {
	custom_effect_tooltip = GER_navy_capital_ship_attack_factor_tt
	add_to_variable = { GER_navy_capital_ship_attack_factor = GER_navy_capital_ship_attack_factor_change }
}
update_GER_naval_invasion_prep_speed = {
	custom_effect_tooltip = GER_naval_invasion_prep_speed_tt
	add_to_variable = { GER_naval_invasion_prep_speed = GER_naval_invasion_prep_speed_change }
}
update_GER_naval_invasion_planning_bonus_speed = {
	custom_effect_tooltip = GER_naval_invasion_planning_bonus_speed_tt
	add_to_variable = { GER_naval_invasion_planning_bonus_speed = GER_naval_invasion_planning_bonus_speed_change }
}
update_GER_critical_receive_chance = {
	custom_effect_tooltip = GER_critical_receive_chance_tt
	add_to_variable = { GER_critical_receive_chance = GER_critical_receive_chance_change }
}
update_GER_naval_critical_score_chance_factor = {
	custom_effect_tooltip = GER_naval_critical_score_chance_factor_tt
	add_to_variable = { GER_naval_critical_score_chance_factor = GER_naval_critical_score_chance_factor_change }
}
update_GER_naval_hit_chance = {
	custom_effect_tooltip = GER_naval_hit_chance_tt
	add_to_variable = { GER_naval_hit_chance = GER_naval_hit_chance_change }
}
update_GER_naval_enemy_fleet_size_ratio_penalty_factor = {
	custom_effect_tooltip = GER_naval_enemy_fleet_size_ratio_penalty_factor_tt
	add_to_variable = { GER_naval_enemy_fleet_size_ratio_penalty_factor = GER_naval_enemy_fleet_size_ratio_penalty_factor_change }
}
update_GER_naval_retreat_speed_after_initial_combat = {
	custom_effect_tooltip = GER_naval_retreat_speed_after_initial_combat_tt
	add_to_variable = { GER_naval_retreat_speed_after_initial_combat = GER_naval_retreat_speed_after_initial_combat_change }
}
update_GER_naval_retreat_chance_after_initial_combat = {
	custom_effect_tooltip = GER_naval_retreat_chance_after_initial_combat_tt
	add_to_variable = { GER_naval_retreat_chance_after_initial_combat = GER_naval_retreat_chance_after_initial_combat_change }
}
update_GER_naval_torpedo_cooldown_factor = {
	custom_effect_tooltip = GER_naval_torpedo_cooldown_factor_tt
	add_to_variable = { GER_naval_torpedo_cooldown_factor = GER_naval_torpedo_cooldown_factor_change }
}
update_GER_naval_torpedo_hit_chance_factor = {
	custom_effect_tooltip = GER_naval_torpedo_hit_chance_factor_tt
	add_to_variable = { GER_naval_torpedo_hit_chance_factor = GER_naval_torpedo_hit_chance_factor_change }
}
update_GER_screening_without_screens = {
	custom_effect_tooltip = GER_screening_without_screens_tt
	add_to_variable = { GER_screening_without_screens = GER_screening_without_screens_change }
}
update_GER_ships_at_battle_start = {
	custom_effect_tooltip = GER_ships_at_battle_start_tt
	add_to_variable = { GER_ships_at_battle_start = GER_ships_at_battle_start_change }
}
update_GER_navy_screen_attack_factor = {
	custom_effect_tooltip = GER_navy_screen_attack_factor_tt
	add_to_variable = { GER_navy_screen_attack_factor = GER_navy_screen_attack_factor_change }
}
update_GER_navy_screen_defence_factor = {
	custom_effect_tooltip = GER_navy_screen_defence_factor_tt
	add_to_variable = { GER_navy_screen_defence_factor = GER_navy_screen_defence_factor_change }
}


#Diplomatie
update_GER_dip_renewable_energy_gain_multiplier = {
	custom_effect_tooltip = GER_dip_renewable_energy_gain_multiplier_tt
	add_to_variable = { GER_dip_renewable_energy_gain_multiplier = GER_dip_renewable_energy_gain_multiplier_change }
}
update_GER_dip_productivity_growth_modifier = {
	custom_effect_tooltip = GER_dip_productivity_growth_modifier_tt
	add_to_variable = { GER_dip_productivity_growth_modifier = GER_dip_productivity_growth_modifier_change }
}
update_GER_dip_industrial_capacity_factory = {
	custom_effect_tooltip = GER_dip_industrial_capacity_factory_tt
	add_to_variable = { GER_dip_industrial_capacity_factory = GER_dip_industrial_capacity_factory_change }
}
update_GER_dip_production_factory_start_efficiency_factor = {
	custom_effect_tooltip = GER_dip_production_factory_start_efficiency_factor_tt
	add_to_variable = { GER_dip_production_factory_start_efficiency_factor = GER_dip_production_factory_start_efficiency_factor_change }
}
update_GER_dip_production_speed_industrial_complex_factor = {
	custom_effect_tooltip = GER_dip_production_speed_industrial_complex_factor_tt
	add_to_variable = { GER_dip_production_speed_industrial_complex_factor = GER_dip_production_speed_industrial_complex_factor_change }
}
update_GER_dip_production_speed_offices_factor = {
	custom_effect_tooltip = GER_dip_production_speed_offices_factor_tt
	add_to_variable = { GER_dip_production_speed_offices_factor = GER_dip_production_speed_offices_factor_change }
}
update_GER_dip_productivity_growth_modifier = {
	custom_effect_tooltip = GER_dip_productivity_growth_modifier_tt
	add_to_variable = { GER_dip_productivity_growth_modifier = GER_dip_productivity_growth_modifier_change }
}
update_GER_dip_research_speed_factor = {
	custom_effect_tooltip = GER_dip_research_speed_factor_tt
	add_to_variable = { GER_dip_research_speed_factor = GER_dip_research_speed_factor_change }
}
update_GER_dip_foreign_influence_modifier = {
	custom_effect_tooltip = GER_dip_foreign_influence_modifier_tt
	add_to_variable = { GER_dip_foreign_influence_modifier = GER_dip_foreign_influence_modifier_change }
}
update_GER_dip_migration_rate_value_factor = {
	custom_effect_tooltip = GER_dip_migration_rate_value_factor_tt
	add_to_variable = { GER_dip_migration_rate_value_factor = GER_dip_migration_rate_value_factor_change }
}
update_GER_dip_return_on_investment_modifier = {
	custom_effect_tooltip = GER_dip_return_on_investment_modifier_tt
	add_to_variable = { GER_dip_return_on_investment_modifier = GER_dip_return_on_investment_modifier_change }
}
update_GER_dip_foreign_influence_defense_modifier = {
	custom_effect_tooltip = GER_dip_foreign_influence_defense_modifier_tt
	add_to_variable = { GER_dip_foreign_influence_defense_modifier = GER_dip_foreign_influence_defense_modifier_change }
}
update_GER_dip_production_speed_infrastructure_factor = {
	custom_effect_tooltip = GER_dip_production_speed_infrastructure_factor_tt
	add_to_variable = { GER_dip_production_speed_infrastructure_factor = GER_dip_production_speed_infrastructure_factor_change }
}
##citizen in uniform
#update_GER_democratic_drift_ciu = {
#	custom_effect_tooltip = GER_democratic_drift_ciu_tt
#	add_to_variable = { GER_democratic_drift_ciu = GER_democratic_drift_ciu_change }
#}
#update_GER_stability_factor_ciu = {
#	custom_effect_tooltip = GER_stability_factor_ciu_tt
#	add_to_variable = { GER_stability_factor_ciu = GER_stability_factor_ciu_change }
#}
#update_GER_political_power_factor_ciu = {
#	custom_effect_tooltip = GER_political_power_factor_ciu_tt
#	add_to_variable = { GER_political_power_factor_ciu = GER_political_power_factor_ciu_change }
#}
#update_GER_foreign_influence_defense_modifier_ciu = {
#	custom_effect_tooltip = GER_foreign_influence_defense_modifier_ciu_tt
#	add_to_variable = { GER_dip_production_speed_infrastructure_factor = GER_foreign_influence_defense_modifier_ciu_change }
#}
#update_GER_drift_defence_factor_ciu = {
#	custom_effect_tooltip = GER_drift_defence_factor_ciu_tt
#	add_to_variable = { GER_drift_defence_factor_ciu = GER_drift_defence_factor_ciu_change }
#}
#update_GER_communism_drift_ciu = {
#	custom_effect_tooltip = GER_communism_drift_ciu_tt
#	add_to_variable = { GER_communism_drift_ciu = GER_communism_drift_ciu_change }
#}
#update_GER_nationalist_drift_ciu = {
#	custom_effect_tooltip = GER_nationalist_drift_ciu_tt
#	add_to_variable = { GER_nationalist_drift_ciu = GER_nationalist_drift_ciu_change }
#}
#
#
#GER_bundeswehr_extremism_check = {
#	# Check for foreign influence (SOV)
#	if = {
#		limit = {
#			SOV = {
#				is_top3_influencer = yes
#			}
#		}
#		add_to_variable = { GER_dark_winds_status = 3 }
#	}
#	else_if = {
#		limit = {
#			SOV = {
#				is_top5_influencer = yes
#			}
#		}
#		add_to_variable = { GER_dark_winds_status = 2 }
#	}
#	else_if = {
#		limit = {
#			SOV = {
#				is_influencer = yes
#			}
#		}
#		add_to_variable = { GER_dark_winds_status = 1 }
#	}
#	# Check for German Societal Leanings
#	if = {
#		limit = {
#			is_power_balance_in_range = {
#				id = GER_power_balance
#				range = GER_right_side_range
#			}
#		}
#		add_to_variable = { GER_dark_winds_status = 1 }
#	}
#	else_if = {
#		limit = {
#			is_power_balance_in_range = {
#				id = GER_power_balance
#				range = GER_middle_right_side_range
#			}
#		}
#		add_to_variable = { GER_dark_winds_status = 2 }
#	}
#	else_if = {
#		limit = {
#			is_power_balance_in_range = {
#				id = GER_power_balance
#				range = GER_far_right_side_range
#			}
#		}
#		add_to_variable = { GER_dark_winds_status = 3 }
#	}
#	else_if = {
#		limit = {
#			is_power_balance_in_range = {
#				id = GER_power_balance
#				range = GER_Anti_Militarism
#			}
#		}
#		add_to_variable = { GER_dark_winds_status = 4 }
#	}
#	# Check for BfV Status
#	if = {
#		limit = {
#			check_variable = { GER_rightwing_department_status < 40 }
#		}
#		add_to_variable = { GER_dark_winds_status = 1 }
#	}
#	else_if = {
#		limit = {
#			check_variable = { GER_rightwing_department_status < 30 }
#		}
#		add_to_variable = { GER_dark_winds_status = 2 }
#	}
#	else_if = {
#		limit = {
#			check_variable = { GER_rightwing_department_status < 20 }
#		}
#		add_to_variable = { GER_dark_winds_status = 3 }
#	}
#	else_if = {
#		limit = {
#			check_variable = { GER_rightwing_department_status < 10 }
#		}
#		add_to_variable = { GER_dark_winds_status = 4 }
#	}
#}