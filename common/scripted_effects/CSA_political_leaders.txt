set_leader_CSA = {

	###############################
	##### Western Autocrats #######
	###############################
	if = { limit = { has_country_flag = set_Western_Autocracy }
		if = { limit = { check_variable = { Western_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Sir Charles Hatt IV"
				picture = ""
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
					railroad_enthusiast
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Western_Autocracy_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Sir Richard Hatt"
				picture = ""
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
					railroad_enthusiast
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}

	###################################
	##### Western Conservatives #######
	###################################
	else_if = { limit = { has_country_flag = set_conservatism }
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ronnie Musgrove"
				picture = "Ronnie_Musgrove.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					lawyer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { conservatism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Phil Bredesen"
				picture = "Phil_Bredesen.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					likeable
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}

	################################
	##### Western Liberalism #######
	################################
	else_if = { limit = { has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Don Eugene Siegelman"
				picture = "Don_Eugene_Siegelman.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { liberalism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mike Huckabee"
				picture = "Mike_Huckabee.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}

	################################
	##### Western Socialism ########
	################################
	else_if = { limit = { has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Bob Riley"
				picture = "Bob_Riley.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { socialism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Roy Barnes"
				picture = "Roy_Barnes.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}

	###################################
	##### Neutral Conservative ########
	###################################
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Kanye \"Ye\" West"
				picture = "Kanye_West.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					likeable
					emotional
					rash
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}

	##########################
	##### Oligarchism ########
	##########################
	else_if = { limit = { has_country_flag = set_oligarchism }
		if = { limit = { check_variable = { oligarchism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mike Foster"
				picture = "Mike_Foster.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism
					businessman
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}

	##########################
	##### Libertarian ########
	##########################
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Barbara Howe"
				picture = "Barbara_Howe.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
					likeable
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}


	###############################
	##### Right Wing Populism #####
	###############################
	else_if = { limit = { has_country_flag = set_Nat_Populism }
		if = { limit = { check_variable = { Nat_Populism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Sonny Perdue"
				picture = "Sonny_Perdue.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
					businessman
					agrarian_expert
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}

	###################
	##### Fascism #####
	###################
	else_if = { limit = { has_country_flag = set_Nat_Fascism }
		if = { limit = { check_variable = { Nat_Fascism_leader = 2 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Franklin Sanders"
				picture = "Franklin_Sanders.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
					emotional
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Nat_Fascism_leader = 1 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Micheal Hill"
				picture = "Micheal_Hill.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
					emotional
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Nat_Fascism_leader = 0 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "David Duke"
				picture = "David_Duke.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}

	##########################
	##### Military Junta #####
	##########################
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 4 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mathew \"The Marshal\" Crouder"
				picture = "Mathew_Crouder.dds"
				desc = "MATHEW_CROUDER_DESC"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					emotional
					rational
					inexperienced
					railroad_enthusiast
					logistical_tunnel_vision
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 3 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "General William L. Freeman Jr."
				picture = "William_L_Freeman_Jr.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
					army_general
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 2 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "General Willie A. Alexander"
				picture = "Willie_A_Alexander.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
					army_general
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "General Henry H. Shelton"
				picture = "Zgeneral_Henry_H_Shelton.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
					army_general
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "General Tommy Franks"
				picture = "Zgeneral_Tommy_Franks.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					military_career
					army_general
					emotional
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
		}
	}

	##########################
	##### Military Junta #####
	##########################
	else_if = { limit = { has_country_flag = set_Monarchist }
		if = { limit = { check_variable = { Monarchist_leader = 4 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Joseph I"
				picture = ""
				ideology = Nat_Autocracy
				traits = {
					nationalist_Monarchist
					emotional
					logistical_tunnel_vision
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
		}
		if = { limit = { check_variable = { Monarchist_leader = 3 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "John II"
				picture = ""
				ideology = Nat_Autocracy
				traits = {
					nationalist_Monarchist
					rational
					army_general
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
		}
		if = { limit = { check_variable = { Monarchist_leader = 2 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Andrew I"
				picture = ""
				ideology = Nat_Autocracy
				traits = {
					nationalist_Monarchist
					likeable
					public_health_admin
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
		}
		if = { limit = { check_variable = { Monarchist_leader = 1 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "John I"
				picture = ""
				ideology = Nat_Autocracy
				traits = {
					nationalist_Monarchist
					rash
					agricultural_resource_economist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
		}
		if = { limit = { check_variable = { Monarchist_leader = 0 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Terry I"
				picture = ""
				ideology = Nat_Autocracy
				traits = {
					nationalist_Monarchist
					humble
					capable
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
		}
	}
}