on_actions = {
	on_daily_TAJ = {
		effect = {
			set_variable = { badakhshan_resistance_cap = TAJ_badakhshan_unrest_var }
			multiply_variable = { badakhshan_resistance_cap = 0.005 }
			set_variable = { badakhshan_resistance_garrison_requirement = TAJ_badakhshan_unrest_var }
			multiply_variable = { badakhshan_resistance_garrison_requirement = 0.03 }
			set_variable = { badakhshan_resistance_garrison_damage = TAJ_badakhshan_unrest_var }
			multiply_variable = { badakhshan_resistance_garrison_damage = 0.019 }
			multiply_variable = { badakhshan_resistance_garrison_damage = 0.019 }
			subtract_from_variable = { badakhshan_resistance_garrison_damage = 0.999 }
		}
	}
	on_weekly_TAJ = {
		effect = {
			if = { # Fail safe
				limit = {
					check_variable = {
						TAJ_badakhshan_unrest_var < 0
					}
				}
				set_variable = {
					TAJ_badakhshan_unrest_var = 0
				}
			}
		}
	}
	on_monthly_TAJ = {
		effect = {
			if = {
				limit = {
					has_country_flag = TAJ_monthly_distribution
				}
				set_temp_variable = { temp_opinion = 2 }
				change_the_military_opinion = yes
				change_oligarchs_opinion = yes
				change_farmers_opinion = yes
				change_small_medium_business_owners_opinion = yes
				change_fossil_fuel_industry_opinion = yes
				change_labour_unions_opinion = yes
				change_international_bankers_opinion = yes
				change_industrial_conglomerates_opinion = yes
				change_defense_industry_opinion = yes
				change_the_ulema_opinion = yes
				change_iranian_quds_force_opinion = yes
				change_landowners_opinion = yes
				change_intelligence_community_opinion = yes
				change_maritime_industry_opinion = yes
				if = {
					limit = {
						has_country_flag = TAJ_high_security_flag
					}
					# Get some cash
					set_temp_variable = { treasury_change = gdp_total }
					multiply_temp_variable = { treasury_change = 0.01 }
					set_temp_variable = { temp_reparation = treasury_change }
					modify_treasury_effect = yes
					newline = yes
					# Get some manpower
					add_manpower = 125
					# Get some rifles
					newline = yes
					add_equipment_to_stockpile = {
						type = infantry_weapons
						amount = 75
						producer = SOV
					}
					newline = yes
					# Get some utility trucks
					add_equipment_to_stockpile = {
						type = util_vehicle_0
						amount = 15
						producer = C01
					}
					newline = yes
					# Political Power
					add_political_power = 15
				}
				else_if = {
					limit = {
						has_country_flag = TAJ_medium_security_flag
					}
					# Get some cash
					set_temp_variable = { treasury_change = gdp_total }
					multiply_temp_variable = { treasury_change = 0.02 }
					set_temp_variable = { temp_reparation = treasury_change }
					modify_treasury_effect = yes
					newline = yes
					# Get some manpower
					add_manpower = 250
					# Get some rifles
					newline = yes
					add_equipment_to_stockpile = {
						type = infantry_weapons
						amount = 175
						producer = SOV
					}
					newline = yes
					# Get some utility trucks
					add_equipment_to_stockpile = {
						type = util_vehicle_0
						amount = 35
						producer = C01
					}
					newline = yes
					# Political Power
					add_political_power = 35
					# Command Power & XP
					add_command_power = 5
					newline = yes
					army_experience = 5
				}
				else_if = {
					limit = {
						has_country_flag = TAJ_negligible_security_flag
					}
					# Get some cash
					set_temp_variable = { treasury_change = gdp_total }
					multiply_temp_variable = { treasury_change = 0.03 }
					set_temp_variable = { temp_reparation = treasury_change }
					modify_treasury_effect = yes
					newline = yes
					# Get some manpower
					add_manpower = 350
					# Get some rifles
					newline = yes
					add_equipment_to_stockpile = {
						type = infantry_weapons
						amount = 275
						producer = SOV
					}
					newline = yes
					# Get some utility trucks
					add_equipment_to_stockpile = {
						type = util_vehicle_0
						amount = 55
						producer = C01
					}
					newline = yes
					# Political Power
					add_political_power = 60
					newline = yes
					# Command Power & XP
					add_command_power = 15
					newline = yes
					# Army XP
					army_experience = 10
					newline = yes
					# Random ass influence on someone
					random_country = {
						set_temp_variable = { percent_change = 1.00 }
						set_temp_variable = { tag_index = TAJ }
						set_temp_variable = { influence_target = THIS }
						change_influence_percentage = yes
					}
				}
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = TAJ_monthly_distribution
					}
					has_dynamic_modifier = {
						modifier = TAJ_russian_payments
					}
				}
				country_event = {
					id = tajik_corruption.1
					days = 29
				}
			}
			# 6 Year thing
			if = {
				limit = {
					has_country_flag = TAJ_civs_yearly_flag
					check_variable = {
						TAJ_yearly_var_civ = 0
					}
				}
				add_to_variable = { var = TAJ_yearly_var_civ value = 12 }
				clamp_variable = { var = TAJ_yearly_var_civ min = 0 max = 12 }
				one_random_industrial_complex = yes
			}
			if = {
				limit = {
					has_country_flag = TAJ_civs_yearly_flag
					check_variable = {
						TAJ_yearly_var_civ > 0
					}
				}
				add_to_variable = { var = TAJ_yearly_var_civ value = -1 }
				clamp_variable = { var = TAJ_yearly_var_civ min = 0 max = 12 }
			}
			if = {
				limit = {
					has_completed_focus = TAJ_masks_of_gold
				}
				if = {
					limit = {
						has_country_flag = TAJ_high_security_flag
					}
					TAJ_random_pop_up_bad_high = yes
					TAJ_random_pop_up_good_high = yes
				}
				else_if = {
					limit = {
						has_country_flag = TAJ_medium_security_flag
					}
					TAJ_random_pop_up_bad_medium = yes
					TAJ_random_pop_up_good_medium = yes
				}
				else_if = {
					limit = {
						has_country_flag = TAJ_negligible_security_flag
					}
					TAJ_random_pop_up_bad_low = yes
					TAJ_random_pop_up_good_low = yes
				}
			}
			# Modifier from Nat-Guard decision
			if = {
				limit = {
					has_country_leader = { name = "Sayid Abdulloh Nuri" ruling_only = yes }
				}
				add_to_variable = { var = TAJ_nuri_months value = -1 }
				clamp_variable = { var = TAJ_nuri_months min = -36 max = 0 }
			}
			if = {
				limit = {
					has_country_leader = { name = "Sayid Abdulloh Nuri" ruling_only = yes }
					check_variable = {
						TAJ_nuri_months = -36
					}
				}
				country_event = tajik_focus.10
			}
			if = {
				limit = {
					has_idea = TAJ_national_guard_deployed
				}
				add_to_variable = { var = TAJ_radical_influence value = -1 }
				clamp_variable = { var = TAJ_radical_influence min = 0 max = 100 }
			}
			# Radicalism check
			if = {
				limit = {
					NOT = {
						has_country_flag = TAJ_radicalism_ended
						neutrality_neutral_muslim_brotherhood_are_in_power = yes
						salafist_caliphate_are_in_power = yes
						salafist_kingdom_are_in_power = yes
					}
				}
				random_list = {
					35 = {
						TAJ_check_radicalism = yes
					}
					65 = {
						# Nothing
					}
				}
				if = {
					limit = {
						NOT = {
							has_idea = TAJ_national_guard_deployed
						}
					}
					random_list = {
						25 = {
							add_to_variable = { var = TAJ_radical_influence value = 1 }
							clamp_variable = { var = TAJ_radical_influence min = 0 max = 100 }
						}
						25 = {
							add_to_variable = { var = TAJ_radical_influence value = 2 }
							clamp_variable = { var = TAJ_radical_influence min = 0 max = 100 }
						}
						50 = {
							# Nothing
						}
					}
				}
			}
			if = {
				limit = {
					has_idea = TAJ_isik
					fascism > 0.24
					has_stability < 0.40
					NOT = {
						has_country_flag = TAJ_ciilwar_ISIK
					}
				}
				country_event = {
					id = tajik_radicalism.16
				}
				set_country_flag = TAJ_ciilwar_ISIK
			}
			#Tajik Lore Events Triggers
			#Year 2000
			if = {
				limit = { check_variable = { global.year = 2000 } }
				#Month April
				if = {
					limit = { check_variable = { global.month = 4 } }
					if = {
						limit = { NOT = { has_country_flag = TAJ_tajik_lore.0 } }
						country_event = { id = tajik_lore.0 }
						set_country_flag = TAJ_tajik_lore.0
					}
					if = {
						limit = { NOT = { has_country_flag = TAJ_tajik_lore.1 } }
						#activate_decision = TAJ_Nadyrov_s_Funding_Proposal_mission
						set_country_flag = TAJ_tajik_lore.1
					}
				}
			}
			#Year 2001
			if = {
				limit = { check_variable = { global.year = 2001 } }
				#Month February
				if = {
					limit = { check_variable = { global.month = 2 } }
					if = {
						limit = { NOT = { has_country_flag = TAJ_tajik_lore.3 } }
						country_event = { id = tajik_lore.3 days = 4 random_hours = 8 }
						set_country_flag = TAJ_tajik_lore.3
					}
					if = {
						limit = { NOT = { has_country_flag = TAJ_tajik_lore.4 } }
						country_event = { id = tajik_lore.4 days = 9 random_hours = 8 }
						set_country_flag = TAJ_tajik_lore.4
					}
					if = {
						limit = { NOT = { has_country_flag = TAJ_tajik_lore.5 } }
						country_event = { id = tajik_lore.5 days = 18 random_hours = 5 }
						set_country_flag = TAJ_tajik_lore.5
					}
					if = {
						limit = { NOT = { has_country_flag = TAJ_tajik_lore.6 } }
						country_event = { id = tajik_lore.6 days = 24 random_hours = 10 }
						set_country_flag = TAJ_tajik_lore.6
					}
					if = {
						limit = { NOT = { has_country_flag = TAJ_tajik_lore.8 } }
						UZB = { country_event = { id = tajik_lore.8 days = 25 random_hours = 10 } }
						set_country_flag = TAJ_tajik_lore.8
					}
				}
			}
			#Year 2002 late
			if = {
				limit = { check_variable = { global.year = 2002 } }
				#Month May
				if = {
					limit = { check_variable = { global.month = 9 } }
					if = {
						limit = { NOT = { has_country_flag = TAJ_tajik_lore.9 } }
						UZB = { country_event = { id = tajik_lore.9 days = 0 random_hours = 0 } }
						set_country_flag = TAJ_tajik_lore.9
						set_country_flag = TAJ_tajik_lore.11
					}
				}
			}
		}
	}
}
