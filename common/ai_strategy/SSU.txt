#Libya befriended South Sudan
SSU_befriend_libya = {
	allowed = { original_tag = SSU }

	enable = {
		LBA = { has_completed_focus = LBA_support_south_sudan }
		NOT = { has_war_with = LBA }
	}

	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "LBA" value = 25 }
}