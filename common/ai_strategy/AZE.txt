hostile_armenia_relations = {
	allowed = { original_tag = AZE }
	enable = {
		original_tag = AZE
		OR = {
			country_exists = ARM
			country_exists = NKR
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = contain id = "ARM" value = 25 }
	ai_strategy = { type = antagonize id = "ARM" value = 25 }
	ai_strategy = { type = befriend id = "ARM" value = -25 }
	ai_strategy = { type = contain id = "NKR" value = 25 }
	ai_strategy = { type = antagonize id = "NKR" value = 25 }
	ai_strategy = { type = befriend id = "NKR" value = -25 }
}

AZE_no_suicide_iran = {
	allowed = { original_tag = AZE }
	enable = {
		original_tag = AZE
		has_wargoal_against = PER
		strength_ratio = {
			tag = PER
			ratio < 1
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = declare_war id = "PER" value = -100 }
	ai_strategy = { type = prepare_for_war id = "PER" value = 50 }
}

AZE_support_SAZ = {
	allowed = { original_tag = AZE }
	enable = { SAZ = { has_war_with = PER } }
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "PER" value = 20 }
	ai_strategy = { type = protect id = "PER" value = 20 }
	ai_strategy = { type = influence id = "PER" value = 20 }
	ai_strategy = { type = support id = "PER" value = 20 }
	ai_strategy = { type = send_volunteers_desire id = "SAZ" value = 200 }
	ai_strategy = { type = befriend id = "PER" value = -100 }
	ai_strategy = { type = conquer id = "PER" value = 50 }
	ai_strategy = { type = invade id = "PER" value = 50 }
	ai_strategy = { type = protect id = "PER" value = -100 }
	ai_strategy = { type = antagonize id = "PER" value = 100 }
	ai_strategy = { type = alliance id = "PER" value = -200 }
	ai_strategy = { type = support id = "PER" value = -100 }
	ai_strategy = { type = contain id = "PER" value = 100 }
}

AZE_attack_iran = {
	allowed = { original_tag = AZE }
	enable = {
		original_tag = AZE
		has_wargoal_against = PER
		strength_ratio = {
			tag = PER
			ratio > 0.99
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = declare_war id = "PER" value = 50 }
	ai_strategy = { type = conquer id = "PER" value = 25 }
}
