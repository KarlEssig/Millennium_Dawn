#AI strats to keep Arab liberation front together
MAU_anti_UAR_coalition = {
	allowed = {
		original_tag = MAU
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		MAU = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "MAU"
		value = 200
	}

}
MOR_anti_UAR_coalition = {
	allowed = {
		original_tag = MOR
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		MOR = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "MOR"
		value = 200
	}
	ai_strategy = {
		type = befriend
		id = "MOR"
		value = 200
	}

}
SHA_anti_UAR_coalition = {
	allowed = {
		original_tag = SHA
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		SHA = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "SHA"
		value = 200
	}

}
ALG_anti_UAR_coalition = {
	allowed = {
		original_tag = ALG
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		ALG = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "ALG"
		value = 200
	}

}
TUN_anti_UAR_coalition = {
	allowed = {
		original_tag = TUN
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		TUN = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "TUN"
		value = 200
	}

}
LBA_anti_UAR_coalition = {
	allowed = {
		original_tag = LBA
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		LBA = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "LBA"
		value = 200
	}

}
EGY_anti_UAR_coalition = {
	allowed = {
		original_tag = EGY
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		EGY = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "EGY"
		value = 200
	}

}
SUD_anti_UAR_coalition = {
	allowed = {
		original_tag = SUD
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		SUD = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "SUD"
		value = 200
	}

}
PAL_anti_UAR_coalition = {
	allowed = {
		original_tag = PAL
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		PAL = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "PAL"
		value = 200
	}

}
HAM_anti_UAR_coalition = {
	allowed = {
		original_tag = HAM
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		HAM = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "HAM"
		value = 200
	}

}
JOR_anti_UAR_coalition = {
	allowed = {
		original_tag = JOR
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		JOR = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "JOR"
		value = 200
	}

}
LEB_anti_UAR_coalition = {
	allowed = {
		original_tag = LEB
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		LEB = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "LEB"
		value = 200
	}

}
HEZ_anti_UAR_coalition = {
	allowed = {
		original_tag = HEZ
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		HEZ = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "HEZ"
		value = 200
	}

}
SYR_anti_UAR_coalition = {
	allowed = {
		original_tag = SYR
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		SYR = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "SYR"
		value = 200
	}

}
FSA_anti_UAR_coalition = {
	allowed = {
		original_tag = FSA
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		FSA = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "FSA"
		value = 200
	}

}
NUS_anti_UAR_coalition = {
	allowed = {
		original_tag = NUS
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		NUS = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "NUS"
		value = 200
	}

}
DRU_anti_UAR_coalition = {
	allowed = {
		original_tag = DRU
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		DRU = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "DRU"
		value = 200
	}

}
ALA_anti_UAR_coalition = {
	allowed = {
		original_tag = ALA
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		ALA = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "ALA"
		value = 200
	}

}
ISI_anti_UAR_coalition = {
	allowed = {
		original_tag = ISI
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		ISI = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "ISI"
		value = 200
	}

}
IRQ_anti_UAR_coalition = {
	allowed = {
		original_tag = IRQ
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		IRQ = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "IRQ"
		value = 200
	}

}
KUW_anti_UAR_coalition = {
	allowed = {
		original_tag = KUW
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		KUW = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "KUW"
		value = 200
	}

}
SAU_anti_UAR_coalition = {
	allowed = {
		original_tag = SAU
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		SAU = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "SAU"
		value = 200
	}

}
HEJ_anti_UAR_coalition = {
	allowed = {
		original_tag = HEJ
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		HEJ = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "HEJ"
		value = 200
	}

}
NEJ_anti_UAR_coalition = {
	allowed = {
		original_tag = NEJ
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		NEJ = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "NEJ"
		value = 200
	}

}
QTF_anti_UAR_coalition = {
	allowed = {
		original_tag = QTF
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		QTF = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "QTF"
		value = 200
	}

}
BHR_anti_UAR_coalition = {
	allowed = {
		original_tag = BHR
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		BHR = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "BHR"
		value = 200
	}

}
QAT_anti_UAR_coalition = {
	allowed = {
		original_tag = QAT
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		QAT = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "QAT"
		value = 200
	}

}
UAE_anti_UAR_coalition = {
	allowed = {
		original_tag = UAE
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		UAE = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "UAE"
		value = 200
	}

}
OMA_anti_UAR_coalition = {
	allowed = {
		original_tag = OMA
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		OMA = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "OMA"
		value = 200
	}

}
YEM_anti_UAR_coalition = {
	allowed = {
		original_tag = YEM
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		YEM = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "YEM"
		value = 200
	}

}
HOU_anti_UAR_coalition = {
	allowed = {
		original_tag = HOU
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		HOU = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "HOU"
		value = 200
	}

}
AQY_anti_UAR_coalition = {
	allowed = {
		original_tag = AQY
	}

	reversed = yes

	abort_when_not_enabled = yes

	enable = {
		has_country_flag = UAR_created_arab_liberation_front
	}

	enable_reverse = {
		AQY = { has_country_flag = UAR_joined_arab_liberation_front }
	}

	ai_strategy = {
		type = alliance
		id = "AQY"
		value = 200
	}

}