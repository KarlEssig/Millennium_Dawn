# Written by LordBogdanoff
#Volunteeer Army for Lugansk
LPR_army_focus = {
	allowed = { original_tag = LPR }
	enable = { original_tag = LPR }
	abort_when_not_enabled = yes

	ai_strategy = { type = role_ratio id = L_Inf value = 20 }
	ai_strategy = { type = role_ratio id = Militia value = 50 } #More Militia in Lugansk AI
	ai_strategy = { type = role_ratio id = infantry value = 5 }
	ai_strategy = { type = role_ratio id = garrison value = 10 }
	ai_strategy = { type = role_ratio id = marines value = -20 }
}
#Lugansk defend Kharkiv and Donetsk
LPR_defend_allies = {
	allowed = { original_tag = LPR }
	enable = { original_tag = LPR }
	abort_when_not_enabled = yes

	ai_strategy = {
		type = force_defend_ally_borders
		id = DPR
		value = 1000
	}
}
LPR_defend_the_capital = {
	allowed = { tag = LPR }
	enable = {
		has_war_with = UKR
		NOT = { is_subject_of = SOV }
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = put_unit_buffers
		ratio = 1.00
		states = {
			1075
		}
		subtract_invasions_from_need = no
		subtract_fronts_from_need = no
	}
}
#Volunteeer Army for Donestk
DPR_army_focus = {
	allowed = { original_tag = DPR }
	enable = { original_tag = DPR }
	abort_when_not_enabled = yes

	ai_strategy = { type = role_ratio id = L_Inf value = 20 }
	ai_strategy = { type = role_ratio id = Militia value = 50 } #More Militia in Donetsk AI
	ai_strategy = { type = role_ratio id = infantry value = 5 }
	ai_strategy = { type = role_ratio id = garrison value = 10 }
	ai_strategy = { type = role_ratio id = marines value = -20 }
	ai_strategy = { type = force_build_armies value = 150 } #Keep Donetsk AI building Units
}
#Donetsk defend Kharkiv and Lugansk
DPR_defend_allies = {
	allowed = { original_tag = DPR }
	enable = { original_tag = DPR }
	abort_when_not_enabled = yes

	ai_strategy = {
		type = force_defend_ally_borders
		id = LPR
		value = 1000
	}
}
DPR_defend_the_capital = {
	allowed = { tag = DPR }
	enable = {
		has_war_with = UKR
		NOT = { is_subject_of = SOV }
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = put_unit_buffers
		ratio = 1.00
		states = {
			693
		}
		subtract_invasions_from_need = no
		subtract_fronts_from_need = no
	}
}

#Volunteeer Army for Kharkiv
HPR_army_focus = {
	allowed = { original_tag = HPR }
	enable = { original_tag = HPR }
	abort_when_not_enabled = yes

	ai_strategy = { type = role_ratio id = L_Inf value = 20 }
	ai_strategy = { type = role_ratio id = Militia value = 50 } #More Militia in Kharkov AI
	ai_strategy = { type = role_ratio id = infantry value = 5 }
	ai_strategy = { type = role_ratio id = garrison value = 10 }
	ai_strategy = { type = role_ratio id = marines value = -20 }
	ai_strategy = { type = force_build_armies value = 150 } #Keep Kharkov AI building Units
}
DPR_influency_lpr = {
	allowed = { original_tag = DPR }
	enable = {
		country_exists = LPR
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = influence id = "LPR" value = 1000 }
}
LPR_influency_dpr = {
	allowed = { original_tag = LPR }
	enable = {
		country_exists = DPR
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = influence id = "DPR" value = 1000 }
}