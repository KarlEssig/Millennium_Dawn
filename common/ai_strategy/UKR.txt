# Written by Angriest Bird/Lord Bogdanoff
#BASED
anti_russia_defensives_stature = {
	allowed = { original_tag = UKR }
	enable = {
		UKR = {
			OR = {
				NOT = { 
					has_government = communism
					is_subject_of = SOV 
					is_in_faction_with = SOV
					AND = {
						has_country_flag = UKR_vitrenko_power
						nationalist_monarchists_are_in_power = yes
					}
				}
			}
		}
		country_exists = SOV
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = contain id = "SOV" value = 200 }
	ai_strategy = { type = befriend id = "GER" value = 150 }
	ai_strategy = { type = alliance id = "GER" value = 20 }
	ai_strategy = { type = befriend id = "ENG" value = 150 }
	ai_strategy = { type = alliance id = "ENG" value = 20 }
	ai_strategy = { type = befriend id = "FRA" value = 150 }
	ai_strategy = { type = alliance id = "FRA" value = 20 }
	ai_strategy = { type = befriend id = "ITA" value = 150 }
	ai_strategy = { type = alliance id = "ITA" value = 20 }
	ai_strategy = { type = befriend id = "SPR" value = 150 }
	ai_strategy = { type = alliance id = "SPR" value = 20 }
	ai_strategy = { type = befriend id = "POL" value = 150 }
	ai_strategy = { type = alliance id = "POL" value = 20 }
	ai_strategy = { type = befriend id = "ROM" value = 150 }
	ai_strategy = { type = alliance id = "ROM" value = 20 }
	ai_strategy = { type = befriend id = "USA" value = 200 }
	ai_strategy = { type = alliance id = "USA" value = 50 }
}
UKR_army_breakdown = {
	allowed = { original_tag = UKR }
	enable = { original_tag = UKR }
	abort_when_not_enabled = yes

	ai_strategy = { type = role_ratio id = mechanized value = 38 }
	ai_strategy = { type = role_ratio id = armor value = 38 }
	ai_strategy = { type = role_ratio id = marines value = 20 }
	ai_strategy = { type = role_ratio id = Air_helicopters value = 5 }
	ai_strategy = { type = role_ratio id = Air_mech value = 3 }
	ai_strategy = { type = role_ratio id = Special_Forces value = 11 }
	ai_strategy = { type = force_build_armies value = 50 }
}

#POLICY OF ECONOMY
UKR_factory_target = {
	allowed = { tag = UKR }
	enable = {
		NOT = { nationalist_fascist_are_in_power = yes }
	}
	abort_when_not_enabled = yes

	ai_strategy = {
		type = building_target
		id = industrial_complex
		value = 180
	}
}
UKR_arms_target = {
	allowed = { tag = UKR }
	enable = {
		nationalist_fascist_are_in_power = yes
	}
	abort_when_not_enabled = yes

	ai_strategy = {
		type = building_target
		id = arms_factory
		value = 180
	}
}
#POLICY OF FRONT
UKR_ignore_the_pol_in_war = {
	allowed = { original_tag = UKR }
	enable = {
		has_war_with = SOV
		NOT = { has_war_with = POL }
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = front_unit_request
		tag = POL
		value = -100
	}
}
UKR_ignore_the_rom_in_war = {
	allowed = { original_tag = UKR }
	enable = {
		has_war_with = SOV
		NOT = { has_war_with = ROM }
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = front_unit_request
		tag = ROM
		value = -100
	}
}
UKR_ignore_the_sov_in_war = {
	allowed = { original_tag = UKR }
	enable = {
		has_war_with = POL
		NOT = { has_war_with = SOV }
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = front_unit_request
		tag = SOV
		value = -100
	}
}
UKR_SOV_owns_crimea = {
	allowed = { original_tag = UKR }
	enable = {
		NOT = {
			OR = {
				UKR = {	is_subject_of = SOV	}
				is_in_faction_with = SOV
			}
		}
		SOV = {
			OR = {
				owns_state = 669
				controls_state = 669
			}
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = front_unit_request
		tag = SOV
		value = 200
	}
	ai_strategy = { type = force_build_armies value = 150 }
}
UKR_BLR_with_russia = {
	allowed = { original_tag = UKR }
	enable = {
		NOT = {
			OR = {
				UKR = {	is_subject_of = SOV	}
				is_in_faction_with = SOV
			}
		}
		OR = {
			BLR = {	is_subject_of = SOV	}
			BLR = { is_in_faction_with = SOV } 
		}
		
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = front_unit_request
		tag = BLR
		value = 100
	}
	ai_strategy = { type = force_build_armies value = 50 }
}
#POLICY OF START WAR
UKR_dont_declare_on_russia = {
	allowed = { original_tag = UKR }
	enable = {
		original_tag = UKR
		has_wargoal_against = SOV
		strength_ratio = {
			tag = SOV
			ratio < 1.0
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = conquer id = "SOV" value = -100 }
	ai_strategy = { type = declare_war id = "SOV" value = -100 }
}
UKR_dont_declare_on_poland = {
	allowed = { original_tag = UKR }
	enable = {
		original_tag = UKR
		has_wargoal_against = POL
		strength_ratio = {
			tag = POL
			ratio < 1.0
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = conquer id = "POL" value = -100 }
	ai_strategy = { type = declare_war id = "POL" value = -100 }
}
#POLICY OF SEPARATISTS REPUBLIC
UKR_novorossiyan_relations = {
	allowed = { original_tag = UKR }
	enable = {
		OR = {
			country_exists = DPR
			country_exists = OPR
			country_exists = LPR
			country_exists = HPR
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "DPR" value = 200 }
	ai_strategy = { type = conquer id = "DPR" value = 100 }
	ai_strategy = { type = contain id = "DPR" value = 100 }
	ai_strategy = { type = antagonize id = "DPR" value = 50 }

	ai_strategy = { type = antagonize id = "LPR" value = 200 }
	ai_strategy = { type = conquer id = "LPR" value = 100 }
	ai_strategy = { type = contain id = "LPR" value = 100 }
	ai_strategy = { type = antagonize id = "LPR" value = 50 }

	ai_strategy = { type = antagonize id = "HPR" value = 200 }
	ai_strategy = { type = conquer id = "HPR" value = 100 }
	ai_strategy = { type = contain id = "HPR" value = 100 }
	ai_strategy = { type = antagonize id = "HPR" value = 50 }

	ai_strategy = { type = antagonize id = "OPR" value = 200 }
	ai_strategy = { type = conquer id = "OPR" value = 100 }
	ai_strategy = { type = contain id = "OPR" value = 100 }
	ai_strategy = { type = antagonize id = "OPR" value = 50 }
}
UKR_galicia_relations = {
	allowed = { original_tag = UKR }
	enable = {
		OR = {
			country_exists = VRP
			country_exists = LRP
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "VRP" value = 200 }
	ai_strategy = { type = conquer id = "VRP" value = 100 }
	ai_strategy = { type = contain id = "VRP" value = 100 }
	ai_strategy = { type = antagonize id = "VRP" value = 50 }

	ai_strategy = { type = antagonize id = "LRP" value = 200 }
	ai_strategy = { type = conquer id = "LRP" value = 100 }
	ai_strategy = { type = contain id = "LRP" value = 100 }
	ai_strategy = { type = antagonize id = "LRP" value = 50 }
}
#FOREIGN POLICY
UKR_support_pmr_peackeepers = {
	allowed = { original_tag = UKR }
	enable = {
		PMR = {
			has_idea = PMR_ukr_army
			has_war = yes
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = send_volunteers_desire
		id = PMR
		value = 1500
	}
}
#Anti Russia and Anti Serbia
UKR_help_croatia_against_ser = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		nationalist_fascist_are_in_power = yes
		CRO = {
			has_war_with = SER
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "CRO" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "CRO" value = 500 }
}
UKR_help_bosnia_against_ser = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		nationalist_fascist_are_in_power = yes
		BOS = {
			has_war_with = SER
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "BOS" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "BOS" value = 500 }
}
UKR_help_kosovo_against_ser = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		nationalist_fascist_are_in_power = yes
		KOS = {
			has_war_with = SER
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "KOS" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "KOS" value = 500 }
}
UKR_help_fyr_against_ser = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		nationalist_fascist_are_in_power = yes
		FYR = {
			has_war_with = SER
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "FYR" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "FYR" value = 500 }
}
UKR_help_pmr_against_mlv = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		nationalist_fascist_are_in_power = yes
		PMR = {
			has_war_with = MLV
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "PMR" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "PMR" value = 500 }
}
UKR_help_azeri_against_russia = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			nationalist_fascist_are_in_power = yes
			western_liberals_are_in_power = yes
		}
		AZE = {
			has_war_with = SOV
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "AZE" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "AZE" value = 500 }
}
UKR_help_azeri_against_armenia = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			nationalist_fascist_are_in_power = yes
			western_liberals_are_in_power = yes
		}
		AZE = {
			has_war_with = ARM
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "AZE" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "AZE" value = 500 }
}
UKR_help_geo_against_russia = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			nationalist_fascist_are_in_power = yes
			western_liberals_are_in_power = yes
		}
		GEO = {
			has_war_with = SOV
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "GEO" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "GEO" value = 500 }
}
UKR_help_geo_against_abkhazia = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			nationalist_fascist_are_in_power = yes
			western_liberals_are_in_power = yes
		}
		GEO = {
			has_war_with = ABK
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "GEO" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "GEO" value = 500 }
}
UKR_help_che_against_russia = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			nationalist_fascist_are_in_power = yes
			western_liberals_are_in_power = yes
		}
		CHE = {
			has_war_with = SOV
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "CHE" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "CHE" value = 500 }
}
#Commies Policy
UKR_help_commi_serbia = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		emerging_communist_state_are_in_power = yes
		SER = {
			has_war = yes
			emerging_communist_state_are_in_power = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "SER" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "SER" value = 500 }
}
UKR_help_commi_russia = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		emerging_communist_state_are_in_power = yes
		SOV = {
			has_war = yes
			emerging_communist_state_are_in_power = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "SOV" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "SOV" value = 500 }
}
UKR_help_commi_belarus = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		emerging_communist_state_are_in_power = yes
		BLR = {
			has_war = yes
			emerging_communist_state_are_in_power = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "BLR" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "BLR" value = 500 }
}
UKR_help_commi_trans = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		emerging_communist_state_are_in_power = yes
		PMR = {
			has_war = yes
			emerging_communist_state_are_in_power = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "PMR" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "PMR" value = 500 }
}
UKR_help_commi_cuba = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		emerging_communist_state_are_in_power = yes
		CUB = {
			has_war = yes
			emerging_communist_state_are_in_power = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "CUB" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "CUB" value = 500 }
}
UKR_help_commi_cuba = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		emerging_communist_state_are_in_power = yes
		CUB = {
			has_war = yes
			emerging_communist_state_are_in_power = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "CUB" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "CUB" value = 500 }
}
UKR_help_commi_nko = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		emerging_communist_state_are_in_power = yes
		NKO = {
			has_war = yes
			emerging_communist_state_are_in_power = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "NKO" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "NKO" value = 500 }
}
#Vitrenko Policy
UKR_help_iraq = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			AND = {
				has_country_flag = UKR_vitrenko_power
				nationalist_monarchists_are_in_power = yes
			}
			emerging_autocracy_are_in_power = yes
		}
		IRQ = {
			has_war = yes
			is_subject = no
			has_government = nationalist
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "IRQ" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "IRQ" value = 500 }
}
UKR_help_hezbolla = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			AND = {
				has_country_flag = UKR_vitrenko_power
				nationalist_monarchists_are_in_power = yes
			}
			emerging_autocracy_are_in_power = yes
		}
		HEZ = {
			has_war = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "HEZ" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "HEZ" value = 500 }
}
UKR_help_serbia = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			AND = {
				has_country_flag = UKR_vitrenko_power
				nationalist_monarchists_are_in_power = yes
			}
			emerging_autocracy_are_in_power = yes
		}
		SER = {
			has_war = yes
			NOT = { has_government = democratic }
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "SER" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "SER" value = 500 }
}
UKR_help_syria = {
	allowed = {
		original_tag = UKR
	}
	enable = {
		OR = {
			AND = {
				has_country_flag = UKR_vitrenko_power
				nationalist_monarchists_are_in_power = yes
			}
			emerging_autocracy_are_in_power = yes
		}
		SYR = {
			has_war = yes
			NOT = { has_government = democratic }
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "SYR" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "SYR" value = 500 }
}
#CIVIL WAR IN AMERICA
UKR_confederate_states_america_help = {
	allowed = { original_tag = UKR }
	enable = {
		original_tag = SOV
		country_exists = UKR
		UKR = {
			has_war = no
			emerging_communist_state_are_in_power = yes
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "CSA" value = 1000 }
	ai_strategy = { type = send_lend_lease_desire id = "CSA" value = 400 }
}
UKR_free_states_america_help = {
	allowed = { original_tag = UKR }
	enable = {
		original_tag = UKR
		country_exists = USB
		UKR = {
			has_war = no
			has_government = democratic
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "USB" value = 1000 }
	ai_strategy = { type = send_lend_lease_desire id = "USB" value = 400 }
}