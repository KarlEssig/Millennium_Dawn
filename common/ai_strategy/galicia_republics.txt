# Written by LordBogdanoff
LRP_anti_russia = {
	allowed = { original_tag = UKR }
	enable = {
		LRP = {
			NOT = {
				is_in_faction_with = SOV
			}
		}
		country_exists = SOV
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = contain id = "SOV" value = 500 }
}
LRP_volhynia_relations = {
	allowed = { original_tag = LRP }
	enable = {
		country_exists = VRP
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "VRP" value = 150 }
	ai_strategy = { type = support id = "VRP" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "VRP" value = 300 }
}
#People Army for Lviv
LPR_army_focus = {
	allowed = { original_tag = LRP }
	enable = { original_tag = LRP }
	abort_when_not_enabled = yes

	ai_strategy = { type = role_ratio id = L_Inf value = 20 }
	ai_strategy = { type = role_ratio id = Militia value = 50 }
	ai_strategy = { type = role_ratio id = infantry value = 5 }
	ai_strategy = { type = role_ratio id = garrison value = 10 }
	ai_strategy = { type = role_ratio id = mechanized value = 0 }
	ai_strategy = { type = role_ratio id = armor value = 0 }
	ai_strategy = { type = role_ratio id = marines value = -20 }
	ai_strategy = { type = role_ratio id = apc_mechanized value = 0 }
	ai_strategy = { type = role_ratio id = ifv_mechanized value = 0 }
	ai_strategy = { type = role_ratio id = Air_mech value = 0 }
	ai_strategy = { type = role_ratio id = Special_Forces value = 0 }
}
LRP_defend_the_capital = {
	allowed = { tag = LPR }
	enable = {
		has_war_with = UKR
		NOT = { is_subject_of = POL }
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = put_unit_buffers
		ratio = 1.00
		states = {
			700
		}
		subtract_invasions_from_need = no
		subtract_fronts_from_need = no
	}
}
#People Army for Volhynia
VPR_army_focus = {
	allowed = { original_tag = VRP }
	enable = { original_tag = VRP }
	abort_when_not_enabled = yes

	ai_strategy = { type = role_ratio id = L_Inf value = 20 }
	ai_strategy = { type = role_ratio id = Militia value = 50 }
	ai_strategy = { type = role_ratio id = infantry value = 5 }
	ai_strategy = { type = role_ratio id = garrison value = 10 }
	ai_strategy = { type = role_ratio id = mechanized value = 0 }
	ai_strategy = { type = role_ratio id = armor value = 0 }
	ai_strategy = { type = role_ratio id = marines value = -20 }
	ai_strategy = { type = role_ratio id = apc_mechanized value = 0 }
	ai_strategy = { type = role_ratio id = ifv_mechanized value = 0 }
	ai_strategy = { type = role_ratio id = Air_mech value = 0 }
	ai_strategy = { type = role_ratio id = Special_Forces value = 0 }
}
VRP_volhynia_relations = {
	allowed = { original_tag = VRP }
	enable = {
		country_exists = LRP
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "LRP" value = 150 }
	ai_strategy = { type = support id = "LRP" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "LRP" value = 300 }
}
