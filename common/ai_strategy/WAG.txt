# Written by LordBogdanoff
#Wagner support Russia
WAG_support_russia = {
	allowed = {
		original_tag = WAG
	}
	reversed = yes
	enable = {
		NOT = { has_war_with = SOV }
	}
	
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "SOV" value = 200 }
	ai_strategy = { type = protect id = "SOV" value = 200 }
	ai_strategy = { type = support id = "SOV" value = 200 }
	ai_strategy = { type = send_volunteers_desire id = "SOV" value = 1200 }
	ai_strategy = { type = influence id = "SOV" value = 200 }

}
WAG_BUNT_voronesh = {
	allowed = { tag = WAG }
	enable = {
		WAG = {
			has_war_with = SOV
		}
	}
	abort_when_not_enabled = yes
	
	ai_strategy = { type = front_unit_request tag = SOV value = 1000 }
}
#Wagner support With Instructors
WAG_peacekeepers_in_dpr = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		DPR = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "DPR" value = 1000 }
}
WAG_peacekeepers_in_lpr = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		LPR = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "LPR" value = 1000 }
}
WAG_peacekeepers_in_hpr = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		HPR = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "HPR" value = 1000 }
}
WAG_peacekeepers_in_opr = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		OPR = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "OPR" value = 1000 }
}
WAG_peacekeepers_in_syria = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		SYR = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "SYR" value = 1000 }
}
WAG_peacekeepers_in_lybia = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		LBA = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "LBA" value = 1000 }
}
WAG_peacekeepers_in_sudan = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		SUD = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "SUD" value = 1000 }
}
WAG_peacekeepers_in_car = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		CAR = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "CAR" value = 1000 }
}
WAG_peacekeepers_in_drc = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		DRC = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "DRC" value = 1000 }
}
WAG_peacekeepers_in_niger = {
	allowed = { original_tag = WAG }
	enable = {
		original_tag = WAG
		WAG = {
			has_war = no
		}
		NGR = {
			has_idea = SOV_wagner_military_advisors
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "NGR" value = 1000 }
}