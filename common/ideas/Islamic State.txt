ideas = {
	country = {
		ISI_jihadist_islamic_state = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISI_jihadist_islamic_state" }
			picture = nationalism

			modifier = {
				war_stability_factor = 0.25
				army_core_attack_factor = 0.10
				army_core_defence_factor = 0.10
				equipment_capture = 0.10
				fascism_drift = 0.50
				fascism_acceptance = 100
			}
		}
	}
}