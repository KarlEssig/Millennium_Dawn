ideas = {

	materiel_manufacturer = {

		designer = yes

		CHL_famae_materiel_manufacturer = {
			allowed = { original_tag = CHL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHL_famae_materiel_manufacturer" }

			picture = FAMAE

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.124
			}

			traits = {
				CAT_inf_wep_4

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		CHL_famae_tank_manufacturer = {
			allowed = { original_tag = CHL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHL_famae_tank_manufacturer" }

			picture = FAMAE

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.124
			}

			traits = {
				CAT_afv_4

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		CHL_enaer_aircraft_manufacturer = {
			allowed = { original_tag = CHL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHL_enaer_aircraft_manufacturer" }

			picture = ENAER
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_l_fighter = 0.155
			}

			traits = {
				CAT_l_fighter_5

			}
			ai_will_do = {
				factor = 1
			}

		}
	}

	naval_manufacturer = {

		designer = yes

		CHL_asmar_naval_manufacturer = {
			allowed = { original_tag = CHL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHL_asmar_naval_manufacturer" }

			picture = ASMAR
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_naval_eqp = 0.124
			}

			traits = {
				CAT_naval_eqp_4
			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}