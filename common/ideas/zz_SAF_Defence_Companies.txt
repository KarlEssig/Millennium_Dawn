ideas = {
	materiel_manufacturer = {
		designer = yes
		SAF_denel_land_systems_materiel_manufacturer = {
			allowed = { original_tag = SAF }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAF_denel_land_systems_materiel_manufacturer" }

			picture = Denel_Dynamics
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.155
			}

			traits = {
				CAT_inf_wep_5

			}
			ai_will_do = {
				factor = 1
			}
		}
		SAF_denel_dynamics_materiel_manufacturer = {
			allowed = { original_tag = SAF }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAF_denel_dynamics_materiel_manufacturer" }

			picture = Denel_Dynamics
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf = 0.155
			}

			traits = {
				CAT_inf_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
	tank_manufacturer = {

		designer = yes

		SAF_denel_land_systems_tank_manufacturer = {
			allowed = { original_tag = SAF }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAF_denel_land_systems_tank_manufacturer" }

			picture = Denel_Land_Systems

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_artillery = 0.155
			}

			traits = {
				CAT_artillery_5

			}
			ai_will_do = {
				factor = 1
			}

		}
		SAF_land_systems_omc_tank_manufacturer = {
			allowed = { original_tag = SAF }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAF_land_systems_omc_tank_manufacturer" }

			picture = Denel_Land_Systems
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.248
			}

			traits = {
				CAT_afv_8

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}