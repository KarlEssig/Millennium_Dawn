ideas = {
	country = {

		#North Atlanic Treaty Organization
		NATO_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea NATO_member" }

			picture = NATO_member

			allowed_civil_war = {
				has_government = democratic
			}
			cancel = {
				OR = {
					is_in_faction = no
					is_subject = yes # This is to have new puppets not be subjects
				}
			}

			modifier = {
				custom_modifier_tooltip = NATO_member_tooltip
			}
		}

		#Resistance Axis
		faction_resistance_axis_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_resistance_axis_member" }

			picture = faction_resistance_axis_member

			allowed_civil_war = {
				has_idea = shia
			}

			cancel = {
				is_in_faction = no
			}

			modifier = {
				send_volunteer_size = 2
				send_volunteer_divisions_required = -0.20
				send_volunteers_tension = -0.05
				custom_modifier_tooltip = faction_resistance_axis_member_TT
			}
		}

		#CSTO
		CSTO_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CSTO_member" }

			picture = CSTO_member

			allowed_civil_war = {
				has_government = communism
			}

			cancel = {
				OR = {
					is_in_faction = no
					NOT = {
						is_in_faction_with = SOV
					}
					SOV = {
						is_subject = yes
					}
				}
			}

			modifier = {
				custom_modifier_tooltip = CSTO_member_tooltip
			}
		}

		#5th International
		faction_fifth_international_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_fifth_international_member" }

			picture = faction_fifth_international_member

			allowed_civil_war = {
				OR = {
					is_in_array = { ruling_party = 4 }
					is_in_array = { ruling_party = 19 }
				}
			}

			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = faction_fifth_international_member_TT
				send_volunteer_size = 4
				send_volunteer_divisions_required = -0.20
				send_volunteers_tension = -0.1
			}

		}

		#Baltic-Black Sea Union
		faction_baltic_black_sea_union_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_fifth_international_member" }

			picture = faction_baltic_black_sea_union_member

			allowed_civil_war = {
			}

			cancel = {
				OR = {
					is_in_faction = no
					NOT = {
						is_in_faction_with = UKR
					}
					UKR = {
						is_subject = yes
					}
				}
			}

			modifier = {
				custom_modifier_tooltip = faction_baltic_black_sea_union_member_TT
			}

		}

		#Jerusalem Defence Pact
		faction_jerusalem_defence_pact_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_jerusalem_defence_pact_member" }

			picture = faction_jerusalem_defence_pact_member

			allowed_civil_war = {
			}

			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = faction_jerusalem_defence_pact_member_TT
			}

		}

		#GCC Member
		idea_gcc_member_state = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea idea_gcc_member_state"
				add_to_array = { global.gcc_member_state = THIS }
			}
			on_remove = {
				log = "[GetDateText]: [Root.GetName]: remove idea idea_gcc_member_state"
				remove_from_array = { global.gcc_member_state = THIS }
			}

			picture = gcc_member

			allowed_civil_war = { no_jihadist_government = yes }
			cancel = { jihadist_government = yes }

			modifier = {
				political_power_factor = 0.10
				trade_opinion_factor = 0.10
				local_resources_factor = 0.10
				ai_get_ally_desire_factor = -1000
				custom_modifier_tooltip = idea_gcc_member_state_TT
			}
		}

		#British Commonwealth Alliance
		faction_british_commonwealth = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_british_commonwealth" }
			picture = faction_british_commonwealth

			allowed = {
				tag = ENG
			}

			modifier = {
				custom_modifier_tooltip = faction_british_commonwealth_TT
			}
		}

		#Monroe Alliance
		faction_monroe_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_monroe_alliance" }
			picture = faction_monroe_alliance

			allowed = {
				tag = USA
			}

			modifier = {
				custom_modifier_tooltip = faction_monroe_alliance_TT
			}
		}

		#Chinese United Front
		faction_chinese_united_front = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_chinese_united_front" }
			picture = faction_chinese_united_front

			allowed = {
				tag = CHI
			}

			modifier = {
				custom_modifier_tooltip = faction_chinese_united_front_TT
			}
		}

		#Shanghai Cooperation Organization
		sco_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sco_member" }

			modifier = {
				political_power_gain = 0.2
				trade_opinion_factor = 0.2
				custom_modifier_tooltip = sco_member_tooltip
			}
		}

		sco_observer = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sco_observer" }

			picture = sco_member

			modifier = {
				political_power_gain = 0.1
				trade_opinion_factor = 0.1
			}
		}

		sco_member_econ = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sco_member_econ" }

			picture = sco_member

			name = sco_member

			modifier = {
				political_power_gain = 0.2
				trade_opinion_factor = 0.2
				production_speed_buildings_factor = 0.1
				research_speed_factor = 0.1
				generate_wargoal_tension = 0.05
				receiving_investment_duration_modifier = -0.075
				receiving_investment_cost_modifier = -0.075
				custom_modifier_tooltip = sco_member_tooltip
			}
		}

		sco_member_pol = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sco_member_pol" }

			picture = sco_member

			name = sco_member

			modifier = {
				political_power_gain = 0.2
				trade_opinion_factor = 0.2
				political_power_factor = 0.1
				corruption_cost_factor = -0.1
				foreign_subversive_activites = -0.1
				communism_drift = 0.02
				custom_modifier_tooltip = sco_member_tooltip
			}
		}

		sco_member_mil = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sco_mil" }

			picture = sco_member

			name = sco_member

			modifier = {
				political_power_gain = 0.2
				trade_opinion_factor = 0.2
				experience_gain_army = 0.05
				max_planning = 0.1
				command_power_gain = 0.1
				max_command_power = 10
				stability_factor = -0.02
				custom_modifier_tooltip = sco_member_tooltip
			}
		}

		#CENTO Member
		faction_cento_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_cento_alliance" }
			picture = CENTO_faction

			allowed = {
				tag = PER
			}
			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = faction_cento_TT
			}
		}

		#IMPERIAL BLOC MEMBER
		faction_imperial_bloc_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_imperial_bloc_alliance" }
			picture = triumphant_will

			allowed = {
				tag = GER
			}
			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = faction_imperial_bloc_TT
			}
		}

		#Periphery Member
		faction_periphery_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_periphery_alliance" }
			picture = isral_faction

			allowed = {
				tag = ISR
			}
			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = faction_periph_TT
			}
		}

		#BRICS Member
		faction_brics_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_brics_alliance" }
			picture = bricks

			allowed = {
				tag = RAJ
			}
			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = faction_brics_TT
			}
		}

		#MTO Member
		faction_mto_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_mto_alliance" }
			picture = mto_idea
		}

		#TAJ Member
		faction_taj_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_taj_alliance" }
			picture = taj_idea
		}

		#FAO Member
		faction_fao_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_fao_alliance" }
			picture = mto_idea
		}

		#Warsaw Pact Member
		faction_warsaw_pact_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_warsaw_pact_member" }

			picture = faction_fifth_international_member

			allowed_civil_war = {
				OR = {
					is_in_array = { ruling_party = 4 }
					is_in_array = { ruling_party = 19 }
				}
			}

			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = faction_warsaw_pact_member
				send_volunteer_size = 4
				send_volunteer_divisions_required = -0.10
				send_volunteers_tension = -0.15
			}

		}

		#GCA Member
		faction_gca_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_gca_alliance" }
			picture = GCA_faction

			allowed = {
				tag = VEN
			}
			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = faction_gca_TT
			}
		}

		#FIA Member
		faction_fia_alliance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea faction_fia_alliance" }
			picture = FIA_faction

			cancel = {
				is_in_faction = no
			}

			modifier = {
				custom_modifier_tooltip = fia_member_tooltip
			}
		}

		#Red Europe Pact Member
		REP_member = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea REP_member" }

			picture = communism5 # TODO: Add a faction icon

			allowed = {
				has_government = communism
			}
			cancel = {
				OR = {
					is_in_faction = no
					NOT = {
						is_in_faction_with = POL
					}
					POL = {
						is_subject = yes
					}
				}
			}

			modifier = {
				political_power_gain = 0.15
				communism_drift = 0.5
				hidden_modifier = {
					ai_badass_factor = 0.35
					ai_focus_peaceful_factor = -0.15
					ai_get_ally_desire_factor = 1
				}
				faction_trade_opinion_factor = 0.15
				enemy_justify_war_goal_time = 0.15
			}

			rule = {
				can_boost_other_ideologies = no
			}
		}
	}
}
