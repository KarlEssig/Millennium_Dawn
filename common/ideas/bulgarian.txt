ideas = {
	country = {
		BUL_high_emigration_1 = {
			allowed = {
				original_tag = BUL
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			picture = bul_third_bulgarian_state
			modifier = {
				monthly_population = -0.5
				research_speed_factor = -0.07
			}
		}
		BUL_high_emigration_2 = {
			allowed = {
				original_tag = BUL
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			picture = bul_third_bulgarian_state
			modifier = {
				monthly_population = -1.1
				research_speed_factor = -0.12
			}
		}
		BUL_improved_administration = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_improved_administration" }
			allowed = { always = no }
			picture = political_power_bonus

			modifier = {
				political_power_factor = 0.2
			}
		}
		BUL_police_chaos_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_police_chaos_idea" }
			allowed = {
				tag = BUL
			}
			picture = bul_nationalized_industry
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.10
				civilian_intel_to_others = 1
			}
		}
		BUL_political_opposition = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_political_opposition" }
			allowed = {
				tag = BUL
			}
			picture = bulgarian_national_catastrophe
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.10
				nationalist_drift = 0.02
				communism_drift = 0.02
				drift_defence_factor = -0.05
			}
		}
		BUL_political_opposition2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_political_opposition3" }
			allowed = {
				tag = BUL
			}
			picture = bulgarian_national_catastrophe
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.10
				democratic_drift = 0.02
				communism_drift = 0.02
				drift_defence_factor = -0.05
			}
		}
		BUL_political_opposition3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_political_opposition3" }
			allowed = {
				tag = BUL
			}
			picture = bulgarian_national_catastrophe
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.10
				democratic_drift = 0.02
				nationalist_drift = 0.02
				drift_defence_factor = -0.05
			}
		}
		BUL_lack_of_gas = {
			allowed = {
				original_tag = BUL
				always = no
			}
			removal_cost = -1
			picture = poor_economy
			modifier = {
				consumer_goods_factor = 0.05
			}
		}
		BUL_bulgarian_legacy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_bulgarian_legacy " }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = bulgarian_irredentism
			modifier = {
				army_core_defence_factor = -0.1
				army_core_attack_factor = 0.05
				conscription = 0.005
			}
		}
		BUL_simeon_prime_minister = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_simeon_prime_minister" }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = idea_bul_ratniks
			modifier = {
				stability_factor = 0.10
				democratic_drift = 0.02
				drift_defence_factor = 0.05
			}
		}
		BUL_prorussian_prime_minister = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_prorussian_prime_minister" }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = idea_bul_ratniks
			modifier = {
				stability_factor = 0.05
				democratic_drift = 0.10
				drift_defence_factor = 0.05
			}
		}
		BUL_FYR_friendship = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_FYR_friendship" }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = idea_volunteer_expedition_bonus
			modifier = {
				stability_factor = 0.05
			}
		}
		BUL_banking_rost = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_banking_rost" }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = financial_crisis
			modifier = {
				stability_factor = 0.3
				research_speed_factor = 0.03
			}
		}
		BUL_banking_krakh = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_banking_krakh" }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = financial_crisis
			modifier = {
				stability_factor = -0.1
				research_speed_factor = -0.05
				production_speed_rail_way_factor = -0.4
				production_speed_arms_factory_factor = -0.3
				production_speed_buildings_factor = -0.4
			}
		}
		BUL_unsatisf_rurk_minority = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_banking_krakh" }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = kemalist_army_officers_powerful_disloyal
			modifier = {
				stability_factor = -0.1
			}
		}
		BUL_tourism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_tourism" }
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			picture = foreign_capital
			modifier = {
				custom_modifier_tooltip = BUL_tourism_tt
			}
		}
		BUL_chi_industry = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_chi_industry" }
			picture = bul_arsenal
			modifier = {
				production_factory_start_efficiency_factor = 0.05
				production_speed_buildings_factor = 0.05
			}
		}
		BUL_chi_industry2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_chi_industry2" }
			picture = bul_arsenal
			modifier = {
				production_factory_start_efficiency_factor = 0.05
				production_speed_buildings_factor = 0.05
				industrial_capacity_factory = 0.05
				production_speed_industrial_complex_factor = 0.05
			}
		}
		BUL_energy1 = {
			name = BUL_energy_idea
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_energy1" }
			picture = fusion_energy
			modifier = {
				fossil_pp_fuel_consumption_modifier = -0.05
				fossil_pp_energy_generation_modifier = 0.05
				renewable_energy_gain_multiplier = 0.05
			}
		}
		BUL_energy2 = {
			name = BUL_energy_idea
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_energy2" }
			picture = fusion_energy
			modifier = {
				fossil_pp_fuel_consumption_modifier = -0.10
				fossil_pp_energy_generation_modifier = 0.10
				renewable_energy_gain_multiplier = 0.10
			}
		}
		BUL_energy3 = {
			name = BUL_energy_idea
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_energy3" }
			picture = fusion_energy
			modifier = {
				fossil_pp_fuel_consumption_modifier = -0.15
				fossil_pp_energy_generation_modifier = 0.15
				renewable_energy_gain_multiplier = 0.15
			}
		}

		BUL_debt_refactoring_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_debt_refactoring_idea" }
			picture = fusion_energy
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				interest_rate_multiplier_modifier = -2
				tax_gain_multiplier_modifier = 0.05
			}
		}

		BUL_trade_cooperation_idea = {

			allowed = {
				original_tag = BUL
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = positive_gold
			modifier = {
				trade_opinion_factor = 0.25
				license_purchase_cost = -0.30
				improve_relations_maintain_cost_factor = -0.3
				opinion_gain_monthly_factor = 0.1
				monthly_population = 0.05
			}
		}

		BUL_sports_develop1 = {

			allowed = {
				original_tag = BUL
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = international_treaty2
			modifier = {
				stability_factor = 0.01
				monthly_population = 0.05
			}
		}

		BUL_sports_develop2 = {

			allowed = {
				original_tag = BUL
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = international_treaty2
			modifier = {
				stability_factor = 0.02
				monthly_population = 0.1
			}
		}

		BUL_idea_agrarian_reform = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_agrarian_reform" }

			picture = communism4

			modifier = {
				MONTHLY_POPULATION = 0.05
				political_power_gain = 0.05
				stability_factor = 0.05
			}
		}

		BUL_decommodify_the_basics_idea = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BUL_decommodify_the_basics_idea"
				ingame_update_setup = yes
			}
			on_remove = {
				ingame_update_setup = yes
			}
			allowed = { always = no }
			picture = economic_increase

			modifier = {
				social_cost_multiplier_modifier = 0.15
				political_power_factor = 0.10
				consumer_goods_factor = -0.05
			}
		}

		BUL_black_market_controlled = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_black_market_controlled" }
			allowed = {
				original_tag = BUL
				always = no
			}

			removal_cost = -1
			picture = new_deal

			modifier = {
				tax_gain_multiplier_modifier = 0.05
				stability_factor = -0.05
				production_speed_buildings_factor = -0.02
			}
		}

		BUL_idea_rainforest_protectors = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_rainforest_protectors" }

			picture = national_defenders

			modifier = {
				conscription = 0.005
				army_org = 0.5
				experience_gain_army = 0.05
			}
		}

		BUL_new_farms_market = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_new_farms_market" }
			allowed = { always = no }
			picture = agricultural_reforms

			modifier = {
				min_export = 0.05
				local_resources_factor = 0.1
			}
		}

		BUL_trade_isolationism_idea = {
			allowed = {
				original_tag = BUL
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = local_self_management
			modifier = {
				opinion_gain_monthly_factor = -0.5
				improve_relations_maintain_cost_factor = 0.3
				trade_opinion_factor = -0.5
				production_factory_max_efficiency_factor = 0.05
				global_building_slots_factor = 0.05
				production_speed_buildings_factor = 0.1

			}
		}

		BUL_liberal_economy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea  BUL_liberal_economy " }

			allowed = {
				always = no
			}

			picture = construction

			modifier = {
				production_speed_buildings_factor = 0.10
				line_change_production_efficiency_factor = 0.05
				return_on_investment_modifier = 0.02
			}
		}

		BUL_develop_public_sector = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea  BUL_develop_public_sector " }
			allowed = { always = no }
			picture = central_management

			modifier = {
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.10
				political_power_factor = 0.25
				civilian_factories_productivity = 0.15
				civilian_industry_tax_modifier = 0.05
			}
		}

		BUL_idea_eu_research_agreement = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_eu_research_agreement" }

			picture = shared_research

			modifier = {
				research_speed_factor = 0.10
			}
		}

		BUL_idea_federalization_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_federalization_1" }

			picture = international_treaty

			allowed = { original_tag = BUL }
			allowed_civil_war = { always = yes }

			modifier = {
				political_power_factor = 0.05
				consumer_goods_factor = -0.02
				production_speed_buildings_factor = 0.05
				local_resources_factor = 0.05
			}
		}

		BUL_idea_federalization_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_federalization_2" }

			picture = international_treaty

			allowed = { original_tag = BUL }
			allowed_civil_war = { always = yes }

			modifier = {
				political_power_factor = 0.05
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.1
				local_resources_factor = 0.1
			}
		}

		BUL_idea_agrotech = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_agrotech" }

			picture = agriculture

			modifier = {
				global_building_slots_factor = 0.05
				local_resources_factor = 0.10
			}
		}

		BUL_expertise = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_expertise" }
			picture = local_self_management
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_buildings_factor = 0.05
				production_factory_start_efficiency_factor = 0.05
			}
		}

		BUL_lower_tax = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea 	BUL_lower_tax" }

			allowed = {
				always = no
			}

			picture = central_management

			modifier = {
				consumer_goods_factor = -0.10
				tax_rate_change_multiplier_modifier = -0.15
			}
		}

		BUL_idea_pro_farmers = {
			name = ARM_idea_pro_farmers
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_pro_farmers" }

			picture = agriculture

			modifier = {
				global_building_slots_factor = 0.05
				MONTHLY_POPULATION = 0.15
				local_resources_factor = 0.10
				agricolture_productivity_modifier = 0.10
				agriculture_tax_modifier = 0.10
				agriculture_workers_modifier = 0.05
			}
		}

		BUL_increased_privatization = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea   BUL_increased_privatization " }
			allowed = { always = no }

			picture = market_dynamics

			modifier = {
				production_speed_buildings_factor = 0.05
				line_change_production_efficiency_factor = 0.075
				production_factory_max_efficiency_factor = 0.05
				office_park_income_tax_modifier = 0.05
			}
		}

		BUL_indoctrinate_youth = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_indoctrinate_youth" }

			allowed = {
				always = no
			}

			picture = fascism4

			modifier = {
				nationalist_drift = 0.03
				conscription = 0.02
			}
		}

		BUL_paramilitary_organization_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_paramilitary_organization_idea" }
			allowed = { always = no }
			picture = army_corruption3

			modifier = {
				experience_gain_army = 0.05
			}
		}

		BUL_christian_state_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_christian_state_2" }
			allowed = { always = no }
			picture = christian_idea

			modifier = {
				stability_factor = 0.1
				monthly_population = 0.08
				research_speed_factor = -0.05
			}
		}

		BUL_wild_unions = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_wild_unions" }

			allowed = {
				always = no
			}

			picture = disjointed_government

			modifier = {
				communism_drift = 0.02
				industrial_capacity_factory = -0.05
				consumer_goods_factor = 0.01
			}
		}

		BUL_press_opposition = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_press_opposition" }

			allowed = {
				always = no
			}
			picture = political_drain

			modifier = {
				political_power_factor = -0.25
			}
		}

		BUL_one_party = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_one_party" }

			allowed = {
				always = no
			}

			picture = international_treaty

			modifier = {
				communism_drift = 0.1
				stability_factor = -0.1
			}
		}

		BUL_red_army_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_red_army_idea" }

			allowed = {
				always = no
			}
			picture = communism9

			modifier = {
				conscription = 0.01
				justify_war_goal_time = -0.25
				offensive_war_stability_factor = 0.05
				training_time_army_factor = -0.2
			}
		}

		BUL_com_resources_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_com_resources_idea" }

			allowed = {
				always = no
			}
			picture = political_drain

			modifier = {
				local_resources_factor = 0.10
			}
		}

		BUL_true_market = {
			allowed = {
				original_tag = BUL
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = china_factory
			modifier = {
				stability_factor = 0.001
				improve_relations_maintain_cost_factor = -0.3
				trade_opinion_factor = 0.3
				production_factory_max_efficiency_factor = 0.05
				global_building_slots_factor = 0.10
				production_speed_buildings_factor = 0.1
			}
		}

		BUL_inner_network_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_inner_network_idea" }
			allowed = { always = no }
			picture = spy_intel

			modifier = {
				encryption = 1
				decryption = 1
			}
		}

		BUL_nato_research_research_boost = {

			picture = wargaming

			allowed = {
				original_tag = BUL
			}

			modifier = {
				land_doctrine_cost_factor = -0.10
			}
		}

		BUL_kalashnikov = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_kalashnikov" }

			picture = volunteer_defenders

			modifier = {
				industrial_capacity_factory = 0.05
			}
		}

		BUL_quantity_small_arms_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_quantity_small_arms_idea" }
			allowed = { always = no }
			picture = volunteer_defenders2
			equipment_bonus = {
				Inf_equipment = {
					instant = yes
					build_cost_ic = -0.05
				}
			}
		}

		BUL_small_ship_project = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_small_ship_project" }
			picture = liberty_ships

			equipment_bonus = {
				submarine = {
					build_cost_ic = -0.15
					instant = yes
				}
				destroyer = {
					build_cost_ic = -0.15
					instant = yes
				}
			}
		}

		BUL_idea_eacu_cooperation = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_eacu_cooperation" }
			picture = sov_eaes_idea
			cancel = {
				has_war_with = BUL
			}
			modifier = {
				political_power_factor = 0.15
				war_support_factor = -0.02
				stability_factor = 0.02
				local_resources_factor = 0.10
			}
		}
		###EACO Integration
		BUL_idea_eacu_cooperation_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_eacu_cooperation_2" }
			picture = sov_eaes_idea
			cancel = {
				has_war_with = BUL
			}
			modifier = {
				political_power_factor = 0.25
				war_support_factor = -0.05
				stability_factor = 0.05
				local_resources_factor = 0.20
			}
		}

		BUL_idea_diplomatic_powerhouse = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_diplomatic_powerhouse" }

			picture = flexible_foreign_policy2

			modifier = {
				trade_opinion_factor = 0.125
				justify_war_goal_time = 0.125
				send_volunteer_size = 2
			}
		}

		BUL_idea_the_international_mediator = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_the_international_mediator" }

			picture = flexible_foreign_policy2

			modifier = {
				trade_opinion_factor = 0.25
				justify_war_goal_time = 0.25
				send_volunteer_size = 5
			}
		}

		BUL_idea_a_global_power = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_a_global_power" }

			picture = international_treaty

			modifier = {
				drift_defence_factor = 0.50
				stability_weekly = 0.001
				max_planning = 0.25
				encryption_factor = 0.05
			}
		}

		BUL_idea_new_wave_ideas = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUL_idea_new_wave_ideas" }

			allowed_civil_war = {
				has_government = communism
			}

			picture = neutrality

			modifier = {
				political_power_cost = 0.25
				war_support_factor = 0.10
				industrial_capacity_factory = 0.10
			}
		}
	}
}