ideas = {
	country = {
		Enduring_Freedom = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea Enduring_Freedom" }
			allowed_civil_war = {
				no_jihadist_government = yes
			}
			cancel = {
				jihadist_government = yes
			}
			modifier = {
				experience_gain_army = 0.05
				fascism_drift = -0.01
			}
		}
		Inherent_Resolve = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea Inherent_Resolve" }
			allowed_civil_war = {
				no_jihadist_government = yes
			}
			cancel = {
				jihadist_government = yes
			}
			modifier = {
				experience_gain_army = 0.05
				fascism_drift = -0.01
			}
		}

		##NAFTA
		USA_pro_american_deal_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add USA_pro_american_deal_idea" }
			picture = trade
			allowed_civil_war = { always = yes }
			modifier = {
				consumer_goods_factor = -0.01
				industrial_capacity_factory = -0.10
				political_power_factor = 0.10
				trade_opinion_factor = 0.15
			}
		}
		USA_pro_american_deal_mex_can_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add USA_pro_american_deal_idea" }
			picture = trade
			modifier = {
				political_power_factor = 0.05
				trade_opinion_factor = 0.05
			}
		}
		USA_mutually_benefical_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add USA_mutually_benefical_idea" }
			picture = trade
			allowed_civil_war = { always = yes }
			modifier = {
				industrial_capacity_factory = -0.10
				political_power_factor = 0.10
				trade_opinion_factor = 0.15
			}
		}
		USA_america_fist_deal_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add USA_america_fist_deal_idea" }
			picture = trade
			allowed_civil_war = { always = yes }
			modifier = {
				consumer_goods_factor = -0.02
				industrial_capacity_factory = -0.15
				political_power_factor = 0.15
				trade_opinion_factor = 0.20
			}
		}
		USA_america_fist_deal_mex_can_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add USA_america_fist_deal_mex_can_idea" }
			picture = trade
			allowed_civil_war = { always = yes }
			modifier = {
				industrial_capacity_factory = -0.05
				political_power_factor = 0.05
				trade_opinion_factor = 0.10
			}
		}
	}
}
