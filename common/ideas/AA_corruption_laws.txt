ideas = {
	corruption = {
		law = yes
		use_list_view = yes

		paralyzing_corruption = {
			cost = 300
			removal_cost = -1

			picture = corruption_10

			available = {
				OR = {
					has_idea = paralyzing_corruption
					has_idea = crippling_corruption
				}
			}
			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea paralyzing_corruption"
			}

			ai_will_do = {
				factor = 0
			}

			modifier = {
				country_productivity_growth_modifier = -0.20
				drift_defence_factor = -0.2 					#Easier for other Countries to influence you
				foreign_subversive_activites = 0.2 				#Easier for other countries to do stuff to you
				industrial_capacity_factory = -0.2 				#Decreases equipment prod efficiency
				research_speed_factor = -0.15 					#Research is more inefficient
				encryption = -1 								#Lowers encryption
				army_morale_factor = -0.2
				army_speed_factor = -0.025
				intelligence_agency_defense = -1
				foreign_influence_defense_modifier = -0.50
				tax_gain_multiplier_modifier = -0.35
				receiving_investment_cost_modifier = 0.45
				international_market_income_modifier = 0.50
				international_market_purchase_modifier = 0.50
				migration_rate_value_factor = -0.15
				minimum_terror_threat_modifier = 20
				minimum_radicalization_modifier = 20
			}

			cancel_if_invalid = no
		}

		crippling_corruption = {
			cost = 300
			removal_cost = -1
			#level = 9

			picture = corruption_9

			available = {
				OR = {
					has_idea = paralyzing_corruption
					has_idea = crippling_corruption
					has_idea = rampant_corruption
				}
			}

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea crippling_corruption"
			}

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 4000 # Nations should make getting away from extreme corruption a top priority
					has_idea = paralyzing_corruption
				}
				modifier = {
					factor = 0 # Should make countries want to move away from current corruption levels
					has_idea = crippling_corruption
				}
			}

			modifier = {
				country_productivity_growth_modifier = -0.18
				drift_defence_factor = -0.16 					#Easier for other Countries to influence you
				foreign_subversive_activites = 0.16 		#Easier for other countries to do stuff to you
				industrial_capacity_factory = -0.18 		#Decreases equipment prod efficiency
				research_speed_factor = -0.13 				#Research is more inefficient
				encryption = -1 							#Lowers encryption
				army_morale_factor = -0.15
				army_speed_factor = -0.010
				intelligence_agency_defense = -0.9
				foreign_influence_defense_modifier = -0.45
				tax_gain_multiplier_modifier = -0.30
				receiving_investment_cost_modifier = 0.40
				international_market_income_modifier = 0.40
				international_market_purchase_modifier = 0.40
				migration_rate_value_factor = -0.12
				minimum_terror_threat_modifier = 15
				minimum_radicalization_modifier = 15
			}

			cancel_if_invalid = no
		}

		rampant_corruption = {
			cost = 300
			removal_cost = -1
			#level = 8

			picture = corruption_8

			available = {
				OR = {
					has_idea = crippling_corruption
					has_idea = rampant_corruption
					has_idea = unrestrained_corruption
				}
			}

			default = yes #Most countries are around this

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea rampant_corruption"
			}

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 4000 # Nations should make getting away from extreme corruption a top priority
					has_idea = crippling_corruption
				}
				modifier = {
					factor = 0 # Should make countries want to move away from current corruption levels
					has_idea = rampant_corruption
				}
			}

			modifier = {
				country_productivity_growth_modifier = -0.16
				drift_defence_factor = -0.12 					#Easier for other Countries to influence you
				foreign_subversive_activites = 0.12 			#Easier for other countries to do stuff to you
				industrial_capacity_factory = -0.16 			#Decreases equipment prod efficiency
				research_speed_factor = -0.13 					#Research is more inefficient
				encryption = -1 								#Lowers encryption
				army_morale_factor = -0.10
				intelligence_agency_defense = -0.8
				foreign_influence_defense_modifier = -0.40
				tax_gain_multiplier_modifier = -0.25
				receiving_investment_cost_modifier = 0.35
				international_market_income_modifier = 0.35
				international_market_purchase_modifier = 0.35
				migration_rate_value_factor = -0.10
				minimum_terror_threat_modifier = 10
				minimum_radicalization_modifier = 10
			}

			cancel_if_invalid = no
		}

		unrestrained_corruption = {
			cost = 300
			removal_cost = -1
			#level = 7

			picture = corruption_7

			available = {
				OR = {
					has_idea = rampant_corruption
					has_idea = unrestrained_corruption
					has_idea = systematic_corruption
				}
			}

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea unrestrained_corruption"
			}

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 320 # Nations should make getting away from high corruption an urgent priority
					has_idea = rampant_corruption
				}
				modifier = {
					factor = 0 # Should make countries want to move away from current corruption levels
					has_idea = unrestrained_corruption
				}
			}

			modifier = {
				country_productivity_growth_modifier = -0.14
				drift_defence_factor = -0.08 					#Easier for other Countries to influence you
				foreign_subversive_activites = 0.08 		#Easier for other countries to do stuff to you
				industrial_capacity_factory = -0.14 		#Decreases equipment prod efficiency
				research_speed_factor = -0.11 				#Research is more inefficient
				army_morale_factor = -0.05
				intelligence_agency_defense = -0.7
				foreign_influence_defense_modifier = -0.35
				tax_gain_multiplier_modifier = -0.20
				receiving_investment_cost_modifier = 0.30
				international_market_income_modifier = 0.30
				international_market_purchase_modifier = 0.30
				migration_rate_value_factor = -0.08
				minimum_terror_threat_modifier = 5
				minimum_radicalization_modifier = 5
			}

			cancel_if_invalid = no
		}

		systematic_corruption = {
			cost = 300
			removal_cost = -1
			#level = 6

			picture = corruption_6

			available = {
				OR = {
					has_idea = unrestrained_corruption
					has_idea = systematic_corruption
					has_idea = widespread_corruption
				}
			}

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea systematic_corruption"
			}

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 320 # Nations should make getting away from high corruption an urgent priority
					has_idea = unrestrained_corruption
				}
				modifier = {
					factor = 0 # Should make countries want to move away from current corruption levels
					has_idea = systematic_corruption
				}
			}

			modifier = {
				country_productivity_growth_modifier = -0.12
				drift_defence_factor = -0.04 					#Easier for other Countries to influence you
				foreign_subversive_activites = 0.04 		#Easier for other countries to do stuff to you
				industrial_capacity_factory = -0.12 		#Decreases equipment prod efficiency
				research_speed_factor = -0.08 				#Research is more inefficient
				intelligence_agency_defense = -0.6
				foreign_influence_defense_modifier = -0.30
				tax_gain_multiplier_modifier = -0.15
				receiving_investment_cost_modifier = 0.25
				international_market_income_modifier = 0.25
				international_market_purchase_modifier = 0.25
				migration_rate_value_factor = -0.05
			}

			cancel_if_invalid = no
		}

		widespread_corruption = {
			cost = 300
			removal_cost = -1
			#level = 5

			picture = corruption_5

			available = {
				OR = {
					has_idea = systematic_corruption
					has_idea = widespread_corruption
					has_idea = medium_corruption
				}
			}

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea widespread_corruption"
			}

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 40 # Nations should make getting away from high corruption high priority
					has_idea = systematic_corruption
				}
				modifier = {
					factor = 0 # Should make countries want to move away from current corruption levels
					has_idea = widespread_corruption
				}
			}

			modifier = {
				country_productivity_growth_modifier = -0.10
				industrial_capacity_factory = -0.10 		#Decreases equipment prod efficiency
				research_speed_factor = -0.06 				#Research is more inefficient
				intelligence_agency_defense = -0.5
				foreign_influence_defense_modifier = -0.25
				tax_gain_multiplier_modifier = -0.10
				receiving_investment_cost_modifier = 0.20
				international_market_income_modifier = 0.20
				international_market_purchase_modifier = 0.20
				migration_rate_value_factor = -0.03
			}

			cancel_if_invalid = no
		}

		medium_corruption = {
			cost = 300
			removal_cost = -1
			#level = 4

			picture = corruption_4

			available = {
				OR = {
					has_idea = widespread_corruption
					has_idea = medium_corruption
					has_idea = modest_corruption
				}
				if = {
					limit = {
						original_tag = TAJ
					}
					NOT = {
						has_idea = TAJ_rahmons_rotten_fruit
					}
				}
			}

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 10 # Nations should make getting away from corruption a priority
					has_idea = widespread_corruption
				}
				modifier = {
					factor = 0 # Should make countries want to move away from current corruption levels
					has_idea = medium_corruption
				}
			}

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea medium_corruption"
			}

			modifier = {
				country_productivity_growth_modifier = -0.08
				industrial_capacity_factory = -0.08 		#Decreases equipment prod efficiency
				research_speed_factor = -0.04 				#Research is more inefficient
				intelligence_agency_defense = -0.4
				foreign_influence_defense_modifier = -0.20
				tax_gain_multiplier_modifier = -0.05
				receiving_investment_cost_modifier = 0.15
				international_market_income_modifier = 0.15
				international_market_purchase_modifier = 0.15
			}

			cancel_if_invalid = no
		}

		modest_corruption = {
			cost = 300
			removal_cost = -1
			#level = 3

			picture = corruption_3

			available = {
				OR = {
					has_idea = medium_corruption
					has_idea = modest_corruption
					has_idea = slight_corruption
				}
				if = {
					limit = {
						original_tag = TAJ
					}
					NOT = {
						has_idea = TAJ_rahmons_rotten_fruit
					}
				}
			}

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea modest_corruption"
			}

			modifier = {
				country_productivity_growth_modifier = -0.06
				industrial_capacity_factory = -0.06 		#Decreases equipment prod efficiency
				research_speed_factor = -0.02 				#Research is more inefficient
				intelligence_agency_defense = -0.3
				foreign_influence_defense_modifier = -0.15
				tax_gain_multiplier_modifier = -0.03
				receiving_investment_cost_modifier = 0.10
				international_market_income_modifier = 0.1
				international_market_purchase_modifier = 0.1
				migration_rate_value_factor = 0.05
			}

			cancel_if_invalid = no

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 2.5 # Nations should make getting away from corruption a priority
					has_idea = medium_corruption
				}
				modifier = {
					factor = 0 # Should make countries want to move away from current corruption levels
					has_idea = modest_corruption
				}
			}
		}

		slight_corruption = {
			cost = 300
			removal_cost = -1
			#level = 2

			picture = corruption_2

			available = {
				OR = {
					has_idea = modest_corruption
					has_idea = slight_corruption
					has_idea = negligible_corruption
				}
				if = {
					limit = {
						original_tag = TAJ
					}
					NOT = {
						has_idea = TAJ_rahmons_rotten_fruit
					}
				}
			}

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea slight_corruption"
			}

			modifier = {
				country_productivity_growth_modifier = -0.04
				research_speed_factor = -0.01 				#Research is more inefficient
				intelligence_agency_defense = -0.2
				foreign_influence_defense_modifier = -0.10
				tax_gain_multiplier_modifier = -0.01
				receiving_investment_cost_modifier = 0.05
				international_market_purchase_modifier = 0.05
				migration_rate_value_factor = 0.10
			}

			cancel_if_invalid = no

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 2.5 # Nations should make getting away from corruption a priority
					has_idea = modest_corruption
				}
				modifier = {
					factor = 0 # Should make countries want to move away from current corruption levels
					has_idea = slight_corruption
				}
			}
		}

		negligible_corruption = {
			cost = 300
			removal_cost = -1
			#level = 1

			picture = corruption_1

			available = {
				OR = {
					has_idea = slight_corruption
					has_idea = negligible_corruption
				}
				if = {
					limit = {
						original_tag = TAJ
					}
					NOT = {
						has_idea = TAJ_rahmons_rotten_fruit
					}
				}
			}

			modifier = {
				migration_rate_value_factor = 0.15
			}

			on_add = {
				custom_effect_tooltip = law_corruption_TT
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea negligible_corruption"
			}

			cancel_if_invalid = no

			ai_will_do = {
				factor = 1
				modifier = {
					factor = 2.5 # Should make countries want to move away from current corruption levels
					western_autocrats_are_not_in_power = yes
					has_idea = slight_corruption
				}
			}
		}
	}
}
