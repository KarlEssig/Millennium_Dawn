ideas = {
	materiel_manufacturer = {
		designer = yes
		CHI_norinco_materiel_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_norinco_materiel_manufacturer" }
			picture = Norinco_CHI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf = 0.248
			}

			traits = { CAT_inf_8 }
			ai_will_do = {
				factor = 1
			}
		}
		CHI_china_south_industries_group_materiel_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_china_south_industries_group_materiel_manufacturer" }

			picture = China_South_Industries_Group_CHI

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.186
			}

			traits = {
				CAT_inf_wep_6
			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		CHI_norinco_tank_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_norinco_tank_manufacturer" }
			picture = Norinco_CHI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.248
			}

			traits = { CAT_armor_8 }
			ai_will_do = {
				factor = 1
			}
		}
		CHI_inner_mongolia_first_machinery_tank_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_inner_mongolia_first_machinery_tank_manufacturer" }
			picture = Inner_Mongolia_First_Machinery_CHI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.186
			}

			traits = { CAT_armor_6 }
			ai_will_do = {
				factor = 1
			}
		}
		CHI_changhe_aircraft_corp_tank_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_changhe_aircraft_corp_tank_manufacturer" }

			picture = Changhe_Aircraft_Corp_CHI
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_heli = 0.155
			}

			traits = {
				CAT_heli_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		CHI_chengdu_aircraft_corp_aircraft_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_chengdu_aircraft_corp_aircraft_manufacturer" }

			picture = Chengdu_Aircraft_Corp_CHI
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fighter = 0.248
			}

			traits = {
				CAT_fighter_8

			}
			ai_will_do = {
				factor = 1
			}
		}

		CHI_shenyang_aircraft_corp_aircraft_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_shenyang_aircraft_corp_aircraft_manufacturer" }

			picture = Shenyang_Aircraft_Corp_CHI
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.217
			}

			traits = {
				CAT_fixed_wing_7

			}
			ai_will_do = {
				factor = 1
			}

		}

		CHI_xian_aircraft_corp_aircraft_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_xian_aircraft_corp_aircraft_manufacturer" }

			picture = Xian_Aircraft_Corp_CHI
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_h_air = 0.186
			}

			traits = {
				CAT_h_air_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		CHI_china_state_shipbuilding_corporation_naval_manufacturer = {
			allowed = {
				original_tag = CHI
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_china_state_shipbuilding_corporation_naval_manufacturer" }

			picture = China_State_Shipbuilding_Corporation_CHI

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_naval_eqp = 0.217
			}

			traits = {
				CAT_naval_eqp_7

			}
			ai_will_do = {
				factor = 1
			}
		}

		CHI_china_shipbuilding_industry_corporation_naval_manufacturer = {
			allowed = { original_tag = CHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CHI_china_shipbuilding_industry_corporation_naval_manufacturer" }
			picture = China_Shipbuilding_Industry_Corporation_CHI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_sub = 0.186
			}

			traits = { CAT_sub_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
