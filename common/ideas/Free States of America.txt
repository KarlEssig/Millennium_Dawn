ideas = {
	country = {
		#Starting Ideas For The Free States of America
		free_states_idea_isolationist_focused_foreign_office = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_isolationist_focused_foreign_office" }
			picture = disjointed_government
			allowed_civil_war = { always = yes }
			modifier = {
				justify_war_goal_time = 1.00
				enemy_justify_war_goal_time = 1.00
				foreign_influence_defense_modifier = 0.50
			}
		}

		free_states_idea_neighborhood_foreign_policy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_neighborhood_foreign_policy" }
			picture = disjointed_government
			allowed_civil_war = { always = yes }
			modifier = {
				justify_war_goal_time = 0.50
				enemy_justify_war_goal_time = 0.50
				foreign_influence_modifier = 0.15
				foreign_influence_defense_modifier = 0.25
			}
		}



		##### A Post United Era Ideas #####

		free_states_idea_naval_budget_priorities = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_naval_budget_priorities" }
			picture = ocean_navy
			allowed_civil_war = { always = yes }
			modifier = {
				min_export = -0.05
				trade_opinion_factor = 0.05
				interest_rate_multiplier_modifier = 0.50
				industrial_capacity_dockyard = 0.15
				navy_anti_air_attack_factor = 0.10
				naval_speed_factor = 0.07
				naval_coordination = 0.05
				naval_damage_factor = 0.03
				naval_defense_factor = 0.03
				foreign_influence_defense_modifier = 0.15
			}
		}

		free_states_idea_army_defensive_budgeting = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_army_defensive_budgeting" }
			picture = defense
			allowed_civil_war = { always = yes }
			modifier = {
				experience_gain_factor = 0.25
				planning_speed = 0.15
				cas_damage_reduction = 0.10
				army_speed_factor = 0.05
				dig_in_speed_factor = 0.10
				max_dig_in = 5
				supply_node_range = 0.25
			}
		}

		free_states_idea_naval_renovations_budget = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_naval_renovations_budget" }
			picture = liberty_ships
			allowed_civil_war = { always = yes }
			modifier = {
				experience_gain_factor = 0.25
				industrial_capacity_dockyard = 0.10
				naval_coordination = 0.05
				naval_accidents_chance = -0.07
				naval_night_attack = 0.07
			}
		}

		free_states_idea_foreign_relations_act = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_foreign_relations_act" }
			picture = trade
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = -0.10
				min_export = -0.10
				production_speed_buildings_factor = 0.25
				industrial_capacity_factory = 0.05
				industrial_capacity_dockyard = 0.10
				foreign_influence_defense_modifier = 0.10
			}
		}

		free_states_idea_domestic_priority_policy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_domestic_priority_policy" }
			picture = triumphant_will
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = -0.05
				trade_opinion_factor = -0.15
				min_export = -0.10
				industrial_capacity_factory = -0.08
				foreign_influence_defense_modifier = 0.10
				foreign_influence_modifier = -0.05
			}
		}

		free_states_idea_diplomatic_corps = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_diplomatic_corps" }
			picture = tower_investment
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = -0.05
				improve_relations_maintain_cost_factor = -0.50
				opinion_gain_monthly = 0.50
				enemy_justify_war_goal_time = 0.35
				foreign_influence_defense_modifier = 0.10
				foreign_influence_modifier = 0.05
			}
		}

		free_states_idea_national_labor_corps = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_national_labor_corps" }
			picture = Labor_Reform
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = -0.02
				production_speed_buildings_factor = 0.10
				local_resources_factor = 0.25
				industry_free_repair_factor = 0.25
				foreign_influence_defense_modifier = 0.04
				foreign_influence_modifier = 0.04
				supply_node_range = 0.10
			}
		}

		free_states_idea_extreme_mining_projects = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_extreme_mining_projects" }
			picture = resource_production
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = -0.09
				local_resources_factor = 0.50
				min_export = 0.15
				trade_opinion_factor = 0.35
				industry_free_repair_factor = 0.25
				subjects_autonomy_gain = -5
				autonomy_gain = 5
				resource_export_multiplier_modifier = 0.20
				foreign_influence_modifier = 0.10
			}
		}

		free_states_idea_strategic_autarky = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_strategic_autarky" }
			picture = coal
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = 0.10
				production_factory_max_efficiency_factor = 0.15
				production_factory_start_efficiency_factor = 0.10
				min_export = -0.15
				autonomy_gain = 6
				resource_export_multiplier_modifier = -0.20
				foreign_influence_defense_modifier = 0.15
				supply_node_range = 0.15
			}
		}

		free_states_idea_consumer_complex_initiative = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_consumer_complex_initiative" }
			picture = consumer_goods
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = 0.05
				production_speed_supply_node_factor = 0.15
				production_speed_industrial_complex_factor = 0.10
				production_speed_offices_factor = -0.15
				production_speed_arms_factory_factor = -0.15
				production_speed_dockyard_factor = -0.15
				min_export = -0.05
				subjects_autonomy_gain = -5
				local_resources_factor = 0.10
				foreign_influence_defense_modifier = 0.05
				foreign_influence_modifier = 0.04
				supply_node_range = 0.25
			}
		}

		free_states_idea_economic_efficiency_model = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_economic_efficiency_model" }
			picture = tiger_economy
			allowed_civil_war = { always = yes }
			modifier = {
				production_factory_efficiency_gain_factor = 0.20
				production_factory_start_efficiency_factor = 0.10
				industrial_capacity_factory = -0.15
				industrial_capacity_dockyard = -0.08
				production_speed_supply_node_factor = 0.15
				production_speed_industrial_complex_factor = -0.15
				production_speed_offices_factor = 0.15
				production_speed_arms_factory_factor = -0.08
				production_speed_dockyard_factor = -0.08
				min_export = -0.05
				subjects_autonomy_gain = -2
				autonomy_gain = 2
				local_resources_factor = 0.10
				foreign_influence_defense_modifier = 0.06
				foreign_influence_modifier = 0.06
				supply_node_range = 0.25
			}
		}

		free_states_idea_military_readiness_model = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_military_readiness_model" }
			picture = generic_smallarms
			allowed_civil_war = { always = yes }
			modifier = {
				production_factory_efficiency_gain_factor = 0.10
				industrial_capacity_factory = 0.08
				industrial_capacity_dockyard = 0.15
				production_speed_supply_node_factor = 0.15
				production_speed_industrial_complex_factor = -0.20
				production_speed_offices_factor = -0.20
				production_speed_arms_factory_factor = 0.25
				production_speed_dockyard_factor = 0.25
				min_export = -0.05
				army_defence_factor = 0.05
				naval_defense_factor = 0.05
				air_defence_factor = 0.05
				foreign_influence_defense_modifier = 0.12
				foreign_influence_modifier = 0.05
				supply_node_range = 0.25
			}
		}

		free_states_idea_department_for_synthetic_development = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_department_for_synthetic_development" }
			picture = refining_concern
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_fossil_powerplant_factor = 0.75
				fossil_pp_fuel_consumption_modifier = -0.34
				production_speed_supply_node_factor = 0.10
				production_speed_synthetic_refinery_factor = -0.50
				production_speed_nuclear_reactor_factor = -0.50
				energy_use_multiplier = -0.10
				fossil_pp_energy_generation_modifier = 0.27
				army_fuel_consumption_factor = -0.18
				air_fuel_consumption_factor = -0.07
				navy_fuel_consumption_factor = -0.07
				foreign_influence_defense_modifier = 0.09
				foreign_influence_modifier = 0.20
				supply_node_range = 0.10
			}
		}

		free_states_idea_department_for_energy_innovation = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_department_for_energy_innovation" }
			picture = nuclear_power
			allowed_civil_war = { always = yes }
			modifier = {
				production_factory_efficiency_gain_factor = 0.10
				production_speed_fossil_powerplant_factor = -0.50
				production_speed_supply_node_factor = 0.10
				production_speed_synthetic_refinery_factor = 0.75
				production_speed_nuclear_reactor_factor = 0.50
				energy_use_multiplier = -0.10
				non_electric_fuel_consumption_modifier = -0.18
				nuclear_energy_generation_modifier = 0.11
				state_renewable_energy_generation_modifier = 0.18
				air_fuel_consumption_factor = -0.15
				navy_fuel_consumption_factor = -0.23
				foreign_influence_defense_modifier = 0.09
				foreign_influence_modifier = 0.20
				supply_node_range = 0.10
			}
		}

		free_states_idea_infrastructure_readiness_act = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_infrastructure_readiness_act" }
			picture = construction
			allowed_civil_war = { always = yes }
			modifier = {
				production_speed_infrastructure_factor = 0.25
				production_speed_internet_station_factor = 0.10
				production_speed_supply_node_factor = 0.25
				tax_gain_multiplier_modifier = 0.05
				interest_rate_multiplier_modifier = -0.50
				production_factory_max_efficiency_factor = 0.15
				local_resources_factor = 0.15
				foreign_influence_defense_modifier = 0.04
				foreign_influence_modifier = 0.04
				supply_node_range = 0.15
			}
		}

		free_states_idea_military_high_command = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_military_high_command" }
			picture = officer_military_academy_idea
			allowed_civil_war = { always = yes }
			modifier = {
				experience_gain_army = 0.05
				experience_gain_navy = 0.02
				experience_gain_air = 0.03
				experience_gain_factor = 0.25
				experience_loss_factor = -0.05
				military_leader_cost_factor = -0.10
			}
		}

		free_states_idea_defensive_officers_school = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_defensive_officers_school" }
			picture = army_officer
			allowed_civil_war = { always = yes }
			modifier = {
				army_leader_start_defense_level = 3
				army_leader_start_attack_level = -1
				experience_gain_factor = 0.20
				experience_loss_factor = -0.10
				recon_factor_while_entrenched = 0.25
				out_of_supply_factor = -0.10
				army_morale_factor = 0.07
			}
		}

		free_states_idea_the_bulwark_doctrine = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_the_bulwark_doctrine" }
			picture = defense
			allowed_civil_war = { always = yes }
			modifier = {
				army_leader_start_logistics_level = 3
				army_leader_start_attack_level = -1
				combat_width_factor = -0.15
				experience_loss_factor = -0.07
				cas_damage_reduction = 0.10
				out_of_supply_factor = -0.05
				army_morale_factor = 0.07
				terrain_penalty_reduction = 0.10
			}
		}

		free_states_idea_the_fighter_defense_doctrine = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_the_fighter_defense_doctrine" }
			picture = fighter_production_increase
			allowed_civil_war = { always = yes }
			modifier = {
				air_range_factor = -0.10
				air_weather_penalty = -0.10
				air_night_penalty = -0.15
				air_superiority_efficiency = 0.25
				air_detection = 0.10
			}
		}

		free_states_idea_the_increasing_defensive_capabilites = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_the_increasing_defensive_capabilites" }
			picture = air_research
			allowed_civil_war = { always = yes }
			modifier = {
				air_range_factor = -0.10
				air_weather_penalty = -0.07
				air_night_penalty = -0.07
				army_bonus_air_superiority_factor = 0.12
				air_detection = 0.05
				air_wing_xp_loss_when_killed_factor = -0.10
			}
		}

		free_states_idea_naval_mining_warfare_refinement_program = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_the_increasing_defensive_capabilites" }
			picture = SOV_mine_warfare
			allowed_civil_war = { always = yes }
			modifier = {
				naval_mine_hit_chance = -0.10
				naval_mines_damage_factor = 0.07
				naval_mines_effect_reduction = 0.10
				mines_planting_by_fleets_factor = 0.08
				mines_sweeping_by_fleets_factor = 0.08
				navy_leader_start_maneuvering_level = 3
				navy_leader_start_attack_level = -1
			}
		}

		free_states_idea_armored_assault_assist_groups = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_armored_assault_assist_groups" }
			picture = armor2
			allowed_civil_war = { always = yes }
			modifier = {
				army_armor_attack_factor = 0.07
				mechanized_attack_factor = 0.07
				river_crossing_factor = -0.07
				terrain_penalty_reduction = 0.08
				army_leader_start_attack_level = 2
				army_leader_start_defense_level = -1
				experience_gain_factor = 0.10
				org_loss_when_moving = -0.05
				army_speed_factor = 0.05
			}
		}

		free_states_idea_aggressive_officer_corps = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_aggressive_officer_corps" }
			picture = army_of_aggression
			allowed_civil_war = { always = yes }
			modifier = {
				planning_speed = 0.15
				land_night_attack = 0.15
				river_crossing_factor = -0.05
				terrain_penalty_reduction = 0.10
				experience_gain_factor = 0.10
				army_leader_start_attack_level = 2
				army_leader_start_logistics_level = -1
				org_loss_when_moving = -0.05
				army_speed_factor = 0.05
			}
		}

		free_states_idea_material_not_human_loss = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_material_not_human_loss" }
			picture = lack_of_supply
			allowed_civil_war = { always = yes }
			modifier = {
				experience_loss_factor = -0.10
				attrition = 0.05
				out_of_supply_factor = 0.07
				no_supply_grace = 72
				experience_gain_factor = 0.10
			}
		}

		free_states_idea_survivability_program = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_survivability_program" }
			picture = air_research
			allowed_civil_war = { always = yes }
			modifier = {
				air_weather_penalty = -0.10
				air_night_penalty = -0.15
				air_superiority_efficiency = 0.25
				air_detection = 0.15
				air_wing_xp_loss_when_killed_factor = -0.15
			}
		}

		free_states_idea_admirable_admiralty = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_admirable_admiralty" }
			picture = navy_bonus
			allowed_civil_war = { always = yes }
			modifier = {
				experience_gain_factor = 0.10
				navy_leader_start_attack_level = 1
				navy_leader_start_maneuvering_level = 1
				navy_leader_start_defense_level = 1
				navy_leader_start_coordination_level = 1
				experience_gain_navy_unit_factor = 0.10
			}
		}

		free_states_idea_hit_them_again_doctrine = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_hit_them_again_doctrine" }
			picture = piracy
			allowed_civil_war = { always = yes }
			modifier = {
				naval_hit_chance = 0.07
				positioning = 0.10
				naval_coordination = 0.10
				naval_critical_score_chance_factor = 0.15
				critical_receive_chance = -0.15
			}
		}

		free_states_idea_the_assimilation_program = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_the_assimilation_program" }
			picture = national_unity
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = -0.10
				subjects_autonomy_gain = -3
				compliance_gain = 0.10
				stability_weekly = 0.0010
				war_support_weekly = 0.0010
				monthly_population = -0.25
				drift_defence_factor = 0.50
				foreign_influence_defense_modifier = 0.10
				foreign_influence_modifier = 0.05
				min_export = -0.03
			}
		}

		free_states_idea_the_second_american_republic = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_the_assimilation_program" }
			picture = land_of_the_free
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = -0.10
				autonomy_gain = 4
				compliance_gain = 0.15
				stability_weekly = 0.0010
				war_support_weekly = 0.0010
				drift_defence_factor = 0.50
				foreign_influence_defense_modifier = 0.20
				foreign_influence_modifier = 0.10
			}
		}

		free_states_idea_the_self_made_embargo = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_the_self_made_embargo" }
			picture = embargo
			allowed_civil_war = { always = yes }
			modifier = {
				min_export = -1
				trade_opinion_factor = -1
				trade_laws_cost_factor = 1
				autonomy_gain = 2
				resistance_damage_to_garrison_on_our_occupied_states = 0.10
				war_support_weekly = -0.0010
			}
		}

		free_states_idea_ground_to_air_dominance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_ground_to_air_dominance" }
			picture = air_support
			allowed_civil_war = { always = yes }
			modifier = {
				land_night_attack = 0.25
				autonomy_gain = 2
				recon_factor = 0.25
				enemy_army_bonus_air_superiority_factor = -0.15
				cas_damage_reduction = 0.15
				industry_air_damage_factor = -0.10
				static_anti_air_damage_factor = 0.15
				static_anti_air_hit_chance_factor = 0.15
				resistance_damage_to_garrison_on_our_occupied_states = 0.10
			}
		}

		free_states_idea_graveyard_warfare_doctrine = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea free_states_idea_graveyard_warfare_doctrine" }
			picture = army_of_aggression
			allowed_civil_war = { always = yes }
			modifier = {
				autonomy_gain = 3
				max_surrender_limit_offset = 0.10
				forced_surrender_limit = 0.10
				compliance_growth_on_our_occupied_states = -0.10
				resistance_growth_on_our_occupied_states = 0.10
				resistance_decay_on_our_occupied_states = -0.25
				resistance_damage_to_garrison_on_our_occupied_states = 0.25
			}
		}

	}

}
