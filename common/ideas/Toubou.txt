ideas = {
	country = {

		TIE_pastoral_culture = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_pastoral_culture" }
			picture = agricultural_reforms

			allowed = {
				original_tag = TIE
			}

			modifier = {
				production_speed_buildings_factor = -0.50
				agriculture_tax_modifier = 1
				agricolture_productivity_modifier = 1
				high_unemployment_threshold_modifier = 0.15
			}

		}

		TIE_soborom_hot_springs = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_soborom_hot_springs" }
			picture = health_care2

			allowed = {
				original_tag = TIE
			}

			modifier = {
				health_cost_multiplier_modifier = -0.15
			}

		}

		TIE_clan_structure = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_clan_structure" }
			picture = disjointed_government

			allowed = {
				original_tag = TIE
			}

			modifier = {
				political_power_factor = -0.10
				drift_defence_factor = -0.10
				war_support_factor = 0.10
				conscription_factor = 0.25
			}

		}

		TIE_clan_structure_reduced = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_clan_structure_reduced" }
			picture = disjointed_government
			name = TIE_clan_structure

			allowed = {
				original_tag = TIE
			}

			modifier = {
				war_support_factor = 0.10
				conscription_factor = 0.25
			}

		}

		TIE_clan_structure_strengthened = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_clan_structure_strengthened" }
			picture = disjointed_government
			name = TIE_clan_structure

			allowed = {
				original_tag = TIE
			}

			modifier = {
				political_power_factor = -0.10
				drift_defence_factor = -0.10
				war_support_factor = 0.20
				conscription_factor = 0.35
			}

		}

		TIE_warrior_spirit = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_warrior_spirit" }
			picture = national_defenders

			allowed = {
				original_tag = TIE
			}

			modifier = {
				army_core_defence_factor = 0.15
			}

		}

		TIE_warrior_spirit_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_warrior_spirit_2" }
			name = TIE_warrior_spirit
			picture = national_defenders

			allowed = {
				original_tag = TIE
			}

			modifier = {
				army_core_defence_factor = 0.15
				acclimatization_hot_climate_gain_factor = 0.25
				heat_attrition_factor = -0.25
			}

		}

		TIE_sharia_law = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_sharia_law" }
			picture = salafist_outlook

			allowed = {
				original_tag = TIE
			}

			cancel = {
				NOT = { has_government = fascism }
			}

			modifier = {
				fascism_drift = 0.05
				political_power_factor = 0.05
				stability_factor = 0.05
			}

		}

		TIE_money_from_refugees = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_money_from_refugees" }
			picture = migrant_crisis_europe

			allowed = {
				original_tag = TIE
			}

			cancel = {
				NOT = { has_government = fascism }
			}

			modifier = {
				population_tax_income_multiplier_modifier = 0.10
				migration_rate_value_factor = 0.05
			}

		}

		TIE_derde = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_derde" }
			picture = semi-presidential_republic_idea

			allowed = {
				original_tag = TIE
			}

			cancel = {
				NOT = { has_government = communism }
				NOT = { has_government = nationalist }
			}

			modifier = {
				political_power_factor = 0.10
			}

		}

		TIE_derde_guards = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_derde_guards" }
			picture = semi-presidential_republic_idea
			name = TIE_derde

			allowed = {
				original_tag = TIE
			}

			cancel = {
				NOT = { has_government = communism }
				NOT = { has_government = nationalist }
			}

			modifier = {
				political_power_factor = 0.10
				army_attack_factor = 0.10
			}

		}

		TIE_punching_above = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_punching_above" }
			picture = political_support

			allowed = {
				original_tag = TIE
			}

			cancel = {
				has_government = fascism
			}

			modifier = {
				foreign_influence_modifier = 0.10
			}

		}

		TIE_clan_elders = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_clan_elders" }
			picture = low_popular_support3

			allowed = {
				original_tag = TIE
			}

			cancel = {
				NOT = { has_government = democratic }
				NOT = { has_government = neutrality }
			}

			modifier = {
				stability_factor = 0.10
			}

		}

		TIE_natron = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_natron" }
			picture = idea_SWE_hungershield

			allowed = {
				original_tag = TIE
			}

			modifier = {
				corporate_tax_income_multiplier_modifier = 0.10
			}

		}

		TIE_trans_saharan_trade_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_trans_saharan_trade_1" }
			picture = trade

			allowed = {
				original_tag = TIE
			}

			modifier = {
				productivity_growth_modifier = 0.15
			}

		}

		TIE_trans_saharan_trade_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_trans_saharan_trade_2" }
			picture = trade

			name = TIE_trans_saharan_trade_1

			allowed = {
				original_tag = TIE
			}

			modifier = {
				productivity_growth_modifier = 0.15
				resource_export_multiplier_modifier = 0.5
			}

		}

		TIE_trans_saharan_trade_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_trans_saharan_trade_3" }
			picture = trade

			name = TIE_trans_saharan_trade_1

			allowed = {
				original_tag = TIE
			}

			modifier = {
				productivity_growth_modifier = 0.15
				resource_export_multiplier_modifier = 0.10
			}

		}

		TIE_trans_saharan_trade_4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TIE_trans_saharan_trade_4" }
			picture = trade

			name = TIE_trans_saharan_trade_1

			allowed = {
				original_tag = TIE
			}

			modifier = {
				productivity_growth_modifier = 0.15
				resource_export_multiplier_modifier = 0.15
			}

		}

	}

}