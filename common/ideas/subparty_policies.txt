ideas = {

	hidden_ideas = {

		#For communists
		IP_spread_the_revolution = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_spread_the_revolution" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 4 }	#Communist-State
					is_in_array = { ruling_party = 19 }	#Neutral_Communism
				}
			}

			modifier = {
				boost_ideology_mission_factor = 0.25
			}
		}
		IP_hard_labour = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_hard_labour" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 4 }	#Communist-State
					is_in_array = { ruling_party = 19 }	#Neutral_Communism
				}
			}

			modifier = {
				buildings_worker_requirement_modifier = 0.25
			}
		}

		#For socialists
		IP_welfare_state = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_welfare_state" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 17 }	#Neutral_green
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				health_cost_multiplier_modifier = -0.15
				social_cost_multiplier_modifier = -0.15
			}
		}
		IP_economic_interventionism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_economic_interventionism" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				internal_investments_pp_cost_modifier = -0.25
				internal_investments_money_cost_modifier = -0.25
			}
		}
		IP_economic_interventionism_green = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_economic_interventionism" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 17 }	#Neutral_green
				}
			}

			modifier = {
				production_speed_synthetic_refinery_factor = 0.50
			}
		}
		IP_peaceful_diplomacy_isolation_bonus_green = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_peaceful_diplomacy_isolation_bonus_green" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 17 }	#Neutral_green
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				political_power_factor = 0.30
			}
		}
		IP_peaceful_diplomacy_local_security_bonus_green = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_peaceful_diplomacy_local_security_bonus_green" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 17 }	#Neutral_green
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				political_power_factor = 0.15
			}
		}
		IP_peaceful_diplomacy_limited_interventionism_bonus_green = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_peaceful_diplomacy_limited_interventionism_bonus_green" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 17 }	#Neutral_green
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				political_power_factor = 0
			}
		}
		IP_peaceful_diplomacy_regional_interventionism_penalty_green = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_peaceful_diplomacy_regional_interventionism_penalty_green" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 17 }	#Neutral_green
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				political_power_factor = -0.15
			}
		}
		IP_peaceful_diplomacy_global_interventionism_penalty_green = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_peaceful_diplomacy_global_interventionism_penalty_green" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 17 }	#Neutral_green
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				political_power_factor = -0.30
			}
		}
		IP_peaceful_diplomacy_neoimperialism_penalty_green = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_peaceful_diplomacy_neoimperialism_penalty_green" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 17 }	#Neutral_green
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				political_power_factor = -0.45
			}
		}
		IP_naive_worldview = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea naive_worldview" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 3 }	#socialsim
					is_in_array = { ruling_party = 5 }	#anarchist_communism
					is_in_array = { ruling_party = 17 }	#Neutral_green
					is_in_array = { ruling_party = 18 }	#neutral_Social
				}
			}

			modifier = {
				foreign_influence_defense_modifier = -0.15
			}
		}

		#For conservatives
		IP_good_old_days = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_good_old_days" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 1 }	#conservative
					is_in_array = { ruling_party = 6 }	#Conservative
					is_in_array = { ruling_party = 8 }	#Mod vilayat e faqih
					is_in_array = { ruling_party = 12 }	#muslim brotherhood
					is_in_array = { ruling_party = 14 }	#neutral conservative
					is_in_array = { ruling_party = 20 }	#national populist
				}
			}

			modifier = {
				foreign_influence_defense_modifier = 0.15
			}
		}
		IP_stable_society = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_stable_society" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 1 }	#conservative
					is_in_array = { ruling_party = 6 }	#Conservative
					is_in_array = { ruling_party = 8 }	#Mod vilayat e faqih
					is_in_array = { ruling_party = 12 }	#muslim brotherhood
					is_in_array = { ruling_party = 14 }	#neutral conservative
					is_in_array = { ruling_party = 20 }	#national populist
				}
			}

			modifier = {
				stability_factor = 0.15
			}
		}
		IP_patriotism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_patriotism" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 1 }	#conservative
					is_in_array = { ruling_party = 6 }	#Conservative
					is_in_array = { ruling_party = 14 }	#neutral conservative
					is_in_array = { ruling_party = 20 }	#national populist
				}
			}

			modifier = {
				war_support_factor = 0.15
			}
		}
		IP_patriotism_religious = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_patriotism_religious" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 8 }	#Mod vilayat e faqih
					is_in_array = { ruling_party = 12 }	#muslim brotherhood
				}
			}

			modifier = {
				compliance_growth = 0.15
			}
		}
		IP_homefront_first = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_homefront_first" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 1 }	#conservative
					is_in_array = { ruling_party = 6 }	#Conservative
					is_in_array = { ruling_party = 8 }	#Mod vilayat e faqih
					is_in_array = { ruling_party = 12 }	#muslim brotherhood
					is_in_array = { ruling_party = 14 }	#neutral conservative
					is_in_array = { ruling_party = 20 }	#national populist
				}
			}

			modifier = {
				foreign_influence_modifier = -0.15
			}
		}

		#For liberals
		IP_high_risk_investment = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_high_risk_investment" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					has_country_leader_with_trait = western_technocrat
					is_in_array = { ruling_party = 2 }	#liberalism
					is_in_array = { ruling_party = 16 }	#Neutral_Libertarian
				}
			}

			modifier = {
				return_on_investment_modifier = 0.02
			}
		}
		IP_invisible_hand = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_invisible_hand" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					has_country_leader_with_trait = western_technocrat
					is_in_array = { ruling_party = 2 }	#liberalism
					is_in_array = { ruling_party = 16 }	#Neutral_Libertarian
				}
			}

			modifier = {
				economic_cycles_cost_factor = -0.15
				econ_cycle_upg_cost_multiplier_modifier = -0.15
				productivity_growth_modifier = 0.75
			}
		}
		IP_business_friendly_environment = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_business_friendly_environment" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					has_country_leader_with_trait = western_technocrat
					is_in_array = { ruling_party = 2 }	#liberalism
					is_in_array = { ruling_party = 16 }	#Neutral_Libertarian
				}
			}

			modifier = {
				corporate_tax_income_multiplier_modifier = 0.10
			}
		}
		IP_anti_military = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_anti_military" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					has_country_leader_with_trait = western_technocrat
					is_in_array = { ruling_party = 2 }	#liberalism
					is_in_array = { ruling_party = 16 }	#Neutral_Libertarian
				}
			}

			modifier = {
				industrial_capacity_factory = -0.15
				industrial_capacity_dockyard = -0.15
			}
		}

		#For autocrats
		IP_paralyzing_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_paralyzing_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 2.7
			}
		}
		IP_crippling_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_crippling_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 2.4
			}
		}
		IP_rampant_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_rampant_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 2.1
			}
		}
		IP_unrestrained_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_unrestrained_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 1.8
			}
		}
		IP_systematic_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_systematic_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 1.5
			}
		}
		IP_widespread_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_widespread_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 1.2
			}
		}
		IP_medium_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_medium_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 0.9
			}
		}
		IP_modest_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_modest_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 0.6
			}
		}
		IP_slight_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_slight_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 0.3
			}
		}
		IP_negligible_corruption_pp_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_negligible_corruption_pp_bonus" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascist
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				political_power_gain = 0
			}
		}
		IP_good_connections = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_good_connections" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 15 }	#oligarchs
				}
			}

			modifier = {
				resource_export_multiplier_modifier = 0.10
			}
		}
		IP_emergency_powers = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_emergency_powers" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				party_popularity_stability_factor = 0.25
			}
		}
		IP_yes_men_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_yes_men_army" }

			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					non_technocrats_western_autocrats_are_in_power = yes
					is_in_array = { ruling_party = 7 }	#Emerging autocrats
					is_in_array = { ruling_party = 13 }	#Neutral autocrats
					is_in_array = { ruling_party = 15 }	#oligarchs
					is_in_array = { ruling_party = 21 }	#fascists
					is_in_array = { ruling_party = 22 }	#military junta
				}
			}

			modifier = {
				experience_gain_army_factor = -0.15
				experience_gain_navy_factor = -0.15
				experience_gain_air_factor = -0.15
			}
		}

		#For monarchists
		IP_keep_it_in_the_family = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_keep_it_in_the_family" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 10 }	#Kingdom
					is_in_array = { ruling_party = 23 }	#Monarchist
				}
			}

			modifier = {
				corruption_cost_factor = -0.15
				economic_cycles_cost_factor = -0.15
				internal_factions_cost_factor = -0.15
				bureaucracy_cost_factor = -0.15
				Military_Spending_cost_factor = -0.15
				crime_fighting_cost_factor = -0.15
				education_budget_cost_factor = -0.15
				health_budget_cost_factor = -0.15
				social_budget_cost_factor = -0.15
				trade_laws_cost_factor = -0.15
				Nuclear_Status_cost_factor = -0.15
				Nuclear_Stance_cost_factor = -0.15
				Conscription_Law_cost_factor = -0.15
				Military_Status_Women_cost_factor = -0.15
				Foreign_Intervention_Law_cost_factor = -0.15
				Officer_Training_Law_cost_factor = -0.15
				materiel_manufacturer_cost_factor = -0.15
				tank_manufacturer_cost_factor = -0.15
				aircraft_manufacturer_cost_factor = -0.15
				naval_manufacturer_cost_factor = -0.15
				tax_rate_change_multiplier_modifier = -0.15
				migration_laws_cost_factor = -0.15
			}
		}
		IP_for_king_and_queen = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_for_king_and_queen" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 10 }	#Kingdom
					is_in_array = { ruling_party = 23 }	#Monarchist
				}
			}

			modifier = {
				army_core_attack_factor = 0.10
				army_core_defence_factor = 0.10
			}
		}
		IP_crown_estate = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_crown_estate" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 10 }	#Kingdom
					is_in_array = { ruling_party = 23 }	#Monarchist
				}
			}

			modifier = {
				population_tax_income_multiplier_modifier = 0.10
			}
		}
		IP_outdated_ideals = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_outdated_ideals" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 10 }	#Kingdom
					is_in_array = { ruling_party = 23 }	#Monarchist
				}
			}

			modifier = {
				stability_factor = -0.15
			}
		}

		#For fundamentalists
		IP_religious_law_emerging = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_religious_law_emerging" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 9 }	#Vilayat_e_Faqih
				}
			}

			modifier = {
				communism_drift = 0.10
			}
		}
		IP_religious_law_salafist = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_religious_law_salafist" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 11 }	#Caliphate
				}
			}

			modifier = {
				fascism_drift = 0.10
			}
		}
		IP_recruitment_campaign = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_recruitment_campaign" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 9 }	#Vilayat_e_Faqih
					is_in_array = { ruling_party = 11 }	#Caliphate
				}
			}

			modifier = {
				weekly_manpower = 300
			}
		}
		IP_nationalist_liberation_movements = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_nationalist_liberation_movements" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 9 }	#Vilayat_e_Faqih
					is_in_array = { ruling_party = 11 }	#Caliphate
				}
			}

			modifier = {
				send_volunteers_tension = -0.10
				lend_lease_tension = -0.10
				send_volunteer_size = 2
			}
		}
		IP_international_pariah = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_international_pariah" }
			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 9 }	#Vilayat_e_Faqih
					is_in_array = { ruling_party = 11 }	#Caliphate
				}
			}

			modifier = {
				min_export = -0.5
			}
		}
	}

	country = {
		IP_paramilitary_units_formed = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IP_paramilitary_units_formed" }

			picture = reserve_divisions

			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = yes
			}

			cancel = {
				NOT = {
					is_in_array = { ruling_party = 21 }	#fascists
				}
			}

			modifier = {
				conscription = 0.005
				training_time_factor = -0.10
			}
		}
	}
}