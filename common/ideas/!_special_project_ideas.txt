ideas = {
	hidden_ideas = {
		sp_fusion_reactors_special_project_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_fusion_reactors_special_project_idea" }
			modifier = {
				nuclear_fuel_consumption_modifier = -0.30
				nuclear_energy_generation_modifier = 0.25
			}
		}

		sp_improved_nuclear_reactor_centrifuge_technology_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_improved_nuclear_reactor_centrifuge_technology_idea" }
			modifier = {
				nuclear_energy_generation_modifier = 0.06
			}
		}

		sp_improved_based_solar_farms_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_improved_based_solar_farms_idea" }
			modifier = {
				renewable_energy_gain_multiplier = 0.20
			}
		}

		sp_improved_space_solar_farms_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_improved_based_solar_farms_idea" }
			modifier = {
				renewable_energy_gain_multiplier = 0.20
			}
		}

		sp_construction_exoskeletons_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_construction_exoskeletons_idea" }
			modifier = {
				production_speed_buildings_factor = 0.05
				civilian_factories_productivity = 0.10
				civ_facs_worker_requirement_modifier = -0.05
			}
		}

		sp_atomspheric_water_recycling_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_atomspheric_water_recycling_idea" }
			modifier = {
				agricolture_productivity_modifier = 0.15
			}
		}

		sp_maglev_trains_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_maglev_trains_idea" }
			modifier = {
				country_productivity_growth_modifier = 0.30
			}
			equipment_bonus = {
				train_equipment = {
					build_cost_ic = 3
				}
			}
		}

		sp_thorium_nuclear_fuel_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_improved_nuclear_reactor_centrifuge_technology_idea" }
			modifier = {
				nuclear_fuel_consumption_modifier = -0.15
				nuclear_energy_generation_modifier = 0.10
			}
		}

		sp_wireless_power_grid_expansion_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_wireless_power_grid_expansion" }
			modifier = {
				production_speed_fossil_powerplant_factor = -0.10
				fossil_pp_energy_generation_modifier = 0.10
			}
		}

		sp_improved_synthetic_diamonds_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea sp_improved_synthetic_diamonds_idea" }
			modifier = {
				country_resource_chromium = 30
			}
		}
	}
}