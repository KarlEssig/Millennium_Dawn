ideas = {

	hidden_ideas = {
		three_months_of_war = {
			allowed_civil_war = { always = yes }
			removal_cost = -1
			modifier = {
				#war_support_factor = 0
			}
		}
		six_months_of_war = {
			allowed_civil_war = { always = yes }
			removal_cost = -1
			modifier = {
				#war_support_factor = 0
			}
		}
		nine_months_of_war = {
			allowed_civil_war = { always = yes }
			removal_cost = -1
			modifier = {
				#war_support_factor = 0
			}
		}
		twelve_months_of_war = {
			allowed_civil_war = { always = yes }
			removal_cost = -1
			modifier = {
				#war_support_factor = 0
			}
		}
		recent_ceasefire_attempt = {
			allowed_civil_war = { always = yes }
			removal_cost = -1
		}
	}
}