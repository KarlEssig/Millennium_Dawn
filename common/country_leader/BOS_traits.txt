leader_traits = {
	Architect_of_Bosnian_Independence = {
		random = no
		political_power_factor = 0.05
		conscription = 0.02
		army_morale_Factor = 0.10
		army_core_defence_factor = 0.05
		ai_will_do = {
			factor = 1
		}
	}
	Controversial_Bosniak_Statesman = {
		random = no
		corruption_cost_factor = 0.30
		custom_modifier_tooltip = BOS_serbians_dislike
		#hidden_modifier = {
		#targeted_modifier = {
		#	tag = RSK
		#	defense_bonus_against = -0.5
		#}
		#}
		ai_will_do = {
			factor = 1
		}
	}
	#Anti-Corruption Crusader
	Fierce_Defender_of_Republika_Srpska = {
		random = no
		corruption_cost_factor = 0.35
		war_support_factor = 0.15
		trade_laws_cost_factor = -0.25
		#mobilization_laws_cost_factor = -0.25
		#economy_cost_factor = -0.25
		#high_command_cost_factor = -0.25
		air_chief_cost_factor = -0.25
		army_chief_cost_factor = -0.25
		navy_chief_cost_factor = -0.25
		nationalist_acceptance = 50
		conscription = 0.01
		custom_modifier_tooltip = BOS_Bosniak_dislike

		ai_strategy = {
			type = befriend
			id = "SER"
			value = 100
		}

		#hidden_modifier = {
		#targeted_modifier = {
		#	tag = BOS
		#	defense_bonus_against = -0.5
		#}
		#}

		ai_will_do = {
			factor = 1
		}
	}
	Anti_Corruption_Crusader = {
		random = no
		corruption_cost_factor = -0.10
		war_support_factor = 0.05
		political_power_factor = 0.05
		nationalist_acceptance = -50

		ai_will_do = {
			factor = 1
		}
	}
	VOTED_FOR_TRUMP = {
		random = no
		custom_modifier_tooltip = bird_loves_constants
		custom_modifier_tooltip = stole_my_dog
		custom_modifier_tooltip = double_teamed_droids_mother
		custom_modifier_tooltip = gets_dominated
		nationalist_acceptance = 50
		health_cost_multiplier_modifier = 0.15
		custom_modifier_tooltip = big_brain_of_the_git
		research_speed_factor = 0.05

		ai_will_do = {
			factor = 1
		}
	}
	defender_of_Bosniak_interests = {
		random = no
		#economy_cost_factor = -0.25
		justify_war_goal_time = -0.25
		political_power_factor = 0.2

		ai_will_do = {
			factor = 1
		}
	}
	politica_cronyism = {
		random = no
		#economy_cost_factor = 0.25
		consumer_goods_factor = 0.15

		ai_will_do = {
			factor = 1
		}
	}
	undermine_democratic_principles = {
		random = no
		stability_factor = -0.05
		corruption_cost_factor = 0.80
		nationalist_acceptance = 50
		political_power_factor = -0.15

		ai_will_do = {
			factor = 1
		}
	}
	One_Armed_Activist = {
		random = no
		stability_factor = 0.05
		corruption_cost_factor = -0.10
		nationalist_acceptance = -50
		foreign_influence_modifier = 0.1

		ai_will_do = {
			factor = 1
		}
	}
	The_Sarajevo_Rose = {
		random = no
		stability_factor = 0.05
		education_cost_multiplier_modifier = -0.05
		health_cost_multiplier_modifier = -0.05
		social_cost_multiplier_modifier = -0.05

		ai_will_do = {
			factor = 1
		}
	}
	Bosnia_s_Bridge_Builder = {
		random = no
		corruption_cost_factor = -0.10
		research_speed_factor = 0.1
		personnel_cost_multiplier_modifier = -0.05
		production_speed_industrial_complex_factor = 0.1

		ai_will_do = {
			factor = 1
		}
	}
	Divisive_Nationalist = {
	random = no
		stability_factor = 0.05
		corruption_cost_factor = -0.10
		nationalist_acceptance = 50
		police_cost_multiplier_modifier = -0.05

		ai_will_do = {
			factor = 1
		}
	}
	Versatile_Visionary = {
		random = no
		#production_speed_buildings_factor = 0.05
		production_speed_offices_factor = 0.05
		econ_cycle_upg_cost_multiplier_modifier = -0.1
		production_speed_industrial_complex_factor = 0.1

			ai_will_do = {
				factor = 1
			}
	}
	Pragmatic_Perfectionist = {
		random = no
			opinion_gain_monthly_same_ideology_factor = 1.0
			production_speed_offices_factor = 0.10

			ai_will_do = {
				factor = 1
			}
	}
	The_Tenacious_Titan = {
		random = no
			personnel_cost_multiplier_modifier = -0.05
			trade_opinion_factor = 0.10

			ai_will_do = {
				factor = 1
			}
	}
	The_Fearless_Fighter = {
		random = no
			command_power_gain_mult = 0.05
			army_core_defence_factor = 0.05
			production_speed_arms_factory_factor = 0.1
			ai_will_do = {
				factor = 1
			}
	}
	Unwavering_Patriot = {
		random = no
			stability_factor = 0.05
			production_speed_industrial_complex_factor = 0.02

			ai_will_do = {
				factor = 1
			}
	}
	From_the_Front_Lines_to_the_Halls_of_Power = {
		random = no
			command_power_gain_mult = 0.05
			nationalist_acceptance = 50
			army_core_defence_factor = 0.05
			production_speed_bunker_factor = 0.2
			production_speed_coastal_bunker_factor = 0.2
			production_speed_anti_air_building_factor = 0.2

			ai_will_do = {
				factor = 1
			}
	}
	Against_the_Grain = {
		random = no
			interest_rate_multiplier_modifier = -2
			neutrality_drift = 0.02

			ai_will_do = {
				factor = 1
			}
	}
	Eye_of_the_Storm = {
		random = no
			stability_factor = 0.05
			corruption_cost_factor = -0.10
			nationalist_acceptance = 50

			ai_will_do = {
				factor = 1
			}
	}
	Steadfast_Protector = {
		random = no
			stability_factor = 0.05
			corruption_cost_factor = -0.10
			nationalist_acceptance = 50

			ai_strategy = {
				type = befriend
				id = "SER"
				value = 100
			}

			ai_will_do = {
				factor = 1
			}
	}
	Uncompromising_Leader = {
		random = no
			stability_factor = 0.05
			corruption_cost_factor = 0.10
			nationalist_acceptance = 50
			opinion_gain_monthly_factor = -0.15
			drift_defence_factor = 0.50

			ai_will_do = {
				factor = 1
			}
	}
	The_Enforcer = {
		random = no
			resistance_damage_to_garrison = -0.25
			foreign_subversive_activites = -0.3

			ai_will_do = {
				factor = 1
			}
	}
	Bosnian_political_reformer = {
		random = no
			interest_rate_multiplier_modifier = -2
			education_cost_multiplier_modifier = -0.05
			health_cost_multiplier_modifier = -0.05
			social_cost_multiplier_modifier = -0.05

			ai_will_do = {
				factor = 1
			}
	}
	Bosnian_cooperator = {
		random = no
			personnel_cost_multiplier_modifier = -0.05
			production_speed_industrial_complex_factor = 0.1
		
			ai_will_do = {
				factor = 1
			}
	}
}