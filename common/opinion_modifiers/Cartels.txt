opinion_modifiers = {

	random_celebrity_death_opinion = {
		value = -25
		decay = 1
	}

	random_oligarchy_opinion = {
		value = -50
		decay = -3
	}

	random_denied_our_demands = {
		value = -25
		decay = 1
	}

	seized_a_contraband_ship = {
		value = -15
		decay = 1
	}

	seized_a_contraband_container = {
		value = -5
		decay = 1
	}
}