opinion_modifiers = {
	opened_eu_talks = {
		value = 25
	}
	improved_minority_rights = {
		value = 25
	}
	eu_work_contracts = {
		trade = yes
		value = 35
	}
	usa_rom_trade = {
		trade = yes
		value = 25
	}
}