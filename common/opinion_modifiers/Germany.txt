#########################################################################
# OPINION MODIFIERS
##########################################################################
# value
# min_trust
# max_trust
# decay
# months/years/days = timer
# trade = yes/no
opinion_modifiers = {

	franco_german_friendship = {
		value = 25
	}
	german_dutch_military_cooperation = {		#German/Dutch Corps
		value = 20
	}
	german_trade_policy = {
		value = 25
	}
	german_trade_policy_trade = {
		value = 25
		trade = yes
	}
	nord_stream = {
		value = 25
	}
	approval_for_nord_stream = {
		value = 10
	}
	opposition_to_nord_stream = {
		value = -20
	}
	great_opposition_to_nord_stream = {
		value = -40
	}
	demand_maastricht_amendments_positive = {
		value = 10
	}
	demand_maastricht_amendments_negative = {
		value = -10
	}

	#TRADE OPINION
	trade_negative = {
		trade = yes
		value = -10
	}
	great_trade_negative = {
		trade = yes
		value = -20
	}
	trade_positive = {
		trade = yes
		value = 10
	}
	great_trade_positive = {
		trade = yes
		value = 20
	}

	#GENERAL OPINION
	GER_great_negative_opinion = {
		value = -20
	}
	GER_slight_negative_opinion = {
		value = -10
	}
	GER_very_slight_negative_opinion = {
		value = -5
	}
	GER_very_slight_positive_opinion = {
		value = 5
	}
	GER_slight_positive_opinion = {
		value = 10
	}
	GER_great_positive_opinion = {
		value = 20
	}
	GER_breach_of_contract_opinion_I = {
		value = -80
		months = 80
		decay = 0.3
	}
	GER_breach_of_contract_opinion_II = {
		value = -50
		months = 50
		decay = 0.3
	}
	GER_breach_of_contract_opinion_III = {
		value = -20
		months = 20
		decay = 0.3
	}
	GER_prefered_the_other_negative_opinion = {
		value = -5
	}
	GER_leave_Project = {
		value = -10
		decay = 0.25
	}
	GER_join_Project = {
		value = 10
		decay = 0.25
	}
	GER_strategic_partnership = {
		value = 35
	}
	helmut_hofer_injailed = {
		value = -10
	}
	Mykonoss_tafel = {
		value = -10
	}
	GER_Legacy_mod = {
		value = 10
	}
	GER_blame_for_BSE = {
		value = -10
		decay = 0.25
	}
	GER_invite_to_garisson = {
		value = 12
		months = 24
		decay = 0.5
	}
	GER_Rejected_Investments = {
		value = -15
		months = 36
		decay = 0.3
	}
	GER_State_Visit = {
		value = 25
		months = 36
		decay = 0.3
	}
}