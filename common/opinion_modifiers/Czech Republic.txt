opinion_modifiers = {
	CZE_skoda_volkswagen_cooperation = {
		value = 25
	}

	CZE_expand_middle_east_trade_modifier = {
		value = 30
		trade = yes
	}

	CZE_skoda_auto_volkswagen_india_modifier = {
		value = 30
		trade = yes
	}

	CZE_usa_dislikes_volkswagen_modifier = {
		value = -15
	}

	CZE_skoda_prepares_to_strike = {
		value = -15
	}

	CZE_skoda_makes_a_plan = {
		value = 15
	}

	CZE_government_helps_skoda = {
		value = -25
	}

	CZE_liberalization_practise = {
		value = 20
	}

	CZE_czech_opens_new_trade_routes = {
		value = 20
		trade = yes
	}

	CZE_reaches_for_our_market = {
		value = 25
		trade = yes
	}

	CZE_czech_restricts_borders = {
		value = -40
	}

	CZE_dont_trust_democracy_modifier = {
		value = 15
	}

	CZE_kscm_opens_backstage = {
		value = 20
	}

	CZE_czech_opens_new_trade_routes = {
		value = 25
		trade = yes
	}

	CZE_asia_sphere = {
		value = 20
	}

	CZE_limited_trust = {
		value = -20
	}

	CZE_attacks_our_government = {
		value = -45
	}

	CZE_kscm_speaks_against_us = {
		value = -20
	}

	CZE_limits_eu_trust = {
		value = -10
	}

	CZE_nato_relations = {
		value = 15
	}

	CZE_czech_republic_gets_closer_to_eu = {
		value = 15
	}

	CZE_nato_protection = {
		value = 15
	}

	CZE_british_relations_improved = {
		value = 20
	}

	CZE_british_relations_improved2 = {
		value = 10
	}

	CZE_czech_republic_handle_over_the_prince = {
		value = 60
	}

	CZE_czech_republic_kept_the_prince = {
		value = -60
	}

	CZE_czech_talks_negative_about_communism = {
		 value = -20
	}

	CZE_petr_pavel_completed_mission = {
		value = 25
	}

	CZE_petr_pavel_helps_afghanistan = {
		value = -50
	}

	CZE_petr_pavel_completed_mission_two = {
		value = 20
	}

	CZE_petr_pavel_completed_mission_three = {
		value = 15
	}

	CZE_petr_pavel_completed_mission_four = {
		value = 20
	}

	CZE_petr_pavel_completed_mission_five = {
		value = 35
	}

	CZE_petr_pavel_completed_mission_six = {
		value = 20
	}

	CZE_petr_pavel_completed_mission_seven = {
		value = 15
	}

	CZE_petr_pavel_completed_mission_eight = {
		value = 15
	}

	CZE_petr_pavel_completed_mission_nine = {
		value = 35
	}

	CZE_petr_pavel_completed_mission_ten = {
		value = 35
	}

	CZE_petr_pavel_completed_mission_eleven = {
		value = 15
	}

	CZE_petr_pavel_international_career = {
		value = 15
	}

	CZE_czech_helps_with_iraq = {
		value = 25
	}

	CZE_helps_in_second_gulf_war = {
		value = 15
	}

	CZE_czech_warms_relations = {
		value = 25
	}

	CZE_petr_pavel_supports_our_group = {
		value = 15
	}

	CZE_supports_our_efforts = {
		value = 20
	}

	CZE_antagonize_us = {
		value = -30
	}

	CZE_petr_pavel_finished_career = {
		value = 50
	}

	CZE_takes_more_independent_step = {
		value = -25
	}

	CZE_is_interested_in_our_doctrine = {
		value = -20
	}

	CZE_new_president_takes_action = {
		value = 15
	}

	CZE_stays_friendly = {
		value = 15
	}

	CZE_improves_military_relations = {
		value = 30
	}

	CZE_makes_our_countries_closer = {
		value = 25
	}

	CZE_buys_our_equipment = {
		value = 10
	}

	CZE_SLO_future_plans = {
		value = 15
	}

	CZE_SLO_having_fun_together = {
		value = 3
	}

	CZE_SLO_chosen_peaceful_path = {
		value = 25
	}

	CZE_SLO_aggressive_unification = {
		value = -35
	}

	CZE_petr_pavel_completed_mission_twelve = {
		value = -40
	}

	CZE_petr_pavel_completed_mission_thirteen = {
		value = 20
	}

	CZE_czech_warms_relations_two = {
		value = 20
	}

	CZE_czech_brings_our_governemnts_closer = {
		value = 25
	}

	CZE_czech_improves_relations_with_iraq = {
		value = -30
	}
}