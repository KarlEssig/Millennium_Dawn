equipments = {
	attack_helicopter_hull = {
		year = 1965
		is_archetype = yes
		is_buildable = no

		picture = archetype_modern_tank_equipment
		type = armor
		group_by = archetype
		interface_category = interface_category_armor
		priority = 2000

		module_slots = {
			helicopter_inner_hardpoint = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
				}
			}
			nose_gun_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_light_nose_gun
					helicopter_medium_nose_gun
					helicopter_heavy_nose_gun
				}
			}
			armor_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_armor_type
				}
			}
			engine_type_slot = {
				required = yes
				allowed_module_categories = {
					helicopter_turbine_engine_type
					helicopter_piston_engine_type
				}
			}
			defence_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
			sensor_and_avionics_slot = {
				required = no
				allowed_module_categories = {
					helicopter_sensor_and_avionics_type
				}
			}
			helicopter_middle_hardpoint_1 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
		}

		module_count_limit = {
			category = helicopter_stealth_module
			count < 2
		}
		module_count_limit = {
			category = helicopter_active_defence_type
			count < 2
		}
		module_count_limit = {
			category = helicopter_em_lock_type
			count < 2
		}
		module_count_limit = {
			category = helicopter_heavy_atgm_type
			count < 2
		}
		module_count_limit = {
			category = helicopter_multiple_atgm_type
			count < 2
		}

		default_modules = {
			helicopter_inner_hardpoint = empty
			nose_gun_type_slot = empty
			armor_type_slot = empty
			engine_type_slot = empty
			defence_type_slot = empty
			sensor_and_avionics_slot = empty
			helicopter_middle_hardpoint_1 = empty
		}

		fuel_consumption = 0
		maximum_speed = 4
		build_cost_ic = 4
		reliability = 0.5
		hardness = 0.15
		armor_value = 0
		lend_lease_cost = 5
		resources = {
			aluminium = 1
		}

		manpower = 3
	}

	#1965
	attack_helicopter_hull_0 = {
		abbreviation = "ahh0"
		derived_variant_name = ahh_equipment_0
		visual_level = 0
		year = 1965
		archetype = attack_helicopter_hull
		priority = 2000
		module_slots = inherit
		lend_lease_cost = 5

		upgrades = {
			helicopter_nsb_upgrade
		}
	}

	#1985
	attack_helicopter_hull_1 = {
		abbreviation = "ahh1"
		derived_variant_name = ahh_equipment_1
		visual_level = 1
		year = 1985
		archetype = attack_helicopter_hull
		parent = attack_helicopter_hull_0
		priority = 2000

		module_slots = {
			helicopter_inner_hardpoint = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
				}
			}
			nose_gun_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_light_nose_gun
					helicopter_medium_nose_gun
					helicopter_heavy_nose_gun
				}
			}
			armor_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_armor_type
				}
			}
			engine_type_slot = {
				required = yes
				allowed_module_categories = {
					helicopter_turbine_engine_type
					helicopter_piston_engine_type
				}
			}
			defence_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
			sensor_and_avionics_slot = {
				required = no
				allowed_module_categories = {
					helicopter_sensor_and_avionics_type
				}
			}
			helicopter_middle_hardpoint_1 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			special_defensive_type_slot_1 = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
		}

		maximum_speed = 4
		build_cost_ic = 6
		reliability = 0.6
		hardness = 0.2
		armor_value = 0
		lend_lease_cost = 5

		upgrades = {
			helicopter_nsb_upgrade
		}
	}

	#2005
	attack_helicopter_hull_2 = {
		abbreviation = "ahh2"
		derived_variant_name = ahh_equipment_2
		visual_level = 2
		year = 2005
		archetype = attack_helicopter_hull
		parent = attack_helicopter_hull_1
		priority = 2000

		module_slots = {
			helicopter_inner_hardpoint = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
				}
			}
			nose_gun_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_light_nose_gun
					helicopter_medium_nose_gun
					helicopter_heavy_nose_gun
				}
			}
			armor_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_armor_type
				}
			}
			engine_type_slot = {
				required = yes
				allowed_module_categories = {
					helicopter_turbine_engine_type
					helicopter_piston_engine_type
				}
			}
			defence_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
			sensor_and_avionics_slot = {
				required = no
				allowed_module_categories = {
					helicopter_sensor_and_avionics_type
				}
			}
			helicopter_middle_hardpoint_1 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			helicopter_middle_hardpoint_2 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			special_defensive_type_slot_1 = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
		}

		maximum_speed = 4
		build_cost_ic = 8
		reliability = 0.7
		hardness = 0.25
		armor_value = 0
		lend_lease_cost = 5

		upgrades = {
			helicopter_nsb_upgrade
		}
	}

	#2015
	attack_helicopter_hull_3 = {
		abbreviation = "ahh3"
		derived_variant_name = ahh_equipment_3
		visual_level = 3
		year = 2015
		archetype = attack_helicopter_hull
		parent = attack_helicopter_hull_2
		priority = 2000

		module_slots = {
			helicopter_inner_hardpoint = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
				}
			}
			nose_gun_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_light_nose_gun
					helicopter_medium_nose_gun
					helicopter_heavy_nose_gun
				}
			}
			armor_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_armor_type
				}
			}
			engine_type_slot = {
				required = yes
				allowed_module_categories = {
					helicopter_turbine_engine_type
					helicopter_piston_engine_type
				}
			}
			defence_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
			sensor_and_avionics_slot = {
				required = no
				allowed_module_categories = {
					helicopter_sensor_and_avionics_type
				}
			}
			helicopter_middle_hardpoint_1 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			helicopter_middle_hardpoint_2 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			helicopter_outer_hardpoint_1 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			special_defensive_type_slot_1 = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
		}
		module_count_limit = {
			category = helicopter_multiple_atgm_type
			count < 3
		}

		maximum_speed = 4
		build_cost_ic = 10
		reliability = 0.8
		hardness = 0.3
		armor_value = 0
		lend_lease_cost = 5

		upgrades = {
			helicopter_nsb_upgrade
		}
	}

	#2035
	attack_helicopter_hull_4 = {
		abbreviation = "ahh4"
		derived_variant_name = ahh_equipment_4
		visual_level = 4
		year = 2035
		archetype = attack_helicopter_hull
		parent = attack_helicopter_hull_3
		priority = 2000

		module_slots = {
			helicopter_inner_hardpoint = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
				}
			}
			nose_gun_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_light_nose_gun
					helicopter_medium_nose_gun
					helicopter_heavy_nose_gun
				}
			}
			armor_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_armor_type
				}
			}
			engine_type_slot = {
				required = yes
				allowed_module_categories = {
					helicopter_turbine_engine_type
					helicopter_piston_engine_type
				}
			}
			defence_type_slot = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
			sensor_and_avionics_slot = {
				required = no
				allowed_module_categories = {
					helicopter_sensor_and_avionics_type
				}
			}
			helicopter_middle_hardpoint_1 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			helicopter_middle_hardpoint_2 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			helicopter_outer_hardpoint_1 = {
				required = no
				allowed_module_categories = {
					helicopter_atgm_type
					helicopter_heavy_atgm_type
					helicopter_multiple_atgm_type
					helicopter_rockets_type
					helicopter_gun_pods_type
					helicopter_wing_armament
				}
			}
			special_defensive_type_slot_1 = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
			special_defensive_type_slot_2 = {
				required = no
				allowed_module_categories = {
					helicopter_defence_type
					helicopter_stealth_module
					helicopter_em_lock_type
					helicopter_active_defence_type
				}
			}
		}
		module_count_limit = {
			category = helicopter_heavy_atgm_type
			count < 3
		}
		module_count_limit = {
			category = helicopter_multiple_atgm_type
			count < 3
		}

		maximum_speed = 4
		build_cost_ic = 12
		reliability = 0.9
		hardness = 0.35
		armor_value = 0
		lend_lease_cost = 5

		upgrades = {
			helicopter_nsb_upgrade
		}
	}
}