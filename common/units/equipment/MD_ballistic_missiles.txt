#MRBM Equipment
equipments = {

	ballistic_missile_equipment = {
		year = 1955

		allow_mission_type = {
			strategic_bomber
		}

		is_archetype = yes
		is_buildable = no
		can_license = yes
		type = ballistic_missile
		group_by = type
		sprite = missile_default
		air_map_icon_frame = 9

		interface_category = interface_category_air

		reliability = 0.8

		# Air vs Navy - high damage / low hit chance / hard to hurt
		naval_strike_attack = 1.5
		naval_strike_targetting = 0.6

		#Space taken in convoy
		lend_lease_cost = 1.5

		build_cost_ic = 104
		resources = {
			aluminium = 2
			tungsten = 2
			chromium = 1
		}

		fuel_consumption = 0
	}

	ballistic_missile_equipment_1 = {
		year = 1965

		archetype = ballistic_missile_equipment
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 5760
		air_agility = 400
		air_bombing = 60
		build_cost_ic = 114
	}
	ballistic_missile_equipment_2 = {
		year = 1975

		archetype = ballistic_missile_equipment
		parent = ballistic_missile_equipment_1
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 6500
		air_agility = 400
		air_bombing = 69
		build_cost_ic = 125.84
	}
	ballistic_missile_equipment_3 = {
		year = 1985

		archetype = ballistic_missile_equipment
		parent = ballistic_missile_equipment_2
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 79.3
		build_cost_ic = 138.42
	}
	ballistic_missile_equipment_4 = {
		year = 1995

		archetype = ballistic_missile_equipment
		parent = ballistic_missile_equipment_3
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 91
		build_cost_ic = 152.26
	}
	ballistic_missile_equipment_5 = {
		year = 2005

		archetype = ballistic_missile_equipment
		parent = ballistic_missile_equipment_4
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 104
		build_cost_ic = 167.49
	}
	ballistic_missile_equipment_6 = {
		year = 2015

		archetype = ballistic_missile_equipment
		parent = ballistic_missile_equipment_5
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 120
		build_cost_ic = 184.24
	}
	ballistic_missile_equipment_7 = {
		year = 2025

		archetype = ballistic_missile_equipment
		parent = ballistic_missile_equipment_6
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 138
		build_cost_ic = 202.66
	}
	ballistic_missile_equipment_8 = {
		year = 2035

		archetype = ballistic_missile_equipment
		parent = ballistic_missile_equipment_7
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 159
		build_cost_ic = 222.93
	}
	nuclear_ballistic_missile_equipment = {
		year = 1965


		allow_mission_type = {
			drop_nuke
		}

		is_archetype = yes
		is_buildable = no
		can_license = no
		type = ballistic_missile
		group_by = type
		sprite = missile_default
		air_map_icon_frame = 9

		interface_category = interface_category_air

		reliability = 0.8

		# Air vs Navy - high damage / low hit chance / hard to hurt
		naval_strike_attack = 1.5
		naval_strike_targetting = 0.6

		#Space taken in convoy
		lend_lease_cost = 1.5

		build_cost_ic = 104
		resources = {
			aluminium = 2
			tungsten = 2
			chromium = 1
		}

		fuel_consumption = 0
	}

	nuclear_ballistic_missile_equipment_1 = {
		year = 1975

		archetype = nuclear_ballistic_missile_equipment
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 5760
		air_agility = 10
		air_bombing = 450
		build_cost_ic = 114
	}
	nuclear_ballistic_missile_equipment_2 = {
		year = 1985

		archetype = nuclear_ballistic_missile_equipment
		parent = nuclear_ballistic_missile_equipment_1
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 6500
		air_agility = 10
		air_bombing = 500
		build_cost_ic = 125.84
	}
	nuclear_ballistic_missile_equipment_3 = {
		year = 1995

		archetype = nuclear_ballistic_missile_equipment
		parent = nuclear_ballistic_missile_equipment_2
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 500
		build_cost_ic = 138.42
	}
	nuclear_ballistic_missile_equipment_4 = {
		year = 2005

		archetype = nuclear_ballistic_missile_equipment
		parent = nuclear_ballistic_missile_equipment_3
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 500
		build_cost_ic = 152.26
	}
	nuclear_ballistic_missile_equipment_5 = {
		year = 2015

		archetype = nuclear_ballistic_missile_equipment
		parent = nuclear_ballistic_missile_equipment_4
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 500
		build_cost_ic = 167.49
	}
	nuclear_ballistic_missile_equipment_6 = {
		year = 2025

		archetype = nuclear_ballistic_missile_equipment
		parent = nuclear_ballistic_missile_equipment_5
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 500
		build_cost_ic = 184.24
	}
	nuclear_ballistic_missile_equipment_7 = {
		year = 2035

		archetype = nuclear_ballistic_missile_equipment
		parent = nuclear_ballistic_missile_equipment_6
		is_buildable = yes
		one_use_only = yes
		sprite = missile_default

		air_range = 5500
		maximum_speed = 9999
		air_agility = 10
		air_bombing = 500
		build_cost_ic = 202.66
	}
	# #SUBMARINE LAUNCHED
	# SLBM_missile_equipment = {
	# 	year = 1965

	# 	allow_mission_type = {
	# 		drop_nuke
	# 	}

	# 	is_archetype = yes
	# 	is_buildable = no
	# 	can_license = no
	# 	type = nuclear_missile
	# 	group_by = type
	# 	sprite = missile_default
	# 	air_map_icon_frame = 9

	# 	interface_category = interface_category_air

	# 	reliability = 0.8

	# 	# Air vs Navy - high damage / low hit chance / hard to hurt
	# 	naval_strike_attack = 1.5
	# 	naval_strike_targetting = 0.6

	# 	#Space taken in convoy
	# 	lend_lease_cost = 1.5

	# 	build_cost_ic = 54
	# 	resources = {
	# 		aluminium = 3
	# 		tungsten = 3
	# 	}

	# 	fuel_consumption = 0
	# }
	# SLBM_missile_equipment_1 = {
	# 	year = 1975

	# 	archetype = nuclear_ballistic_missile_equipment
	# 	is_buildable = yes
	# 	one_use_only = yes
	# 	sprite = missile_default

	# 	air_range = 5500
	# 	maximum_speed = 5760
	# 	air_agility = 10
	# 	air_bombing = 450
	# }
}