#Written by Papinian7

equipments = {

	Missile = {
		is_archetype = yes
		type = {
			support
		}
		group_by = type
		sprite = missile_default
		air_map_icon_frame = 9
		interface_category = support
		can_be_produced = {	# Optional, specifies when equipment of this category can be produced.
			always = no
		}
		active = no
		is_buildable = no
		can_license = no

	}

	GNSS1 = { archetype = Missile }
	GNSS2 = { archetype = Missile }
	GNSS3 = { archetype = Missile }
	GNSS4 = { archetype = Missile }
	GNSS5 = { archetype = Missile }
	GNSS6 = { archetype = Missile }
	GNSS7 = { archetype = Missile }
	GNSS8 = { archetype = Missile }

	COMSAT1 = { archetype = Missile }
	COMSAT2 = { archetype = Missile }
	COMSAT3 = { archetype = Missile }
	COMSAT4 = { archetype = Missile }
	COMSAT5 = { archetype = Missile }
	COMSAT6 = { archetype = Missile }
	COMSAT7 = { archetype = Missile }
	COMSAT8 = { archetype = Missile }

	SPYSAT1 = { archetype = Missile }
	SPYSAT2 = { archetype = Missile }
	SPYSAT3 = { archetype = Missile }
	SPYSAT4 = { archetype = Missile }
	SPYSAT5 = { archetype = Missile }
	SPYSAT6 = { archetype = Missile }
	SPYSAT7 = { archetype = Missile }
	SPYSAT8 = { archetype = Missile }

	KILLSAT1 = { archetype = Missile }
	KILLSAT2 = { archetype = Missile }
	KILLSAT3 = { archetype = Missile }
	KILLSAT4 = { archetype = Missile }
	KILLSAT5 = { archetype = Missile }
	KILLSAT6 = { archetype = Missile }

	OLV1 = { archetype = Missile }
	OLV2 = { archetype = Missile }
	OLV3 = { archetype = Missile }
	OLV4 = { archetype = Missile }
	OLV5 = { archetype = Missile }
	OLV6 = { archetype = Missile }
	OLV7 = { archetype = Missile }
	OLV8 = { archetype = Missile }
}
