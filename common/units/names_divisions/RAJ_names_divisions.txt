﻿RAJ_INF_01 = {
	name = "Infantry Brigades"

	for_countries = { RAJ }

	division_types = { "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d. Payadala Divhijana"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "Red Eagle Division" }
		2 = { "Ball of Fire" }
		3 = { "Dao Division" }
		4 = { "Golden Arrow Division" }
		5 = { "Khumbathang" }
		6 = { "Crossed Swords" }
		7 = { "Golden Katar" }
		8 = { "Jaisalmer" }
		9 = { "Panther Division" }
		10 = { "The Black Cat Division" }
		11 = { "Dagger Division" }
		12 = { "White Tiger Division" }
		13 = { "Strike Corps" }
		14 = { "Red Eagle Brigade" }
		15 = { "Cockerel Brigade" }
		16 = { "Sand Viper Brigade" }
		17 = { "Dah Division" }
		18 = { "Thambi Division" }
		19 = { "Red Shield Division" }
	}
}