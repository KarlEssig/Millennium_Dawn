### TRANSLATIONS ###
# Mudaraea - Armored
# Motorizeh - Bimuharikat
# Firqat - Division
# Firqa - Brigade
# Kumanduz - Commando
# Mikanikiun - Mechanized
# Mashat AlBahria - Marines
# AlMizaliyayn - Parachuters



HEZ_ARM_01 = {
	name = "Armored Brigade"

	for_countries = { HEZ }



	division_types = { "armor_Bat" }

	fallback_name = "%d. Liwa' Mudarae"

	ordered = {
		1 = { "181st Liwa' Mudarae" }
		2 = { "281st Liwa' Mudarae" }
		3 = { "381st Liwa' Mudarae" }
	}
}
#
HEZ_ARM_02 = {
	name = "Armored Division"

	for_countries = { HEZ }



	division_types = { "armor_Bat" }

	fallback_name = "%d. Al-Firqat Al-Mudaraea"

	ordered = {
		1 = { "190th Al-Firqat Al-Mudaraea" }
		2 = { "290th Al-Firqat Al-Mudaraea" }
		3 = { "390th Al-Firqat Al-Mudaraea" }
	}
}
# #
HEZ_MEC_01 = {
	name = "Mechanized Brigade"

	for_countries = { HEZ }



	division_types = { "armor_Bat" }

	fallback_name = "%d. Liwa' Mudarae"

	ordered = {
		1 = { "77th Liwa' Mudarae" }
		2 = { "84th Liwa' Mudarae" }
		3 = { "181st Liwa' Mudarae" }
	}
}
#
HEZ_MEC_02 = {
	name = "Mechanized Division"

	for_countries = { HEZ }



	division_types = { "armor_Bat" }

	fallback_name = "%d. Al-Firqat Al-Mikanikiat"

	ordered = {
		1 = { "258th Al-Firqat Al-Mikanikiat" }
		2 = { "358th Al-Firqat Al-Mikanikiat" }
		3 = { "458th Al-Firqat Al-Mikanikiat" }
	}
}
#
HEZ_INF_01 = {
	name = "Infantry Brigade"

	for_countries = { HEZ }

	division_types = { "Mot_Inf_Bat" }

	fallback_name = "%d. Liwa' Mushaa"

	ordered = {
		1 = { "221st Liwa' Mushaa" }
		2 = { "321st Liwa' Mushaa" }
		3 = { "421st Liwa' Mushaa" }
		4 = { "128th Liwa' Mushaa" }
		5 = { "228th Liwa' Mushaa" }
	}
}
#
HEZ_INF_02 = {
	name = "Infantry Division"

	for_countries = { HEZ }

	division_types = { "Mot_Inf_Bat" }

 	fallback_name = "%d. Firqat Mushaa"

	ordered = {
		1 = { "54th Firqat Mushaa" }
		2 = { "55th Firqat Mushaa" }
		3 = { "56th Firqat Mushaa" }
		4 = { "57th Firqat Mushaa" }
	}
}
#
HEZ_COM_01 = {
	name = "Commando Division"

	for_countries = { HEZ }

	division_types = { "L_Air_assault_Bat" }

	fallback_name = "%d. Firqat Al-Kumanduz"

	ordered = {
		1 = { "212nd Firqat Al-Kumanduz" }
		2 = { "312nd Firqat Al-Kumanduz" }
		3 = { "412nd Firqat Al-Kumanduz" }
	}
}
#
HEZ_COM_02 = {
	name = "Commando Brigade"

	for_countries = { HEZ }

	division_types = { "Special_Forces" }

	fallback_name = "%d. Liwa' Kumanduz"

	ordered = {
		1 = { "113th Liwa' Kumanduz" }
		2 = { "313th Liwa' Kumanduz" }
		3 = { "413th Liwa' Kumanduz" }
	}
}
#
HEZ_AIR_01 = {
	name = "Airborne Brigade"

	for_countries = { HEZ }

	division_types = { "L_Air_assault_Bat" }

	fallback_name = "%d. Liwa' Mazaliy"

	ordered = {
		1 = { "321st Liwa' Mazaliy" }
		2 = { "421st Liwa' Mazaliy" }
		3 = { "165th Liwa' Mazaliy" }
	}
}
#
HEZ_MAR_01 = {
	name = "Marine Brigade"

  	for_countries = { HEZ }

	division_types = { "L_Marine_Bat" }

	fallback_name = "%d. Liwa' Al-Bahria"

	ordered = {
		1 = { "21st Liwa' Al-Bahria" }
		2 = { "31st Liwa' Al-Bahria" }
		3 = { "41st Liwa' Al-Bahria" }
	}
}
