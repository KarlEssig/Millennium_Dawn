﻿#This list is based upon following priorities:
#1, existing BCTs, through numbered Div.s with lone BCT sized forces coming last
#2, adding recently deactivated 4th BCTs to existing Div.s - 4th/25th ID is kept separate in the airborne list. See below for why.
#3, adding 3rd BCTs to existing Div.s
#4, Div.s according to the US Army reflagging system. These only have 3 BCTs however, as the 4th BCTs were largely added after their deactivation.
#
#As Div.s today have units from all different kinds of branches, this list is organized along the rule of all units of a branch, say armored, are sorted first.
#But as the US reflaggs units, I have also copied in the other branch units in so they are under the same identity code, so they can be switched.
#
#As for infantry vs Armored for ordering of the potential reflagging Div.s, prio is amount of battalions of the branch.
#If equal, however, they are ordered according to the Div. designation.
#A few BCTs of the reflagging Div.s are high numbered and today act as training Div.s. I have included them here as combat units under the appropriate Div..
#
#Units that are separate are mountain and airborne - as they hold unique roles. This is to make the game easier. They also haven't been reflagged before.
#However, Airborne and Air Assault lists are the same, as the BCTs are both called 'Airborne'.
#
USB_INF_01 =
{
	name = "Infantry Brigade"

	for_countries = { USB }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { USA_INF_02 }

	fallback_name = "%dth Infantry Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Div/ 1st Inf BDE" }
		2 = { "1st Div/ 2nd Inf BDE" }
		3 = { "1st Div/ 3rd Inf BDE" }
		4 = { "1st Div/ 4th Inf BDE" }
		5 = { "2nd Div/ 1st Inf BDE" }
		6 = { "2nd Div/ 2nd Inf BDE" }
		7 = { "2nd Div/ 3rd Inf BDE" }
		8 = { "2nd Div/ 4th Inf BDE" }
		9 = { "3rd Div/ 1st Inf BDE" }
		10 = { "3rd Div/ 2nd Inf BDE" }
		11 = { "3rd Div/ 3rd Inf BDE" }
		12 = { "3rd Div/ 4th Inf BDE" }
		13 = { "4th Div/ 1st Inf BDE" }
		14 = { "4th Div/ 2nd Inf BDE" }
		15 = { "4th Div/ 3rd Inf BDE" }
		16 = { "4th Div/ 4th Inf BDE" }
		17 = { "5th Div/ 1st Inf BDE" }
		18 = { "5th Div/ 2nd Inf BDE" }
		19 = { "5th Div/ 3rd Inf BDE" }
		20 = { "5th Div/ 4th Inf BDE" }
		21 = { "6th Div/ 1st Inf BDE" }
		22 = { "6th Div/ 2nd Inf BDE" }
		23 = { "6th Div/ 3rd Inf BDE" }
		24 = { "6th Div/ 4th Inf BDE" }
		25 = { "7th Div/ 1st Inf BDE" }
		26 = { "7th Div/ 2nd Inf BDE" }
		27 = { "7th Div/ 3rd Inf BDE" }
		28 = { "7th Div/ 4th Inf BDE" }
		29 = { "8th Div/ 1st Inf BDE" }
		30 = { "8th Div/ 2nd Inf BDE" }
		31 = { "8th Div/ 3rd Inf BDE" }
		32 = { "8th Div/ 4th Inf BDE" }
		33 = { "9th Div/ 1st Inf BDE" }
		34 = { "9th Div/ 2nd Inf BDE" }
		35 = { "9th Div/ 3rd Inf BDE" }
		36 = { "9th Div/ 4th Inf BDE" }
		37 = { "10th Div/ 1st Inf BDE" }
		38 = { "10th Div/ 2nd Inf BDE" }
		39 = { "10th Div/ 3rd Inf BDE" }
		40 = { "10th Div/ 4th Inf BDE" }
		41 = { "11th Div/ 1st Inf BDE" }
		42 = { "11th Div/ 2nd Inf BDE" }
		43 = { "11th Div/ 3rd Inf BDE" }
		44 = { "11th Div/ 4th Inf BDE" }
		45 = { "12th Div/ 1st Inf BDE" }
		46 = { "12th Div/ 2nd Inf BDE" }
		47 = { "12th Div/ 3rd Inf BDE" }
		48 = { "12th Div/ 4th Inf BDE" }
		49 = { "13th Div/ 1st Inf BDE" }
		50 = { "13th Div/ 2nd Inf BDE" }
		51 = { "13th Div/ 3rd Inf BDE" }
		52 = { "13th Div/ 4th Inf BDE" }
		53 = { "14th Div/ 1st Inf BDE" }
		54 = { "14th Div/ 2nd Inf BDE" }
		55 = { "14th Div/ 3rd Inf BDE" }
		56 = { "14th Div/ 4th Inf BDE" }
		57 = { "15th Div/ 1st Inf BDE" }
		58 = { "15th Div/ 2nd Inf BDE" }
		59 = { "15th Div/ 3rd Inf BDE" }
		60 = { "15th Div/ 4th Inf BDE" }
		61 = { "16th Div/ 1st Inf BDE" }
		62 = { "16th Div/ 2nd Inf BDE" }
		63 = { "16th Div/ 3rd Inf BDE" }
		64 = { "16th Div/ 4th Inf BDE" }
		65 = { "17th Div/ 1st Inf BDE" }
		66 = { "17th Div/ 2nd Inf BDE" }
		67 = { "17th Div/ 3rd Inf BDE" }
		68 = { "17th Div/ 4th Inf BDE" }
		69 = { "18th Div/ 1st Inf BDE" }
		70 = { "18th Div/ 2nd Inf BDE" }
		71 = { "18th Div/ 3rd Inf BDE" }
		72 = { "18th Div/ 4th Inf BDE" }
		73 = { "19th Div/ 1st Inf BDE" }
		74 = { "19th Div/ 2nd Inf BDE" }
		75 = { "19th Div/ 3rd Inf BDE" }
		76 = { "19th Div/ 4th Inf BDE" }
		77 = { "20th Div/ 1st Inf BDE" }
		78 = { "20th Div/ 2nd Inf BDE" }
		79 = { "20th Div/ 3rd Inf BDE" }
		80 = { "20th Div/ 4th Inf BDE" }
		81 = { "21st Div/ 1st Inf BDE" }
		82 = { "21st Div/ 2nd Inf BDE" }
		83 = { "21st Div/ 3rd Inf BDE" }
		84 = { "21st Div/ 4th Inf BDE" }
		85 = { "22nd Div/ 1st Inf BDE" }
		86 = { "22nd Div/ 2nd Inf BDE" }
		87 = { "22nd Div/ 3rd Inf BDE" }
		88 = { "22nd Div/ 4th Inf BDE" }
		89 = { "23rd Div/ 1st Inf BDE" }
		90 = { "23rd Div/ 2nd Inf BDE" }
		91 = { "23rd Div/ 3rd Inf BDE" }
		92 = { "23rd Div/ 4th Inf BDE" }
		93 = { "24th Div/ 1st Inf BDE" }
		94 = { "24th Div/ 2nd Inf BDE" }
		95 = { "24th Div/ 3rd Inf BDE" }
		96 = { "24th Div/ 4th Inf BDE" }
		97 = { "25th Div/ 1st Inf BDE" }
		98 = { "25th Div/ 2nd Inf BDE" }
		99 = { "25th Div/ 3rd Inf BDE" }
		100 = { "25th Div/ 4th Inf BDE" }
		101 = { "26th Div/ 1st Inf BDE" }
		102 = { "26th Div/ 2nd Inf BDE" }
		103 = { "26th Div/ 3rd Inf BDE" }
		104 = { "26th Div/ 4th Inf BDE" }
		105 = { "27th Div/ 1st Inf BDE" }
		106 = { "27th Div/ 2nd Inf BDE" }
		107 = { "27th Div/ 3rd Inf BDE" }
		108 = { "27th Div/ 4th Inf BDE" }
		109 = { "28th Div/ 1st Inf BDE" }
		110 = { "28th Div/ 2nd Inf BDE" }
		111 = { "28th Div/ 3rd Inf BDE" }
		112 = { "28th Div/ 4th Inf BDE" }
		113 = { "29th Div/ 1st Inf BDE" }
		114 = { "29th Div/ 2nd Inf BDE" }
		115 = { "29th Div/ 3rd Inf BDE" }
		116 = { "29th Div/ 4th Inf BDE" }
		117 = { "30th Div/ 1st Inf BDE" }
		118 = { "30th Div/ 2nd Inf BDE" }
		119 = { "30th Div/ 3rd Inf BDE" }
		120 = { "30th Div/ 4th Inf BDE" }
	}
}

USB_INF_02 =
{
	name = "Infantry Regiment"
	for_countries = { USB }
	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { USB_INF_02 }

	fallback_name = "%d Infantry Regiment"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Div/ 1st Inf Regt" }
		2 = { "1st Div/ 2nd Inf Regt" }
		3 = { "1st Div/ 3rd Inf Regt" }
		4 = { "1st Div/ 4th Inf Regt" }
		5 = { "1st Div/ 5th Inf Regt" }
		6 = { "1st Div/ 6th Inf Regt" }
		7 = { "1st Div/ 7th Inf Regt" }
		8 = { "1st Div/ 8th Inf Regt" }
		9 = { "1st Div/ 9th Inf Regt" }
		10 = { "1st Div/ 10th Inf Regt" }
		11 = { "2nd Div/ 1st Inf Regt" }
		12 = { "2nd Div/ 2nd Inf Regt" }
		13 = { "2nd Div/ 3rd Inf Regt" }
		14 = { "2nd Div/ 4th Inf Regt" }
		15 = { "2nd Div/ 5th Inf Regt" }
		16 = { "2nd Div/ 6th Inf Regt" }
		17 = { "2nd Div/ 7th Inf Regt" }
		18 = { "2nd Div/ 8th Inf Regt" }
		19 = { "2nd Div/ 9th Inf Regt" }
		20 = { "2nd Div/ 10th Inf Regt" }
		21 = { "3rd Div/ 1st Inf Regt" }
		22 = { "3rd Div/ 2nd Inf Regt" }
		23 = { "3rd Div/ 3rd Inf Regt" }
		24 = { "3rd Div/ 4th Inf Regt" }
		25 = { "3rd Div/ 5th Inf Regt" }
		26 = { "3rd Div/ 6th Inf Regt" }
		27 = { "3rd Div/ 7th Inf Regt" }
		28 = { "3rd Div/ 8th Inf Regt" }
		29 = { "3rd Div/ 9th Inf Regt" }
		30 = { "3rd Div/ 10th Inf Regt" }
		31 = { "4th Div/ 1st Inf Regt" }
		32 = { "4th Div/ 2nd Inf Regt" }
		33 = { "4th Div/ 3rd Inf Regt" }
		34 = { "4th Div/ 4th Inf Regt" }
		35 = { "4th Div/ 5th Inf Regt" }
		36 = { "4th Div/ 6th Inf Regt" }
		37 = { "4th Div/ 7th Inf Regt" }
		38 = { "4th Div/ 8th Inf Regt" }
		39 = { "4th Div/ 9th Inf Regt" }
		40 = { "4th Div/ 10th Inf Regt" }
		41 = { "5th Div/ 1st Inf Regt" }
		42 = { "5th Div/ 2nd Inf Regt" }
		43 = { "5th Div/ 3rd Inf Regt" }
		44 = { "5th Div/ 4th Inf Regt" }
		45 = { "5th Div/ 5th Inf Regt" }
		46 = { "5th Div/ 6th Inf Regt" }
		47 = { "5th Div/ 7th Inf Regt" }
		48 = { "5th Div/ 8th Inf Regt" }
		49 = { "5th Div/ 9th Inf Regt" }
		50 = { "5th Div/ 10th Inf Regt" }
		51 = { "6th Div/ 1st Inf Regt" }
		52 = { "6th Div/ 2nd Inf Regt" }
		53 = { "6th Div/ 3rd Inf Regt" }
		54 = { "6th Div/ 4th Inf Regt" }
		55 = { "6th Div/ 5th Inf Regt" }
		56 = { "6th Div/ 6th Inf Regt" }
		57 = { "6th Div/ 7th Inf Regt" }
		58 = { "6th Div/ 8th Inf Regt" }
		59 = { "6th Div/ 9th Inf Regt" }
		60 = { "6th Div/ 10th Inf Regt" }
		61 = { "7th Div/ 1st Inf Regt" }
		62 = { "7th Div/ 2nd Inf Regt" }
		63 = { "7th Div/ 3rd Inf Regt" }
		64 = { "7th Div/ 4th Inf Regt" }
		65 = { "7th Div/ 5th Inf Regt" }
		66 = { "7th Div/ 6th Inf Regt" }
		67 = { "7th Div/ 7th Inf Regt" }
		68 = { "7th Div/ 8th Inf Regt" }
		69 = { "7th Div/ 9th Inf Regt" }
		70 = { "7th Div/ 10th Inf Regt" }
		81 = { "8th Div/ 1st Inf Regt" }
		82 = { "8th Div/ 2nd Inf Regt" }
		83 = { "8th Div/ 3rd Inf Regt" }
		84 = { "8th Div/ 4th Inf Regt" }
		85 = { "8th Div/ 5th Inf Regt" }
		86 = { "8th Div/ 6th Inf Regt" }
		87 = { "8th Div/ 7th Inf Regt" }
		88 = { "8th Div/ 8th Inf Regt" }
		89 = { "8th Div/ 9th Inf Regt" }
		90 = { "8th Div/ 10th Inf Regt" }
		91 = { "9th Div/ 1st Inf Regt" }
		92 = { "9th Div/ 2nd Inf Regt" }
		93 = { "9th Div/ 3rd Inf Regt" }
		94 = { "9th Div/ 4th Inf Regt" }
		95 = { "9th Div/ 5th Inf Regt" }
		96 = { "9th Div/ 6th Inf Regt" }
		97 = { "9th Div/ 7th Inf Regt" }
		98 = { "9th Div/ 8th Inf Regt" }
		99 = { "9th Div/ 9th Inf Regt" }
		100 = { "9th Div/ 10th Inf Regt" }
		101 = { "10th Div/ 1st Inf Regt" }
		102 = { "10th Div/ 2nd Inf Regt" }
		103 = { "10th Div/ 3rd Inf Regt" }
		104 = { "10th Div/ 4th Inf Regt" }
		105 = { "10th Div/ 5th Inf Regt" }
		106 = { "10th Div/ 6th Inf Regt" }
		107 = { "10th Div/ 7th Inf Regt" }
		108 = { "10th Div/ 8th Inf Regt" }
		109 = { "10th Div/ 9th Inf Regt" }
		110 = { "10th Div/ 10th Inf Regt" }
	}
}

USB_ARM_01 =
{
	name = "Armored Brigade"

	for_countries = { USB }

	division_types = { "armor_Bat" "Arm_Inf_Bat" }

	# Number reservation system will tie to another group.

	fallback_name = "%dth Armored Division"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Div/ 1st Arm BDE" }
		2 = { "1st Div/ 2nd Arm BDE" }
		3 = { "1st Div/ 3rd Arm BDE" }
		4 = { "1st Div/ 4th Arm BDE" }
		5 = { "2nd Div/ 1st Arm BDE" }
		6 = { "2nd Div/ 2nd Arm BDE" }
		7 = { "2nd Div/ 3rd Arm BDE" }
		8 = { "2nd Div/ 4th Arm BDE" }
		9 = { "3rd Div/ 1st Arm BDE" }
		10 = { "3rd Div/ 2nd Arm BDE" }
		11 = { "3rd Div/ 3rd Arm BDE" }
		12 = { "3rd Div/ 4th Arm BDE" }
		13 = { "4th Div/ 1st Arm BDE" }
		14 = { "4th Div/ 2nd Arm BDE" }
		15 = { "4th Div/ 3rd Arm BDE" }
		16 = { "4th Div/ 4th Arm BDE" }
		17 = { "5th Div/ 1st Arm BDE" }
		18 = { "5th Div/ 2nd Arm BDE" }
		19 = { "5th Div/ 3rd Arm BDE" }
		20 = { "5th Div/ 4th Arm BDE" }
		21 = { "6th Div/ 1st Arm BDE" }
		22 = { "6th Div/ 2nd Arm BDE" }
		23 = { "6th Div/ 3rd Arm BDE" }
		24 = { "6th Div/ 4th Arm BDE" }
		25 = { "7th Div/ 1st Arm BDE" }
		26 = { "7th Div/ 2nd Arm BDE" }
		27 = { "7th Div/ 3rd Arm BDE" }
		28 = { "7th Div/ 4th Arm BDE" }
		29 = { "8th Div/ 1st Arm BDE" }
		30 = { "8th Div/ 2nd Arm BDE" }
		31 = { "8th Div/ 3rd Arm BDE" }
		32 = { "8th Div/ 4th Arm BDE" }
		33 = { "9th Div/ 1st Arm BDE" }
		34 = { "9th Div/ 2nd Arm BDE" }
		35 = { "9th Div/ 3rd Arm BDE" }
		36 = { "9th Div/ 4th Arm BDE" }
		37 = { "10th Div/ 1st Arm BDE" }
		38 = { "10th Div/ 2nd Arm BDE" }
		39 = { "10th Div/ 3rd Arm BDE" }
		40 = { "10th Div/ 4th Arm BDE" }
		41 = { "11th Div/ 1st Arm BDE" }
		42 = { "11th Div/ 2nd Arm BDE" }
		43 = { "11th Div/ 3rd Arm BDE" }
		44 = { "11th Div/ 4th Arm BDE" }
		45 = { "12th Div/ 1st Arm BDE" }
		46 = { "12th Div/ 2nd Arm BDE" }
		47 = { "12th Div/ 3rd Arm BDE" }
		48 = { "12th Div/ 4th Arm BDE" }
		49 = { "13th Div/ 1st Arm BDE" }
		50 = { "13th Div/ 2nd Arm BDE" }
		51 = { "13th Div/ 3rd Arm BDE" }
		52 = { "13th Div/ 4th Arm BDE" }
		53 = { "14th Div/ 1st Arm BDE" }
		54 = { "14th Div/ 2nd Arm BDE" }
		55 = { "14th Div/ 3rd Arm BDE" }
		56 = { "14th Div/ 4th Arm BDE" }
		57 = { "15th Div/ 1st Arm BDE" }
		58 = { "15th Div/ 2nd Arm BDE" }
		59 = { "15th Div/ 3rd Arm BDE" }
		60 = { "15th Div/ 4th Arm BDE" }
		61 = { "16th Div/ 1st Arm BDE" }
		62 = { "16th Div/ 2nd Arm BDE" }
		63 = { "16th Div/ 3rd Arm BDE" }
		64 = { "16th Div/ 4th Arm BDE" }
		65 = { "17th Div/ 1st Arm BDE" }
		66 = { "17th Div/ 2nd Arm BDE" }
		67 = { "17th Div/ 3rd Arm BDE" }
		68 = { "17th Div/ 4th Arm BDE" }
		69 = { "18th Div/ 1st Arm BDE" }
		70 = { "18th Div/ 2nd Arm BDE" }
		71 = { "18th Div/ 3rd Arm BDE" }
		72 = { "18th Div/ 4th Arm BDE" }
		73 = { "19th Div/ 1st Arm BDE" }
		74 = { "19th Div/ 2nd Arm BDE" }
		75 = { "19th Div/ 3rd Arm BDE" }
		76 = { "19th Div/ 4th Arm BDE" }
		77 = { "20th Div/ 1st Arm BDE" }
		78 = { "20th Div/ 2nd Arm BDE" }
		79 = { "20th Div/ 3rd Arm BDE" }
		80 = { "20th Div/ 4th Arm BDE" }
		81 = { "21st Div/ 1st Arm BDE" }
		82 = { "21st Div/ 2nd Arm BDE" }
		83 = { "21st Div/ 3rd Arm BDE" }
		84 = { "21st Div/ 4th Arm BDE" }
		85 = { "22nd Div/ 1st Arm BDE" }
		86 = { "22nd Div/ 2nd Arm BDE" }
		87 = { "22nd Div/ 3rd Arm BDE" }
		88 = { "22nd Div/ 4th Arm BDE" }
		89 = { "23rd Div/ 1st Arm BDE" }
		90 = { "23rd Div/ 2nd Arm BDE" }
		91 = { "23rd Div/ 3rd Arm BDE" }
		92 = { "23rd Div/ 4th Arm BDE" }
		93 = { "24th Div/ 1st Arm BDE" }
		94 = { "24th Div/ 2nd Arm BDE" }
		95 = { "24th Div/ 3rd Arm BDE" }
		96 = { "24th Div/ 4th Arm BDE" }
		97 = { "25th Div/ 1st Arm BDE" }
		98 = { "25th Div/ 2nd Arm BDE" }
		99 = { "25th Div/ 3rd Arm BDE" }
		100 = { "25th Div/ 4th Arm BDE" }
		101 = { "26th Div/ 1st Arm BDE" }
		102 = { "26th Div/ 2nd Arm BDE" }
		103 = { "26th Div/ 3rd Arm BDE" }
		104 = { "26th Div/ 4th Arm BDE" }
		105 = { "27th Div/ 1st Arm BDE" }
		106 = { "27th Div/ 2nd Arm BDE" }
		107 = { "27th Div/ 3rd Arm BDE" }
		108 = { "27th Div/ 4th Arm BDE" }
		109 = { "28th Div/ 1st Arm BDE" }
		110 = { "28th Div/ 2nd Arm BDE" }
		111 = { "28th Div/ 3rd Arm BDE" }
		112 = { "28th Div/ 4th Arm BDE" }
		113 = { "29th Div/ 1st Arm BDE" }
		114 = { "29th Div/ 2nd Arm BDE" }
		115 = { "29th Div/ 3rd Arm BDE" }
		116 = { "29th Div/ 4th Arm BDE" }
		117 = { "30th Div/ 1st Arm BDE" }
		118 = { "30th Div/ 2nd Arm BDE" }
		119 = { "30th Div/ 3rd Arm BDE" }
		120 = { "30th Div/ 4th Arm BDE" }
	}
}

USB_ARM_02 =
{
	name = "Armored Regiment"

	for_countries = { USB }

	division_types = { "armor_Bat" "Arm_Inf_Bat" "Mech_Inf_Bat" }

	# Number reservation system will tie to another group.

	fallback_name = "%d Armored Regiment"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Div/ 1st Arm Regt" }
		2 = { "1st Div/ 2nd Arm Regt" }
		3 = { "1st Div/ 3rd Arm Regt" }
		4 = { "1st Div/ 4th Arm Regt" }
		5 = { "1st Div/ 5th Arm Regt" }
		6 = { "1st Div/ 6th Arm Regt" }
		7 = { "1st Div/ 7th Arm Regt" }
		8 = { "1st Div/ 8th Arm Regt" }
		9 = { "1st Div/ 9th Arm Regt" }
		10 = { "1st Div/ 10th Arm Regt" }
		11 = { "2nd Div/ 1st Arm Regt" }
		12 = { "2nd Div/ 2nd Arm Regt" }
		13 = { "2nd Div/ 3rd Arm Regt" }
		14 = { "2nd Div/ 4th Arm Regt" }
		15 = { "2nd Div/ 5th Arm Regt" }
		16 = { "2nd Div/ 6th Arm Regt" }
		17 = { "2nd Div/ 7th Arm Regt" }
		18 = { "2nd Div/ 8th Arm Regt" }
		19 = { "2nd Div/ 9th Arm Regt" }
		20 = { "2nd Div/ 10th Arm Regt" }
		21 = { "3rd Div/ 1st Arm Regt" }
		22 = { "3rd Div/ 2nd Arm Regt" }
		23 = { "3rd Div/ 3rd Arm Regt" }
		24 = { "3rd Div/ 4th Arm Regt" }
		25 = { "3rd Div/ 5th Arm Regt" }
		26 = { "3rd Div/ 6th Arm Regt" }
		27 = { "3rd Div/ 7th Arm Regt" }
		28 = { "3rd Div/ 8th Arm Regt" }
		29 = { "3rd Div/ 9th Arm Regt" }
		30 = { "3rd Div/ 10th Arm Regt" }
		31 = { "4th Div/ 1st Arm Regt" }
		32 = { "4th Div/ 2nd Arm Regt" }
		33 = { "4th Div/ 3rd Arm Regt" }
		34 = { "4th Div/ 4th Arm Regt" }
		35 = { "4th Div/ 5th Arm Regt" }
		36 = { "4th Div/ 6th Arm Regt" }
		37 = { "4th Div/ 7th Arm Regt" }
		38 = { "4th Div/ 8th Arm Regt" }
		39 = { "4th Div/ 9th Arm Regt" }
		40 = { "4th Div/ 10th Arm Regt" }
		41 = { "5th Div/ 1st Arm Regt" }
		42 = { "5th Div/ 2nd Arm Regt" }
		43 = { "5th Div/ 3rd Arm Regt" }
		44 = { "5th Div/ 4th Arm Regt" }
		45 = { "5th Div/ 5th Arm Regt" }
		46 = { "5th Div/ 6th Arm Regt" }
		47 = { "5th Div/ 7th Arm Regt" }
		48 = { "5th Div/ 8th Arm Regt" }
		49 = { "5th Div/ 9th Arm Regt" }
		50 = { "5th Div/ 10th Arm Regt" }
		51 = { "6th Div/ 1st Arm Regt" }
		52 = { "6th Div/ 2nd Arm Regt" }
		53 = { "6th Div/ 3rd Arm Regt" }
		54 = { "6th Div/ 4th Arm Regt" }
		55 = { "6th Div/ 5th Arm Regt" }
		56 = { "6th Div/ 6th Arm Regt" }
		57 = { "6th Div/ 7th Arm Regt" }
		58 = { "6th Div/ 8th Arm Regt" }
		59 = { "6th Div/ 9th Arm Regt" }
		60 = { "6th Div/ 10th Arm Regt" }
		61 = { "7th Div/ 1st Arm Regt" }
		62 = { "7th Div/ 2nd Arm Regt" }
		63 = { "7th Div/ 3rd Arm Regt" }
		64 = { "7th Div/ 4th Arm Regt" }
		65 = { "7th Div/ 5th Arm Regt" }
		66 = { "7th Div/ 6th Arm Regt" }
		67 = { "7th Div/ 7th Arm Regt" }
		68 = { "7th Div/ 8th Arm Regt" }
		69 = { "7th Div/ 9th Arm Regt" }
		70 = { "7th Div/ 10th Arm Regt" }
		81 = { "8th Div/ 1st Arm Regt" }
		82 = { "8th Div/ 2nd Arm Regt" }
		83 = { "8th Div/ 3rd Arm Regt" }
		84 = { "8th Div/ 4th Arm Regt" }
		85 = { "8th Div/ 5th Arm Regt" }
		86 = { "8th Div/ 6th Arm Regt" }
		87 = { "8th Div/ 7th Arm Regt" }
		88 = { "8th Div/ 8th Arm Regt" }
		89 = { "8th Div/ 9th Arm Regt" }
		90 = { "8th Div/ 10th Arm Regt" }
		91 = { "9th Div/ 1st Arm Regt" }
		92 = { "9th Div/ 2nd Arm Regt" }
		93 = { "9th Div/ 3rd Arm Regt" }
		94 = { "9th Div/ 4th Arm Regt" }
		95 = { "9th Div/ 5th Arm Regt" }
		96 = { "9th Div/ 6th Arm Regt" }
		97 = { "9th Div/ 7th Arm Regt" }
		98 = { "9th Div/ 8th Arm Regt" }
		99 = { "9th Div/ 9th Arm Regt" }
		100 = { "9th Div/ 10th Arm Regt" }
		101 = { "10th Div/ 1st Arm Regt" }
		102 = { "10th Div/ 2nd Arm Regt" }
		103 = { "10th Div/ 3rd Arm Regt" }
		104 = { "10th Div/ 4th Arm Regt" }
		105 = { "10th Div/ 5th Arm Regt" }
		106 = { "10th Div/ 6th Arm Regt" }
		107 = { "10th Div/ 7th Arm Regt" }
		108 = { "10th Div/ 8th Arm Regt" }
		109 = { "10th Div/ 9th Arm Regt" }
		110 = { "10th Div/ 10th Arm Regt" }
	}
}

USB_SOF_01 =
{
	name = "Special Forces Brigade"

	for_countries = { USB }

	division_types = { "Special_Forces" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { USA_INF_01 }

	fallback_name = "%dth SF Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Div/ 1st SF BDE" }
		2 = { "1st Div/ 2nd SF BDE" }
		3 = { "1st Div/ 3rd SF BDE" }
		4 = { "1st Div/ 4th SF BDE" }
		5 = { "2nd Div/ 1st SF BDE" }
		6 = { "2nd Div/ 2nd SF BDE" }
		7 = { "2nd Div/ 3rd SF BDE" }
		8 = { "2nd Div/ 4th SF BDE" }
		9 = { "3rd Div/ 1st SF BDE" }
		10 = { "3rd Div/ 2nd SF BDE" }
		11 = { "3rd Div/ 3rd SF BDE" }
		12 = { "3rd Div/ 4th SF BDE" }
		13 = { "4th Div/ 1st SF BDE" }
		14 = { "4th Div/ 2nd SF BDE" }
		15 = { "4th Div/ 3rd SF BDE" }
		16 = { "4th Div/ 4th SF BDE" }
		17 = { "5th Div/ 1st SF BDE" }
		18 = { "5th Div/ 2nd SF BDE" }
		19 = { "5th Div/ 3rd SF BDE" }
		20 = { "5th Div/ 4th SF BDE" }
		21 = { "6th Div/ 1st SF BDE" }
		22 = { "6th Div/ 2nd SF BDE" }
		23 = { "6th Div/ 3rd SF BDE" }
		24 = { "6th Div/ 4th SF BDE" }
		25 = { "7th Div/ 1st SF BDE" }
		26 = { "7th Div/ 2nd SF BDE" }
		27 = { "7th Div/ 3rd SF BDE" }
		28 = { "7th Div/ 4th SF BDE" }
		29 = { "8th Div/ 1st SF BDE" }
		30 = { "8th Div/ 2nd SF BDE" }
		31 = { "8th Div/ 3rd SF BDE" }
		32 = { "8th Div/ 4th SF BDE" }
		33 = { "9th Div/ 1st SF BDE" }
		34 = { "9th Div/ 2nd SF BDE" }
		35 = { "9th Div/ 3rd SF BDE" }
		36 = { "9th Div/ 4th SF BDE" }
		37 = { "10th Div/ 1st SF BDE" }
		38 = { "10th Div/ 2nd SF BDE" }
		39 = { "10th Div/ 3rd SF BDE" }
		40 = { "10th Div/ 4th SF BDE" }
		41 = { "11th Div/ 1st SF BDE" }
		42 = { "11th Div/ 2nd SF BDE" }
		43 = { "11th Div/ 3rd SF BDE" }
		44 = { "11th Div/ 4th SF BDE" }
		45 = { "12th Div/ 1st SF BDE" }
		46 = { "12th Div/ 2nd SF BDE" }
		47 = { "12th Div/ 3rd SF BDE" }
		48 = { "12th Div/ 4th SF BDE" }
		49 = { "13th Div/ 1st SF BDE" }
		50 = { "13th Div/ 2nd SF BDE" }
		51 = { "13th Div/ 3rd SF BDE" }
		52 = { "13th Div/ 4th SF BDE" }
		53 = { "14th Div/ 1st SF BDE" }
		54 = { "14th Div/ 2nd SF BDE" }
		55 = { "14th Div/ 3rd SF BDE" }
		56 = { "14th Div/ 4th SF BDE" }
		57 = { "15th Div/ 1st SF BDE" }
		58 = { "15th Div/ 2nd SF BDE" }
		59 = { "15th Div/ 3rd SF BDE" }
		60 = { "15th Div/ 4th SF BDE" }
		61 = { "16th Div/ 1st SF BDE" }
		62 = { "16th Div/ 2nd SF BDE" }
		63 = { "16th Div/ 3rd SF BDE" }
		64 = { "16th Div/ 4th SF BDE" }
		65 = { "17th Div/ 1st SF BDE" }
		66 = { "17th Div/ 2nd SF BDE" }
		67 = { "17th Div/ 3rd SF BDE" }
		68 = { "17th Div/ 4th SF BDE" }
		69 = { "18th Div/ 1st SF BDE" }
		70 = { "18th Div/ 2nd SF BDE" }
		71 = { "18th Div/ 3rd SF BDE" }
		72 = { "18th Div/ 4th SF BDE" }
		73 = { "19th Div/ 1st SF BDE" }
		74 = { "19th Div/ 2nd SF BDE" }
		75 = { "19th Div/ 3rd SF BDE" }
		76 = { "19th Div/ 4th SF BDE" }
		77 = { "20th Div/ 1st SF BDE" }
		78 = { "20th Div/ 2nd SF BDE" }
		79 = { "20th Div/ 3rd SF BDE" }
		80 = { "20th Div/ 4th SF BDE" }
		81 = { "21st Div/ 1st SF BDE" }
		82 = { "21st Div/ 2nd SF BDE" }
		83 = { "21st Div/ 3rd SF BDE" }
		84 = { "21st Div/ 4th SF BDE" }
		85 = { "22nd Div/ 1st SF BDE" }
		86 = { "22nd Div/ 2nd SF BDE" }
		87 = { "22nd Div/ 3rd SF BDE" }
		88 = { "22nd Div/ 4th SF BDE" }
		89 = { "23rd Div/ 1st SF BDE" }
		90 = { "23rd Div/ 2nd SF BDE" }
		91 = { "23rd Div/ 3rd SF BDE" }
		92 = { "23rd Div/ 4th SF BDE" }
		93 = { "24th Div/ 1st SF BDE" }
		94 = { "24th Div/ 2nd SF BDE" }
		95 = { "24th Div/ 3rd SF BDE" }
		96 = { "24th Div/ 4th SF BDE" }
		97 = { "25th Div/ 1st SF BDE" }
		98 = { "25th Div/ 2nd SF BDE" }
		99 = { "25th Div/ 3rd SF BDE" }
		100 = { "25th Div/ 4th SF BDE" }
		101 = { "26th Div/ 1st SF BDE" }
		102 = { "26th Div/ 2nd SF BDE" }
		103 = { "26th Div/ 3rd SF BDE" }
		104 = { "26th Div/ 4th SF BDE" }
		105 = { "27th Div/ 1st SF BDE" }
		106 = { "27th Div/ 2nd SF BDE" }
		107 = { "27th Div/ 3rd SF BDE" }
		108 = { "27th Div/ 4th SF BDE" }
		109 = { "28th Div/ 1st SF BDE" }
		110 = { "28th Div/ 2nd SF BDE" }
		111 = { "28th Div/ 3rd SF BDE" }
		112 = { "28th Div/ 4th SF BDE" }
		113 = { "29th Div/ 1st SF BDE" }
		114 = { "29th Div/ 2nd SF BDE" }
		115 = { "29th Div/ 3rd SF BDE" }
		116 = { "29th Div/ 4th SF BDE" }
		117 = { "30th Div/ 1st SF BDE" }
		118 = { "30th Div/ 2nd SF BDE" }
		119 = { "30th Div/ 3rd SF BDE" }
		120 = { "30th Div/ 4th SF BDE" }
	}
}

USB_PARA_01 =
{
	name = "Airborne Brigade"
	for_countries = { USB }
	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { USB_PARA_01 }

	fallback_name = "%d Paratrooper Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Para Div/ 1st BDE" }
		2 = { "1st Para Div/ 2nd BDE" }
		3 = { "1st Para Div/ 3rd BDE" }
		4 = { "1st Para Div/ 4th BDE" }
		5 = { "1st Para Div/ 5th BDE" }
		6 = { "2nd Para Div/ 1st BDE" }
		7 = { "2nd Para Div/ 2nd BDE" }
		8 = { "2nd Para Div/ 3rd BDE" }
		9 = { "2nd Para Div/ 4th BDE" }
		10 = { "2nd Para Div/ 5th BDE" }
		11 = { "3rd Para Div/ 1st BDE" }
		12 = { "3rd Para Div/ 2nd BDE" }
		13 = { "3rd Para Div/ 3rd BDE" }
		14 = { "3rd Para Div/ 4th BDE" }
		15 = { "3rd Para Div/ 5th BDE" }
		16 = { "4th Para Div/ 1st BDE" }
		17 = { "4th Para Div/ 2nd BDE" }
		18 = { "4th Para Div/ 3rd BDE" }
		19 = { "4th Para Div/ 4th BDE" }
		20 = { "4th Para Div/ 5th BDE" }
		21 = { "5th Para Div/ 1st BDE" }
		22 = { "5th Para Div/ 2nd BDE" }
		23 = { "5th Para Div/ 3rd BDE" }
		24 = { "5th Para Div/ 4th BDE" }
		25 = { "5th Para Div/ 5th BDE" }
		26 = { "6th Para Div/ 1st BDE" }
		27 = { "6th Para Div/ 2nd BDE" }
		28 = { "6th Para Div/ 3rd BDE" }
		29 = { "6th Para Div/ 4th BDE" }
		30 = { "6th Para Div/ 5th BDE" }
		31 = { "7th Para Div/ 1st BDE" }
		32 = { "7th Para Div/ 2nd BDE" }
		33 = { "7th Para Div/ 3rd BDE" }
		34 = { "7th Para Div/ 4th BDE" }
		35 = { "7th Para Div/ 5th BDE" }
		36 = { "8th Para Div/ 1st BDE" }
		37 = { "8th Para Div/ 2nd BDE" }
		38 = { "8th Para Div/ 3rd BDE" }
		39 = { "8th Para Div/ 4th BDE" }
		40 = { "8th Para Div/ 5th BDE" }
		41 = { "9th Para Div/ 1st BDE" }
		42 = { "9th Para Div/ 2nd BDE" }
		43 = { "9th Para Div/ 3rd BDE" }
		44 = { "9th Para Div/ 4th BDE" }
		45 = { "9th Para Div/ 5th BDE" }
		46 = { "10th Para Div/ 1st BDE" }
		47 = { "10th Para Div/ 2nd BDE" }
		48 = { "10th Para Div/ 3rd BDE" }
		49 = { "10th Para Div/ 4th BDE" }
		50 = { "10th Para Div/ 5th BDE" }
	}
}

USB_MAR_01 =
{
	name = "Marine Brigade"
	for_countries = { USB }
	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { USA_INF_01 }

	fallback_name = "%d Marine Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Mar Div/ 1st BDE" }
		2 = { "1st Mar Div/ 2nd BDE" }
		3 = { "1st Mar Div/ 3rd BDE" }
		4 = { "1st Mar Div/ 4th BDE" }
		5 = { "1st Mar Div/ 5th BDE" }
		6 = { "2nd Mar Div/ 1st BDE" }
		7 = { "2nd Mar Div/ 2nd BDE" }
		8 = { "2nd Mar Div/ 3rd BDE" }
		9 = { "2nd Mar Div/ 4th BDE" }
		10 = { "2nd Mar Div/ 5th BDE" }
		11 = { "3rd Mar Div/ 1st BDE" }
		12 = { "3rd Mar Div/ 2nd BDE" }
		13 = { "3rd Mar Div/ 3rd BDE" }
		14 = { "3rd Mar Div/ 4th BDE" }
		15 = { "3rd Mar Div/ 5th BDE" }
		16 = { "4th Mar Div/ 1st BDE" }
		17 = { "4th Mar Div/ 2nd BDE" }
		18 = { "4th Mar Div/ 3rd BDE" }
		19 = { "4th Mar Div/ 4th BDE" }
		20 = { "4th Mar Div/ 5th BDE" }
		21 = { "5th Mar Div/ 1st BDE" }
		22 = { "5th Mar Div/ 2nd BDE" }
		23 = { "5th Mar Div/ 3rd BDE" }
		24 = { "5th Mar Div/ 4th BDE" }
		25 = { "5th Mar Div/ 5th BDE" }
		26 = { "6th Mar Div/ 1st BDE" }
		27 = { "6th Mar Div/ 2nd BDE" }
		28 = { "6th Mar Div/ 3rd BDE" }
		29 = { "6th Mar Div/ 4th BDE" }
		30 = { "6th Mar Div/ 5th BDE" }
		31 = { "7th Mar Div/ 1st BDE" }
		32 = { "7th Mar Div/ 2nd BDE" }
		33 = { "7th Mar Div/ 3rd BDE" }
		34 = { "7th Mar Div/ 4th BDE" }
		35 = { "7th Mar Div/ 5th BDE" }
		36 = { "8th Mar Div/ 1st BDE" }
		37 = { "8th Mar Div/ 2nd BDE" }
		38 = { "8th Mar Div/ 3rd BDE" }
		39 = { "8th Mar Div/ 4th BDE" }
		40 = { "8th Mar Div/ 5th BDE" }
		41 = { "9th Mar Div/ 1st BDE" }
		42 = { "9th Mar Div/ 2nd BDE" }
		43 = { "9th Mar Div/ 3rd BDE" }
		44 = { "9th Mar Div/ 4th BDE" }
		45 = { "9th Mar Div/ 5th BDE" }
		46 = { "10th Mar Div/ 1st BDE" }
		47 = { "10th Mar Div/ 2nd BDE" }
		48 = { "10th Mar Div/ 3rd BDE" }
		49 = { "10th Mar Div/ 4th BDE" }
		50 = { "10th Mar Div/ 5th BDE" }
	}
}