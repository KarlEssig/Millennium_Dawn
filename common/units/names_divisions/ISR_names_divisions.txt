﻿ISR_GAR_01 =
{
	name = "Territorial Brigade"

	for_countries = { ISR }

	can_use = {	always = yes }

	division_types = { "L_Inf_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { ISR_INF_01 ISR_GAR_01 ISR_RES_01 ISR_ARM_01 ISR_ARM_02 ISR_PAR_01 ISR_PAR_02 }

	fallback_name = "%dth Infantry Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Territorial Brigade 'Golani'" }
		2 = { "2nd Territorial Brigade 'Carmeli'" }
		3 = { "3rd Territorial Brigade 'Alexandroni'" }
		4 = { "4th Territorial Brigade 'Kiryati'" }
		5 = { "5th Territorial Brigade 'Sharon'" }
		6 = { "6th Territorial Brigade 'Etzioni'" }
		7 = { "7th Territorial Brigade 'Saar me-Golan'" }
		8 = { "8th Territorial Brigade 'HaZaken'" }
		9 = { "9th Territorial Brigade 'Oded'" }
		10 = { "10th Territorial Brigade 'Harel'" }
		11 = { "11th Territorial Brigade 'Yiftah'" }
		12 = { "12th Territorial Brigade 'Negev'" }
		14 = { "14th Territorial Brigade 'Machatz'" }
		16 = { "16th Territorial Brigade 'Jerusalem'" }
		35 = { "35th Territorial Brigade 'Shfifon'" }
		37 = { "37th Territorial Brigade 'Ram'" }
		55 = { "55th Territorial Brigade 'Hod ha-Hanit'" }
		84 = { "84th Territorial Brigade 'Giv'ati'" }
		188 = { "188th Territorial Brigade 'Barak'" }
		205 = { "205th Territorial Brigade 'I'kvot'" }
		211 = { "211th Territorial Brigade 'Yishai'" }
		226 = { "226th Territorial Brigade 'Nesher'" }
		228 = { "228th Territorial Brigade 'Alon'" }
		263 = { "263rd Territorial Brigade 'Merkavot ha-Esh'" }
		270 = { "270th Territorial Brigade 'Arava'" }
		300 = { "300th Territorial Brigade 'Bar'am'" }
		401 = { "401st Territorial Brigade 'I'kvot Ha-Barzel'" }
		406 = { "406th Territorial Brigade 'Yoav'" }
		417 = { "417th Territorial Brigade 'Emek ha-Yarden'" }
		460 = { "460th Territorial Brigade 'Bnei Or'" }
		474 = { "474th Territorial Brigade 'Golan'" }
		500 = { "500th Territorial Brigade 'Kfir'" }
		512 = { "512th Territorial Brigade 'Paran'" }
		551 = { "551st Territorial Brigade 'Hetzei ha-Esh'" }
		646 = { "646th Territorial Brigade 'Shu'ali Marom'" }
		679 = { "679th Territorial Brigade 'Yiftah'" }
		769 = { "769th Territorial Brigade 'Hiram'" }
		847 = { "847th Territorial Brigade 'Merkavot ha-I'kvot'" }
		900 = { "900th Territorial Brigade 'Kfir'" }
		933 = { "933rd Territorial Brigade 'Nahal'" }
		1001 = { "1001st Territorial Brigade 'Gefen'" }
		1002 = { "1002nd Territorial Brigade 'Katif'" }
		1003 = { "1003rd Territorial Brigade 'Menashe'" }
		1004 = { "1004th Territorial Brigade 'Ephraim'" }
		1005 = { "1005th Territorial Brigade 'Samaria'" }
		1006 = { "1006th Territorial Brigade 'Binyamin'" }
		1007 = { "1007th Territorial Brigade 'Judea'" }
		1008 = { "1008th Territorial Brigade 'Yehuda'" }
	}
}
ISR_RES_01 =
{
	name = "Reserve Infantry Brigade"

	for_countries = { ISR }

	can_use = {	always = yes }

	division_types = { "Mot_Inf_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { ISR_INF_01 ISR_GAR_01 ISR_RES_01 ISR_ARM_01 ISR_ARM_02 ISR_PAR_01 ISR_PAR_02 }

	fallback_name = "%dth Infantry Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Infantry Brigade 'Golani' (Reserve)" }
		2 = { "2nd Infantry Brigade 'Carmeli' (Reserve)" }
		3 = { "3rd Infantry Brigade 'Alexandroni' (Reserve)" }
		4 = { "4th Infantry Brigade 'Kiryati' (Reserve)" }
		5 = { "5th Infantry Brigade 'Sharon' (Reserve)" }
		6 = { "6th Infantry Brigade 'Etzioni' (Reserve)" }
		7 = { "7th Infantry Brigade 'Saar me-Golan' (Reserve)" }
		8 = { "8th Infantry Brigade 'HaZaken' (Reserve)" }
		9 = { "9th Infantry Brigade 'Oded' (Reserve)" }
		10 = { "10th Infantry Brigade 'Harel' (Reserve)" }
		11 = { "11th Infantry Brigade 'Yiftah' (Reserve)" }
		12 = { "12th Infantry Brigade 'Negev' (Reserve)" }
		14 = { "14th Infantry Brigade 'Machatz' (Reserve)" }
		16 = { "16th Infantry Brigade 'Jerusalem' (Reserve)" }
		35 = { "35th Infantry Brigade 'Shfifon' (Reserve)" }
		37 = { "37th Infantry Brigade 'Ram' (Reserve)" }
		55 = { "55th Infantry Brigade 'Hod ha-Hanit' (Reserve)" }
		84 = { "84th Infantry Brigade 'Giv'ati' (Reserve)" }
		188 = { "188th Infantry Brigade 'Barak' (Reserve)" }
		205 = { "205th Infantry Brigade 'I'kvot' (Reserve)" }
		211 = { "211th Infantry Brigade 'Yishai' (Reserve)" }
		226 = { "226th infantry Brigade 'Nesher' (Reserve)" }
		228 = { "228th Infantry Brigade 'Alon' (Reserve)" }
		263 = { "263rd Infantry Brigade 'Merkavot ha-Esh' (Reserve)" }
		270 = { "270th Infantry Brigade 'Arava' (Reserve)" }
		300 = { "300th Infantry Brigade 'Bar'am' (Reserve)" }
		401 = { "401st Infantry Brigade 'I'kvot Ha-Barzel' (Reserve)" }
		406 = { "406th Infantry Brigade 'Yoav' (Reserve)" }
		417 = { "417th Infantry Brigade 'Emek ha-Yarden' (Reserve)" }
		460 = { "460th Infantry Brigade 'Bnei Or' (Reserve)" }
		474 = { "474th Infantry Brigade 'Golan' (Reserve)" }
		500 = { "500th Infantry Brigade 'Kfir' (Reserve)" }
		512 = { "512th Infantry Brigade 'Paran' (Reserve)" }
		551 = { "551st Infantry Brigade 'Hetzei ha-Esh' (Reserve)" }
		646 = { "646th Infantry Brigade 'Shu'ali Marom' (Reserve)" }
		679 = { "679th Infantry Brigade 'Yiftah' (Reserve)" }
		769 = { "769th Infantry Brigade 'Hiram' (Reserve)" }
		847 = { "847th Infantry Brigade 'Merkavot ha-I'kvot' (Reserve)" }
		900 = { "900th Infantry Brigade 'Kfir' (Reserve)" }
		933 = { "933rd Infantry Brigade 'Nahal' (Reserve)" }
		1001 = { "1001st Infantry Brigade 'Gefen' (Reserve)" }
		1002 = { "1002nd Infantry Brigade 'Katif' (Reserve)" }
		1003 = { "1003rd Infantry Brigade 'Menashe' (Reserve)" }
		1004 = { "1004th Infantry Brigade 'Ephraim' (Reserve)" }
		1005 = { "1005th Infantry Brigade 'Samaria' (Reserve)" }
		1006 = { "1006th Infantry Brigade 'Binyamin' (Reserve)" }
		1007 = { "1007th Infantry Brigade 'Judea' (Reserve)" }
		1008 = { "1008th Infantry Brigade 'Yehuda' (Reserve)" }
	}
}

ISR_INF_01 =
{
	name = "Infantry Brigade"

	for_countries = { ISR }

	can_use = {	always = yes }

	division_types = { "Mech_Inf_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { ISR_INF_01 ISR_GAR_01 ISR_RES_01 ISR_ARM_01 ISR_ARM_02 ISR_PAR_01 ISR_PAR_02 }

	fallback_name = "%dth Infantry Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Infantry Brigade 'Golani'" }
		2 = { "2nd Infantry Brigade 'Carmeli'" }
		3 = { "3rd Infantry Brigade 'Alexandroni'" }
		4 = { "4th Infantry Brigade 'Kiryati'" }
		5 = { "5th Infantry Brigade 'Sharon'" }
		6 = { "6th Infantry Brigade 'Etzioni'" }
		7 = { "7th Infantry Brigade 'Saar me-Golan'" }
		8 = { "8th Infantry Brigade 'HaZaken'" }
		9 = { "9th Infantry Brigade 'Oded'" }
		10 = { "10th Infantry Brigade 'Harel'" }
		11 = { "11th Infantry Brigade 'Yiftah'" }
		12 = { "12th Infantry Brigade 'Negev'" }
		14 = { "14th Infantry Brigade 'Machatz'" }
		16 = { "16th Infantry Brigade 'Jerusalem'" }
		35 = { "35th Infantry Brigade 'Shfifon'" }
		37 = { "37th Infantry Brigade 'Ram'" }
		55 = { "55th Infantry Brigade 'Hod ha-Hanit'" }
		84 = { "84th Infantry Brigade 'Giv'ati'" }
		188 = { "188th Infantry Brigade 'Barak'" }
		205 = { "205th Infantry Brigade 'I'kvot'" }
		211 = { "211th Infantry Brigade 'Yishai'" }
		226 = { "226th infantry Brigade 'Nesher'" }
		228 = { "228th Infantry Brigade 'Alon'" }
		263 = { "263rd Infantry Brigade 'Merkavot ha-Esh'" }
		270 = { "270th Infantry Brigade 'Arava'" }
		300 = { "300th Infantry Brigade 'Bar'am'" }
		401 = { "401st Infantry Brigade 'I'kvot Ha-Barzel'" }
		406 = { "406th Infantry Brigade 'Yoav'" }
		417 = { "417th Infantry Brigade 'Emek ha-Yarden'" }
		460 = { "460th Infantry Brigade 'Bnei Or'" }
		474 = { "474th Infantry Brigade 'Golan'" }
		500 = { "500th Infantry Brigade 'Kfir'" }
		512 = { "512th Infantry Brigade 'Paran'" }
		551 = { "551st Infantry Brigade 'Hetzei ha-Esh'" }
		646 = { "646th Infantry Brigade 'Shu'ali Marom'" }
		679 = { "679th Infantry Brigade 'Yiftah'" }
		769 = { "769th Infantry Brigade 'Hiram'" }
		847 = { "847th Infantry Brigade 'Merkavot ha-I'kvot'" }
		900 = { "900th Infantry Brigade 'Kfir'" }
		933 = { "933rd Infantry Brigade 'Nahal'" }
		1001 = { "1001st Infantry Brigade 'Gefen'" }
		1002 = { "1002nd Infantry Brigade 'Katif'" }
		1003 = { "1003rd Infantry Brigade 'Menashe'" }
		1004 = { "1004th Infantry Brigade 'Ephraim'" }
		1005 = { "1005th Infantry Brigade 'Samaria'" }
		1006 = { "1006th Infantry Brigade 'Binyamin'" }
		1007 = { "1007th Infantry Brigade 'Judea'" }
		1008 = { "1008th Infantry Brigade 'Yehuda'" }
	}
}

ISR_ARM_01 =
{
	name = "Armored Brigade"

	for_countries = { ISR }

	division_types = { "armor_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { ISR_INF_01 ISR_GAR_01 ISR_RES_01 ISR_ARM_01 ISR_ARM_02 ISR_PAR_01 ISR_PAR_02 }

	fallback_name = "%dth Armor Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Armored Brigade 'Golani'" }
		2 = { "2nd Armored Brigade 'Carmeli'" }
		3 = { "3rd Armored Brigade 'Alexandroni'" }
		4 = { "4th Armored Brigade 'Kiryati'" }
		5 = { "5th Armored Brigade 'Sharon'" }
		6 = { "6th Armored Brigade 'Etzioni'" }
		7 = { "7th Armored Brigade 'Saar me-Golan'" }
		8 = { "8th Armored Brigade 'HaZaken'" }
		9 = { "9th Armored Brigade 'Oded'" }
		10 = { "10th Armored Brigade 'Harel'" }
		11 = { "11th Armored Brigade 'Yiftah'" }
		12 = { "12th Armored Brigade 'Negev'" }
		14 = { "14th Armored Brigade 'Machatz'" }
		16 = { "16th Armored Brigade 'Jerusalem'" }
		35 = { "35th Armored Brigade 'Shfifon'" }
		37 = { "37th Armored Brigade 'Ram'" }
		55 = { "55th Armored Brigade 'Hod ha-Hanit'" }
		84 = { "84th Armored Brigade 'Giv'ati'" }
		188 = { "188th Armored Brigade 'Barak'" }
		205 = { "205th Armored Brigade 'I'kvot'" }
		211 = { "211th Armored Brigade 'Yishai'" }
		226 = { "226th Armored Brigade 'Nesher'" }
		228 = { "228th Armored Brigade 'Alon'" }
		263 = { "263rd Armored Brigade 'Merkavot ha-Esh'" }
		270 = { "270th Armored Brigade 'Arava'" }
		300 = { "300th Armored Brigade 'Bar'am'" }
		401 = { "401st Armored Brigade 'I'kvot Ha-Barzel'" }
		406 = { "406th Armored Brigade 'Yoav'" }
		417 = { "417th Armored Brigade 'Emek ha-Yarden'" }
		460 = { "460th Armored Brigade 'Bnei Or'" }
		474 = { "474th Armored Brigade 'Golan'" }
		500 = { "500th Armored Brigade 'Kfir'" }
		512 = { "512th Armored Brigade 'Paran'" }
		551 = { "551st Armored Brigade 'Hetzei ha-Esh'" }
		646 = { "646th Armored Brigade 'Shu'ali Marom'" }
		679 = { "679th Armored Brigade 'Yiftah'" }
		769 = { "769th Armored Brigade 'Hiram'" }
		847 = { "847th Armored Brigade 'Merkavot ha-I'kvot'" }
		900 = { "900th Armored Brigade 'Kfir'" }
		933 = { "933rd Armored Brigade 'Nahal'" }
		1001 = { "1001st Armored Brigade 'Gefen'" }
		1002 = { "1002nd Armored Brigade 'Katif'" }
		1003 = { "1003rd Armored Brigade 'Menashe'" }
		1004 = { "1004th Armored Brigade 'Ephraim'" }
		1005 = { "1005th Armored Brigade 'Samaria'" }
		1006 = { "1006th Armored Brigade 'Binyamin'" }
		1007 = { "1007th Armored Brigade 'Judea'" }
		1008 = { "1008th Armored Brigade 'Yehuda'" }
	}
}
ISR_ARM_02 =
{
	name = "Reserve Armored Brigade"

	for_countries = { ISR }

	division_types = { "armor_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { ISR_INF_01 ISR_GAR_01 ISR_RES_01 ISR_ARM_01 ISR_ARM_02 ISR_PAR_01 ISR_PAR_02 }

	fallback_name = "%dth Armor Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Armored Brigade 'Golani' (Reserve)" }
		2 = { "2nd Armored Brigade 'Carmeli' (Reserve)" }
		3 = { "3rd Armored Brigade 'Alexandroni' (Reserve)" }
		4 = { "4th Armored Brigade 'Kiryati' (Reserve)" }
		5 = { "5th Armored Brigade 'Sharon' (Reserve)" }
		6 = { "6th Armored Brigade 'Etzioni' (Reserve)" }
		7 = { "7th Armored Brigade 'Saar me-Golan' (Reserve)" }
		8 = { "8th Armored Brigade 'HaZaken' (Reserve)" }
		9 = { "9th Armored Brigade 'Oded' (Reserve)" }
		10 = { "10th Armored Brigade 'Harel' (Reserve)" }
		11 = { "11th Armored Brigade 'Yiftah' (Reserve)" }
		12 = { "12th Armored Brigade 'Negev' (Reserve)" }
		14 = { "14th Armored Brigade 'Machatz' (Reserve)" }
		16 = { "16th Armored Brigade 'Jerusalem' (Reserve)" }
		35 = { "35th Armored Brigade 'Shfifon' (Reserve)" }
		37 = { "37th Armored Brigade 'Ram' (Reserve)" }
		55 = { "55th Armored Brigade 'Hod ha-Hanit' (Reserve)" }
		84 = { "84th Armored Brigade 'Giv'ati' (Reserve)" }
		188 = { "188th Armored Brigade 'Barak' (Reserve)" }
		205 = { "205th Armored Brigade 'I'kvot' (Reserve)" }
		211 = { "211th Armored Brigade 'Yishai' (Reserve)" }
		226 = { "226th Armored Brigade 'Nesher' (Reserve)" }
		228 = { "228th Armored Brigade 'Alon' (Reserve)" }
		263 = { "263rd Armored Brigade 'Merkavot ha-Esh' (Reserve)" }
		270 = { "270th Armored Brigade 'Arava' (Reserve)" }
		300 = { "300th Armored Brigade 'Bar'am' (Reserve)" }
		401 = { "401st Armored Brigade 'I'kvot Ha-Barzel' (Reserve)" }
		406 = { "406th Armored Brigade 'Yoav' (Reserve)" }
		417 = { "417th Armored Brigade 'Emek ha-Yarden' (Reserve)" }
		460 = { "460th Armored Brigade 'Bnei Or' (Reserve)" }
		474 = { "474th Armored Brigade 'Golan' (Reserve)" }
		500 = { "500th Armored Brigade 'Kfir' (Reserve)" }
		512 = { "512th Armored Brigade 'Paran' (Reserve)" }
		551 = { "551st Armored Brigade 'Hetzei ha-Esh' (Reserve)" }
		646 = { "646th Armored Brigade 'Shu'ali Marom' (Reserve)" }
		679 = { "679th Armored Brigade 'Yiftah' (Reserve)" }
		769 = { "769th Armored Brigade 'Hiram' (Reserve)" }
		847 = { "847th Armored Brigade 'Merkavot ha-I'kvot' (Reserve)" }
		900 = { "900th Armored Brigade 'Kfir' (Reserve)" }
		933 = { "933rd Armored Brigade 'Nahal' (Reserve)" }
		1001 = { "1001st Armored Brigade 'Gefen' (Reserve)" }
		1002 = { "1002nd Armored Brigade 'Katif' (Reserve)" }
		1003 = { "1003rd Armored Brigade 'Menashe' (Reserve)" }
		1004 = { "1004th Armored Brigade 'Ephraim' (Reserve)" }
		1005 = { "1005th Armored Brigade 'Samaria' (Reserve)" }
		1006 = { "1006th Armored Brigade 'Binyamin' (Reserve)" }
		1007 = { "1007th Armored Brigade 'Judea' (Reserve)" }
		1008 = { "1008th Armored Brigade 'Yehuda' (Reserve)" }
	}
}

ISR_PAR_01 =
{
	name = "Airborne Brigade"
	for_countries = { ISR }

	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" "L_Air_assault_Bat" "Arm_Air_assault_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { ISR_INF_01 ISR_GAR_01 ISR_RES_01 ISR_ARM_01 ISR_ARM_02 ISR_PAR_01 ISR_PAR_02 }

	fallback_name = "%dth Paratroopers Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Paratrooper Brigade 'Golani'" }
		2 = { "2nd Paratrooper Brigade 'Carmeli'" }
		3 = { "3rd Paratrooper Brigade 'Alexandroni'" }
		4 = { "4th Paratrooper Brigade 'Kiryati'" }
		5 = { "5th Paratrooper Brigade 'Sharon'" }
		6 = { "6th Paratrooper Brigade 'Etzioni'" }
		7 = { "7th Paratrooper Brigade 'Saar me-Golan'" }
		8 = { "8th Paratrooper Brigade 'HaZaken'" }
		9 = { "9th Paratrooper Brigade 'Oded'" }
		10 = { "10th Paratrooper Brigade 'Harel'" }
		11 = { "11th Paratrooper Brigade 'Yiftah'" }
		12 = { "12th Paratrooper Brigade 'Negev'" }
		14 = { "14th Paratrooper Brigade 'Machatz'" }
		16 = { "16th Paratrooper Brigade 'Jerusalem'" }
		35 = { "35th Paratrooper Brigade 'Shfifon'" }
		37 = { "37th Paratrooper Brigade 'Ram'" }
		55 = { "55th Paratrooper Brigade 'Hod ha-Hanit'" }
		84 = { "84th Paratrooper Brigade 'Giv'ati'" }
		188 = { "188th Paratrooper Brigade 'Barak'" }
		205 = { "205th Paratrooper Brigade 'I'kvot'" }
		211 = { "211th Paratrooper Brigade 'Yishai'" }
		226 = { "226th Paratrooper Brigade 'Nesher'" }
		228 = { "228th Paratrooper Brigade 'Alon'" }
		263 = { "263rd Paratrooper Brigade 'Merkavot ha-Esh'" }
		270 = { "270th Paratrooper Brigade 'Arava'" }
		300 = { "300th Paratrooper Brigade 'Bar'am'" }
		401 = { "401st Paratrooper Brigade 'I'kvot Ha-Barzel'" }
		406 = { "406th Paratrooper Brigade 'Yoav'" }
		417 = { "417th Paratrooper Brigade 'Emek ha-Yarden'" }
		460 = { "460th Paratrooper Brigade 'Bnei Or'" }
		474 = { "474th Paratrooper Brigade 'Golan'" }
		500 = { "500th Paratrooper Brigade 'Kfir'" }
		512 = { "512th Paratrooper Brigade 'Paran'" }
		551 = { "551st Paratrooper Brigade 'Hetzei ha-Esh'" }
		646 = { "646th Paratrooper Brigade 'Shu'ali Marom'" }
		679 = { "679th Paratrooper Brigade 'Yiftah'" }
		769 = { "769th Paratrooper Brigade 'Hiram'" }
		847 = { "847th Paratrooper Brigade 'Merkavot ha-I'kvot'" }
		900 = { "900th Paratrooper Brigade 'Kfir'" }
		933 = { "933rd Paratrooper Brigade 'Nahal'" }
		1001 = { "1001st Paratrooper Brigade 'Gefen'" }
		1002 = { "1002nd Paratrooper Brigade 'Katif'" }
		1003 = { "1003rd Paratrooper Brigade 'Menashe'" }
		1004 = { "1004th Paratrooper Brigade 'Ephraim'" }
		1005 = { "1005th Paratrooper Brigade 'Samaria'" }
		1006 = { "1006th Paratrooper Brigade 'Binyamin'" }
		1007 = { "1007th Paratrooper Brigade 'Judea'" }
		1008 = { "1008th Paratrooper Brigade 'Yehuda'" }
	}
}
ISR_PAR_02 =
{
	name = "Airborne Brigade"
	for_countries = { ISR }

	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" "L_Air_assault_Bat" "Arm_Air_assault_Bat" }

	# Number reservation system will tie to another group.
	link_numbering_with = { ISR_INF_01 ISR_GAR_01 ISR_RES_01 ISR_ARM_01 ISR_ARM_02 ISR_PAR_01 ISR_PAR_02 }

	fallback_name = "%dth Paratroopers Brigade"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "1st Paratrooper Brigade 'Golani' (Reserve)" }
		2 = { "2nd Paratrooper Brigade 'Carmeli' (Reserve)" }
		3 = { "3rd Paratrooper Brigade 'Alexandroni' (Reserve)" }
		4 = { "4th Paratrooper Brigade 'Kiryati' (Reserve)" }
		5 = { "5th Paratrooper Brigade 'Sharon' (Reserve)" }
		6 = { "6th Paratrooper Brigade 'Etzioni' (Reserve)" }
		7 = { "7th Paratrooper Brigade 'Saar me-Golan' (Reserve)" }
		8 = { "8th Paratrooper Brigade 'HaZaken' (Reserve)" }
		9 = { "9th Paratrooper Brigade 'Oded' (Reserve)" }
		10 = { "10th Paratrooper Brigade 'Harel' (Reserve)" }
		11 = { "11th Paratrooper Brigade 'Yiftah' (Reserve)" }
		12 = { "12th Paratrooper Brigade 'Negev' (Reserve)" }
		14 = { "14th Paratrooper Brigade 'Machatz' (Reserve)" }
		16 = { "16th Paratrooper Brigade 'Jerusalem' (Reserve)" }
		35 = { "35th Paratrooper Brigade 'Shfifon' (Reserve)" }
		37 = { "37th Paratrooper Brigade 'Ram' (Reserve)" }
		55 = { "55th Paratrooper Brigade 'Hod ha-Hanit' (Reserve)" }
		84 = { "84th Paratrooper Brigade 'Giv'ati' (Reserve)" }
		188 = { "188th Paratrooper Brigade 'Barak' (Reserve)" }
		205 = { "205th Paratrooper Brigade 'I'kvot' (Reserve)" }
		211 = { "211th Paratrooper Brigade 'Yishai' (Reserve)" }
		226 = { "226th Paratrooper Brigade 'Nesher' (Reserve)" }
		228 = { "228th Paratrooper Brigade 'Alon' (Reserve)" }
		263 = { "263rd Paratrooper Brigade 'Merkavot ha-Esh' (Reserve)" }
		270 = { "270th Paratrooper Brigade 'Arava' (Reserve)" }
		300 = { "300th Paratrooper Brigade 'Bar'am' (Reserve)" }
		401 = { "401st Paratrooper Brigade 'I'kvot Ha-Barzel' (Reserve)" }
		406 = { "406th Paratrooper Brigade 'Yoav' (Reserve)" }
		417 = { "417th Paratrooper Brigade 'Emek ha-Yarden' (Reserve)" }
		460 = { "460th Paratrooper Brigade 'Bnei Or' (Reserve)" }
		474 = { "474th Paratrooper Brigade 'Golan' (Reserve)" }
		500 = { "500th Paratrooper Brigade 'Kfir' (Reserve)" }
		512 = { "512th Paratrooper Brigade 'Paran' (Reserve)" }
		551 = { "551st Paratrooper Brigade 'Hetzei ha-Esh' (Reserve)" }
		646 = { "646th Paratrooper Brigade 'Shu'ali Marom' (Reserve)" }
		679 = { "679th Paratrooper Brigade 'Yiftah' (Reserve)" }
		769 = { "769th Paratrooper Brigade 'Hiram' (Reserve)" }
		847 = { "847th Paratrooper Brigade 'Merkavot ha-I'kvot' (Reserve)" }
		900 = { "900th Paratrooper Brigade 'Kfir' (Reserve)" }
		933 = { "933rd Paratrooper Brigade 'Nahal' (Reserve)" }
		1001 = { "1001st Paratrooper Brigade 'Gefen' (Reserve)" }
		1002 = { "1002nd Paratrooper Brigade 'Katif' (Reserve)" }
		1003 = { "1003rd Paratrooper Brigade 'Menashe' (Reserve)" }
		1004 = { "1004th Paratrooper Brigade 'Ephraim' (Reserve)" }
		1005 = { "1005th Paratrooper Brigade 'Samaria' (Reserve)" }
		1006 = { "1006th Paratrooper Brigade 'Binyamin' (Reserve)" }
		1007 = { "1007th Paratrooper Brigade 'Judea' (Reserve)" }
		1008 = { "1008th Paratrooper Brigade 'Yehuda' (Reserve)" }
	}
}
ISR_SOF_01 =
{
	name = "Special Operations Forces"

	for_countries = { ISR }



	division_types = { "Special_Forces" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { ISR_INF_01 }

	fallback_name = "Unit %d"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		269 = { "Unit %d 'Sayeret Matkal'" }
		13 = { "Unit %d 'Shayetet'" }
		5101 = { "Unit %d 'Shaldag'" }
		212 = { "Unit %d 'Maglan'" }
		217 = { "Unit %d 'Duvdevan'" }
		621 = { "Unit %d 'Egoz'" }
		685 = { "Unit %d 'Rimon'" }
		1001 = { "Yamam" }
		1002 = { "Yamas" }
	}
}