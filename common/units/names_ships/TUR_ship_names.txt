﻿##### TURKEY NAME LISTS #####

### REGULAR DESTROYER NAMES###
TUR_MODERN_DESTROYERS_NAMES = {
	name = NAME_THEME_MODERN_DESTROYERS
	for_countries = { TUR }
	type = ship
	ship_types = { destroyer stealth_destroyer }

	prefix = "TCG "
	fallback_name = "TCG Muhrip (D-%d)"

	unique = {
		"Şimşek" "Tankut" "Timur" "Toprak" "Türk" "Türker" "Üstün" "Yaman" "Yavuz" "İnanç" "İskender" "Kaan" "Kalkan" "Kartal" "Kılıç" "Kutlu" "Kuvvet" "Levent"
	}

}

### FRIGATE NAMES###
TUR_MODERN_FRIGATE_NAMES = {
	name = NAME_THEME_MODERN_FRIGATES
	for_countries = { TUR }
	type = ship
	ship_types = { frigate }

	prefix = "TCG "
	fallback_name = "TCG Fırkateyn (F-%d)"

	unique = {
		"Heyecan" "Horoz" "Hür" "Işık" "İlgi" "İnanış" "İnce" "İstiklal" "İstikrar" "Jüpiter" "Kaplan" "Kardeş" "Kaşık" "Kavga" "Kaya" "Kehribar" "Kıbrıs" "Kırlangıç" "Kırmızı" "Kuş" "Kutlu" "Lale" "Liman" "Mars" "Masal" "Mavi" "Meydan" "Mucize" "Nokta" "Okyanus" "Papatya" "Parlayan" "Parsel" "Pasifik" "Paylaşım" "Pırıl" "Pırıltı" "Pusula" "Renk" "Rüzgar" "Sahil" "Samanyolu" "Sevgi" "Sır" "Sual" "Su" "Şafak" "Şahin" "Şan" "Şarkı" "Şehir" "Şeker" "Şimşek" "Şöhret" "Tarık" "Türbe" "Uğur" "Ulus" "Umman" "Umut" "Üzüm" "Vatan" "Yalın" "Yelken" "Yenilmez" "Yıldız" "Yol" "Yüksek" "Yürek" "Zaman" "Zümrüt"
	}
}

######## Corvette Names ##########
TUR_MODERN_CORVETTE_NAMES = {
	name = NAME_THEME_MODERN_CORVETTES
	for_countries = { TUR }
	type = ship
	ship_types = { corvette }

	prefix = "TCG "
	fallback_name = "TCG Korvet (F-%d)"

	unique = {
		"Yıldırım" "Yıldız" "Zafer" "Zırhlı" "Ziya" "Ziya Gökalp" "Ahenk" "Akasya" "Akdeniz" "Aksiyon" "Alaz" "Albatros" "Altın" "Anka" "Anlatım" "Arı" "Asker" "Ay" "Aydınlatma" "Ayı" "Aykırı" "Ayna" "Bakan" "Balta" "Bardak" "Baykuş" "Başar" "Beyin" "Beylik" "Bilgi" "Birlik" "Boğa" "Bulut" "Buz" "Cemre" "Çağ" "Çalı" "Çalışma" "Çam" "Çanak" "Çınar" "Dağ" "Dalgıç" "Dalga" "Damla" "Dede" "Deniz" "Destan" "Diken" "Dil" "Dolunay" "Doru" "Dost" "Dünya" "Elmas" "Enerji" "Esinti" "Evren" "Fark" "Fırat" "Fırtına" "Fırça" "Filiz" "Gemi" "Gizem" "Gök" "Güç" "Güleç" "Güneş" "Güvercin" "Gül" "Gürültü" "Hak" "Han" "Harika" "Hedef"
	}
}


### BATTLESHIP NAMES ###
TUR_BB_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_BB

	for_countries = { TUR }

	type = ship
	ship_types = { ship_hull_heavy battleship }

	prefix = "TCG "
	fallback_name = "TCG Savaş Gemisi (BB-%d)"

	unique = {
		"Mehmetçik" "Öncü" "Özgür" "Pars" "Poyraz" "Rüzgar" "Sancak" "Savaş" "Seçkin"
	}
}

### CRUISER NAMES ###
TUR_MODERN_CRUISERS_NAMES = {
	name = NAME_THEME_MODERN_CRUISERS
	for_countries = { TUR }
	type = ship
	ship_types = { cruiser battle_cruiser }

	prefix = "TCG "
	fallback_name = "Muharebe Kruvazör (CGN-%d)"
	unique = {
		"Göktürk" "Görkem" "Güçlü" "Gür" "Hakan" "Harun" "Hızır" "İlgün" "İlhan"
	}

}

### AIRCRAFT CARRIER NAMES ###
TUR_CV_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CARRIERS

	for_countries = { TUR }

	type = ship
	ship_types = { carrier }

	prefix = "TCG "
	fallback_name = "TCG Uçak Gemisi (CV-%d)"

	unique = {
		"Atatürk" "Çakmak" "İnönü"
	}
}

### LHA NAMES ###
TUR_LHA_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_HELICOPTER_CARRIERS

	for_countries = { TUR }

	type = ship
		ship_types = { helicopter_operator }

	prefix = "TCG "
	fallback_name = "TCG Amfibi Hücum Gemisi (L-%d)"

	unique = {
		"Akın" "Alparslan" "Altay" "Anadolu" "Aslan" "Ateş" "Barbaros" "Berk" "Beyaz" "Bozkurt" "Cengiz" "Çelik" "Çılgın" "Demir" "Doruk" "Ejder" "Fırtına" "Gazi"
	}
}

### SUBMARINES ###
TUR_MODERN_SUBMARINES_NAMES = {
	name = NAME_THEME_MODERN_SUBMARINES
	for_countries = { TUR }
	type = ship
	ship_types = { attack_submarine }

	prefix = "TCG "
	fallback_name = "TCG Denizaltı (S-%d) "

	unique = {
		"Gür (S-357)" "Çanakkale (S-358)" "Burak Reis (S-359)" "Birinci İnönü (S-360)" "Piri Reis (S-330)" "Hızır Reis (S-331)" "Murat Reis (S-332)" "Aydın Reis (S-333)" "Seydi Ali Reis (S-334)" "Selman Reis (S-335)"
	}
}

### MISSILE SUBMARINES ###
TUR_MISSILE_SUBMARINES_NAMES = {
	name = NAME_THEME_MODERN_MISSILE_SUBMARINES
	for_countries = { TUR }
	type = ship
	ship_types = { missile_submarine }

	prefix = "TUR "
	fallback_name = "TCG Nükleer Denizaltı (ND-%d)"

}

### THEME: TURKEY CITIES ###
TUR_STATES = {
	name = NAME_THEME_CITIES

	for_countries = { TUR }

	type = ship

	prefix = "TCG "

	unique = {
		"Adana" "Adıyaman" "Afyonkarahisar" "Ağrı" "Tekirdağ" "Aksaray" "Amasya" "Ankara" "Antalya" "Ardahan" "Artvin" "Aydın" "Balıkesir" "Bartın" "Batman"
		"Bayburt" "Bilecik" "Bingöl" "Bitlis" "Mersin" "Bolu" "Burdur" "Bursa" "Çanakkale"
		"Çankırı" "Çorum" "Denizli" "Diyarbakır" "Düzce" "Edirne" "Elazığ" "Erzincan" "Erzurum" "Eskişehir" "Gaziantep"
		"Giresun" "Gümüşhane" "Hakkâri" "Hatay" "Iğdır" "Isparta" "İstanbul" "İzmir" "Kars" "Kastamonu" "Kayseri" "Kırklareli" "Kırşehir" "Kocaeli" "Konya" "Kütahya"
		"Malatya" "Manisa" "Kahramanmaraş" "Mardin" "Muğla" "Muş" "Nevşehir" "Niğde" "Ordu" "Rize" "Sakarya" "Samsun" "Siirt" "Sinop" "Sivas" "Tekirdağ" "Tokat"
		"Trabzon" "Tunceli" "Şanlıurfa" "Uşak" "Van" "Yozgat" "Zonguldak"
	}
}

### THEME: HISTORICAL LEADERS ###
TUR_LEADERS = {
	name = NAME_THEME_LEADERS

	for_countries = { USA }

	type = ship

	prefix = "TCG "

	unique = {
		"Mustafa Kemal Atatürk" "İsmet İnönü" "Celâl Bayar" "Turgut Özal"
	}
}

### THEME: HISTORICAL BATTLES ###
TUR_BATTLES = {
	name = NAME_THEME_BATTLES

	for_countries = { TUR }

	type = ship

	prefix = "TCG "

	unique = {
		"Dandanakan" "Pasinler" "Malazgirt" "Harran" "Dorileon" "Miryokefelon" "Yassı Çimen" "Kösedağ" "Bursa" "Sırp Sındığı" "I. Kosova" "Niğbolu" "Ankara" "Varna" "II. Kosova"
		"İstanbul" "Otlukbeli" "Mercidabık" "Ridaniye" "I. Viyana" "Kıbrıs" "Bağdat" "II. Viyana" "Kırım" "93 Harbi" "Çanakkale" "Kut'ül Amare" "Kore"
	}
}
