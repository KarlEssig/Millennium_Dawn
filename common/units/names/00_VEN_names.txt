VEN = {
	air_wing_names_template = AIR_WING_NAME_VEN_FALLBACK

	#Air wings can only be named through archetype
	small_plane_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	small_plane_suicide_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	small_plane_strike_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	small_plane_cas_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_small_plane_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_small_plane_suicide_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_small_plane_strike_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_small_plane_cas_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	medium_plane_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	medium_plane_suicide_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_medium_plane_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_medium_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	cv_medium_plane_scout_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	large_plane_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	large_plane_cas_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	large_plane_awacs_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	large_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	large_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	attack_helicopter_hull = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
	guided_missile_equipment = {
		prefix = ""
		generic = { "Ala Aerea" }
		generic_pattern = AIR_WING_NAME_VEN_GENERIC
		unique = {
		}
	}
}
