characters = {

	#FIELD MARSHALS
	USA_mark_milley = {
		name = "Mark Milley"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Mark_Miley_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Mark_Miley.dds" }
		}
		# New England
		field_marshal = {
			traits = { new_england_soldier logistics_wizard offensive_doctrine }
			skill = 4
			attack_skill = 6
			defense_skill = 2
			planning_skill = 4
			logistics_skill = 1
		}
	}

	USA_tommy_franks = {
		name = "Tommy R. Franks"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Tommy_Franks_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Tommy_Franks.dds" }
		}
		# Southerner
		field_marshal = {
			traits = { southern_soldier career_officer kind_leader artillery_officer artillery_leader organizer thorough_planner }
			skill = 5
			attack_skill = 4
			defense_skill = 5
			planning_skill = 4
			logistics_skill = 3
		}
	}

	USA_Bantz_J_Craddock = {
		name = "Bantz J. Craddock"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Bantz_J_Craddock_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Bantz_J_Craddock.dds" }
		}
		# Federalist
		field_marshal = {
			traits = { loyalist_soldier old_guard organizer armor_officer thorough_planner }
			skill = 6
			attack_skill = 4
			defense_skill = 4
			planning_skill = 6
			logistics_skill = 5
		}
	}

	USA_John_P_Abizaid = {
		name = "John P. Abizaid"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_John_P_Abizaid_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_John_P_Abizaid.dds" }
		}
		# California
		field_marshal = {
			traits = { california_soldier old_guard career_officer infantry_leader ranger adaptable }
			skill = 6
			attack_skill = 7
			defense_skill = 5
			planning_skill = 5
			logistics_skill = 2
		}
	}

	USA_James_L_Jones_Jr = {
		name = "James L. Jones Jr."
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_James_L_Jones_Jr_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_James_L_Jones_Jr.dds" }
		}
		# Midwesterner
		field_marshal = {
			traits = { midwest_soldier career_officer old_guard organizer organizer_expert thorough_planner naval_invader jungle_rat }
			skill = 8
			attack_skill = 4
			defense_skill = 6
			planning_skill = 8
			logistics_skill = 7
		}
	}

	USA_Jose_J_Reyes = {
		name = "Jose J. Reyes"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Jose_J_Reyes_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Jose_J_Reyes_Jr.dds" }
		}
		# Puerto Rico
		field_marshal = {
			traits = { puerto_rican_soldier organizer artillery_leader artillery_officer }
			skill = 3
			attack_skill = 2
			defense_skill = 3
			planning_skill = 4
			logistics_skill = 2
		}
	}

	USA_Eric_K_Shinseki = {
		name = "Eric K. Shinseki"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Eric_K_Shinseki_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Eric_K_Shinseki.dds" }
		}
		# Pacific Islander
		field_marshal = {
			traits = { pacific_islander_soldier career_officer armor_officer artillery_leader armoured_cavalry_leader }
			skill = 6
			attack_skill = 3
			defense_skill = 5
			planning_skill = 6
			logistics_skill = 5
		}
	}

	USA_John_M_Keane = {
		name = "John M. \"Jack\" Keane"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_John_M_Keane_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_John_M_Keane.dds" }
		}
		# New York
		field_marshal = {
			traits = { new_yorker_soldier career_officer infantry_officer jungle_rat ranger infantry_leader air_cavalry_leader  }
			skill = 6
			attack_skill = 7
			defense_skill = 4
			planning_skill = 5
			logistics_skill = 3
		}
	}

	USA_Vincent_K_Brooks = {
		name = "Vincent K. Brooks"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Vincent_K_Brooks_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Vincent_K_Brooks.dds" }
		}
		# Alaska
		field_marshal = {
			traits = { alaskan_soldier career_officer infantry_leader ranger commando inspirational_leader }
			skill = 3
			attack_skill = 4
			defense_skill = 2
			planning_skill = 3
			logistics_skill = 1
		}
	}

	USA_Billy_K_Soloman = {
		name = "Billy K. Soloman"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Billy_K_Soloman_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Billy_K_Soloman.dds" }
		}
		# Texas
		field_marshal = {
			traits = { texan_soldier career_officer organizer logistics_wizard }
			skill = 2
			attack_skill = 1
			defense_skill = 1
			planning_skill = 2
			logistics_skill = 3
		}
	}

	USA_James_C_King = {
		name = "James C. King"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_James_C_King_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_James_C_King.dds" }
		}
		# Alaska
		field_marshal = {
			traits = { rocky_mountain_soldier inspirational_leader }
			skill = 2
			attack_skill = 1
			defense_skill = 2
			planning_skill = 3
			logistics_skill = 1
		}
	}

	USA_Gregory_B_Gardner = {
		name = "Gregory B. Gardner"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Gregory_B_Gardner_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Gregory_B_Gardner.dds" }
		}
		# Great Plains
		field_marshal = {
			traits = { great_plains_soldier career_officer organizer organizer_expert thorough_planner }
			skill = 2
			attack_skill = 1
			defense_skill = 1
			planning_skill = 4
			logistics_skill = 1
		}
	}

	#GENERALS
	USA_Glenn_M_Walters = {
		name = "Glenn M. Walters"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Glenn_M_Walters_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Glenn_M_Walters.dds" }
		}
		# Federalist
		corps_commander = {
			traits = { loyalist_soldier naval_invader commando swamp_fox }
			skill = 3
			attack_skill = 3
			defense_skill = 3
			planning_skill = 2
			logistics_skill = 2
		}
	}

	USA_Robert_Abrams = {
		name = "Robert B. Abrams"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Robert_Abrams_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Robert_Abrams.dds" }
		}
		# Federalist
		corps_commander = {
			traits = { loyalist_soldier panzer_leader }
			skill = 4
			attack_skill = 6
			defense_skill = 2
			planning_skill = 4
			logistics_skill = 1
		}
	}

	USA_Richard_Clarke = {
		name = "Richard Clarke"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Richard_Clarke_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Richard_Clarke.dds" }
		}
		# Federalist
		corps_commander = {
			traits = { loyalist_soldier commando ranger }
			skill = 4
			attack_skill = 4
			defense_skill = 4
			planning_skill = 2
			logistics_skill = 3
		}
	}

	USA_John_Nicholson = {
		name = "John W. Nicholson, Jr"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_John_Nicholson_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_John_Nicholson.dds" }
		}
		# Federalist
		corps_commander = {
			traits = { loyalist_soldier desert_fox hill_fighter }
			skill = 4
			attack_skill = 5
			defense_skill = 4
			planning_skill = 3
			logistics_skill = 1
		}
	}

	USA_joseph_dunford = {
		name = "Joseph Dunford"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_joseph_dunford_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_joseph_dunford.dds" }
		}
		# New England
		corps_commander = {
			traits = { new_england_soldier inspirational_leader offensive_doctrine }
			skill = 4
			attack_skill = 6
			defense_skill = 3
			planning_skill = 3
			logistics_skill = 1
		}
	}

	USA_Leon_J_LaPorte = {
		name = "Leon J. LaPorte"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Leon_J_LaPorte_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Leon_J_LaPorte.dds" }
		}
		# New England
		corps_commander = {
			traits = { new_england_soldier infantry_officer air_cavalry_leader organizer ranger desert_fox }
			skill = 5
			attack_skill = 6
			defense_skill = 5
			planning_skill = 3
			logistics_skill = 2
		}
	}

	USA_Gary_J_Volesky = {
		name = "Gary J. Volesky"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Gary_J_Volesky_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Gary_J_Volesky.dds" }
		}
		# Cascadian
		corps_commander = {
			traits = { cascadian_soldier commando hill_fighter }
			skill = 3
			attack_skill = 2
			defense_skill = 4
			planning_skill = 1
			logistics_skill = 3
		}
	}

	USA_James_Mattis = {
		name = "James N. Mattis"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_James_Mattis_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_James_Mattis.dds" }
		}
		# Cascadian
		corps_commander = {
			traits = { cascadian_soldier career_officer naval_invader adaptable }
			skill = 5
			attack_skill = 6
			defense_skill = 3
			planning_skill = 5
			logistics_skill = 2
		}
	}

	USA_Jeffrey_L_Bannister = {
		name = "Jeffrey L. Bannister"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Jeffrey_L_Bannister_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Jeffrey_L_Bannister.dds" }
		}
		# California
		corps_commander = {
			traits = { california_soldier commando desert_fox trait_mountaineer }
			skill = 4
			attack_skill = 5
			defense_skill = 4
			planning_skill = 2
			logistics_skill = 2
		}
	}

	USA_David_Petraeus = {
		name = "David H. Petraeus"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_David_Petraeus_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_David_Petraeus.dds" }
		}
		# New York
		corps_commander = {
			traits = { new_yorker_soldier commando skilled_staffer }
			skill = 3
			attack_skill = 2
			defense_skill = 1
			planning_skill = 4
			logistics_skill = 3
		}
	}

	USA_William_J_Lennox_Jr = {
		name = "William J. Lennox Jr."
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_William_J_Lennox_Jr_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_William_J_Lennox_Jr.dds" }
		}
		# New York
		corps_commander = {
			traits = { new_yorker_soldier artillery_officer artillery_leader ranger organizer }
			skill = 3
			attack_skill = 1
			defense_skill = 4
			planning_skill = 3
			logistics_skill = 2
		}
	}

	USA_Kevin_P_Byrnes = {
		name = "Kevin P. Byrnes"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Kevin_P_Byrnes_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Kevin_P_Byrnes.dds" }
		}
		# New York
		corps_commander = {
			traits = { new_yorker_soldier artillery_officer artillery_leader jungle_rat }
			skill = 3
			attack_skill = 2
			defense_skill = 1
			planning_skill = 4
			logistics_skill = 3
		}
	}
	
	USA_Paul_T_Mikolashek = {
		name = "Paul T. Mikolashek"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Paul_T_Mikolashek_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Paul_T_Mikolashek.dds" }
		}
		# Midwesterner
		corps_commander = {
			traits = { midwest_soldier career_officer desperate_defender air_cavalry_leader }
			skill = 3
			attack_skill = 2
			defense_skill = 4
			planning_skill = 5
			logistics_skill = 2
		}
	}

	USA_Timothy_Joseph_Maude = {
		name = "Timothy Joseph Maude"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Timothy_Joseph_Maude_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Timothy_Joseph_Maude.dds" }
		}
		# Midwesterner
		corps_commander = {
			traits = { midwest_soldier organizer }
			skill = 2
			attack_skill = 1
			defense_skill = 1
			planning_skill = 4
			logistics_skill = 1
		}
	}

	USA_Joseph_Keith_Kellogg_Jr = {
		name = "Joseph Keith Kellogg Jr."
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Joseph_Keith_Kellogg_Jr_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Joseph_Keith_Kellogg_Jr.dds" }
		}
		# Midwesterner
		corps_commander = {
			traits = { midwest_soldier career_officer air_cavalry_leader organizer jungle_rat }
			skill = 4
			attack_skill = 4
			defense_skill = 3
			planning_skill = 4
			logistics_skill = 2
		}
	}

	USA_James_T_Hill = {
		name = "James T. Hill"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_James_T_Hill_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_James_T_Hill.dds" }
		}
		# Midwesterner
		corps_commander = {
			traits = { midwest_soldier infantry_officer air_cavalry_leader ranger kind_leader }
			skill = 4
			attack_skill = 3
			defense_skill = 5
			planning_skill = 3
			logistics_skill = 2
		}
	}

	USA_Henry_H_Shelton = {
		name = "Henry H. Shelton"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Henry_H_Shelton_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Henry_H_Shelton.dds" }
		}
		# Southerner
		corps_commander = {
			traits = { southern_soldier career_officer air_cavalry_officer air_cavalry_leader jungle_rat fast_planner  }
			skill = 4
			attack_skill = 5
			defense_skill = 3
			planning_skill = 3
			logistics_skill = 2
		}
	}

	USA_Dan_K_McNeill = {
		name = "Dan K. McNeill"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Dan_K_McNeill_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Dan_K_McNeill.dds" }
		}
		# Southerner
		corps_commander = {
			traits = { southern_soldier trait_reckless harsh_leader }
			skill = 3
			attack_skill = 5
			defense_skill = 1
			planning_skill = 4
			logistics_skill = 3
		}
	}

	USA_Joseph_M_Cosumano = {
		name = "Joseph M. Cosumano"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Joseph_M_Cosumano_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Joseph_M_Cosumano.dds" }
		}
		# Southerner
		corps_commander = {
			traits = { southern_soldier desperate_defender }
			skill = 3
			attack_skill = 1
			defense_skill = 5
			planning_skill = 2
			logistics_skill = 2
		}
	}

	USA_Victor_S_Perez = {
		name = "Victor S. Perez"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Victor_S_Perez_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Victor_S_Perez.dds" }
		}
		# Puerto Rico
		corps_commander = {
			traits = { puerto_rican_soldier organizer organizer_expert infantry_leader }
			skill = 3
			attack_skill = 1
			defense_skill = 2
			planning_skill = 4
			logistics_skill = 3
		}
	}

	USA_Irene_M_Zoppi = {
		name = "Irene M. Zoppi"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Irene_M_Zoppi_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Irene_M_Zoppi.dds" }
		}
		# Puerto Rico
		corps_commander = {
			traits = { puerto_rican_soldier organizer }
			skill = 2
			attack_skill = 1
			defense_skill = 1
			planning_skill = 3
			logistics_skill = 2
		}
	}

	USA_Emilio_Diaz_Colon = {
		name = "Emilio Diaz Colon"
		portraits = {
			army = { small = "gfx/leaders/USA/small/Zgen_Emilio_Diaz_Colon_small.dds" }
			army = { large = "gfx/leaders/USA/Zgeneral_Emilio_Diaz_Colon.dds" }
		}
		# Puerto Rico
		corps_commander = {
			traits = { puerto_rican_soldier engineer_officer trait_engineer }
			skill = 2
			attack_skill = 1
			defense_skill = 1
			planning_skill = 2
			logistics_skill = 3
		}
	}

	#ADMIRALS
	USA_John_Richardson = {
		name = "John M. Richardson"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/John_Richardson_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_John_Richardson.dds"
			}
		}
		navy_leader = {
			traits = { gentlemanly superior_tactician loyalist_soldier }
			skill = 6
			attack_skill = 6
			defense_skill = 4
			maneuvering_skill = 5
			coordination_skill = 4
		}
	}

	USA_William_Moran = {
		name = "William F. Moran"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/William_Moran_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_William_Moran.dds"
			}
		}
		navy_leader = {
			traits = { superior_tactician navy_career_officer new_yorker_soldier }
			skill = 6
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 6
			coordination_skill = 1
		}
	}

	USA_James_Caldwell = {
		name = "James F. Caldwell"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/James_Caldwell_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_James_Caldwell.dds"
			}
		}
		navy_leader = {
			traits = { cruiser_adherent superior_tactician midwest_soldier }
			skill = 6
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 6
			coordination_skill = 1
		}
	}

	USA_Philip_Davidson = {
		name = "Philip S. Davidson"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Philip_Davidson_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_Philip_Davidson.dds"
			}
		}
		navy_leader = {
			traits = { superior_tactician midwest_soldier }
			skill = 5
			attack_skill = 4
			defense_skill = 6
			maneuvering_skill = 4
			coordination_skill = 2
		}
	}

	USA_Michelle_Howard = {
		name = "Michelle J. Howard"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Michelle_Howard_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_Michelle_Howard.dds"
			}
		}
		navy_leader = {
			traits = { spotter california_soldier }
			skill = 5
			attack_skill = 2
			defense_skill = 2
			maneuvering_skill = 6
			coordination_skill = 6
		}
	}

	USA_Scott_Swift = {
		name = "Scott H. Swift"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Scott_Swift_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_Scott_Swift.dds"
			}
		}
		navy_leader = {
			traits = { air_controller pacific_islander_soldier }
			skill = 5
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 4
			coordination_skill = 4
		}
	}

	USA_Robert_Gaucher = {
		name = "Robert Gaucher"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/adm_Robert_Gaucher_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_Robert_Gaucher.dds"
			}
		}
		navy_leader = {
			traits = { seawolf new_england_soldier }
			skill = 5
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 4
			coordination_skill = 4
		}
	}

	USA_Nora_Tyson = {
		name = "Nora Tyson"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/adm_Nora_Tyson_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_Nora_Tyson.dds"
			}
		}
		navy_leader = {
			traits = { air_controller southern_soldier }
			skill = 5
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 4
			coordination_skill = 4
		}
	}

	USA_Fred_Kacher = {
		name = "Fred Kacher"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Adm_Fred_Kacher_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_Fred_Kacher.dds"
			}
		}
		navy_leader = {
			traits = { fly_swatter loyalist_soldier }
			skill = 5
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 4
			coordination_skill = 4
		}
	}

	USA_William_Triplett = {
		name = "William Triplet"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Adm_William_Triplett_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_William_Triplett.dds"
			}
		}
		navy_leader = {
			traits = { fly_swatter loyalist_soldier }
			skill = 5
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 4
			coordination_skill = 4
		}
	}

	USA_Fernandez_Ponds = {
		name = "Fernandez Ponds"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/adm_Fernandez_Ponds_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_Fernandez_Ponds.dds"
			}
		}
		navy_leader = {
			traits = { blockade_runner southern_soldier }
			skill = 5
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 4
			coordination_skill = 4
		}
	}

	USA_Nils_Wirstrom = {
		name = "Nils Wirstrom"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/adm_Nils_Wirstrom_small.dds"
			}
			army = {
				large = "gfx/leaders/USA/Portrait_Admiral_Nils_Wirstrom.dds"
			}
		}
		navy_leader = {
			traits = { blockade_runner loyalist_soldier }
			skill = 5
			attack_skill = 4
			defense_skill = 4
			maneuvering_skill = 4
			coordination_skill = 4
		}
	}

	#ARMY CHIEFS
	USA_Robert_Neller = {
		name = "Robert B. Neller"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Robert_Neller_small.dds"

				large = "gfx/leaders/USA/Portrait_Robert_Neller.dds"
			}
		}
		advisor = {
			slot = army_chief
			idea_token = robert_neller
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				army_chief_reform_3
			}
			 cost = 150
			ai_will_do = {
				factor = 1
			}
		}
		# Southern Republic of America
		corps_commander = {
			traits = { organizer_expert logistics_wizard aggressive_assaulter southern_soldier }
			skill = 5
			attack_skill = 3
			defense_skill = 1
			planning_skill = 6
			logistics_skill = 6
		}
	}

	USA_Joseph_Votel = {
		name = "Joseph L. Votel"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Joseph_Votel_small.dds"

				large = "gfx/leaders/USA/Portrait_Joseph_Votel.dds"
			}
		}
		advisor = {
			slot = army_chief
			idea_token = joseph_votel
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				army_chief_planning_3
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_Gen_James_E_Rainey = {
		name = "Gen James E. Rainey"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Gen_James_E_Rainey_small.dds"

				large = "gfx/leaders/USA/Portrait_James_E_Rainey.dds"
			}
		}
		advisor = {
			slot = army_chief
			idea_token = Joseph_Votel
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				army_chief_morale_3
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	#NAVY CHIEFS

	USA_James_Loeblein = {
		name = "James Loeblein"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/adm_James_Loeblein_small.dds"

				large = "gfx/leaders/USA/Portrait_Admiral_James_Loeblein.dds"
			}
		}
		advisor = {
			slot = navy_chief
			idea_token = james_loeblein
			ledger = navy
			allowed = {
				original_tag = USA
			}
			traits = {
				navy_chief_reform_3
			}
			 cost = 150
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_John_Alexander = {
		name = "John Alexander"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/adm_John_Alexander_small.dds"

				large = "gfx/leaders/USA/Portrait_Admiral_John_Alexander.dds"
			}
		}
		advisor = {
			slot = navy_chief
			idea_token = john_alexander
			ledger = navy
			allowed = {
				original_tag = USA
			}
			traits = {
				navy_chief_maneuver_3
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_Joseph_Aucoin = {
		name = "Joseph Aucoin"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/adm_Joseph_Aucoin_small.dds"

				large = "gfx/leaders/USA/Portrait_Admiral_Joseph_Aucoin.dds"
			}
		}
		advisor = {
			slot = navy_chief
			idea_token = joseph_aucoin
			ledger = navy
			allowed = {
				original_tag = USA
			}
			traits = {
				navy_chief_naval_aviation_3
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	# AIR CHIEFS

	USA_Ryan_Gonsalves = {
		name = "Ryan Gonsalves"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Gen_Ryan_Gonsalves_small.dds"

				large = "gfx/leaders/USA/Portrait_Ryan_Gonsalves.dds"
			}
		}
		advisor = {
			slot = air_chief
			idea_token = ryan_gonsalves
			ledger = air
			allowed = {
				original_tag = USA
			}
			traits = {
				air_chief_reform_3
			}
			 cost = 150
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_Theodore_Martin = {
		name = "Theodore Martin"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/gen_Theodore_Martin_small.dds"

				large = "gfx/leaders/USA/Portrait_Theodore_Martin.dds"
			}
		}
		advisor = {
			slot = air_chief
			idea_token = theodore_martin
			ledger = air
			allowed = {
				original_tag = USA
			}
			traits = {
				air_air_superiority_3
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_Charles_Flynn = {
		name = "Charles Flynn"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/gen_Charles_Flynn_small.dds"

				large = "gfx/leaders/USA/Portrait_Charles_Flynn.dds"
			}
		}
		advisor = {
			slot = air_chief
			idea_token = charles_flynn
			ledger = air
			allowed = {
				original_tag = USA
			}
			traits = {
				air_pilot_training_3
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	#HIGH COMMAND

	USA_Thomas_D_Waldhauser = {
		name = "Thomas D. Waldhauser"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/gen_Thomas_D_Waldhauser_small.dds"

				large = "gfx/leaders/USA/Portrait_Thomas_D_Waldhauser.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = thomas_d_waldhauser
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				army_armored_2
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_John_Thomson = {
		name = "John Thomson III"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/gen_John_Thomson_III_small.dds"

				large = "gfx/leaders/USA/Portrait_John_Thomson_III.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = John_Thomson
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				army_entrenchment_2
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_Vincent_Brooks = {
		name = "Vincent K. Brooks"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Vincent_Brooks_small.dds"

				large = "gfx/leaders/USA/Portrait_Vincent_Brooks.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = vincent_brooks
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				army_artillery_2
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_Robert_Brown = {
		name = "Robert B. Brown"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Robert_Brown_small.dds"

				large = "gfx/leaders/USA/Portrait_Robert_Brown.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = robert_brown
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				army_concealment_2
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_Harry_Harris = {
		name = "Harry B. Harris, Jr"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Harry_Harris_small.dds"

				large = "gfx/leaders/USA/Portrait_Harry_Harris.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = harry_harris
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				navy_amphibious_assault_2
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_Kurt_Tidd = {
		name = "Kurt Tidd"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Kurt_Tidd_small.dds"

				large = "gfx/leaders/USA/Portrait_Kurt_Tidd.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = kurt_tidd
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				air_force_multiplier_2
			}
			 cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}

	USA_James_Bynum = {
		name = "James Bynum"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/adm_James_Bynum_small.dds"

				large = "gfx/leaders/USA/Portrait_Admiral_James_Bynum.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = james_bynum
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				navy_carrier_3
			}
			 cost = 125
			ai_will_do = {
				factor = 1
			}
		}
	}


	USA_Curtis_Scaparrotti = {
		name = "Curtis Scaparrotti"
		portraits = {
			army = {
				small = "gfx/leaders/USA/small/Gen_Curtis_Scaparrotti_small.dds"

				large = "gfx/leaders/USA/Portrait_Curtis_Scaparrotti.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = curtis_scaparrotti
			ledger = army
			allowed = {
				original_tag = USA
			}
			traits = {
				army_commando_3
			}
			 cost = 125
			ai_will_do = {
				factor = 1
			}
		}
	}
}
