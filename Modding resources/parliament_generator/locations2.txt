<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns:svg="http://www.w3.org/2000/svg"
xmlns="http://www.w3.org/2000/svg" version="1.1"
width="360" height="185">
<!-- Created with the Wikimedia parliament diagram creator (http://parliamentdiagram.toolforge.org/parliamentinputform.html) -->
<g>
<text x="175" y="175" style="font-size:36px;font-weight:bold;text-align:center;text-anchor:middle;font-family:sans-serif">245</text>
  <g style="fill:#FF9933; stroke:#000000" id="BharatiyaJanataParty">
    <circle cx="11.32" cy="175.00" r="5.00"/>
    <circle cx="23.83" cy="175.00" r="5.00"/>
    <circle cx="36.34" cy="175.00" r="5.00"/>
    <circle cx="48.85" cy="175.00" r="5.00"/>
    <circle cx="61.36" cy="175.00" r="5.00"/>
    <circle cx="73.87" cy="175.00" r="5.00"/>
    <circle cx="86.38" cy="175.00" r="5.00"/>
    <circle cx="98.90" cy="175.01" r="5.00"/>
    <circle cx="12.18" cy="162.35" r="5.00"/>
    <circle cx="24.79" cy="162.04" r="5.00"/>
    <circle cx="37.37" cy="162.06" r="5.00"/>
    <circle cx="49.98" cy="162.08" r="5.00"/>
    <circle cx="62.61" cy="162.10" r="5.00"/>
    <circle cx="75.26" cy="162.14" r="5.00"/>
    <circle cx="13.98" cy="149.79" r="5.00"/>
    <circle cx="87.96" cy="162.18" r="5.00"/>
    <circle cx="26.81" cy="149.21" r="5.00"/>
    <circle cx="39.57" cy="149.26" r="5.00"/>
    <circle cx="100.71" cy="162.24" r="5.00"/>
    <circle cx="52.38" cy="149.33" r="5.00"/>
    <circle cx="16.71" cy="137.41" r="5.00"/>
    <circle cx="65.26" cy="149.42" r="5.00"/>
    <circle cx="29.90" cy="136.59" r="5.00"/>
    <circle cx="78.21" cy="149.53" r="5.00"/>
    <circle cx="42.92" cy="136.72" r="5.00"/>
    <circle cx="91.28" cy="149.69" r="5.00"/>
    <circle cx="20.37" cy="125.26" r="5.00"/>
    <circle cx="56.03" cy="136.88" r="5.00"/>
    <circle cx="34.03" cy="124.27" r="5.00"/>
    <circle cx="69.27" cy="137.09" r="5.00"/>
    <circle cx="104.52" cy="149.92" r="5.00"/>
    <circle cx="47.39" cy="124.53" r="5.00"/>
    <circle cx="24.94" cy="113.43" r="5.00"/>
    <circle cx="82.67" cy="137.38" r="5.00"/>
    <circle cx="60.90" cy="124.85" r="5.00"/>
    <circle cx="39.16" cy="112.33" r="5.00"/>
    <circle cx="96.30" cy="137.78" r="5.00"/>
    <circle cx="74.61" cy="125.28" r="5.00"/>
    <circle cx="30.37" cy="101.97" r="5.00"/>
    <circle cx="52.93" cy="112.79" r="5.00"/>
    <circle cx="45.27" cy="100.87" r="5.00"/>
    <circle cx="66.92" cy="113.36" r="5.00"/>
    <circle cx="88.58" cy="125.86" r="5.00"/>
    <circle cx="110.23" cy="138.36" r="5.00"/>
    <circle cx="36.66" cy="90.95" r="5.00"/>
    <circle cx="59.51" cy="101.59" r="5.00"/>
    <circle cx="81.20" cy="114.12" r="5.00"/>
    <circle cx="102.90" cy="126.67" r="5.00"/>
    <circle cx="52.31" cy="89.95" r="5.00"/>
    <circle cx="43.75" cy="80.44" r="5.00"/>
    <circle cx="74.05" cy="102.53" r="5.00"/>
    <circle cx="95.84" cy="115.15" r="5.00"/>
    <circle cx="67.08" cy="91.04" r="5.00"/>
    <circle cx="117.70" cy="127.84" r="5.00"/>
    <circle cx="88.97" cy="103.75" r="5.00"/>
    <circle cx="60.23" cy="79.65" r="5.00"/>
    <circle cx="51.61" cy="70.49" r="5.00"/>
    <circle cx="82.22" cy="92.45" r="5.00"/>
    <circle cx="110.96" cy="116.57" r="5.00"/>
    <circle cx="75.57" cy="81.22" r="5.00"/>
    <circle cx="104.35" cy="105.39" r="5.00"/>
    <circle cx="68.99" cy="70.05" r="5.00"/>
    <circle cx="60.20" cy="61.15" r="5.00"/>
    <circle cx="97.82" cy="94.28" r="5.00"/>
    <circle cx="91.34" cy="83.22" r="5.00"/>
    <circle cx="84.91" cy="72.20" r="5.00"/>
    <circle cx="126.74" cy="118.64" r="5.00"/>
    <circle cx="69.47" cy="52.49" r="5.00"/>
    <circle cx="78.50" cy="61.20" r="5.00"/>
    <circle cx="120.34" cy="107.68" r="5.00"/>
    <circle cx="113.98" cy="96.75" r="5.00"/>
    <circle cx="107.65" cy="85.84" r="5.00"/>
    <circle cx="101.33" cy="74.94" r="5.00"/>
    <circle cx="79.36" cy="44.55" r="5.00"/>
    <circle cx="95.02" cy="64.06" r="5.00"/>
    <circle cx="88.73" cy="53.18" r="5.00"/>
    <circle cx="89.81" cy="37.37" r="5.00"/>
    <circle cx="137.11" cy="110.99" r="5.00"/>
    <circle cx="130.85" cy="100.16" r="5.00"/>
    <circle cx="124.60" cy="89.34" r="5.00"/>
    <circle cx="118.34" cy="78.51" r="5.00"/>
    <circle cx="112.09" cy="67.69" r="5.00"/>
    <circle cx="105.83" cy="56.86" r="5.00"/>
    <circle cx="99.58" cy="46.04" r="5.00"/>
    <circle cx="100.78" cy="31.00" r="5.00"/>
  </g>
  <g style="fill:#007500; stroke:#000000" id="AllIndiaAnnaDravidaMunnetraKazhagam">
    <circle cx="110.99" cy="39.82" r="5.00"/>
    <circle cx="117.24" cy="50.67" r="5.00"/>
    <circle cx="123.50" cy="61.53" r="5.00"/>
    <circle cx="129.77" cy="72.40" r="5.00"/>
    <circle cx="136.03" cy="83.27" r="5.00"/>
    <circle cx="142.30" cy="94.16" r="5.00"/>
    <circle cx="112.20" cy="25.47" r="5.00"/>
    <circle cx="148.57" cy="105.07" r="5.00"/>
    <circle cx="122.87" cy="34.57" r="5.00"/>
  </g>
  <g style="fill:#004285; stroke:#000000" id="JanataDal(United)">
    <circle cx="129.17" cy="45.54" r="5.00"/>
    <circle cx="135.48" cy="56.53" r="5.00"/>
    <circle cx="123.99" cy="20.82" r="5.00"/>
    <circle cx="141.79" cy="67.56" r="5.00"/>
    <circle cx="148.12" cy="78.65" r="5.00"/>
  </g>
  <g style="fill:#BD710F; stroke:#000000" id="ShiromaniAkaliDal">
    <circle cx="135.15" cy="30.32" r="5.00"/>
    <circle cx="154.46" cy="89.80" r="5.00"/>
    <circle cx="141.51" cy="41.50" r="5.00"/>
  </g>
  <g style="fill:#99CCFF; stroke:#000000" id="AsomGanaParishad">
    <circle cx="136.11" cy="17.06" r="5.00"/>
  </g>
  <g style="fill:#FF6600; stroke:#000000" id="BodolandPeople'sFront">
    <circle cx="147.88" cy="52.74" r="5.00"/>
  </g>
  <g style="fill:#0093DD; stroke:#000000" id="LokJanshaktiParty">
    <circle cx="160.82" cy="101.05" r="5.00"/>
  </g>
  <g style="fill:#DB7093; stroke:#000000" id="NationalPeople'sParty">
    <circle cx="154.27" cy="64.07" r="5.00"/>
  </g>
  <g style="fill:#99FF00; stroke:#000000" id="PattaliMakkalKatchi">
    <circle cx="147.74" cy="27.12" r="5.00"/>
  </g>
  <g style="fill:#002BB4; stroke:#000000" id="RepublicanPartyofIndia">
    <circle cx="148.47" cy="14.22" r="5.00"/>
  </g>
  <g style="fill:#FF4500; stroke:#000000" id="TamilaManilaCongress(M)">
    <circle cx="160.68" cy="75.52" r="5.00"/>
  </g>
  <g style="fill:#2E5694; stroke:#000000" id="MizoNationalFront">
    <circle cx="154.16" cy="38.59" r="5.00"/>
  </g>
  <g style="fill:#CDCDCD; stroke:#000000" id="IndependentPolitician">
    <circle cx="160.60" cy="50.19" r="5.00"/>
  </g>
  <g style="fill:#000000; stroke:#000000" id="Nominated">
    <circle cx="167.11" cy="87.14" r="5.00"/>
    <circle cx="160.56" cy="24.96" r="5.00"/>
    <circle cx="161.01" cy="12.32" r="5.00"/>
    <circle cx="167.06" cy="61.96" r="5.00"/>
  </g>
  <g style="fill:#00BFFF; stroke:#000000" id="IndianNationalCongress">
    <circle cx="167.03" cy="36.84" r="5.00"/>
    <circle cx="173.55" cy="99.01" r="5.00"/>
    <circle cx="173.53" cy="73.95" r="5.00"/>
    <circle cx="173.51" cy="48.91" r="5.00"/>
    <circle cx="173.50" cy="23.89" r="5.00"/>
    <circle cx="173.66" cy="11.37" r="5.00"/>
    <circle cx="180.00" cy="86.25" r="5.00"/>
    <circle cx="180.00" cy="36.25" r="5.00"/>
    <circle cx="180.00" cy="61.25" r="5.00"/>
    <circle cx="186.34" cy="11.37" r="5.00"/>
    <circle cx="186.50" cy="23.89" r="5.00"/>
    <circle cx="186.49" cy="48.91" r="5.00"/>
    <circle cx="186.47" cy="73.95" r="5.00"/>
    <circle cx="186.45" cy="99.01" r="5.00"/>
    <circle cx="192.97" cy="36.84" r="5.00"/>
    <circle cx="192.94" cy="61.96" r="5.00"/>
    <circle cx="198.99" cy="12.32" r="5.00"/>
    <circle cx="199.44" cy="24.96" r="5.00"/>
    <circle cx="192.89" cy="87.14" r="5.00"/>
    <circle cx="199.40" cy="50.19" r="5.00"/>
    <circle cx="205.84" cy="38.59" r="5.00"/>
    <circle cx="199.32" cy="75.52" r="5.00"/>
    <circle cx="211.53" cy="14.22" r="5.00"/>
    <circle cx="212.26" cy="27.12" r="5.00"/>
    <circle cx="205.73" cy="64.07" r="5.00"/>
    <circle cx="199.18" cy="101.05" r="5.00"/>
    <circle cx="212.12" cy="52.74" r="5.00"/>
    <circle cx="223.89" cy="17.06" r="5.00"/>
    <circle cx="218.49" cy="41.50" r="5.00"/>
    <circle cx="205.54" cy="89.80" r="5.00"/>
    <circle cx="224.85" cy="30.32" r="5.00"/>
    <circle cx="211.88" cy="78.65" r="5.00"/>
    <circle cx="218.21" cy="67.56" r="5.00"/>
    <circle cx="236.01" cy="20.82" r="5.00"/>
    <circle cx="224.52" cy="56.53" r="5.00"/>
    <circle cx="230.83" cy="45.54" r="5.00"/>
    <circle cx="237.13" cy="34.57" r="5.00"/>
    <circle cx="211.43" cy="105.07" r="5.00"/>
    <circle cx="247.80" cy="25.47" r="5.00"/>
    <circle cx="217.70" cy="94.16" r="5.00"/>
  </g>
  <g style="fill:#DD1100; stroke:#000000" id="DravidaMunnetraKazhagam">
    <circle cx="223.97" cy="83.27" r="5.00"/>
    <circle cx="230.23" cy="72.40" r="5.00"/>
    <circle cx="236.50" cy="61.53" r="5.00"/>
    <circle cx="242.76" cy="50.67" r="5.00"/>
    <circle cx="249.01" cy="39.82" r="5.00"/>
    <circle cx="259.22" cy="31.00" r="5.00"/>
    <circle cx="260.42" cy="46.04" r="5.00"/>
  </g>
  <g style="fill:#008000; stroke:#000000" id="RashtriyaJanataDal">
    <circle cx="254.17" cy="56.86" r="5.00"/>
    <circle cx="247.91" cy="67.69" r="5.00"/>
    <circle cx="241.66" cy="78.51" r="5.00"/>
    <circle cx="235.40" cy="89.34" r="5.00"/>
    <circle cx="229.15" cy="100.16" r="5.00"/>
  </g>
  <g style="fill:#00B2B2; stroke:#000000" id="NationalistCongressParty">
    <circle cx="222.89" cy="110.99" r="5.00"/>
    <circle cx="270.19" cy="37.37" r="5.00"/>
    <circle cx="271.27" cy="53.18" r="5.00"/>
    <circle cx="264.98" cy="64.06" r="5.00"/>
  </g>
  <g style="fill:#8A0000; stroke:#000000" id="JharkhandMuktiMorcha">
    <circle cx="280.64" cy="44.55" r="5.00"/>
  </g>
  <g style="fill:#996699; stroke:#000000" id="MarumalarchiDravidaMunnetraKazhagam">
    <circle cx="258.67" cy="74.94" r="5.00"/>
  </g>
  <g style="fill:#008500; stroke:#000000" id="IndianUnionMuslimLeague">
    <circle cx="252.35" cy="85.84" r="5.00"/>
  </g>
  <g style="fill:#757575; stroke:#000000" id="IndependentPolitician">
    <circle cx="246.02" cy="96.75" r="5.00"/>
  </g>
  <g style="fill:#3CB371; stroke:#000000" id="AllIndiaTrinamoolCongress">
    <circle cx="239.66" cy="107.68" r="5.00"/>
    <circle cx="281.50" cy="61.20" r="5.00"/>
    <circle cx="290.53" cy="52.49" r="5.00"/>
    <circle cx="233.26" cy="118.64" r="5.00"/>
    <circle cx="275.09" cy="72.20" r="5.00"/>
    <circle cx="268.66" cy="83.22" r="5.00"/>
    <circle cx="262.18" cy="94.28" r="5.00"/>
    <circle cx="299.80" cy="61.15" r="5.00"/>
    <circle cx="291.01" cy="70.05" r="5.00"/>
    <circle cx="255.65" cy="105.39" r="5.00"/>
    <circle cx="284.43" cy="81.22" r="5.00"/>
    <circle cx="249.04" cy="116.57" r="5.00"/>
    <circle cx="277.78" cy="92.45" r="5.00"/>
  </g>
  <g style="fill:#006400; stroke:#000000" id="BijuJanataDal">
    <circle cx="308.39" cy="70.49" r="5.00"/>
    <circle cx="299.77" cy="79.65" r="5.00"/>
    <circle cx="271.03" cy="103.75" r="5.00"/>
    <circle cx="242.30" cy="127.84" r="5.00"/>
    <circle cx="292.92" cy="91.04" r="5.00"/>
    <circle cx="264.16" cy="115.15" r="5.00"/>
    <circle cx="285.95" cy="102.53" r="5.00"/>
    <circle cx="316.25" cy="80.44" r="5.00"/>
    <circle cx="307.69" cy="89.95" r="5.00"/>
  </g>
  <g style="fill:#EE0000; stroke:#000000" id="SamajwadiParty">
    <circle cx="257.10" cy="126.67" r="5.00"/>
    <circle cx="278.80" cy="114.12" r="5.00"/>
    <circle cx="300.49" cy="101.59" r="5.00"/>
    <circle cx="323.34" cy="90.95" r="5.00"/>
    <circle cx="249.77" cy="138.36" r="5.00"/>
    <circle cx="271.42" cy="125.86" r="5.00"/>
    <circle cx="293.08" cy="113.36" r="5.00"/>
    <circle cx="314.73" cy="100.87" r="5.00"/>
  </g>
  <g style="fill:#FFC0DB; stroke:#000000" id="TelanganaRashtraSamithi">
    <circle cx="307.07" cy="112.79" r="5.00"/>
    <circle cx="329.63" cy="101.97" r="5.00"/>
    <circle cx="285.39" cy="125.28" r="5.00"/>
    <circle cx="263.70" cy="137.78" r="5.00"/>
    <circle cx="320.84" cy="112.33" r="5.00"/>
    <circle cx="299.10" cy="124.85" r="5.00"/>
    <circle cx="277.33" cy="137.38" r="5.00"/>
  </g>
  <g style="fill:#1569C7; stroke:#000000" id="YSRCongressParty">
    <circle cx="335.06" cy="113.43" r="5.00"/>
    <circle cx="312.61" cy="124.53" r="5.00"/>
    <circle cx="255.48" cy="149.92" r="5.00"/>
    <circle cx="290.73" cy="137.09" r="5.00"/>
    <circle cx="325.97" cy="124.27" r="5.00"/>
    <circle cx="303.97" cy="136.88" r="5.00"/>
  </g>
  <g style="fill:#FF0000; stroke:#000000" id="CommunistPartyOfIndia(M)">
    <circle cx="339.63" cy="125.26" r="5.00"/>
    <circle cx="268.72" cy="149.69" r="5.00"/>
    <circle cx="317.08" cy="136.72" r="5.00"/>
    <circle cx="281.79" cy="149.53" r="5.00"/>
    <circle cx="330.10" cy="136.59" r="5.00"/>
  </g>
  <g style="fill:#22409A; stroke:#000000" id="BahujanSamajParty">
    <circle cx="294.74" cy="149.42" r="5.00"/>
    <circle cx="343.29" cy="137.41" r="5.00"/>
    <circle cx="307.62" cy="149.33" r="5.00"/>
    <circle cx="259.29" cy="162.24" r="5.00"/>
  </g>
  <g style="fill:#5BB30E; stroke:#000000" id="AamAadmiParty">
    <circle cx="320.43" cy="149.26" r="5.00"/>
    <circle cx="333.19" cy="149.21" r="5.00"/>
    <circle cx="272.04" cy="162.18" r="5.00"/>
  </g>
  <g style="fill:#FF6634; stroke:#000000" id="ShivSena">
    <circle cx="346.02" cy="149.79" r="5.00"/>
    <circle cx="284.74" cy="162.14" r="5.00"/>
    <circle cx="297.39" cy="162.10" r="5.00"/>
  </g>
  <g style="fill:#990099; stroke:#000000" id="JammuKashmirPDP">
    <circle cx="310.02" cy="162.08" r="5.00"/>
    <circle cx="322.63" cy="162.06" r="5.00"/>
  </g>
  <g style="fill:#CC9900; stroke:#000000" id="KeralaCongress(M)">
    <circle cx="335.21" cy="162.04" r="5.00"/>
  </g>
  <g style="fill:#FFFF00; stroke:#000000" id="TeluguDesamParty">
    <circle cx="347.82" cy="162.35" r="5.00"/>
  </g>
  <g style="fill:#138808; stroke:#000000" id="JanataDal(Secular)">
    <circle cx="261.10" cy="175.01" r="5.00"/>
  </g>
  <g style="fill:#FFFF00; stroke:#000000" id="SikkimDemocraticFront">
    <circle cx="273.62" cy="175.00" r="5.00"/>
  </g>
  <g style="fill:#990066; stroke:#000000" id="NagaPeople'sFront">
    <circle cx="286.13" cy="175.00" r="5.00"/>
  </g>
  <g style="fill:#FF4A4A; stroke:#000000" id="CommunistPartyOfIndia">
    <circle cx="298.64" cy="175.00" r="5.00"/>
  </g>
  <g style="fill:#FFFFFF; stroke:#000000" id="VacantSeats">
    <circle cx="311.15" cy="175.00" r="5.00"/>
    <circle cx="323.66" cy="175.00" r="5.00"/>
    <circle cx="336.17" cy="175.00" r="5.00"/>
    <circle cx="348.68" cy="175.00" r="5.00"/>
  </g>
</g>
</svg>
