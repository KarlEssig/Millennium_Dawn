###Warsaw Pact Reactions
 ###Poland
 SOV_Global.20.t: "Armed Insurgency in Poland"
 SOV_Global.20.d: "There's an armed uprising going on in Poland right now. The uprising was caused by the population's dissatisfaction with the current leadership of the country. Should we bring our troops into the territory of our ally or should we choose a more neutral position?"
 SOV_Global.20.a: "We need to put down this rebellion and protect our brothers in arms. Raise our army!"
 SOV_Global.20.b: "We will refrain from intervening in the conflict."
 ###Czechia
 SOV_Global.21.t: "Armed Insurgency in Czechia"
 SOV_Global.21.d: "There's an armed uprising going on in Czechia right now. The uprising was caused by the population's dissatisfaction with the current leadership of the country. Should we bring our troops into the territory of our ally or should we choose a more neutral position?"
 SOV_Global.21.a: "We need to put down this rebellion and protect our brothers in arms. Raise our army!"
 SOV_Global.21.b: "We will refrain from intervening in the conflict."
 ###Slovakia
 SOV_Global.22.t: "Armed Insurgency in Slovakia"
 SOV_Global.22.d: "There's an armed uprising going on in Slovakia right now. The uprising was caused by the population's dissatisfaction with the current leadership of the country. Should we bring our troops into the territory of our ally or should we choose a more neutral position?"
 SOV_Global.22.a: "We need to put down this rebellion and protect our brothers in arms. Raise our army!"
 SOV_Global.22.b: "We will refrain from intervening in the conflict."
 ###Hungary
 SOV_Global.23.t: "Armed Insurgency in Hungary"
 SOV_Global.23.d: "There's an armed uprising going on in Hungary right now. The uprising was caused by the population's dissatisfaction with the current leadership of the country. Should we bring our troops into the territory of our ally or should we choose a more neutral position?"
 SOV_Global.23.a: "We need to put down this rebellion and protect our brothers in arms. Raise our army!"
 SOV_Global.23.b: "We will refrain from intervening in the conflict."
 ###Romania
 SOV_Global.24.t: "Armed Insurgency in Romania"
 SOV_Global.24.d: "There's an armed uprising going on in Romania right now. The uprising was caused by the population's dissatisfaction with the current leadership of the country. Should we bring our troops into the territory of our ally or should we choose a more neutral position?"
 SOV_Global.24.a: "We need to put down this rebellion and protect our brothers in arms. Raise our army!"
 SOV_Global.24.b: "We will refrain from intervening in the conflict."
 ###Bulgaria
 SOV_Global.25.t: "Armed Insurgency in Bulgaria"
 SOV_Global.25.d: "There's an armed uprising going on in Bulgaria right now. The uprising was caused by the population's dissatisfaction with the current leadership of the country. Should we bring our troops into the territory of our ally or should we choose a more neutral position?"
 SOV_Global.25.a: "We need to put down this rebellion and protect our brothers in arms. Raise our army!"
 SOV_Global.25.b: "We will refrain from intervening in the conflict."