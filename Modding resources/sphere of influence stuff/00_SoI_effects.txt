FoPol_view_influence_options = {
	clr_country_flag = view_FoPol_influence_actions
	clr_country_flag = view_FoPol_terror
	clr_country_flag = view_FoPol_PMC
	clr_country_flag = view_FoPol_protest_promises
	clr_country_flag = view_FoPol_corruption
	set_country_flag = view_FoPol_influence_options
}

FoPol_view_influence_actions = {
	clr_country_flag = view_FoPol_influence_options
	clr_country_flag = view_FoPol_terror
	clr_country_flag = view_FoPol_PMC
	clr_country_flag = view_FoPol_protest_promises
	clr_country_flag = view_FoPol_corruption
	set_country_flag = view_FoPol_influence_actions
}

FoPol_view_terror = {
	clr_country_flag = view_FoPol_influence_options
	clr_country_flag = view_FoPol_influence_actions
	clr_country_flag = view_FoPol_PMC
	clr_country_flag = view_FoPol_protest_promises
	clr_country_flag = view_FoPol_corruption
	set_country_flag = view_FoPol_terror
}

FoPol_view_PMC = {
	clr_country_flag = view_FoPol_influence_options
	clr_country_flag = view_FoPol_influence_actions
	clr_country_flag = view_FoPol_terror
	clr_country_flag = view_FoPol_protest_promises
	clr_country_flag = view_FoPol_corruption
	set_country_flag = view_FoPol_PMC
}

FoPol_view_protest_promises = {
	clr_country_flag = view_FoPol_influence_options
	clr_country_flag = view_FoPol_influence_actions
	clr_country_flag = view_FoPol_terror
	clr_country_flag = view_FoPol_PMC
	clr_country_flag = view_FoPol_corruption
	set_country_flag = view_FoPol_protest_promises
}

FoPol_view_corruption = {
	clr_country_flag = view_FoPol_influence_options
	clr_country_flag = view_FoPol_influence_actions
	clr_country_flag = view_FoPol_terror
	clr_country_flag = view_FoPol_protest_promises
	clr_country_flag = view_FoPol_PMC
	set_country_flag = view_FoPol_corruption
}

FoPol_close_menu = {
	clr_country_flag = view_FoPol_influence_options
	clr_country_flag = view_FoPol_influence_actions
	clr_country_flag = view_FoPol_terror
	clr_country_flag = view_FoPol_protest_promises
	clr_country_flag = view_FoPol_PMC
	clr_country_flag = view_FoPol_corruption
	clr_country_flag = open_FoPol_screen
}

FoPol_existing_country_check = {
	clear_array = ROOT.influencer_option
	every_country = {
		add_to_array = { ROOT.influencer_option = THIS }
	}
	refresh_gui = yes
}

# EUROPEAN BUTTON
	FoPol_european_button_effects = {
		if = {
			limit = { has_country_flag = view_FoPol_influence_options }
			FoPol_existing_european_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_influence_actions }
			FoPol_sphere_european_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_terror }
			FoPol_terror_european_country_check = yes
		}
	}
	FoPol_existing_european_country_check = {
		clear_array = ROOT.influencer_option
		every_country = {
			limit = { is_in_europe = yes }
			add_to_array = { ROOT.influencer_option = THIS }
		}
		refresh_gui = yes
	}
	FoPol_sphere_european_country_check = {
		clear_array = ROOT.influenced_nation
		every_country = {
			limit = {
				is_in_array = { influence_array = ROOT }
				is_in_europe = yes
			}
			add_to_array = { ROOT.influenced_nation = THIS }
		}
		refresh_gui = yes
	}
	FoPol_terror_european_country_check = {
		clear_array = ROOT.counter_terror_targets
		every_country = {
			limit = {
				is_in_array = { global.ct_states = THIS }
				is_in_array = { influence_array = ROOT }
				is_in_europe = yes
			}
			add_to_array = { ROOT.counter_terror_targets = THIS }
		}
		refresh_gui = yes
	}
# AMERICAN BUTTON
	FoPol_american_button_effects = {
		if = {
			limit = { has_country_flag = view_FoPol_influence_options }
			FoPol_existing_american_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_influence_actions }
			FoPol_sphere_american_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_terror }
			FoPol_terror_american_country_check = yes
		}
	}
	FoPol_existing_american_country_check = {
		clear_array = ROOT.influencer_option
		every_country = {
			limit = { is_in_the_americas = yes }
			add_to_array = { ROOT.influencer_option = THIS }
		}
		refresh_gui = yes
	}
	FoPol_sphere_american_country_check = {
		clear_array = ROOT.influenced_nation
		every_country = {
			limit = {
				is_in_array = { influence_array = ROOT }
				is_in_the_americas = yes
			}
			add_to_array = { ROOT.influenced_nation = THIS }
		}
		refresh_gui = yes
	}
	FoPol_terror_american_country_check = {
		clear_array = ROOT.counter_terror_targets
		every_country = {
			limit = {
				is_in_array = { global.ct_states = THIS }
				is_in_array = { influence_array = ROOT }
				is_in_the_americas = yes
			}
			add_to_array = { ROOT.counter_terror_targets = THIS }
		}
		refresh_gui = yes
	}
# OCEANIC BUTTON
	FoPol_oceanic_button_effects = {
		if = {
			limit = { has_country_flag = view_FoPol_influence_options }
			FoPol_existing_oceanic_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_influence_actions }
			FoPol_sphere_oceanic_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_terror }
			FoPol_terror_oceanic_country_check = yes
		}
	}
	FoPol_existing_oceanic_country_check = {
		clear_array = ROOT.influencer_option
		every_country = {
			limit = { is_oceania_nation = yes }
			add_to_array = { ROOT.influencer_option = THIS }
		}
		refresh_gui = yes
	}
	FoPol_sphere_oceanic_country_check = {
		clear_array = ROOT.influenced_nation
		every_country = {
			limit = {
				is_in_array = { influence_array = ROOT }
				is_oceania_nation = yes
			}
			add_to_array = { ROOT.influenced_nation = THIS }
		}
		refresh_gui = yes
	}
	FoPol_terror_oceanic_country_check = {
		clear_array = ROOT.counter_terror_targets
		every_country = {
			limit = {
				is_in_array = { global.ct_states = THIS }
				is_in_array = { influence_array = ROOT }
				is_oceania_nation = yes
			}
			add_to_array = { ROOT.counter_terror_targets = THIS }
		}
		refresh_gui = yes
	}
# AFRICAN BUTTON
	FoPol_african_button_effects = {
		if = {
			limit = { has_country_flag = view_FoPol_influence_options }
			FoPol_existing_african_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_influence_actions }
			FoPol_sphere_african_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_terror }
			FoPol_terror_african_country_check = yes
		}
	}
	FoPol_existing_african_country_check = {
		clear_array = ROOT.influencer_option
		every_country = {
			limit = { is_in_africa = yes }
			add_to_array = { ROOT.influencer_option = THIS }
		}
		refresh_gui = yes
	}
	FoPol_sphere_african_country_check = {
		clear_array = ROOT.influenced_nation
		every_country = {
			limit = {
				is_in_array = { influence_array = ROOT }
				is_in_africa = yes
			}
			add_to_array = { ROOT.influenced_nation = THIS }
		}
		refresh_gui = yes
	}
	FoPol_terror_african_country_check = {
		clear_array = ROOT.counter_terror_targets
		every_country = {
			limit = {
				is_in_array = { global.ct_states = THIS }
				is_in_array = { influence_array = ROOT }
				is_in_africa = yes
			}
			add_to_array = { ROOT.counter_terror_targets = THIS }
		}
		refresh_gui = yes
	}
# ASIAN BUTTON
	FoPol_asian_button_effects = {
		if = {
			limit = { has_country_flag = view_FoPol_influence_options }
			FoPol_existing_asian_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_influence_actions }
			FoPol_sphere_asian_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_terror }
			FoPol_terror_asian_country_check = yes
		}
	}
	FoPol_existing_asian_country_check = {
		clear_array = ROOT.influencer_option
		every_country = {
			limit = { is_in_asia = yes }
			add_to_array = { ROOT.influencer_option = THIS }
		}
		refresh_gui = yes
	}
	FoPol_sphere_asian_country_check = {
		clear_array = ROOT.influenced_nation
		every_country = {
			limit = {
				is_in_array = { influence_array = ROOT }
				is_in_asia = yes
			}
			add_to_array = { ROOT.influenced_nation = THIS }
		}
		refresh_gui = yes
	}
	FoPol_terror_asian_country_check = {
		clear_array = ROOT.counter_terror_targets
		every_country = {
			limit = {
				is_in_array = { global.ct_states = THIS }
				is_in_array = { influence_array = ROOT }
				is_in_asia = yes
			}
			add_to_array = { ROOT.counter_terror_targets = THIS }
		}
		refresh_gui = yes
	}
# MIDDLE EASTERN BUTTON
	FoPol_middle_eastern_button_effects = {
		if = {
			limit = { has_country_flag = view_FoPol_influence_options }
			FoPol_existing_middle_eastern_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_influence_actions }
			FoPol_sphere_middle_eastern_country_check = yes
		}
		if = {
			limit = { has_country_flag = view_FoPol_terror }
			FoPol_terror_middle_eastern_country_check = yes
		}
	}
	FoPol_existing_middle_eastern_country_check = {
		clear_array = ROOT.influencer_option
		every_country = {
			limit = { is_in_the_middle_east = yes }
			add_to_array = { ROOT.influencer_option = THIS }
		}
		refresh_gui = yes
	}
	FoPol_sphere_middle_eastern_country_check = {
		clear_array = ROOT.influenced_nation
		every_country = {
			limit = {
				is_in_array = { influence_array = ROOT }
				is_in_the_middle_east = yes
			}
			add_to_array = { ROOT.influenced_nation = THIS }
		}
		refresh_gui = yes
	}
	FoPol_terror_middle_eastern_country_check = {
		clear_array = ROOT.counter_terror_targets
		every_country = {
			limit = {
				is_in_array = { global.ct_states = THIS }
				is_in_array = { influence_array = ROOT }
				is_in_the_middle_east = yes
			}
			add_to_array = { ROOT.counter_terror_targets = THIS }
		}
		refresh_gui = yes
	}

FoPol_existing_country_check = {
	clear_array = ROOT.influencer_option
	every_country = {
		add_to_array = { ROOT.influencer_option = THIS }
	}
	refresh_gui = yes
}

FoPol_sphere_check = {
	clear_array = ROOT.influenced_nation
	every_country = {
		limit = { is_in_array = { influence_array = ROOT } }
		add_to_array = { ROOT.influenced_nation = THIS }
	}
	refresh_gui = yes
}

FoPol_terror_check = {
	clear_array = ROOT.counter_terror_targets
	every_country = {
		limit = {
			is_in_array = { global.ct_states = THIS }
			is_in_array = { influence_array = ROOT }
		}
		add_to_array = { ROOT.counter_terror_targets = THIS }
	}
	refresh_gui = yes
}