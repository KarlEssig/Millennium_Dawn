TAI_category = {
	icon = GFX_decision_generic_taiwan
	allowed = { original_tag = TAI }
}

TAI_economy_decisions_category = {
	 icon = taiwan
	 visible = { has_completed_focus = TAI_focus_on_internal_development } 
	 allowed = { original_tag = TAI }
}

TAI_mobilization_decisions_category = {
	 icon = infiltration
	 visible = { has_completed_focus = TAI_army_reserve_command } 
	 allowed = { original_tag = TAI }
}

TAI_png_decisions_category = {
	 icon = military_plan
	 allowed = { original_tag = TAI }	   
	 visible = {
	 has_completed_focus = TAI_launch_our_plan
	 }

}
