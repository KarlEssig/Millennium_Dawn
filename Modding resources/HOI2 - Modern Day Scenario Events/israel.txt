#########################################################
#							#
# Events for Israel (ISR)                               #
#                                			#
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#39501 - 39800# Main events
#39801 - 39850# Political events
#39851 - 40000# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### Israeli-Syrian war
#############################################
event = {
         id = 39701
         random = no
         country = ISR

	 trigger = {
		   OR = {
		   	war = { country = SYR country = USA }
		   	war = { country = PER country = USA }
		   }
		   OR = {	
		   	puppet = { country = IRQ country = SYR }
			puppet = { country = IRQ country = PER }
		   }
		   NOT = { 
			war = { country = SYR country = ISR }
		 	war = { country = PER country = ISR }
			alliance = { country = SYR country = ISR }
			alliance = { country = PER country = ISR }
			ispuppet = ISR
			event = 23520 #EGY
			
		   }

         }
 
         name = "EVT_39701_NAME"
         desc = "EVT_39701_DESC"
         style = 0
	 picture = "israel"
 
         date = { day = 1 month = april year = 2003 }
	 offset = 19
	 deathdate = { day = 30 month = december year = 2019 }
 
           action_a = {
                  name = "We cannot allow this, this means war!"
		  ai_chance = 99
		  command = { type = war which = SYR }
		  command = { type = war which = PER }
		  command = { type = setflag which = oil_war }
		  command = { type = domestic which = interventionism value = 3 }
           }
	   action_b = {
                  name = "No, we are not yet prepared"
		  ai_chance = 1
		  command = { type = dissent value = 3 } # hawks unhappy
           }

}