units = {
	fleet = {
		name = "Puntland Maritime Force"
		naval_base = 3202 # Eyl
		task_force = {
			name = "Patrol Task Force"
			location = 3202
			ship = { name = "PMF Abdullahi" definition = corvette equipment = { corvette_1 = { amount = 1 owner = PUN creator = UAE } } }
		}
	}
}
