shared_focus = {
	id = SOM_a_nation_torn
	icon = unkown
	cost = 10
	x = 10
	y = 0
	available = {
		has_country_flag = SOM_civil_war_active
		if = {
			limit = {
				original_tag = PUN
			}
			has_completed_focus = PUN_the_new_government ##Puntland needs to take this to access this tree
		}
	}
	completion_reward = { }
}

