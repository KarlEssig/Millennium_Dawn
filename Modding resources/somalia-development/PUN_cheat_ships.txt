units = {
	fleet = {
		name = "Cheat Squadron"
		naval_base = 11090 # Eyl
		task_force = {
			name = "Cheat Flotilla 1"
			location = 11090 # Halifax
			ship = { name = "Cheat Ship 1" definition = corvette equipment = { corvette_1 = { amount = 1 owner = PUN creator = PUN } } }
		}
	}
}
