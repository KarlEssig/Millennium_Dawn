################
##### SOM ######
################

SOM_generic_dec = {

	icon = generic_economy

	visible_when_empty = yes

	allowed = {
		OR = {
			original_tag = SOM
			original_tag = SML
			original_tag = SWS
			original_tag = SNA
			original_tag = JUB
		}
	}

	visible = {
	}
}
