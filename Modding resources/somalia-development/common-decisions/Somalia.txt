SOM_generic_dec = {

	SOM_invite_PUN = {

		icon = GFX_decision_generic_form_nation

		fire_only_once = yes

		cost = 150

		ai_will_do = {
			factor = 1
		}

		visible = {
			OR = {
				original_tag = SOM
				original_tag = SML
				original_tag = SWS
				original_tag = SNA
				original_tag = JUB
			}
		}

		available = {
			240 = { is_owned_and_controlled_by = ROOT }
			618 = { is_owned_and_controlled_by = ROOT }
			584 = { is_owned_and_controlled_by = ROOT }
			593 = { is_owned_and_controlled_by = ROOT }
			581 = { is_owned_and_controlled_by = ROOT }
			ROOT = {
				NOT = {
					OR = {
						has_war_with = SOM
						has_war_with = SML
						has_war_with = SWS
						has_war_with = SNA
						has_war_with = JUB
						has_war_with = PUN
					}
				}
			}
			country_exists = PUN
		}

		complete_effect = {
			PUN = { country_event = { id = puntland.45 days = 2 } }
			if = {
				limit = { tag = SOM }
				set_global_flag = som_was_it_flg
			}
			if = {
				limit = { tag = SML }
				set_global_flag = sml_was_it_flg
			}
			if = {
				limit = { tag = SWS }
				set_global_flag = sws_was_it_flg
			}
			if = {
				limit = { tag = JUB }
				set_global_flag = jub_was_it_flg
			}
			if = {
				limit = { tag = SNA }
				set_global_flag = sna_was_it_flg
			}
		}
	}

}
