################
##### JUB ######
################

JUB_prepare_for_return = {

	icon = GFX_decision_revolt

	visible_when_empty = yes

	allowed = {
		original_tag = JUB
	}

	visible = {
		original_tag = JUB
		has_completed_focus = jubaland_butcher_returns
	}
}

JUB_destroy_spm = {

	icon = GFX_decision_revolt

	visible_when_empty = yes

	allowed = {
		original_tag = JUB
	}

	visible = {
		original_tag = JUB
		has_completed_focus = jubaland_destroy_pockets
	}
}
