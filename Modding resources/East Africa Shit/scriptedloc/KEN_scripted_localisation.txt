defined_text = {
	name = KEN_current_kikuyu_opinion
	#22
	text = { trigger = { check_variable = { KEN_kikuyu_influence_var > 21 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_kikuyu_influence_var < 11 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}
defined_text = {
	name = KEN_current_luhya_opinion
	#14
	text = { trigger = { check_variable = { KEN_luhya_influence_var > 13 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_luhya_influence_var < 7 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}
defined_text = {
	name = KEN_current_kalenjin_opinion
	#13
	text = { trigger = { check_variable = { KEN_kalenjin_influence_var > 12 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_kalenjin_influence_var < 6 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}
defined_text = {
	name = KEN_current_luo_opinion
	#11
	text = { trigger = { check_variable = { KEN_luo_influence_var > 10 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_luo_influence_var < 5 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}
defined_text = {
	name = KEN_current_kamba_opinion
	#10
	text = { trigger = { check_variable = { KEN_kamba_influence_var > 9 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_kamba_influence_var < 5 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}
defined_text = {
	name = KEN_current_kisii_opinion
	#6
	text = { trigger = { check_variable = { KEN_kisii_influence_var > 5 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_kisii_influence_var < 3 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}
defined_text = {
	name = KEN_current_mijikenda_opinion
	#6
	text = { trigger = { check_variable = { KEN_mijikenda_influence_var > 5 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_mijikenda_influence_var < 3 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}
defined_text = {
	name = KEN_current_somali_opinion
	#5
	text = { trigger = { check_variable = { KEN_somali_influence_var > 4 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_somali_influence_var < 3 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}
defined_text = {
	name = KEN_current_others_opinion
	#13
	text = { trigger = { check_variable = { KEN_others_influence_var > 12 } } localization_key = KEN_tribe_pleased }
	text = { trigger = { check_variable = { KEN_others_influence_var < 6 } } localization_key = KEN_tribe_displeased }
	text = { localization_key = KEN_tribe_content }
}


#tribal opinion
defined_text = {
	name = KEN_current_tribal_status
	text = { trigger = { has_country_flag = hostile_oligarchs } localization_key = script.0.hostile }
	text = { trigger = { has_country_flag = negative_oligarchs } localization_key = script.0.negative }
	text = { trigger = { has_country_flag = indifferent_oligarchs } localization_key = script.0.indifferent }
	text = { trigger = { has_country_flag = positive_oligarchs } localization_key = script.0.positive }
	text = { trigger = { has_country_flag = enthusiastic_oligarchs } localization_key = script.0.enthusiastic }
	text = { localization_key = script.0.indifferent }
}