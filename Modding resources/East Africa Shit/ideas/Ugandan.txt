ideas = {

	country = {

		UGA_infrastructure = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UGA_infrastructure" }
			picture = electric_bulb
			
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				production_speed_infrastructure_factor = 0.30
				production_speed_arms_factory_factor = -0.05
				production_speed_industrial_complex_factor = -0.05
				political_power_gain = -0.05
			}
		}

		UGA_bad_pilots = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UGA_infrastructure" }
			picture = air_war_plans_division
			
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			research_bonus = { air_doctrine = -0.30 }
			modifier = {
				experience_gain_air_factor = -0.20
				air_accidents_factor = 0.25
			}
		}

		UGA_military_despot = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UGA_no_elections" }
			picture = general_staff
			
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				experience_gain_army = 0.10
				stability_factor = 0.02
			}
		}

		UGA_dictator = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UGA_dictator" }
			picture = fascism2
			
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.01
				political_power_gain = 0.05
				neutrality_drift = 0.02
			}
		}

		UGA_dictator_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UGA_dictator_1" }
			picture = fascism2
			
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.03
				political_power_gain = 0.1
				neutrality_drift = 0.04
			}
		}

		UGA_dictator_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UGA_dictator_2" }
			picture = fascism2
			
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.05
				political_power_gain = 0.2
				neutrality_drift = 0.06
			}
		}

		UGA_democracy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UGA_democracy" }
			picture = democracy
			
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				democratic_drift = 0.04
				communism_drift = 0.02
				neutrality_drift = 0.01
			}
		}

		UGA_northern_troubles = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UGA_northern_troubles" }
			picture = scw_intervention_rep
			
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.02
				war_support_factor = 0.03
				production_speed_buildings_factor = -0.1
				political_power_gain = -0.1
				local_resources_factor = -0.1
			}
		}

	}

}