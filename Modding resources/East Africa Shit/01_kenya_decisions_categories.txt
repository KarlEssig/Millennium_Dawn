###Kenya Decisions - Author: Bluehunter

KEN_tribalism_category = {	
	icon = decision_category_tribalism
	
	visible = {
		tag = KEN
	}
	
	allowed = {
		tag = KEN
	}

	visible_when_empty = yes

	scripted_gui = tribalism_decision_ui
}