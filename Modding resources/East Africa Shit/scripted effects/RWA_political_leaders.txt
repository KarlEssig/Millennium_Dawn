set_leader_RWA = {

	if = { limit = { has_country_flag = set_Monarchist }

		if = { limit = { check_variable = { Monarchist_leader = 0 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Kigeli V"
				picture = "generic.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Monarchist_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yuhi VI"
				picture = "generic.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Western_Autocracy }
		if = { limit = { check_variable = { set_Western_Autocracy= 0 } }
			add_to_variable = { set_Western_Autocracy = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Paul Kagame"
				desc = "POLITICS_PAUL_KAGAME_DESC"
				picture = "paul_kagame.dds"
				ideology = Western_Autocracy
				traits = {
					guerrilla_leader
					western_Western_Autocracy
					sly
					ruthless
					legendary_guerrilla_leader
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { set_Western_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
}