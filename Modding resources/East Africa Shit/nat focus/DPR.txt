#Made by Lord Bogdanoff
focus_tree = {
	id = dpr_focus
	country = {
		factor = 0
		modifier = {
			add = 20
			original_tag = DPR
		}
	}
	continuous_focus_position = { x = 3000 y = 1100 }
	focus = {
		id = DPR_start
		icon = sov_novorossia_bunt
		x = 20
		y = 0
		cost = 0.1
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_start"
			add_political_power = 150
			hidden_effect = {
				DPR_maidan_create_army = yes
				if = {
					limit = { DPR = { is_ai = yes } }
					country_event = { id = dpr.6 days = 1 }
				}
			}
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_vstavai_dombass
		icon = dpr_vstavai
		x = 0
		y = 1
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_start }
		cost = 1
		search_filters = { FOCUS_FILTER_POLITICAL  FOCUS_FILTER_WAR_SUPPORT FOCUS_FILTER_MANPOWER }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_vstavai_dombass"
			add_stability = -0.05
			add_ideas = DPR_vstaia_donbass
			add_war_support = 0.05
			add_manpower = 750
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_create_nov
		icon = align_to_novorossiya
		x = 7
		y = 1
		relative_position_id = DPR_start
		prerequisite = {
			focus = DPR_start
		}
		available = {
			country_exists = LPR
			LPR = { is_subject = no }
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		bypass = {
			has_idea = NOV_novorossia
		}
		cost = 1
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_create_nov"
			LPR = { 
				add_ideas = NOV_novorossia
				country_event = dpr.7 
			}
			add_ideas = NOV_novorossia
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_nov_army
		icon = NOV_army
		x = 7
		y = 2
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_create_nov }
		available = {
			country_exists = LPR
			LPR = { is_subject = no }
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		bypass = {
			has_idea = NOV_novorossia1
		}
		cost = 1
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nov_army"
			give_military_access = LPR
			swap_ideas = {
				remove_idea = NOV_novorossia
				add_idea = NOV_novorossia1
			}
			LPR = {
				swap_ideas = {
					remove_idea = NOV_novorossia
					add_idea = NOV_novorossia1
				}
				set_temp_variable = { percent_change = 5 }
				set_temp_variable = { tag_index = LPR }
				set_temp_variable = { influence_target = DPR }
				change_influence_percentage = yes
			}
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = DPR }
			set_temp_variable = { influence_target = LPR }
			change_influence_percentage = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_nov_propaganda
		icon = NOV_propaganda
		x = 6
		y = 3
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_nov_army }
		available = {
			country_exists = LPR
			LPR = { is_subject = no }
			LPR = {	influence_higher_30 = yes }
		}
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nov_propaganda"
			set_temp_variable = { percent_change = 10 }
			set_temp_variable = { tag_index = DPR }
			set_temp_variable = { influence_target = LPR }
			change_influence_percentage = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_nov_economic
		icon = NOV_economic
		x = 6
		y = 4
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_nov_propaganda }
		available = {
			country_exists = LPR
			LPR = { is_subject = no }
			LPR = {	influence_higher_50 = yes }
		}
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nov_economic"
			set_temp_variable = { percent_change = 15 }
			set_temp_variable = { tag_index = DPR }
			set_temp_variable = { influence_target = LPR }
			change_influence_percentage = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_nov_zavods
		icon = NOV_zavods
		x = 8
		y = 3
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_nov_army }
		available = {
			country_exists = LPR
			LPR = { is_subject = no }
			LPR = {	influence_higher_30 = yes }
		}
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nov_zavods"
			set_temp_variable = { percent_change = 10 }
			set_temp_variable = { tag_index = DPR }
			set_temp_variable = { influence_target = LPR }
			change_influence_percentage = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_nov_politic
		icon = NOV_admin
		x = 8
		y = 4
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_nov_zavods }
		available = {
			country_exists = LPR
			LPR = { is_subject = no }
			LPR = {	influence_higher_50 = yes }
		}
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nov_zavods"
			set_temp_variable = { percent_change = 15 }
			set_temp_variable = { tag_index = DPR }
			set_temp_variable = { influence_target = LPR }
			change_influence_percentage = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_nov_annex
		icon = NOV_create
		x = 7
		y = 5
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_nov_politic }
		prerequisite = { focus = DPR_nov_economic }
		available = {
			country_exists = LPR
			LPR = { is_subject_of = DPR }
		}
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nov_annex"
			annex_country = { target = LPR transfer_troops = yes }
			LPR = { every_core_state = { add_core_of = DPR } }
			DPR = {
				add_to_variable = { treasury = LPR.treasury }
				add_to_variable = { debt = LPR.debt }
				add_to_variable = { int_investments = LPR.int_investments }
			}
			add_political_power = 200
			set_cosmetic_tag = NOV_communism
			clr_country_flag = dynamic_flag
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_nov_annex_hpr
		icon = hpr_start
		x = 6
		y = 6
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_nov_annex }
		available = {
			country_exists = HPR
			HPR = { is_subject_of = DPR }
		}
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nov_annex_hpr"
			annex_country = { target = HPR transfer_troops = yes }
			HPR = { every_core_state = { add_core_of = DPR } }
			DPR = {
				add_to_variable = { treasury = HPR.treasury }
				add_to_variable = { debt = HPR.debt }
				add_to_variable = { int_investments = HPR.int_investments }
			}
			add_political_power = -50
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_nov_annex_opr
		icon = opr_start
		x = 8
		y = 6
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_nov_annex }
		available = {
			country_exists = OPR
			OPR = { is_subject_of = DPR }
		}
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nov_annex_hpr"
			annex_country = { target = OPR transfer_troops = yes }
			OPR = { every_core_state = { add_core_of = DPR } }
			DPR = {
				add_to_variable = { treasury = OPR.treasury }
				add_to_variable = { debt = OPR.debt }
				add_to_variable = { int_investments = OPR.int_investments }
			}
			add_political_power = -50
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_reform_army
		icon = dpr_vs
		x = -8
		y = 2
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_vstavai_dombass }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_reform_army"
			add_stability = -0.05
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			remove_ideas = DPR_unproffesional_army
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = 2000
				producer = SOV
			}
			create_corps_commander = {
				name = "Seks"
				portrait_path = "gfx/leaders/DPR/dpr_seks.dds"
				traits = { bold }
				skill = 3
				attack_skill = 3
				defense_skill = 3
				planning_skill = 3
				logistics_skill = 3
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_bank
		icon = dpr_bank
		x = 0
		y = 2
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = {
			focus = DPR_vstavai_dombass
		}
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_bank"
			one_office_construction = yes
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = DPR_government
		icon = dpr_gover
		x = -2
		y = 2
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_vstavai_dombass }
		cost = 1
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_government"
			remove_ideas = DPR_no_goverment_idea
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = DPR_donetsk_build
		icon = military_fort
		x = 2
		y = 2
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_vstavai_dombass }
		cost = 1
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_propaganda"
			add_stability = 0.1
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
			693 = {
				add_building_construction = { type = bunker province = 6474 level = 2 }
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_propaganda
		icon = propaganda
		x = 1
		y = 3
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_donetsk_build }
		prerequisite = { focus = DPR_bank }
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_propaganda"
			add_war_support = 0.03
			add_manpower = 550
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = DPR_pasports
		icon = dpr_pasport
		x = -1
		y = 3
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_bank }
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY  }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_pasports"
			add_stability = 0.07
			add_political_power = 50
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_gaz_contracts_russia
		icon = Rosneft
		x = -3
		y = 3
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_government }
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY  }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_gaz_contracts_russia"
			add_ideas = DPR_russian_oil
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_warehouse
		icon = Generic_Military_Equipment
		x = -10
		y = 3
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_reform_army }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_warehouse"
			add_equipment_to_stockpile = {
				type = infantry_weapons3
				amount = 1500
				producer = SOV
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment2
				amount = 250
				producer = SOV
			}
			add_equipment_to_stockpile = {
				type = AA_Equipment
				amount = 100
				producer = SOV
			}
			add_equipment_to_stockpile = {
				type = Anti_Air_0
				amount = 15
				producer = SOV
			}
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_vpk
		icon = army_tank_industry
		x = -6
		y = 3
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_reform_army }
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS  FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_vpk"
			one_random_arms_factory = yes
			add_ideas = DPR_vpk 
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_rifles
		icon = army_more_ak47
		x = -7
		y = 4
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_vpk }
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_rzso_cheburashka"
			swap_ideas = {
				remove_idea = DPR_vpk 
				add_idea = DPR_vpk1
			}
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_rzso_snejinka
		icon = dpr_snow
		x = -5
		y = 4
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_vpk }
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_rzso_cheburashka"
			set_temp_variable = { treasury_change = -1.00 }
			modify_treasury_effect = yes
			add_tech_bonus = {
				name = DPR_cheburashka
				bonus = 0.90
				uses = 2
				category = Cat_SP_R_ARTY
			}
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_rzso_cheburashka
		icon = dpr_rzso_cheburashka
		x = -6
		y = 5
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_rzso_snejinka }
		prerequisite = { focus = DPR_rifles }
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_rzso_cheburashka"
			swap_ideas = {
				remove_idea = DPR_vpk1
				add_idea = DPR_vpk2
			}
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_blpa
		icon = Drones
		x = -6
		y = 6
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_rzso_cheburashka }
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_rzso_cheburashka"
			set_temp_variable = { treasury_change = -4.00 }
			modify_treasury_effect = yes
			swap_ideas = {
				remove_idea = DPR_vpk2
				add_idea = DPR_vpk3
			}
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_univer
		icon = army_doctrine
		x = -7
		y = 7
		relative_position_id = DPR_start
		cost = 7
		prerequisite = { focus = DPR_gos_granitca }
		prerequisite = { focus = DPR_blpa }
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_univer"
			add_ideas = DPR_proffesional_army1
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_reforms_army
		icon = dpr_vs
		x = -9
		y = 7
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_khan }
		prerequisite = { focus = DPR_gos_granitca }
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_reform_army"
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			add_ideas = DPR_proffesional_army
		}
		ai_will_do = { factor = 20 }
	}

	focus = {
		id = DPR_rebuild_ruins
		icon = economic_civil_industry
		x = 3
		y = 3
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = {
			focus = DPR_donetsk_build
		}
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_rebuild_ruins"
			swap_ideas = {
				remove_idea = DPR_donbass_ruins
				add_idea = DPR_donbass_ruins2
			}
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = DPR_gos_granitca
		icon = military_fort
		x = -8
		y = 6
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_somali }
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_gos_granitca"
			add_stability = 0.1
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			693 = {
				add_building_construction = { type = bunker province = 14531 level = 2 }
				add_building_construction = { type = bunker province = 3421 level = 2 }
			}

		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = DPR_interbrigade
		icon = sov_nazbol_military
		x = -8
		y = 5
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_somali }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_gos_granitca"
			hidden_effect = {
				division_template = {
					name = "Batalyon Dobrovolchev"
					regiments = {
						L_Inf_Bat = { x = 0 y = 0 }
						L_Inf_Bat = { x = 0 y = 1 }
						L_Inf_Bat = { x = 0 y = 2 }
						L_Inf_Bat = { x = 0 y = 3 }
					}
					priority = 0
				}
			}
			693 = {
				create_unit = {
					division = "name = \"Interbrigade Donetsk\" division_template = \"Batalyon Dobrovolchev\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}

		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_infra
		icon = infrastructure1
		x = 2
		y = 6
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_npz }
		prerequisite = { focus = DPR_youth }
		cost = 5
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_infra"
			one_random_infrastructure = yes
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_bivaluta
		icon = hpr_grivna_ban
		x = -2
		y = 4
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = {
			focus = DPR_pasports
		}
		prerequisite = {
			focus = DPR_gaz_contracts_russia
		}
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_bivaluta"
			remove_ideas = DPR_bivaluta
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_religion_boost
		icon = orthodox_patriarh
		x = -3
		y = 5
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_bivaluta }
		cost = 3
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_religion_boost"
			add_political_power = 150
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_anti_gibdd
		icon = anti_lgbt
		x = -1
		y = 5
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_bivaluta }
		prerequisite = { focus = DPR_internet }
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_anti_gibdd"
			add_ideas = DPR_anti_lgbt
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_anti_ukrain
		icon = attack_ukraine
		x = -2
		y = 6
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_anti_gibdd }
		prerequisite = { focus = DPR_religion_boost }
		cost = 4
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_anti_ukrain"
			set_temp_variable = { percent_change = -15 }
			set_temp_variable = { tag_index = UKR }
			set_temp_variable = { influence_target = DPR }
			change_influence_percentage = yes
			add_stability = -0.05
			add_war_support = 0.10
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_mgb
		icon = dpr_mgb
		x = 0
		y = 6
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_anti_gibdd }
		prerequisite = { focus = DPR_youth }
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_mgb"
			add_ideas = DPR_mgb
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_internet
		icon = economic_internet
		x = 0
		y = 4
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_pasports }
		prerequisite = { focus = DPR_propaganda	}
		cost = 7
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_bivaluta"
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			random_owned_controlled_state = {
				add_building_construction = {
					type = internet_station
					level = 1
					instant_build = yes
				}
			}
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_youth
		icon = dpr_youth
		x = 1
		y = 5
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = {
			focus = DPR_sports
		}
		prerequisite = {
			focus = DPR_internet
		}
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_youth"
			add_ideas = DPR_youth_front
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_somali
		icon = dpr_somali
		x = -8
		y = 3
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_reform_army }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_somali"
			693 = {
				create_unit = {
					division = "name = \"Batalyon Somali\" division_template = \"Tankovyi Batalyon\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_sparta
		icon = dpr_sparta
		x = -9
		y = 4
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_somali }
		prerequisite = { focus = DPR_warehouse }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_sparta"
			693 = {
				create_unit = {
					division = "name = \"Batalyon Sparta\" division_template = \"Batalyon Spetsnaza\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_oplot
		icon = dpr_oplot
		x = -11
		y = 4
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_warehouse }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_sparta"
			693 = {
				create_unit = {
					division = "name = \"Brigada Oplot\" division_template = \"Mekhanizirovannaya Brigada\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_territorial_defence
		icon = DPR_ter_defence
		x = -12
		y = 5
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_oplot }
		cost = 2
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_ter_oborona"
			hidden_effect = {
				division_template = {
					name = "Territorial Defense Brigade"
					regiments = {
						L_Inf_Bat = { x = 0 y = 0 }
						L_Inf_Bat = { x = 0 y = 1 }
						Mot_Inf_Bat = { x = 1 y = 0 }
					}
				}
			}
			add_manpower = -1500
			add_ideas = DPR_ter_orobona
			693 = {
				create_unit = {
					division = "name = \"1st Battalion of Territorial defence\" division_template = \"Territorial Defense Brigade\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}
			693 = {
				create_unit = {
					division = "name = \"3rd Battalion of Territorial defence\" division_template = \"Territorial Defense Brigade\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_dizel
		icon = dpr_disel
		x = -10
		y = 5
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_oplot }
		prerequisite = { focus = DPR_sparta }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_sparta"
			693 = {
				create_unit = {
					division = "name = \"Brigada Dizel\" division_template = \"Tankovyi Batalyon\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_khan
		icon = DPR_khan
		x = -10
		y = 6
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_dizel }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_khan"
			division_template = {
				name = "Separate Special Purpose Battalion"
				regiments = {
					Special_Forces = { x = 0 y = 0 }
				}
			}
			693 = {
				create_unit = {
					division = "name = \"Separate Special Purpose Battalion Khan\" division_template = \"Separate Special Purpose Battalion\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 155 }
	}
	focus = {
		id = DPR_guard
		icon = dpr_guard
		x = -11
		y = 7
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_khan }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_sparta"
			693 = {
				create_unit = {
					division = "name = \"Respublikanskaya Gvardiya DNR\" division_template = \"Mekhanizirovannaya Brigada\" start_experience_factor = 0.1"
					prioritize_location = 6474
					owner = DPR
				}
			}
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_obtf_kaskad
		icon = dpr_kaskad
		x = -8
		y = 8
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_reforms_army }
		prerequisite = { focus = DPR_univer }
		cost = 3
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_obtf_kaskad"
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
			division_template = {
				name = "OBTF Kaskad"
					regiments = {
						Special_Forces = { x = 0 y = 0 }
						Special_Forces = { x = 0 y = 1 }
						Special_Forces = { x = 1 y = 0 }
						Special_Forces = { x = 1 y = 1 }
						Special_Forces = { x = 1 y = 2 }
						Special_Forces = { x = 2 y = 1 }
						Arty_Bat = { x = 2 y = 0 }
						Arty_Bat = { x = 2 y = 1 }
						Arty_Bat = { x = 2 y = 2 }
					}
					support = {
						SP_AA_Battery = { x = 0 y = 0 }
						armor_Recce_Comp = { x = 0 y = 1 }
						H_Engi_Comp = { x = 0 y = 2 }
						combat_service_support_company = { x = 0 y = 3 }
					}
			}
			693 = {
				create_unit = {
					division = "name = \"OBTF Kaskad\" division_template = \"OBTF Kaskad\" start_experience_factor = 1.0"
					prioritize_location = 6474
					owner = DPR
				}
			}
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_nationalise
		icon = economic_nationalisation
		x = 4
		y = 4
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = { focus = DPR_rebuild_ruins }
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_nationalise"
			two_random_industrial_complex = yes
			set_temp_variable = { percent_change = -6 }
			set_temp_variable = { tag_index = UKR }
			set_temp_variable = { influence_target = DPR }
			change_influence_percentage = yes
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_npz
		icon = oil_production2
		x = 3
		y = 5
		relative_position_id = DPR_start
		prerequisite = { focus = DPR_nationalise }
		prerequisite = { focus = DPR_sports }
		cost = 7
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_npz"
			set_temp_variable = { temp_opinion = 3 }
			change_fossil_fuel_industry_opinion = yes
			693 = {
				add_building_construction = {
					type = fuel_silo
					level = 1
					instant_build = yes
				}
			}
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_sports
		icon = dpr_sport
		x = 2
		y = 4
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
		}
		prerequisite = {
			focus = DPR_rebuild_ruins
		}
		prerequisite = {
			focus = DPR_propaganda
		}
		cost = 5
		search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_sports"
			add_ideas = DPR_sport_development
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_vstuplenie_v_rf
		icon = constitution
		x = 0
		y = 7
		relative_position_id = DPR_start
		cost = 3
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
			has_war = no
		}
		prerequisite = {
			focus = DPR_anti_ukrain
		}
		prerequisite = {
			focus = DPR_mgb
		}
		prerequisite = {
			focus = DPR_infra
		}
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_vstuplenie_v_rf"
			add_political_power = 100
			hidden_effect = {
			kill_country_leader = yes
			create_country_leader = {
				name = "Denis Pushilin"
				picture = "Penis_Dushilin.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
				}
			}
		}

		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = DPR_referendum
		icon = russian_elections
		x = 0
		y = 8
		relative_position_id = DPR_start
		available = {
			OR = {
				DPR = { is_subject_of = SOV }
				is_subject = no
			}
			OR = {
				emerging_autocracy_are_in_power = yes
				emerging_reactionaries_are_in_power = yes
			}
			has_war = no
		}
		prerequisite = {
			focus = DPR_vstuplenie_v_rf
		}
		cost = 5
		search_filters = {  FOCUS_FILTER_POLITICAL }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus DPR_referendum"
			SOV = { country_event = dpr.4 }
		}
		ai_will_do = { factor = 20 }
	}
	shared_focus = SUB_russia_politic
}


