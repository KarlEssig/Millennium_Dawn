### How to mod subideologies

hidden_effect = {
  clear_array = ruling_party
  clear_array = gov_coalition_array                     ### clears the existing parties
  add_to_array = { ruling_party = 22 }                  ### Sets the ruling party
  update_government_coalition_strength = yes
  update_party_name = yes                               ### Updates the party shown in the main screen
  set_coalition_drift = yes                             ### reset coalition counts
  add_to_variable = { party_pop_array^23 value = 0.05 } ### Adds popularity to an specific subideology (In this case 5% to the Monarchist Party)
  multiply_variable = { party_pop_array^2 = 1.1 }       ### Adds 10% popularity to the liberal party

}

### Increase the popularities of the ruling coalition by 10%, and the main opposition party by 5%
hidden_effect = {
  for_each_loop = {
    array = ruling_party
    value = v
    multiply_variable = { party_pop_array^v = 1.1 }
  }
  ### Increases the popularity of the ruling party by 10%
  for_each_loop = {
    array = gov_coalition_array
    value = v
    multiply_variable = { party_pop_array^v = 1.1 }
  }
  ### Increases the popularity of the parties which are in a coalition with the ruling party by 10%
  if = {
    limit = {
      is_in_array = {
        array = ruling_party
        value = 1
      }
    }
    multiply_variable = { party_pop_array^2 = 1.05 }
  }
  if = {
    limit = {
      is_in_array = {
        array = ruling_party
        value = 2
      }
    }
    multiply_variable = { party_pop_array^1 = 1.05 }
  }
}

List of party types:

### Pro-Western (democratic):
    0 - Western_Autocracy
    1 - conservatism
    2 - liberalism
    3 - socialism

### Emerging (communism):
    4 - Communist-State
    5 - anarchist_communism
    6 - Conservative
    7 - Autocracy
    8 - Mod_Vilayat_e_Faqih
    9 - Vilayat_e_Faqih

### Salafist (fascism):
    10 - Kingdom
    11 - Caliphate

### Non-Aligned (neutrality):
    12 - Neutral_Muslim_Brotherhood
    13 - Neutral_Autocracy
    14 - Neutral_conservatism
    15 - oligarchism
    16 - Neutral_Libertarian
    17 - Neutral_green
    18 - neutral_Social
    19 - Neutral_Communism

### Nationalist (nationalist):
    20 - Nat_Populism
    21 - Nat_Fascism
    22 - Nat_Autocracy
    23 - Monarchist

# All their definitions are stored in arrays - (party_pop_array) stores current popularities, (party_pop_elect_array) stores popularities at the previous election, (ruling_party) stores the ID of the governing party, (gov_coalition_array) stores the ID's of it's coalition partners. If party_pop_elect_array entries are not set for a party, it will take values from party_pop_array for this purpose. If no popularities are set at all, the game will give them failsafe values based on ruling outlook, geography and religion.
# Please note that the main ruling party should NOT be also added to gov_coalition_array. Upon startup, parties are recalculated to conform to outlook popularites. This means that if the sum of popularity of say the western parties don't match the popularity of the democratic entry in set_politics = {}, or add up to 100, they will appear a bit different ingame, but there is no harm in this in general.
# Before any array values can be set for both 2000 and 2017, one must first use the scripted function start_politics_input = yes
