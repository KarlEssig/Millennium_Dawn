

#Old Military Focus Branch

	focus = {
		id = ITA_military_focus
		icon = italian_armed_forces
		x = 102
		y = 0
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_military_focus"
			army_experience = 15
			navy_experience = 15
			air_experience = 15
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 160
				has_manpower < 10000
			}
		}
	}

	focus = {
		id = ITA_army_focus
		icon = Italian_Army
		relative_position_id = ITA_military_focus
		x = -4
		y = 1
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }

		prerequisite = {
			focus = ITA_military_focus
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_army_focus"
			army_experience = 50
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 160
				has_manpower < 10000
			}
		}
	}

	focus = {
		id = ITA_navy_focus
		icon = Italian_Navy
		relative_position_id = ITA_military_focus
		x = 0
		y = 1
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }

		prerequisite = {
			focus = ITA_military_focus
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_navy_focus"
			navy_experience = 50
		}
	}

	focus = {
		id = ITA_air_focus
		icon = Italian_Air_Force
		relative_position_id = ITA_military_focus
		x = 4
		y = 1
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }

		prerequisite = {
			focus = ITA_military_focus
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_air_focus"
			air_experience = 50
		}
	}

	focus = {
		id = ITA_small_arms_program
		icon = small_arms
		relative_position_id = ITA_army_focus
		x = 0
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_army_focus
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_small_arms_program"
			add_tech_bonus = {
				name = infantry_tech
				bonus = 0.3
				uses = 2
				category = Cat_INF
			}
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 160
				has_manpower < 10000
			}
		}
	}

	focus = {
		id = ITA_destroyers_focus
		icon = battleships
		relative_position_id = ITA_navy_focus
		x = 0
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_navy_focus
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_destroyers_focus"
			add_tech_bonus = {
				name = destroyer_bonus
				bonus = 0.3
				uses = 2
				technology = destroyer_1
				technology = destroyer_2
				technology = missile_destroyer_1
				technology = missile_destroyer_2
				technology = missile_destroyer_3
				technology = missile_destroyer_4
				technology = missile_destroyer_5
			}
		}
	}

	focus = {
		id = ITA_fighters_focus
		icon = modern_fighter
		relative_position_id = ITA_air_focus
		x = 0
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_air_focus
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_fighters_focus"
			add_tech_bonus = {
				name = fighter_bonus
				bonus = 0.3
				uses = 2
				technology = L_Strike_fighter1
				technology = L_Strike_fighter2
				technology = L_Strike_fighter3
				technology = L_Strike_fighter4
				technology = L_Strike_fighter5
			}
		}
	}

	focus = {
		id = ITA_tanks_focus
		icon = tanks
		relative_position_id = ITA_small_arms_program
		x = -1
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_small_arms_program
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_tanks_focus"
			add_tech_bonus = {
				name = tank_bonus
				bonus = 0.3
				uses = 2
				technology = MBT_1
				technology = MBT_2
				technology = MBT_3
				technology = MBT_4
				technology = MBT_5
				technology = MBT_7
				technology = MBT_8
			}
		}
	}

	focus = {
		id = ITA_motorized_focus
		icon = motorized
		relative_position_id = ITA_small_arms_program
		x = 1
		y = 1
		cost = 5
		search_filters = { FOCUS_FILTER_EQUIPMENT  }

		prerequisite = {
			focus = ITA_small_arms_program
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_motorized_focus"
			add_ideas = motorization_investments
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 160
				has_manpower < 10000
			}
		}
	}

	focus = {
		id = ITA_submarines_focus
		icon = submarines
		relative_position_id = ITA_destroyers_focus
		x = 0
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_destroyers_focus
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_submarines_focus"
			add_tech_bonus = {
				name = submarine_bonus
				bonus = 0.3
				uses = 2
				technology = attack_submarine_1
				technology = attack_submarine_2
				technology = attack_submarine_3
				technology = attack_submarine_4
				technology = attack_submarine_5
				technology = attack_submarine_6
				technology = attack_submarine_7
			}
		}
	}

	focus = {
		id = ITA_close_air_support
		icon = close_air_support
		relative_position_id = ITA_fighters_focus
		x = 0
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_fighters_focus
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_close_air_support"
			add_tech_bonus = {
				name = bomber_bonus
				bonus = 0.3
				uses = 2
				technology = cas1
				technology = cas2
				technology = cas3
				technology = cas4
				technology = cas5
			}
		}
	}

	focus = {
		id = ITA_artillery_focus
		icon = artillery2
		relative_position_id = ITA_tanks_focus
		x = 1
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_tanks_focus
			focus = ITA_motorized_focus
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_artillery_focus"
			add_tech_bonus = {
				name = tank_bonus
				bonus = 0.3
				uses = 2
				technology = artillery_1
				technology = artillery_2
				technology = artillery_3
				technology = artillery_4
				technology = SP_arty_1
				technology = SP_arty_2
				technology = SP_arty_3
				technology = SP_arty_4
			}
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 160
				has_manpower < 10000
			}
		}
	}

	focus = {
		id = ITA_naval_bases_focus
		icon = trieste_port
		relative_position_id = ITA_submarines_focus
		x = 1
		y = 1
		cost = 5
		search_filters = {FOCUS_FILTER_INDUSTRY }

		prerequisite = {
			focus = ITA_submarines_focus
		}

		mutually_exclusive = {
			focus = ITA_air_bases_focus
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_naval_bases_focus"
			random_owned_state = {
				limit = {
					is_coastal = yes
				}
				add_building_construction = {
					type = naval_base
					level = 1
					instant_build = yes
				}
			}
			random_owned_state = {
				limit = {
					is_coastal = yes
				}
				add_building_construction = {
					type = naval_base
					level = 1
					instant_build = yes
				}
			}
			random_owned_state = {
				limit = {
					is_coastal = yes
				}
				add_building_construction = {
					type = naval_base
					level = 1
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = ITA_air_bases_focus
		icon = air_production
		relative_position_id = ITA_close_air_support
		x = -1
		y = 1
		cost = 5
		search_filters = {FOCUS_FILTER_INDUSTRY }

		prerequisite = {
			focus = ITA_close_air_support
		}

		mutually_exclusive = {
			focus = ITA_naval_bases_focus
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_air_bases_focus"
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = air_base
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = air_base
					level = 1
					instant_build = yes
				}
			}
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = air_base
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = air_base
					level = 1
					instant_build = yes
				}
			}
			random_owned_state = {
				limit = {
					free_building_slots = {
						building = air_base
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = air_base
					level = 1
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = ITA_quality_ideas
		icon = army_reform
		relative_position_id = ITA_artillery_focus
		x = -1
		y = 1
		cost = 5
		search_filters = {FOCUS_FILTER_MILITARY_LAWS}

		prerequisite = {
			focus = ITA_artillery_focus
		}

		mutually_exclusive = {
			focus = ITA_quantity_ideas
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_quality_ideas"
			add_ideas = ITA_quality_army
		}
	}

	focus = {
		id = ITA_quantity_ideas
		icon = cavalry
		relative_position_id = ITA_artillery_focus
		x = 1
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_artillery_focus
		}

		mutually_exclusive = {
			focus = ITA_quality_ideas
		}

		search_filters = {FOCUS_FILTER_MANPOWER}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_quantity_ideas"
			add_ideas = ITA_quantity_army
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 160
				has_manpower < 10000
			}
		}
	}

	focus = {
		id = ITA_carriers_focus
		icon = carriers
		relative_position_id = ITA_submarines_focus
		x = 0
		y = 2
		cost = 5

		prerequisite = {
			focus = ITA_submarines_focus
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_carriers_focus"
			add_tech_bonus = {
				name = carrier_bonus
				bonus = 0.3
				uses = 2
				technology = modern_carrier_0
				technology = modern_carrier_1
				technology = modern_carrier_2
				technology = modern_carrier_3
				technology = modern_carrier_4
				technology = modern_carrier_5
				technology = modern_carrier_6
			}
		}
	}

	focus = {
		id = ITA_frecce_tricolori
		icon = fighters
		relative_position_id = ITA_close_air_support
		x = 0
		y = 2
		cost = 5

		prerequisite = {
			focus = ITA_close_air_support
		}

		search_filters = {FOCUS_FILTER_EQUIPMENT }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_frecce_tricolori"
			air_experience = 100
		}
	}

	focus = {
		id = ITA_land_doctrine
		icon = army_doctrine
		relative_position_id = ITA_quality_ideas
		x = 1
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_quality_ideas
			focus = ITA_quantity_ideas
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_land_doctrine"
			army_experience = 100
		}
	}

	focus = {
		id = ITA_naval_doctrine
		icon = naval_doctrine
		relative_position_id = ITA_carriers_focus
		x = 0
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_carriers_focus
		}
		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_naval_doctrine"
			navy_experience = 100
		}
	}

	focus = {
		id = ITA_airforce_doctrine
		icon = air_doctrine
		relative_position_id = ITA_frecce_tricolori
		x = 0
		y = 1
		cost = 5

		prerequisite = {
			focus = ITA_frecce_tricolori
		}

		search_filters = {FOCUS_FILTER_RESEARCH}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ITA_airforce_doctrine"
			air_experience = 100
		}
	}

	focus = {
		id = ITA_deal_with_mafia
		icon = dangerous_deal
		relative_position_id = ITA_south_status_quo
		x = 0
		y = 1
		cost = 10

		search_filters = {}
		prerequisite = {
			focus = ITA_south_status_quo
		}

		completion_reward = {
			add_political_power = 100
			log = "[GetDateText]: [Root.GetName]: Focus ITA_deal_with_mafia"
		}
	}

	focus = {
		id = ITA_deafness_in_the_south
		icon = Fancy_Lady
		relative_position_id = ITA_deal_with_mafia
		x = 0
		y = 1
		cost = 10

		search_filters = {}
		prerequisite = {
			focus = ITA_deal_with_mafia
		}

		completion_reward = {
			add_political_power = 100
			log = "[GetDateText]: [Root.GetName]: Focus ITA_deafness_in_the_south"
			}
		}

	focus = {
		id = ITA_blindness_in_the_north
		icon = control_judiciary
		relative_position_id = ITA_deafness_in_the_south
		x = 0
		y = 1
		cost = 10

		search_filters = {}
		prerequisite = {
			focus = ITA_deafness_in_the_south
		}

		completion_reward = {
			add_political_power = 100
			log = "[GetDateText]: [Root.GetName]: Focus ITA_blindness_in_the_north"
			}
		}

	focus = {
		id = ITA_rein_in_favors
		icon = intelligence_exchange
		relative_position_id = ITA_blindness_in_the_north
		x = 0
		y = 1
		cost = 10

		search_filters = {}
		prerequisite = {
			focus = ITA_blindness_in_the_north
		}

		completion_reward = {
			add_political_power = 100
			log = "[GetDateText]: [Root.GetName]: Focus ITA_rein_in_favors"
			}
		}

	focus = {
		id = ITA_export_mafia
		icon = black_market
		relative_position_id = ITA_rein_in_favors
		x = 0
		y = 1
		cost = 10

		search_filters = {}
		prerequisite = {
			focus = ITA_rein_in_favors
		}

		completion_reward = {
			add_political_power = 100
			log = "[GetDateText]: [Root.GetName]: Focus ITA_export_mafia"
			}
		}