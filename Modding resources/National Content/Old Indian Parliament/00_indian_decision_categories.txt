MD_indian_parliament_decision_category = {
	icon = generic_political_actions
	priority = 100
	scripted_gui = indian_parliament_decision_ui
	allowed = { original_tag = RAJ }
	visible = { original_tag = RAJ }
	visible_when_empty = yes
}