﻿# focus_tree = {
# 	id = israel
# 	country = {
# 		factor = 0
# 		modifier = {
# 			add = 30
# 			tag = ISR
# 		}
# 	}

# 	#Focus for Chinese investments
# 	focus = {
# 		id = ISR_chinese_investments
# 		icon = trade_with_china
# 		x = 56
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_chinese_relations }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_opinion_modifier = {
# 				target = CHI
# 				modifier = large_commercial_relations
# 			}
# 			204 = {
# 				add_building_construction = {
# 					type = dockyard
# 					level = 2
# 					instant_build = yes
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Ruso-Israeli Brotherhood
# 	focus = {
# 		id = ISR_rusoisraeli_brotherhood
# 		icon = align_to_russia
# 		x = 50
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_russian_relations }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 				add_opinion_modifier = {
# 					target = SOV
# 					modifier = large_increase
# 				}
# 			}
# 		}

# 	#Focus for Russian relations
# 	focus = {
# 		id = ISR_russian_relations
# 		icon = align_to_russia2
# 		x = 50
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_our_ties_with_the_east }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_opinion_modifier = {
# 				target = SOV
# 				modifier = medium_increase
# 			}
# 		}
# 	}

# 	#Focus for Joint Military Training
# 	focus = {
# 		id = ISR_joint_military_training
# 		icon = military_mission2
# 		x = 53
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_our_ties_with_the_east }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			army_experience = 30
# 			air_experience = 30
# 			RAJ = {
# 				army_experience = 30
# 				air_experience = 30
# 			}
# 		}
# 	}

# 	#Focus for Indian relations
# 	focus = {
# 		id = ISR_indian_relations
# 		icon = align_to_india
# 		x = 53
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_our_ties_with_the_east }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 				add_opinion_modifier = {
# 					target = RAJ
# 					modifier = medium_increase
# 				}
# 			}
# 		}

# 	#Focus for Chinese Relations
# 	focus = {
# 		id = ISR_chinese_relations
# 		icon = align_to_china
# 		x = 56
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_our_ties_with_the_east }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_opinion_modifier = {
# 				target = CHI
# 				modifier = medium_increase
# 			}
# 		}
# 	}

# 	#Focus for The Law of Return
# 	focus = {
# 		id = ISR_the_law_of_return
# 		icon = refugees
# 		x = 58
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_beta_israel }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = ethiopian_return
# 		}
# 	}

# 	#Focus for Military Academies
# 	focus = {
# 		id = ISR_military_academies
# 		icon = military_academy
# 		x = 26
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_reformed_education_program }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 	}

# 	#Focus for A New Breed of Tank
# 	focus = {
# 		id = ISR_a_new_breed_of_tank
# 		icon = tanks5
# 		x = 26
# 		y = 4
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_nazareths_arms_industries }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			log = "[GetDateText]: [Root.GetName]: Focus ISR_a_new_breed_of_tank"
# 			add_tech_bonus = {
# 				bonus = 0.5
# 				uses = 1
# 				category = Cat_TANKS
# 			}
# 		}
# 	}

# 	#Focus for The Carmel Program
# 	focus = {
# 		id = ISR_the_carmel_program
# 		icon = license_production
# 		x = 26
# 		y = 5
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_a_new_breed_of_tank }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = carmel_program
# 		}
# 	}

# 	#Focus for Nazareth's Arms Industries
# 	focus = {
# 		id = ISR_nazareths_arms_industries
# 		icon = tank_production
# 		x = 26
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_military_academies }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			log = "[GetDateText]: [Root.GetName]: Focus ISR_nazareths_arms_industries"
# 		204 = {
# 			add_building_construction = {
# 				type = arms_factory
# 				level = 3
# 				instant_build = yes
# 			}
# 		}
# 		set_temp_variable = { treasury_change = -15 }
# 		modify_treasury_effect = yes
# 		}
# 	}

# 	#Focus for Tel Aviv industry expansion
# 	focus = {
# 		id = ISR_tel_aviv_industry_expansion
# 		icon = industry_democratic
# 		x = 29
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_reform_healthcare }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			log = "[GetDateText]: [Root.GetName]: Focus ISR_tel_aviv_industrial_expansion"
# 			204 = {
# 			add_building_construction = {
# 				type = industrial_complex
# 				level = 3
# 				instant_build = yes
# 			}
# 			add_extra_state_shared_building_slots = 3
# 		}
# 		set_temp_variable = { treasury_change = -15 }
# 		modify_treasury_effect = yes
# 		}
# 	}

# 	#Focus for Beersheba Land Development
# 	focus = {
# 		id = ISR_beersheba_land_development
# 		icon = construction4
# 		x = 29
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_tel_aviv_industry_expansion }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			log = "[GetDateText]: [Root.GetName]: Focus ISR_beersheba_land_development"
# 		206 = {
# 			add_extra_state_shared_building_slots = 4
# 		}
# 			set_temp_variable = { treasury_change = -15 }
# 			modify_treasury_effect = yes
# 		}
# 	}

# 	#Focus for Expand the Police Force
# 	focus = {
# 		id = ISR_expand_the_police_force
# 		icon = secret_police
# 		x = 32
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_centralize_the_state }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			increase_policing_budget = yes
# 		}
# 	}

# 	#Focus for Next Generation Warfare
# 	focus = {
# 		id = ISR_next_generation_warfare
# 		icon = soldier
# 		x = 11
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = {
# 			focus = ISR_the_iron_dome
# 		}
# 		prerequisite = {
# 			focus = ISR_expand_the_israeli_navy
# 		}
# 		completion_reward = {
# 			add_tech_bonus = {
# 				bonus = 0.5
# 				uses = 2
# 				category = Cat_NETWORK_CENTRIC_WARFARE
# 			}
# 		}
# 		ai_will_do = {
# 			factor = 1
# 		}
# 	}

# 	#Focus for The Air War
# 	focus = {
# 		id = ISR_the_air_war
# 		icon = air_doctrine
# 		x = 11
# 		y = 4
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_next_generation_warfare }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_tech_bonus = {
# 				bonus = 0.5
# 				uses = 2
# 				category = CAT_FIXED_WING
# 			}
# 		}
# 	}

# 	#Focus for IAF F-35
# 	focus = {
# 		id = ISR_iaf_f35
# 		icon = f35
# 		x = 11
# 		y = 5
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_air_war }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {

# 		}
# 	}

# 	#Focus for The Iron Beam
# 	focus = {
# 		id = ISR_the_iron_beam
# 		icon = air_defense2
# 		x = 9
# 		y = 7
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_iaf_f35 }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_to_variable = {defence_israel = 10}
# 			custom_effect_tooltip = decrease_missile_defence_cost_tt
# 		}
# 	}

# 	#Focus for David's Sling
# 	focus = {
# 		id = ISR_davids_sling
# 		icon = rocketry
# 		x = 13
# 		y = 7
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_iaf_f35 }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_to_variable = {defence_israel = 10}
# 			custom_effect_tooltip = further_missile_defence_tt
# 		}
# 	}

# 	#Focus for The Jewish State
# 	focus = {
# 		id = ISR_the_jewish_state
# 		icon = judaism
# 		x = 27
# 		y = 0
# 		cost = 10
# 		available_if_capitulated = yes
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_political_power = 150
# 		}
# 	}

# 	#Focus for Centralize the State
# 	focus = {
# 		id = ISR_centralize_the_state
# 		icon = government_beu
# 		x = 32
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_jewish_state }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			increase_centralization = yes
# 		}
# 	}

# 	#Focus for Compulsory Service
# 	focus = {
# 		id = ISR_compulsory_service
# 		icon = volunteers
# 		x = 23
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_jewish_state }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = compulsary_service
# 		}
# 	}

# 	#Focus for Conscript Training
# 	focus = {
# 		id = ISR_conscript_training
# 		icon = conscription
# 		x = 23
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_compulsory_service }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = conscription_reform
# 		}
# 	}

# 	#Focus for Invest in the Military
# 	focus = {
# 		id = ISR_invest_in_the_military
# 		icon = money
# 		x = 23
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_conscript_training }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			increase_military_spending = yes
# 		}
# 	}

# 	#Focus for Expand Tavor
# 	focus = {
# 		id = ISR_expand_tavor
# 		icon = industry_military
# 		x = 23
# 		y = 5
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_invest_in_the_military }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 		204 = {
# 			add_building_construction = {
# 				type = arms_factory
# 				level = 2
# 				instant_build = yes
# 			}
# 		}
# 		set_temp_variable = { treasury_change = -15 }
# 		modify_treasury_effect = yes
# 		}
# 	}

# 	#Focus for Reformed Education Program
# 	focus = {
# 		id = ISR_reformed_education_program
# 		icon = university
# 		x = 26
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_jewish_state }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			increase_education_budget = yes
# 		}
# 	}

# 	#Focus for The Political Sphere
# 	focus = {
# 		id = ISR_the_political_sphere
# 		icon = parliament_box
# 		x = 4
# 		y = 0
# 		cost = 10
# 		available_if_capitulated = yes
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_political_power = 50
# 		}
# 	}

# 	#Focus for The 2001 Election
# 	focus = {
# 		id = ISR_the_2001_election
# 		icon = democracy6
# 		x = 4
# 		y = 1
# 		cost = 5
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_political_sphere }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			if = {
# 				limit = { has_start_date < 2016.01.01 }
# 				country_event = { id = Israel.4 days = 1
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Ariel Sharon
# 	focus = {
# 		id = ISR_ariel_sharon
# 		icon = liberalism
# 		x = 4
# 		y = 2
# 		cost = 8
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_2001_election }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			custom_effect_tooltip = likud_in_power_tt
# 			if = {
# 				limit = { has_start_date < 2016.01.01 }
# 				hidden_effect = {
# 	                clear_array = ruling_party
# 	                clear_array = gov_coalition_array
# 	                add_to_array = { ruling_party = 3 } #Monarchists
# 	                add_to_array = { gov_coalition_array = 21 } #Populists/Fascists
# 	                update_government_coalition_strength = yes
# 	                update_party_name = yes
# 	                set_coalition_drift = yes
# 	               	set_ruling_leader = yes
#					set_leader = yes
# 	                set_politics = {
# 	                    ruling_party = democratic
# 	                    elections_allowed = yes
# 	                }
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Operation Defensive Shield
# 	focus = {
# 		id = ISR_operation_defensive_shield
# 		icon = reactionary
# 		x = 2
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_ariel_sharon }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			if = {
# 				limit = { has_start_date < 2016.01.01 }
# 				country_event = { id = Israel.5 days = 1
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Pro-Russian Sentiment
# 	focus = {
# 		id = ISR_prorussian_sentiment
# 		icon = tsarist_russia
# 		x = 6
# 		y = 3
# 		cost = 8
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_ariel_sharon }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_opinion_modifier = {
# 				target = SOV
# 				modifier = medium_increase
# 			}
# 		}
# 	}

# 	#Focus for Withdraw from Gaza
# 	focus = {
# 		id = ISR_withdraw_from_gaza
# 		icon = bread_and_peace
# 		x = 4
# 		y = 4
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = {
# 			focus = ISR_prorussian_sentiment
# 		}
# 		prerequisite = {
# 			focus = ISR_operation_defensive_shield
# 		}
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			if = {
# 				limit = { has_start_date < 2016.01.01 }
# 				add_threat = -2
# 			}
# 			add_war_support = -0.05
# 			add_stability = 0.05
# 			add_to_variable = { hap_gaza = 20 }
# 			subtract_from_variable = { ter_gaza = 5 }
# 			set_variable = { is_attacking_gaza = 2 }
# 			add_to_variable = { hap_isr = 10 }
# 			add_to_variable = { ter_isr = 15 }
# 			custom_effect_tooltip = less_occupation_tt
# 		}
# 	}

# 	#Focus for Ehud Olmert
# 	focus = {
# 		id = ISR_ehud_olmert
# 		icon = democracy5
# 		x = 4
# 		y = 5
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_withdraw_from_gaza }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			custom_effect_tooltip = kadima_in_power_tt
# 			if = {
# 				limit = { has_start_date < 2016.01.01 }
# 				hidden_effect = {
# 					clear_array = ruling_party
# 					clear_array = gov_coalition_array
# 					add_to_array = { ruling_party = 4 } #Monarchists
# 					add_to_array = { gov_coalition_array = 21 } #Populists/Fascists
# 					update_government_coalition_strength = yes
# 					update_party_name = yes
# 					set_coalition_drift = yes
# 	               	set_ruling_leader = yes
#					set_leader = yes
# 					set_politics = {
# 						ruling_party = democratic
# 						elections_allowed = yes
# 					}
# 				}
# 			}
# 		}
# 	}

# 	#Focus for The Lebanon War
# 	focus = {
# 		id = ISR_the_lebanon_war
# 		icon = attack_lebanon
# 		x = 4
# 		y = 6
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_ehud_olmert }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			if = {
# 				limit = { has_start_date < 2016.01.01 }
# 				declare_war_on = {
# 					target = HEZ
# 					type = annex_everything
# 				}
# 				declare_war_on = {
# 					target = LEB
# 					type = annex_everything
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Netanyahu
# 	focus = {
# 		id = ISR_netanyahu
# 		icon = conservative
# 		x = 4
# 		y = 7
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_lebanon_war }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			custom_effect_tooltip = likud_in_power_tt
# 			if = {
# 				limit = { has_start_date < 2016.01.01 }
# 				hidden_effect = {
# 					clear_array = ruling_party
# 					clear_array = gov_coalition_array
# 					add_to_array = { ruling_party = 3 } #Monarchists
# 					add_to_array = { gov_coalition_array = 21 } #Populists/Fascists
# 					update_government_coalition_strength = yes
# 					update_party_name = yes
# 					set_coalition_drift = yes
# 	               	set_ruling_leader = yes
#					set_leader = yes
# 					set_politics = {
# 						ruling_party = democratic
# 						elections_allowed = yes
# 					}
# 				}
# 			}
# 		}
# 	}

# 	#Focus for the IDF
# 	focus = {
# 		id = ISR_the_idf
# 		icon = license_production
# 		x = 11
# 		y = 0
# 		cost = 10
# 		available_if_capitulated = yes
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			air_experience = 20
# 			army_experience = 20
# 			navy_experience = 20
# 		}
# 	}

# 	#Focus for Battlefield Support
# 	focus = {
# 		id = ISR_battlefield_support
# 		icon = military_deal
# 		x = 9
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_idf }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_tech_bonus = {
# 				bonus = 0.5
# 				uses = 2
# 				category = Cat_battlefield_support
# 			}
# 		}
# 	}

# 	#Focus for Doctrine of the Future
# 	focus = {
# 		id = ISR_doctrine_of_the_future
# 		icon = cryptography
# 		x = 13
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_idf }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_tech_bonus = {
# 				bonus = 0.5
# 				uses = 2
# 				category = Cat_NETWORK_CENTRIC_WARFARE
# 			}
# 		}
# 	}

# 	#Focus for Expand the Israeli Navy
# 	focus = {
# 		id = ISR_expand_the_israeli_navy
# 		icon = navy3
# 		x = 9
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = {
# 			focus = ISR_battlefield_support
# 			focus = ISR_doctrine_of_the_future
# 		}
# 		ai_will_do = {
# 			factor = 1
# 		}
# 			completion_reward = {
# 			204 = {
# 				add_building_construction = {
# 					type = dockyard
# 					level = 4
# 					instant_build = yes
# 				}
# 			}
# 			set_temp_variable = { treasury_change = -15 }
# 			modify_treasury_effect = yes
# 		}
# 	}

# 	#Focus for The Iron Dome
# 	focus = {
# 		id = ISR_the_iron_dome
# 		icon = radar2
# 		x = 13
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = {
# 			focus = ISR_doctrine_of_the_future
# 			focus = ISR_battlefield_support
# 		}
# 		ai_will_do = {
# 			factor = 1
# 		}
# 			completion_reward = {
# 				set_variable = { defence_israel = 70 }
# 				custom_effect_tooltip = increase_missile_defence_cost_tt
# 		}
# 	}


# 	#Focus for Reform Healthcare
# 	focus = {
# 		id = ISR_reform_healthcare
# 		icon = generic_hospital
# 		x = 29
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_jewish_state }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			increase_healthcare_budget = yes
# 		}
# 	}

# 	#Focus for Our ties with the east
# 	focus = {
# 		id = ISR_our_ties_with_the_east
# 		icon = race_relations2
# 		x = 52
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_foreign_relations }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_popularity = {
# 				ideology = communism
# 				popularity = 0.05
# 			}
# 		}
# 	}

# 	#Focus for The F35
# 	focus = {
# 		id = ISR_the_f35
# 		icon = modern_fighter
# 		x = 45
# 		y = 4
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_usisraeli_weapons_development }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			set_technology = {
# 				CV_MR_Fighter5 = 1
# 			}
# 		}
# 	}

# 	#Focus for Push for Designation
# 	focus = {
# 		id = ISR_push_for_designation
# 		icon = treaty3
# 		x = 41
# 		y = 4
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_encourage_eu_trade }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			every_country = {
# 				limit = {
# 					has_idea = EU_member
# 				}
# 				country_event = { id = Israel.3 days = 1
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Encourage EU trade
# 	focus = {
# 		id = ISR_encourage_eu_trade
# 		icon = trade_with_europe
# 		x = 41
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_eu_relations }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = eu_trade_isr
# 		}
# 	}

# 	#Focus for US-Israeli Weapons Development
# 	focus = {
# 		id = ISR_usisraeli_weapons_development
# 		icon = m1a1_abrams
# 		x = 45
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_us_relations }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_to_tech_sharing_group = israel_usa_Tech_Share
# 			USA = {
# 				add_to_tech_sharing_group = israel_usa_Tech_Share
# 			}
# 		}
# 	}

# 	#Focus for US relations
# 	focus = {
# 		id = ISR_us_relations
# 		icon = align_to_america
# 		x = 45
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_our_brothers_in_the_west }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_opinion_modifier = {
# 				target = USA
# 				modifier = large_increase
# 			}
# 			add_ideas = american_support
# 		}
# 	}

# 	#Focus for EU relations
# 	focus = {
# 		id = ISR_eu_relations
# 		icon = align_to_europe
# 		x = 41
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_our_brothers_in_the_west }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			every_country = {
# 				limit = {
# 					has_idea = EU_member
# 				}
# 				add_opinion_modifier = {
# 					target = ISR
# 					modifier = medium_increase
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Our brothers in the west
# 	focus = {
# 		id = ISR_our_brothers_in_the_west
# 		icon = diplomacy
# 		x = 43
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_foreign_relations }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_popularity = {
# 				ideology = democratic
# 				popularity = 0.05
# 			}
# 		}
# 	}

# 	#Focus for Foreign Relations
# 	focus = {
# 		id = ISR_foreign_relations
# 		icon = internationalism
# 		x = 47
# 		y = 0
# 		cost = 5
# 		available_if_capitulated = yes
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_political_power = 20
# 		}
# 	}

# 	#Focus for Drone Capitol
# 	focus = {
# 		id = ISR_drone_capitol
# 		icon = united_states_dollar
# 		x = 18
# 		y = 7
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_superior_drones }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = drone_capitol
# 		}
# 	}

# 	#Focus for Superior Drones
# 	focus = {
# 		id = ISR_superior_drones
# 		icon = drone
# 		x = 18
# 		y = 6
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_drone_warfare }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			air_experience = 40
# 		}
# 	}

# 	#Focus for Drone Warfare
# 	focus = {
# 		id = ISR_drone_warfare
# 		icon = escort_fighters
# 		x = 18
# 		y = 5
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = {
# 			focus = ISR_next_generation_warfare
# 		}
# 		prerequisite = {
# 			focus = ISR_invest_in_the_military
# 		}
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_tech_bonus = {
# 				bonus = 0.5
# 				uses = 2
# 				category = Cat_A_UAV
# 			}
# 		}
# 	}

# 	#Focus for Expand Aman
# 	focus = {
# 		id = ISR_expand_aman
# 		icon = espionage3
# 		x = 15
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = {
# 			focus = ISR_israeli_intelligence_services
# 		}
# 		prerequisite = {
# 			focus = ISR_the_idf
# 		}
# 		completion_reward = {
# 			add_ideas = aman
# 		}
# 		ai_will_do = {
# 			factor = 1
# 		}
# 	}

# 	#Focus for Targeted Assassination's
# 	focus = {
# 		id = ISR_targeted_assassinations
# 		icon = sniper
# 		x = 17
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_expand_the_mossad }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			custom_effect_tooltip = ISR_targeted_assassinations_mechanic_tt
# 		}
# 	}

# 	#Focus for The Shin bet
# 	focus = {
# 		id = ISR_the_shin_bet
# 		icon = bloody_purge
# 		x = 20
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_israeli_intelligence_services }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = shin_bet
# 		}
# 	}

# 	#Focus for Expand the Mossad
# 	focus = {
# 		id = ISR_expand_the_mossad
# 		icon = espionage2
# 		x = 17
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_israeli_intelligence_services }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = mossad
# 		}
# 	}

# 	#Focus for Israeli Intelligence Services
# 	focus = {
# 		id = ISR_israeli_intelligence_services
# 		icon = espionage
# 		x = 18
# 		y = 1
# 		cost = 5
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_jewish_state }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			create_intelligence_agency = yes
# 		}
# 	}

# 	#Focus for Highway Project One
# 	focus = {
# 		id = ISR_highway_project_one
# 		icon = infrastructure1
# 		x = 32
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_expand_the_police_force }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			207 = {
# 				add_building_construction = {
# 					type = infrastructure
# 					level = 4
# 					instant_build = yes
# 				}
# 			}
# 			set_temp_variable = { treasury_change = -7.5 }
# 			modify_treasury_effect = yes
# 		}
# 	}

# 	#Focus for Highway Project Two
# 	focus = {
# 		id = ISR_highway_project_two
# 		icon = infrastructure
# 		x = 32
# 		y = 4
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_highway_project_one }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			log = "[GetDateText]: [Root.GetName]: Focus ISR_highway_project_two"
# 			206 = {
# 				add_building_construction = {
# 					type = infrastructure
# 					level = 3
# 					instant_build = yes
# 				}
# 			}
# 			set_temp_variable = { treasury_change = -7.5 }
# 			modify_treasury_effect = yes
# 		}
# 	}

# 	#Focus for The Meged Oil Field
# 	focus = {
# 		id = ISR_the_meged_oil_field
# 		icon = oil_production2
# 		x = 32
# 		y = 5
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_highway_project_two }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 		204 = {
# 			add_resource = {
# 				type = oil
# 				amount = 8
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Leviathan Gas Field
# 	focus = {
# 		id = ISR_leviathan_gas_field
# 		icon = oil
# 		x = 32
# 		y = 6
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_meged_oil_field }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 		204 = {
# 			add_resource = {
# 				type = oil
# 				amount = 18
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Israeli Innovation
# 	focus = {
# 		id = ISR_israeli_innovation
# 		icon = research4
# 		x = 35
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_jewish_state }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_research_slot = 1
# 		}
# 	}

# 	#Focus for Expand Yozma
# 	focus = {
# 		id = ISR_expand_yozma
# 		icon = liberalization_policies
# 		x = 35
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_israeli_innovation }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = yozma
# 			set_temp_variable = { treasury_change = -15 }
# 			modify_treasury_effect = yes
# 		}
# 	}

# 	#Focus for Tech Hub of the World
# 	focus = {
# 		id = ISR_tech_hub_of_the_world
# 		icon = tech_sharing
# 		x = 35
# 		y = 3
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_expand_yozma }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 		add_research_slot = 1
# 		}
# 	}

# 	#Focus for Bastion of Democracy
# 	focus = {
# 		id = ISR_bastion_of_democracy
# 		icon = blue_democracy
# 		x = 38
# 		y = 1
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_jewish_state }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_popularity = {
# 				ideology = democratic
# 				popularity = 0.1
# 			}
# 		}
# 	}

# 	#Focus for Welcome Minorities
# 	focus = {
# 		id = ISR_welcome_minorities
# 		icon = pro_immigration1
# 		x = 38
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_bastion_of_democracy }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			custom_effect_tooltip = ISR_minorities_mechanic_tt
# 		}
# 	}

# 	#Focus for Beta Israel
# 	focus = {
# 		id = ISR_beta_israel
# 		icon = support_the_left_right
# 		x = 58
# 		y = 2
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_our_ties_with_the_east }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_opinion_modifier = {
# 				target = ETH
# 				modifier = medium_increase
# 			}
# 		}
# 	}


# 	#Focus for The one state solution
# 	focus = {
# 		id = ISR_the_one_state_solution
# 		icon = one_state_solution
# 		x = 6
# 		y = 8
# 		cost = 10
# 		available_if_capitulated = yes
# 		mutually_exclusive = { focus = ISR_the_two_state_solution }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_threat = 5
# 			annex_country = {
# 				target = PAL
# 			}
# 		}
# 	}

# 	#Focus for The two state solution
# 	focus = {
# 		id = ISR_the_two_state_solution
# 		icon = two_state_solution
# 		x = 16
# 		y = 8
# 		cost = 10
# 		available_if_capitulated = yes
# 		mutually_exclusive = { focus = ISR_the_one_state_solution }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_threat = -5
# 		}
# 	}

# 	#Focus for Israel First
# 	focus = {
# 		id = ISR_israel_first
# 		icon = molotov_ribbentrop_pact
# 		x = 7
# 		y = 9
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_one_state_solution }
# 		mutually_exclusive = { focus = ISR_the_new_palestine }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_threat = 3
# 			add_ideas = cheap_pal_labour
# 			add_stability = -0.1
# 			add_popularity = {
# 				ideology = nationalist
# 				popularity = 0.05
# 			}
# 		}
# 	}

# 	#Focus for Equal Rights
# 	focus = {
# 		id = ISR_equal_rights
# 		icon = national_unity_red
# 		x = 3
# 		y = 9
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_one_state_solution }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_threat = -3
# 			add_stability = 0.1
# 		}
# 	}

# 	#Focus for Israel of 2 Peoples
# 	focus = {
# 		id = ISR_israel_of_2_peoples
# 		icon = angel
# 		x = 3
# 		y = 10
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_equal_rights }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 		208 = {
# 			add_core_of = ISR
# 			}
# 		209 = {
# 			add_core_of = ISR
# 			}
# 		}
# 	}

# 	#Focus for Law of Return
# 	focus = {
# 		id = ISR_law_of_return
# 		icon = pass_legislation
# 		x = 2
# 		y = 11
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_israel_of_2_peoples }
# 		mutually_exclusive = { focus = ISR_only_israelis }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = palestinians_return
# 		}
# 	}

# 	#Focus for Only Israelis
# 	focus = {
# 		id = ISR_only_israelis
# 		icon = self_management
# 		x = 4
# 		y = 11
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_israel_of_2_peoples }
# 		mutually_exclusive = { focus = ISR_law_of_return }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_stability = 0.1
# 			add_ideas = pal_diaspora
# 		}
# 	}

# 	#Focus for The New Palestine
# 	focus = {
# 		id = ISR_the_new_palestine
# 		icon = align_to_palestine
# 		x = 11
# 		y = 9
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_one_state_solution }
# 		mutually_exclusive = { focus = ISR_israel_first }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_popularity = {
# 				ideology = nationalist
# 				popularity = 0.10
# 			}
# 			add_stability = -0.05
# 			add_threat = 5
# 			208 = {
# 				add_manpower = -700000
# 				add_core_of = ISR
# 				}
# 			209 = {
# 				add_manpower = -700000
# 				add_core_of = ISR
# 				}
# 			210 = {
# 				add_manpower = 140000
# 				}
# 			}
# 		}

# 	#Focus for Greater Israel
# 	focus = {
# 		id = ISR_greater_israel
# 		icon = aggressive_diplomacy
# 		x = 9
# 		y = 10
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = {
# 			focus = ISR_israel_first
# 			focus = ISR_the_new_palestine
# 		}
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_popularity = {
# 				ideology = nationalist
# 				popularity = 0.10
# 			}
# 			add_ideas = greater_israel
# 		}
# 	}

# 	#Focus for Protect our border
# 	focus = {
# 		id = ISR_protect_our_border
# 		icon = generic_entrenchment
# 		x = 9
# 		y = 11
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_greater_israel }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 		ISR = {
# 			create_wargoal = {
# 				type = annex_everything
# 				target = JOR
# 				}
# 			}
# 		}
# 	}

# 	#Focus for Dependence
# 	focus = {
# 		id = ISR_dependence
# 		icon = forced_labour
# 		x = 14
# 		y = 9
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_two_state_solution }
# 		mutually_exclusive = { focus = ISR_independence }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_ideas = cheap_pal_labour
# 		}
# 	}

# 	#Focus for Independence
# 	focus = {
# 		id = ISR_independence
# 		icon = peace
# 		x = 18
# 		y = 9
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_the_two_state_solution }
# 		mutually_exclusive = { focus = ISR_dependence }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			add_stability = 0.2
# 		}
# 	}

# 	#Focus for Demilitarize Palestine
# 	focus = {
# 		id = ISR_demilitarize_palestine
# 		icon = tripartite_pact
# 		x = 16
# 		y = 10
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = {
# 			focus = ISR_independence
# 		}
# 		prerequisite = {
# 			focus = ISR_dependence
# 		}
# 		completion_reward = {
# 			208 = {
# 				set_demilitarized_zone = yes
# 				}
# 			209 = {
# 				set_demilitarized_zone = yes
# 				}
# 		}
# 		ai_will_do = {
# 			factor = 1
# 		}
# 	}

# 	#Focus for Israeli Arabs
# 	focus = {
# 		id = ISR_israeli_arabs
# 		icon = treaty
# 		x = 16
# 		y = 11
# 		cost = 10
# 		available_if_capitulated = yes
# 		prerequisite = { focus = ISR_demilitarize_palestine }
# 		ai_will_do = {
# 			factor = 1
# 		}
# 		completion_reward = {
# 			country_event = { id = Israel.6 days = 1 }
# 		}
# 	}
# 	#End of focuses
# }